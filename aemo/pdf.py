import os.path
from functools import partial
from io import BytesIO

from django.db.models import Q
from django.utils.text import slugify

from reportlab.lib.units import cm
from reportlab.lib.enums import TA_LEFT
from reportlab.lib import colors
from reportlab.platypus import KeepTogether, PageBreak, Paragraph, Table, TableStyle, Spacer

from pypdf import PdfWriter, PdfReader

from carrefour.pdf import BaseCarrefourPDF, CleanParagraph, GenogrammePdf, SociogrammePdf
from carrefour.utils import format_d_m_Y, str_or_empty

"""
Organisation des documents PDF pour Aemo:

1. AnalyseDemandePdf (CoordonneesPagePdf + GenogrammePdf + SociogrammePdf + DemandeAccompagnementPagePdf)
2. CoordonneesPdf
3. ContratCollaborationPdf
"""


class AnalyseDemandePdf:
    def __init__(self, tampon):
        self.tampon = tampon
        self.merger = PdfWriter()

    def append_pdf(self, PDFClass):
        tampon = BytesIO()
        pdf = PDFClass(tampon)
        pdf.produce(self.famille)
        self.merger.append(tampon)

    def produce(self, famille, **kwargs):
        self.famille = famille

        self.append_pdf(CoordonneesPagePdf)
        self.append_pdf(DemandeAccompagnementPagePdf)

        if famille.genogramme and os.path.exists(famille.genogramme.path):
            self.merger.append(PdfReader(famille.genogramme.path))
        else:
            self.append_pdf(GenogrammePdf)

        if famille.sociogramme and os.path.exists(famille.sociogramme.path):
            self.merger.append(PdfReader(famille.sociogramme.path))
        else:
            self.append_pdf(SociogrammePdf)

        self.merger.write(self.tampon)

    def get_filename(self, famille):
        return '{}_analyse_demande.pdf'.format(slugify(famille.nom))


class CoordonneesFamillePdf(BaseCarrefourPDF):
    title = "Coordonnées"

    def get_filename(self, famille):
        return '{}_coordonnees.pdf'.format(slugify(famille.nom))

    def produce(self, famille, **kwargs):

        suivi = famille.suiviaemo
        P = partial(Paragraph, style=self.style_normal)

        self.set_title('Famille {} - {}'.format(famille.nom, self.title))

        # Cadre supérieur
        data = [
            ["Date de l’entretien: {}".format(format_d_m_Y(suivi.date_analyse)),
                "Intervenants AEMO présents: {}".format(suivi.ref_aemo_presents)],
            [P("Membres famille présents: {}".format(suivi.pers_famille_presentes)),
             P("Autres pers. présentes: {}".format(suivi.autres_pers_presentes))],
            ["Orienté par: {}".format(suivi.get_service_orienteur_display())],
            ['Date de la confirmation: {}'.format(format_d_m_Y(suivi.date_confirmation)),
                'Date de début de suivi {}'.format(format_d_m_Y(suivi.date_debut_suivi))]
        ]
        t = Table(
            data=data, colWidths=[9 * cm, 9 * cm], hAlign=TA_LEFT,
            spaceBefore=0 * cm, spaceAfter=0.5 * cm
        )
        t.hAlign = 0
        t.setStyle(tblstyle=TableStyle([
            ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
            ('FONTSIZE', (0, 0), (-1, -1), self.base_font_size),
            ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
            ('LINEABOVE', (0, 1), (-1, 1), 0.25, colors.black),
        ]))
        self.story.append(t)

        # Parents
        self.story.append(Paragraph('Parents', self.style_sub_title))
        data = self.formate_persons([famille.mere(), famille.pere()])
        self.story.append(self.get_table(data, columns=[2.2, 6.8, 2.2, 6.8], before=0, after=0))

        # Situation matrimoniale
        data = [
            ['Situation matrimoniale: {}'.format(famille.get_statut_marital_display()),
             'Autorité parentale: {}'.format(famille.get_autorite_parentale_display())],
        ]
        self.story.append(self.get_table(data, columns=[9, 9], before=0, after=0))

        # Personnes significatives
        autres_parents = list(famille.autres_parents())
        if autres_parents:
            self.story.append(Paragraph('Personne-s significative-s', self.style_sub_title))
            data = self.formate_persons(autres_parents)
            self.story.append(self.get_table(data, columns=[2.2, 6.8, 2.2, 6.8], before=0, after=0))
            if len(autres_parents) > 2:
                self.story.append(PageBreak())

        # Enfants suivis
        enfants = famille.membres.filter(
            Q(role__nom='Enfant suivi') | Q(role__nom='Enfant non-suivi')
        ).order_by('-date_naissance')

        titres = [
            'Nom', 'Prénom', 'Suivi', 'Filiation', 'Naissance', 'Tél.', 'Formation', 'Classe',
            'Centre scol.', 'École/Coll.', 'Enseignant', 'Struct. E-F.'
        ]
        formations = [
            enfant.formation if hasattr(enfant, 'formation') else None
            for enfant in enfants
        ]
        data = [
            [P(enfant.nom) for enfant in enfants],
            [P(enfant.prenom) for enfant in enfants],
            [P({'Enfant suivi': 'Oui', 'Enfant non-suivi': 'Non'}[enfant.role.nom]) for enfant in enfants],
            [P(enfant.filiation) for enfant in enfants],
            [P(format_d_m_Y(enfant.date_naissance)) for enfant in enfants],
            [P(enfant.telephone) for enfant in enfants],
            [P(formation.get_statut_display()) if formation else '' for formation in formations],
            [Paragraph(formation.classe, self.style_normal) if formation else '' for formation in formations],
            [P(str_or_empty(formation.cercle_scolaire)) if formation else '' for formation in formations],
            [P(str_or_empty(formation.college)) if formation else '' for formation in formations],
            [P(str_or_empty(formation.enseignant)) if formation else '' for formation in formations],
            [P(str_or_empty(formation.creche)) if formation else '' for formation in formations],
        ]

        for i, value in enumerate(titres):
            data[i].insert(0, value)
        self.story.append(KeepTogether([
            Paragraph('Enfants', self.style_sub_title),
            self.get_table(data, columns=[2.2, 3, 3, 3, 3, 2.8], before=0, after=0)
        ]))

        # Réseau
        data = [
            ['Famille', '{} - (Mandat: {})'.format(
                suivi.ope_referent if suivi.ope_referent else '',
                suivi.get_mandat_ope_display()
            )]
        ]
        for enfant in famille.membres_suivis():
            for contact in enfant.reseaux.all():
                data.append([
                    enfant.prenom,
                    '{} ({})'.format(contact, contact.contact)
                ])
        self.story.append(KeepTogether([
            Paragraph("Réseau", self.style_sub_title),
            self.get_table(data, columns=[2.5, 15.5], before=0, after=0)
        ]))

        self.print_chronologie(famille)

        return super().produce()


class DemandeAccompagnementPagePdf(BaseCarrefourPDF):
    title = "Demande d'accompagnement AEMO"

    def produce(self, famille, **kwargs):
        suivi = famille.suiviaemo

        self.set_title('Famille {} - {}'.format(famille.nom, self.title))

        self.story.append(Paragraph("Motif(s) de la demande: {}".format(
            suivi.get_motif_demande_display()), self.style_normal
        ))
        self.story.append(Paragraph('_' * 90, self.style_normal))
        self.story.append(Spacer(0, 0.5 * cm))
        self.story.append(Paragraph('Difficultés', self.style_sub_title))
        self.story.append(Paragraph(
            "Quelles sont les difficultés éducatives que vous rencontrez et depuis combien de temps ?",
            self.style_normal
        ))
        self.story.append(Paragraph(
            "Fonctionnement familial: règles, coucher, lever, repas, jeux, relations parent-enfants,"
            " rapport au sein de la fratrie, … (exemple)",
            self.style_italic
        ))
        self.story.append(Spacer(0, 0.5 * cm))
        self.story.append(CleanParagraph(suivi.difficultes, self.style_indent))

        self.story.append(KeepTogether([
            Paragraph('Aides souhaitées', self.style_sub_title),
            CleanParagraph(suivi.aides, self.style_indent)
        ]))
        self.story.append(KeepTogether([
            Paragraph('Autres services', self.style_sub_title),
            Paragraph(suivi.autres_contacts, self.style_indent)
        ]))
        self.story.append(KeepTogether([
            Paragraph('Disponibilités', self.style_sub_title),
            CleanParagraph(suivi.disponibilites, self.style_indent)
        ]))
        self.story.append(KeepTogether([
            Paragraph('Remarques', self.style_sub_title),
            Paragraph(suivi.remarque, self.style_indent)
        ]))
        return super().produce()


class ContratCollaborationPdf(BaseCarrefourPDF):
    title = "Contrat de collaboration AEMO"

    def get_filename(self, famille):
        return '{}_contrat_collaboration.pdf'.format(slugify(famille.nom))

    def produce(self, famille, **kwargs):
        self.set_title('{} - Famille {}'.format(self.title, famille.nom))
        suivi = famille.suiviaemo

        if suivi.ope_referent and suivi.mandat_ope and 'tutelle' in suivi.mandat_ope:
            repr_legal = "Le tuteur / la tutrice"
            nom_repr_legal = suivi.ope_referent.nom_prenom
        else:
            repr_legal = "Les Parents"
            nom_repr_legal = ""

        P = partial(Paragraph, style=self.style_normal)
        Psub = partial(Paragraph, style=self.style_sub_title)

        self.story.append(Psub("Enfants/Jeunes"))
        for enf in famille.membres_suivis():
            self.story.append(P('{} (*{})'.format(enf.nom_prenom, format_d_m_Y(enf.date_naissance))))

        # Parents
        self.story.append(Psub('Parents'))
        data = self.formate_persons(famille.parents_contractant())
        if data:
            self.story.append(self.get_table(data, columns=[2.2, 6.8, 2.2, 6.8], before=0, after=0.5))

        # Personnes significatives
        autres_parents = list(famille.autres_parents().filter(contractant=True))
        if autres_parents:
            self.story.append(Paragraph('Personne-s significative-s', self.style_sub_title))
            data = self.formate_persons(autres_parents)
            self.story.append(self.get_table(data, columns=[2.2, 6.8, 2.2, 6.8], before=0, after=0))
            if len(autres_parents) > 2:
                self.story.append(PageBreak())

        self.story.append(Psub("Date du début de l’accompagnement : {}".format(
            format_d_m_Y(suivi.date_debut_suivi) if suivi.date_debut_suivi else '')
        ))
        self.story.append(Psub("Orienté par: {}".format(suivi.get_service_orienteur_display())))
        self.story.append(Psub("1. Les modalités"))
        data = [
            [
                "Durée de l’accompagnement:", "18 mois au maximum."
            ],
            [
                P("Office de protection de l’enfant (OPE):"),
                P(
                    "Un-e intervenant-e en protection de l’enfant (IPE) est référent-e du suivi. "
                    "Une copie du contrat lui est envoyée et sa présence est requise "
                    "au minimum lors du bilan des trois mois et lors du bilan final.")
            ],
            [
                P("Intervenant-e en protection de l’enfant (IPE):"),
                P('{} - (mandat: {})'.format(
                    suivi.ope_referent.nom_prenom, suivi.get_mandat_ope_display()))
                if suivi.ope_referent else ''
            ],
            [
                "Coût:",
                P("Aucune participation financière n’est demandée à la famille.")
            ],
            [
                "Annulation des rendez-vous:",
                "Par téléphone."
            ],
            [
                "Confidentialité:",
                P(
                    "Garantie d’un espace de confidentialité pour autant que l’intégrité "
                    "physique et psychique des personnes impliquées ne soit pas mise en danger.")
            ],
            [
                "Collaboration:",
                "Avec les professionnel-le-s du réseau."
            ],
            [
                "Signature:",
                P("Documents «Contrat de collaboration» et «Attentes et projet commun».")
            ],
            [
                "Arrêt de l’accompagnement:",
                P("Un bilan de fin d’accompagnement doit avoir lieu.")
            ],
        ]
        self.story.append(self.get_table(data, columns=[6, 10], before=0.0, after=0.0))
        self.story.append(Psub("2. L’organisation"))
        referents = suivi.suivi_referents
        data = [
            [
                "Lieux de l’intervention:",
                "Domicile, extérieur et/ou bureau."
            ],
            [
                "Fréquence des rencontres:",
                P("En règle générale, 1 fois par semaine, modulable et évolutif dans le temps.")
            ],
            [
                "Prestations collectives:",
                P(
                    "Des prestations collectives peuvent vous être proposées durant "
                    "l’accompagnement (activités en groupe, groupes de paroles, etc.")
            ],
            [
                "Bilans:",
                P("À trois mois, à une année et à la fin de l’accompagnement.")
            ],
            [
                "Engagement:",
                P("Un engagement personnel est indispensable à cette démarche.")
            ],
            [Spacer(0, 1*cm)],
            ["Lieu et date:"],
            [],
            ["Je m’engage à respecter les points ci-dessus:"],
            [],
            [f"{repr_legal}:", "pour l'AEMO"],
            [],
            [nom_repr_legal, ', '.join([f'{ref.nom_prenom}' for ref in referents])],
            [Spacer(0, 1*cm)],
            ['{} {} {}'.format('-'*60, 'FIN DU CONTRAT', '-'*60)],
            [],
            ["Date du bilan de fin:", format_d_m_Y(suivi.date_bilan_final)],
            ["Date de fin de l’accompagnement:", format_d_m_Y(suivi.date_fin_suivi)],
            [],
            ["Lieu et date:"],
            [],
            [f"{repr_legal}:", "pour l'AEMO"],
            [],
            [nom_repr_legal, ', '.join([f'{ref.nom_prenom}' for ref in referents])],
        ]
        self.story.append(self.get_table(data, columns=[6, 10], before=0.0, after=0.5))
        self.story.append(Spacer(0, 3 * cm))
        if suivi.ope_referent:
            service = suivi.ope_referent.service.nom_complet if suivi.ope_referent.service else ''
            self.story.append(P("Copie: {}, {}".format(
                suivi.ope_referent.nom_prenom, service))
            )
        self.doc.build(self.story, onFirstPage=self.draw_header, onLaterPages=self.draw_footer)


class CoordonneesPagePdf(CoordonneesFamillePdf):
    title = "Analyse de la demande AEMO"

    def get_filename(self, famille):
        return '{}_analyse_demande.pdf'.format(slugify(famille.nom))
