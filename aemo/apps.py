from django.apps import AppConfig


class AemoConfig(AppConfig):
    name = 'aemo'

    @property
    def benef_model(self):
        from aemo.models import FamilleAemo
        return FamilleAemo
