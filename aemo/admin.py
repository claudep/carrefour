from django.contrib import admin

from carrefour.admin import TypePrestationFilter
from .models import FamilleAemo, PrestationAemo, SuiviAemo, JournalAemo


@admin.register(FamilleAemo)
class FamilleAemoAdmin(admin.ModelAdmin):
    list_display = ('nom', 'npa', 'localite', 'region')
    list_filter = ('region',)
    search_fields = ['nom', 'npa', 'localite']
    ordering = ('nom',)


@admin.register(SuiviAemo)
class SuiviAemoAdmin(admin.ModelAdmin):
    list_display = ('famille', 'equipe', 'etape')
    list_filter = ('equipe',)
    search_fields = ('famille__nom',)
    ordering = ('famille__nom',)


@admin.register(PrestationAemo)
class PrestationAemoAdmin(admin.ModelAdmin):
    list_filter = (TypePrestationFilter,)
    search_fields = ('famille__nom',)


@admin.register(JournalAemo)
class JournalAemoAdmin(admin.ModelAdmin):
    search_fields = ('famille__nom',)
    ordering = ('famille__nom', '-date')
