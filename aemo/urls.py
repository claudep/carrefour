from django.urls import path

from aemo import views

urlpatterns = [
    # Famille
    path('famille/list/', views.FamilleListView.as_view(), name='aemo-famille-list'),
    path('famille/attente/', views.FamilleListView.as_view(mode='attente'), name='aemo-famille-attente'),
    path('famille/add/', views.FamilleCreateView.as_view(), name='aemo-famille-add'),
    path('famille/<int:pk>/edit/', views.FamilleUpdateView.as_view(), name='aemo-famille-edit'),
    path('famille/<int:pk>/delete/', views.FamilleDeleteView.as_view(), name='aemo-famille-delete'),
    path('famille/archivable/list/', views.FamilleArchivableListe.as_view(), name='aemo-famille-archivable'),
    path('famille/<int:pk>/niveau/add/', views.NiveauCreateUpdateView.as_view(), name='famille-niveau-add'),
    path('famille/<int:pk>/niveau/<int:niv_pk>/edit/', views.NiveauCreateUpdateView.as_view(),
         name='famille-niveau-edit'
         ),
    path('famille/<int:pk>/niveau/<int:niv_pk>/delete/', views.NiveauDeleteView.as_view(),
         name='famille-niveau-delete'),

    # Prestations
    path('prestation/menu/', views.PrestationMenu.as_view(), name='aemo-prestation-menu'),
    path('prestation/recap_perso/', views.PrestationRecapPersoView.as_view(), name='aemo-prestation-recap-perso'),
    path('famille/<int:pk>/prestation/list/', views.PrestationListView.as_view(), name='aemo-prestation-famille-list'),
    path('famille/<int:pk>/prestation/add/', views.PrestationCreateView.as_view(), name='aemo-prestation-famille-add'),
    path('famille/<int:pk>/prestation/<int:obj_pk>/edit/', views.PrestationUpdateView.as_view(),
         name='aemo-prestation-edit'),
    path('famille/<int:pk>/prestation/<int:obj_pk>/delete/', views.PrestationDeleteView.as_view(),
         name='aemo-prestation-delete'),
    path('prestation_gen/list/', views.PrestationListView.as_view(), name='aemo-prestation-gen-list'),
    path('prestation_gen/add/', views.PrestationCreateView.as_view(), name='aemo-prestation-gen-add'),
    path('prestations/list/', views.AdminPrestationListView.as_view(), name='aemo-prestation-adminlist'),

    # Doc. à imprimer
    path('famille/<int:pk>/print-coord/', views.AnalyseDemandePDFView.as_view(),
         name='aemo-print-analyse-demande'),
    path('famille/<int:pk>/print-contrat/', views.ContratCollaborationPDFView.as_view(),
         name='aemo-print-contrat-collaboration'),
    path('famille/<int:pk>/print-info/', views.CoordonneesPDFView.as_view(),
         name='aemo-print-coord-famille'),
    path('famille/<int:pk>/print-journal', views.JournalPDFView.as_view(),
         name='aemo-print-journal'),
    path('famille/<int:pk>/bilan_3mois/', views.print_docx, {'docname': 'bilan_3mois'},
         name='print-bilan-3mois'),
    path('famille/<int:pk>/bilan_annee/', views.print_docx, {'docname': 'bilan_annee'},
         name='print-bilan-annee'),
    path('famille/<int:pk>/bilan_final/', views.print_docx, {'docname': 'bilan_final'},
         name='print-bilan-final'),
    path('famille/<int:pk>/projet_commun/', views.print_docx, {'docname': 'projet_commun'},
         name='print-projet-commun'),

    # Demande, suivi, agenda, suivis terminés
    path('famille/<int:pk>/demande/', views.DemandeView.as_view(), name='aemo-demande'),
    path('famille/<int:pk>/suivi/', views.AemoSuiviView.as_view(), name='aemo-famille-suivi'),
    path('famille/<int:pk>/agenda/', views.AgendaAemoSuiviView.as_view(), name='aemo-famille-agenda'),
    path('suivis_termines/', views.SuivisTerminesListView.as_view(), name='aemo-suivis-termines'),

    # Journal
    path('famille/<int:pk>/journal/list/', views.JournalListView.as_view(), name='aemo-journal-list'),
    path('famille/<int:pk>/journal/add/', views.JournalCreateView.as_view(), name='aemo-journal-add'),
    path('famille/<int:pk>/journal/<int:obj_pk>/edit/', views.JournalUpdateView.as_view(),
         name='aemo-journal-edit'),
    path('famille/<int:pk>/journal/<int:obj_pk>/delete/', views.JournalDeleteView.as_view(),
         name='aemo-journal-delete'),
]
