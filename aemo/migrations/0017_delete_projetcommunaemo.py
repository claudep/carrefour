from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aemo', '0016_Migration_projet_commun'),
    ]

    operations = [
        migrations.DeleteModel(
            name='ProjetCommunAemo',
        ),
    ]
