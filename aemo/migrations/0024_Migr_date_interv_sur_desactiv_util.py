from django.db import migrations


def fin_interventions(apps, schema_editor):
    """
    Marque la fin d'une intervention si l'utilisateur a été désactivé avant la
    fin du suivi.
    """
    Utilisateur = apps.get_model('carrefour', 'Utilisateur')
    for util in Utilisateur.objects.filter(date_desactivation__isnull=False):
        for interv in util.interventions.filter(date_fin__isnull=True):
            if interv.suivi.date_fin_suivi is None or util.date_desactivation < interv.suivi.date_fin_suivi:
                interv.date_fin = util.date_desactivation
                interv.save()


class Migration(migrations.Migration):

    dependencies = [
        ('aemo', '0023_Migration_aemo_intervenants'),
    ]

    operations = [
        migrations.RunPython(fin_interventions)
    ]
