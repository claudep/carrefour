from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aemo', '0011_Add_fields_to_PrestationAemo'),
    ]

    operations = [
        migrations.RenameField(
            model_name='suiviaemo',
            old_name='aemo_equipe',
            new_name='equipe',
        ),
        migrations.AlterField(
            model_name='suiviaemo',
            name='equipe',
            field=models.CharField(choices=[('montagnes', 'AEMO - Montagnes'), ('littoral', 'AEMO - Littoral')], max_length=10, verbose_name='Équipe'),
        ),
    ]
