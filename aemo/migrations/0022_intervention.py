from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('aemo', '0021_Alter_lib_prestation'),
    ]

    operations = [
        migrations.CreateModel(
            name='Intervention',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_debut', models.DateField(verbose_name='Date début')),
                ('date_fin', models.DateField(blank=True, null=True, verbose_name='Date fin')),
                ('intervenant', models.ForeignKey(on_delete=models.deletion.CASCADE, related_name='interventions', to=settings.AUTH_USER_MODEL)),
                ('suivi', models.ForeignKey(on_delete=models.deletion.CASCADE, to='aemo.suiviaemo')),
            ],
        ),
        migrations.AddField(
            model_name='suiviaemo',
            name='aemo_referents',
            field=models.ManyToManyField(blank=True, related_name='aemo_suivis', through='aemo.Intervention', to=settings.AUTH_USER_MODEL),
        ),
    ]
