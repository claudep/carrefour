from django.db import migrations


def set_niveaux_1(apps, schema_editor):
    FamilleAemo = apps.get_model('aemo', 'FamilleAemo')
    NiveauInterv = apps.get_model('carrefour', 'NiveauInterv')

    familles_actives = FamilleAemo.objects.filter(
        suiviaemo__isnull=False, suiviaemo__date_fin_suivi__isnull=True
    )
    for fam in familles_actives:
        NiveauInterv.objects.create(famille=fam, niveau=1, date_debut=fam.suiviaemo.date_debut_suivi)


class Migration(migrations.Migration):

    dependencies = [
        ('carrefour', '0045_niveauinterv'),
        ('aemo', '0024_Migr_date_interv_sur_desactiv_util'),
    ]

    operations = [
        migrations.RunPython(set_niveaux_1)
    ]
