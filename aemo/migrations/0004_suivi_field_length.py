import common.fields
from common.choices import MANDATS_OPE_CHOICES
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aemo', '0003_Modif_champs_suiviaemo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='suiviaemo',
            name='demarche',
            field=common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=[('volontaire', 'Volontaire'), ('contrainte', 'Contrainte'), ('post_placement', 'Post placement'), ('non_placement', 'Eviter placement')], max_length=60), blank=True, null=True, size=None, verbose_name='Démarche'),
        ),
        migrations.AlterField(
            model_name='suiviaemo',
            name='mandat_ope',
            field=common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=MANDATS_OPE_CHOICES, max_length=65), blank=True, null=True, size=None, verbose_name='Mandat OPE'),
        ),
        migrations.AlterField(
            model_name='suiviaemo',
            name='motif_demande',
            field=common.fields.ChoiceArrayField(base_field=models.CharField(choices=[('parentalite', 'Soutien à la parentalité'), ('education', "Aide éducative à l'enfant"), ('developpement', "Répondre aux besoins de développement de l'enfant"), ('integration', "Soutien à l'intégration sociale")], max_length=60), blank=True, null=True, size=None, verbose_name='Motif de la demande'),
        ),
    ]
