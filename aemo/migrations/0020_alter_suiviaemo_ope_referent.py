from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('carrefour', '0035_contact_unique_constraint'),
        ('aemo', '0019_Rpl_fk_contact_protect'),
    ]

    operations = [
        migrations.AlterField(
            model_name='suiviaemo',
            name='ope_referent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='suivisaemo', to='carrefour.contact', verbose_name='Interv. IPE'),
        ),
    ]
