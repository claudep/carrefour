from django.db import migrations


def migrate_aemo_referents(apps, schema_editor):
    """
    Déplace SuiviAemo.aemo_referent_1/aemo_referent_2 vers le modèle Intervention.
    """

    SuiviAemo = apps.get_model('aemo', 'SuiviAemo')
    Intervention = apps.get_model('aemo', 'Intervention')

    for suivi in SuiviAemo.objects.all():
        debut = suivi.date_demande or suivi.date_debut_suivi or suivi.date_demande_eventuelle or suivi.date_fin_suivi
        if suivi.aemo_referent_1:
            Intervention.objects.create(
                suivi=suivi, intervenant=suivi.aemo_referent_1, date_debut=debut,
            )
        if suivi.aemo_referent_2:
            if suivi.aemo_referent_1 and suivi.aemo_referent_2 and suivi.aemo_referent_1 == suivi.aemo_referent_2:
                continue
            Intervention.objects.create(
                suivi=suivi, intervenant=suivi.aemo_referent_2, date_debut=debut,
            )


class Migration(migrations.Migration):

    dependencies = [
        ('aemo', '0022_intervention'),
    ]

    operations = [
        migrations.RunPython(migrate_aemo_referents, migrations.RunPython.noop)
    ]
