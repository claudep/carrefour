from datetime import date, timedelta
from collections import OrderedDict
from operator import attrgetter

from django.db import models

from django.urls import reverse

from carrefour.models import (
    Contact, Famille, FamilleManager, JournalBase, PrestationBase, Utilisateur,
)
from carrefour.utils import delay_for_destruction

from common import choices
from common.fields import ChoiceArrayField

DELAI_ANNULATION = 60  # Delai pour annulation d'une demande non-aboutie


class Etape:
    """
        1. Numéro d'ordre pour tri dans la vue
        2. Code
        3. Affichage dans tableau
        4. Nom visible de l'étape
        5. Code de l'étape suivante
        6. Délai jusqu'à l'échéance (en jours)
        7. Code de l'étape précédente
        8. Date obligatoire précédente (code ou '')
    """
    def __init__(self, num, code, abrev, nom, suivante, delai, precedente, preced_oblig, oblig):
        self.num = num
        self.code = code
        self.abrev = abrev
        self.nom = nom
        self.suivante = suivante
        self.delai = delai
        self.precedente = precedente
        self.preced_oblig = preced_oblig
        self.oblig = oblig

    def __str__(self):
        return self.nom

    def date(self, suivi):
        return getattr(suivi, 'date_{}'.format(self.code))

    def date_suivante(self, suivi):
        """Date d'échéance de cette étape selon délai."""
        date_etape = self.date(suivi)
        return (date_etape + timedelta(self.delai)) if date_etape else None

    def date_nom(self):
        return 'date_{}'.format(self.code)

    def etape_precedente(self):
        return SuiviAemo.WORKFLOW[self.precedente]


class FamilleAemoManager(FamilleManager):
    def get_queryset(self):
        return super().get_queryset().filter(suiviaemo__isnull=False)

    def create_famille(self, equipe=None, **kwargs):
        kwargs.pop('_state', None)
        famille = self.create(**kwargs)
        SuiviAemo.objects.create(
            famille=famille,
            equipe=equipe
        )
        famille.suiviaemo.date_demande = date.today()
        famille.suiviaemo.save()
        return famille


class FamilleAemo(Famille):
    class Meta:
        proxy = True
        verbose_name = "Famille AEMO"
        verbose_name_plural = "Familles AEMO"

    typ = 'aemo'
    objects = FamilleAemoManager()

    @property
    def suivi(self):
        return self.suiviaemo

    @classmethod
    def actives(cls, date=None):
        qs = FamilleAemo.objects.filter(suiviaemo__isnull=False)
        if date is not None:
            return qs.filter(suiviaemo__date_demande__lte=date).filter(
                models.Q(suiviaemo__date_fin_suivi__isnull=True) |
                models.Q(suiviaemo__date_fin_suivi__gte=date)
            )
        else:
            return qs.filter(suiviaemo__date_fin_suivi__isnull=True)

    @classmethod
    def check_perm_view(cls, user):
        perms = set()
        if user.has_perm('aemo.view_littoral'):
            perms.add('littoral')
        if user.has_perm('aemo.view_montagnes'):
            perms.add('montagnes')
        return perms

    @classmethod
    def check_perm_change(cls, user):
        perms = set()
        if user.has_perm('aemo.change_littoral'):
            perms.add('littoral')
        if user.has_perm('aemo.change_montagnes'):
            perms.add('montagnes')
        return perms

    @classmethod
    def check_perm_manage_aemo(cls, user):
        perms = set()
        if user.has_perm('aemo.manage_littoral'):
            perms.add('littoral')
        if user.has_perm('aemo.manage_montagnes'):
            perms.add('montagnes')
        return perms

    def ope_display(self):
        return "{} {}".format(self.suiviaemo.ope_referent, self.suiviaemo.get_mandat_ope_display())

    def add_referent(self, utilisateur, debut=None, fin=None):
        return Intervention.objects.create(
            suivi=self.suivi,
            intervenant=utilisateur,
            date_debut=debut or date.today(),
            date_fin=fin
        )

    @property
    def prestations(self):
        return self.prestations_aemo

    @property
    def journaux(self):
        return self.journaux_aemo

    def niveau_actuel(self):
        try:
            return [
                niv.niveau for niv in sorted(
                    self.niveaux_interv.all(), key=attrgetter('date_debut')
                )
            ][-1]
        except IndexError:
            return None

    def can_edit(self, user):
        if self.suiviaemo.equipe in self.check_perm_manage_aemo(user):
            return True
        if self.suiviaemo.equipe in self.check_perm_change(user):
            referents = self.suiviaemo.suivi_referents
            if user in referents or not referents:
                return True
        return False

    def can_be_deleted(self, user):
        if user.has_perm('carrefour.export_aemo') or self.suiviaemo.equipe in self.check_perm_manage_aemo(user):
            if self.suiviaemo.motif_fin_suivi == choices.MotifsFinSuivi.ERREUR:
                return True
            if user.has_perm('carrefour.export_aemo') and delay_for_destruction(self.suiviaemo.date_fin_suivi):
                return True
        return False

    def anonymiser(self):
        super().anonymiser()
        self.suivi.anonymiser()
        self.journaux_aemo.all().delete()


class SuiviAemo(models.Model):
    """
    Dossier de suivi
    """
    AEMO_EQUIPES_CHOICES = (
        ('montagnes', 'AEMO - Montagnes'),
        ('littoral', 'AEMO - Littoral'),
    )

    WORKFLOW = OrderedDict([
        ('demande',
         Etape(1, 'demande', 'dem', 'Demande déposée', 'analyse', 5, 'demande', 'demande', True)),
        ('analyse',
         Etape(2, 'analyse', 'ana', 'Analyse de demande', 'confirmation', 5, 'demande', 'demande', False)),
        ('confirmation',
         Etape(3, 'confirmation', "conf", 'Demande confirmée', 'debut_suivi', 10, 'analyse', 'demande', True)),
        ('debut_suivi',
         Etape(4, 'debut_suivi', "dacc", 'Début du suivi', 'bilan_3mois', 90, 'confirmation', 'confirmation', True)),
        ('bilan_3mois',
         Etape(5, 'bilan_3mois', "b3m", 'Bilan 3 mois', 'bilan_annee', 270, 'debut_suivi', 'debut_suivi', False)),
        ('bilan_annee',
         Etape(6, 'bilan_annee', 'b12m', 'Bilan annuel', 'prolongation_1', 180, 'bilan_3mois', 'debut_suivi', False)),
        ('prolongation_1',
         Etape(7, "prolongation_1", 'prol1', 'Prolongation 1', 'bilan_prolongation_1', 90,
               'bilan_annee', 'debut_suivi', False)),
        ('bilan_prolongation_1',
         Etape(8, "bilan_prolongation_1", 'bpr1', 'Bilan prolongation 1', 'fin_suivi', 90,
               'prolongation_1', 'debut_suivi', False)),
        ('prolongation_2',
         Etape(9, "prolongation_2", 'prol2', 'Prolongation 2', 'bilan_final', 20,
               'bilan_prolongation_1', 'debut_suivi', False)),
        ('bilan_prolongation_2',
         Etape(10, "bilan_prolongation_2", "bpr2", "Bilan prolongation 2", "fin_suivi", 30,
               'prolongation_2', 'debut_suivi', False)),
        ('bilan_final',
         Etape(11, 'bilan_final', "b18m", 'Bilan final', 'fin_suivi', 30, 'prolongation_2', 'debut_suivi', False)),
        ('fin_suivi',
         Etape(12, 'fin_suivi', "facc", 'Fin accomp,', 'archivage', 0, 'bilan_final', 'debut_suivi', True)),
        ('archivage',
         Etape(13, 'arch_dossier', 'arch', 'Archivage du dossier', None, 0, 'fin_suivi', 'fin_suivi', False)),
    ])

    famille = models.OneToOneField(FamilleAemo, on_delete=models.CASCADE)
    difficultes = models.TextField("Difficultés", blank=True)
    aides = models.TextField("Aides souhaitées", blank=True)
    autres_contacts = models.TextField("Autres services contactés", blank=True)
    disponibilites = models.TextField('Disponibilités', blank=True)
    remarque = models.TextField(blank=True)
    remarque_privee = models.TextField('Remarque privée', blank=True)
    service_orienteur = models.CharField(
        "Orienté vers l'AEMO par", max_length=15, choices=choices.SERVICE_ORIENTEUR_CHOICES, blank=True
    )
    detail_demande = models.CharField("Détail de la demande", max_length=100, blank=True)
    motif_demande = ChoiceArrayField(
        models.CharField(max_length=60, choices=choices.MOTIF_DEMANDE_CHOICES),
        verbose_name="Motif de la demande", blank=True, null=True)

    # Référents
    equipe = models.CharField('Équipe', max_length=10, choices=AEMO_EQUIPES_CHOICES)
    aemo_referent_1 = models.ForeignKey(Utilisateur, blank=True, null=True, related_name='+',
                                        on_delete=models.SET_NULL, verbose_name='Educ. AEMO 1')
    aemo_referent_2 = models.ForeignKey(Utilisateur, blank=True, null=True, related_name='+',
                                        on_delete=models.SET_NULL, verbose_name='Educ. AEMO 2')
    aemo_referents = models.ManyToManyField(to=Utilisateur, related_name='aemo_suivis', blank=True,
                                            through='Intervention')
    ope_referent = models.ForeignKey(Contact, blank=True, null=True, related_name='suivisaemo',
                                     on_delete=models.PROTECT, verbose_name='Interv. IPE')
    mandat_ope = ChoiceArrayField(
        models.CharField(max_length=65, choices=choices.MANDATS_OPE_CHOICES, blank=True),
        verbose_name="Mandat OPE", blank=True, null=True,
    )
    referent_note = models.TextField('Autres contacts', blank=True)

    date_demande_eventuelle = models.DateField("Demande éventuelle le", blank=True, null=True, default=None)
    date_demande = models.DateField("Demande déposée le", blank=True, null=True, default=None)
    date_analyse = models.DateField("Analyse réalisée le", blank=True, null=True, default=None)
    date_confirmation = models.DateField("Demande confirmée le", blank=True, null=True, default=None)
    date_debut_suivi = models.DateField("Début du suivi le", blank=True, null=True, default=None)
    date_fin_suivi = models.DateField("Fin du suivi le", blank=True, null=True, default=None)
    date_bilan_3mois = models.DateField("Bilan 3 mois le", blank=True, null=True, default=None)
    date_bilan_annee = models.DateField("Bilan 12 mois le", blank=True, null=True, default=None)
    date_bilan_final = models.DateField("Bilan 18 mois/final le", blank=True, null=True, default=None)
    date_prolongation_1 = models.DateField("Début 1ère prolong.", blank=True, null=True, default=None)
    date_bilan_prolongation_1 = models.DateField("Bilan 1ère prolong.", blank=True, null=True, default=None)
    date_prolongation_2 = models.DateField("Début 2ème prolong.", blank=True, null=True, default=None)
    date_bilan_prolongation_2 = models.DateField("Bilan 2ème prolong.", blank=True, null=True, default=None)

    demande_prioritaire = models.BooleanField("Demande prioritaire", default=False)
    demarche = ChoiceArrayField(models.CharField(max_length=60, choices=choices.DEMARCHE_CHOICES, blank=True),
                                verbose_name="Démarche", blank=True, null=True)

    pers_famille_presentes = models.CharField('Membres famille présents', max_length=200, blank=True)
    ref_aemo_presents = models.CharField('Intervenants Aemo présents', max_length=40, blank=True)
    autres_pers_presentes = models.CharField('Autres pers. présentes', max_length=100, blank=True)
    motif_fin_suivi = models.CharField('Motif de fin de suivi', max_length=20,
                                       choices=choices.MotifsFinSuivi.choices, blank=True)

    class Meta:
        verbose_name = "Suivi AEMO"
        verbose_name_plural = "Suivis AEMO"
        permissions = (
            ('view_littoral', 'Consulter les dossiers AEMO Littoral'),
            ('view_montagnes', 'Consulter les dossiers AEMO Montagnes'),
            ('manage_littoral', 'RE AEMO Littoral'),
            ('manage_montagnes', 'RE AEMO Montagnes'),
            ('change_littoral', 'Gérer les dossiers AEMO Littoral'),
            ('change_montagnes', 'Gérer les dossiers AEMO Montagnes'),
        )

    def __str__(self):
        return 'Suivi AEMO pour la famille {} '.format(self.famille)

    @property
    def date_fin_theorique(self):
        if self.date_fin_suivi:
            return self.date_fin_suivi
        if self.date_debut_suivi is None:
            return None
        return self.date_debut_suivi + timedelta(days=548)  # env. 18 mois

    @property
    def get_ope_referents(self):
        return str(self.ope_referent) if self.ope_referent else '-'

    @property
    def suivi_referents(self):
        """Renvoie une liste des référents actuels du dossier."""
        return [
            interv.intervenant for interv in self.intervention_set.all().select_related('intervenant')
            if interv.date_fin is None
        ]

    @property
    def etape(self):
        for key, etape in reversed(self.WORKFLOW.items()):
            if key != 'archivage':
                if etape.date(self):
                    return etape

    def etape_suivante(self):
        etape = self.etape
        return self.WORKFLOW[etape.suivante] if etape else None

    def date_suivante(self):
        return self.etape.date_suivante(self)

    def get_mandat_ope_display(self):
        dic = dict(choices.MANDATS_OPE_CHOICES)
        return '; '.join([dic[value] for value in self.mandat_ope]) if self.mandat_ope else '-'

    def get_motif_demande_display(self):
        dic = dict(choices.MOTIF_DEMANDE_CHOICES)
        return '; '.join([dic[value] for value in self.motif_demande]) if self.motif_demande else '-'

    def anonymiser(self):
        new_data = {
            'famille': self.famille,
            'difficultes': '',
            'aides': '',
            'autres_contacts': '',
            'disponibilites': '',
            'remarque': '',
            'remarque_privee': '',
            'service_orienteur': self.service_orienteur,
            'detail_demande': '',
            'motif_demande': self.motif_demande,
            # Référents
            'equipe': self.equipe,
            'ope_referent': None,
            'mandat_ope': self.mandat_ope,
            'referent_note': '',
            'date_demande_eventuelle': self.date_demande_eventuelle,
            'date_demande': self.date_demande,
            'date_analyse': self.date_analyse,
            'date_confirmation': self.date_confirmation,
            'date_debut_suivi': self.date_debut_suivi,
            'date_fin_suivi': self.date_fin_suivi,
            'date_bilan_3mois': self.date_bilan_3mois,
            'date_bilan_annee': self.date_bilan_annee,
            'date_bilan_final': self.date_bilan_final,
            'date_prolongation_1': self.date_prolongation_1,
            'date_bilan_prolongation_1': self.date_bilan_prolongation_1,
            'date_prolongation_2': self.date_prolongation_2,
            'date_bilan_prolongation_2': self.date_bilan_prolongation_2,
            'demande_prioritaire': self.demande_prioritaire,
            'demarche': self.demarche,
            'pers_famille_presentes': '',
            'ref_aemo_presents': '',
            'autres_pers_presentes': '',
            'motif_fin_suivi': self.motif_fin_suivi,
        }
        [setattr(self, field, value) for field, value in new_data.items()]
        self.save()

    def can_cancel_abandon(self, user):
        motif = self.motif_fin_suivi
        user_re = user.has_perm('aemo.manage_littoral') or user.has_perm('aemo.manage_montagnes')
        date_ok = (
            not self.date_debut_suivi
            and self.date_fin_suivi
            and (date.today() - self.date_fin_suivi).days <= DELAI_ANNULATION
        )
        motif_ok = motif and (motif in ['erreur', 'non_aboutie'] or motif.startswith('abandon_'))
        return user_re and date_ok and motif_ok


class Intervention(models.Model):
    suivi = models.ForeignKey(to=SuiviAemo, on_delete=models.CASCADE)
    intervenant = models.ForeignKey(to=Utilisateur, related_name='interventions', on_delete=models.CASCADE)
    date_debut = models.DateField('Date début')
    # date_fin est utilisé pour les interventions s'arrêtant avant la fin du suivi.
    date_fin = models.DateField('Date fin', null=True, blank=True)

    def __str__(self):
        return f"{self.suivi.famille.nom}: {self.intervenant.nom_prenom}"


class PrestationAemo(PrestationBase):
    famille = models.ForeignKey(FamilleAemo, related_name='prestations_aemo', null=True, blank=True,
                                on_delete=models.SET_NULL, verbose_name="Famille")
    intervenants = models.ManyToManyField(Utilisateur, related_name='prestations_aemo')
    # Nombre de familles actives au moment de la prestation, utilisé pour ventiler les heures
    # dans les statistiques.
    familles_actives = models.PositiveSmallIntegerField(blank=True, default=0)

    class Meta(PrestationBase.Meta):
        verbose_name = "Prestation AEMO"
        verbose_name_plural = "Prestations AEMO"

    def save(self, *args, **kwargs):
        if self.famille is None:
            self.familles_actives = FamilleAemo.actives(self.date_prestation).count()
        super().save(*args, **kwargs)

    def can_edit(self, user):
        return user in self.intervenants.all() and self.check_date_editable(self.date_prestation, user)

    def edit_url(self):
        return reverse('aemo-prestation-edit', args=[self.famille.pk if self.famille else 0, self.pk])


class JournalAemo(JournalBase):
    famille = models.ForeignKey(
        FamilleAemo, related_name='journaux_aemo', on_delete=models.CASCADE, verbose_name="Famille"
    )

    class Meta(JournalBase.Meta):
        verbose_name = "Journal AEMO"
        verbose_name_plural = "Journaux AEMO"

    def __str__(self):
        return f"Journal de la famille {self.famille} - {self.date}"

    def can_edit(self, user):
        return user == self.auteur
