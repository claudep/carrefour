from datetime import date, timedelta
from io import BytesIO
from itertools import groupby

from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.core.exceptions import PermissionDenied
from django.core.paginator import EmptyPage
from django.db.models import Prefetch, Sum
from django.db.models.functions import Coalesce
from django.http import Http404, HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, reverse
from django.utils.text import slugify
from django.views.generic import (
    CreateView, ListView, UpdateView, TemplateView, View
)

from aemo.docx import DocxBilanAemo, ProjetCommunDocx
from aemo.forms import (
    DemandeAemoForm, SuiviAemoForm, PrestationAemoForm, FamilleForm,
    FamilleFilterForm, NiveauIntervForm, RegionFilterForm, AgendaAemoForm, JournalAemoForm
)
from aemo.models import FamilleAemo, JournalAemo, SuiviAemo, PrestationAemo
from aemo.pdf import AnalyseDemandePdf, ContratCollaborationPdf, CoordonneesFamillePdf

from carrefour.docx_base import docx_content_type
from carrefour.models import LibellePrestation, NiveauInterv, Role, Utilisateur
from carrefour.pdf import JournalPdf
from carrefour.utils import get_selected_date
from carrefour.views import (
    BaseAdminPrestationListView, BaseDemandeView, BasePDFView, BaseJournalDeleteView, BaseJournalUpdateView,
    BaseJournalCreateView, BaseJournalListView, DeleteView, PaginateListView, MSG_NO_ACCESS, MSG_READ_ONLY
)


class AemoEquipeRequiredMixin:
    def dispatch(self, request, *args, **kwargs):
        self.equipes = FamilleAemo.check_perm_view(request.user)
        if not self.equipes:
            raise PermissionDenied(MSG_NO_ACCESS)
        self.famille = get_object_or_404(FamilleAemo, pk=kwargs['pk']) if 'pk' in self.kwargs else None
        if self.famille and self.famille.suiviaemo.equipe not in self.equipes:
            raise PermissionDenied(MSG_NO_ACCESS)
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.famille:
            can_edit = self.famille.can_edit(self.request.user)
            if not can_edit:
                messages.info(self.request, MSG_READ_ONLY)
            context['can_edit'] = can_edit
            context['famille'] = self.famille
        else:
            context['can_edit'] = (self.request.user.has_perm('aemo.change_littoral') or
                                   self.request.user.has_perm('aemo.change_montagnes'))
        return context


class FamilleCreateView(AemoEquipeRequiredMixin, CreateView):
    template_name = 'aemo/famille_edit.html'
    model = FamilleAemo
    form_class = FamilleForm
    action = 'Nouvelle famille'

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if user.has_perm('aemo.change_littoral') or user.has_perm('aemo.change_montagnes'):
            return super().dispatch(request, *args, **kwargs)
        raise PermissionDenied(MSG_NO_ACCESS)

    def form_valid(self, form):
        famille = form.save()
        return HttpResponseRedirect(reverse('aemo-famille-edit', args=[famille.pk]))

    def get_initial(self):
        equipes = self.model.check_perm_change(self.request.user)
        if len(equipes) == 1:
            return {'equipe': list(equipes)[0]}
        else:
            return super().get_initial()


class FamilleUpdateView(AemoEquipeRequiredMixin, UpdateView):
    template_name = 'aemo/famille_edit.html'
    context_object_name = 'famille'
    model = FamilleAemo
    form_class = FamilleForm
    title = 'Modification'

    def form_valid(self, form):
        famille = form.save()
        return HttpResponseRedirect(reverse('aemo-famille-edit', args=[famille.pk]))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'enfant_suivi': Role.objects.get(nom='Enfant suivi'),
            'enfant_non_suivi': Role.objects.get(nom='Enfant non-suivi'),
        })
        return context


class FamilleListView(AemoEquipeRequiredMixin, PaginateListView):
    template_name = 'aemo/famille_list.html'
    model = FamilleAemo
    mode = 'normal'
    normal_cols = [
        ('Nom', 'nom'), ('Adresse', 'localite'), ('Réf. AEMO', 'referents'),
        ('Réf. OPE', 'referents_ope'), ('Statut', 'suivi'), ('Prior.', 'prioritaire'),
        ('Niveau', 'niveau_interv'),
    ]
    attente_cols = [
        ('Nom', 'nom'), ('Adresse', 'localite'), ('Région', 'region'),
        ('Réf. AEMO', 'referents'), ('Réf. OPE', 'referents_ope'), ('Prior.', 'prioritaire'),
        ('Demande', 'date_demande'), ('Analyse', 'date_analyse'), ('Confirmation', 'date_confirmation'),
    ]

    def get(self, request, *args, **kwargs):
        form_class = RegionFilterForm if self.mode == 'attente' else FamilleFilterForm
        self.filter_form = form_class(request.user, data=self.request.GET or None)
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        familles = super().get_queryset().filter(
            suiviaemo__isnull=False,
            suiviaemo__equipe__in=self.equipes
        ).exclude(
            suiviaemo__date_fin_suivi__isnull=False
        ).select_related('suiviaemo').prefetch_related(
            Prefetch(
                'niveaux_interv', queryset=NiveauInterv.objects.order_by('-date_debut')
            ),
        ).order_by('nom')

        if self.mode == 'attente':
            familles = familles.filter(
                suiviaemo__date_demande__lte=date.today(),
                suiviaemo__date_debut_suivi__isnull=True
            ).order_by(
                '-suiviaemo__demande_prioritaire', 'suiviaemo__date_demande', 'suiviaemo__date_analyse',
                'suiviaemo__date_confirmation'
            )

        if self.filter_form.is_bound and self.filter_form.is_valid():
            familles = self.filter_form.filter(familles)
        self.tot_familles = familles.count()
        return familles

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.mode == 'attente':
            cols = self.attente_cols
        else:
            cols = self.normal_cols
        context.update({
            'labels': [c[0] for c in cols],
            'col_keys': [c[1] for c in cols],
            'equipe_value': (
                dict(SuiviAemo.AEMO_EQUIPES_CHOICES)[self.equipes.pop()]
                if len(self.equipes) == 1 else 'AEMO'
            ),
            'form': self.filter_form,
            'tot_familles': self.tot_familles,
        })
        return context


class FamilleArchivableListe(View):
    """Return all family ids which are deletable by the current user."""

    def get(self, request, *args, **kwargs):
        data = [famille.pk for famille in FamilleAemo.objects.filter(archived_at__isnull=True)
                if famille.can_be_deleted(request.user)]
        return JsonResponse(data, safe=False)


class SuivisTerminesListView(FamilleListView):
    template_name = 'aemo/suivis_termines_list.html'

    def get(self, request, *args, **kwargs):
        try:
            return super().get(request, *args, **kwargs)
        except (Http404, EmptyPage):
            return HttpResponseRedirect(reverse('aemo-suivis-termines'))

    def get_queryset(self):
        familles = FamilleAemo.objects.filter(
            suiviaemo__date_fin_suivi__isnull=False,
            suiviaemo__equipe__in=self.equipes).exclude(archived_at__isnull=False).select_related('suiviaemo')
        if self.filter_form.is_bound and self.filter_form.is_valid():
            familles = self.filter_form.filter(familles)
        self.tot_familles = familles.count()
        return familles


class FamilleDeleteView(AemoEquipeRequiredMixin, DeleteView):
    model = FamilleAemo

    def form_valid(self, form):
        famille = self.object
        if not famille.can_be_deleted(self.request.user):
            raise PermissionDenied("Vous n’avez pas la permission de supprimer cette famille.")
        msg = "La famille %s a bien été supprimée" % str(famille)
        for doc in famille.documents.all():
            doc.delete()
        resp = super().form_valid(form)
        messages.success(self.request, msg)
        return resp

    def get_success_url(self):
        return self.request.META.get('HTTP_REFERER') or reverse('aemo-suivis-termines')


class PrestationMenu(AemoEquipeRequiredMixin, TemplateView):
    template_name = 'aemo/prestation_menu.html'
    famille_model = FamilleAemo

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        annee = date.today().year

        context.update({
            'familles': self.famille_model.actives().filter(
                suiviaemo__aemo_referents=user
            ).filter(prestations_aemo__intervenants=self.request.user).annotate(
                user_prest=Sum('prestations_aemo__duree')
            ),
            'temps_gen_user': PrestationAemo.temps_total(
                user.prestations_gen('aemo').filter(date_prestation__year=annee), tous_utilisateurs=False
            ),
            'temps_gen_aemo': PrestationAemo.temps_total_general(annee, familles=False),
            'annee': annee,
        })
        return context


class PrestationBaseView:
    template_name = 'aemo/prestation_edit.html'
    model = PrestationAemo
    form_class = PrestationAemoForm

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if not user.has_perm('aemo.change_littoral') and not user.has_perm('aemo.change_montagnes'):
            raise PermissionDenied(MSG_NO_ACCESS)
        self.famille = None
        if 'pk' in self.kwargs and self.kwargs['pk'] > 0:
            self.famille = get_object_or_404(FamilleAemo, pk=self.kwargs['pk'])
        if self.famille and not self.famille.can_edit(user):
            raise PermissionDenied(MSG_NO_ACCESS)
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'famille': self.famille
        })
        return context

    def get_success_url(self):
        if self.famille:
            return reverse('aemo-prestation-famille-list', args=[self.famille.pk])
        return reverse('aemo-prestation-gen-list')

    def get_initial(self):
        if self.famille:
            return {'lib_prestation': LibellePrestation.objects.get(code='p01').pk}
        return super().get_initial()

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['famille'] = self.famille
        kwargs['prest_codes'] = ['p01'] if self.famille else ['p02', 'p03']
        kwargs['user'] = self.request.user
        return kwargs


class PrestationListView(PrestationBaseView, TemplateView):
    template_name = 'aemo/prestation_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.famille:
            prestations = self.famille.prestations.all().order_by('-date_prestation')
            historique = self.famille.prestations_historiques()
        else:
            prestations = self.request.user.prestations_aemo.filter(famille=None)
            historique = PrestationAemo.prestations_historiques(prestations)
        grouped_by_month = {
            k: list(prests) for (k, prests) in groupby(
                prestations, lambda p: f'{p.date_prestation.year}_{p.date_prestation.month}'
            )
        }
        context['prestations'] = [
            (hist, grouped_by_month.get(f"{hist['annee']}_{hist['mois']}"))
            for hist in historique
        ]
        return context


class PrestationCreateView(PrestationBaseView, CreateView):

    def get_initial(self):
        return {**super().get_initial(), 'duree': timedelta(0)}

    def form_valid(self, form):
        if self.famille:
            form.instance.famille_id = self.kwargs['pk']
        response = super().form_valid(form)
        self.object.intervenants.add(self.request.user)
        return response


class PrestationUpdateView(PrestationBaseView, UpdateView):
    pk_url_kwarg = 'obj_pk'

    def delete_url(self):
        fam_id = self.famille.pk if self.famille else 0
        return reverse('aemo-prestation-delete', args=[fam_id, self.object.pk])


class PrestationDeleteView(PrestationBaseView, DeleteView):
    template_name = 'carrefour/object_confirm_delete.html'
    pk_url_kwarg = 'obj_pk'


class PrestationRecapPersoView(PrestationBaseView, ListView):
    template_name = 'aemo/prestation_recap_perso.html'
    model = PrestationAemo
    start_date = date(2019, 2, 1)

    def dispatch(self, request, *args, **kwargs):
        str_curdate = request.GET.get('date', None)
        self.dfrom, self.dto = get_selected_date(str_curdate)
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return self.request.user.prestations_aemo.filter(
            date_prestation__gte=self.dfrom, date_prestation__lte=self.dto
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        prev_month = self.dfrom - timedelta(days=2)
        next_month = self.dfrom + timedelta(days=33)

        context.update({
            'current_date': self.dfrom,
            'prev_month': prev_month if prev_month >= self.start_date else None,
            'next_month': next_month if next_month <= date.today() else None,
            'totaux': [(pp.code, self.get_queryset().filter(
                lib_prestation__code=f"{pp.code}"
            ).aggregate(
                tot=Sum('duree')
            )['tot']) for pp in LibellePrestation.objects.filter(unite='aemo')],
            'total_final': self.get_queryset().filter(lib_prestation__unite='aemo').aggregate(tot=Sum('duree'))[
                'tot'],
        })
        return context


class AdminPrestationListView(AemoEquipeRequiredMixin, BaseAdminPrestationListView):
    prest_model = PrestationAemo
    titre = 'Interv. Aemo'

    def get_queryset(self):
        return Utilisateur.intervenants_aemo(annee=self.annee)


class DemandeView(AemoEquipeRequiredMixin, BaseDemandeView):
    model = SuiviAemo
    form_class = DemandeAemoForm

    def get_object(self, queryset=None):
        suivi = self.prepare_form(self.famille.suiviaemo)
        return suivi


class AemoSuiviView(AemoEquipeRequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = 'aemo/suivi_edit.html'
    model = SuiviAemo
    form_class = SuiviAemoForm
    success_message = 'Les modifications ont été enregistrées avec succès'

    def get_object(self, queryset=None):
        famille = get_object_or_404(FamilleAemo, pk=self.kwargs['pk'])
        return famille.suiviaemo

    def get_success_url(self):
        return reverse('aemo-famille-suivi', args=[self.object.famille.pk])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        ope_coor = [getattr(self.object.ope_referent, el) for el in ['email', 'telephone'] if self.object.ope_referent]
        context.update({
            'famille': self.object.famille,
            'documents': self.object.famille.documents.order_by('-date_creation'),
            'ope_coordonnees': ' / '.join(ope_coor),
            'niveaux': self.object.famille.niveaux_interv.all().annotate(
                date_fin_calc=Coalesce('date_fin', self.object.date_fin_suivi)
            ).order_by('pk'),
        })
        return context


class AgendaAemoSuiviView(AemoEquipeRequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = 'aemo/agenda_suivi.html'
    model = SuiviAemo
    form_class = AgendaAemoForm
    success_message = 'Les modifications ont été enregistrées avec succès'

    def get_object(self, queryset=None):
        famille = get_object_or_404(FamilleAemo, pk=self.kwargs['pk'])
        return famille.suiviaemo

    def get_success_url(self):
        if self.object.date_fin_suivi:
            return reverse('aemo-famille-list')
        return reverse('aemo-famille-agenda', args=[self.object.famille.pk])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'mode': 'preparation' if self.object.date_debut_suivi is None else 'suivi',
            'can_cancel': self.object.can_cancel_abandon(self.request.user),
            'niveaux': self.object.famille.niveaux_interv.all().annotate(
                date_fin_calc=Coalesce('date_fin', self.object.date_fin_suivi)
            ).order_by('pk'),
        })
        return context

    def form_valid(self, form):
        instance = form.save()
        if self.request.POST.get('btn-annule-abandon') == 'on':
            instance.date_fin_suivi = None
            instance.motif_fin_suivi = ''
            instance.save()
        return HttpResponseRedirect(self.get_success_url())


class AnalyseDemandePDFView(BasePDFView):
    obj_class = FamilleAemo
    pdf_class = AnalyseDemandePdf


class ContratCollaborationPDFView(BasePDFView):
    obj_class = FamilleAemo
    pdf_class = ContratCollaborationPdf


class CoordonneesPDFView(BasePDFView):
    obj_class = FamilleAemo
    pdf_class = CoordonneesFamillePdf


class JournalPDFView(BasePDFView):
    obj_class = FamilleAemo
    pdf_class = JournalPdf


def print_docx(request, pk, docname):
    documents = {
        'bilan_3mois': {
            'file_template': '{}_bilan_3mois.docx',
            'titre': '3 mois',
            'doc_class': DocxBilanAemo
        },
        'bilan_annee': {
            'file_template': '{}_bilan_annee.docx',
            'titre': '1 an',
            'doc_class': DocxBilanAemo
        },
        'bilan_final': {
            'file_template': '{}_bilan_final.docx',
            'titre': 'Final',
            'doc_class': DocxBilanAemo
        },
        'projet_commun': {
            'file_template': '{}_projet_commun.docx',
            'titre': '',
            'doc_class': ProjetCommunDocx
        },
    }
    famille = get_object_or_404(FamilleAemo, pk=pk)
    if docname not in documents:
        raise Http404
    doc = documents[docname]
    tampon = BytesIO()
    docx = doc['doc_class'](tampon)
    docx.produce(famille, doc['titre'])

    filename = doc['file_template'].format(slugify(famille.nom))
    response = HttpResponse(tampon.getvalue(), content_type=docx_content_type)
    response['Content-Disposition'] = 'attachment; filename="{}"'.format(filename)
    return response


class JournalListView(AemoEquipeRequiredMixin, BaseJournalListView):
    template_name = 'aemo/journal_list.html'
    model = JournalAemo


class JournalCreateView(AemoEquipeRequiredMixin, BaseJournalCreateView):
    template_name = 'aemo/journal_edit.html'
    model = JournalAemo
    form_class = JournalAemoForm

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if not user.has_perm('aemo.change_littoral') and not user.has_perm('aemo.change_montagnes'):
            raise PermissionDenied(MSG_NO_ACCESS)
        self.famille = None
        if 'pk' in self.kwargs and self.kwargs['pk'] > 0:
            self.famille = get_object_or_404(FamilleAemo, pk=self.kwargs['pk'])
        if self.famille and not self.famille.can_edit(user):
            raise PermissionDenied(MSG_NO_ACCESS)
        return super().dispatch(request, *args, **kwargs)


class JournalUpdateView(AemoEquipeRequiredMixin, SuccessMessageMixin, BaseJournalUpdateView):
    template_name = 'aemo/journal_edit.html'
    model = JournalAemo
    form_class = JournalAemoForm


class JournalDeleteView(AemoEquipeRequiredMixin, BaseJournalDeleteView):
    model = JournalAemo


class NiveauCreateUpdateView(UpdateView):
    model = NiveauInterv
    form_class = NiveauIntervForm
    template_name = 'carrefour/form_in_popup.html'

    def dispatch(self, request, *args, **kwargs):
        self.is_create = 'niv_pk' not in kwargs
        self.famille = get_object_or_404(FamilleAemo, pk=kwargs['pk'])
        self.titre_page = f"Famille: {self.famille.nom}"
        self.titre_formulaire = "Nouveau niveau d'intervention" if self.is_create \
            else "Modification de l'enregistrement"
        return super().dispatch(request, *args, **kwargs)

    def get_object(self):
        return None if self.is_create else get_object_or_404(self.model, pk=self.kwargs['niv_pk'])

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['famille'] = self.famille
        return kwargs

    def form_valid(self, form):
        if self.is_create:
            der_niv = self.famille.niveaux_interv.last() if self.famille.niveaux_interv.count() > 0 else None
            if der_niv:
                der_niv.date_fin = form.cleaned_data['date_debut'] - timedelta(days=1)
                der_niv.save()

            form.instance.famille = self.famille
            form.instance.date_fin = None
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('aemo-famille-suivi', args=[self.famille.pk])

    def delete_url(self):
        return reverse('famille-niveau-delete', args=[self.famille.pk, self.object.pk])


class NiveauDeleteView(DeleteView):
    template_name = 'croixrouge/object_confirm_delete.html'
    form_class = DeleteView.form_class
    pk_url_kwarg = 'niv_pk'
    model = NiveauInterv

    def get_success_url(self):
        return reverse('aemo-famille-suivi', args=[self.kwargs['pk']])
