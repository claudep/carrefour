
import os
from datetime import date, datetime, timedelta
from pathlib import Path
from unittest.mock import patch

from freezegun import freeze_time

from django.contrib.auth.models import Group, Permission
from django.core.files import File
from django.forms.models import model_to_dict
from django.test import TestCase, tag
from django.urls import reverse
from django.utils import timezone

from aemo.forms import (
    AgendaAemoForm, FamilleForm, NiveauIntervForm, PrestationAemoForm, SuiviAemoForm
)
from aemo.models import FamilleAemo, JournalAemo, PrestationAemo, SuiviAemo, DELAI_ANNULATION
from common.choices import MotifsFinSuivi
from carrefour.docx_base import docx_content_type
from carrefour.models import (
    Contact, Document, DerogationSaisieTardive, Formation, LibellePrestation,
    NiveauInterv, Personne, Role, Utilisateur
)
from carrefour.tests import InitialDataMixin
from carrefour.utils import format_d_m_Y


class FamilleAemoTests(InitialDataMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.client.force_login(self.user_aemo)  # groupe Montagnes

    famille_data = {
        'nom': 'Dupont',
        'rue': 'Rue du Moulins',
        'npa': '3000',
        'localite': 'Paris',
        'autorite_parentale': 'conjointe',
        'equipe': 'montagnes',
    }

    def test_famille_creation(self):
        response = self.client.get(reverse('aemo-famille-add'))
        self.assertContains(response, '<option value="montagnes" selected>AEMO - Montagnes</option>')
        self.assertContains(
            response,
            '<button class="btn btn-sm btn-success" name="save" type="submit">Enregistrer</button>'
        )
        self.assertNotContains(response, 'id="id_archived"')
        response = self.client.post(reverse('aemo-famille-add'), data=self.famille_data)
        famille = FamilleAemo.objects.get(nom='Dupont')
        self.assertRedirects(response, reverse('aemo-famille-edit', args=[famille.pk]))
        famille = FamilleAemo.objects.get(nom='Dupont')
        self.assertEqual(famille.suiviaemo.equipe, 'montagnes')
        self.assertEqual(famille.suiviaemo.date_demande, date.today())

    def test_famille_edition(self):
        famille = FamilleAemo.objects.get(nom='Haddock')
        edit_url = reverse('aemo-famille-edit', args=[famille.pk])
        response = self.client.get(edit_url)
        self.assertContains(response, reverse('aemo-print-coord-famille', args=[famille.pk]))
        self.assertNotContains(response, 'id="id_archived"')
        data = {**self.famille_data, 'localite': 'Monaco'}
        response = self.client.post(edit_url, data=data)
        self.assertRedirects(response, edit_url)
        famille.refresh_from_db()
        self.assertEqual(famille.localite, 'Monaco')

    def test_famille_edition_perm(self):
        famille = FamilleAemo.objects.get(nom='Haddock')
        user_aemo1 = Utilisateur.objects.create(username='jean')
        user_aemo1.user_permissions.add(Permission.objects.get(codename='change_montagnes'))
        user_aemo2 = Utilisateur.objects.create(username='ben')
        user_aemo2.user_permissions.add(Permission.objects.get(codename='change_montagnes'))
        manager = Utilisateur.objects.create(username='boss')
        manager.user_permissions.add(Permission.objects.get(codename='manage_montagnes'))
        # No referent set, any team user can edit
        self.assertTrue(famille.can_edit(user_aemo1))
        famille.add_referent(user_aemo2)
        # Only referent (or manager) can access
        self.assertFalse(famille.can_edit(user_aemo1))
        self.assertTrue(famille.can_edit(user_aemo2))
        self.assertTrue(famille.can_edit(manager))

    def test_famille_list(self):
        fam1 = FamilleAemo.objects.get(nom='Haddock')
        # Ajout d'une intervention passée
        fam1.add_referent(
            self.user_aemo, debut=date.today() - timedelta(days=60), fin=date.today() - timedelta(days=30)
        )
        fam2 = FamilleAemo.objects.create_famille(
            nom='Dupond', rue='Château 4', npa=2299, localite='Klow',
            region='mont_cdf', equipe='montagnes',
        )
        fam2.add_referent(self.user_aemo)
        # Ne doit jamais apparaître car region='litt_ouest'
        FamilleAemo.objects.create_famille(
            nom='Tournesol', rue='Château1', npa=2000, localite='Moulinsart',
            region='litt_ouest', equipe='littoral',
        )
        response = self.client.get(reverse('aemo-famille-list'))
        self.assertEqual(len(response.context['object_list']), 2)
        self.assertContains(
            response,
            '<a href="%s" title="Suivi">Haddock</a>' % reverse('aemo-famille-suivi', args=[fam1.pk]),
            html=True
        )
        # The list should only show familles from 'montagnes'
        self.assertNotContains(response, 'Tournesol')

        # Apply filters
        response = self.client.get(
            reverse('aemo-famille-list') +
            '?region=mont_cdf&statut=demande&letter=du&interv=%s' % self.user_aemo.pk
        )
        self.assertEqual(response.context['form'].errors, {})
        self.assertEqual(len(response.context['object_list']), 1)
        # No result for that status
        response = self.client.get(reverse('aemo-famille-list') + '?statut=confirmation')
        self.assertEqual(len(response.context['object_list']), 0)
        # One family with current interv = self.user_aemo
        response = self.client.get(reverse('aemo-famille-list') + f'?interv={self.user_aemo.pk}')
        self.assertEqual(len(response.context['object_list']), 1)
        # One family without interv
        response = self.client.get(reverse('aemo-famille-list') + '?interv=0')
        self.assertEqual(len(response.context['object_list']), 1)
        # One family with status demande_eventuelle
        fam2.suiviaemo.date_demande_eventuelle = date.today()
        fam2.suiviaemo.date_demande = None
        fam2.suiviaemo.save()
        response = self.client.get(reverse('aemo-famille-list') + '?statut=demande_eventuelle')
        self.assertEqual(len(response.context['object_list']), 1)

        # One family in Liste d'attente
        fam2.suiviaemo.date_demande = date.today() - timedelta(days=2)
        fam2.suiviaemo.date_confirmation = date.today()
        fam2.suiviaemo.date_debut_suivi = None
        fam2.suiviaemo.save()
        response = self.client.get(reverse('aemo-famille-attente'))
        self.assertEqual(len(response.context['object_list']), 2)

        response = self.client.get(reverse('aemo-famille-attente') + '?region=mont_locle')
        self.assertEqual(len(response.context['object_list']), 0)

        response = self.client.get(reverse('aemo-famille-attente') + '?region=mont_cdf')
        self.assertEqual(len(response.context['object_list']), 1)
        self.assertContains(response,  format_d_m_Y(fam2.suiviaemo.date_demande))
        self.assertContains(response, format_d_m_Y(fam2.suiviaemo.date_confirmation))


    def test_delete_enfant_suivi_error(self):
        famille = FamilleAemo.objects.first()
        pers = Personne.objects.create_personne(
            famille=famille, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Enfant suivi')
        )
        with self.assertLogs('django.request', level='WARNING'):
            response = self.client.post(
                reverse('personne-delete', args=[famille.typ, famille.pk, pers.pk]), follow=True
            )
        self.assertEqual(response.status_code, 403)

    def test_suppression_famille_avec_suppression_document(self):
        famille = FamilleAemo.objects.create_famille(
            nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart',
            equipe='montagnes'
        )
        famille.suiviaemo.motif_fin_suivi = 'erreur'
        famille.suiviaemo.date_fin_suivi = date.today()
        famille.suiviaemo.save()

        self.user_aemo.user_permissions.add(Permission.objects.get(codename='manage_montagnes'))
        with open(__file__, mode='rb') as fh:
            response = self.client.post(reverse('famille-doc-upload', args=[famille.pk]), data={
                'famille': famille.pk,
                'fichier': File(fh),
                'titre': 'Titre1',
            })
        documents = famille.documents.all()
        self.assertEqual(famille.documents.count(), 1)
        doc = documents[0]

        delete_url = reverse('aemo-famille-delete', args=[famille.pk])
        response = self.client.post(delete_url, data={})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(documents.count(), 0)
        self.assertFalse(os.path.exists(doc.fichier.path))

    def test_affichage_familles_libres_pour_tous_les_educs(self):
        famille = FamilleAemo.objects.create_famille(
            nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart',
            equipe='montagnes'
        )
        self.assertFalse(famille.suiviaemo.aemo_referents.all().exists())
        response = self.client.get(reverse('aemo-famille-list'))
        self.assertContains(response, 'Simpson')

    def test_affichage_adresse(self):
        famille = FamilleAemo(nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart')
        self.assertEqual(famille.adresse, 'Château1, 2000 Moulinsart')
        famille.rue = ''
        self.assertEqual(famille.adresse, '2000 Moulinsart')
        famille.npa = ''
        self.assertEqual(famille.adresse, 'Moulinsart')

    def test_affichage_filtre_regroupement_region_pour_re(self):
        response = self.client.get(reverse('aemo-famille-list'))
        self.assertNotContains(response, '<option value="mont">AEMO-Montagnes</option>', html=True)
        self.assertNotContains(response, '<option value="litt">AEMO-Littoral</option>', html=True)

        self.user_aemo.user_permissions.add(Permission.objects.get(codename='manage_montagnes'))
        response = self.client.get(reverse('aemo-famille-list'))
        self.assertContains(response, '<option value="mont">AEMO-Montagnes</option>', html=True)
        self.assertContains(response, '<option value="litt">AEMO-Littoral</option>', html=True)

    def test_filtre_avec_regroupement_montagnes_pour_re(self):
        fam1 = FamilleAemo.objects.get(nom='Haddock')
        fam1.region = 'mont_locle'
        fam1.save()
        FamilleAemo.objects.create_famille(
            nom='Dupond', rue='Château 4', npa=2299, localite='Klow',
            region='mont_cdf', equipe='montagnes',
        )
        FamilleAemo.objects.create_famille(
            nom='Tournesol', rue='Château1', npa=2000, localite='Moulinsart',
            region='litt_ouest', equipe='littoral',
        )
        self.assertEqual(FamilleAemo.objects.all().count(), 3)
        self.user_aemo.user_permissions.add(Permission.objects.get(codename='manage_montagnes'))
        response = self.client.get(reverse('aemo-famille-list'), data={'region': 'mont'})
        self.assertContains(response, '<option value="mont" selected>AEMO-Montagnes</option>', html=True)
        self.assertContains(response, '<option value="litt">AEMO-Littoral</option>', html=True)
        self.assertEqual(response.context['object_list'].count(), 2)

    def test_filtre_avec_regroupement_littoral_pour_re(self):
        aemo_re = Utilisateur.objects.create_user(username='aemo_re', email='aemo_re@example.org')
        grp_aemo = Group.objects.get(name='aemo_littoral')
        aemo_re.user_permissions.add(Permission.objects.get(codename='view_littoral'))
        aemo_re.user_permissions.add(Permission.objects.get(codename='manage_littoral'))
        aemo_re.groups.add(grp_aemo)
        FamilleAemo.objects.create_famille(
            nom='Dupond', rue='Château 4', npa=2299, localite='Klow',
            region='litt_est', equipe='littoral',
        )
        FamilleAemo.objects.create_famille(
            nom='Tournesol', rue='Château1', npa=2000, localite='Moulinsart',
            region='litt_ouest', equipe='littoral',
        )
        self.assertEqual(FamilleAemo.objects.all().count(), 3)
        self.client.force_login(aemo_re)
        response = self.client.get(reverse('aemo-famille-list'), data={'region': 'litt'})
        self.assertContains(
            response, '<option value="mont">AEMO-Montagnes</option>', html=True)
        self.assertContains(
            response, '<option value="litt" selected>AEMO-Littoral</option>', html=True)
        self.assertEqual(response.context['object_list'].count(), 2)

    def test_famille_creation_with_aemo_ref(self):
        create_kwargs = {'nom': 'Dupont', 'equipe': 'montagnes', 'aemo_ref': self.user_aemo.pk}
        self.client.force_login(self.user_aemo)
        self.client.post(reverse('aemo-famille-add'), data=create_kwargs)
        fam = FamilleAemo.objects.get(nom="Dupont")
        self.assertEqual(fam.suiviaemo.aemo_referents.first(), self.user_aemo)

    def test_equipe_par_defaut_lors_creation_famille(self):
        user_aemo_litt = Utilisateur.objects.create_user(
            'user_aemo_litt', 'user_aemo_litt@example.org', nom='AemoLitt', prenom='Prénom', sigle='APL'
        )
        user_aemo_litt.groups.add(Group.objects.get(name='aemo_littoral'))
        user_aemo_litt.user_permissions.add(Permission.objects.get(codename='view_littoral'))
        user_aemo_litt.user_permissions.add(Permission.objects.get(codename='change_littoral'))
        self.client.force_login(user_aemo_litt)
        response = self.client.get(reverse('aemo-famille-add'))
        self.assertContains(response, '<option value="littoral" selected>AEMO - Littoral</option>')

    def test_famille_form_localite_error(self):
        form = FamilleForm(instance=None, data={'nom': 'Doe', 'localite': '2000 Neuchâtel', 'equipe': 'montagnes'})
        self.assertFalse(form.is_valid())
        self.assertIn("Le nom de la localité est incorrect.", form.errors['localite'])

    def test_famille_form_localite_sans_npa_error(self):
        form = FamilleForm(instance=None, data={'nom': 'Doe', 'localite': 'Neuchâtel', 'equipe': 'montagnes'})
        self.assertFalse(form.is_valid())
        self.assertIn("Vous devez saisir un NPA pour la localité.", form.errors['__all__'])

    def test_famille_anonymiser(self):
        famille = FamilleAemo.objects.create_famille(
            nom='Doe', rue='Rue du Lac', npa='2000', localite='Marseille', telephone='+41 79 111 22 33',
            equipe='montagnes'
        )
        Personne.objects.create_personne(
            nom='Doe', genre='M', role=Role.objects.get(nom='Père'), famille=famille
        )
        enf_suivi = Personne.objects.create_personne(
            nom='Doe', genre='M', role=Role.objects.get(nom='Enfant suivi'), famille=famille,
            date_naissance=date(2018, 1, 1)
        )

        famille.add_referent(Utilisateur.objects.get(nom='Champmard'))
        famille.suiviaemo.ope_referent = Contact.objects.get(nom='Rathfeld')
        famille.suiviaemo.date_demande = date.today() - timedelta(days=10)
        famille.suivi.date_fin_suivi = date.today()
        famille.suivi.save()
        prest_fam = LibellePrestation.objects.get(code='p01')
        prest = PrestationAemo.objects.create(
            date_prestation=date.today(), duree=timedelta(hours=8), famille=famille, lib_prestation=prest_fam
        )
        prest.intervenants.add(Utilisateur.objects.get(nom='Champmard'))

        JournalAemo.objects.create(
            date=timezone.now(), theme='Un thème', texte='Un texte', famille=famille, auteur=self.user_aemo,
        )
        with open(__file__, mode='rb') as fh:
            doc = Document.objects.create(famille=famille, fichier=File(fh, name='essai.txt'), titre='Essai')
        doc_path = doc.fichier.path

        self.assertTrue(FamilleAemo.objects.filter(pk=famille.pk).exists())
        self.assertTrue(Formation.objects.filter(personne=enf_suivi).exists())
        self.assertEqual(famille.documents.count(), 1)
        self.assertEqual(famille.membres.count(), 2)
        self.assertEqual(famille.prestations_aemo.count(), 1)
        self.assertEqual(famille.journaux_aemo.count(), 1)

        famille.anonymiser()
        famille.refresh_from_db()

        self.assertNotIn('Doe', famille.nom)
        self.assertEqual('', famille.rue)
        self.assertEqual('', famille.telephone)
        self.assertEqual('2000', famille.npa)
        self.assertEqual('Marseille', famille.localite)
        self.assertIsNone(famille.suivi.ope_referent)
        self.assertIsNotNone(famille.suivi.date_demande)
        self.assertIsNotNone(famille.suivi.date_demande)
        self.assertEqual(famille.membres.count(), 1)
        self.assertEqual(famille.documents.count(), 0)
        self.assertFalse(os.path.exists(doc_path))
        self.assertEqual(famille.journaux_aemo.count(), 0)
        self.assertTrue(Formation.objects.filter(personne=enf_suivi).exists())
        self.assertEqual(famille.prestations_aemo.count(), 1)

    def test_famille_personne_contractante(self):
        famille = FamilleAemo.objects.create_famille(
            nom='Doe', rue='Rue du Lac', npa='2000', localite='Marseille', telephone='+41 79 111 22 33',
            equipe='montagnes'
        )
        pere = Personne.objects.create_personne(
            nom='Doe', genre='M', role=Role.objects.get(nom='Père'), famille=famille
        )
        Personne.objects.create_personne(
            nom='Doe', genre='M', role=Role.objects.get(nom='Enfant suivi'), famille=famille,
            date_naissance=date(2018, 1, 1)
        )
        self.client.force_login(self.user_aemo)
        response = self.client.get(reverse('aemo-famille-edit', args=[famille.pk]))
        self.assertContains(
            response,
            f'formaction="/personne/{pere.pk}/toggle_contract/" class="btn btn-xs btn-success"',
            html=False
        )
        self.client.post(reverse('personne-toggle-contract', args=[pere.pk]))

        response = self.client.get(reverse('aemo-famille-edit', args=[famille.pk]))
        self.assertContains(
            response,
            f'formaction="/personne/{pere.pk}/toggle_contract/" class="btn btn-xs btn-danger"',
            html=False
        )


class SuiviTests(InitialDataMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.client.force_login(self.user_aemo)

    def test_suivi_dossier(self):
        famille = FamilleAemo.objects.get(nom='Haddock')
        suivi_url = reverse('aemo-famille-suivi', args=[famille.pk])
        response = self.client.get(suivi_url)
        self.assertContains(
            response,
            '<textarea name="referent_note" cols="40" rows="10" id="id_referent_note"'
            ' class="form-control"></textarea>',
            html=True
        )
        self.assertContains(
            response,
            'title="CTRL + clic pour sélectionner ou déselectionner un-e autre intervenant-e."',
            html=False
        )
        response = self.client.post(suivi_url, data={
            'equipe': 'montagnes',
            'ope_referent': Contact.objects.get(nom='Rathfeld').pk,
            'date_demande': '2018-10-24',
            'etape_suivi': 'demande',
        })
        self.assertRedirects(response, suivi_url)
        suivi = famille.suiviaemo
        self.assertEqual(suivi.equipe, 'montagnes')

    def test_effacement_famille_aemo_impossible_en_janvier(self):
        self.user_aemo.user_permissions.add(Permission.objects.get(codename='manage_montagnes'))
        # Recharger pour réinitialiser cache des permissions
        user_mont = Utilisateur.objects.get(pk=self.user_aemo.pk)
        self.client.force_login(user_mont)
        famille = FamilleAemo.objects.get(nom='Haddock')
        famille.date_fin_suivi = date(2017, 2, 1)
        famille.motif_fin_suivi = 'autres'
        famille.date_bilan_final = date(2015, 1, 1)
        famille.save()

        response = self.client.get(reverse('aemo-suivis-termines'))
        self.assertContains(response, '<th>Arch.</th>', html=True)

        with patch('aemo.models.date') as mock_date:
            mock_date.today.return_value = date(2019, 1, 15)
            response = self.client.get(reverse('aemo-suivis-termines'))
            self.assertNotIn('<th>Arch.</th>', response)

    def test_permission_effacement_definitif(self):
        """
        Les RE ne peuvent pas supprimer/archiver, seul le directeur (permission
        export_aemo) le peut.
        """
        fam_montagnes = FamilleAemo.objects.get(nom='Haddock')
        fam_montagnes.suivi.date_fin_suivi = date(2020, 12, 31)
        fam_montagnes.suivi.motif_fin_suivi = 'abandon_famille'
        fam_montagnes.suivi.save()
        fam_littoral = FamilleAemo.objects.create_famille(nom='Littoral', equipe='littoral')
        fam_littoral.suivi.date_fin_suivi = date(2020, 12, 31)
        fam_littoral.suivi.motif_fin_suivi = 'abandon_famille'
        fam_littoral.suivi.save()
        aemo_re_littoral = Utilisateur.objects.create_user(username='aemo_re', email='aemo_re@example.org')
        aemo_re_littoral.user_permissions.add(Permission.objects.get(codename='view_littoral'))
        aemo_re_littoral.user_permissions.add(Permission.objects.get(codename='manage_littoral'))
        aemo_re_littoral.groups.add(Group.objects.get(name='aemo_littoral'))
        direc = Utilisateur.objects.create_user(username='dir', email='dir@example.org')
        direc.user_permissions.add(Permission.objects.get(codename='export_aemo'))
        self.assertFalse(fam_littoral.can_be_deleted(aemo_re_littoral))
        self.assertFalse(fam_montagnes.can_be_deleted(aemo_re_littoral))
        self.assertTrue(fam_littoral.can_be_deleted(direc))
        self.assertTrue(fam_montagnes.can_be_deleted(direc))

    def test_permission_effacement_definitif_saisie_par_erreur(self):
        famille = FamilleAemo.objects.create_famille(
            nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart',
            equipe='montagnes'
        )
        famille.suiviaemo.date_fin_suivi = date.today() - timedelta(days=500)
        famille.suiviaemo.motif_fin_suivi = MotifsFinSuivi.ERREUR
        famille.suiviaemo.save()
        self.assertFalse(famille.can_be_deleted(self.user_aemo))

        self.user_aemo.user_permissions.add(Permission.objects.get(codename='manage_montagnes'))
        # Recharger pour réinitialiser cache des permissions
        user_mont = Utilisateur.objects.get(pk=self.user_aemo.pk)
        self.client.force_login(user_mont)
        self.assertTrue(famille.can_be_deleted(user_mont))

        response = self.client.post(
            reverse('aemo-famille-agenda', args=[famille.pk]),
            data={'motif_fin_suivi': 'erreur'}
        )
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        # Lorsque le dossier est fermé, la redirection se fait sur la liste des familles
        self.assertRedirects(response, reverse('aemo-famille-list'))

        famille.refresh_from_db()
        self.assertEqual(famille.suiviaemo.date_fin_suivi, date.today())
        self.assertEqual(famille.suiviaemo.motif_fin_suivi, 'erreur')
        self.assertTrue(famille.can_be_deleted(user_mont))

    def test_acces_aux_prestations_pour_RE(self):
        famille = FamilleAemo.objects.create_famille(
            nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart',
            equipe='montagnes'
        )
        Personne.objects.create_personne(
            famille=famille, role=Role.objects.get(nom='Enfant suivi'),
            nom='Simpson', prenom='Bart', genre='M', date_naissance=date(1956, 2, 16),
        )
        famille.suiviaemo.date_demande = date.today()
        famille.suiviaemo.save()
        famille.add_referent(self.user_aemo)

        self.aemo_re = Utilisateur.objects.create_user(
            'aemo_re', 'aemo_re@example.org', nom='Aemo', prenom='Re'
        )
        self.aemo_re.groups.add(Group.objects.get(name='aemo_montagnes'))
        self.aemo_re.user_permissions.add(Permission.objects.get(codename='view_montagnes'))
        self.aemo_re.user_permissions.add(Permission.objects.get(codename='change_montagnes'))
        self.aemo_re.user_permissions.add(Permission.objects.get(codename='manage_montagnes'))

        add_url = reverse('aemo-prestation-famille-add', args=[famille.pk])
        self.client.force_login(self.aemo_re)
        response = self.client.post(add_url, data={
            'date_prestation': date.today(),
            'duree': '1:00',
            'famille': famille,
            'lib_prestation': LibellePrestation.objects.get(code='p01').pk
        })
        self.assertRedirects(response, reverse('aemo-prestation-famille-list', args=[famille.pk]))

    def test_suivi_termine_affichage_pour_archivage(self):
        famille = FamilleAemo.objects.create_famille(
            nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart',
            equipe='montagnes'
        )
        famille.suiviaemo.motif_fin_suivi = 'autres'
        famille.suiviaemo.date_fin_suivi = '2019-3-27'
        famille.suiviaemo.save()
        self.user_aemo.user_permissions.add(Permission.objects.get(codename='export_aemo'))
        self.client.force_login(self.user_aemo)
        response = self.client.get(reverse('aemo-suivis-termines'))
        self.assertContains(response, 'Simpson', html=False)
        url = reverse('archive-add', args=['aemo', famille.pk])
        self.assertContains(response, f'<form method="post" action="{url}">', html=False)

    def test_suivi_termine_affichage_pour_destruction_si_motif_fin_egal_erreur(self):
        famille = FamilleAemo.objects.create_famille(
            nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart',
            equipe='montagnes'
        )
        famille.suiviaemo.motif_fin_suivi = 'erreur'
        famille.suiviaemo.date_fin_suivi = '2019-3-27'
        famille.suiviaemo.save()
        self.user_aemo.user_permissions.add(Permission.objects.get(codename='manage_montagnes'))
        self.client.force_login(self.user_aemo)
        response = self.client.get(reverse('aemo-suivis-termines'))
        self.assertContains(response, 'Simpson', html=False)
        url = reverse('aemo-famille-delete', args=[famille.pk])
        self.assertContains(response, f'<form method="post" action="{url}">', html=False)

    def test_date_fin_interventions_lors_desactivation_utilisateur(self):
        famille = FamilleAemo.objects.create_famille(
            nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart', equipe='montagnes')
        famille.suiviaemo.date_demande = '2020-01-01'
        famille.suiviaemo.date_debut_suivi = '2020-02-01'
        famille.suiviaemo.save()

        util = Utilisateur.objects.create_user(username='util1', nom='Util1', prenom='Pierre', sigle='PPP')
        util.groups.add(Group.objects.get(name='aemo_montagnes'))
        famille.add_referent(util, debut=famille.suiviaemo.date_demande, fin=None)

        self.client.force_login(self.user_admin)
        response = self.client.post(reverse('utilisateur-delete', args=[util.pk]))
        self.assertRedirects(response, reverse('utilisateur-list'))
        self.assertEqual(famille.suivi.intervention_set.count(), 1)
        self.assertEqual(famille.suivi.intervention_set.first().date_fin, date.today())
        self.assertEqual(famille.suivi.suivi_referents, [])


class InterventionTests(InitialDataMixin, TestCase):

    def test_affichage_aemo_referents(self):
        """N'affiche que les référents actuellement actifs."""
        famille = FamilleAemo.objects.create_famille(
            nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart', equipe='montagnes')
        famille.suiviaemo.date_demande = '2020-01-01'
        famille.suiviaemo.save()
        famille.add_referent(self.user_aemo, debut=famille.suiviaemo.date_demande)
        util1 = Utilisateur.objects.create_user(username='util1', nom='Util1', prenom='Pierre')
        util1.groups.add(Group.objects.get(name='aemo_montagnes'))
        famille.add_referent(util1, debut=famille.suiviaemo.date_demande, fin=date.today() - timedelta(days=10))
        self.client.force_login(self.user_aemo)
        response = self.client.get(reverse('aemo-famille-suivi', args=[famille.pk]), follow=True)
        self.assertContains(response, 'Aemo Prénom')
        self.assertNotContains(response, 'Util1 Pierre')
        self.assertIn(self.user_aemo, famille.suivi.suivi_referents)

    def test_ajouter_referent_par_formulaire(self):
        famille = FamilleAemo.objects.create_famille(
            nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart', equipe='montagnes')
        famille.suiviaemo.date_demande = '2020-01-01'
        famille.suiviaemo.save()
        util1 = Utilisateur.objects.create_user(username='util1', nom='Util1', prenom='Pierre')
        util1.groups.add(Group.objects.get(name='aemo_montagnes'))
        util2 = Utilisateur.objects.create_user(username='util2', nom='Util2', prenom='Paul')
        util2.groups.add(Group.objects.get(name='aemo_montagnes'))
        famille.add_referent(util1, debut=famille.suiviaemo.date_demande)

        form = SuiviAemoForm(instance=famille.suiviaemo, initial={}, data={
            'referents_actifs': [util1.pk, util2.pk], 'equipe': 'montagnes'}
        )
        self.assertIs(form.is_valid(), True)
        instance = form.save()
        interv = instance.intervention_set.get(intervenant=util2)
        self.assertEqual(interv.date_debut, date.today())
        self.assertEqual(interv.date_fin, None)

    def test_ajouter_referent_non_aemo_par_formulaire(self):
        famille = FamilleAemo.objects.create_famille(
            nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart', equipe='montagnes')
        famille.suiviaemo.date_demande = '2020-01-01'
        famille.suiviaemo.save()
        util1 = Utilisateur.objects.create_user(username='util1', nom='Util1', prenom='Pierre')
        util1.groups.add(Group.objects.get(name='aemo_montagnes'))
        util2 = Utilisateur.objects.create_user(username='util2', nom='Util2', prenom='Paul')
        util2.groups.add(Group.objects.get(name='asap'))
        famille.add_referent(util1, debut=famille.suiviaemo.date_demande)

        form = SuiviAemoForm(instance=famille.suiviaemo, initial={}, data={
            'referents_actifs': [util1.pk, util2.pk], 'equipe': 'montagnes'}
        )
        self.assertIs(form.is_valid(), True)
        instance = form.save()
        interv = instance.intervention_set.get(intervenant=util2)
        self.assertEqual(interv.date_debut, date.today())
        self.assertEqual(interv.date_fin, None)

    def test_retirer_referent_par_formulaire(self):
        famille = FamilleAemo.objects.create_famille(
            nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart', equipe='montagnes')
        famille.suiviaemo.date_demande = '2020-01-01'
        famille.suiviaemo.save()
        util1 = Utilisateur.objects.create_user(username='util1', nom='Util1', prenom='Pierre')
        util1.groups.add(Group.objects.get(name='aemo_montagnes'))
        famille.add_referent(util1, debut=date.today() - timedelta(days=10))
        form = SuiviAemoForm(instance=famille.suiviaemo, initial={},
                             data={'referents_actifs': [], 'equipe': 'montagnes', 'monoparentale': True})
        self.assertIs(form.is_valid(), True)
        instance = form.save()
        interv = instance.intervention_set.get(intervenant=util1)
        self.assertEqual(interv.date_debut, date.today() - timedelta(days=10))
        self.assertEqual(interv.date_fin, date.today())


class AgendaTests(InitialDataMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.client.force_login(self.user_aemo)

    def test_affichage_agenda(self):
        famille = FamilleAemo.objects.get(nom='Haddock')
        self.client.force_login(self.user_aemo)
        suivi_url = reverse('aemo-famille-agenda', args=[famille.pk])
        response = self.client.get(suivi_url)
        titres = ['PRÉPARATION', 'ACCOMPAGNEMENT', 'PROLONGATION', 'FIN']
        for titre in titres:
            self.assertContains(response, titre, html=False)

    def test_dates_obligatoires(self):
        famille = FamilleAemo.objects.get(nom="Haddock")
        famille.suiviaemo.date_debut_suivi = date.today()
        famille.suiviaemo.save()
        form_data = {
            field_name: '2019-01-15' for field_name in AgendaAemoForm._meta.fields
            if field_name != 'motif_fin_suivi'
        }
        for field, etape in SuiviAemo.WORKFLOW.items():
            if etape.oblig is True:
                etape_preced_oblig = SuiviAemo.WORKFLOW[etape.preced_oblig]
                form = AgendaAemoForm(instance=famille.suiviaemo,
                                      data={**form_data, etape_preced_oblig.date_nom(): ''})
                self.assertFalse(form.is_valid())
                self.assertEqual(
                    form.errors['__all__'],
                    ['La date «{}» est obligatoire'.format(etape_preced_oblig)]
                )
            else:
                form = AgendaAemoForm(instance=famille.suiviaemo,
                                      data={**form_data, 'motif_fin_suivi': 'placement'})
                self.assertTrue(form.is_valid())

    def test_motif_fin_suivi_sans_date(self):
        famille = FamilleAemo.objects.get(nom='Haddock')
        famille.suiviaemo.date_demande = '2019-01-01'
        famille.suiviaemo.date_confirmation = '2019-01-01'
        famille.suiviaemo.date_debut_suivi = '2019-01-01'
        famille.suiviaemo.save()
        data = {'motif_fin_suivi': 'placement'}
        form = AgendaAemoForm(data=data, instance=famille.suiviaemo)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {'date_fin_suivi': ['La date «Fin de suivi» est obligatoire']})
        # Test avec date_fin_suivi non valide
        data['date_fin_suivi'] = '2019-01-32'
        form = AgendaAemoForm(data=data, instance=famille.suiviaemo)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {
            'date_fin_suivi': ['Saisissez une date valide.', 'La date «Fin de suivi» est obligatoire'],
        })

    def test_date_fin_suivi_sans_motif(self):
        famille = FamilleAemo.objects.get(nom='Haddock')
        famille.suiviaemo.date_debut_suivi = '2019-01-01'
        famille.suiviaemo.save()
        data = {'date_demande': '2019-01-01', 'date_confirmation': '2019-01-01',
                'date_debut_suivi': '2019-01-01', 'date_fin_suivi': date.today()}
        form = AgendaAemoForm(data=data, instance=famille.suiviaemo)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {'motif_fin_suivi': ['Le champ «Motif de fin» est obligatoire.']})

    def test_dates_non_chronologiques(self):
        data = {
            field_name: '2019-01-15' for field_name in AgendaAemoForm._meta.fields
            if field_name != 'motif_fin_suivi'
        }
        date_field_preced = None
        for field_name in AgendaAemoForm._meta.fields:
            if field_name in ['date_demande_eventuelle', 'motif_fin_suivi']:
                date_field_preced = field_name
                continue
            form = AgendaAemoForm(dict(data, **{date_field_preced: '2019-01-16'}))
            self.assertFalse(form.is_valid())
            self.assertEqual(
                form.errors,
                {'__all__': ["La date «{}» ne respecte pas l’ordre chronologique!".format(
                    form.fields[field_name].label
                )]}
            )
            date_field_preced = field_name
        # Test avec trou dans les valeurs de dates
        data = {
            'date_demande': '2019-01-15',
            'date_analyse': '2019-01-01',  # Ordre chronologique non respecté
            'date_debut_suivi': '2019-02-01'
        }
        form = AgendaAemoForm(data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'__all__': ['La date «Analyse réalisée le» ne respecte pas l’ordre chronologique!']}
        )

    def test_saisie_par_erreur_OK(self):
        famille = FamilleAemo.objects.get(nom="Haddock")
        data = {'motif_fin_suivi': 'erreur'}
        form = AgendaAemoForm(instance=famille.suiviaemo, data=data)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['date_fin_suivi'], date.today())
        self.assertEqual(famille.suiviaemo.motif_fin_suivi, 'erreur')

    def test_abandon(self):
        famille = FamilleAemo.objects.get(nom="Haddock")
        data = {'motif_fin_suivi': 'abandon_aemo'}
        form = AgendaAemoForm(instance=famille.suiviaemo, data=data)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['date_fin_suivi'], date.today())
        self.assertEqual(famille.suiviaemo.motif_fin_suivi, 'abandon_aemo')

    def test_prolongation_erreur(self):
        famille = FamilleAemo.objects.get(nom="Haddock")
        famille.suiviaemo.date_debut_suivi = '2019-03-02'
        famille.suiviaemo.save()
        data = {
            'date_demande': '2019-01-01',
            'date_confirmation': '2019-02-02',
            'date_debut_suivi': '2019-03-03',
            'date_prolongation_1': '2019-04-04',
            'date_fin_suivi': '2020-01-01',
            'motif_fin_suivi': 'placement',
        }
        form = AgendaAemoForm(instance=famille.suiviaemo, data=data)
        self.assertTrue(form.is_valid())

    def test_affichage_menu_abandon(self):
        famille = FamilleAemo.objects.get(nom='Haddock')
        famille.suiviaemo.date_demande = date.today()
        famille.suiviaemo.save()
        self.client.force_login(self.user_aemo)
        suivi_url = reverse('aemo-famille-agenda', args=[famille.pk])
        response = self.client.get(suivi_url)
        self.assertEqual(response.context['mode'], 'preparation')
        self.assertContains(response,
                            '<div><em>Si abandon du dossier:</em></div>',
                            html=True)
        self.assertContains(response,
                            '<option value="erreur">Erreur de saisie</option>',
                            html=True)
        self.assertContains(response,
                            '<option value="non_aboutie">Demande non aboutie</option>',
                            html=True)
        self.assertContains(response,
                            '<option value="abandon_famille">Refus de la famille</option>',
                            html=True)
        self.assertContains(response,
                            '<option value="abandon_aemo">Refus de l’AEMO</option>',
                            html=True)
        self.assertContains(response,
                            '<option value="abandon_relais">Réorientation</option>',
                            html=True)

        famille.suiviaemo.date_debut_suivi = date.today()
        famille.suiviaemo.save()
        response = self.client.get(suivi_url)
        self.assertEqual(response.context['mode'], 'suivi')
        self.assertContains(
            response,
            '<div id="abandon" class="border p-3 d-none" style="background-color:#FFDDAA;">',
            html=False)

    def test_controle_saisies_avec_date_debut_suivi(self):
        today = date.today()
        famille = FamilleAemo.objects.get(nom='Haddock')
        Personne.objects.create_personne(nom="Haddock", prenom="Junior", famille=famille,
                                         role=Role.objects.get(nom='Enfant suivi'))
        self.client.force_login(self.user_aemo)
        edit_url = reverse('aemo-famille-agenda', args=[famille.pk])
        response = self.client.post(
            edit_url,
            data={'date_demande': today, 'date_confirmation': today, 'date_debut_suivi': today}
        )
        self.assertContains(
            response, "<strong>Famille: Le champ «Tél.» est nécessaire pour continuer.</strong>",
            html=True
        )
        self.assertContains(
            response, "<strong>Famille: Le champ «Région» est nécessaire pour continuer.</strong>",
            html=True
        )
        self.assertContains(
            response, "<strong>Suivi: Le champ «Mandat OPE» est nécessaire pour continuer.</strong>",
            html=True
        )
        self.assertContains(
            response,
            "<strong>Suivi: Le champ «Mandat OPE» est nécessaire pour continuer.</strong>",
            html=True
        )
        self.assertContains(
            response,
            "<strong>Haddock Junior: Le champ «Scolarité (cycle)» est nécessaire pour continuer.</strong>",
            html=True
        )

    def test_controle_saisies_sans_date_debut_suivi(self):
        today = date.today()
        famille = FamilleAemo.objects.get(nom='Haddock')
        self.client.force_login(self.user_aemo)
        edit_url = reverse('aemo-famille-agenda', args=[famille.pk])
        response = self.client.post(
            edit_url,
            data={'date_demande': today, 'date_confirmation': today}
        )
        self.assertRedirects(response, reverse('aemo-famille-agenda', args=[famille.pk]))

    def test_demande_non_aboutie(self):
        famille = FamilleAemo.objects.get(nom="Haddock")
        famille.suiviaemo.date_demande = date.today()
        famille.suiviaemo.save()
        data = {'motif_fin_suivi': 'non_aboutie'}
        form = AgendaAemoForm(instance=famille.suiviaemo, data=data)
        self.assertTrue(form.is_valid())
        self.assertEqual(famille.suiviaemo.date_fin_suivi,  date.today())
        self.assertEqual(famille.suiviaemo.motif_fin_suivi, 'non_aboutie')

    def test_affichage_bouton_annuler_demande_non_aboutie(self):
        famille = FamilleAemo.objects.get(nom="Haddock")
        famille.suiviaemo.date_demande = date.today()
        famille.suiviaemo.motif_fin_suivi = MotifsFinSuivi.ABANDON_NON_ABOUTIE
        famille.suiviaemo.date_fin_suivi = date.today()
        famille.suiviaemo.save()

        self.user_aemo.user_permissions.add(Permission.objects.get(codename='manage_montagnes'))
        self.client.force_login(self.user_aemo)
        response = self.client.get(reverse('aemo-famille-agenda', args=[famille.pk]))
        self.assertTrue(response.context['can_cancel'])
        self.assertContains(
            response,
            '<button type="submit" class="btn btn-sm bg-danger text-white" name="btn-annule-abandon" value="on">'
            'Annuler</button>',
            html=True
        )

    def test_bouton_annuler_demande_non_aboutie_non_affiche(self):
        famille = FamilleAemo.objects.get(nom="Haddock")
        famille.suiviaemo.date_demande = date.today()
        famille.suiviaemo.motif_fin_suivi = MotifsFinSuivi.ABANDON_NON_ABOUTIE
        famille.suiviaemo.date_fin_suivi = date.today()
        famille.suiviaemo.save()

        # user without permission
        self.client.force_login(self.user_aemo)
        response = self.client.get(reverse('aemo-famille-agenda', args=[famille.pk]))
        self.assertFalse(response.context['can_cancel'])
        self.assertNotContains(
            response,
            '<button type="submit" class="btn btn-sm bg-danger text-white" name="btn-annule-abandon" value="on">'
            'Annuler</button>',
            html=True
        )

        # timeout
        self.user_aemo.user_permissions.add(Permission.objects.get(codename='manage_montagnes'))
        self.client.force_login(self.user_aemo)
        famille.suiviaemo.date_fin_suivi = date.today() - timedelta(days=DELAI_ANNULATION + 1)
        famille.suiviaemo.save()

        response = self.client.get(reverse('aemo-famille-agenda', args=[famille.pk]))
        self.assertFalse(response.context['can_cancel'])
        self.assertNotContains(
            response,
            '<button type="submit" class="btn btn-sm bg-danger text-white" name="btn-annule-abandon" value="on">'
            'Annuler</button>',
            html=True
        )

        # Other motif_fin_suivi
        famille.suiviaemo.date_fin_suivi = date.today()
        famille.suiviaemo.motif_fin_suivi = MotifsFinSuivi.ABANDON_FAMILLE
        famille.suiviaemo.save()

        response = self.client.get(reverse('aemo-famille-agenda', args=[famille.pk]))
        self.assertTrue(response.context['can_cancel'])
        self.assertContains(
            response,
            '<button type="submit" class="btn btn-sm bg-danger text-white" name="btn-annule-abandon" value="on">'
            'Annuler</button>',
            html=True
        )

    def test_modifier_motif_abandon_impossible(self):
        famille = FamilleAemo.objects.get(nom="Haddock")
        famille.suiviaemo.date_demande = date.today()
        famille.suiviaemo.motif_fin_suivi = MotifsFinSuivi.ABANDON_NON_ABOUTIE
        famille.suiviaemo.date_fin_suivi = date.today()
        famille.suiviaemo.save()

        self.client.force_login(self.user_aemo)
        response = self.client.post(
            reverse('aemo-famille-agenda', args=[famille.pk]),
            data={'date_fin_suivi': date.today(), 'motif_fin_suivi': MotifsFinSuivi.ABANDON_FAMILLE}
        )
        self.assertContains(
            response,
            '<strong>Vous ne pouvez plus modifier le dossier.</strong>',
            html=True
        )

    def test_saisie_date_sans_ipe(self):
        famille = FamilleAemo.objects.get(nom="Haddock")
        famille.telephone = '032 123 45 67'
        famille.region = 'litt-est'
        famille.save()
        famille.suiviaemo.mandat_ope = ['volontaire']
        famille.suiviaemo.demarche = ['volontaire']
        famille.suiviaemo.motif_demande = ['éducation']
        famille.suiviaemo.save()
        today = date.today()
        data = {
            'date_demande': today, 'date_analyse': today, 'date_confirmation': today, 'date_debut_suivi': today
        }
        form = AgendaAemoForm(instance=famille.suiviaemo, data=data)
        self.assertTrue(form.is_valid())


@tag('pdf')
class PdfTests(InitialDataMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.client.force_login(self.user_aemo)

    def _test_print_pdf(self, url, filename):
        self.client.force_login(self.user_aemo)
        response = self.client.get(url)
        self.assertEqual(
            response['content-disposition'],
            'attachment; filename="{}"'.format(filename)
        )
        self.assertEqual(response['content-type'], 'application/pdf')
        self.assertGreater(len(response.getvalue()), 200)
        return response

    def test_print_analyse_demande(self):
        fam = FamilleAemo.objects.get(nom='Haddock')
        self._test_print_pdf(
            reverse('aemo-print-analyse-demande', args=(fam.pk,)),
            'haddock_analyse_demande.pdf'
        )
        # Avec genogramme en PDF
        with (Path('.').parent / 'carrefour' / 'test.pdf').open(mode='rb') as fh:
            fam.genogramme = File(fh)
            fam.save()
        self._test_print_pdf(
            reverse('aemo-print-analyse-demande', args=(fam.pk,)),
            'haddock_analyse_demande.pdf'
        )

    def test_print_contrat_collaboration(self):
        fam_min = FamilleAemo.objects.create_famille(
            nom='Dupond', rue='Château 4', npa=2299, localite='Klow',
            region='mont_cdf', equipe='montagnes',
        )
        self._test_print_pdf(
            reverse('aemo-print-contrat-collaboration', args=(fam_min.pk,)),
            'dupond_contrat_collaboration.pdf'
        )

        fam = FamilleAemo.objects.get(nom='Haddock')
        self.assertIsNone(fam.suiviaemo.ope_referent)
        self.assertFalse(fam.suiviaemo.aemo_referents.all().exists())
        self._test_print_pdf(
            reverse('aemo-print-contrat-collaboration', args=(fam.pk,)),
            'haddock_contrat_collaboration.pdf'
        )
        fam.suiviaemo.ope_referent = Contact.objects.get(nom='Rathfeld')
        fam.suiviaemo.save()
        fam.add_referent(Utilisateur.objects.first())
        self._test_print_pdf(
            reverse('aemo-print-contrat-collaboration', args=(fam.pk,)),
            'haddock_contrat_collaboration.pdf'
        )

    def test_contrat_collaboration_avec_tutelle(self):
        fam = FamilleAemo.objects.create_famille(
            nom='Dupond', rue='Château 4', npa=2299, localite='Klow',
            region='mont_cdf', equipe='montagnes',
        )
        ope = Utilisateur.objects.create_user('DoeJ', 'john.doe@exemple.org', 'secret', prenom='John', nom='Doe')
        fam.suiviaemo.ope_referent = ope
        fam.suiviaemo.mandat_ope = ['tutelle']
        fam.suiviaemo.save()

        response = self.client.get(reverse('aemo-print-contrat-collaboration', args=[fam.pk]))
        refs = 'Le tuteur / la tutrice'
        pdf = self.extract_page_pdf(response, -1)
        self.assertIn(refs, pdf)
        self.assertIn('Doe John', pdf)

    def test_referents_names_in_contrat_de_collaboration(self):
        ref1 = Utilisateur.objects.create_user('CookJ', 'joe.cook@example.org', 'pwd', prenom='Joe', nom='Cook')
        ref2 = Utilisateur.objects.create_user('DoeJ', 'john.doe@exemple.org', 'secret', prenom='John', nom='Doe')
        famille = FamilleAemo.objects.create_famille(
            nom='Dupond', rue='Château 4', npa=2299, localite='Klow', region='mont_cdf', equipe='montagnes',
        )
        famille.add_referent(ref1)
        famille.add_referent(ref2)
        self.client.force_login(self.user_aemo)
        response = self.client.get(reverse('aemo-print-contrat-collaboration', args=[famille.pk]))
        pdf = self.extract_page_pdf(response, -1)
        refs = 'Cook Joe, Doe John'
        self.assertIn(refs, pdf)

    def test_print_coord_famille(self):
        fam = FamilleAemo.objects.get(nom='Haddock')
        Personne.objects.create_personne(
            famille=fam, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Enfant suivi')
        )
        self._test_print_pdf(
            reverse('aemo-print-coord-famille', args=(fam.pk,)),
            'haddock_coordonnees.pdf'
        )

    def test_print_coord_famille_error_si_date_demande_is_none(self):
        fam = FamilleAemo.objects.get(nom='Haddock')
        Personne.objects.create_personne(
            famille=fam, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Enfant suivi')
        )
        fam.suiviaemo.date_demande = None
        fam.suiviaemo.save()
        self.client.force_login(self.user_aemo)
        response = self.client.get(reverse('aemo-print-coord-famille', args=(fam.pk,)))
        page = self.extract_page_pdf(response, 0)
        self.assertIn('Chronologie', page)


class DocxTests(InitialDataMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.client.force_login(self.user_aemo)

    def test_produce_bilan3mois(self):
        fam = FamilleAemo.objects.get(nom='Haddock')
        url = reverse('print-bilan-3mois', args=(fam.pk,))
        response = self.client.get(url)
        self.assertEqual(
            response['content-disposition'],
            'attachment; filename="haddock_bilan_3mois.docx"'
        )
        self.assertEqual(response['content-type'], docx_content_type)

    def test_produce_projet_commun(self):
        fam = FamilleAemo.objects.get(nom='Haddock')
        url = reverse('print-projet-commun', args=(fam.pk,))
        response = self.client.get(url)
        self.assertEqual(
            response['content-disposition'],
            'attachment; filename="haddock_projet_commun.docx"'
        )
        self.assertEqual(response['content-type'], docx_content_type)


class JournalTests(InitialDataMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.client.force_login(self.user_aemo)

    def test_createview(self):
        famille = FamilleAemo.objects.get(nom='Haddock')
        self.client.force_login(self.user_aemo)
        response = self.client.get(reverse('aemo-journal-add', args=(famille.pk,)))
        self.assertContains(response, date.today().strftime('%Y-%m-%d'))

    def test_add_page_with_form(self):
        famille = FamilleAemo.objects.get(nom='Haddock')
        add_url = reverse('aemo-journal-add', args=(famille.pk,))
        journal_data = {
            'date_0': '23.4.2023',
            'date_1': '20:30',
            'theme': 'Un autre thème',
            'texte': 'Un autre texte',
        }

        self.client.force_login(self.user_externe)
        with self.assertLogs('django.request', level='WARNING'):
            response = self.client.post(add_url, data=journal_data)
        self.assertEqual(response.status_code, 403)

        self.client.force_login(self.user_aemo)
        response = self.client.post(add_url, data=journal_data)
        page = famille.journaux.first()
        self.assertEqual(page.theme, 'Un autre thème')
        self.assertEqual(
            timezone.localtime(page.date), timezone.make_aware(datetime(2023, 4, 23, 20, 30))
        )

    def test_update_page(self):
        fam = FamilleAemo.objects.get(nom='Haddock')
        page = JournalAemo.objects.create(
            date=timezone.now(),
            theme='Un thème',
            texte='Un texte',
            famille=fam,
            auteur=self.user_aemo,
        )
        edit_url = reverse('aemo-journal-edit', args=(fam.pk, page.pk))

        self.client.force_login(self.user_externe)
        with self.assertLogs('django.request', level='WARNING'):
            response = self.client.get(edit_url)
        self.assertEqual(response.status_code, 403)

        self.client.force_login(self.user_aemo)
        response = self.client.get(edit_url)
        self.assertEqual(response.status_code, 200)
        response = self.client.post(edit_url, data={
            'date_0': '28.4.2019',
            'date_1': '16:30',
            'theme': 'Un troisième thème',
            'texte': 'Un troisième texte',
        })
        self.assertRedirects(response, reverse('aemo-journal-list', args=[fam.pk]))
        page = JournalAemo.objects.get(theme='Un troisième thème')
        self.assertEqual(page.texte, 'Un troisième texte')

    def test_delete_page(self):
        fam = FamilleAemo.objects.get(nom='Haddock')
        page = JournalAemo.objects.create(
            date=timezone.now(),
            theme='Un thème',
            texte='Un texte',
            famille=fam,
            auteur=self.user_aemo,
        )
        self.assertEqual(JournalAemo.objects.all().count(), 1)
        delete_url = reverse('aemo-journal-delete', args=(fam.pk, page.pk))
        self.client.force_login(self.user_aemo)
        response = self.client.post(delete_url)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(JournalAemo.objects.all().count(), 0)

    def test_droits_en_ecriture_pour_RE(self):
        self.client.force_login(self.user_externe)
        famille = FamilleAemo.objects.get(nom='Haddock')
        add_url = reverse('aemo-journal-add', args=(famille.pk,))
        journal_data = {
            'date_0': '22.2.2019',
            'date_1': '22:22',
            'theme': 'Nouveau thème',
            'texte': 'Nouveau texte',
        }
        with self.assertLogs('django.request', level='WARNING'):
            response = self.client.post(add_url, data=journal_data)
        self.assertEqual(response.status_code, 403)

        self.user_externe.user_permissions.add(Permission.objects.get(codename='view_montagnes'))
        self.user_externe.user_permissions.add(Permission.objects.get(codename='change_montagnes'))
        self.user_externe.user_permissions.add(Permission.objects.get(codename='manage_montagnes'))
        response = self.client.post(add_url, data=journal_data)
        page = famille.journaux.first()
        self.assertEqual(page.theme, 'Nouveau thème')
        self.assertEqual(page.auteur.last_name, 'Batmann')


class PrestationTest(InitialDataMixin, TestCase):

    def setUp(self):
        super().setUp()
        self.client.force_login(self.user_aemo)

        self.famille = FamilleAemo.objects.get(nom='Haddock')
        Personne.objects.create_personne(
            famille=self.famille, role=Role.objects.get(nom='Enfant suivi'),
            nom='Haddock', prenom='Paulet', genre='M', date_naissance=date(1956, 2, 16),
            rue='Château1', npa=2000, localite='Moulinsart',
        )
        self.famille.add_referent(self.user_aemo)
        self.prest_fam = LibellePrestation.objects.get(code='p01')
        self.prest_gen = LibellePrestation.objects.get(code='p02')

    def test_check_date_editable(self):
        with freeze_time("2021-01-04"):
            self.assertFalse(PrestationAemo.check_date_editable(date(2020, 1, 10), self.user_aemo))
            self.assertFalse(PrestationAemo.check_date_editable(date(2020, 11, 30), self.user_aemo))
            self.assertTrue(PrestationAemo.check_date_editable(date(2020, 12, 2), self.user_aemo))
            self.assertTrue(PrestationAemo.check_date_editable(date(2021, 1, 2), self.user_aemo))

    def create_prestation_fam(self):
        data = dict(
            date_prestation=date.today(), duree=timedelta(hours=8), famille=self.famille, lib_prestation=self.prest_fam
        )
        prest = PrestationAemo.objects.create(**data)
        prest.intervenants.add(self.user_aemo)
        return prest

    def create_prestation_gen(self):
        data = dict(
            date_prestation=date.today(), duree=timedelta(hours=8), famille=None, lib_prestation=self.prest_gen
        )
        prest = PrestationAemo.objects.create(**data)
        prest.intervenants.add(self.user_aemo)
        return prest

    def test_prestation_date_prestation_dans_mois_courant(self):
        data = dict(duree='3:40', date_prestation=date.today(), lib_prestation=self.prest_fam.pk)
        form = PrestationAemoForm(famille=self.famille, prest_codes=['p01'], user=self.user_aemo, data=data)
        self.assertTrue(form.is_valid(), msg=form.errors)

    def test_prestation_saisie_anticipee(self):
        data = dict(duree='3:40', date_prestation=date.today() + timedelta(days=1), lib_prestation=self.prest_fam.pk)
        form = PrestationAemoForm(famille=self.famille, prest_codes=['p01'], data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {'date_prestation': ['La saisie anticipée est impossible !']})

    def test_prestation_saisie_tardive_pour_educs(self):
        data = dict(duree='3:40', date_prestation=date.today() - timedelta(days=31 + PrestationAemo.max_jour),
                    lib_prestation=self.prest_fam.pk)
        form = PrestationAemoForm(famille=self.famille, prest_codes=['p01'], user=self.user_aemo, data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'date_prestation': ['La saisie des prestations des mois précédents est close !']}
        )

    def test_derogation_saisie_tardive(self):
        today = date.today()
        demain = today + timedelta(days=1)
        data = dict(
            duree='3:40', date_prestation=date.today() - timedelta(days=31 + PrestationAemo.max_jour),
            lib_prestation=self.prest_fam.pk
        )
        with freeze_time(demain):
            form = PrestationAemoForm(famille=self.famille, prest_codes=['p01'], user=self.user_aemo, data=data)
            self.assertFalse(form.is_valid())
            DerogationSaisieTardive.objects.create(
                educ=self.user_aemo, duree=[demain, demain + timedelta(days=1)]
            )
            form = PrestationAemoForm(famille=self.famille, prest_codes=['p01'], user=self.user_aemo, data=data)
            self.assertTrue(form.is_valid())

    def test_delai_saisie_prestation_pour_educs(self):
        mock_date = date(2023, 2, PrestationAemo.max_jour)
        with freeze_time(mock_date):
            data = dict(duree='3:40', date_prestation="2023-01-31", lib_prestation=self.prest_fam.pk)
            form = PrestationAemoForm(famille=self.famille, prest_codes=['p01'], user=self.user_aemo, data=data)
            self.assertTrue(form.is_valid())
        mock_date = date(2023, 2, PrestationAemo.max_jour + 1)
        with freeze_time(mock_date):
            data = dict(duree='3:40', date_prestation="2023-01-31", lib_prestation=self.prest_fam.pk)
            form = PrestationAemoForm(famille=self.famille, prest_codes=['p01'], user=self.user_aemo, data=data)
            self.assertFalse(form.is_valid())

    def test_delai_saisie_prestation_pour_re(self):
        user_aemo_re = Utilisateur.objects.create_user(
            'user_aemo_re', 'user_aemo_re@example.org', nom='AemoRe', prenom='PrénomRe', sigle='AP'
        )
        user_aemo_re.user_permissions.add(Permission.objects.get(codename='manage_montagnes'))
        mock_date = date(2023, 2, PrestationAemo.max_jour_re)
        with freeze_time(mock_date):
            data = dict(duree='3:40', date_prestation="2023-01-31", lib_prestation=self.prest_fam.pk)
            form = PrestationAemoForm(famille=self.famille, prest_codes=['p01'], user=user_aemo_re, data=data)
            self.assertTrue(form.is_valid())

        mock_date = date(2023, 2, PrestationAemo.max_jour_re + 1)
        with freeze_time(mock_date):
            data = dict(duree='3:40', date_prestation="2023-01-31", lib_prestation=self.prest_fam.pk)
            form = PrestationAemoForm(famille=self.famille, prest_codes=['p01'], user=user_aemo_re, data=data)
            self.assertFalse(form.is_valid())

    def test_add_prestation_fam(self):
        add_url = reverse('aemo-prestation-famille-add', args=[self.famille.pk])
        data = dict(
            duree='3:40', date_prestation=date.today(), famille=self.famille.pk, lib_prestation=self.prest_fam.pk
        )
        response = self.client.post(add_url, data)
        self.assertRedirects(response, reverse('aemo-prestation-famille-list', args=[self.famille.pk]))

    def test_affichage_prestation_menu_fam(self):
        self.create_prestation_fam()
        menu_url = reverse('aemo-prestation-menu')
        response = self.client.get(menu_url)
        self.assertContains(
            response,
            ('<tr><td>Haddock - Château1, 2000 Moulinsart</td><td>08:00</td><td>08:00</td>' +
             '<td align="center"><a href="{}"><img src="/static/admin/img/search.svg"></a> &nbsp;' +
             '<a href="{}"><img src="/static/admin/img/icon-addlink.svg"></a></td></tr>').format(
                 reverse('aemo-prestation-famille-list', args=[self.famille.pk]),
                 reverse('aemo-prestation-famille-add', args=[self.famille.pk])
            ),
            html=True
        )
        self.client.force_login(self.user_externe)
        response = self.client.get(reverse('aemo-prestation-menu'))
        self.assertEqual(response.status_code, 403)

    def test_add_prestation_gen(self):
        add_url = reverse('aemo-prestation-gen-add')
        data = dict(duree='3:40', date_prestation=date.today(), famille='', lib_prestation=self.prest_gen.pk)
        self.client.force_login(self.user_aemo)
        response = self.client.post(add_url, data)
        self.assertRedirects(response, reverse('aemo-prestation-gen-list'))

    def test_affichage_prestation_menu_gen(self):
        menu_url = reverse('aemo-prestation-menu')
        prest = self.create_prestation_gen()
        response = self.client.get(menu_url)
        self.assertContains(
            response,
            '<li class="nav-item"><a class="nav-link" href="{}">Prestations générales</a></li>'.format(
                 reverse('aemo-prestation-gen-list'),
            ),
            html=True
        )

        # Affichage dans prestation-gen-list
        list_url = reverse('aemo-prestation-gen-list')
        response = self.client.get(list_url)
        self.assertContains(
            response,
            ('<tr class="am{}_{}" style="display:none"><td>{}</td><td>08:00</td><td>AP</td><td>Prestation 2</td>' +
             '<td align="center"><a href="{}"><img src="/static/admin/img/icon-changelink.svg"></a></td></tr>').format(
                date.today().year, date.today().month, format_d_m_Y(date.today()),
                reverse('aemo-prestation-edit', args=[0, prest.pk]),
            ),
            html=True
        )

    def test_update_prestation_fam(self):
        prest = self.create_prestation_fam()
        url = reverse('aemo-prestation-edit', args=[self.famille.pk, prest.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        data = model_to_dict(prest)
        data['duree'] = '12:00'
        response = self.client.post(url, data=data)
        prest.refresh_from_db()
        self.assertEqual(prest.duree, timedelta(hours=12))

    def test_affichage_adminlist(self):
        self.create_prestation_fam()
        self.create_prestation_gen()
        response = self.client.get(reverse('aemo-prestation-adminlist'))
        empty_td = '<td class="m-0 p-1 text-center"></td>'
        m1 = date.today().month - 1
        m2 = 12 - date.today().month
        self.assertContains(
            response,
            ('<tr><td class="m-0 p-1">Aemo P.</td>' + (empty_td * m1) +
             '<td class="m-0 p-1 text-center">16:00</td>' + (empty_td * m2) +
             '<td class="m-0 p-1 text-center fw-bold">16:00</td>'
             '</tr>'),
            html=True
        )
        # Just check total
        self.assertContains(response, '<th class="text-center">16:00</th>')

    def test_tableau_prestation_aemo(self):
        self.client.force_login(self.user_aemo)

        with patch('carrefour.views.date') as mock_date:
            mock_date.today.return_value = date(2019, 7, 15)
            response = self.client.get(reverse('aemo-prestation-adminlist'))
            self.assertEqual(response.context['annee'], 2019)

            mock_date.today.return_value = date(2020, 1, 15)
            response = self.client.get(reverse('aemo-prestation-adminlist'))
            self.assertEqual(response.context['annee'], 2020)

            mock_date.today.return_value = date(2020, 3, 1)
            response = self.client.get(reverse('aemo-prestation-adminlist'))
            self.assertEqual(response.context['annee'], 2020)

        self.client.force_login(self.user_externe)
        response = self.client.get(reverse('aemo-prestation-adminlist'))
        self.assertEqual(response.status_code, 403)


class NiveauxIntervTests(InitialDataMixin, TestCase):

    def setUp(self):
        self.famille = FamilleAemo.objects.get(nom='Haddock')
        self.client.force_login(self.user_aemo)

    def test_niveau_model(self):
        niv = NiveauInterv.objects.create(
            famille=self.famille,
            niveau=2,
            date_debut=date.today()
        )
        self.assertEqual(niv.famille, self.famille)
        self.assertEqual(niv.niveau, 2)
        self.assertEqual(niv.date_debut, date.today())
        self.assertIsNone(niv.date_fin)

    def test_niveau_add_view(self):
        auj = date.today()
        demain = auj + timedelta(days=1)
        response = self.client.post(
            path=reverse('famille-niveau-add', args=[self.famille.pk]),
            data={'niveau': 3, 'date_debut': demain},
            follow=True
        )
        self.assertContains(response, f"<td>{format_d_m_Y(demain)}</td><td>---</td><td>3</td>", html=True)

    def test_niveau_add_form(self):
        auj = date.today()
        demain = auj + timedelta(days=1)
        form = NiveauIntervForm(famille=self.famille, data={'niveau': 3, 'date_debut': demain})
        self.assertTrue(form.is_valid())

    def test_niveau_add_second_enregistrement(self):
        auj = date.today()
        demain = auj + timedelta(days=1)
        niv = NiveauInterv.objects.create(
            famille=self.famille, niveau=2, date_debut=auj, date_fin=None
        )
        self.client.post(
            path=reverse('famille-niveau-add', args=[self.famille.pk]),
            data={'niveau': 3, 'date_debut': demain},
            follow=True
        )
        # Mise à jour dernier enreg.
        niv.refresh_from_db()
        self.assertEqual(niv.date_fin, auj)

        # Test nouvel enreg.
        self.assertEqual(self.famille.niveaux_interv.count(), 2)
        der_niv = self.famille.niveaux_interv.last()
        self.assertEqual(der_niv.famille, self.famille)
        self.assertEqual(der_niv.niveau, 3)
        self.assertEqual(der_niv.date_debut, demain)
        self.assertIsNone(der_niv.date_fin)

    def test_niveau_edit_view(self):
        auj = date.today()
        demain = auj + timedelta(days=1)
        niv = NiveauInterv.objects.create(famille=self.famille, niveau=2, date_debut=auj, date_fin=None)
        self.client.post(
            path=reverse('famille-niveau-edit', args=[self.famille.pk, niv.pk]),
            data={'niveau': 3, 'date_debut': demain},
            follow=True
        )
        niv.refresh_from_db()
        self.assertEqual(niv.niveau, 3)

    def test_niveau_edit_form(self):
        auj = date.today()
        demain = auj + timedelta(days=1)
        niv = NiveauInterv.objects.create(famille=self.famille, niveau=2, date_debut=auj, date_fin=None)
        form = NiveauIntervForm(famille=self.famille, instance=niv, data={'niveau': 3, 'date_debut': demain})
        self.assertTrue(form.is_valid())
        form.save()
        niv.refresh_from_db()
        self.assertEqual(niv.niveau, 3)

    def test_niveau_delete(self):
        auj = date.today()
        niv = NiveauInterv.objects.create(famille=self.famille, niveau=2, date_debut=auj, date_fin=None)
        response = self.client.post(
            path=reverse('famille-niveau-delete', args=[self.famille.pk, niv.pk]),
            follow=True
        )
        self.assertEqual(len(response.context['niveaux']), 0)

    def test_niveau_affichage_dans_agenda(self):
        auj = date.today()
        NiveauInterv.objects.create(
            famille=self.famille, niveau=2, date_debut=auj - timedelta(days=10), date_fin=None
        )
        agenda_url = reverse('aemo-famille-agenda', args=[self.famille.pk])
        response = self.client.get(agenda_url)
        self.assertContains(response, '<span class="fw-bold">Niv. d’intervention 2</span>', html=True)
        # Fin niveau affiché au plus tard à fin du suivi.
        self.famille.suivi.date_fin_suivi = auj
        self.famille.suivi.save()
        response = self.client.get(agenda_url)
        self.assertEqual(response.context['niveaux'][0].date_fin_calc, auj)
