
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT

from carrefour.docx_base import DocxBase
from carrefour.utils import format_d_m_Y


class DocxBilanAemo(DocxBase):
    def produce(self, famille, title):
        p = self.document.add_paragraph('BILAN AEMO - {}'.format(title), 'Titre1')
        p.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER

        p = self.document.add_paragraph('Coordonnées de la famille: \t\t\t\t\t', 'SousTitre')
        if title == '3 mois':
            date_doc = famille.suiviaemo.date_bilan_3mois
        elif title == '1 an':
            date_doc = famille.suiviaemo.date_bilan_annee
        elif title == 'Final':
            date_doc = famille.suiviaemo.date_bilan_final

        p.add_run('Date: {}'.format(self.date_formated(date_doc))).bold = True
        self.document.add_paragraph('   {} - {}'.format(famille.nom, famille.adresse))
        self.document.add_paragraph('Enfants suivis:')
        for enfant in famille.membres_suivis():
            self.document.add_paragraph('  - {} - {}'.format(enfant.nom_prenom, format_d_m_Y(enfant.date_naissance)))
        self.document.add_paragraph('')

        self.document.add_paragraph('Personnes invitées:')
        self.document.add_paragraph('')
        self.document.add_paragraph('Présentation de la famille et du réseau:')
        self.document.add_paragraph('')
        self.document.add_paragraph('Origine de la demande d’accompagnement :', 'SousTitre')
        p = self.document.add_paragraph('Orienteur: ')
        p.add_run(famille.suiviaemo.get_service_orienteur_display())
        self.document.add_paragraph('')
        self.document.add_paragraph('Raisons: ')
        self.document.add_paragraph("- selon l’orienteur: ")
        self.document.add_paragraph("- selon les parents: ")
        self.document.add_paragraph("- selon le jeune: ")
        self.document.add_paragraph('')
        self.document.add_paragraph('Attentes: ')
        self.document.add_paragraph("- selon l’orienteur: ")
        self.document.add_paragraph("- selon les parents: ")
        self.document.add_paragraph("- selon le jeune: ")
        self.document.add_paragraph('')

        self.document.add_paragraph('Travail AEMO: ', 'SousTitre')
        date_debut = famille.suiviaemo.date_debut_suivi
        p = self.document.add_paragraph("Date du début de l’accompagnement: {} \t\t".format(self.date_formated(
            date_debut)))
        date_terme = famille.suiviaemo.date_bilan_3mois
        if title in ['1 an', 'Final']:
            date_terme = famille.suiviaemo.date_bilan_annee
            date_3mois = self.date_formated(famille.suiviaemo.date_bilan_3mois)
            p.add_run('Bilan des 3 mois: {}'.format(date_3mois))
        if title == 'Final':
            date_terme = famille.suiviaemo.date_bilan_final
            date_annee = famille.suiviaemo.date_bilan_annee
            self.document.add_paragraph('Date du bilan des 12 mois: {}'.format(self.date_formated(date_annee)))
        if date_terme and date_debut:
            mois_suivis = (date_terme.year - date_debut.year) * 12 + (date_terme.month - date_debut.month)
        else:
            mois_suivis = ''

        self.document.add_paragraph(
            'Nombre de mois suivis: {} \t\t\t\t\tNombre de rencontres: '.format(mois_suivis),
        )

        self.document.add_paragraph('Type de rencontre (famille / parents / jeune) et participation: ')
        self.document.add_paragraph('')

        if title == '1 an':
            self.document.add_paragraph('Projet commun au bilan des 3 mois: ')
            self.document.add_paragraph('')
            self.document.add_paragraph('Évolution du projet commun depuis: ')
            self.document.add_paragraph('')
        if title == 'Final':
            self.document.add_paragraph('Projet commun au bilan de 1 an: ')
            self.document.add_paragraph('')
            self.document.add_paragraph('Évolution du projet commun depuis: ')
            self.document.add_paragraph('')

        self.document.add_paragraph('Chemin parcouru:')
        self.document.add_paragraph("(Vécu de la famille / Retour sur le travail AEMO / "
                                    "Contenu des entretiens / Ressources et vulnérabilités / "
                                    "Observations de l’intervenant / Problématique nouvelle)")
        self.document.add_paragraph('')
        self.document.add_paragraph("Retour du réseau:")
        self.document.add_paragraph('')

        self.document.add_paragraph('La suite', 'SousTitre')
        if title == '3 mois':
            self.document.add_paragraph('Décision pour la suite du suivi AEMO (poursuite / arrêt / autre): ')
            self.document.add_paragraph('')
            self.document.add_paragraph(
                'Proposition pour le projet commun (à élaborer dans le mois qui suit le bilan): ')
            self.document.add_paragraph('')
        if title == '1 an':
            self.document.add_paragraph('Décision pour la suite du suivi AEMO (poursuite / arrêt / autre): ')
            self.document.add_paragraph('')
            self.document.add_paragraph(
                'Proposition pour un nouveau projet commun (à élaborer dans le mois qui suit le bilan): ')
            self.document.add_paragraph('')
        if title == 'Final':
            self.document.add_paragraph('Décision pour la suite du suivi AEMO (arrêt / prolongation): ')
            self.document.add_paragraph('')
            self.document.add_paragraph('Orientation vers un autre service: ')
            self.document.add_paragraph('')

        self.document.save(self.path)


class ProjetCommunDocx(DocxBase):
    def produce(self, famille, *args):
        p = self.document.add_paragraph('PROJET COMMUN', 'Titre1')
        p.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
        self.document.add_paragraph("Famille: {} - {}".format(famille.nom, famille.adresse), 'SousTitre')
        self.document.add_paragraph('')
        self.document.add_paragraph("Évolution des attentes et/ou demandes d’aide", 'SousTitre')
        self.document.add_paragraph('')
        self.document.add_paragraph('Des parents: ')
        self.document.add_paragraph('')
        self.document.add_paragraph('')
        self.document.add_paragraph("De(s) l'enfant(s) / du(des) jeune(s): ")
        self.document.add_paragraph('')
        self.document.add_paragraph('')
        self.document.add_paragraph('Du service orienteur: ')
        self.document.add_paragraph('')
        self.document.add_paragraph('')
        self.document.add_paragraph("Projet commun", 'SousTitre')
        self.document.add_paragraph('-')
        self.document.add_paragraph('-')
        self.document.add_paragraph('-')
        self.document.add_paragraph('')
        self.document.add_paragraph('')
        self.document.add_paragraph('Lieu et date: ')
        self.document.add_paragraph('')
        self.document.add_paragraph('')
        self.document.add_paragraph('')
        self.document.add_paragraph("Les parents: \t\t\t l’(les) enfant(s) / le(s) jeune(s): \t\t l'intervenant AEMO:")

        self.document.save(self.path)
