from datetime import date

from django import forms
from django.db.models import Count, Q

from dal import autocomplete
from tinymce.widgets import TinyMCE

from aemo.models import (
    FamilleAemo, Intervention, PrestationAemo, JournalAemo, SuiviAemo
)
from carrefour.forms import (
    AgendaFormBase, BootstrapMixin, BSRadioSelect, CleanNpaLocaliteMixin, DateInput,
    PickDateWidget, PrestationFormBase, PickDateTimeWidget
)
from carrefour.models import Contact, LibellePrestation, NiveauInterv, Utilisateur
from common.choices import MotifsFinSuivi, REGIONS_CHOICES


class DemandeAemoForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = SuiviAemo
        fields = (
            'ref_aemo_presents', 'pers_famille_presentes', 'autres_pers_presentes',
            'difficultes', 'aides', 'disponibilites', 'autres_contacts', 'remarque'
        )
        widgets = {
            'difficultes': TinyMCE,
            'aides': TinyMCE,
            'disponibilites': TinyMCE,
        }


class RegionFilterForm(forms.Form):
    region = forms.ChoiceField(label="Région", required=False)

    def __init__(self, user, **kwargs):
        self.user = user
        super().__init__(**kwargs)
        equipes = FamilleAemo.check_perm_view(user)
        regions = []
        if 'littoral' in equipes:
            regions.extend([(k, v) for k, v in REGIONS_CHOICES if k.startswith('litt')])
        if 'montagnes' in equipes:
            regions.extend([(k, v) for k, v in REGIONS_CHOICES if k.startswith('mont')])
        if self.user.has_perm('aemo.manage_littoral') or self.user.has_perm(
                'aemo.manage_montagnes'):
            regions.insert(0, ('mont', 'AEMO-Montagnes'))
            regions.insert(0, ('litt', 'AEMO-Littoral'))
        self.fields['region'].choices = [('0', 'Toutes')] + regions

    def filter(self, familles):
        region = self.cleaned_data['region']
        if region and region != '0':
            if region == 'litt':
                familles = familles.filter(region__startswith='litt')
            elif region == 'mont':
                familles = familles.filter(region__startswith='mont')
            else:
                familles = familles.filter(region=region)
        return familles


class FamilleFilterForm(RegionFilterForm):
    statut = forms.ChoiceField(label="Statut", required=False)
    interv = forms.ChoiceField(label="Intervenant-e", required=False)
    letter = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Recherche…', 'autocomplete': 'off'}), required=False
    )

    def __init__(self, user, **kwargs):
        self.user = user
        super().__init__(user, **kwargs)
        self.fields['interv'].choices = [
            ('', 'Tous'), ('0', 'Aucun')] + [
            (user.id, user.nom_prenom) for user in user.utilisateurs_meme_equipe()
        ]
        self.fields['statut'].choices = [
            ('', 'Tous'),
            ('demande_prioritaire', 'Demande prioritaire'),
            ('demande_eventuelle', 'Demande éventuelle'),
        ] + [(key, etape.nom) for key, etape in SuiviAemo.WORKFLOW.items()]

    def filter(self, familles):
        if self.cleaned_data['interv']:
            if self.cleaned_data['interv'] == '0':
                familles = familles.annotate(
                    num_interv=Count('suiviaemo__intervention', filter=Q(suiviaemo__intervention__date_fin=None))
                ).filter(
                    num_interv=0
                )
            else:
                familles = familles.filter(
                    suiviaemo__aemo_referents=self.cleaned_data['interv'],
                    suiviaemo__intervention__date_fin=None
                )

        if self.cleaned_data['letter']:
            familles = familles.filter(nom__istartswith=self.cleaned_data['letter'])

        familles = super().filter(familles)

        if self.cleaned_data['statut']:
            if self.cleaned_data['statut'] == 'demande_prioritaire':
                familles = familles.filter(suiviaemo__demande_prioritaire=True)
            elif self.cleaned_data['statut'] == 'demande_eventuelle':
                familles = familles.filter(suiviaemo__date_demande_eventuelle__isnull=False,
                                           suiviaemo__date_demande__isnull=True)
            else:
                result = []
                for fam in familles:
                    if fam.suiviaemo.etape and fam.suiviaemo.etape.code == self.cleaned_data['statut']:
                        result.append(fam.pk)
                familles = FamilleAemo.objects.filter(pk__in=result)
        return familles


class SuiviAemoForm(BootstrapMixin, forms.ModelForm):
    ope_referent = forms.ModelChoiceField(
        queryset=Contact.membres_ope(), required=False, label='Interv. IPE'
    )
    referents_actifs = forms.ModelMultipleChoiceField(
        queryset=Utilisateur.objects.actifs(),
        required=False,
        label='Intervenant-e-s AEMO',
        widget=autocomplete.ModelSelect2Multiple(
            url='utilisateur-autocomplete',
            attrs={'title': "CTRL + clic pour sélectionner ou déselectionner un-e autre intervenant-e."}
        ),
    )

    class Meta:
        model = SuiviAemo
        fields = (
            'equipe', 'ope_referent', 'mandat_ope', 'referents_actifs', 'referent_note',
            'service_orienteur', 'motif_demande', 'detail_demande', 'demande_prioritaire', 'demarche', 'remarque',
            'remarque_privee'
        )
        labels = {
            'service_orienteur': 'Service orienteur',
            'referent_note': 'Autres intervenant-e-s',
            'remarque_privee': 'Remarque privée (non imprimée)',
            'demarche': 'Statut de la démarche',
        }

    def __init__(self, *args, **kwargs):
        kwargs['initial']['referents_actifs'] = kwargs['instance'].suivi_referents
        super().__init__(*args, **kwargs)

    def save(self, **kwargs):
        instance = super().save(**kwargs)
        if 'referents_actifs' in self.changed_data:
            added = [
                ref for ref in self.cleaned_data['referents_actifs']
                if ref not in self.initial['referents_actifs']
            ]
            for ref in added:
                Intervention.objects.create(suivi=instance, intervenant=ref, date_debut=date.today())
            deleted = [
                ref for ref in self.initial['referents_actifs']
                if ref not in self.cleaned_data['referents_actifs']
            ]
            for ref in deleted:
                inter = instance.intervention_set.get(intervenant=ref, date_fin=None)
                inter.date_fin = date.today()
                inter.save()
        return instance


class AgendaAemoForm(AgendaFormBase):
    ope_referent = forms.ModelChoiceField(
        queryset=Contact.membres_ope(), required=False
    )

    class Meta:
        model = SuiviAemo
        fields = (
            'date_demande_eventuelle', 'date_demande', 'date_analyse', 'date_confirmation',
            'date_debut_suivi', 'date_bilan_3mois', 'date_bilan_annee',
            'date_prolongation_1', 'date_bilan_prolongation_1',
            'date_prolongation_2', 'date_bilan_prolongation_2',
            'date_bilan_final', 'date_fin_suivi', 'motif_fin_suivi'
        )
        widgets = {field: PickDateWidget for field in fields[:-1]}

    def __init__(self, data=None, **kwargs):
        super().__init__(data=data, **kwargs)
        empty = [('', '--------')]
        has_suivi = self.instance.date_debut_suivi is not None or bool(data and data.get('date_debut_suivi'))
        self.fields['motif_fin_suivi'].choices = empty + (
            MotifsFinSuivi.choices_fin() if has_suivi else MotifsFinSuivi.choices_abandon('aemo')
        )

    def clean(self):
        cleaned_data = super().clean()
        original = SuiviAemo.objects.get(pk=self.instance.pk)
        errors = []
        if original.date_debut_suivi is None and cleaned_data.get('date_debut_suivi') is not None:
            # Coordonnées de la famille
            required_fields = ['npa', 'localite', 'rue', 'telephone', 'region']
            for field in required_fields:
                if getattr(original.famille, field) == '':
                    verbose = FamilleAemo._meta.get_field(field).verbose_name
                    errors.append(f"Famille: Le champ «{verbose}» est nécessaire pour continuer.")
            # Enfants suivis
            for enfant in original.famille.membres_suivis():
                if getattr(enfant, "date_naissance") in [None, '']:
                    errors.append(
                        f"{enfant.nom_prenom}: Le champ «Date de naissance» est nécessaire pour continuer.")
                if getattr(enfant.formation, "statut") in [None, '']:
                    errors.append(
                        f"{enfant.nom_prenom}: Le champ «Scolarité (cycle)» est nécessaire pour continuer.")
            # Suivi
            required_fields = ['mandat_ope', 'motif_demande', 'demarche']
            for field in required_fields:
                if getattr(original, field) in [None, '']:
                    verbose = SuiviAemo._meta.get_field(field).verbose_name
                    errors.append(f"Suivi: Le champ «{verbose}» est nécessaire pour continuer.")
        if errors:
            raise forms.ValidationError(errors)
        return self.cleaned_data


class PrestationAemoForm(PrestationFormBase):

    class Meta(PrestationFormBase.Meta):
        model = PrestationAemo
        fields = ('lib_prestation', 'date_prestation', 'duree')

    def __init__(self, *args, famille=None, prest_codes=None, user=None, **kwargs):
        self.famille = famille
        super().__init__(*args, user=user, **kwargs)
        self.fields['lib_prestation'] = forms.ModelChoiceField(
            label='Prestations',
            queryset=LibellePrestation.objects.filter(unite='aemo', code__in=prest_codes).order_by('code'),
            widget=BSRadioSelect,
            required=True,
            empty_label=None,
        )

    def clean(self):
        if self.instance.pk is None and self.famille and not self.famille.membres_suivis().exists():
            raise forms.ValidationError("Vous devez enregistrer un Enfant pour saisir une prestation!")
        return self.cleaned_data


class FamilleForm(BootstrapMixin, CleanNpaLocaliteMixin, forms.ModelForm):

    equipe = forms.ChoiceField(label="Équipe AEMO", choices=SuiviAemo.AEMO_EQUIPES_CHOICES)
    aemo_ref = forms.ModelChoiceField(
        label="Interv. AEMO",
        queryset=Utilisateur.intervenants_aemo(annee=date.today().year),
        required=False
    )

    class Meta:
        model = FamilleAemo
        exclude = ('genogramme', 'sociogramme', 'archived')

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if kwargs['instance'] and kwargs['instance'].pk:
            self.initial['equipe'] = kwargs['instance'].suiviaemo.equipe
            del self.fields['aemo_ref']

    def save(self, **kwargs):
        if self.instance.pk is None:
            famille = FamilleAemo.objects.create_famille(
                equipe=self.cleaned_data['equipe'], **self.instance.__dict__
            )
            if self.cleaned_data['aemo_ref']:
                famille.add_referent(self.cleaned_data['aemo_ref'])
        else:
            famille = super().save(**kwargs)
            if famille.suiviaemo.equipe != self.cleaned_data['equipe']:
                famille.suiviaemo.equipe = self.cleaned_data['equipe']
                famille.suiviaemo.save()
        return famille


class JournalAemoForm(BootstrapMixin, forms.ModelForm):

    class Meta:
        model = JournalAemo
        fields = ['theme', 'date', 'texte']
        field_classes = {
            'date': forms.SplitDateTimeField,
        }
        widgets = {
            'date': PickDateTimeWidget,
        }


class NiveauIntervForm(BootstrapMixin, forms.ModelForm):
    titre = "Édition du niveau d’intervention"

    class Meta:
        model = NiveauInterv
        fields = ['niveau', 'date_debut', 'date_fin']
        widgets = {
            'date_debut': DateInput,
            'date_fin': DateInput,
        }

    def __init__(self, *args, **kwargs):
        self.famille = kwargs.pop('famille')
        super().__init__(*args, **kwargs)
        if self.instance.pk is None:
            self.fields.pop('date_fin')
            if self.famille.niveaux_interv.count() > 0:
                self.fields['date_debut'].required = True

    def clean_niveau(self):
        niv = self.cleaned_data['niveau']
        if self.instance.pk is None:
            der_niv = self.famille.niveaux_interv.last() if self.famille.niveaux_interv.count() > 0 else None
            if der_niv and der_niv.niveau == niv:
                raise forms.ValidationError(f"Le niveau {niv} est déjà actif.")
        return niv
