import io
import os
import calendar
from collections import OrderedDict
from datetime import date
from itertools import chain
from dal import autocomplete
from two_factor.views import SetupView as TwoFactSetupView

from django.apps import apps
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.models import Group, Permission
from django.contrib.auth.views import PasswordChangeView as AuthPasswordChangeView
from django.contrib.messages.views import SuccessMessageMixin
from django.core.exceptions import PermissionDenied
from django.core.paginator import EmptyPage
from django.db.models import CharField, Value
from django.db.models.deletion import ProtectedError
from django.http import FileResponse, HttpResponseRedirect, JsonResponse, Http404
from django.shortcuts import get_object_or_404
from django.urls import reverse, reverse_lazy
from django.utils.crypto import get_random_string
from django.utils.timezone import now
from django.views.generic import (
    CreateView, DeleteView as DjangoDeleteView, DetailView, FormView, ListView, TemplateView,
    UpdateView, View
)

from carrefour.export import ExportReporting
from carrefour.forms import (
    CercleScolaireForm, ContactForm, ContactMergeForm, ContactExterneAutocompleteForm,
    DerogationSaisieTardiveForm, DocumentUploadForm, FormationForm, MonthSelectionForm,
    PersonneForm, RoleForm, ServiceForm, UtilisateurForm
)
from carrefour.models import (
    CercleScolaire, Contact, DerogationSaisieTardive, Document, Famille, Formation,
    Personne, Role, Service, Utilisateur
)
from carrefour.utils import is_ajax

MSG_NO_ACCESS = "Vous n’avez pas la permission d’accéder à cette page."
MSG_READ_ONLY = "Vous n'avez pas les droits nécessaires pour modifier cette page"


class PaginateListView(ListView):
    """
    Evite une erreur 404 lorsque le paramètre ?page est plus grand que le
    nombre total de pages calculé par Paginator
    """
    paginate_by = 20

    def get(self, request, *args, **kwargs):
        try:
            return super().get(request, *args, **kwargs)
        except (Http404, EmptyPage):
            return HttpResponseRedirect(request.path)


class DeleteView(DjangoDeleteView):
    """
    Nous ne suivons pas la méthode Django d'afficher une page de confirmation
    avant de supprimer un objet, mais nous avertissons avec un message JS avant
    de POSTer directement la suppression. Pour cela, nous autorisons uniquement
    la méthode POST.
    """
    http_method_names = ['post']


class HomeView(TemplateView):
    template_name = 'carrefour/index.html'


class SetupView(TwoFactSetupView):
    def get(self, request, *args, **kwargs):
        # The original view is short-circuiting to complete is a device exists.
        # We want to allow adding a second device if needed.
        return super(TwoFactSetupView, self).get(request, *args, **kwargs)


class PasswordChangeView(AuthPasswordChangeView):
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        response = super().form_valid(form)
        messages.success(self.request, "Votre mot de passe a bien été modifié.")
        return response


class ContactCreateView(CreateView):
    template_name = 'carrefour/contact_edit.html'
    model = Contact
    form_class = ContactForm
    success_url = reverse_lazy('contact-list')
    action = 'Création'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['show_role_service'] = not bool(self.request.GET.get('forpers'))
        return kwargs

    def form_valid(self, form):
        contact = form.save()
        for_pers = self.request.GET.get('forpers')
        if for_pers and '-' in for_pers:
            model, pk = for_pers.split('-')
            if model == 'personne':
                pers = get_object_or_404(Personne, pk=pk)
                pers.reseaux.add(contact)
                return HttpResponseRedirect(reverse('personne-reseau-list', args=[pers.pk]))
            elif model == 'person':
                from ser.models import Person
                pers = get_object_or_404(Person, pk=pk)
                pers.contacts.add(contact)
                return HttpResponseRedirect(reverse('asap-person-change', args=[pers.pk]))
            elif model == 'benef':
                from batoude.models import Beneficiaire
                benef = get_object_or_404(Beneficiaire, pk=pk)
                benef.reseau.add(contact)
                return HttpResponseRedirect(reverse('batoude-contact-list', args=[benef.pk]))
        return HttpResponseRedirect(self.success_url)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'role_form': RoleForm(),
            'service_form': ServiceForm(),
        })
        return context


class ContactListView(PaginateListView):
    template_name = 'carrefour/contact_list.html'
    model = Contact

    def dispatch(self, request, *args, **kwargs):
        doublons = request.GET.getlist('doublons', None)
        if doublons:
            url = reverse('contact-merge', args=[doublons[0]])
            return HttpResponseRedirect(f"{url}?{request.META['QUERY_STRING']}")
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        self.current_letter = self.request.GET.get('l', 'all')
        query = Contact.objects.filter(est_actif=True).exclude(
            utilisateur__isnull=False
        ).prefetch_related('personne_set', 'beneficiaire_set', 'beneficiaires')

        if self.current_letter != 'all':
            self.paginate_by = None
            query = query.filter(nom__istartswith=self.current_letter)

        return query

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'current_letter': self.current_letter,
        })
        return context


class ContactMergeView(PermissionRequiredMixin, UpdateView):
    """
    Fusion des contacts sélectionnés. Le premier contact est conservé,
    les suivants sont supprimés. Le paramètre pk est nécessaire pour la
    classe UpdateView.
    """
    permission_required = 'carrefour.delete_contact'
    template_name = 'carrefour/contact_merge.html'
    form_class = ContactMergeForm
    model = Contact

    def dispatch(self, request, *args, **kwargs):
        if request.method == 'GET':
            if len(request.GET.getlist('doublons')) < 2:
                messages.error(request, "Vous devez sélectionner au moins deux contacts.")
                return HttpResponseRedirect(reverse('contact-list'))
            self.doublons_id = request.GET.getlist('doublons')[1:]
        else:
            self.doublons_id = request.POST.get('doublons').split(',')
        self.doublons = Contact.objects.filter(pk__in=self.doublons_id)
        return super().dispatch(request, *args, **kwargs)

    def get_initial(self):
        return {'doublons': ','.join(self.doublons_id)}

    def get_context_data(self, **kwargs):
        return {**super().get_context_data(**kwargs),
                'contacts': self.doublons.values()}

    def form_valid(self, form):
        original = form.save()
        original.fusionner(*self.doublons)
        return HttpResponseRedirect(reverse('contact-list'))


class ContactUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'carrefour.change_contact'
    template_name = 'carrefour/contact_edit.html'
    model = Contact
    form_class = ContactForm
    success_url = reverse_lazy('contact-list')
    action = 'Modification'

    def delete_url(self):
        return reverse('contact-delete', args=[self.object.pk])


class ContactDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'carrefour.delete_contact'
    model = Contact
    success_url = reverse_lazy('contact-list')

    def form_valid(self, form):
        if len(self.object.dossiers(actif=False)) > 0:
            self.object.est_actif = False
            self.object.save()
            return HttpResponseRedirect(self.success_url)
        return super().form_valid(form)


class ContactAutocompleteView(autocomplete.Select2QuerySetView):
    ope = False

    def get_queryset(self):
        qs = Contact.membres_ope() if self.ope else Contact.objects.filter(est_actif=True)
        if self.q:
            qs = qs.filter(nom__istartswith=self.q)
        return qs


class ContactExterneAutocompleteView(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Contact.objects.\
            filter(est_actif=True).\
            exclude(service__sigle__startswith='OPE').\
            exclude(service__sigle='CARREFOUR')
        if self.q:
            qs = qs.filter(nom__istartswith=self.q)
        return qs


class ContactTestDoublon(View):
    def post(self, request, *args, **kwargs):
        nom = request.POST.get('nom')
        prenom = request.POST.get('prenom')
        contacts = Contact.objects.filter(nom=nom, prenom=prenom)
        data = ''
        if contacts.exists():
            data = [{'nom': c.nom, 'prenom': c.prenom} for c in contacts]
        return JsonResponse(data, safe=False)


class ContactDossiersActifsView(View):
    def get(self, request, *args, **kwargs):
        contact = get_object_or_404(Contact, pk=kwargs['pk'])
        dossiers = contact.dossiers(actif=True)
        result = {
            'contact': contact.nom_prenom,
            'dossiers': dossiers if dossiers else 'Aucun dossier en cours',
        }
        return JsonResponse(result)


class ServiceCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'carrefour.add_service'
    template_name = 'carrefour/service_edit.html'
    model = Service
    form_class = ServiceForm
    success_url = reverse_lazy('service-list')
    action = 'Création'

    def form_valid(self, form):
        if is_ajax(self.request):
            service = form.save()
            return JsonResponse({'pk': service.pk, 'sigle': service.sigle})
        return super().form_valid(form)

    def form_invalid(self, form):
        if is_ajax(self.request):
            return JsonResponse({'error': form.errors.as_text()})
        return super().form_invalid(form)


class ServiceListView(ListView):
    template_name = 'carrefour/service_list.html'
    model = Service

    def get_queryset(self):
        return Service.objects.exclude(sigle='FAMILLE')


class ServiceUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'carrefour.change_service'
    template_name = 'carrefour/service_edit.html'
    model = Service
    form_class = ServiceForm
    success_url = reverse_lazy('service-list')
    action = 'Modification'

    def delete_url(self):
        return reverse('service-delete', args=[self.object.pk])


class ServiceDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'carrefour.delete_service'
    model = Service
    success_url = reverse_lazy('service-list')


class FormationView(SuccessMessageMixin, UpdateView):
    template_name = 'carrefour/formation_edit.html'
    model = Formation
    form_class = FormationForm
    success_message = 'Les modifications ont été enregistrées avec succès'

    def get_object(self, queryset=None):
        personne = get_object_or_404(Personne, pk=self.kwargs['pk'])
        return personne.formation

    def get_success_url(self):
        return self.object.personne.famille.edit_url

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'famille': get_object_or_404(Personne, pk=self.kwargs['pk']).famille,
            'source': self.object.personne.famille.typ,
        })
        return context


class PersonneBaseMixin:
    template_name = 'carrefour/personne_edit.html'
    model = Personne
    form_class = PersonneForm

    def dispatch(self, request, *args, **kwargs):
        self.famille = get_object_or_404(Famille, pk=kwargs['pk'])
        if not self.famille.check_perm_view(request.user):
            raise PermissionDenied("Vous n’avez pas la permission de modifier cette famille.")
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        return {**super().get_context_data(**kwargs), 'famille': self.famille}

    def get_success_url(self):
        return self.famille.edit_url


class PersonneCreateView(PersonneBaseMixin, CreateView):
    action = 'Membre de la famille '

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['famille'] = self.famille
        kwargs['role'] = self.request.GET.get('role', None)
        return kwargs


class PersonneUpdateView(SuccessMessageMixin, PersonneBaseMixin, UpdateView):
    pk_url_kwarg = 'obj_pk'
    action = 'Modification'
    success_message = 'Les modifications ont été enregistrées avec succès'

    def delete_url(self):
        return reverse('personne-delete', args=[self.famille.typ, self.famille.pk, self.object.pk])


class PersonneDeleteView(PersonneBaseMixin, DeleteView):
    form_class = DeleteView.form_class

    def get_object(self, *args, **kwargs):
        pers = get_object_or_404(Personne, pk=self.kwargs['obj_pk'])
        if pers.role.nom == "Enfant suivi":
            raise PermissionDenied(
                "Un enfant suivi ne peut pas être directement supprimé. Si c’est "
                "vraiment ce que vous voulez, mettez-le d’abord comme Enfant non suivi."
            )
        return pers


class PersonneReseauListView(DetailView):
    template_name = 'carrefour/personne_reseau_list.html'
    model = Personne

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        famille = self.object.famille
        context.update({
            'source': famille.typ,
            'famille': famille,
            'form': ContactExterneAutocompleteForm(),
        })
        return context


class PersonneReseauAddView(FormView):
    template_name = 'carrefour/contact_add_form.html'
    form_class = ContactExterneAutocompleteForm
    titre_page = "Contacts"
    titre_formulaire = "Contacts enregistrés"

    def get_success_url(self):
        return reverse('personne-reseau-list', args=[self.kwargs['pk']])

    def form_valid(self, form):
        personne = get_object_or_404(Personne, pk=self.kwargs['pk'])
        personne.reseaux.add(*form.cleaned_data['reseaux'])
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'person_pk': self.kwargs['pk'],
        }


class PersonneReseauRemove(View):
    def post(self, request, *args, **kwargs):
        pers = get_object_or_404(Personne, pk=kwargs['pk'])
        contact = get_object_or_404(Contact, pk=kwargs['obj_pk'])
        pers.reseaux.remove(contact)
        return HttpResponseRedirect(reverse('personne-reseau-list', args=[pers.pk]))


class UtilisateurListView(PaginateListView):
    template_name = 'carrefour/utilisateur_list.html'
    model = Utilisateur
    is_active = True

    def get_queryset(self):
        return Utilisateur.objects.filter(date_desactivation__isnull=self.is_active)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'active_users': self.is_active,
        })
        return context


class UtilisateurCreateView(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'carrefour.add_utilisateur'

    template_name = 'carrefour/utilisateur_edit.html'
    model = Utilisateur
    form_class = UtilisateurForm
    success_url = reverse_lazy('utilisateur-list')
    success_message = "L’utilisateur «%(username)s» a été créé avec le mot de passe «%(password)s»."

    def form_valid(self, form):
        form.instance.first_name = form.cleaned_data['prenom']
        form.instance.last_name = form.cleaned_data['nom']
        form.instance.service, _ = Service.objects.get_or_create(sigle='CARREFOUR', defaults={'sigle': 'CARREFOUR'})
        pwd = get_random_string(length=10)
        form.instance.set_password(pwd)
        form.cleaned_data['password'] = pwd  # for success message
        return super().form_valid(form)


class UtilisateurUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'carrefour.change_utilisateur'

    template_name = 'carrefour/utilisateur_edit.html'
    model = Utilisateur
    form_class = UtilisateurForm
    success_url = reverse_lazy('utilisateur-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context.update({
            'interventions': self.object.interventions.filter(
                date_fin__isnull=True,
                suivi__date_fin_suivi__isnull=True
            ).select_related('suivi__famille')
        })
        return context

    def delete_url(self):
        return reverse('utilisateur-delete', args=[self.object.pk])


class UtilisateurReactivateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'carrefour.change_utilisateur'

    def post(self, request, *args, **kwargs):
        utilisateur = get_object_or_404(Utilisateur, pk=self.kwargs['pk'])
        pwd = get_random_string(length=10)
        utilisateur.set_password(pwd)
        utilisateur.date_desactivation = None
        utilisateur.is_active = True  # C'est ce flag qui empêche la connexion au système
        utilisateur.est_actif = True
        utilisateur.save()

        messages.success(
            request,
            f"Le dossier de {utilisateur.username} a été réactivé avec succès. Le nouveau mot de passe est «{pwd}»."
        )
        return HttpResponseRedirect(reverse('utilisateur-edit', kwargs=self.kwargs))


class UtilisateurPasswordReinitView(PermissionRequiredMixin, View):
    permission_required = 'carrefour.change_utilisateur'

    def post(self, request, *args, **kwargs):
        utilisateur = get_object_or_404(Utilisateur, pk=self.kwargs['pk'])
        pwd = get_random_string(length=10)
        utilisateur.set_password(pwd)
        utilisateur.save()
        messages.success(request, 'Le nouveau mot de passe de «%s» est «%s».' % (utilisateur, pwd))
        return HttpResponseRedirect(reverse('utilisateur-edit', kwargs=self.kwargs))


class UtilisateurDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'carrefour.delete_utilisateur'

    model = Utilisateur
    success_url = reverse_lazy('utilisateur-list')

    def form_valid(self, form):
        self.object.is_active = False  # C'est ce flag qui empêche la connexion au système
        self.object.est_actif = False
        self.object.date_desactivation = date.today()
        self.object.save()
        self.object.interventions.filter(date_fin__isnull=True).update(date_fin=date.today())
        messages.success(self.request, "La personne a été désactivée avec succès")
        return HttpResponseRedirect(self.success_url)


class UtilisateurAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Utilisateur.objects.filter(service__sigle='CARREFOUR', date_desactivation__isnull=True)

        if self.q:
            qs = qs.filter(nom__istartswith=self.q)
        return qs


class CercleScolaireListView(ListView):
    template_name = 'carrefour/cercle_scolaire_list.html'
    model = CercleScolaire


class CercleScolaireUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'carrefour.change_cerclescolaire'
    template_name = 'carrefour/cercle_scolaire_edit.html'
    model = CercleScolaire
    form_class = CercleScolaireForm
    success_url = reverse_lazy('cercle-list')

    def delete_url(self):
        return reverse('cercle-delete', args=[self.object.pk])


class CercleScolaireCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'carrefour.add_cerclescolaire'
    template_name = 'carrefour/cercle_scolaire_edit.html'
    model = CercleScolaire
    form_class = CercleScolaireForm
    success_url = reverse_lazy('cercle-list')


class CercleScolaireDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'carrefour.delete_cerclescolaire'
    model = CercleScolaire
    success_url = reverse_lazy('cercle-list')


class RoleListView(ListView):
    template_name = 'carrefour/role_list.html'
    model = Role


class RoleUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'carrefour.change_role'
    template_name = 'carrefour/role_edit.html'
    model = Role
    form_class = RoleForm
    success_url = reverse_lazy('role-list')

    def delete_url(self):
        if self.object.personne_set.count() == 0:
            return reverse('role-delete', args=[self.object.pk])


class RoleCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'carrefour.add_role'
    template_name = 'carrefour/role_edit.html'
    model = Role
    form_class = RoleForm
    success_url = reverse_lazy('role-list')

    def form_valid(self, form):
        if is_ajax(self.request):
            role = form.save()
            return JsonResponse({'pk': role.pk, 'nom': role.nom})
        return super().form_valid(form)

    def form_invalid(self, form):
        if is_ajax(self.request):
            return JsonResponse({'error': form.errors.as_text()})
        return super().form_invalid(form)


class RoleDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'carrefour.delete_role'
    model = Role
    success_url = reverse_lazy('role-list')

    def form_valid(self, form):
        try:
            return super().form_valid(form)
        except ProtectedError as e:
            # TODO: Il y a certainement mieux...
            messages.add_message(self.request, messages.ERROR, str(e), "Suppression impossible")
            return HttpResponseRedirect(reverse('role-list'))


class DocumentBaseView:
    template_name = 'carrefour/document_upload.html'
    titre_page = ''
    titre_formulaire = ''

    def dispatch(self, request, *args, **kwargs):
        self.famille = get_object_or_404(Famille, pk=kwargs['pk'])
        self.titre_page = self.titre_page.format(famille=self.famille.nom)
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return self.famille.suivi_url


class DocumentUploadView(DocumentBaseView, CreateView):
    form_class = DocumentUploadForm
    titre_page = 'Documents externes de la famille {famille}'
    titre_formulaire = 'Nouveau document'

    def get_initial(self):
        initial = super().get_initial()
        initial['famille'] = self.famille
        return initial

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'user': self.request.user}


class DocumentDeleteView(DeleteView):
    model = Document
    pk_url_kwarg = 'doc_pk'

    def delete(self, *args, **kwargs):
        response = super().delete(*args, **kwargs)
        messages.success(self.request, "Le document a été supprimé avec succès")
        return response

    def get_success_url(self):
        return self.object.famille.suivi_url


class GenogrammeUploadView(DocumentBaseView, UpdateView):
    model = Famille
    fields = ['genogramme']
    titre_page = 'Génogramme de la famille {famille}'
    titre_formulaire = 'Nouveau génogramme (format PDF uniquement)'


class SociogrammeUploadView(DocumentBaseView, UpdateView):
    model = Famille
    fields = ['sociogramme']
    titre_page = 'Sociogramme de la famille {famille}'
    titre_formulaire = 'Nouveau sociogramme (format PDF uniquement)'


class FamilleFileDeleteView(View):
    """Common view to remove famille.genogramme or famille.sociogramme."""
    field_name = None

    def post(self, *args, **kwargs):
        famille = get_object_or_404(Famille, pk=self.kwargs['pk'])
        os.remove(getattr(famille, self.field_name).path)
        setattr(famille, self.field_name, '')
        famille.save()
        return HttpResponseRedirect(famille.suivi_url)


class PermissionOverview(TemplateView):
    template_name = 'carrefour/permissions.html'
    perm_map = {'add': 'création', 'change': 'modification', 'delete': 'suppression', 'view': 'affichage'}

    def get_context_data(self, **kwargs):
        from asaef.models import SuiviAsaef
        from aemo.models import SuiviAemo
        from batoude.models import Beneficiaire

        def verbose(perm):
            return self.perm_map.get(perm.codename.split('_')[0], perm.codename)

        context = super().get_context_data(**kwargs)
        groups = Group.objects.all().prefetch_related('permissions')
        grp_perms = {gr.name: [perm.codename for perm in gr.permissions.all()] for gr in groups}
        objects = [CercleScolaire, Contact, Role, Service, Utilisateur]
        all_perms = Permission.objects.filter(
            content_type__app_label__in=['carrefour', 'aemo'],
            content_type__model__in=[o.__name__.lower() for o in objects]
        )

        def perms_for_model(model):
            return [
                (perm.codename, verbose(perm)) for perm in all_perms
                if perm.content_type.model == model.__name__.lower()
            ]

        perm_groups = OrderedDict()
        # {'Contact': [('view_contact', 'affichage), ('change_contact', 'modification'), ...]}
        for obj in objects:
            perm_groups[obj._meta.verbose_name] = perms_for_model(obj)
        perm_groups['AEMO'] = list(SuiviAemo._meta.permissions)
        perm_groups['AEMO'].extend([('delete_familleaemo', 'Supprimer une famille'), ])
        perm_groups['ASAEF'] = SuiviAsaef._meta.permissions
        perm_groups['Batoude'] = Beneficiaire._meta.permissions
        context.update({
            'groups': groups,
            'grp_perms': grp_perms,
            'perms_by_categ': perm_groups,
        })
        return context


class BaseAdminPrestationListView(ListView):
    model = Utilisateur
    prest_model = None
    template_name = 'carrefour/admin_prestation_list.html'

    def dispatch(self, request, *args, **kwargs):
        self.annee = int(request.GET.get('annee', date.today().year))
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        raise NotImplementedError

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'totaux_mensuels': self.prest_model.temps_totaux_mensuels_fam_gen(self.annee),
            'total_annuel_general': self.prest_model.temps_total_general_fam_gen(self.annee),
            'annee': self.annee,
            'titre': self.titre,
        })
        return context


class BasePDFView(View):
    obj_class = None
    pdf_class = None

    def get_object(self):
        return get_object_or_404(self.obj_class, pk=self.kwargs['pk'])

    def get(self, request, *args, **kwargs):
        selector = request.GET.get('selector', None)
        instance = self.get_object()
        temp = io.BytesIO()
        pdf = self.pdf_class(temp)
        pdf.produce(instance, selector=selector)
        filename = pdf.get_filename(instance)
        temp.seek(0)
        return FileResponse(temp, as_attachment=True, filename=filename)


class BaseJournalListView(PaginateListView):
    famille = None

    def get_queryset(self):
        return super().get_queryset().filter(
            famille__pk=self.famille.pk
        ).order_by('-date').select_related('famille', 'auteur')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'famille': self.famille,
        })
        return context


class BaseJournalCreateView(CreateView):
    famille = None

    def get_initial(self):
        return {**super().get_initial(), 'date': now()}

    def form_valid(self, form):
        form.instance.famille = self.famille
        form.instance.auteur = self.request.user
        form.save()
        return HttpResponseRedirect(reverse('{}-journal-list'.format(self.famille.typ), args=[self.kwargs['pk']]))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'famille': self.famille,
        })
        return context


class BaseJournalUpdateView(UpdateView):
    pk_url_kwarg = 'obj_pk'
    action = 'Modification'
    success_message = 'Les modifications ont été enregistrées avec succès'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'famille': self.object.famille,
        })
        return context

    def get_success_url(self):
        return reverse('{}-journal-list'.format(self.object.famille.typ), args=[self.object.famille.pk])

    def delete_url(self):
        return reverse(
            '{}-journal-delete'.format(self.object.famille.typ),
            args=[self.object.famille.pk, self.object.pk]
        )


class BaseJournalDeleteView(DeleteView):
    pk_url_kwarg = 'obj_pk'

    def get_success_url(self):
        return reverse('{}-journal-list'.format(self.object.famille.typ), args=[self.object.famille.pk])


class ExportPrestationView(PermissionRequiredMixin, FormView):
    permission_required = 'carrefour.export_aemo'
    template_name = 'carrefour/export.html'
    form_class = MonthSelectionForm
    filename = 'carrefour_reporting_{}'.format(date.strftime(date.today(), '%Y-%m-%d'))

    def _prepare_query(self, *queries):
        """Return a list of families, sorted by prest and name."""
        queries = [
            q.annotate(
                prest=Value(q.model._meta.app_label.upper(), output_field=CharField())
            )
            for q in queries
        ]
        # Not using union to keep proxy classes
        return sorted(
            chain(*queries),
            key=lambda fam: (fam.prest, fam.nom)
        )

    def get_initial(self):
        initial = super().get_initial()
        initial.update({
            'mois': date.today().month - 1 if date.today().month > 1 else 12,
            'annee': date.today().year if date.today().month > 1 else date.today().year - 1,
        })
        return initial

    def form_valid(self, form):
        mois = int(form.cleaned_data['mois'])
        annee = int(form.cleaned_data['annee'])
        debut_mois = date(annee, mois, 1)
        fin_mois = date(annee, mois, calendar.monthrange(annee, mois)[1])

        export = ExportReporting()
        benef_models = [apps.get_app_config(app).benef_model for app in ['aemo', 'asaef', 'batoude']]

        # Demandes en cours
        queries_en_cours = [model.suivis_en_cours(debut_mois, fin_mois) for model in benef_models]
        export.produce_suivis(
            'En_cours_{}'.format(debut_mois.strftime("%m.%Y")),
            self._prepare_query(*queries_en_cours),
            debut_mois
        )

        #  Nouvelles demandes
        queries_nouveaux = [model.suivis_nouveaux(debut_mois, fin_mois) for model in benef_models]
        export.produce_nouveaux(
            "Nouveaux_{}".format(debut_mois.strftime("%m.%Y")),
            self._prepare_query(*queries_nouveaux)
        )

        #  Fins de suivis
        queries_termines = [model.suivis_termines(debut_mois, fin_mois) for model in benef_models]
        export.produce_termines(
            "Terminés_{}".format(debut_mois.strftime("%m.%Y")),
            self._prepare_query(*queries_termines)
        )

        #  Familles en attente
        queries_attente = [benef_models[0].suivis_en_attente(debut_mois, fin_mois)]
        export.produce_attente(
            "En attente_{}".format(debut_mois.strftime("%m.%Y")),
            self._prepare_query(*queries_attente)
        )

        return export.get_http_response(
            'carrefour_reporting_{}'.format(date.strftime(debut_mois, '%m_%Y'))
        )


class UtilisateurOtpDeviceReinitView(PermissionRequiredMixin, View):
    permission_required = 'carrefour.change_utilisateur'

    def post(self, request, *args, **kwargs):
        utilisateur = get_object_or_404(Utilisateur, pk=self.kwargs['pk'])
        if utilisateur.totpdevice_set.exists():
            utilisateur.totpdevice_set.all().delete()
            messages.success(request, f'Tous les mobiles de «{utilisateur}» ont été réinitialisés.')
        else:
            messages.error(request, f'Aucune configuration mobile trouvée pour «{utilisateur}».')
        return HttpResponseRedirect(reverse('utilisateur-edit', kwargs=self.kwargs))


class BaseDemandeView(SuccessMessageMixin, UpdateView):
    template_name = 'aemo/demande_edit.html'
    success_message = 'Les modifications ont été enregistrées avec succès'

    def prepare_form(self, suivi):
        items = [
            "Selon le professionel",
            "Selon les parents",
            "Selon l'enfant",
        ]
        if suivi.difficultes == '':
            suivi.difficultes = ''.join(
                ['<p><strong>{}</strong></p><p></p>'.format(item) for item in items if suivi.difficultes == '']
            )
        if suivi.aides == '':
            suivi.aides = ''.join(
                ['<strong>{}</strong></p><p></p>'.format(item) for item in items if suivi.aides == '']
            )
        if suivi.disponibilites == '':
            suivi.disponibilites = ''.join([
                '<strong>{}</strong></p><p></p>'.format(item) for item in ['Parents', 'Enfants']
                if suivi.disponibilites == ''
            ])
        return suivi

    def get_success_url(self):
        return reverse(f'{self.object.famille.typ}-demande', args=[self.object.famille.pk])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'famille': self.object.famille,
        })
        return context


class PersonneToggleContract(View):

    def post(self, request, *args, **kwargs):
        pers = get_object_or_404(Personne, pk=kwargs['pk'])
        if not pers.famille.can_edit(request.user):
            raise PermissionDenied()
        pers.contractant = not pers.contractant
        pers.save()
        url = reverse(f"{pers.famille.typ}-famille-edit", args=[pers.famille.pk])
        return HttpResponseRedirect(url)


class DerogationSaisieTardiveBase(PermissionRequiredMixin):
    permission_required = 'carrefour.add_utilisateur'
    model = DerogationSaisieTardive
    form_class = DerogationSaisieTardiveForm

    def dispatch(self, request, *args, **kwargs):
        self.educ = get_object_or_404(Utilisateur, pk=kwargs['pk'])
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('utilisateur-edit', args=[self.educ.pk])

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'educ': self.educ}


class DerogationSaisieTardiveCreateView(DerogationSaisieTardiveBase, CreateView):
    template_name = 'carrefour/derogation_change.html'


class DerogationSaisieTardiveUpdateView(DerogationSaisieTardiveBase, UpdateView):
    template_name = 'carrefour/derogation_change.html'
    pk_url_kwarg = 'obj_pk'

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(), 'instance': self.object
        }
