import os
from datetime import date, timedelta
from unittest.mock import patch

from django.conf.global_settings import PASSWORD_HASHERS
from django.contrib.auth.models import Group, Permission
from django.core.files import File
from django.db.utils import IntegrityError
from django.test import TestCase
from django.test.utils import override_settings
from django.urls import reverse
from django.utils import timezone

from aemo.models import FamilleAemo, JournalAemo, PrestationAemo
from asaef.models import FamilleAsaef, PrestationAsaef
from batoude.models import Beneficiaire
from carrefour.export import ExportReporting, openxml_contenttype
from carrefour.forms import ContactForm, DerogationSaisieTardiveForm, MonthSelectionForm, PersonneForm
from carrefour.models import (
    CercleScolaire, Contact, DerogationSaisieTardive, Document, Formation, LibellePrestation, Personne, Role, Service,
    Utilisateur,
)
from carrefour.utils import format_duree
from common.test_utils import TestUtilsMixin
from ser.models import Person


class InitialDataMixin(TestUtilsMixin):
    @classmethod
    def setUpTestData(cls):
        s1, _ = Service.objects.get_or_create(sigle='CARREFOUR')
        Service.objects.bulk_create([
            Service(sigle='OPEN'),
            Service(sigle='SSE')
        ])

        CercleScolaire.objects.bulk_create([
            CercleScolaire(nom='EOREN-MAIL'),
            CercleScolaire(nom='EOCF-NUMA-DROZ')
        ])

        Utilisateur.objects.create(nom='Champmard', prenom='Ben', service=s1)

        Role.objects.bulk_create([
            Role(nom='Père', famille=True),
            Role(nom='Mère', famille=True),
            Role(nom='Beau-père', famille=True),
            Role(nom='Enfant suivi', famille=True),
            Role(nom='Enfant non-suivi', famille=True),
            Role(nom='Médecin', famille=False),
        ])

        Contact.objects.bulk_create([
            Contact(nom='Sybarnez', prenom='Tina', service=s1),
            Contact(nom='Rathfeld', prenom='Christophe', service=Service.objects.get(sigle='OPEN')),
            Contact(nom='DrSpontz', prenom='Igor', role=Role.objects.get(nom='Médecin')),
        ])

        LibellePrestation.objects.bulk_create([
            LibellePrestation(unite='aemo', code='p01', nom='Prestation 1'),
            LibellePrestation(unite='aemo', code='p02', nom='Prestation 2'),
            LibellePrestation(unite='aemo', code='p03', nom='Prestation 3'),
            LibellePrestation(unite='ser', code='p04', nom='Prestation 4'),
            LibellePrestation(unite='batoude', code='p05', nom='Prestation 5 (avec le jeune)'),
            LibellePrestation(unite='batoude', code='p06', nom='Prestation 6 (sans le jeune)'),
            LibellePrestation(unite='batoude', code='p07', nom='Prestation 7'),
            LibellePrestation(unite='batoude', code='p08', nom='Prestation 8'),
            LibellePrestation(unite='asaef', code='p09', nom='Prestation 9 Étude'),
            LibellePrestation(unite='asaef', code='p10', nom='Prestation 10 Soutien'),
            LibellePrestation(unite='asaef', code='p11', nom='Prestation 11'),
            LibellePrestation(unite='asaef', code='p12', nom='Prestation 12'),
        ], ignore_conflicts=True)

        Group.objects.bulk_create([
            Group(name='aemo_littoral'),
            Group(name='aemo_montagnes'),
            Group(name='direction'),
            Group(name='asaef'),
            Group(name='batoude'),
            Group(name='asap'),
            Group(name='admin')
        ], ignore_conflicts=True)

        pere = Role.objects.get(nom='Père')

        famille = FamilleAemo.objects.create_famille(
            nom='Haddock', rue='Château1', npa=2000, localite='Moulinsart',
            equipe='montagnes', autorite_parentale='conjointe', statut_marital='divorce', monoparentale=False
        )
        famille_asaef = FamilleAsaef.objects.create_famille(
            nom='Tournesol', rue='Château1', npa=2000, localite='Moulinsart',
        )

        Personne.objects.create_personne(
            famille=famille, role=pere,
            nom='Haddock', prenom='Archibald', genre='M', date_naissance=date(1956, 2, 16),
            rue='Château1', npa=2000, localite='Moulinsart',
        )
        Personne.objects.create_personne(
            famille=famille_asaef, role=pere,
            nom='Hergé', prenom='Rémy', genre='M', date_naissance=date(1965, 4, 26),
            rue='Château1', npa=2000, localite='Moulinsart',
        )

        cls.user_admin = Utilisateur.objects.create_user(
             'admin', 'admin@example.org', first_name='Jean', last_name='Valjean',
        )
        cls.user_admin.user_permissions.add(
            *list(Permission.objects.filter(codename__in=[
                'export_aemo', 'add_utilisateur', 'change_utilisateur', 'delete_utilisateur',
                'add_role', 'delete_role', 'add_service', 'delete_contact',
            ]))
        )

        cls.user_aemo = Utilisateur.objects.create_user(
            'user_aemo', 'user_aemo@example.org', nom='Aemo', prenom='Prénom', sigle='AP'
        )
        cls.user_aemo.groups.add(Group.objects.get(name='aemo_montagnes'))
        cls.user_aemo.user_permissions.add(Permission.objects.get(codename='view_montagnes'))
        cls.user_aemo.user_permissions.add(Permission.objects.get(codename='change_montagnes'))
        cls.user_externe = Utilisateur.objects.create_user(
            'externe', 'externe@example.org', 'mepassword', first_name='Bruce', last_name='Batmann',
        )


class FamilleTests(InitialDataMixin, TestCase):
    personne_data = {
        'nom': 'Dupont',
        'prenom': 'Jean',
        'date_naissance': '1950-03-30',
        'genre': 'M',
        'contractant': True,
    }

    def setUp(self):
        self.client.force_login(self.user_aemo)

    def test_acces(self):
        """
        Sans permission adaptée, un utilisateur ne peut pas accéder à certaines
        pages (création de famille, etc.).
        """
        qqn = Utilisateur.objects.create_user(
            'qqun', 'qqun@example.org', 'pwd', first_name='Qqun', last_name='Personne',
        )
        self.client.force_login(qqn)
        with self.assertLogs('django.request', level='WARNING'):
            response = self.client.get(reverse('aemo-famille-add'))
        self.assertEqual(response.status_code, 403)

    def test_personne_creation(self):
        famille = FamilleAemo.objects.get(nom='Haddock')
        add_url = reverse('personne-add', args=[famille.typ, famille.pk])
        response = self.client.get(add_url)
        self.assertEqual(response.context['form'].initial['nom'], famille.nom)
        response = self.client.post(
            add_url,
            data=dict(self.personne_data, role=Role.objects.get(nom='Beau-père').pk)
        )
        self.assertEqual(response.status_code, 302)
        p1 = Personne.objects.get(nom='Dupont')
        self.assertEqual(p1.famille, famille)
        with self.assertRaises(Formation.DoesNotExist):
            p1.formation

        # Personne < 4 ans
        pers = Personne.objects.create_personne(
            famille=famille, prenom='Gaston', nom='Lagaffe',
            date_naissance=date.today() - timedelta(days=720),
            role=Role.objects.get(nom='Enfant suivi')
        )
        self.assertEqual(pers.formation.get_statut_display(), 'Pré-scolaire')
        self.assertTrue(famille.suiviaemo.demande_prioritaire)

    def test_label_profession_variable(self):
        famille = FamilleAemo.objects.get(nom='Haddock')
        response = self.client.get(
            '{}?role={}'.format(reverse('personne-add', args=[famille.typ, famille.pk]),
                                Role.objects.get(nom='Enfant suivi').pk)
        )
        self.assertContains(
            response, '<label for="id_profession" class="form-label">Profession/École:</label>', html=True
        )

        response = self.client.get(
            '{}?role={}'.format(reverse('personne-add', args=[famille.typ, famille.pk]),
                                Role.objects.get(nom='Père').pk)
        )
        self.assertContains(
            response, '<label for="id_profession" class="form-label">Profession:</label>', html=True
        )

    def test_pere_mere_unique(self):
        famille = FamilleAemo.objects.get(nom='Haddock')
        role_pere = Role.objects.get(nom='Père')
        role_mere = Role.objects.get(nom='Mère')
        archibald = famille.pere()

        response = self.client.get(reverse('personne-edit', args=[famille.typ, famille.pk, archibald.pk]))
        self.assertContains(response, '<option value="{}" selected>Père</option>'.format(role_pere.pk), html=True)

        response = self.client.get(
            '{}?role=ps'.format(reverse('personne-add', args=[famille.typ, famille.pk]))
        )
        self.assertNotContains(response, '<option value="{}">Père</option>'.format(role_pere.pk), html=True)

        pers_data = dict(self.personne_data, famille=famille.pk, role=Role.objects.get(nom='Père').pk)
        form = PersonneForm(data=pers_data, famille=famille)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['role'], ['Une personne a déjà le rôle de Père !'])

        # Une personne significative ne peut avoir un rôle Père, Mère, Enfant suivi, Enfant non-suivi
        response = self.client.get(
            '{}?role=ps'.format(reverse('personne-add', args=[famille.typ, famille.pk]))
        )
        for role in ['Père', 'Mère', 'Enfant suivi', 'Enfant non-suivi']:
            self.assertNotContains(
                response,
                '<option value="{}">{}</option>'.format(Role.objects.get(nom='Père').pk, role),
                html=True
            )

        Personne.objects.create_personne(
            famille=famille, role=Role.objects.get(nom='Mère'),
            nom='Haddock', prenom='Ursule', genre='F', date_naissance=date(1956, 2, 16)
        )
        form = PersonneForm(
            data={'nom': 'Haddock', 'prenom': 'Jeanne', 'role': Role.objects.get(nom='Mère').pk}, famille=famille
        )
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['role'], ['Une personne a déjà le rôle de Mère !'])

        ursule = Personne.objects.get(nom='Haddock', prenom='Ursule')
        response = self.client.get(reverse('personne-edit', args=[famille.typ, famille.pk, ursule.pk]))
        self.assertContains(response, '<option value="{}" selected>Mère</option>'.format(role_mere.pk), html=True)

    def test_personne_age(self):
        with patch('carrefour.models.date') as mock_date:
            mock_date.today.return_value = date(2019, 1, 16)
            self.assertEqual(Personne(date_naissance=date(1999, 11, 4)).age, 19.2)
            self.assertEqual(Personne(date_naissance=date(2000, 1, 1)).age, 19.0)
            self.assertEqual(Personne(date_naissance=date(2000, 1, 31)).age, 18.9)

    def test_personne_edition(self):
        famille = FamilleAemo.objects.create_famille(nom='Dupont', equipe='montagnes')
        pers_data = dict(self.personne_data, famille=famille.pk, role=Role.objects.get(nom='Père').pk)
        form = PersonneForm(data=pers_data, famille=famille)
        self.assertTrue(form.is_valid(), msg=form.errors)
        pers = form.save()
        edit_url = reverse('personne-edit', args=[famille.typ, famille.pk, pers.pk])
        new_data = dict(pers_data, rue='Rue du Parc 12')
        response = self.client.post(edit_url, data=new_data)
        self.assertEqual(response.status_code, 302)
        pers.refresh_from_db()
        self.assertEqual(pers.rue, 'Rue du Parc 12')
        # Edition is refused if user is missing the permission
        user = Utilisateur.objects.create_user(
            'joe', 'joe@example.org', 'pwd', first_name='Joe', last_name='Cook',
        )
        self.client.force_login(user)
        with self.assertLogs('django.request', level='WARNING'):
            response = self.client.get(edit_url)
        self.assertEqual(response.status_code, 403)

    def test_personne_formation(self):
        famille = FamilleAemo.objects.get(nom='Haddock')
        pers = Personne.objects.create_personne(
            famille=famille, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Enfant suivi')
        )
        form_url = reverse('formation', args=[pers.pk])
        response = self.client.post(form_url, data={
            'statut': 'cycle2', 'cercle_scolaire': CercleScolaire.objects.first().pk,
            'college': 'École parfaite', 'classe': '6H',
        })
        self.assertRedirects(response, reverse('aemo-famille-edit', args=[famille.pk]))
        pers.refresh_from_db()
        self.assertEqual(pers.formation.classe, '6H')

    def test_delete_personne_formation_impossible(self):
        pers = Personne.objects.create_personne(
            famille=FamilleAemo.objects.first(), prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Enfant suivi')
        )
        self.assertRaises(IntegrityError, pers.formation.delete)

    def test_delete_personne(self):
        famille = FamilleAemo.objects.first()
        pers = Personne.objects.create_personne(
            famille=famille, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Père')
        )
        response = self.client.post(
            reverse('personne-delete', args=[famille.typ, famille.pk, pers.pk]), follow=True
        )
        self.assertRedirects(response, reverse('aemo-famille-edit', args=[famille.pk]), status_code=302)

    def test_upload_document(self):
        famille = FamilleAemo.objects.first()
        with open(__file__, mode='rb') as fh:
            response = self.client.post(reverse('famille-doc-upload', args=[famille.pk]), data={
                'famille': famille.pk,
                'fichier': File(fh),
                'titre': 'Titre1',
            })
        self.assertRedirects(response, famille.suivi_url)
        self.assertEqual(famille.documents.count(), 1)
        doc = famille.documents.first()
        self.assertEqual(doc.auteur, self.user_aemo)
        self.assertEqual(doc.date_creation.date(), date.today())

        os.remove(doc.fichier.path)
        # Adding second file with same title gives a validation error
        with open(__file__, mode='rb') as fh:
            response = self.client.post(reverse('famille-doc-upload', args=[famille.pk]), data={
                'famille': famille.pk,
                'fichier': File(fh),
                'titre': 'Titre1',
            })
        self.assertEqual(
            response.context['form'].errors['__all__'],
            ['Un objet Document avec ces champs Famille et Titre existe déjà.']
        )

    def test_ajoute_contact_a_enfant_suivi(self):
        famille = FamilleAemo.objects.get(nom='Haddock')
        pers = Personne.objects.create_personne(
            famille=famille, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Enfant suivi')
        )
        new_contact = Contact.objects.get(nom='DrSpontz')
        self.client.force_login(self.user_externe)
        add_contact_url = reverse('personne-reseau-add', args=[pers.pk])
        response = self.client.get(add_contact_url)
        self.assertContains(
            response,
            f'href="{reverse("contact-add")}?forpers=personne-{pers.pk}"'
        )
        response = self.client.post(add_contact_url, data={'reseaux': new_contact.pk})
        self.assertRedirects(response, reverse('personne-reseau-list', args=[pers.pk]))
        self.assertEqual(pers.reseaux.count(), 1)

    def test_creation_contact_possible_pour_educ(self):
        famille = FamilleAemo.objects.get(nom='Haddock')
        pers = Personne.objects.create_personne(
            famille=famille, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Enfant suivi')
        )
        self.client.force_login(self.user_aemo)
        add_contact_url = f"{reverse('contact-add')}?forpers=personne-{pers.pk}"
        new_contact = {
            'nom': "ContactNom",
            'prenom': 'ContactPrenom'
        }
        response = self.client.post(add_contact_url, data=new_contact, follow=True)
        self.assertContains(response, "<td>ContactNom ContactPrenom</td>", html=True)
        self.assertEqual(pers.reseaux.count(), 1)

    def test_retirer_contact_du_reseau(self):
        famille = FamilleAemo.objects.get(nom='Haddock')
        pers = Personne.objects.create_personne(
            famille=famille, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Enfant suivi')
        )
        new_contact = Contact.objects.get(nom='DrSpontz')
        pers.reseaux.add(new_contact)
        self.assertEqual(pers.reseaux.count(), 1)

        remove_url = reverse('personne-reseau-remove', args=[pers.pk, new_contact.pk])
        response = self.client.post(remove_url, data={})
        self.assertEqual(response.status_code, 302)
        pers.refresh_from_db()
        self.assertEqual(pers.reseaux.count(), 0)

    def test_personne_contractant(self):
        famille = FamilleAemo.objects.get(nom='Haddock')
        pere = Personne.objects.get(famille=famille, prenom='Archibald', role=Role.objects.get(nom='Père'))
        self.assertTrue(pere.contractant)
        self.client.force_login(self.user_aemo)
        self.client.post(reverse('personne-toggle-contract', args=[pere.pk]))
        pere.refresh_from_db()
        self.assertFalse(pere.contractant)

    def test_personne_non_contractant(self):
        famille = FamilleAemo.objects.get(nom='Haddock')
        enfant = Personne.objects.create_personne(
            famille=famille, prenom='Junior', role=Role.objects.get(nom='Enfant suivi'))
        self.assertFalse(enfant.contractant)

    def test_anonymiser_famille(self):
        famille = FamilleAemo.objects.create_famille(nom='Doe', equipe='montagnes')
        Personne.objects.create_personne(
            nom='Doe', genre='M', role=Role.objects.get(nom='Père'), famille=famille
        )
        enf_suivi = Personne.objects.create_personne(
            nom='Doe', genre='M', role=Role.objects.get(nom='Enfant suivi'), famille=famille,
            date_naissance=date(2018, 1, 1)
        )

        famille.add_referent(Utilisateur.objects.get(nom='Champmard'))
        famille.suiviaemo.ope_referent = Contact.objects.get(nom='Rathfeld')
        famille.suiviaemo.date_demande = date.today()
        prest_fam = LibellePrestation.objects.get(code='p01')
        prest = PrestationAemo.objects.create(
            date_prestation=date.today(), duree=timedelta(hours=8), famille=famille, lib_prestation=prest_fam
        )
        prest.intervenants.add(Utilisateur.objects.get(nom='Champmard'))

        JournalAemo.objects.create(
            date=timezone.now(), theme='Un thème', texte='Un texte', famille=famille, auteur=self.user_aemo,
        )
        with open(__file__, mode='rb') as fh:
            Document.objects.create(
                famille=famille,
                fichier=File(fh, name='essai.txt'),
                titre='Essai'
            )
        self.assertTrue(FamilleAemo.objects.filter(pk=famille.pk).exists())
        self.assertTrue(Formation.objects.filter(personne=enf_suivi).exists())
        self.assertEqual(famille.documents.count(), 1)
        self.assertEqual(famille.membres.count(), 2)
        self.assertEqual(famille.prestations_aemo.count(), 1)
        self.assertEqual(famille.journaux_aemo.count(), 1)
        famille.anonymiser()
        self.assertTrue(FamilleAemo.objects.filter(pk=famille.pk).exists())
        self.assertTrue(Formation.objects.filter(personne=enf_suivi).exists())
        self.assertEqual(famille.documents.count(), 0)
        self.assertEqual(famille.membres.count(), 1)
        self.assertEqual(famille.prestations_aemo.count(), 1)
        self.assertEqual(famille.journaux_aemo.count(), 0)


class UtilisateurTests(InitialDataMixin, TestCase):
    @property
    def utilisateur_data(self):
        return {
            'nom': 'Muller',
            'prenom': 'Hans',
            'username': 'MullerH',
            'sigle': 'HM',
            'groups': [gr.pk for gr in Group.objects.all()[:2]]
        }

    def test_utilisateur_list(self):
        self.client.force_login(self.user_aemo)
        response = self.client.get(reverse('utilisateur-list'))
        self.assertEqual(len(response.context['object_list']), 4)

    def test_create_utilisateur_interdit(self):
        Utilisateur.objects.create_user(
            'joe', 'joe@example.org', 'pwd', first_name='Joe', last_name='Cook',
        )
        self.client.force_login(self.user_aemo)
        with self.assertLogs('django.request', level='WARNING'):
            response = self.client.post(reverse('utilisateur-add'), data=self.utilisateur_data)
        self.assertEqual(response.status_code, 403)

    def test_create_utilisateur_ok(self):
        self.client.force_login(self.user_admin)
        response = self.client.get(reverse('utilisateur-add'))
        self.assertTrue(response.status_code, 200)
        response = self.client.post(reverse('utilisateur-add'), data=self.utilisateur_data)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('utilisateur-list'))
        user = Utilisateur.objects.get(nom='Muller')
        self.assertEqual(user.service.sigle, 'CARREFOUR')
        self.assertEqual(user.username, 'MullerH')
        self.assertEqual(user.groups.count(), 2)

    def test_delete_utilisateur_interdit(self):
        qqn = Utilisateur.objects.create_user(username='user1', password='mepassword', nom='Supprimer')
        self.client.force_login(self.user_aemo)
        with self.assertLogs('django.request', level='WARNING'):
            response = self.client.post(reverse('utilisateur-delete', args=[qqn.pk]))
        self.assertEqual(response.status_code, 403)

    def test_delete_utilisateur_ok(self):
        qqn = Utilisateur.objects.create_user(username='user1', password='mepassword', nom='Supprimer')
        self.client.force_login(self.user_admin)
        response = self.client.post(reverse('utilisateur-delete', args=[qqn.pk]))
        self.assertRedirects(response, reverse('utilisateur-list'))
        qqn.refresh_from_db()
        self.assertIsNotNone(qqn.date_desactivation)
        self.assertFalse(qqn.is_active)
        self.assertFalse(qqn.est_actif)

        response = self.client.get(reverse('utilisateur-autocomplete') + '?q=Sup')
        self.assertEqual(response.json()['results'], [])

    def test_affiche_utilisateur_inactif(self):
        user = Utilisateur.objects.create_user(username='user', nom="Spontz")
        self.client.force_login(self.user_admin)
        self.client.post(reverse('utilisateur-delete', args=[user.pk]))
        response = self.client.get(reverse('utilisateur-desactive-list'))

        self.assertEqual(response.context['object_list'][0], user)
        self.assertContains(
            response,
            '<button class="btn btn-sm btn-outline-success" type="submit">Ré-activer</button>',
            html=True
        )

    def test_reactivation(self):
        user = Utilisateur.objects.create_user(username='user', nom="Spontz")
        user.is_active = False
        user.est_actif = False
        user.date_desactivation = date.today()
        user.save()
        pwd = user.password

        self.client.force_login(self.user_admin)
        self.client.post(reverse('utilisateur-reactiver', args=[user.pk]))
        user.refresh_from_db()
        self.assertTrue(user.is_active)
        self.assertTrue(user.est_actif)
        self.assertIsNone(user.date_desactivation)
        self.assertNotEqual(pwd, user.password)

    def test_utilisateurs_meme_equipe(self):
        user_both = Utilisateur.objects.create_user(username='user1', nom='Chef')
        user_both.groups.add(Group.objects.get(name='aemo_littoral'))
        user_both.groups.add(Group.objects.get(name='aemo_montagnes'))
        user_litt = Utilisateur.objects.create_user(username='user2', nom='Litt')
        user_litt.groups.add(Group.objects.get(name='aemo_littoral'))
        # Aemo montagnes user already exists.
        self.assertQuerySetEqual(
            user_both.utilisateurs_meme_equipe(),
            [self.user_aemo, user_both, user_litt]
        )
        self.assertQuerySetEqual(
            user_litt.utilisateurs_meme_equipe(),
            [user_both, user_litt]
        )

    def test_intervenants_aemo(self):
        user_both = Utilisateur.objects.create_user(username='user1', nom='Chef')
        user_both.groups.add(Group.objects.get(name='aemo_littoral'))
        user_both.groups.add(Group.objects.get(name='aemo_montagnes'))
        self.assertEqual(Utilisateur.intervenants_aemo().get(pk=user_both.pk), user_both)

    @override_settings(PASSWORD_HASHERS=PASSWORD_HASHERS)
    def test_reinit_password(self):
        user = Utilisateur.objects.create_user(username='user')
        self.assertTrue(user.password.startswith('!'))  # Unusable password
        self.client.force_login(self.user_admin)
        self.client.post(reverse('utilisateur-password-reinit', args=[user.pk]))
        user.refresh_from_db()
        self.assertTrue(user.password.startswith('pbkdf2_sha256$'))  # Usable password

    def test_reinit_mobile(self):
        from django_otp.plugins.otp_totp.models import TOTPDevice
        user = Utilisateur.objects.create_user(username='user', nom="Spontz")
        TOTPDevice.objects.create(user=user, name='default')
        TOTPDevice.objects.create(user=user, name='suppl')
        self.client.force_login(self.user_admin)
        response = self.client.post(
            reverse('utilisateur-otp-device-reinit', args=[user.pk]), follow=True)
        self.assertEqual(str(list(response.context['messages'])[0]),
                         'Tous les mobiles de «Spontz » ont été réinitialisés.')
        response = self.client.post(
            reverse('utilisateur-otp-device-reinit', args=[user.pk]), follow=True)
        self.assertEqual(str(list(response.context['messages'])[0]),
                         'Aucune configuration mobile trouvée pour «Spontz ».')

    def test_affichage_dossiers_pour_aemo(self):
        famille = FamilleAemo.objects.create_famille(
            nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart', equipe='montagnes')
        famille.suiviaemo.date_demande = '2020-01-01'
        famille.suiviaemo.save()
        util1 = Utilisateur.objects.create_user(username='util1', nom='Util1', prenom='Pierre')
        util1.groups.add(Group.objects.get(name='aemo_montagnes'))
        famille.add_referent(util1, debut=famille.suiviaemo.date_demande)

        self.client.force_login(self.user_admin)
        response = self.client.get(reverse('utilisateur-edit', args=[util1.pk]))
        edit_url = reverse('aemo-famille-suivi', args=[famille.pk])
        self.assertContains(response, f'<a href="{edit_url}">Simpson</a>', html=True)


class OtherTests(InitialDataMixin, TestCase):

    def test_service_creation(self):
        self.client.force_login(self.user_admin)
        s1 = Service.objects.create(sigle='lower')  # transform code from lower to uppercase
        self.assertEqual(s1.sigle, 'LOWER')
        # By form
        response = self.client.post(
            reverse('service-add'), data={'sigle': 'SERVICE', 'nom_complet': 'Super service'}
        )
        self.assertRedirects(response, reverse('service-list'))
        self.assertEqual(Service.objects.filter(sigle='SERVICE').count(), 1)

    def test_service_list(self):
        self.client.force_login(self.user_aemo)
        response = self.client.get(reverse('service-list'))
        self.assertEqual(len(response.context['object_list']), 3)

    def test_raise_unique_constraint_school_center_creation(self):
        sc = CercleScolaire(nom='EOREN-MAIL')
        self.assertRaises(IntegrityError, sc.save)

    def test_cerclescolaire_list(self):
        self.client.force_login(self.user_aemo)
        response = self.client.get(reverse('cercle-list'))
        self.assertEqual(len(response.context['object_list']), 2)

    def test_role_create(self):
        self.client.force_login(self.user_admin)
        response = self.client.post(reverse('role-add'), data={'nom': 'ROLE1', 'famille': False})
        self.assertRedirects(response, reverse('role-list'))
        self.assertEqual(Role.objects.filter(nom='ROLE1').count(), 1)

    def test_role_list(self):
        self.client.force_login(self.user_externe)
        response = self.client.get(reverse('role-list'))
        self.assertEqual(len(response.context['object_list']), 6)
        # Les rôles "protégés" ne sont pas éditables.
        self.assertContains(response, '<tr><td>Enfant suivi</td><td>oui</td></tr>', html=True)

    def test_delete_used_role(self):
        """A role with at least one attached Personne cannot be deleted."""
        self.client.force_login(self.user_admin)
        role = Role.objects.get(nom="Père")
        self.assertGreater(role.personne_set.count(), 0)
        response = self.client.post(reverse('role-delete', args=[role.pk]), follow=True)
        self.assertContains(response, "Cannot delete")

    def test_truncate_with_more_ttag(self):
        from carrefour.templatetags.my_tags import truncate_with_more_and_default
        txt = '<p>Ceci <b>est un très long</b> texte HTML.<br> Seconde ligne</p>'
        self.assertHTMLEqual(
            truncate_with_more_and_default(txt, 3),
            '<div class="long" style="display:none"><p>Ceci <b>est un très long</b> texte HTML.<br>'
            ' Seconde ligne</p></div>'
            '<div class="short"><p>Ceci <b>est un…</b></p></div>'
            '<a class="read_more" href=".">Afficher la suite</a>'
        )

        txt = "L'oiseau s'est envolé.\n Vraiment envolé! Comme 2 > 1."
        self.assertHTMLEqual(
            truncate_with_more_and_default(txt, 3),
            '<div class="long" style="display:none">L&#x27;oiseau s&#x27;est envolé.<br>'
            ' Vraiment envolé! Comme 2 > 1.</div>'
            '<div class="short">L&#x27;oiseau s&#x27;est envolé.<br>…</div>'
            '<a class="read_more" href=".">Afficher la suite</a>'
        )
        self.assertIn("Comme 2 &gt; 1", truncate_with_more_and_default("Comme 2 > 1", 3))


class TestExporter(ExportReporting):
    """A fake exporter class that just collect data in lists to be able to assert the contents."""
    def __init__(self):
        super().__init__()
        self.sheets = {}
        self._current_sh = None

    def __call__(self):
        # This allows to provide an instance to mock
        return self

    def setup_sheet(self, title):
        self.sheets[title] = []
        self._current_sh = self.sheets[title]

    def write_line(self, values, **kwargs):
        self._current_sh.append(values)


class StatTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = Utilisateur.objects.create(
            username='me', first_name='Jean', last_name='Valjean',
        )
        user_haut = Utilisateur.objects.create(username='ld', prenom='Lise', nom="Duhaut")
        user_haut.user_permissions.add(Permission.objects.get(codename='view_montagnes'))
        user_bas = Utilisateur.objects.create(username='jd', prenom='Jean', nom="Dubas")
        user_bas.user_permissions.add(Permission.objects.get(codename='view_littoral'))

        user_asaef = Utilisateur.objects.create(username='pa', prenom='Paul', nom="Asaef")
        user_asaef.user_permissions.add(Permission.objects.get(codename='view_asaef'))

        Group.objects.bulk_create([
            Group(name='aemo_littoral'),
            Group(name='aemo_montagnes'),
            Group(name='asaef'),
        ], ignore_conflicts=True)

        cls.enf_suivi = Role.objects.create(nom='Enfant suivi', famille=True)
        cls.role_pere = Role.objects.create(nom='Père', famille=True)
        cls.role_mere = Role.objects.create(nom='Mère', famille=True)
        cls.famille_litt = FamilleAemo.objects.create_famille(
            equipe='littoral',
            nom='Haddock', rue='Château1', npa=2000, localite='Moulinsart',
        )
        Personne.objects.create_personne(
            famille=cls.famille_litt, role=cls.enf_suivi,
            nom='Haddock', prenom='Archibald', genre='M', date_naissance=date(1996, 2, 16)
        )
        Personne.objects.create_personne(
            famille=cls.famille_litt, role=cls.enf_suivi,
            nom='Haddock', prenom='Honorine', genre='F', date_naissance=date(1996, 2, 17)
        )
        cls.famille_litt.suiviaemo.date_demande = '2019-01-01'
        cls.famille_litt.suiviaemo.save()
        cls.famille_litt.add_referent(user_bas)

        cls.famille_mont = FamilleAemo.objects.create_famille(
            equipe='montagnes',
            nom='Tournesol', rue='Château1', npa=2000, localite='Moulinsart',
        )
        Personne.objects.create_personne(
            famille=cls.famille_mont, role=cls.enf_suivi,
            nom='Tournesol', prenom='Tryphon', genre='M', date_naissance=date(1991, 1, 3)
        )
        cls.famille_mont.suiviaemo.date_demande = '2019-01-01'
        cls.famille_mont.suiviaemo.save()
        cls.famille_litt.add_referent(user_haut)

        cls.famille_asaef = FamilleAsaef.objects.create_famille(
            nom='Castafiore', rue='Château1', npa=2000, localite='Moulinsart',
        )

    def test_temps_total_prestations(self):
        self.user.user_permissions.add(Permission.objects.get(codename='view_montagnes'))
        my_group = Group.objects.get(name='aemo_littoral')
        my_group.user_set.add(self.user)
        self.assertEqual(self.famille_litt.temps_total_prestations(), timedelta(0))
        auj = date.today()
        mois_sui1 = date(auj.year, auj.month, 3)
        mois_sui2 = date(auj.year, auj.month, 4)
        user2 = Utilisateur.objects.create(
            username='you', first_name='Hans', last_name='Zwei',
        )
        user2.user_permissions.add(Permission.objects.get(codename='view_montagnes'))
        my_group = Group.objects.get(name='aemo_littoral')
        my_group.user_set.add(user2)
        p1 = PrestationAemo.objects.create(
            famille=self.famille_litt,
            date_prestation=auj,
            duree='0:45'
        )
        p1.intervenants.set([self.user])
        p2 = PrestationAemo.objects.create(
            famille=self.famille_litt,
            date_prestation=mois_sui1,
            duree='1:0'
        )
        #  Chaque intervenant AEMO saisit ses propres prestations
        p2.intervenants.set([self.user])
        p3 = PrestationAemo.objects.create(
            famille=self.famille_litt,
            date_prestation=mois_sui2,
            duree='1:05'
        )
        p3.intervenants.set([user2])

        # Avec ce processus, la même prestation peut avoir des durées différentes!!!

        # p1 (00:45) + 2 interv. x p2 (1:00) >> p1(00:45) + p2(1:00) + p3(1:05) = 2:50
        self.assertEqual(self.famille_litt.temps_total_prestations(), timedelta(hours=2, minutes=50))
        self.assertEqual(self.famille_litt.temps_total_prestations_reparti(), timedelta(hours=1, minutes=25))
        # self.user = p1 (00:45) + p2 (1:00) = 1:45
        # user2 = p3(1:05)
        self.assertEqual(self.user.temps_total_prestations('aemo'), '01:45')
        self.assertEqual(user2.temps_total_prestations('aemo'), '01:05')

    def test_aemo_stats(self):
        auj = date.today()
        mois_sui1 = date(auj.year, auj.month, 3)
        user_bas = Utilisateur.objects.get(nom='Dubas')
        manager_bas = Utilisateur.objects.create(username='boss', nom='Boss')
        manager_bas.user_permissions.add(Permission.objects.get(codename='manage_littoral'))
        p = PrestationAemo.objects.create(
            famille=self.famille_litt,
            date_prestation=auj,
            duree='0:45'
        )
        p.intervenants.set([user_bas])
        p = PrestationAemo.objects.create(
            famille=self.famille_litt,
            date_prestation=mois_sui1,
            duree='1:00'
        )
        p.intervenants.set([user_bas])
        # Cette famille est du littoral, mais n'a pas Lise Dubas comme référente
        FamilleAemo.objects.create_famille(
            equipe='littoral',
            nom='Tintin', rue='Château1', npa=2000, localite='Moulinsart',
        )

    def test_export_stats_aemo(self):
        self.client.force_login(self.user)
        # Test denied access without export_aemo permission
        with self.assertLogs('django.request', level='WARNING'):
            response = self.client.get(reverse('export-prestation'))
        self.assertEqual(response.status_code, 403)

        self.user.user_permissions.add(Permission.objects.get(codename='export_aemo'))
        # Test default selected options in export form
        with patch('carrefour.views.date') as mock_date:
            mock_date.today.return_value = date(2019, 7, 3)
            response = self.client.get(reverse('export-prestation'))
            self.assertContains(
                response,
                '<option value="6" selected>juin</option>'
            )
            self.assertContains(
                response,
                '<option value="2019" selected>2019</option>'
            )

            mock_date.today.return_value = date(2020, 1, 30)
            response = self.client.get(reverse('export-prestation'))
            self.assertContains(
                response,
                '<option value="12" selected>décembre</option>'
            )
            self.assertContains(
                response,
                '<option value="2019" selected>2019</option>'
            )

        response = self.client.post(reverse('export-prestation'), data={'mois': '2', 'annee': '2019'})
        self.assertEqual(response['Content-Type'], openxml_contenttype)
        self.assertEqual(
            response['Content-Disposition'],
            'attachment; filename="carrefour_reporting_02_2019.xlsx"'
        )

    def test_openxml_export_sheets(self):

        # "Nouvelle" famille
        famille = FamilleAemo.objects.create_famille(
            equipe='littoral',
            nom='NouvelleFamille', rue='Château 4', npa=2000, localite='Moulinsart',
        )
        famille.suiviaemo.date_demande = '2019-02-10'
        famille.suiviaemo.date_fin_suivi = '2019-05-30'
        famille.suiviaemo.save()
        Personne.objects.create_personne(
            famille=famille, role=self.enf_suivi,
            nom='NouvelleFamille', prenom='Hégésipe', genre='M', date_naissance=date(1996, 2, 16)
        )

        # Famille "en cours"
        famille = FamilleAemo.objects.create_famille(
            equipe='littoral',
            nom='FamilleEnCours', rue='Château 5', npa=2000, localite='Moulinsart',
            autorite_parentale='conjointe', statut_marital='divorce', monoparentale=False,
        )
        famille.suiviaemo.date_demande = '2019-01-01'
        famille.suiviaemo.date_debut_suivi = '2019-01-28'
        ope_service = Service.objects.create(sigle="OPEC")
        famille.suiviaemo.ope_referent = Contact.objects.create(
            nom="Duplain", prenom="Irma", service=ope_service
        )
        famille.suiviaemo.save()
        Personne.objects.create_personne(
            famille=famille, role=self.enf_suivi,
            nom='FamilleEnCours', prenom='Alain', genre='M', date_naissance=date(2003, 4, 23),
            localite='Moulinsart',
        )
        Personne.objects.create_personne(
            famille=famille, role=self.role_mere,
            nom='FamilleEnCours', prenom='Judith', genre='F', date_naissance=date(1974, 11, 2)
        )
        Personne.objects.create_personne(
            famille=famille, role=self.role_pere,
            nom='NomDuPere', prenom='Hans', genre='M', date_naissance=date(1968, 3, 13)
        )

        # Famille "terminée"
        famille = FamilleAsaef.objects.create_famille(
            nom='FamilleTerminée', rue='Château 6', npa=2000, localite='Moulinsart',
        )
        famille.suiviasaef.date_demande = '2018-10-02'
        famille.suiviasaef.date_debut_suivi = '2018-11-11'
        famille.suiviasaef.date_fin_suivi = '2019-02-20'
        famille.suiviasaef.motif_fin_suivi = 'relai'
        famille.suiviasaef.save()
        Personne.objects.create_personne(
            famille=famille, role=self.enf_suivi,
            nom='FamilleTerminée', prenom='Jeanne', genre='F', date_naissance=date(1998, 12, 14)
        )

        self.user.user_permissions.add(Permission.objects.get(codename='export_aemo'))
        self.client.force_login(self.user)
        exp = TestExporter()
        with patch('carrefour.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '2', 'annee': '2019'})

        self.assertEqual(len(exp.sheets['En_cours_02.2019']), 7)
        self.assertEqual(
            exp.sheets['En_cours_02.2019'][0],
            ['Institution', 'Prestation', 'Nom', 'Prenom', 'Genre', 'Date de naissance', 'Adresse',
             'NPA', 'Localité', 'Canton', 'OPE', 'Nom mère', 'Prénom mère', 'Nom père', 'Prénom père',
             'Autorité parentale', 'Statut marital', 'Statut financier', 'Fam. monopar.', 'Nbre enfants',
             'H. de suivi', 'Prest. gén.', 'Jours facturés']
        )
        self.assertEqual(
            exp.sheets['En_cours_02.2019'][1],
            ['Carrefour', 'AEMO', 'FamilleEnCours', 'Alain', 'M', '23.04.2003', '', '', 'Moulinsart', 'NE',
             'Duplain Irma', 'FamilleEnCours', 'Judith', 'NomDuPere', 'Hans', 'Conjointe', 'Divorcé', '',
             'NON', 1,
             timedelta(0), timedelta(0), '']
        )
        self.assertEqual(
            [(line[2], line[3]) for line in exp.sheets['En_cours_02.2019'][1:]],
            [('FamilleEnCours', 'Alain'), ('Haddock', 'Honorine'), ('Haddock', 'Archibald'),
             ('NouvelleFamille', 'Hégésipe'), ('Tournesol', 'Tryphon'), ('FamilleTerminée', 'Jeanne')]
        )

        self.assertEqual(len(exp.sheets['Nouveaux_02.2019']), 2)
        self.assertEqual(
            exp.sheets['Nouveaux_02.2019'][0],
            ['Institution', 'Prestation', 'Nom', 'Prenom', 'Genre', 'Date de naissance', 'Adresse',
             'NPA', 'Localité', 'Canton', 'OPE', 'Nom mère', 'Prénom mère', 'Nom père', 'Prénom père',
             'Autorité parentale', 'Statut marital', 'Statut financier', 'Fam. monopar.', 'Nbre enfants',
             'Date demande', 'Motif']
        )
        self.assertEqual(
            exp.sheets['Nouveaux_02.2019'][1],
            ['Carrefour', 'AEMO', 'NouvelleFamille', 'Hégésipe', 'M', '16.02.1996', '', '', '', 'NE',
             '', '', '', '', '', '', '', '', '', 1,
             '10.02.2019', '-'
             ]
        )
        self.assertEqual(len(exp.sheets['Terminés_02.2019']), 2)
        self.assertEqual(
            exp.sheets['Terminés_02.2019'][0],
            ['Institution', 'Prestation', 'Nom', 'Prenom', 'Genre', 'Date de naissance', 'Adresse',
             'NPA', 'Localité', 'Canton', 'OPE', 'Nom mère', 'Prénom mère', 'Nom père', 'Prénom père',
             'Autorité parentale', 'Statut marital', 'Statut financier', 'Fam. monopar.', 'Nbre enfants',
             'Date entrée', 'Date sortie', 'Total heures', 'Motif de fin', 'Destination']
        )
        self.assertEqual(
            exp.sheets['Terminés_02.2019'][1],
            ['Carrefour', 'ASAEF', 'FamilleTerminée', 'Jeanne', 'F', '14.12.1998', '', '', '', 'NE',
             '', '', '', '', '', '', '', '', '', 1,
             '11.11.2018', '20.02.2019', timedelta(0), 'Relai vers autre service', '']
        )

        famille = FamilleAemo.objects.get(nom='NouvelleFamille')
        famille.suiviaemo.motif_fin_suivi = 'erreur'
        famille.suiviaemo.date_fin_suivi = date(2019, 2, 17)
        famille.suiviaemo.save()

        famille = FamilleAsaef.objects.get(nom='FamilleTerminée')
        famille.suiviasaef.motif_fin_suivi = 'erreur'
        famille.suiviasaef.date_fin_suivi = date(2019, 2, 17)
        famille.suiviasaef.save()

        exp = TestExporter()
        with patch('carrefour.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '2', 'annee': '2019'})
        # Plus de famille, puisque les motifs de fin de suivi "erreur" ne sont pas pris en compte
        self.assertEqual(len(exp.sheets['Nouveaux_02.2019']), 1)
        self.assertEqual(len(exp.sheets['Terminés_02.2019']), 1)
        # 'NouvelleFamille' plus exportée
        self.assertEqual(len(exp.sheets['En_cours_02.2019']), 5)

    def test_total_mensuel(self):
        famille = FamilleAemo.objects.get(nom='Haddock')
        auj = date.today()
        mois_suiv = auj + timedelta(days=31)
        self.assertEqual(famille.total_mensuel(auj), timedelta(0))
        PrestationAemo.objects.bulk_create([
            PrestationAemo(famille=famille, date_prestation=date(auj.year, auj.month, 1), duree='1:30'),
            PrestationAemo(famille=famille, date_prestation=date(auj.year, auj.month, 25), duree='0:15'),
            # Not included in this month
            PrestationAemo(famille=famille, date_prestation=date(mois_suiv.year, mois_suiv.month, 1), duree='0:15'),
        ])
        self.assertEqual(famille.total_mensuel(auj), timedelta(minutes=105))

    def test_total_prestations_aemo(self):
        famille = FamilleAemo.objects.get(nom='Haddock')
        user = Utilisateur.objects.get(nom='Duhaut')
        my_group = Group.objects.get(name='aemo_littoral')
        my_group.user_set.add(user)
        PrestationAemo.objects.bulk_create([
            PrestationAemo(famille=famille, date_prestation=date(2019, 3, 1), duree='3:40'),
            PrestationAemo(famille=famille, date_prestation=date(2019, 3, 19), duree='6:40'),
            # Not included in this month
            PrestationAemo(famille=famille, date_prestation=date(2019, 4, 1), duree='2:00'),
        ])
        for p in PrestationAemo.objects.filter(famille=famille):
            p.intervenants.set([user])

        self.assertEqual(user.total_mensuel('aemo', 3, 2019), '10:20')
        self.assertEqual(user.totaux_mensuels('aemo', 2019)[3], '02:00')
        self.assertEqual(user.total_annuel('aemo', 2019), '12:20')
        self.assertEqual(PrestationAemo.temps_totaux_mensuels(2019)[2], '10:20')
        self.assertEqual(PrestationAemo.temps_totaux_mensuels(2019)[3], '02:00')
        self.assertEqual(PrestationAemo.temps_total_general(2019), '12:20')

        user.user_permissions.add(Permission.objects.get(codename='export_aemo'))
        self.client.force_login(user)
        exp = TestExporter()
        with patch('carrefour.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '3', 'annee': '2019'})
        self.assertEqual(format_duree(exp._total_aemo), '10:20')

    def test_total_prestations_asaef(self):
        famille = FamilleAsaef.objects.get(nom='Castafiore')
        Personne.objects.create_personne(
            famille=famille, role=self.enf_suivi,
            nom='FamilleEnCours', prenom='Alain', genre='M', date_naissance=date(2003, 4, 23),
            localite='Moulinsart',
        )
        famille.suiviasaef.date_demande = '2019-02-01'
        famille.suiviasaef.save()
        user = Utilisateur.objects.get(nom='Asaef')
        my_group = Group.objects.get(name='asaef')
        my_group.user_set.add(user)
        famille.suiviasaef.intervenants_asaef.add(user)

        PrestationAsaef.objects.bulk_create([
            PrestationAsaef(famille=famille, date_prestation=date(2019, 3, 1), duree='3:40', intervenant=user),
            PrestationAsaef(famille=famille, date_prestation=date(2019, 3, 19), duree='6:40', intervenant=user),
            PrestationAsaef(famille=famille, date_prestation=date(2019, 4, 1), duree='2:00', intervenant=user),
            # Prestations générales
            PrestationAsaef(famille=None, date_prestation=date(2019, 4, 8), duree='1:30', familles_actives=1),
            PrestationAsaef(famille=None, date_prestation=date(2019, 4, 29), duree='2:30', familles_actives=2),
            PrestationAsaef(famille=None, date_prestation=date(2019, 5, 10), duree='2:30', familles_actives=2),
        ])

        self.assertEqual(user.total_mensuel('asaef', 3, 2019), '10:20')
        self.assertEqual(user.total_mensuel('asaef', 4, 2019), '02:00')
        self.assertEqual(user.totaux_mensuels('asaef', 2019)[3], '02:00')
        self.assertEqual(user.total_annuel('asaef', 2019), '12:20')

        self.assertEqual(PrestationAsaef.temps_totaux_mensuels(2019)[2], '10:20')
        self.assertEqual(PrestationAsaef.temps_totaux_mensuels(2019)[3], '02:00')
        self.assertEqual(PrestationAsaef.temps_total_general(2019), '12:20')
        self.assertEqual(format_duree(famille.total_mensuel_prest_gen(date(2019, 3, 1))), '00:00')
        self.assertEqual(format_duree(famille.total_mensuel_prest_gen(date(2019, 4, 1))), '02:45')
        self.assertEqual(format_duree(famille.total_mensuel_prest_gen(date(2019, 5, 1))), '01:15')

        user.user_permissions.add(Permission.objects.get(codename='export_aemo'))
        self.client.force_login(user)
        exp = TestExporter()
        with patch('carrefour.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '4', 'annee': '2019'})
        self.assertEqual(format_duree(exp._total_asaef), '02:00')
        self.assertEqual(format_duree(exp._total_gen_asaef), '02:45')

    def test_export_prestations_decembre(self):
        self.user.user_permissions.add(Permission.objects.get(codename='export_aemo'))
        self.client.force_login(self.user)
        response = self.client.post(reverse('export-prestation'), {'mois': '12', 'annee': '2019'})
        self.assertEqual(response.status_code, 200)


class ExportTests(InitialDataMixin, TestCase):

    def setUp(self):
        self.client.force_login(self.user_admin)
        self.cfg_date = date(2019, 10, 3)
        self.cfg_sheet = 'En_cours_{}.{}'.format(self.cfg_date.month, self.cfg_date.year)
        self.cfg_export_month = {'mois': self.cfg_date.month, 'annee': self.cfg_date.year}

    def create_famille_aemo(self, name='Haddock'):
        famille = FamilleAemo.objects.create_famille(
            nom=name, rue='Château1', npa=2000, localite='Moulinsart',
            equipe='montagnes', autorite_parentale='conjointe', statut_marital='divorce', monoparentale=False
        )
        pere = Role.objects.get(nom='Père')
        Personne.objects.create_personne(
            famille=famille, role=pere,
            nom=name, prenom='Archibald', genre='M', date_naissance=date(1956, 2, 16),
            rue='Château1', npa=2000, localite='Moulinsart',
        )
        famille.suiviaemo.ope_referent = Contact.objects.get(nom="Sybarnez")
        famille.suiviaemo.save()
        Personne.objects.create_personne(
            famille=famille, role=Role.objects.get(nom='Enfant suivi'),
            nom=name, prenom='Toto', genre='M', date_naissance=date(2010, 2, 16),
            rue='Château1', npa=2000, localite='Moulinsart',
        )

        return famille

    def test_entete_nouveux_suivis(self):
        exp = TestExporter()
        with patch('carrefour.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '10', 'annee': '2019'})
        sheet_name = 'Nouveaux_10.2019'
        self.assertEqual(len(exp.sheets[sheet_name]), 1)
        self.assertEqual(
            exp.sheets[sheet_name][0],
            ['Institution', 'Prestation', 'Nom', 'Prenom', 'Genre', 'Date de naissance', 'Adresse', 'NPA', 'Localité',
             'Canton', 'OPE', 'Nom mère', 'Prénom mère', 'Nom père', 'Prénom père', 'Autorité parentale',
             'Statut marital', 'Statut financier', 'Fam. monopar.', 'Nbre enfants',
             'Date demande', 'Motif']
        )

    def test_entete_suivis_en_cours(self):
        exp = TestExporter()
        with patch('carrefour.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '10', 'annee': '2019'})
        sheet_name = 'En_cours_10.2019'
        self.assertEqual(len(exp.sheets[sheet_name]), 1)
        self.assertEqual(
            exp.sheets[sheet_name][0],
            ['Institution', 'Prestation', 'Nom', 'Prenom', 'Genre', 'Date de naissance', 'Adresse', 'NPA', 'Localité',
             'Canton', 'OPE', 'Nom mère', 'Prénom mère', 'Nom père', 'Prénom père', 'Autorité parentale',
             'Statut marital', 'Statut financier', 'Fam. monopar.', 'Nbre enfants',
             'H. de suivi', 'Prest. gén.', 'Jours facturés']
        )

    def test_entete_suivis_termines(self):
        exp = TestExporter()
        with patch('carrefour.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '10', 'annee': '2019'})
        sheet_name = 'Terminés_10.2019'
        self.assertEqual(len(exp.sheets[sheet_name]), 1)
        self.assertEqual(
            exp.sheets[sheet_name][0],
            ['Institution', 'Prestation', 'Nom', 'Prenom', 'Genre', 'Date de naissance', 'Adresse', 'NPA', 'Localité',
             'Canton', 'OPE', 'Nom mère', 'Prénom mère', 'Nom père', 'Prénom père', 'Autorité parentale',
             'Statut marital', 'Statut financier', 'Fam. monopar.', 'Nbre enfants',
             'Date entrée', 'Date sortie', 'Total heures', 'Motif de fin', 'Destination']
        )

    def test_date_nouveau_suivi(self):
        fam1 = self.create_famille_aemo(name='Haddock')
        fam1.suiviaemo.date_demande = '2019-09-20'
        fam1.suiviaemo.save()

        fam2 = self.create_famille_aemo(name='Tournesol')
        fam2.suiviaemo.date_demande = '2019-10-20'
        fam2.suiviaemo.save()

        fam3 = self.create_famille_aemo(name='Castafiore')
        fam3.suiviaemo.date_demande = '2019-11-20'
        fam3.suiviaemo.save()

        exp = TestExporter()
        with patch('carrefour.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '10', 'annee': '2019'})
        sheet_name = 'Nouveaux_10.2019'
        self.assertEqual(len(exp.sheets[sheet_name]), 2)
        self.assertEqual([line[2] for line in exp.sheets[sheet_name]], ['Nom', 'Tournesol'])

    def test_date_suivi_en_cours(self):
        fam1 = self.create_famille_aemo(name='Haddock')
        fam1.suiviaemo.date_demande = '2019-09-20'
        fam1.suiviaemo.save()

        fam2 = self.create_famille_aemo(name='Tournesol')
        fam2.suiviaemo.date_demande = '2019-10-20'
        fam2.suiviaemo.save()

        fam3 = self.create_famille_aemo(name='Castafiore')
        fam3.suiviaemo.date_demande = '2019-11-20'
        fam3.suiviaemo.save()

        self.benef1 = dict(nom='Haddock', date_admission='2019-10-02')
        self.benef2 = dict(nom='Tournesol', date_admission='2019-09-30')

    def test_date_suivi_termine(self):
        fam1 = self.create_famille_aemo(name='Haddock')
        fam1.suiviaemo.date_demande = '2019-09-20'
        fam1.suiviaemo.date_fin_suivi = '2019-10-31'
        fam1.suiviaemo.save()

        fam2 = self.create_famille_aemo(name='Tournesol')
        fam2.suiviaemo.date_demande = '2019-10-20'
        fam2.suiviaemo.save()

        fam3 = self.create_famille_aemo(name='Castafiore')
        fam3.suiviaemo.date_demande = '2019-11-20'
        fam3.suiviaemo.date_fin_suivi = '2019-12-31'
        fam3.suiviaemo.save()

        exp = TestExporter()
        with patch('carrefour.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '10', 'annee': '2019'})
        sheet_name = 'Terminés_10.2019'
        self.assertEqual(len(exp.sheets[sheet_name]), 2)
        self.assertEqual([line[2] for line in exp.sheets[sheet_name]], ['Nom', 'Haddock'])

    def test_un_enfant_par_ligne(self):
        fam = self.create_famille_aemo()
        fam.suiviaemo.date_demande = '2019-10-20'
        fam.suiviaemo.save()
        Personne.objects.create_personne(
            famille=fam, role=Role.objects.get(nom='Enfant suivi'),
            nom='Haddock', prenom='Ursule', genre='M', date_naissance=date(2010, 2, 17),
        )

        exp = TestExporter()
        with patch('carrefour.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '10', 'annee': '2019'})
        sheet_name = 'Nouveaux_10.2019'
        self.assertEqual(len(exp.sheets[sheet_name]), 3)
        self.assertEqual(
            [(line[2], line[3]) for line in exp.sheets['Nouveaux_10.2019']],
            [('Nom', 'Prenom'), ('Haddock', 'Ursule'), ('Haddock', 'Toto'), ]
        )

    def test_repartition_temps_total_prestation_mensuel_entre_enfants(self):
        fam = self.create_famille_aemo()
        fam.suiviaemo.date_demande = '2019-10-20'
        fam.suiviaemo.save()
        Personne.objects.create_personne(
            famille=fam, role=Role.objects.get(nom='Enfant suivi'),
            nom='Haddock', prenom='Ursule', genre='M', date_naissance=date(2011, 2, 17),
        )
        PrestationAemo.objects.bulk_create([
            PrestationAemo(famille=fam, date_prestation=date(2019, 10, 21), duree='3:40'),
            PrestationAemo(famille=fam, date_prestation=date(2019, 10, 22), duree='6:40'),
            PrestationAemo(famille=fam, date_prestation=date(2019, 10, 30), duree='2:00'),
        ])

        exp = TestExporter()
        with patch('carrefour.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '10', 'annee': '2019'})
        sheet_name = 'En_cours_10.2019'
        self.assertEqual(len(exp.sheets[sheet_name]), 3)
        self.assertEqual(
            [(line[2], line[3], line[20]) for line in exp.sheets[sheet_name]],
            [('Nom', 'Prenom', 'H. de suivi'),
             ('Haddock', 'Ursule', timedelta(hours=6, minutes=10)),
             ('Haddock', 'Toto', timedelta(hours=6, minutes=10)),
             ]
        )

    def test_export_nouvelle_famille(self):
        fam = self.create_famille_aemo()
        fam.suiviaemo.date_demande = '2019-10-20'
        fam.suiviaemo.motif_demande = ['parentalite']
        fam.suiviaemo.save()
        PrestationAemo.objects.bulk_create([
            PrestationAemo(famille=fam, date_prestation=date(2019, 10, 21), duree='3:40'),
            PrestationAemo(famille=fam, date_prestation=date(2019, 10, 22), duree='6:40'),
            # Not included in this month
            PrestationAemo(famille=fam, date_prestation=date(2019, 11, 1), duree='2:00'),
        ])
        self.assertEqual(PrestationAemo.objects.filter(famille=fam).count(), 3)
        for p in PrestationAemo.objects.filter(famille=fam):
            p.intervenants.set([self.user_externe])

        exp = TestExporter()
        with patch('carrefour.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '10', 'annee': '2019'})
        sheet_name = 'Nouveaux_10.2019'
        self.assertEqual(len(exp.sheets[sheet_name]), 2)
        self.assertEqual(
            exp.sheets[sheet_name][1],
            ['Carrefour', 'AEMO', 'Haddock', 'Toto', 'M', '16.02.2010', 'Château1', '2000', 'Moulinsart', 'NE',
             'Sybarnez Tina', '', '', 'Haddock', 'Archibald', 'Conjointe', 'Divorcé', '', 'NON', 1,
             '20.10.2019', 'Soutien à la parentalité']
        )

    def test_export_total_prestation_mois_courant(self):
        fam = self.create_famille_aemo()
        fam.suiviaemo.date_demande = '2019-10-20'
        fam.suiviaemo.save()
        PrestationAemo.objects.bulk_create([
            PrestationAemo(famille=fam, date_prestation=date(2019, 10, 21), duree='3:40'),
            PrestationAemo(famille=fam, date_prestation=date(2019, 10, 22), duree='6:40'),
            # Not included in this month
            PrestationAemo(famille=fam, date_prestation=date(2019, 11, 1), duree='2:00'),
        ])
        for p in PrestationAemo.objects.filter(famille=fam):
            p.intervenants.set([self.user_externe])

        exp = TestExporter()
        with patch('carrefour.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '10', 'annee': '2019'})
        self.assertEqual(len(exp.sheets['En_cours_10.2019']), 2)
        self.assertEqual(exp.sheets['En_cours_10.2019'][1][20], timedelta(hours=10, minutes=20))

    def test_save_familles_actives_aemo(self):
        familles = [self.create_famille_aemo('Fam_{}'.format(i)) for i in range(5)]
        for famille in familles:
            famille.suiviaemo.date_demande = date(2019, 10, 1)
            famille.suiviaemo.save()
        prest = PrestationAemo.objects.create(
            famille=None,
            date_prestation=date(2019, 10, 21),
            duree=timedelta(hours=3, minutes=40),
            lib_prestation=LibellePrestation.objects.get(code='p02')
        )
        self.assertEqual(prest.familles_actives, 5)

    def test_export_prest_gen_aemo(self):
        familles = [self.create_famille_aemo('Fam_{}'.format(i)) for i in range(5)]
        for famille in familles:
            famille.suiviaemo.date_demande = date(2019, 10, 1)
            famille.suiviaemo.save()
        # 2 prestations familiales
        PrestationAemo.objects.create(
            famille=familles[0],
            date_prestation=date(2019, 10, 21),
            duree=timedelta(hours=3, minutes=40),
            lib_prestation=LibellePrestation.objects.get(code='p01')
        )
        PrestationAemo.objects.create(
            famille=familles[1],
            date_prestation=date(2019, 10, 21),
            duree=timedelta(hours=4, minutes=12),
            lib_prestation=LibellePrestation.objects.get(code='p01')
        )
        # 2 prestations générales
        PrestationAemo.objects.create(
            famille=None,
            date_prestation=date(2019, 10, 21),
            duree=timedelta(hours=6, minutes=50),
            lib_prestation=LibellePrestation.objects.get(code='p02')
        )
        PrestationAemo.objects.create(
            famille=None,
            date_prestation=date(2019, 10, 25),
            duree=timedelta(hours=0, minutes=50),
            lib_prestation=LibellePrestation.objects.get(code='p02')
        )
        exp = TestExporter()
        with patch('carrefour.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), self.cfg_export_month)
        self.assertEqual(len(exp.sheets[self.cfg_sheet]), 6)
        # Prest. fam. (col=20) + prest. gén. (col=21)
        data = [(3, 40), (4, 12), (0, 0), (0, 0), (0, 0)]
        for row in range(5):
            self.assertEqual(
                exp.sheets[self.cfg_sheet][row+1][20],
                timedelta(hours=data[row][0], minutes=data[row][1])
            )
            self.assertEqual(
                exp.sheets[self.cfg_sheet][row+1][21],
                timedelta(hours=1, minutes=32)
            )

        self.assertEqual(format_duree(exp._total_aemo), '07:52')
        self.assertEqual(format_duree(exp._total_gen_aemo), '07:40')

    def test_export_prestation_aemo_asaef_batoude(self):
        fam_aemo = FamilleAemo.objects.get(nom='Haddock')
        fam_aemo.suiviaemo.date_demande = '2019-09-20'
        fam_aemo.suiviaemo.date_debut_suivi = '2019-09-20'
        fam_aemo.suiviaemo.save()
        Personne.objects.create_personne(
            famille=fam_aemo, role=Role.objects.get(nom='Enfant suivi'),
            nom='Haddock', prenom='Archibald', genre='M', date_naissance=date(1956, 2, 16),
        )

        fam_asaef = FamilleAsaef.objects.get(nom='Tournesol')
        fam_asaef.suiviasaef.date_demande = '2019-09-20'
        fam_asaef.suiviasaef.save()
        Personne.objects.create_personne(
            famille=fam_asaef, role=Role.objects.get(nom='Enfant suivi'),
            nom='Tournesol', prenom='Tryphon Jr', genre='M', date_naissance=date(1956, 2, 16),
        )

        Beneficiaire.objects.create(
            nom='Valjean', prenom='Jean', date_demande='2019-09-20', date_admission='2019-09-20',
            region='mont_locle')
        Beneficiaire.objects.create(
            nom='Dupont', prenom='Paul', date_demande='2019-09-20', region='mont_locle'
        )
        self.client.force_login(self.user_admin)
        exp = TestExporter()
        with patch('carrefour.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '9', 'annee': '2019'})
        self.assertEqual(len(exp.sheets['En_cours_09.2019']), 4)
        self.assertEqual(len(exp.sheets['Nouveaux_09.2019']), 4)
        self.assertEqual(len(exp.sheets['Terminés_09.2019']), 1)

    def test_annee_actuelle_dans_formulaire(self):
        form = MonthSelectionForm()
        year = date.today().year
        self.assertIn((year, year), form.fields['annee'].choices)


class AccessTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        # ANONYME
        cls.user_anonyme = Utilisateur.objects.create_user(
            'anonyme', 'anonym@example.org', first_name='Jean', last_name='Anonyme')

        # ADMIN ( Secrétatiat)
        group_admin, _ = Group.objects.get_or_create(name='admin')
        group_admin.permissions.add(*list(Permission.objects.filter(codename__in=[
            'view_littoral', 'view_montagnes', 'view_asaef', 'view_batoude', 'view_ser'])))
        cls.user_admin = Utilisateur.objects.create_user(
            'admin', 'admin@example.org', first_name='Jean', last_name='Admin')
        cls.user_admin.groups.add(group_admin)

        # AEMO_LITTORAL
        group_aemo_litt, _ = Group.objects.get_or_create(name='aemo_littoral')
        group_aemo_litt.permissions.add(*list(Permission.objects.filter(
            codename__in=['view_littoral', 'change_littoral'])))
        cls.user_aemo_litt = Utilisateur.objects.create_user(
            'Aemolitt', 'aemo_litt@exemple.org', first_name='Jean', last_name='AemoLittoral')
        cls.user_aemo_litt.groups.add(group_aemo_litt)

        cls.user_aemo_re_litt = Utilisateur.objects.create_user(
            'AemoRelitt', 'aemo_re_litt@exemple.org', first_name='Jean',
            last_name='AemoReLittoral')
        cls.user_aemo_re_litt.groups.add(group_aemo_litt)
        cls.user_aemo_re_litt.user_permissions.add(
            *list(Permission.objects.filter(codename__in=[
                'manage_littoral', 'view_montagnes', 'view_asaef', 'view_batoude', 'view_ser',
                'add_contact', 'change_contact'])))

        # AEMO_MONTAGNES
        group_aemo_mont, _ = Group.objects.get_or_create(name='aemo_montagnes')
        group_aemo_mont.permissions.add(*list(Permission.objects.filter(codename__in=[
            'view_montagnes', 'change_montagnes'])))
        cls.user_aemo_mont = Utilisateur.objects.create_user(
            'Aemomont', 'aemo_mont@exemple.org', first_name='Jean', last_name='AemoMontagnes')
        cls.user_aemo_mont.groups.add(group_aemo_mont)

        cls.user_aemo_re_mont = Utilisateur.objects.create_user(
            'AemoReMont', 'aemo_re_mont@exemple.org', first_name='Jean',
            last_name='AemoReMontagne')
        cls.user_aemo_re_mont.groups.add(group_aemo_mont)
        cls.user_aemo_re_mont.user_permissions.add(
            *list(Permission.objects.filter(codename__in=[
                'manage_montagnes', 'view_littoral', 'view_asaef', 'view_batoude', 'view_ser',
                'add_contact', 'change_contact'])))

        # ASAEF
        group_asaef, _ = Group.objects.get_or_create(name='asaef')
        cls.user_asaef = Utilisateur.objects.create_user(
            'Asaef', 'asaef@exemple.org', first_name='Jean', last_name='Asaef')
        cls.user_asaef.groups.add(group_asaef)
        group_asaef.permissions.add(*list(Permission.objects.filter(codename__in=[
            'view_asaef', 'change_asaef'])))

        cls.user_asaef_re = Utilisateur.objects.create_user(
            'AsaefRe', 'asaef_re@exemple.org', first_name='Jean', last_name='AsaefRe')
        cls.user_asaef_re.groups.add(group_asaef)
        cls.user_asaef_re.user_permissions.add(*list(Permission.objects.filter(codename__in=[
            'manage_asaef', 'view_montagnes', 'view_littoral', 'view_batoude', 'view_ser',
            'add_contact', 'change_contact'])))

        # BATOUDE
        group_batoude, _ = Group.objects.get_or_create(name='batoude')
        cls.user_batoude = Utilisateur.objects.create_user(
            'Batoude', 'batoude@exemple.org', first_name='Jean', last_name='Batoude')
        cls.user_batoude.groups.add(group_batoude)
        group_batoude.permissions.add(*list(Permission.objects.filter(codename__in=[
            'view_batoude', 'change_batoude'])))

        cls.user_batoude_re = Utilisateur.objects.create_user(
            'BatoudeRe', 'batoude_re@exemple.org', first_name='Jean', last_name='BatoudeRe')
        cls.user_batoude_re.groups.add(group_batoude)
        cls.user_batoude_re.user_permissions.add(*list(Permission.objects.filter(codename__in=[
            'manage_batoude', 'view_montagnes', 'view_littoral', 'view_asaef', 'view_ser',
            'add_contact', 'change_contact'])))

        # ASAP
        group_asap, _ = Group.objects.get_or_create(name='asap')
        cls.user_asap = Utilisateur.objects.create_user(
            'Asap', 'asap@exemple.org', first_name='Jean', last_name='Asap')
        cls.user_asap.groups.add(group_asap)
        group_asap.permissions.add(*list(Permission.objects.filter(codename__in=[
            'view_ser', 'change_ser'])))

        cls.user_asap_re = Utilisateur.objects.create_user(
            'AsapRe', 'asap_re@exemple.org', first_name='Jean', last_name='AsapRe')
        cls.user_asap_re.groups.add(group_asap)
        cls.user_asap_re.user_permissions.add(*list(Permission.objects.filter(codename__in=[
            'manage_ser', 'view_montagnes', 'view_littoral', 'view_asaef', 'view_batoude',
            'add_contact', 'change_contact'])))

        # DIRECTION
        group_direction, _ = Group.objects.get_or_create(name='direction)')
        cls.user_direction = Utilisateur.objects.create_user(
            'dir', 'dir@exemple.org', first_name='Jean', last_name='Direction')
        cls.user_direction.groups.add(group_direction)
        group_direction.permissions.add(*list(Permission.objects.filter(codename__in=[
            'view_montagnes', 'change_montagnes', 'manage_montagnes',
            'view_littoral', 'change_littoral', 'manage_littoral',
            'view_asaef', 'change_asaef', 'manage_asaef',
            'view_batoude', 'change_batoude', 'manage_batoude',
            'view_ser', 'change_ser', 'manage_ser',
            'add_contact', 'change_contact',
            'export_aemo', 'add_utilisateur', 'change_utilisateur', 'delete_utilisateur',
            'add_role', 'delete_role', 'add_service',
        ])))

        FamilleAemo.objects.create_famille(
            nom='FamilleAemoMont', rue='Château1', npa=2000, localite='Moulinsart',
            equipe='montagnes', autorite_parentale='conjointe', statut_marital='divorce',
            monoparentale=False
        )
        FamilleAsaef.objects.create_famille(
            nom='FamilleAsaef', rue='Château1', npa=2000, localite='Moulinsart',
        )
        Beneficiaire.objects.create(
            nom="PersonBatoude", genre="M"
        )
        Person.objects.create(
            nom="PersonAsap", genre="M"
        )
        Role.objects.bulk_create([
            Role(nom='Père', famille=True),
            Role(nom='Mère', famille=True),
            Role(nom='Beau-père', famille=True),
            Role(nom='Enfant suivi', famille=True),
            Role(nom='Enfant non-suivi', famille=True),
            Role(nom='Médecin', famille=False),
        ])

    def _test_access(self, users, url_patterns, mode=None):
        for user in users:
            self.client.force_login(user)
            for name, args in url_patterns:
                response = self.client.get(reverse(name, args=args))
                if mode == 'full':
                    self.assertEqual(response.status_code, 200)
                    self.assertTrue(response.context['can_edit'])
                elif mode == 'read_only':
                    self.assertEqual(response.status_code, 200)
                    self.assertFalse(response.context['can_edit'])
                elif mode == 'prohibited':
                    self.assertEqual(response.status_code, 403)
                else:
                    raise ValueError(f"Paramètre mode '{mode}' inconnu")

    def test_aemo_prohibited_access(self):
        famille = FamilleAemo.objects.get(nom="FamilleAemoMont")
        url_patterns = [
            ('aemo-famille-list', []), ('aemo-famille-add', []), ('aemo-famille-edit', [famille.pk]),
            ('aemo-demande', [famille.pk]), ('aemo-famille-suivi', [famille.pk]),
            ('aemo-famille-agenda', [famille.pk]), ('aemo-journal-list', [famille.pk])
        ]
        users = [self.user_anonyme, self.user_asaef, self.user_batoude, self.user_asap]
        self._test_access(users, url_patterns, mode='prohibited')

    def test_aemo_read_only_access(self):
        famille = FamilleAemo.objects.get(nom="FamilleAemoMont")
        url_patterns = [
            ('aemo-famille-list', []), ('aemo-famille-edit', [famille.pk]),
            ('aemo-demande', [famille.pk]), ('aemo-famille-suivi', [famille.pk]),
            ('aemo-famille-agenda', [famille.pk]), ('aemo-journal-list', [famille.pk])
        ]
        users = [self.user_admin, self.user_asaef_re, self.user_batoude_re, self.user_asap_re]
        self._test_access(users, url_patterns, mode='read_only')

    def test_aemo_full_access(self):
        famille = FamilleAemo.objects.get(nom="FamilleAemoMont")
        url_patterns = [
            ('aemo-famille-list', []), ('aemo-famille-add', []),
            ('aemo-famille-edit', [famille.pk]), ('aemo-demande', [famille.pk]),
            ('aemo-famille-suivi', [famille.pk]), ('aemo-famille-agenda', [famille.pk]),
            ('aemo-journal-list', [famille.pk])
        ]
        users = [self.user_aemo_mont, self.user_aemo_re_mont, self.user_direction]
        self._test_access(users, url_patterns, mode='full')

    def test_asaef_prohibited_access(self):
        famille = FamilleAsaef.objects.get(nom="FamilleAsaef")
        url_patterns = [
            ('asaef-famille-list', []), ('asaef-famille-add', []), ('asaef-famille-edit', [famille.pk]),
            ('asaef-demande', [famille.pk]), ('asaef-projet-edit', [famille.pk]),
            ('asaef-famille-suivi', [famille.pk]), ('asaef-famille-agenda', [famille.pk]),
        ]
        users = [self.user_anonyme, self.user_aemo_litt, self.user_aemo_mont,
                 self.user_batoude, self.user_asap]
        self._test_access(users, url_patterns, mode='prohibited')

    def test_asaef_read_only_access(self):
        famille = FamilleAsaef.objects.get(nom="FamilleAsaef")
        url_patterns = [
            ('asaef-famille-list', []), ('asaef-famille-edit', [famille.pk]),
            ('asaef-demande', [famille.pk]), ('asaef-projet-edit', [famille.pk]),
            ('asaef-famille-suivi', [famille.pk]), ('asaef-famille-agenda', [famille.pk]),
        ]
        users = [self.user_admin, self.user_aemo_re_litt, self.user_aemo_re_mont,
                 self.user_batoude_re, self.user_asap_re]
        self._test_access(users, url_patterns, mode='read_only')

    def test_asaef_full_access(self):
        famille = FamilleAsaef.objects.get(nom="FamilleAsaef")
        url_patterns = [
            ('asaef-famille-list', []), ('asaef-famille-add', []),
            ('asaef-famille-edit', [famille.pk]), ('asaef-demande', [famille.pk]),
            ('asaef-projet-edit', [famille.pk]), ('asaef-famille-suivi', [famille.pk]),
            ('asaef-famille-agenda', [famille.pk]),
        ]
        users = [self.user_asaef, self.user_asaef_re, self.user_direction]
        self._test_access(users, url_patterns, mode='full')

    def test_batoude_prohibited_access(self):
        person = Beneficiaire.objects.get(nom="PersonBatoude")
        url_patterns = [
            ('batoude-person-list', []), ('batoude-person-add', []),
            ('batoude-person-edit', [person.pk]), ('batoude-journal-list', [person.pk]),
            ('batoude-famille-list', [person.pk]), ('batoude-famille-add', [person.pk]),
            ('batoude-assurance-edit', [person.pk]),
        ]
        users = [self.user_anonyme, self.user_aemo_litt, self.user_aemo_mont,
                 self.user_asaef, self.user_asap]
        self._test_access(users, url_patterns, mode='prohibited')

    def test_batoude_read_only_access(self):
        person = Beneficiaire.objects.get(nom="PersonBatoude")
        url_patterns = [
            ('batoude-person-list', []), ('batoude-person-edit', [person.pk]),
            ('batoude-journal-list', [person.pk]),
            ('batoude-famille-list', [person.pk]),
        ]
        users = [self.user_admin, self.user_aemo_re_litt, self.user_aemo_re_mont,
                 self.user_asaef_re, self.user_asap_re]
        self._test_access(users, url_patterns, mode='read_only')

    def test_batoude_full_access(self):
        person = Beneficiaire.objects.get(nom="PersonBatoude")
        url_patterns = [
            ('batoude-person-list', []), ('batoude-person-add', []),
            ('batoude-person-edit', [person.pk]), ('batoude-journal-list', [person.pk]),
            ('batoude-famille-list', [person.pk]), ('batoude-famille-add', [person.pk]),
            ('batoude-assurance-edit', [person.pk]),
        ]
        users = [self.user_batoude, self.user_batoude_re, self.user_direction]
        self._test_access(users, url_patterns, mode='full')

    def test_asap_access_prohibited(self):
        person = Person.objects.get(nom="PersonAsap")
        url_patterns = [('asap-person-list', []), ('asap-person-change', [person.pk]),
                        ('asap-journal-list', [person.pk])]
        users = [self.user_anonyme, self.user_aemo_litt, self.user_aemo_mont,
                 self.user_asaef, self.user_batoude]
        self._test_access(users, url_patterns, mode='prohibited')

    def test_asap_access_read_only(self):
        person = Person.objects.get(nom="PersonAsap")
        url_patterns = [('asap-person-list', []), ('asap-person-change', [person.pk]),
                        ('asap-journal-list', [person.pk])]
        users = [self.user_admin, self.user_aemo_re_litt, self.user_aemo_re_mont,
                 self.user_asaef_re, self.user_batoude_re]
        self._test_access(users, url_patterns, mode='read_only')

    def test_asap_full_acces(self):
        person = Person.objects.get(nom="PersonAsap")
        url_patterns = [('asap-person-list', []), ('asap-person-change', [person.pk]),
                        ('asap-journal-list', [person.pk]), ('asap-journal-add', [person.pk])]
        users = [self.user_asap, self.user_asap_re, self.user_direction]
        self._test_access(users, url_patterns, mode='full')


class ContactTests(InitialDataMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.contact1 = Contact.objects.create(nom='Doe', prenom='John')
        cls.contact2 = Contact.objects.create(nom='Doe', prenom='John2', profession='Médecin')

    def test_contact_list(self):
        self.client.force_login(self.user_externe)
        response = self.client.get(reverse('contact-list'))
        self.assertEqual(len(response.context['object_list']), 5)

    def test_contact_autocomplete(self):
        self.client.force_login(self.user_externe)
        medecin = Role.objects.get(nom='Médecin')
        ope_service = Service.objects.create(sigle="OPEC")
        other_service = Service.objects.get(sigle="SSE")
        Contact.objects.create(
            nom="Duplain", prenom="Irma", role=medecin, service=ope_service
        )
        Contact.objects.create(
            nom="Dupont", prenom="Paul", role=medecin, service=other_service
        )
        response = self.client.get(reverse('contact-autocomplete') + '?q=Dup')
        self.assertEqual(
            [res['text'] for res in response.json()['results']],
            ['Duplain Irma (OPEC)', 'Dupont Paul (SSE)']
        )
        # The OPE version
        response = self.client.get(reverse('contact-ope-autocomplete') + '?q=Dup')
        self.assertEqual(
            [res['text'] for res in response.json()['results']],
            ['Duplain Irma (OPEC)']
        )

    def test_formulaire_contact_sans_service_carrefour(self):
        form = ContactForm()
        service = Service.objects.get(sigle='CARREFOUR')
        self.assertNotIn(str(form['service']), f'<option value="{service.pk}">CARREFOUR</option>')

    def test_formulaire_contact_sans_role_famille(self):
        form = ContactForm()
        pere = Role.objects.get(nom='Père')
        self.assertNotIn(str(form['role']), f'<option value="{pere.pk}">Père</option>')
        mere = Role.objects.get(nom='Mère')
        self.assertNotIn(str(form['role']), f'<option value="{mere.pk}">Mère</option>')

    def test_suppression_affichage_du_contact_desactive_dans_contact_autocomplete(self):
        self.client.force_login(self.user_aemo)
        medecin = Role.objects.get(nom='Médecin')
        ope_service = Service.objects.create(sigle="OPEC")
        other_service = Service.objects.get(sigle="SSE")
        Contact.objects.create(
            nom="Duplain", prenom="Irma", role=medecin, service=ope_service, est_actif=False
        )
        Contact.objects.create(
            nom="Dupont", prenom="Paul", role=medecin, service=other_service
        )
        response = self.client.get(reverse('contact-autocomplete') + '?q=Dup')
        self.assertEqual(
            [res['text'] for res in response.json()['results']],
            ['Dupont Paul (SSE)']
        )

    def test_suppression_affichage_contact_desactive_dans_list_contact(self):
        self.client.force_login(self.user_aemo)
        medecin = Role.objects.get(nom='Médecin')
        ope_service = Service.objects.create(sigle="OPEC")
        other_service = Service.objects.get(sigle="SSE")
        Contact.objects.create(
            nom="Duplain", prenom="Irma", role=medecin, service=ope_service, est_actif=False
        )
        Contact.objects.create(
            nom="Dupont", prenom="Paul", role=medecin, service=other_service
        )
        response = self.client.get(reverse('contact-list'))
        self.assertNotContains(response, 'Duplain')

    def test_controler_doublon_contact(self):
        self.client.force_login(self.user_aemo)
        medecin = Role.objects.get(nom='Médecin')
        Contact.objects.create(
            nom="Duplain", prenom="Irma", role=medecin, est_actif=True
        )
        doublon_url = reverse('contact-doublon')
        response = self.client.post(doublon_url, data={'nom': "Duplain", 'prenom': "Irma"})
        self.assertEqual(response.json(), [{'nom': "Duplain", 'prenom': "Irma"}])
        response = self.client.post(doublon_url, data={'nom': "Nouveau", 'prenom': "Contact"})
        # Réponse vide signifie pas de doublon détecté.
        self.assertEqual(response.json(), '')

    def test_desactivation_contact_avec_dossiers_actifs(self):
        self.user_admin.user_permissions.add(Permission.objects.get(codename='delete_contact'))
        self.client.force_login(self.user_admin)
        contact = Contact.objects.get(nom='Sybarnez')
        FamilleAsaef.objects.get(nom='Tournesol')
        pers = Personne.objects.get(nom='Hergé')
        pers.reseaux.add(contact)

        response = self.client.post(reverse('contact-delete', args=[contact.pk]), follow=True)
        self.assertTrue(Contact.objects.filter(nom='Sybarnez').exists())
        contact.refresh_from_db()
        self.assertFalse(contact.est_actif)
        self.assertNotContains(response, 'Sybarnez')

    def test_desactivation_contact_avec_dossiers_inactifs(self):
        self.user_admin.user_permissions.add(Permission.objects.get(codename='delete_contact'))
        self.client.force_login(self.user_admin)
        contact = Contact.objects.get(nom='Sybarnez')
        famille = FamilleAsaef.objects.get(nom='Tournesol')
        pers = Personne.objects.get(nom='Hergé')
        pers.reseaux.add(contact)
        famille.suiviasaef.date_fin_suivi = date.today() - timedelta(days=7)
        famille.suiviasaef.save()

        response = self.client.post(reverse('contact-delete', args=[contact.pk]), follow=True)
        self.assertTrue(Contact.objects.filter(nom='Sybarnez').exists())
        contact.refresh_from_db()
        self.assertFalse(contact.est_actif)
        self.assertNotContains(response, 'Sybarnez')

    def test_suppression_contact_sans_dossiers_actifs(self):
        self.user_admin.user_permissions.add(Permission.objects.get(codename='delete_contact'))
        self.client.force_login(self.user_admin)
        contact = Contact.objects.get(nom='Sybarnez')

        response = self.client.post(reverse('contact-delete', args=[contact.pk]), follow=True)
        self.assertFalse(Contact.objects.filter(nom='Sybarnez').exists())
        self.assertNotContains(response, 'Sybarnez')

    def test_affichage_dossiers_actifs_dans_popup(self):
        self.client.force_login(self.user_admin)
        contact = Contact.objects.create(nom='Doe', prenom='John')
        fam1 = FamilleAsaef.objects.create_famille(nom='Fam1')
        person1 = Personne.objects.create(nom='Person1', prenom='Prénom1', genre='M', famille=fam1,
                                          role=Role.objects.get(nom='Enfant suivi'))
        person1.reseaux.add(contact)
        person2 = Beneficiaire.objects.create(nom='Person2', prenom='Prénom2', genre='F')
        person2.reseau.add(contact)
        response = self.client.get(reverse('contact-dossier', args=[contact.pk]))
        self.assertEqual(
            response.json(),
            {"contact": "Doe John", "dossiers": ["Fam. Fam1 (Asaef)", "Person2 Prénom2 (Batoude)"]}
        )

    def test_transferer_beneficiaires_batoude_depuis_doublons(self):
        person1 = Beneficiaire.objects.create(nom='Person1', prenom='Prénom1', genre='M')
        person2 = Beneficiaire.objects.create(nom='Person2', prenom='Prénom2', genre='F')
        person1.reseau.add(self.contact1)
        person2.reseau.add(self.contact2)
        Contact.fusionner(self.contact1, self.contact2)
        self.assertIn(self.contact1, person2.reseau.all())

    def test_transferer_famille_aemo_depuis_doublons(self):
        fam1 = FamilleAemo.objects.create_famille(nom='Fam1', equipe='montagnes')
        fam2 = FamilleAemo.objects.create_famille(nom='Fam2', equipe='montagnes')
        person1 = Personne.objects.create(
            nom='Person1', prenom='Prénom1', genre='M', famille=fam1, role=Role.objects.get(nom='Enfant suivi'))
        person2 = Personne.objects.create(
            nom='Person2', prenom='Prénom2', genre='F', famille=fam2, role=Role.objects.get(nom='Enfant suivi'))
        person1.reseaux.add(self.contact1)
        person2.reseaux.add(self.contact2)
        Contact.fusionner(self.contact1, self.contact2)
        self.assertIn(self.contact1, person2.reseaux.all())

    def test_transferer_famille_asaef_depuis_doublons(self):
        fam1 = FamilleAsaef.objects.create_famille(nom='Fam1')
        fam2 = FamilleAsaef.objects.create_famille(nom='Fam2')
        person1 = Personne.objects.create(
            nom='Person1', prenom='Prénom1', genre='M', famille=fam1, role=Role.objects.get(nom='Enfant suivi'))
        person2 = Personne.objects.create(
            nom='Person2', prenom='Prénom2', genre='F', famille=fam2, role=Role.objects.get(nom='Enfant suivi'))
        person1.reseaux.add(self.contact1)
        person2.reseaux.add(self.contact2)
        Contact.fusionner(self.contact1, self.contact2)
        self.assertIn(self.contact1, person2.reseaux.all())

    def test_fusion_affichage_colonne_checkbox(self):
        self.client.force_login(self.user_aemo)
        response = self.client.get(reverse('contact-list'))
        self.assertNotContains(response, "Fusion")

        self.client.force_login(self.user_admin)
        response = self.client.get(reverse('contact-list'))
        self.assertContains(response, "Fusion")

    def test_fusion_ordre_saisie_asc_contacts(self):
        self.assertEqual(Contact.objects.filter(nom='Doe').count(), 2)
        self.client.force_login(self.user_admin)
        self.client.post(
            reverse('contact-merge', args=[self.contact1.pk]),
            data={'doublons': f"{self.contact2.pk}",
                  'nom': self.contact1.nom,
                  'prenom': self.contact1.prenom}
        )
        self.assertEqual(Contact.objects.filter(nom='Doe').count(), 1)
        result = Contact.objects.get(nom='Doe')
        self.assertEqual(result.pk, self.contact1.pk)

    def test_fusion_ordre_saisie_desc_contacts(self):
        self.assertEqual(Contact.objects.filter(nom='Doe').count(), 2)
        self.client.force_login(self.user_admin)
        self.client.post(
            reverse('contact-merge', args=[self.contact2.pk]),
            data={'doublons': f"{self.contact1.pk}",
                  'nom': self.contact1.nom,
                  'prenom': self.contact1.prenom}
        )
        self.assertEqual(Contact.objects.filter(nom='Doe').count(), 1)
        result = Contact.objects.get(nom='Doe')
        self.assertEqual(result.pk, self.contact2.pk)

    def test_fusion_contact_permission_error(self):
        self.client.force_login(self.user_aemo)
        reponse = self.client.post(
            reverse('contact-merge', args=[self.contact1.pk]),
            data={'doublons': f"{self.contact2.pk}",
                  'nom': self.contact1.nom,
                  'prenom': self.contact1.prenom}
        )
        self.assertEqual(reponse.status_code, 403)

    def test_fusion_trois_doublons(self):
        contact3 = Contact.objects.create(nom="Verne", prenom="Jules")
        contact4 = Contact.objects.create(nom="Zola", prenom="Emile")
        self.client.force_login(self.user_admin)
        self.client.post(
            reverse('contact-merge', args=[self.contact1.pk]),
            data={'doublons': f"{self.contact2.pk}, {contact3.pk}, {contact4.pk}",
                  'nom': self.contact1.nom,
                  'prenom': self.contact1.prenom}
        )
        self.assertEqual(Contact.objects.filter(nom='Doe').count(), 1)
        self.assertFalse(Contact.objects.filter(nom="Verne").exists())
        self.assertFalse(Contact.objects.filter(nom="Zola").exists())

    def test_fusion_un_contact(self):
        self.client.force_login(self.user_admin)
        response = self.client.get(
            reverse('contact-merge', args=[self.contact1.pk]) + f'?doublons={self.contact1.pk}',
            follow=True
        )
        self.assertContains(response, "Vous devez sélectionner au moins deux contacts.")

    def test_fusion_avec_dossiers_suivis_AEMO(self):
        self.assertEqual(self.contact1.suivisaemo.count(), 0)

        famille = FamilleAemo.objects.get(nom='Haddock')
        famille.suivi.ope_referent = self.contact2
        famille.suivi.save()
        pers = Personne.objects.create_personne(
            famille=famille, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Enfant suivi')
        )
        pers.reseaux.add(self.contact2)

        self.client.force_login(self.user_admin)
        self.client.post(
            reverse('contact-merge', args=[self.contact1.pk]),
            data={'doublons': f"{self.contact2.pk}",
                  'nom': self.contact1.nom,
                  'prenom': self.contact1.prenom}
        )
        self.contact1.refresh_from_db()
        self.assertEqual(self.contact1.suivisaemo.first().famille.nom, 'Haddock')

        pers.refresh_from_db()
        self.assertEqual(pers.reseaux.first(), self.contact1)

    def test_fusion_avec_dossier_ASAEF(self):
        famille = FamilleAsaef.objects.create_famille(nom='Tournesol')
        famille.suivi.ope_referent = self.contact2
        famille.suivi.ope_referent2 = self.contact2
        famille.suivi.save()
        pers = Personne.objects.create_personne(
            famille=famille, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Enfant suivi')
        )
        pers.reseaux.add(self.contact2)

        self.client.force_login(self.user_admin)
        self.client.post(
            reverse('contact-merge', args=[self.contact1.pk]),
            data={'doublons': f"{self.contact2.pk}",
                  'nom': self.contact1.nom,
                  'prenom': self.contact1.prenom}
        )
        self.contact1.refresh_from_db()
        self.assertEqual(self.contact1.suivisasaef.first().famille.nom, 'Tournesol')

        pers.refresh_from_db()
        self.assertEqual(pers.reseaux.first(), self.contact1)

    def test_fusion_avec_dossier_BATOUDE(self):
        benef = Beneficiaire.objects.create(
            nom='Batoudon', genre='X', referent_ope=self.contact2, referent_ope2=self.contact2
        )
        benef.reseau.add(self.contact2)

        self.client.force_login(self.user_admin)
        self.client.post(
            reverse('contact-merge', args=[self.contact1.pk]),
            data={'doublons': f"{self.contact2.pk}",
                  'nom': self.contact1.nom,
                  'prenom': self.contact1.prenom}
        )
        self.contact1.refresh_from_db()
        self.assertEqual(self.contact1.suivisbatoude.first().nom, 'Batoudon')

        benef.refresh_from_db()
        self.assertEqual(benef.referent_ope, self.contact1)
        self.assertEqual(benef.referent_ope2, self.contact1)
        self.assertEqual(benef.reseau.first(), self.contact1)

    def test_fusion_avec_dossier_ASAP(self):
        benef = Person.objects.create(prenom='Asap', genre='X', statut='actif')
        benef.contacts.add(self.contact2)

        self.client.force_login(self.user_admin)
        self.client.post(
            reverse('contact-merge', args=[self.contact1.pk]),
            data={'doublons': f"{self.contact2.pk}",
                  'nom': self.contact1.nom,
                  'prenom': self.contact1.prenom}
        )
        self.contact1.refresh_from_db()
        self.assertEqual(self.contact1.beneficiaires.first().prenom, 'Asap')

    def test_contact_form_localite_error(self):
        form = ContactForm(instance=None, data={'nom': 'Doe', 'localite': '2000 Neuchâtel'})
        self.assertFalse(form.is_valid())
        self.assertIn("Le nom de la localité est incorrect.", form.errors['localite'])

    def test_contact_form_localite_sans_npa_error(self):
        form = ContactForm(instance=None, data={'nom': 'Doe', 'localite': 'Neuchâtel'})
        self.assertFalse(form.is_valid())
        self.assertIn("Vous devez saisir un NPA pour la localité.", form.errors['__all__'])


class DerogationSaisieTardiveTests(InitialDataMixin, TestCase):

    def test_access_direction_only(self):
        today = date.today()
        demain = today + timedelta(days=1)

        derog = DerogationSaisieTardive.objects.create(educ=self.user_aemo, duree=(today, demain))
        urls = [
            reverse('utilisateur-derogation-add', args=[self.user_aemo.pk]),
            reverse('utilisateur-derogation-edit', args=[self.user_aemo.pk, derog.pk])
        ]
        self.client.force_login(self.user_aemo)
        for url in urls:
            response = self.client.get(url)
            self.assertEqual(response.status_code, 403)

        self.client.force_login(self.user_admin)
        for url in urls:
            response = self.client.get(url)
            self.assertEqual(response.status_code, 200)

    def test_derogation_create(self):
        today = date.today()
        demain = today + timedelta(days=1)
        form = DerogationSaisieTardiveForm(
            educ=self.user_aemo,
            data={'educ': self.user_aemo.pk, 'date_debut': today, 'date_fin': demain}
        )
        self.assertTrue(form.is_valid())
        form.save()
        self.assertEqual(DerogationSaisieTardive.objects.count(), 1)

    def test_derogation_update(self):
        today = date.today()
        demain = today + timedelta(days=1)
        derogation = DerogationSaisieTardive.objects.create(
            educ=self.user_aemo, duree=[today, demain]
        )
        form = DerogationSaisieTardiveForm(
            educ=self.user_aemo,
            instance=derogation,
            data={'educ': self.user_aemo.pk, 'date_debut': demain, 'date_fin': demain+timedelta(days=1)}
        )
        self.assertTrue(form.is_valid())
        derogation.refresh_from_db()
        self.assertTrue(derogation.duree.upper, demain+timedelta(days=1))

    def test_derogation_erreurs(self):
        today = date.today()
        demain = today + timedelta(days=1)
        form = DerogationSaisieTardiveForm(
            educ=self.user_aemo,
            data={'educ': self.user_aemo.pk, 'date_debut': demain, 'date_fin': today}
        )
        self.assertFalse(form.is_valid())
        self.assertFormError(form, None, "L'ordre chronologique n'est pas respecté.")

        form = DerogationSaisieTardiveForm(
            educ=self.user_aemo,
            data={'educ': self.user_aemo.pk, 'date_debut': demain, 'date_fin': demain}
        )
        self.assertFalse(form.is_valid())
        self.assertFormError(form, None, "Il faut au moins 24 heures entre les deux dates.")
