from datetime import date, timedelta
from tempfile import NamedTemporaryFile

from django.http import HttpResponse
from django.utils.functional import cached_property

from openpyxl import Workbook
from openpyxl.styles import Font
from openpyxl.utils import get_column_letter

from carrefour.utils import format_d_m_Y

openxml_contenttype = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'


class OpenXMLExport:
    def __init__(self, sheet_title):
        self.wb = Workbook()
        self.ws = self.wb.active
        self.ws.title = sheet_title
        self.bold = Font(name='Calibri', bold=True)
        self.row_idx = 1

    def write_line(self, values, bold=False, col_widths=()):
        for col_idx, value in enumerate(values, start=1):
            cell = self.ws.cell(row=self.row_idx, column=col_idx)
            if isinstance(value, timedelta):
                cell.number_format = '[h]:mm;@'
            try:
                cell.value = value
            except KeyError:
                # Ugly workaround for https://bugs.python.org/issue28969
                from openpyxl.utils.datetime import to_excel
                to_excel.cache_clear()
                cell.value = value
            if bold:
                cell.font = self.bold
            if col_widths:
                self.ws.column_dimensions[get_column_letter(col_idx)].width = col_widths[col_idx - 1]
        self.row_idx += 1

    def add_sheet(self, title):
        self.wb.create_sheet(title)
        self.ws = self.wb[title]
        self.row_idx = 1

    def get_http_response(self, filename):
        with NamedTemporaryFile() as tmp:
            self.wb.save(tmp.name)
            tmp.seek(0)
            response = HttpResponse(tmp, content_type=openxml_contenttype)
            response['Content-Disposition'] = 'attachment; filename="{}.xlsx"'.format(filename)
        return response


class ExportReporting(OpenXMLExport):
    def __init__(self):
        super().__init__('temp')
        self.first_sheet = True
        # Totaux pour vérification interne dans les tests.
        self._total_aemo = timedelta(0)
        self._total_gen_aemo = timedelta(0)
        self._total_asaef = timedelta(0)
        self._total_gen_asaef = timedelta(0)

    def setup_sheet(self, title):
        if not self.first_sheet:
            self.add_sheet(title)
        else:
            self.ws.title = title
        self.first_sheet = False

    def produce_suivis(self, sheet_title, query, mois):
        self.setup_sheet(sheet_title)
        SuiviSheet(self, mois).produce(query)
        self._set_col_dimensions()

    def produce_nouveaux(self, sheet_title, query):
        self.setup_sheet(sheet_title)
        NouveauxSheet(self).produce(query)
        self._set_col_dimensions()

    def produce_termines(self, sheet_title, query):
        self.setup_sheet(sheet_title)
        TerminesSheet(self).produce(query)
        self._set_col_dimensions()

    def produce_attente(self, sheet_title, query):
        self.setup_sheet(sheet_title)
        AttenteSheet(self).produce(query)
        self._set_col_dimensions()

    def _set_col_dimensions(self):
        self.ws.column_dimensions['A'].width = 10
        self.ws.column_dimensions['B'].width = 10
        self.ws.column_dimensions['C'].width = 25
        self.ws.column_dimensions['D'].width = 15
        self.ws.column_dimensions['E'].width = 10
        self.ws.column_dimensions['F'].width = 17
        self.ws.column_dimensions['G'].width = 20
        self.ws.column_dimensions['I'].width = 17
        self.ws.column_dimensions['J'].width = 10
        self.ws.column_dimensions['K'].width = 25
        self.ws.column_dimensions['L'].width = 20
        self.ws.column_dimensions['M'].width = 20
        self.ws.column_dimensions['N'].width = 20
        self.ws.column_dimensions['O'].width = 20
        self.ws.column_dimensions['P'].width = 20
        self.ws.column_dimensions['Q'].width = 15
        self.ws.column_dimensions['R'].width = 15
        self.ws.column_dimensions['S'].width = 15
        self.ws.column_dimensions['T'].width = 15
        self.ws.column_dimensions['U'].width = 15
        self.ws.column_dimensions['V'].width = 15
        self.ws.column_dimensions['W'].width = 35


class BaseFamilleSheet:
    en_tetes = [
        'Institution', 'Prestation', 'Nom', 'Prenom', 'Genre', 'Date de naissance',
        'Adresse', 'NPA', 'Localité', 'Canton', 'OPE', 'Nom mère', 'Prénom mère', 'Nom père',
        'Prénom père', 'Autorité parentale', 'Statut marital', 'Statut financier', 'Fam. monopar.', 'Nbre enfants'
    ]

    def __init__(self, exp):
        self.exp = exp

    def produce(self, query):
        self.exp.write_line(self.en_tetes, bold=True)
        for famille_or_benef in query:
            if famille_or_benef._meta.app_label == 'batoude':
                data = self.collect_benef_data(famille_or_benef)
                self.exp.write_line(data)
            else:
                for pers in famille_or_benef.membres_suivis():
                    data = self.collect_pers_data(pers)
                    self.exp.write_line(data)
        self.exp.ws.freeze_panes = self.exp.ws['A2']

    def collect_benef_data(self, pers):
        # BATOUDE
        return [
            'Carrefour',
            pers.prest,  # prest défini dans le queryset (niveau views)
            pers.nom,
            pers.prenom,
            pers.genre,
            format_d_m_Y(pers.naissance),
            pers.rue,
            pers.npa,
            pers.localite,
            'NE',
            pers.referent_ope.nom_prenom if pers.referent_ope else '',
            pers.mere().nom if pers.mere() else '',
            pers.mere().prenom if pers.mere() else '',
            pers.pere().nom if pers.pere() else '',
            pers.pere().prenom if pers.pere() else '',
            pers.get_autorite_parentale_display(),
            pers.get_statut_marital_display(),
            pers.get_statut_financier_display(),
            {True: 'OUI', False: 'NON', None: ''}[pers.monoparentale],
            pers.membres_famille.filter(role='fratrie').count()
        ]

    def collect_pers_data(self, pers):
        # AEMO et ASAEF
        return [
            'Carrefour',
            pers.famille.prest,  # prest défini dans le queryset (niveau views)
            pers.nom,
            pers.prenom,
            pers.genre,
            format_d_m_Y(pers.date_naissance),
            pers.rue,
            pers.npa,
            pers.localite,
            'NE',
            pers.famille.suivi.ope_referent.nom_prenom if pers.famille.suivi.ope_referent else '',
            pers.famille.mere().nom if pers.famille.mere() else '',
            pers.famille.mere().prenom if pers.famille.mere() else '',
            pers.famille.pere().nom if pers.famille.pere() else '',
            pers.famille.pere().prenom if pers.famille.pere() else '',
            pers.famille.get_autorite_parentale_display(),
            pers.famille.get_statut_marital_display(),
            '',  # statut_financier,
            {True: 'OUI', False: 'NON', None: ''}[pers.famille.monoparentale],
            pers.famille.membres_suivis().count() + pers.famille.enfants_non_suivis().count()
        ]


class SuiviSheet(BaseFamilleSheet):
    en_tetes = BaseFamilleSheet.en_tetes + ['H. de suivi', 'Prest. gén.', 'Jours facturés']

    def __init__(self, exp, date_debut_mois):
        self.date_debut_mois = date_debut_mois
        super().__init__(exp)

    @cached_property
    def _temps_total_batoude(self):
        from batoude.models import PrestationBatoude
        return [
            PrestationBatoude.temps_total_mensuel_prest_export(self.date_debut_mois, typ='benef'),
            PrestationBatoude.temps_total_mensuel_prest_export(self.date_debut_mois, typ='gen'),
        ]

    def collect_benef_data(self, pers):
        data = super().collect_benef_data(pers)
        data.extend(self._temps_total_batoude)
        data.append(pers.jours_factures_par_mois(self.date_debut_mois))
        return data

    def collect_pers_data(self, pers):
        data = super().collect_pers_data(pers)
        h_suivi = pers.famille.total_mensuel(self.date_debut_mois) // (pers.famille.membres_suivis().count() or 1)
        prest_gen = pers.famille.total_mensuel_prest_gen(self.date_debut_mois)
        data.extend([h_suivi, prest_gen, ''])
        # Variables pour tests
        if pers.famille.prest == 'AEMO':
            self.exp._total_aemo += h_suivi
            self.exp._total_gen_aemo += prest_gen
        elif pers.famille.prest == 'ASAEF':
            self.exp._total_asaef += h_suivi
            self.exp._total_gen_asaef += prest_gen
        return data


class NouveauxSheet(BaseFamilleSheet):
    en_tetes = BaseFamilleSheet.en_tetes + ['Date demande', 'Motif']

    def collect_benef_data(self, pers):
        data = super().collect_benef_data(pers)
        data.append(format_d_m_Y(pers.date_admission))
        data.append(pers.get_provenance_display())
        return data

    def collect_pers_data(self, pers):
        data = super().collect_pers_data(pers)
        data.append(format(format_d_m_Y(pers.famille.suivi.date_demande)))
        data.append(pers.famille.suivi.get_motif_demande_display())
        return data


class TerminesSheet(BaseFamilleSheet):
    en_tetes = BaseFamilleSheet.en_tetes + [
        'Date entrée', 'Date sortie', 'Total heures', 'Motif de fin', 'Destination',
    ]

    def collect_benef_data(self, pers):
        data = super().collect_benef_data(pers)
        data.extend([
            format_d_m_Y(pers.date_admission),
            format_d_m_Y(pers.date_sortie),
            '',
            '',
            pers.get_destination_display(),
        ])
        return data

    def collect_pers_data(self, pers):
        data = super().collect_pers_data(pers)
        suivi = pers.famille.suivi
        data.extend([
            format_d_m_Y(suivi.date_debut_suivi),
            format_d_m_Y(suivi.date_fin_suivi),
            pers.famille.temps_total_prestations_reparti(),
            suivi.get_motif_fin_suivi_display(),
            ''
        ])
        return data


class AttenteSheet(BaseFamilleSheet):
    en_tetes = ['Institution', 'Prestation', 'Nom', 'Prenom', 'Genre', 'Date de naissance',
        'Adresse', 'NPA', 'Localité', 'Canton', 'OPE', 'Date demande', 'Date analyse', 'Date confirmation',
        'Jours attente (-> auj.)'
    ]

    def collect_pers_data(self, pers):
        # AEMO et ASAEF
        return [
            'Carrefour',
            pers.famille.prest,  # prest défini dans le queryset (niveau views)
            pers.nom,
            pers.prenom,
            pers.genre,
            format_d_m_Y(pers.date_naissance),
            pers.rue,
            pers.npa,
            pers.localite,
            'NE',
            pers.famille.suivi.ope_referent.nom_prenom if pers.famille.suivi.ope_referent else '',
            format_d_m_Y(pers.famille.suivi.date_demande),
            format_d_m_Y(pers.famille.suivi.date_analyse),
            format_d_m_Y(pers.famille.suivi.date_confirmation),
            (date.today() - pers.famille.suivi.date_demande).days
        ]


class ExportStatistique(OpenXMLExport):
    def __init__(self):
        super().__init__('temp')
        self.first_sheet = True

    def _setup_sheet(self, title):
        if not self.first_sheet:
            self.add_sheet(title)
        else:
            self.ws.title = title
        self.first_sheet = False

    def _header(self, head1, months, head2):
        head = list()
        head.extend(head1)
        head.extend([f"{month[1]}.{month[0]}" for month in months])
        head.extend(head2)
        return head

    def _col_width(self, head1, months, head2):
        width = list()
        width.extend(head1)
        width.extend([10 for month in months])
        width.extend(head2)
        return width

    def _prepare_row(self, titre, stats, months, cols=[]):
        row = list([titre])
        row.extend([stats[month] for month in months])
        for col in cols:
            row.append(stats[col])
        return row

    def produce_localite(self, sheet_title, context):
        self._setup_sheet(sheet_title)
        header = self._header([context['titre']], context['months'], ['Total'])
        col_widths = self._col_width([30], context['months'], [10])
        self.write_line(header, bold=True, col_widths=col_widths)
        for loc, stats in context['localites'].items():
            row = self._prepare_row(loc, stats, context['months'], ['total'])
            self.write_line(row)

    def produce_suivi_aemo_asaef(self, sheet_title, context):
        self._setup_sheet(sheet_title)
        header = self._header([context['titre']], context['months'], ['Total'])
        col_widths = self._col_width([30], context['months'], [10])
        self.write_line(header, bold=True, col_widths=col_widths)
        for entite in context['entites']:
            self.write_line([entite['titre']], bold=True)
            months = context['months']
            for key, val in entite['data'].items():
                row = self._prepare_row(val['titre'], val, months, ['total'])
                self.write_line(row)
            row = ["Durée moyenne de l'attente (en jours)"]
            row.extend(['' for month in months])
            row.append(entite['attente'])
            self.write_line(row)

    def produce_annonceur(self, sheet_title, context):
        self._setup_sheet(sheet_title)
        header = self._header([context['titre']], context['months'], ['Total', 'Pourcent'])
        col_widths = self._col_width([30], context['months'], [10, 10])
        self.write_line(header, bold=True, col_widths=col_widths)
        months = context['months']
        for data in context['motifs_data']:
            row = [f"{data['titre']} ({data['empty']}% des champs ne sont pas remplis.)"]
            self.write_line(row, bold=True)
            for motif, stats in data['annonceur'].items():
                row = self._prepare_row(motif, stats, months, ['total', 'pourcent'])
                self.write_line(row)

    def produce_motif_suivi(self, sheet_title, context):
        self._setup_sheet(sheet_title)
        header = self._header([context['titre']], context['months'], ['Total', 'Pourcent'])
        col_widths = self._col_width([30], context['months'], [10, 10])
        self.write_line(header, bold=True, col_widths=col_widths)
        months = context['months']
        for data in context['motifs_data']:
            row = [f"{data['titre']} ({data['empty']}% des champs ne sont pas remplis.)"]
            self.write_line(row, bold=True)
            for motif, stats in data['dem'].items():
                row = self._prepare_row(motif, stats, months, ['total', 'pourcent'])
                self.write_line(row)

    def produce_depart(self, sheet_title, context):
        self._setup_sheet(sheet_title)
        header = self._header([context['titre']], context['months'], ['Total', 'Pourcent'])
        col_widths = self._col_width([30], context['months'], [10, 10])
        self.write_line(header, bold=True, col_widths=col_widths)
        months = context['months']
        for data in context['motifs_data']:
            self.write_line([data['titre']], bold=True)
            for motif, stats in data['depart'].items():
                row = self._prepare_row(motif, stats, months, ['total', 'pourcent'])
                self.write_line(row)

    def produce_region(self, sheet_title, context):
        self._setup_sheet(sheet_title)
        header = self._header([context['titre']], context['months'], ['Total'])
        col_widths = self._col_width([40], context['months'], [10])
        self.write_line(header, bold=True, col_widths=col_widths)
        months = context['months']
        for loc, stats in context['regions'].items():
            row = self._prepare_row(loc, stats, months, ['total'])
            self.write_line(row)

    def produce_age(self, sheet_title, context):
        self._setup_sheet(sheet_title)
        header = self._header([context['titre']], context['months'], ['Total', 'Pourcent'])
        col_widths = self._col_width([40], context['months'], [10, 10])
        self.write_line(header, bold=True, col_widths=col_widths)
        months = context['months']
        for data in context['motifs_data']:
            self.write_line([data['titre']], bold=True)
            for motif, stats in data['age'].items():
                row = self._prepare_row(motif, stats, months, ['total', 'pourcent'])
                self.write_line(row)

    def produce_suivi_intervenant(self, sheet_title, context):
        self._setup_sheet(sheet_title)
        header = self._header([context['titre']], context['months'], ['Moyenne'])
        col_widths = self._col_width([40], context['months'], [10])
        self.write_line(header, bold=True, col_widths=col_widths)
        months = context['months']
        for data in context['intervenants_data']:
            self.write_line([data['titre']], bold=True)
            for interv, stats in data['intervenants'].items():
                row = self._prepare_row(interv, stats, months, ['moyenne'])
                self.write_line(row)

    def produce_heure_intervenant(self, sheet_title, context):
        self._setup_sheet(sheet_title)
        header = self._header([context['titre'], ''], context['months'], ['Total', 'Pourcent'])
        col_widths = self._col_width([40, 15], context['months'], [10, 10])
        self.write_line(header, bold=True, col_widths=col_widths)
        months = context['months']
        categories = [
            {'titre': 'H. prestées', 'key': 'h_prestees'},
            {'titre': 'H. dues', 'key': 'h_dues'},
            {'titre': 'Ecart', 'key': 'ecart'},
        ]
        for data in context['intervenants_data']:
            self.write_line([data['titre']], bold=True)
            for interv, stats in data['intervenants'].items():
                for cat in categories:
                    row = [interv, cat['titre']]
                    row.extend([stats[cat['key']][month] for month in months])
                    row.extend([stats[cat['key']]['total']])
                    if cat['key'] == 'h_prestees':
                        row.extend([round(stats[cat['key']]['pourcent'], 1)])
                    self.write_line(row)

    def produce_prestation(self, sheet_title, context):
        self._setup_sheet(sheet_title)
        header = self._header(
            [f"{context['unite'].upper()}: {context['titre']}"],
            context['months'], ['Total', 'Pourcent'])
        col_widths = self._col_width([40], context['months'], [10, 10])
        self.write_line(header, bold=True, col_widths=col_widths)
        months = context['months']
        for motif, stats in context['motifs_data'].items():
            row = self._prepare_row(motif, stats, months, ['total', 'pourcent'])
            self.write_line(row)

    def produce_suivi_ser(self, sheet_title, context):
        self._setup_sheet(sheet_title)
        header = self._header(
            [f"{context['unite'].upper()}: {context['titre']}"],
            context['months'], ['Total'])
        col_widths = self._col_width([40], context['months'], [10])
        self.write_line(header, bold=True, col_widths=col_widths)
        months = context['months']
        for interv, data in context['suivis'].items():
            row = self._prepare_row(interv, data, months, ['total'])
            self.write_line(row)

    def produce_sortie_asap(self, sheet_title, context):
        self._setup_sheet(sheet_title)
        columns = [
            ('Date', 15),
            ('Educs', 10),
            ('Lieu', 20),
            ('Durée', 10),
            ('Nbre garçons', 15),
            ('Nbre filles', 15),
            ('Non-binaires', 15),
        ]
        header = [item[0] for item in columns]
        col_widths = [item[1] for item in columns]
        self.write_line(
            [f"Sorties ASAP du {format_d_m_Y(context['debut'])} au {format_d_m_Y(context['fin'])}"], bold=True
        )
        self.write_line([])
        self.write_line(header, bold=True, col_widths=col_widths)

        for tshm in context['data']:
            self.write_line(
                [format_d_m_Y(tshm.prestation.date), tshm.prestation.auteur.nom_prenom, str(tshm.lieu),
                 tshm.duree, tshm.prestation.homme, tshm.prestation.femme, tshm.prestation.non_binaire
                 ]
            )
        self.write_line([])
        self.write_line([
            'Nombre de sorties', 12, 'Durée totale', context['duree_totale']
        ])

    def produce_beneficiaire_batoude(self, sheet_title, context):
        self._setup_sheet(sheet_title)
        header = ['Nom, prénom', 'Adresse', 'Date de naissance', 'IPE', 'Date demande', 'Date validation',
                  'Date admission']
        col_widths = (40, 40, 20, 30, 20, 20, 20)
        self.write_line([f'Batoude: Liste des bénéficiaires au {format_d_m_Y(date.today())}'], bold=True)
        self.write_line([])
        self.write_line(header, bold=True, col_widths=col_widths)
        for benef in context['object_list']:
            self.write_line(
                [benef.nom_prenom(), benef.adresse_officielle(), format_d_m_Y(benef.naissance),
                 benef.get_referents_ope(), format_d_m_Y(benef.date_demande), format_d_m_Y(benef.date_validation),
                 format_d_m_Y(benef.date_admission)]
            )
        self.write_line([])
        self.write_line(["Liste d'attente"], bold=True)
        self.write_line(['Nom, prénom', 'Adresse', 'Date de naissance', 'IPE', 'Date demande', 'Date validation'],
                        bold=True)
        for benef in context['liste_attente']:
            self.write_line(
                [benef.nom_prenom(), benef.adresse_officielle(), format_d_m_Y(benef.naissance),
                 benef.get_referents_ope(), format_d_m_Y(benef.date_demande), format_d_m_Y(benef.date_validation)]
            )
        self.write_line([])
        self.write_line(["Demandes déposées"], bold=True)
        self.write_line(['Nom, prénom', 'Adresse', 'Date de naissance', 'IPE', 'Date demande'], bold=True)
        for benef in context['liste_demande']:
            self.write_line(
                [benef.nom_prenom(), benef.adresse_officielle(), format_d_m_Y(benef.naissance),
                 benef.get_referents_ope(), format_d_m_Y(benef.date_demande)]
            )