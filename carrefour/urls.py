from django.urls import path
from carrefour import views

urlpatterns = [
    path('contact/add/', views.ContactCreateView.as_view(), name='contact-add'),
    path('contact/list/', views.ContactListView.as_view(), name='contact-list'),
    path('contact/<int:pk>/edit/', views.ContactUpdateView.as_view(), name='contact-edit'),
    path('contact/<int:pk>/delete/', views.ContactDeleteView.as_view(), name='contact-delete'),
    path('contact/dal/', views.ContactAutocompleteView.as_view(), name='contact-autocomplete'),
    path('contact/dal-ope/', views.ContactAutocompleteView.as_view(ope=True),
         name='contact-ope-autocomplete'),
    path('contact/dal-externe/', views.ContactExterneAutocompleteView.as_view(),
         name='contact-externe-autocomplete'),
    path('contact/test/doublon/', views.ContactTestDoublon.as_view(), name='contact-doublon'),
    path('contact/<int:pk>/dossier/list/', views.ContactDossiersActifsView.as_view(), name='contact-dossier'),
    path('contact/<int:pk>/merge/', views.ContactMergeView.as_view(), name='contact-merge'),

    path('service/add/', views.ServiceCreateView.as_view(), name='service-add'),
    path('service/list/', views.ServiceListView.as_view(), name='service-list'),
    path('service/<int:pk>/edit/', views.ServiceUpdateView.as_view(), name='service-edit'),
    path('service/<int:pk>/delete/', views.ServiceDeleteView.as_view(), name='service-delete'),

    # Personne
    path('<slug:source>/famille/<int:pk>/personne/add/', views.PersonneCreateView.as_view(), name="personne-add"),
    path('<slug:source>/famille/<int:pk>/personne/<int:obj_pk>/edit/', views.PersonneUpdateView.as_view(),
         name="personne-edit"),
    path('<slug:source>/famille/<int:pk>/personne/<int:obj_pk>/delete/', views.PersonneDeleteView.as_view(),
         name='personne-delete'),

    path('personne/<int:pk>/formation/', views.FormationView.as_view(), name='formation'),
    path('personne/<int:pk>/contacts/', views.PersonneReseauListView.as_view(), name='personne-reseau-list'),
    path('personne/<int:pk>/contact/add/', views.PersonneReseauAddView.as_view(), name='personne-reseau-add'),
    path('personne/<int:pk>/contact/<int:obj_pk>/remove/', views.PersonneReseauRemove.as_view(),
         name='personne-reseau-remove'),
    path('personne/<int:pk>/toggle_contract/', views.PersonneToggleContract.as_view(),
         name='personne-toggle-contract'),
    path('famille/<int:pk>/upload/', views.DocumentUploadView.as_view(), name='famille-doc-upload'),
    path('famille/<int:pk>/genogramme/upload/', views.GenogrammeUploadView.as_view(),
         name='famille-genogramme-upload'),
    path('famille/<int:pk>/sociogramme/upload/', views.SociogrammeUploadView.as_view(),
         name='famille-sociogramme-upload'),
    path('famille/<int:pk>/genogramme/delete/', views.FamilleFileDeleteView.as_view(field_name='genogramme'),
         name='famille-genogramme-delete'),
    path('famille/<int:pk>/sociogramme/delete/', views.FamilleFileDeleteView.as_view(field_name='sociogramme'),
         name='famille-sociogramme-delete'),

    path('famille/<int:pk>/doc/<int:doc_pk>/delete/', views.DocumentDeleteView.as_view(), name='famille-doc-delete'),

    path('utilisateur/', views.UtilisateurListView.as_view(), name='utilisateur-list'),
    path('utilisateur/<int:pk>/edit/', views.UtilisateurUpdateView.as_view(), name='utilisateur-edit'),
    path('utilisateur/add/', views.UtilisateurCreateView.as_view(), name='utilisateur-add'),
    path('utilisateur/<int:pk>/delete/', views.UtilisateurDeleteView.as_view(), name='utilisateur-delete'),
    path('utilisateur/<int:pk>/password_reinit/', views.UtilisateurPasswordReinitView.as_view(),
         name='utilisateur-password-reinit'),
    path('utilisateur/dal/', views.UtilisateurAutocompleteView.as_view(), name='utilisateur-autocomplete'),
    path('utilisateur/<int:pk>/otp_device/reinit/', views.UtilisateurOtpDeviceReinitView.as_view(),
         name='utilisateur-otp-device-reinit'),
    path('utilisateur/desactive/list/', views.UtilisateurListView.as_view(is_active=False),
         name='utilisateur-desactive-list'),
    path('utilisateur/<int:pk>/reactiver/', views.UtilisateurReactivateView.as_view(),
         name='utilisateur-reactiver'),

    path('utilisateur/<int:pk>/derogation/add/', views.DerogationSaisieTardiveCreateView.as_view(),
         name='utilisateur-derogation-add'),
    path('utilisateur/<int:pk>/derogation/<int:obj_pk>/edit/', views.DerogationSaisieTardiveUpdateView.as_view(),
         name='utilisateur-derogation-edit'),


    path('cerclescolaire/', views.CercleScolaireListView.as_view(), name='cercle-list'),
    path('cerclescolaire/<int:pk>/edit/', views.CercleScolaireUpdateView.as_view(), name='cercle-edit'),
    path('cerclescolaire/add/', views.CercleScolaireCreateView.as_view(), name='cercle-add'),
    path('cerclescolaire/<int:pk>/delete/', views.CercleScolaireDeleteView.as_view(), name='cercle-delete'),

    path('role/', views.RoleListView.as_view(), name='role-list'),
    path('role/<int:pk>/edit/', views.RoleUpdateView.as_view(), name='role-edit'),
    path('role/add/', views.RoleCreateView.as_view(), name='role-add'),
    path('role/<int:pk>/delete/', views.RoleDeleteView.as_view(), name='role-delete'),

    path('permissions/', views.PermissionOverview.as_view(), name='permissions'),

    path('export/prestation/', views.ExportPrestationView.as_view(), name='export-prestation'),
]
