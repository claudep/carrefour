import calendar
import re

from bootstrap_datepicker_plus.widgets import DatePickerInput, TimePickerInput
from dal import autocomplete
from dal.widgets import WidgetMixin
from datetime import date, timedelta

from django import forms
from django.utils import timezone
from django.utils.dates import MONTHS

from carrefour.models import (
    CercleScolaire, Contact, Document, Formation, Personne, Role, Service,
    Utilisateur, DerogationSaisieTardive
)


class BootstrapMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for ffield in self.fields.values():
            if isinstance(ffield.widget, WidgetMixin) or getattr(ffield.widget, '_bs_enabled', False):
                continue
            elif isinstance(ffield.widget, (forms.Select, forms.NullBooleanSelect)):
                self.add_attr(ffield.widget, 'form-select')
            elif isinstance(ffield.widget, (forms.CheckboxInput, forms.RadioSelect)):
                self.add_attr(ffield.widget, 'form-check-input')
            else:
                self.add_attr(ffield.widget, 'form-control')

    @staticmethod
    def add_attr(widget, class_name):
        if 'class' in widget.attrs:
            widget.attrs['class'] += f' {class_name}'
        else:
            widget.attrs.update({'class': class_name})


class BootstrapChoiceMixin:
    """
    Mixin to customize choice widgets to set 'form-check' on container and
    'form-check-input' on sub-options.
    """
    _bs_enabled = True

    def get_context(self, *args, **kwargs):
        context = super().get_context(*args, **kwargs)
        if 'class' in context['widget']['attrs']:
            context['widget']['attrs']['class'] += ' form-check'
        else:
            context['widget']['attrs']['class'] = 'form-check'
        return context

    def create_option(self, *args, attrs=None, **kwargs):
        attrs = attrs or {}
        if 'class' in attrs:
            attrs['class'] += ' form-check-input'
        else:
            attrs.update({'class': 'form-check-input'})
        return super().create_option(*args, attrs=attrs, **kwargs)


class CleanNpaLocaliteMixin(forms.Form):

    def clean_localite(self):
        localite = self.cleaned_data['localite']
        digit = re.search(r"\d", localite)
        if digit:
            raise forms.ValidationError('Le nom de la localité est incorrect.')
        return localite

    def clean(self):
        cleaned_data = super().clean()
        npa = cleaned_data.get("npa")
        localite = cleaned_data.get("localite")
        if localite and npa == '':
            raise forms.ValidationError("Vous devez saisir un NPA pour la localité.")


class BSCheckboxSelectMultiple(BootstrapChoiceMixin, forms.CheckboxSelectMultiple):
    pass


class BSRadioSelect(BootstrapChoiceMixin, forms.RadioSelect):
    pass


class HMDurationField(forms.DurationField):
    """A duration field taking HH:MM as input."""
    widget = forms.TextInput(attrs={'placeholder': 'hh:mm'})

    def to_python(self, value):
        if value in self.empty_values or isinstance(value, timedelta):
            return super().to_python(value)
        if len(value) < 6:
            value += ':00'  # Simulate seconds
        return super().to_python(value)

    def prepare_value(self, value):
        value = super().prepare_value(value)
        if value:
            return value[:5]  # Remove seconds
        return value


class PickDateWidget(DatePickerInput):
    options = {
        'format': 'DD.MM.YYYY',
        'locale': 'fr'
    }


class PickTimeWidget(TimePickerInput):
    options = {
        "locale": "fr",
        "format": "HH:mm",
        "showTodayButton": False,
    }


class PickDateTimeWidget(forms.SplitDateTimeWidget):
    template_name = 'widgets/datetimewidget.html'

    def __init__(self, attrs=None):
        widgets = [PickDateWidget, PickTimeWidget]
        forms.MultiWidget.__init__(self, widgets, attrs)


class DateInput(forms.DateInput):
    """Native date picker (e.g in modals)."""
    input_type = 'date'

    def format_value(self, value):
        return str(value) if value is not None else None


class ContactForm(BootstrapMixin, CleanNpaLocaliteMixin, forms.ModelForm):
    class Meta:
        model = Contact
        fields = ['role', 'nom', 'prenom', 'profession', 'rue', 'npa', 'localite', 'telephone', 'email',
                  'service', 'remarque']

    def __init__(self, *args, show_role_service=True, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['service'].queryset = Service.objects.exclude(sigle='CARREFOUR')
        self.fields['role'].queryset = Role.objects.exclude(famille=True).order_by('nom')
        if not show_role_service:
            self.fields['role'].widget = forms.HiddenInput()
            self.fields['service'].widget = forms.HiddenInput()


class ContactMergeForm(ContactForm):
    doublons = forms.CharField(widget=forms.HiddenInput())


class RoleForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = Role
        fields = '__all__'


class ServiceForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = Service
        fields = ['sigle', 'nom_complet']


class FormationForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = Formation
        exclude = ('personne',)


class CercleScolaireForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = CercleScolaire
        fields = ['nom', 'telephone']


class UtilisateurForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = Utilisateur
        fields = ['nom', 'prenom', 'sigle', 'telephone', 'username', 'email', 'taux_activite',
                  'groups']
        widgets = {'groups': BSCheckboxSelectMultiple}


class PersonneForm(BootstrapMixin, CleanNpaLocaliteMixin, forms.ModelForm):
    role = forms.ModelChoiceField(label="Rôle", queryset=Role.objects.filter(famille=True), required=True)
    contractant = forms.ChoiceField(
        label="Personne partie au contrat", choices=((True, 'Oui'), (False, 'Non')),
        initial=True, required=False,
    )

    class Meta:
        model = Personne
        fields = [
            'role', 'nom', 'prenom', 'date_naissance', 'genre', 'filiation', 'contractant',
            'rue', 'npa', 'localite', 'telephone',
            'email', 'profession', 'assurance', 'permis', 'remarque', 'remarque_privee',
        ]
        widgets = {
            'date_naissance': PickDateWidget,
        }
        labels = {
            'remarque_privee': 'Remarque privée (pas imprimée)',
        }

    def __init__(self, **kwargs):
        self.famille = kwargs.pop('famille', None)
        role_id = kwargs.pop('role', None)

        super().__init__(**kwargs)
        if self.famille:
            self.initial['nom'] = self.famille.nom
            self.initial['rue'] = self.famille.rue
            self.initial['npa'] = self.famille.npa
            self.initial['localite'] = self.famille.localite
            self.initial['telephone'] = self.famille.telephone
            if self.famille.typ == 'aemo':
                self.fields.pop('assurance')
                self.fields.pop('permis')

        if role_id:
            if role_id == 'ps':  # personne significative
                excl = ['Père', 'Mère', 'Enfant suivi', 'Enfant non-suivi']
                self.fields['role'].queryset = Role.objects.filter(
                    famille=True).exclude(
                    nom__in=excl
                )
            else:
                self.role = Role.objects.get(pk=role_id)

                if self.role.nom in ['Père', 'Mère']:
                    self.fields['role'].queryset = Role.objects.filter(nom=self.role.nom)
                    self.initial['genre'] = 'M' if self.role.nom == 'Père' else 'F'

                if self.role.nom in ['Enfant suivi', 'Enfant non-suivi']:
                    self.fields['role'].queryset = Role.objects.filter(nom__in=['Enfant suivi', 'Enfant non-suivi'])
                    self.fields['profession'].label = 'Profession/École'
                    del self.fields['contractant']
                self.initial['role'] = self.role.pk
        else:
            if self.instance.pk:
                famille = self.instance.famille
                if self.instance.role.nom in ['Père', 'Mère']:
                    self.fields['role'].queryset = Role.objects.filter(nom=self.instance.role)
                elif self.instance.role.nom in ['Enfant suivi', 'Enfant non-suivi']:
                    self.fields['role'].queryset = Role.objects.filter(nom__in=['Enfant suivi', 'Enfant non-suivi'])
                    del self.fields['contractant']
                elif self.instance.role.nom not in ['Père', 'Mère', 'Enfant suivi', 'Enfant non-suivi']:
                    excl = ['Père', 'Mère', 'Enfant suivi', 'Enfant non-suivi']
                    if not famille.pere():
                        excl.remove('Père')
                    if not famille.mere():
                        excl.remove('Mère')
                    self.fields['role'].queryset = Role.objects.filter(
                        famille=True).exclude(
                        nom__in=excl)

    def clean_role(self):
        role = self.cleaned_data['role']
        # Assurer l'unicité des rôles 'Père' et 'Mère' dans la famille
        if ('role' in self.changed_data and role.nom == 'Père' and
                self.famille.membres.filter(role__nom='Père').exists()):
            raise forms.ValidationError('Une personne a déjà le rôle de Père !')
        if ('role' in self.changed_data and role.nom == 'Mère' and
                self.famille.membres.filter(role__nom='Mère').exists()):
            raise forms.ValidationError('Une personne a déjà le rôle de Mère !')
        return role

    def save(self, **kwargs):
        if self.instance.pk is None:
            self.instance.famille = self.famille
            pers = Personne.objects.create_personne(**self.instance.__dict__)
        else:
            pers = super().save(**kwargs)
            pers = Personne.objects.add_formation(pers)
        return pers


class AgendaFormBase(BootstrapMixin, forms.ModelForm):
    def clean(self):
        cleaned_data = super().clean()
        motif = cleaned_data.get('motif_fin_suivi')
        date_fin = cleaned_data.get('date_fin_suivi', None)

        if motif and (motif in ['erreur', 'non_aboutie'] or motif.startswith('abandon_')) and date_fin:
            raise forms.ValidationError("Vous ne pouvez plus modifier le dossier.")
        if motif and (motif in ['erreur', 'non_aboutie'] or motif.startswith('abandon_')):
            if not cleaned_data.get('date_fin_suivi'):
                cleaned_data['date_fin_suivi'] = date.today()
            return cleaned_data

        # Check date chronology
        date_preced = None
        for field_name in [field for field in self._meta.fields if field.startswith('date_')]:
            dt = cleaned_data.get(field_name)
            if not dt:
                continue
            if date_preced and dt < date_preced:
                raise forms.ValidationError(
                    "La date «{}» ne respecte pas l’ordre chronologique!".format(self.fields[field_name].label)
                )
            date_preced = dt

        if cleaned_data.get('motif_fin_suivi') and cleaned_data.get('date_fin_suivi') is None:
            self.add_error('date_fin_suivi', forms.ValidationError('La date «Fin de suivi» est obligatoire'))
        if cleaned_data.get('date_fin_suivi') and not cleaned_data.get('motif_fin_suivi'):
            self.add_error('motif_fin_suivi', forms.ValidationError('Le champ «Motif de fin» est obligatoire.'))

        workflow = self._meta.model.WORKFLOW
        for field, etape in reversed((workflow.items())):
            if field == 'archivage':
                continue
            date_etape = cleaned_data.get(etape.date_nom())
            etape_courante = etape
            etape_preced_oblig = workflow[etape.preced_oblig]
            date_preced_oblig = cleaned_data.get(etape_preced_oblig.date_nom())
            while True:
                etape_preced = workflow[etape_courante.precedente]
                date_preced = cleaned_data.get(etape_preced.date_nom())
                if date_preced is None and etape_courante.num > 1:
                    etape_courante = etape_preced
                else:
                    break
            if date_etape and date_preced_oblig is None:
                raise forms.ValidationError("La date «{}» est obligatoire".format(etape_preced_oblig.nom))
        return cleaned_data


class PrestationFormBase(BootstrapMixin, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super().__init__(*args, **kwargs)

    class Meta:
        exclude = ('famille', 'intervenants')
        widgets = {
            'date_prestation': PickDateWidget,
            'duree': PickTimeWidget,
        }
        field_classes = {
            'duree': HMDurationField,
        }

    def clean_date_prestation(self):
        date_prestation = self.cleaned_data['date_prestation']
        today = date.today()
        if date_prestation > today:
            raise forms.ValidationError("La saisie anticipée est impossible !")

        if DerogationSaisieTardive.objects.filter(educ=self.user, duree__contains=today).exists():
            return self.cleaned_data['date_prestation']

        if not self.instance.check_date_editable(date_prestation, self.user):
            raise forms.ValidationError(
                "La saisie des prestations des mois précédents est close !"
            )
        return self.cleaned_data['date_prestation']


class DocumentUploadForm(BootstrapMixin, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super().__init__(*args, **kwargs)

    class Meta:
        model = Document
        exclude = ('date_creation', 'auteur')
        widgets = {'famille': forms.HiddenInput}
        labels = {'fichier': ''}

    def save(self, *args, **kwargs):
        if self.instance.pk is None:
            self.instance.auteur = self.user
            self.instance.date_creation = timezone.now()
        return super().save(*args, **kwargs)


class MonthSelectionForm(BootstrapMixin, forms.Form):
    year = date.today().year
    mois = forms.ChoiceField(choices=((mois_idx, MONTHS[mois_idx]) for mois_idx in range(1, 13)))
    annee = forms.ChoiceField(
        label='Année',
        choices = ((y, y) for y in range(year, 2018, -1))
    )


class ArchivesForm(forms.Form):
    pass
    # aemo = forms.BooleanField(label="AEMO", required=False)
    # asaef = forms.BooleanField(label='ASAEF', required=False)
    # batoude = forms.BooleanField(label='la Batoude', required=False)


class ContactExterneAutocompleteForm(forms.ModelForm):

    class Meta:
        model = Personne
        fields = ('reseaux',)
        widgets = {'reseaux': autocomplete.ModelSelect2Multiple(url='contact-externe-autocomplete')}
        labels = {'reseaux': 'Contacts'}


class JournalPopupForm(forms.Form):
    selector = forms.ChoiceField(
        label="À choix :",
        choices=(('month', 'le mois courant'), ('two_months', 'les deux derniers mois'),
                 ('last_10', 'les 10 dernières prestations'), ('all', 'tout le journal')),
        widget=forms.RadioSelect()
    )


class DateLimitForm(forms.Form):
    YEAR_CHOICES = tuple(
        (str(y), str(y))
        for y in range(2020, date.today().year + (1 if date.today().month < 12 else 2))
    )
    start_month = forms.ChoiceField(choices=[(str(m), MONTHS[m]) for m in range(1, 13)])
    start_year = forms.ChoiceField(choices=YEAR_CHOICES)
    end_month = forms.ChoiceField(choices=[(str(m), MONTHS[m]) for m in range(1, 13)])
    end_year = forms.ChoiceField(choices=YEAR_CHOICES)

    def __init__(self, data, **kwargs):
        if not data:
            today = date.today()
            data = {
                'start_year': today.year, 'start_month': 1,
                'end_year': today.year,
                'end_month': today.month,
            }
        super().__init__(data, **kwargs)

    def clean(self):
        cleaned_data = super().clean()
        if not self.errors and self.start > self.end:
            raise forms.ValidationError("Les dates ne sont pas dans l’ordre.")
        return cleaned_data

    @property
    def start(self):
        return date(int(self.cleaned_data['start_year']), int(self.cleaned_data['start_month']), 1)

    @property
    def end(self):
        return date(
            int(self.cleaned_data['end_year']),
            int(self.cleaned_data['end_month']),
            calendar.monthrange(int(self.cleaned_data['end_year']), int(self.cleaned_data['end_month']))[1]
        )


class DerogationSaisieTardiveForm(forms.ModelForm):

    date_debut = forms.DateField(label='Du', widget=PickDateWidget, required=True)
    date_fin = forms.DateField(label='Au', widget=PickDateWidget, required=False)

    class Meta:
        model = DerogationSaisieTardive
        fields = ('date_debut', 'date_fin')

    def __init__(self, *args, **kwargs):
        self.educ = kwargs.pop('educ')
        self.instance = kwargs.pop('instance', None)
        super().__init__(*args, **kwargs)
        if self.instance and self.instance.pk:
            self.fields['date_debut'].initial = self.instance.duree[0]
            self.fields['date_fin'].initial = self.instance.duree[1]

    def save(self, commit=True):
        self.instance.educ = self.educ
        self.instance.duree = (self.cleaned_data['date_debut'], self.cleaned_data['date_fin'])
        return super().save()

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['date_debut'] == cleaned_data['date_fin']:
            raise forms.ValidationError("Il faut au moins 24 heures entre les deux dates.")

        if cleaned_data['date_debut'] > cleaned_data['date_fin']:
            raise forms.ValidationError("L'ordre chronologique n'est pas respecté.")



