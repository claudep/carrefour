from django.apps import AppConfig


class CarrefourConfig(AppConfig):
    name = 'carrefour'

    def ready(self):
        from carrefour.pdf import BaseCarrefourPDF
        BaseCarrefourPDF.register_fonts()
