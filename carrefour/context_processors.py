from urllib.parse import urlparse


def current_app(request):
    def find_source(path):
        src = path.strip('/').split('/')[0]
        if src in ['aemo', 'asaef', 'batoude', 'asap']:
            return src
        return ''

    source = find_source(request.path)
    if source == '':
        source = find_source(urlparse(request.META.get('HTTP_REFERER', '')).path)
    if source == '':
        source = request.GET.get('from', '')
    return {'source': source}
