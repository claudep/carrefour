import random
import string

from datetime import date, timedelta
from django.utils.dateformat import format as django_format


def format_d_m_Y(date):
    return django_format(date, 'd.m.Y') if date else ''


def format_d_m_Y_HM(date):
    return django_format(date, 'l d.m.Y - G:i') if date else ''


def format_m_Y(date):
    return django_format(date, "F Y")


def format_j_F_Y(date):
    return django_format(date, "j F Y")


def format_duree(duree):
    if duree in ['', None]:
        return '00:00'
    secondes = duree.total_seconds()
    heures = secondes // 3600
    minutes = (secondes % 3600) // 60
    return '{:02}:{:02}'.format(int(heures), int(minutes))


def format_adresse(rue, npa, localite):
    """ Formate the complete adress """

    if rue and npa and localite:
        return f"{rue}, {npa} {localite}"
    if npa and localite:
        return f"{npa} {localite}"
    return f"{localite}"


def format_contact(telephone, email):
    """ Formate the contact data (phone and email) """
    return '{} {} {}'.format(telephone, '-' if telephone != '' and email != '' else '', email)


def is_ajax(request):
    return request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest'


def get_selected_date(dt):
    if dt and len(dt) != 6 and dt.isdigit() is False:
        raise ValueError("Erreur de paramètre")
    month = int(dt[:2]) if dt else date.today().month
    year = int(dt[-4:]) if dt else date.today().year
    dfrom = date(year, month, 1)
    dto = (dfrom + timedelta(days=33)).replace(day=1)
    return dfrom, dto


def str_or_empty(value):
    return '' if not value else str(value)


def delay_for_destruction(date_fin):
    """
    Return True if suivi was finished last year (or older) and more than
    6 months ago and we are past January 31 only for suivi finished previous year.
    (past year stats done).
    """
    if date_fin is None:
        return False
    return (
        (date.today() - date_fin).days > 180 and
        date_fin.year < date.today().year and
        ((date.today().year - date_fin.year) > 1 or date.today().month > 1)
    )


def random_string_generator(size=10, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))
