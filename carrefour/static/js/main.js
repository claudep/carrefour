function dateFormat(input) {
    if (input) {
        var dt = new Date(input);
        return dt.toLocaleDateString("fr-CH");
    }
    return '-';
}

var changed = false;

function check_changed(ev) {
    if (changed) {
        alert("Vos données n'ont pas été sauvegardées !");
        ev.preventDefault();
        return false;
    }
}

function toggle_read_more(ev) {
    ev.preventDefault();
    this.innerHTML = (this.innerHTML == 'Afficher la suite') ? 'Réduire' : 'Afficher la suite';
    $(this).parent().find('.long').toggle();
    $(this).parent().find('.short').toggle();
}

function toggle_display_more(ev) {
    ev.preventDefault();
    var title = $(this).closest('tr');
    var title_id = title.attr("id");
    if (title.hasClass('prestation_titre')) {
        title.removeClass('prestation_titre');
        $('.am' + title_id).removeClass('prestation');
    }
    else {
        title.addClass('prestation_titre');
        $('.am' + title_id).addClass('prestation');
    }
    $('.am' + title_id).toggle();
}

function debounce(func, timeout=300) {
  let timer;
  return (...args) => {
    if (timeout <= 0) func.apply(this, args);
    else {
      clearTimeout(timer);
      timer = setTimeout(() => { func.apply(this, args); }, timeout);
    }
  };
}

function archiveFamilies(ev) {
    const btn = ev.target;
    const archiveUrl = btn.dataset.archiveurl;
    const counterSpan = $('#archive-counter');
    const totalSpan = $('#archive-total');
    const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;

    bootstrap.Modal.getInstance(document.getElementById('archiveModal')).hide();
    document.getElementById('archive-message').removeAttribute('hidden');
    $.get({
        url: btn.dataset.getarchivableurl,
        success: function(data){
            let compteur = 0;
            if (data.length == 0) {
                const messageP = $("#archive-message p");
                messageP.text("Il n'y a aucun dossier à archiver.");
                messageP.removeClass('alert-danger').addClass('alert-success');
            } else {
                totalSpan.text(data.length);
                $.each(data, function(index, pk) {
                    $.post({
                        url: archiveUrl.replace('999', pk),
                        data: {csrfmiddlewaretoken: csrftoken},
                        delay: 50
                    }).done(function(response) {
                        compteur += 1;
                        counterSpan.text(compteur);
                        $('tr[data-famille="' + pk + '"]').remove();
                        if (compteur == data.length){
                            const messageP = $("#archive-message p");
                            messageP.text(data.length + ' dossiers ont été archivés avec succès.');
                            messageP.removeClass('alert-danger').addClass('alert-success');
                        }
                    });
                });
            }
        }
    });
}

function resetForm(ev) {
    const form = ev.target.closest('form');
    Array.from(form.elements).forEach(el => { el.value = ''; });
    form.submit();
}

function incrementTotalForms(form, numb) {
    const formTotal = form.querySelector('[id$="-TOTAL_FORMS"]');
    const totalValue = parseInt(formTotal.value);
    formTotal.value = totalValue + numb;
    return totalValue + numb;
}

function show(selector_or_el) {
    if (typeof selector_or_el === 'string' || selector_or_el instanceof String) {
        document.querySelector(selector_or_el).removeAttribute('hidden');
    } else {
        selector_or_el.removeAttribute('hidden');
    }
}

function setConfirmHandlers(section) {
    if (typeof section === 'undefined') section = document;
    const selector = section.querySelectorAll(".btn-danger, .confirm");
    selector.forEach(button => {
        button.addEventListener('click', ev => {
            if (button.dataset.confirm) {
                ev.preventDefault();
                if (!confirm(button.dataset.confirm)) {
                    return false;
                } else {
                    if (button.getAttribute('formaction')) button.form.action = button.formAction;
                    button.form.submit();
                }
            }
        });
    });
}

async function openFormInModal(url) {
    const popup = document.querySelector('#popup0');

    function setupForm() {
        setConfirmHandlers(popup);
        document.querySelectorAll("#popup0 form").forEach((form) => {
            form.addEventListener('submit', (ev) => {
                ev.preventDefault();
                const form = ev.target;
                const formData = new FormData(form);
                // GET/POST with fetch
                let url = form.action;
                let params = {method: form.method};
                if (form.method == 'post') {
                    params['body'] = formData;
                }
                fetch(url, params).then(res => {
                    if (res.redirected) {
                        window.location.reload(true);
                        return '';
                    }
                    return res.text();
                }).then(html => {
                    if (html) {
                        // Redisplay form with errors or display confirm page
                        popup.querySelector('.modal-body').innerHTML = html;
                        setupForm();
                    }
                }).catch((err) => {
                    console.log(err);
                    alert("Désolé, une erreur s'est produite");
                });
            });
        });
    }

    const resp = await fetch(url);
    const html = await resp.text();
    const modal = new bootstrap.Modal(popup);
    popup.querySelector('.modal-body').innerHTML = html;
    modal.show();
    setupForm();
}

document.addEventListener('DOMContentLoaded', () => {
    autosize(document.querySelectorAll('textarea'));
    document.querySelectorAll('form:not(.selection_form)').forEach((frm) => {
        frm.addEventListener('change', () => {changed = true});
    });
    document.querySelectorAll('#my_buttons, #menu_aemo, #print_buttons').forEach((item) => {
        item.addEventListener('click', check_changed);
    });
    document.querySelectorAll('.btn-danger, .confirm').forEach((item) => {
        item.addEventListener('click', (ev) => {
            if (ev.target.dataset.confirm) {
                if (!confirm(ev.target.dataset.confirm)) {
                    ev.preventDefault();
                    return false;
                }
            }
        });
    });
    document.querySelectorAll('table.sortable').forEach((tbl) => {
        new Tablesort(tbl);
    });

    document.querySelectorAll(".js-add, .js-edit").forEach((item) => {
        item.addEventListener('click', (ev) => {
            ev.preventDefault();
            const url = ev.currentTarget.dataset.url || ev.currentTarget.href;
            openFormInModal(url);
            return false;
        });
    });

    const resetBtn = document.querySelector('#reset-button');
    if (resetBtn) resetBtn.addEventListener('click', resetForm);

    document.querySelectorAll(".js-add-item-formset").forEach((item) => {
        item.addEventListener('click', (ev) => {
            ev.preventDefault();
            const form = ev.target.closest('form');
            const emptyForm = form.querySelector('.empty_form');
            let newElement = emptyForm.cloneNode(true);
            newElement.classList.remove('empty_form');
            const newTotal = incrementTotalForms(form, 1);
            newElement.querySelectorAll('input').forEach(inp => {
                for (const attr of inp.attributes) {
                    if (attr.value.includes('__prefix__')) {
                       inp.setAttribute(attr.name, attr.value.replace('__prefix__', newTotal - 1));
                    }
                    if (attr.value.includes('-ORDER')) {
                       inp.setAttribute('value', newTotal)
                    }
                }
            });
            show(newElement);
            emptyForm.parentNode.insertBefore(newElement, emptyForm);
            return newElement;
        });
    });
});
