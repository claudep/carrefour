import bleach
from functools import partial

import extract_msg
from reportlab.lib import colors
from reportlab.lib.enums import TA_LEFT, TA_CENTER
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import ParagraphStyle, getSampleStyleSheet
from reportlab.lib.units import cm
from reportlab.pdfbase.pdfmetrics import registerFont, registerFontFamily
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from reportlab.platypus import (
    Paragraph, SimpleDocTemplate, Spacer, Table, TableStyle
)

from django.contrib.staticfiles.finders import find
from django.utils.text import slugify

from carrefour.utils import format_d_m_Y
from settings.carrefour import CARREFOUR_NOM, CARREFOUR_CONTACT


class PageNumCanvas(canvas.Canvas):
    """A special canvas to be able to draw the total page number in the footer."""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._pages = []

    def showPage(self):
        self._pages.append(dict(self.__dict__))
        self._startPage()

    def save(self):
        page_count = len(self._pages)
        for page in self._pages:
            self.__dict__.update(page)
            self.draw_page_number(page_count)
            canvas.Canvas.showPage(self)
        super().save()

    def draw_page_number(self, page_count):
        self.setFont('Carlito', 7)
        self.drawRightString(
            self._pagesize[0] - 1.6*cm, 1.2 * cm, "p. %s/%s" % (self._pageNumber, page_count)
        )


class CleanParagraph(Paragraph):
    """If the (HTML) text cannot be parsed, try to clean it."""
    def __init__(self, text, *args, **kwargs):
        if text:
            text = text.replace('</p>', '</p><br />')
        try:
            super().__init__(text, *args, **kwargs)
        except ValueError:
            text = bleach.clean(
                text, tags=['p', 'br', 'b', 'strong', 'u', 'i', 'em'], strip=True
            ).replace('<br>', '<br/>')
        super().__init__(text, *args, **kwargs)


class StyleMixin:
    MAXLINELENGTH = 120

    def __init__(self, **kwargs):
        self.styles = getSampleStyleSheet()
        font_name = self.base_font_name
        font_size = self.base_font_size
        self.style_title = ParagraphStyle(
            name='title', fontName=f'{font_name}-Bold', fontSize=font_size + 4, leading=(font_size + 4) * 1.2,
            alignment=TA_CENTER, spaceAfter=1.5*cm,
        )
        self.style_normal = ParagraphStyle(
            name='normal', fontName=font_name, fontSize=font_size,
            leading=font_size * 1.2, alignment=TA_LEFT, spaceAfter=0,
        )
        self.style_sub_title = ParagraphStyle(
            name='sous_titre', fontName=f'{font_name}-Bold',
            fontSize=font_size + 2, leading=(font_size + 2) * 1.2,
            alignment=TA_LEFT, spaceBefore=0.5 * cm, spaceAfter=0 * cm,
        )
        self.style_inter_title = ParagraphStyle(
            name='inter_titre', fontName=f'{font_name}-Bold', fontSize=font_size + 1,
            leading=(font_size + 1) * 1.2, alignment=TA_LEFT, spaceBefore=0.3 * cm, spaceAfter=0,
        )
        self.style_bold = ParagraphStyle(
            name='bold', fontName=f'{font_name}-Bold', fontSize=font_size,
            leading=font_size * 1.2
        )
        self.style_italic = ParagraphStyle(
            name='italic', fontName=f'{font_name}-Italic', fontSize=font_size - 1,
            leading=(font_size - 1) * 1.2
        )
        self.style_indent = ParagraphStyle(
            name='indent', fontName=font_name, fontSize=font_size,
            leading=font_size * 1.2, alignment=TA_LEFT, leftIndent=1 * cm,
        )
        super().__init__(**kwargs)


class HeaderFooterMixin:
    LOGO = find('img/logo.png')

    def draw_header(self, canvas, doc):
        canvas.saveState()
        canvas.drawImage(
            self.LOGO, doc.leftMargin, doc.height+40, 5 * cm, 3 * cm, preserveAspectRatio=True
        )
        canvas.restoreState()

    def draw_footer(self, canvas, doc):
        canvas.saveState()
        canvas.setLineWidth(0.5)
        canvas.line(doc.leftMargin, 2 * cm, doc.width + doc.leftMargin, 2 * cm)
        canvas.setFont(self.base_font_name, 8)
        canvas.drawCentredString(doc.leftMargin + doc.width / 2, 1.6 * cm, CARREFOUR_NOM)
        canvas.drawCentredString(doc.leftMargin + doc.width / 2, 1.2 * cm, CARREFOUR_CONTACT)
        canvas.restoreState()


class BaseCarrefourPDF(HeaderFooterMixin, StyleMixin):
    base_font_name = 'Carlito'
    base_font_size = 11

    def __init__(self, tampon, with_page_num=True, **kwargs):
        self.doc = SimpleDocTemplate(
            tampon, title=self.title, pagesize=A4,
            leftMargin=1.5 * cm, rightMargin=1.5 * cm, topMargin=1.5 * cm, bottomMargin=3 * cm
        )
        self.with_page_num = with_page_num
        self.story = []
        super().__init__(**kwargs)

    @classmethod
    def register_fonts(cls):
        """Called once on in apps.ready() to prevent "redefining named object" errors"""
        registerFont(TTFont('Carlito', 'Carlito-Regular.ttf'))
        registerFont(TTFont('Carlito-Bold', 'Carlito-Bold.ttf'))
        registerFont(TTFont('Carlito-Italic', 'Carlito-Italic.ttf'))
        registerFontFamily(
            family='Carlito', normal='Carlito', bold='Carlito-Bold', italic='Carlito-Italic'
        )

    def draw_header_footer(self, canvas, doc):
        self.draw_header(canvas, doc)
        self.draw_footer(canvas, doc)

    def set_title(self, title=''):
        title = title or getattr(self, 'title')
        if title:
            self.story.append(Spacer(0, 1 * cm))
            self.story.append(Paragraph(title, self.style_title))

    def print_parents(self, story, famille):
        story.append(Paragraph('Parents', self.style_sub_title))
        data = self.formate_persons([famille.mere(), famille.pere()])
        story.append(self.get_table(data, columns=[2, 7, 2, 7], before=0, after=0))

        # Situation matrimoniale
        data = [
            ['Situation matrimoniale: {}'.format(famille.get_statut_marital_display()),
             'Autorité parentale: {}'.format(famille.get_autorite_parentale_display())],
        ]
        story.append(self.get_table(data, columns=[9, 9], before=0, after=0))

    def print_personnes_significatives(self, story, famille):
        autres_parents = list(famille.autres_parents())
        if autres_parents:
            story.append(Paragraph('Personne-s significative-s', self.style_sub_title))
            data = self.formate_persons(autres_parents)
            story.append(self.get_table(data, columns=[2, 7, 3, 6], before=0, after=0))

    def produce(self, *args, **kwargs):
        self.doc.build(
            self.story,
            onFirstPage=self.draw_header_footer, onLaterPages=self.draw_footer,
            canvasmaker=PageNumCanvas if self.with_page_num else canvas.Canvas
        )

    def formate_persons(self, parents_list):
        labels = (
            ("Nom", "nom"), ("Prénom", "prenom"), ("Adresse", "rue"),
            ("Localité", 'localite_display'), ("Profession", 'profession'), ("Tél.", 'telephone'),
            ("Courriel", 'email'), ('Remarque', 'remarque'),
        )
        P = partial(Paragraph, style=self.style_normal)
        Pbold = partial(Paragraph, style=self.style_bold)

        data = []
        for idx, parent1 in enumerate(parents_list):
            if idx % 2 == 0:
                role1 = parent1.role.nom if parent1 and parent1.role else ''
                if idx+1 < len(parents_list):
                    parent2 = parents_list[idx+1]
                    role2 = parent2.role.nom if parent2 and parent2.role else ''
                    role2 = 'Père' if role1 == 'Mère' else role2
                    data.append([Pbold(role1), '', Pbold(role2), ''])
                    data.extend(
                        ['{}: '.format(label), P(getattr(parent1, field) if parent1 else ''),
                         '{}: '.format(label), P(getattr(parent2, field) if parent2 else '')
                         ] for label, field in labels
                    )
                else:
                    data.append([Pbold(role1), ''])
                    data.extend(
                        ['{}: '.format(label), P(getattr(parent1, field) if parent1 else '')
                         ] for label, field in labels
                    )
        return data

    def get_table(self, data, columns, before=0.0, after=0.0, inter=None, bordered=None):
        """Prepare a Table instance with data and columns, with a common style."""
        if inter:
            inter = inter * cm
        cols = [c * cm for c in columns]

        t = Table(
            data=data, colWidths=cols, hAlign=TA_LEFT,
            spaceBefore=before * cm, spaceAfter=after * cm
        )
        t.hAlign = 0
        styles_list = [
            ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
            ('FONTSIZE', (0, 0), (-1, -1), self.base_font_size),
            ('VALIGN', (0, 0), (-1, -1), "TOP"),
            ('LEFTPADDING', (0, 0), (0, -1), 5),
            ('LEADING', (0, 0), (-1, -1), 9),
        ]
        if bordered:
            styles_list.append(('BOX', (0, 0), (-1, -1), 0.25, colors.black))
        t.setStyle(tblstyle=TableStyle(styles_list))
        return t

    def print_chronologie(self, famille, **kwargs):
        P = partial(Paragraph, style=self.style_normal)
        Pbold = partial(Paragraph, style=self.style_bold)

        fields_aemo = [
            'date_demande', 'date_analyse', 'date_confirmation', 'date_debut_suivi', 'date_fin_suivi',
            'date_bilan_3mois', 'date_bilan_annee', 'date_bilan_final', 'date_prolongation_1',
            'date_bilan_prolongation_1', 'date_prolongation_2', 'date_bilan_prolongation_2'
        ]

        fields_asaef = [
            'date_demande', 'date_analyse', 'date_bilan_analyse', 'date_debut_suivi', 'date_bilan_1mois',
            'date_bilan_3mois', 'date_bilan_final', 'date_prolongation', 'date_fin_suivi'
        ]
        fields = fields_aemo if famille.typ == 'aemo' else fields_asaef
        data = []
        self.story.append(Paragraph('Chronologie', self.style_sub_title))
        for field_name in fields:
            if getattr(famille.suivi, field_name):
                field = famille.suivi._meta.get_field(field_name)
                data.append(
                    [Pbold(f"{field.verbose_name} :"), P(format_d_m_Y(getattr(famille.suivi, field_name)))]
                )
            if famille.suivi.motif_fin_suivi:
                data.append([Pbold("Motif de fin de suivi :"), famille.suivi.get_motif_fin_suivi_display()])
            if famille.archived_at:
                data.append([Pbold("Date d'archivage: "), format_d_m_Y(famille.archived_at)])
        if data:
            self.story.append(self.get_table(data, [4, 5]))


class GenogrammePdf(BaseCarrefourPDF):
    title = "Génogramme"

    def produce(self, *args, **kwargs):
        self.set_title()
        return super().produce(*args, **kwargs)


class SociogrammePdf(GenogrammePdf):
    title = "Sociogramme"


class JournalPdf(BaseCarrefourPDF):
    title = "Journal de bord"

    def get_filename(self, famille):
        return '{}_journal.pdf'.format(slugify(famille.nom))

    def get_title(self, famille):
        return 'Famille {} - {}'.format(famille.nom, self.title)

    def produce(self, famille, **kwargs):
        self.set_title(self.get_title(famille))
        for page in famille.journaux.all().order_by('date'):
            self.story.append(Paragraph(page.theme, self.style_sub_title))
            self.story.append(Paragraph(page.texte.replace('\n', '<br />\n'), self.style_normal))
            self.story.append(
                Paragraph('{} - {}'.format(page.auteur.sigle, format_d_m_Y(page.date)), self.style_italic)
            )
        return super().produce(**kwargs)


class MessagePdf(BaseCarrefourPDF):
    title = 'Message'

    def get_filename(self, msg):
        return '{}_message.pdf'.format(slugify(msg.subject))

    def produce(self, doc, **kwargs):
        self.set_title('{} - Famille {}'.format(self.title, doc.famille.nom))
        msg = extract_msg.Message(doc.fichier.path)
        P = partial(Paragraph, style=self.style_normal)
        Pbold = partial(Paragraph, style=self.style_bold)

        data = [
            [Pbold('De:'), P(msg.sender)],
            [Pbold('À:'), P(msg.to)],
            [Pbold('CC:'), P(msg.cc)],
            [Pbold('Date:'), P(msg.date)],
            [Pbold('Sujet:'), P(msg.subject)],
            [Pbold('Message:'), P(msg.body)]
        ]

        self.story.append(self.get_table(data, [3, 15]))
        return super().produce()
