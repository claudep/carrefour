from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('carrefour', '0022_Ajouter_genogramme_et_sociogramme'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='est_actif',
            field=models.BooleanField(default=True, verbose_name='actif'),
        ),
        migrations.AddField(
            model_name='utilisateur',
            name='date_desactivation',
            field=models.DateField(blank=True, null=True, verbose_name='Date désactivation'),
        ),
    ]
