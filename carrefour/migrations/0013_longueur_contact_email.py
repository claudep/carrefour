from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('carrefour', '0012_suppr_1_champs_Famille_et_2_Personne'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contact',
            name='email',
            field=models.EmailField(blank=True, max_length=100),
        ),
    ]
