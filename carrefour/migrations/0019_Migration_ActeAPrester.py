from django.db import migrations


def ajout_prestations(apps, schema_editor):
    ActeAPrester = apps.get_model('carrefour', 'ActeAPrester')
    LibellePrestation = apps.get_model('carrefour', 'LibellePrestation')

    lib = LibellePrestation.objects.create(unite='aemo', code='p01', nom='Prestation 1')
    ActeAPrester.objects.bulk_create([
        ActeAPrester(libelle_prestation=lib, nom="Entretien parents/jeunes au bureau"),
        ActeAPrester(libelle_prestation=lib, nom="Entretien parents/jeune à domicile yc déplacement"),
        ActeAPrester(libelle_prestation=lib, nom="Gestion du dossier (réflexivité, préparation, entretien)"),
        ActeAPrester(libelle_prestation=lib, nom="Bilan en lien avec le suivi"),
        ActeAPrester(libelle_prestation=lib, nom="Réseau en lien avec le suivi"),
        ActeAPrester(libelle_prestation=lib, nom="Téléphone en lien avec l'accompagnement"),
        ActeAPrester(libelle_prestation=lib, nom="Échanges avec AS et réseau"),
        ActeAPrester(libelle_prestation=lib, nom="Entretien RE"),
        ActeAPrester(libelle_prestation=lib, nom="Coaching étud. (PF= => 1h./semaine)"),
        ActeAPrester(libelle_prestation=lib, nom="Supervision"),
        ActeAPrester(libelle_prestation=lib, nom="Intervision"),
        ActeAPrester(libelle_prestation=lib, nom="Point-équipe (pédagogique) 50%"),
    ])

    lib = LibellePrestation.objects.create(unite='aemo', code='p02', nom='Prestation 2')
    ActeAPrester.objects.bulk_create([
        ActeAPrester(libelle_prestation=lib, nom="Renseignement sur le travail (hors suivi)"),
        ActeAPrester(libelle_prestation=lib, nom="Présentation et représentation du service"),
        ActeAPrester(libelle_prestation=lib, nom="Analyse de demande"),
    ])

    lib = LibellePrestation.objects.create(unite='aemo', code='p03', nom='Prestation 3')
    ActeAPrester.objects.bulk_create([
        ActeAPrester(libelle_prestation=lib, nom="Journées ressources"),
        ActeAPrester(libelle_prestation=lib, nom="Wenk-end (papas, ados,…)"),
        ActeAPrester(libelle_prestation=lib, nom="Groupe de paroles (parents, ados,…)"),
        ActeAPrester(libelle_prestation=lib, nom="Groupe à thème (estime de soi,…)"),
    ])

    lib = LibellePrestation.objects.create(unite='ser', code='p04', nom='Prestation 4')
    ActeAPrester.objects.bulk_create([
        ActeAPrester(libelle_prestation=lib, nom="BIP"),
        ActeAPrester(libelle_prestation=lib, nom="TSHM"),
        ActeAPrester(libelle_prestation=lib, nom="Formation donnée à l'extérieur (police,…)"),
        ActeAPrester(libelle_prestation=lib, nom="Permanences"),
        ActeAPrester(libelle_prestation=lib, nom="Manifestations (Plage, Promos,…)"),
        ActeAPrester(libelle_prestation=lib, nom="Accompagnement de jeunes"),
        ActeAPrester(libelle_prestation=lib, nom="Supervision"),
        ActeAPrester(libelle_prestation=lib, nom="Intervision"),
    ])

    lib = LibellePrestation.objects.create(unite='batoude', code='p05', nom='Prestation 5')
    ActeAPrester(libelle_prestation=lib, nom="Présence jeune/famille"),

    lib = LibellePrestation.objects.create(unite='batoude', code='p06', nom='Prestation 6')
    ActeAPrester(libelle_prestation=lib, nom="Hors présence jeune/famille"),

    lib = LibellePrestation.objects.create(unite='batoude', code='p07', nom='Prestation 7')
    ActeAPrester(libelle_prestation=lib, nom="Présentation du service"),

    lib = LibellePrestation.objects.create(unite='batoude', code='p08', nom='Prestation 8')
    ActeAPrester(libelle_prestation=lib, nom="Travail posture"),

    lib = LibellePrestation.objects.create(unite='asaef', code='p09', nom='Prestation 9 Étude')
    ActeAPrester.objects.bulk_create([
        ActeAPrester(libelle_prestation=lib, nom="Entretien parents/jeunes au bureau"),
        ActeAPrester(libelle_prestation=lib, nom="Entretien parents/jeune à domicile yc déplacement"),
        ActeAPrester(libelle_prestation=lib, nom="Gestion du dossier (réflexivité, préparation, entretien)"),
        ActeAPrester(libelle_prestation=lib, nom="Bilan en lien avec le suivi"),
        ActeAPrester(libelle_prestation=lib, nom="Téléphone en lien avec l'accompagnement"),
        ActeAPrester(libelle_prestation=lib, nom="Réseau en lien avec le suivi"),
        ActeAPrester(libelle_prestation=lib, nom="Échanges avec AS et réseau"),
    ])

    lib = LibellePrestation.objects.create(unite='asaef', code='p10', nom='Prestation 10 Soutien')
    ActeAPrester.objects.bulk_create([
        ActeAPrester(libelle_prestation=lib, nom="Entretien parents/jeunes au bureau"),
        ActeAPrester(libelle_prestation=lib, nom="Entretien parents/jeune à domicile yc déplacement"),
        ActeAPrester(libelle_prestation=lib, nom="Gestion du dossier (réflexivité, préparation, entretien)"),
        ActeAPrester(libelle_prestation=lib, nom="Bilan en lien avec le suivi"),
        ActeAPrester(libelle_prestation=lib, nom="Téléphone en lien avec l'accompagnement"),
        ActeAPrester(libelle_prestation=lib, nom="Réseau en lien avec le suivi"),
        ActeAPrester(libelle_prestation=lib, nom="Échanges avec AS et réseau"),
    ])

    lib = LibellePrestation.objects.create(unite='asaef', code='p11', nom='Prestation 11')
    ActeAPrester.objects.bulk_create([
        ActeAPrester(libelle_prestation=lib, nom="Entretien RE"),
        ActeAPrester(libelle_prestation=lib, nom="Supervision"),
        ActeAPrester(libelle_prestation=lib, nom="Intervision"),
        ActeAPrester(libelle_prestation=lib, nom="Point-équipe 50%"),
        ActeAPrester(libelle_prestation=lib, nom="Coaching étud. (PF= => 1h./semaine)"),
    ])

    lib = LibellePrestation.objects.create(unite='asaef', code='p12', nom='Prestation 12')
    ActeAPrester.objects.bulk_create([
        ActeAPrester(libelle_prestation=lib, nom="Renseignement sur le travail (hors suivi)"),
        ActeAPrester(libelle_prestation=lib, nom="Présentation et représentation du service"),
    ])


class Migration(migrations.Migration):
    dependencies = [
        ('carrefour', '0018_Table_ActeAPrester'),
    ]

    operations = [
        migrations.RunPython(ajout_prestations)
    ]
