# Generated by Django 2.2.1 on 2019-05-22 17:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('carrefour', '0017_Table_LibellePrestation'),
    ]

    operations = [
        migrations.CreateModel(
            name='ActeAPrester',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=80, verbose_name='Nom')),
                ('libelle_prestation', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='actes_aprester', to='carrefour.LibellePrestation')),
            ],
            options={
                'verbose_name': 'Acte à prester',
                'verbose_name_plural': 'Actes à prester',
            },
        ),
    ]
