from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('carrefour', '0034_Migration_contractant'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='contact',
            unique_together=set(),
        ),
        migrations.AddConstraint(
            model_name='contact',
            constraint=models.UniqueConstraint(condition=models.Q(('est_actif', True)), fields=('nom', 'prenom', 'service'), name='unique_contact'),
        ),
    ]
