from django.db import migrations


def fix_contractants(apps, schema_editor):
    Personne = apps.get_model('carrefour', 'Personne')

    Personne.objects.filter(
        role__famille=True).exclude(
        role__nom__in=['Enfant suivi', 'Enfant non-suivi']).update(
        contractant=True
    )


class Migration(migrations.Migration):

    dependencies = [
        ('carrefour', '0033_Add_Pers_contractant'),
    ]

    operations = [
        migrations.RunPython(fix_contractants)
    ]
