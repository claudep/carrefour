from django.db import migrations


def update_libelles(apps, schema_editor):
    """
    Modifie le libelle des prestations Batoude
    """
    LibellePrestation = apps.get_model('carrefour', 'LibellePrestation')
    LibellePrestation.objects.filter(code='p05').update(nom='Prestation 5 (avec le jeune)')
    LibellePrestation.objects.filter(code='p06').update(nom='Prestation 6 (sans le jeune)')


class Migration(migrations.Migration):

    dependencies = [
        ('carrefour', '0040_alter_utilisateur_managers'),
    ]

    operations = [
        migrations.RunPython(update_libelles)
    ]
