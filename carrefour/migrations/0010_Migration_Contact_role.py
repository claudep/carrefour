from django.db import migrations


def apply_migration(apps, schema_editor):
    """ Renseigne le champ Contact.role aec la valeur Contact.service.sigle  """

    Role = apps.get_model('carrefour', 'Role')
    Contact = apps.get_model('carrefour', 'Contact')

    for c in Contact.objects.all():
        if c.service:
            sigle = c.service.sigle
            role, _ = Role.objects.get_or_create(nom=sigle, defaults={'famille': False})
            c.role = role
            c.save()


class Migration(migrations.Migration):

    dependencies = [
        ('carrefour', '0009_Ajout_champ_Contact_role'),
    ]

    operations = [

        migrations.RunPython(apply_migration)
    ]
