# Generated by Django 4.1 on 2022-10-28 09:03
import os
import tzlocal

from datetime import datetime
from pathlib import Path

from django.core.exceptions import ObjectDoesNotExist
from django.db import migrations


def migrate_famille_archived_at(apps, schema_editor):
    FamilleAemo = apps.get_model('aemo', 'FamilleAemo')
    FamilleAsaef = apps.get_model('asaef', 'FamilleAsaef')
    Archive = apps.get_model('archive', 'Archive')

    local_timezone = tzlocal.get_localzone()

    for archive in Archive.objects.all():
        famille_model = FamilleAemo if archive.unite == 'aemo' else FamilleAsaef
        p = Path(archive.pdf.path)
        if p.exists():
            stat = os.stat(p)
            timestamp = stat.st_mtime

            pk = p.name.split('-')[-1].split('.')[0]
            if not pk.isdigit():
                continue
            try:
                famille = famille_model.objects.get(pk=pk)
                famille.archived_at = datetime.fromtimestamp(timestamp, local_timezone)
                famille.save()
            except ObjectDoesNotExist:
                print('Fichier non trouvé pour la famille :', pk)
                continue


class Migration(migrations.Migration):

    dependencies = [
        ('aemo', '0007_famille_aemo_proxy'),
        ('asaef', '__first__'),
        ('archive', '__first__'),
        ('carrefour', '0037_Add_famille_archived_at'),
    ]

    operations = [
        migrations.RunPython(migrate_famille_archived_at)
    ]
