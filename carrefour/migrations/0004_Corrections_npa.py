from django.db import migrations


def apply_migration(apps, schema_editor):
    """ Corrige les NPA des adresses de Famille et Peronne """

    npa = {
        '2000': 'Neuchâtel',
        '2001': 'Neuchâtel',
        '2012': 'Auvernier',
        '2013': 'Colombier',
        '2014': 'Bôle',
        '2015': 'Areuse',
        '2016': 'Cortaillod',
        '2017': 'Boudry',
        '2022': 'Bevaix',
        '2023': 'Gorgier',
        '2024': 'St-Aubin-Sauges',
        '2034': 'Peseux',
        '2035': 'Corcelles',
        '2036': 'Cormondrèche',
        '2043': 'Boudevilliers',
        '2052': 'Fontainemelon',
        '2053': 'Cernier',
        '2054': 'Chézard-St-Martin',
        '2065': 'Savagnier',
        '2068': 'Hauterive',
        '2072': 'St-Blaise',
        '2074': 'Marin-Epagnier',
        '2087': 'Cornaux',
        '2088': 'Cressier',
        '2103': 'Noiraigue',
        '2105': 'Travers',
        '2108': 'Couvet',
        '2114': 'Fleurier',
        '2206': 'Les Geneveys-sur-Coffrane',
        '2300': 'La Chaux-de-Fonds',
        '2316': 'Les Ponts-de-Martel',
        '2322': 'Le Crêt-du-Locle',
        '2400': 'Le Locle',
        '2523': 'Lignières',
        '2525': 'Le Landeron',
    }

    Famille = apps.get_model('carrefour', 'Famille')
    for fam in Famille.objects.all():
        fam.localite = npa[fam.npa]
        fam.save()

    Personne = apps.get_model('carrefour', 'Personne')
    for pers in Personne.objects.all():
        pers.localite = npa[pers.npa]
        pers.save()


class Migration(migrations.Migration):

    dependencies = [
        ('carrefour', '0003_Nouveaux_champs_Personne'),
    ]

    operations = [

        migrations.RunPython(apply_migration)
    ]
