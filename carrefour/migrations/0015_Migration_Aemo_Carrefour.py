from django.db import migrations


def apply_migration(apps, schema_editor):
    """ Remplace le sigle AEMO par CARREFOUR dans Service """

    Service = apps.get_model('carrefour', 'Service')
    try:
        s = Service.objects.get(sigle='AEMO')
    except Service.DoesNotExist:
        s = Service()
    s.sigle = 'CARREFOUR'
    s.nom_complet = 'Accompagnement éducatif ambulatoire'
    s.save()


class Migration(migrations.Migration):

    dependencies = [
        ('carrefour', '0014_add_Personne_assurance'),
    ]

    operations = [

        migrations.RunPython(apply_migration)
    ]
