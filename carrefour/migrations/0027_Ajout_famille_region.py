from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('carrefour', '0026_Ajout_champ_Contact_profession'),
    ]

    operations = [
        migrations.AddField(
            model_name='famille',
            name='region',
            field=models.CharField(blank=True, choices=[('mont_locle', 'Le Locle'), ('mont_pms', 'Ponts-de-Martel/Sagne'), ('mont_cdf', 'La Chaux-de-Fonds'), ('litt_est', 'Littoral Est'), ('litt_ouest', 'Littoral Ouest'), ('litt_ntel', 'Neuchâtel Centre'), ('litt_vdr', 'Val-de-Ruz'), ('litt_vdt', 'Val-de-Travers')], max_length=12, verbose_name='Région'),
        ),
    ]
