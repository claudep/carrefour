# Generated by Django 4.1 on 2022-10-28 09:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('carrefour', '0036_Add_field_archived'),
    ]

    operations = [
        migrations.AddField(
            model_name='famille',
            name='archived_at',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Archivée le'),
        ),
    ]
