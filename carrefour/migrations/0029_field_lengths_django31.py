from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('carrefour', '0028_Migration_actes_a_prester_aemo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contact',
            name='email',
            field=models.EmailField(blank=True, max_length=100, verbose_name='Courriel'),
        ),
        migrations.AlterField(
            model_name='utilisateur',
            name='first_name',
            field=models.CharField(blank=True, max_length=150, verbose_name='first name'),
        ),
    ]
