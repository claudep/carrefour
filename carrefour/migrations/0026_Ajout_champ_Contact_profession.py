from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('carrefour', '0025_Supprime_champ_archive_famille'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='profession',
            field=models.CharField(blank=True, max_length=100, verbose_name='Activité/prof.'),
        ),
    ]
