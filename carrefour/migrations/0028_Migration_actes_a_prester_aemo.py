from django.db import migrations
from django.core.exceptions import ObjectDoesNotExist


def apply_migration(apps, schema_editor):
    """ Passe les actes suivants dans la prest. AEMO No 2  """
    actes = ['Coaching étud.', 'Supervision', 'Intervision', 'Point-équipe']

    LibellePrestation = apps.get_model('carrefour', 'LibellePrestation')
    ActeAPrester = apps.get_model('carrefour', 'ActeAPrester')
    prest_02 = LibellePrestation.objects.get(code='p02')
    for acte in actes:
        try:
            obj = ActeAPrester.objects.get(nom__startswith=acte, libelle_prestation__code='p01')
            obj.libelle_prestation = prest_02
            obj.save()
        except ObjectDoesNotExist:
            print('Erreur dans la migration de la ligne {}'.format(acte))


class Migration(migrations.Migration):

    dependencies = [
        ('carrefour', '0027_Ajout_famille_region'),
    ]

    operations = [
        migrations.RunPython(apply_migration)
    ]
