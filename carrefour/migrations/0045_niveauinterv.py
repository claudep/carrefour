from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('carrefour', '0044_derogationsaisietardive'),
    ]

    operations = [
        migrations.CreateModel(
            name='NiveauInterv',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('niveau', models.PositiveSmallIntegerField(
                    choices=[(1, '1'), (2, '2'), (3, '3')], default=1, verbose_name='Niveau d’intervention'
                )),
                ('date_debut', models.DateField(blank=True, null=True, verbose_name='Date début')),
                ('date_fin', models.DateField(blank=True, null=True, verbose_name='Date fin')),
                ('famille', models.ForeignKey(
                    on_delete=models.deletion.CASCADE, related_name='niveaux_interv', to='carrefour.famille', verbose_name='Famille'
                )),
            ],
        ),
    ]
