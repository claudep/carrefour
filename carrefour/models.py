import os
from datetime import date, timedelta
from django.conf import settings
from django.contrib.auth.models import AbstractUser, UserManager
from django.contrib.postgres.fields import DateRangeField
from django.core.exceptions import ObjectDoesNotExist
from django.core.validators import FileExtensionValidator, MaxValueValidator
from django.db import models, transaction
from django.db.models import Count, F, OuterRef, Q, Subquery, Sum, Value
from django.db.models.functions import Coalesce, ExtractMonth, ExtractYear, NullIf
from django.db.utils import IntegrityError
from django.urls import reverse
from django.utils import timezone

from common import choices

from .utils import format_adresse, format_contact, format_duree, format_d_m_Y, random_string_generator


class CercleScolaire(models.Model):
    """ Les 7 cercles scolaires du canton"""

    nom = models.CharField(max_length=50, unique=True)
    telephone = models.CharField('tél.', max_length=35, blank=True)

    class Meta:
        verbose_name = 'Cercle scolaire'
        verbose_name_plural = 'Cercles scolaires'
        ordering = ('nom',)

    def __str__(self):
        return self.nom


class Service(models.Model):
    """ Tous les services sociaux en lien avec AEMO"""

    sigle = models.CharField('Sigle', max_length=10, unique=True)
    nom_complet = models.CharField('Nom complet', max_length=80, blank=True)

    class Meta:
        ordering = ('sigle',)

    def __str__(self):
        return self.sigle

    def save(self, *args, **kwargs):
        self.sigle = self.sigle.upper()
        super().save(*args, **kwargs)


class RoleManager(models.Manager):
    def get_by_natural_key(self, nom):
        return self.get(nom=nom)


class Role(models.Model):
    NON_EDITABLE = ['Père', 'Mère', 'Enfant suivi', 'Enfant non-suivi']

    nom = models.CharField("Nom", max_length=50, unique=True)
    famille = models.BooleanField("Famille", default=False)

    objects = RoleManager()

    class Meta:
        ordering = ('nom',)

    def __str__(self):
        return self.nom

    @property
    def editable(self):
        return self.nom not in self.NON_EDITABLE

    def natural_key(self):
        return (self.nom,)


class Contact(models.Model):
    prenom = models.CharField('Prénom', max_length=30)
    nom = models.CharField('Nom', max_length=30)
    rue = models.CharField('Rue', max_length=30, blank=True)
    npa = models.CharField('NPA', max_length=4, blank=True)
    localite = models.CharField('Localité', max_length=30, blank=True)
    telephone = models.CharField('Tél.', max_length=30, blank=True)
    email = models.EmailField('Courriel', max_length=100, blank=True)
    service = models.ForeignKey(Service, null=True, blank=True, on_delete=models.PROTECT)
    role = models.ForeignKey(Role, null=True, blank=True, on_delete=models.PROTECT, verbose_name="Rôle")
    profession = models.CharField('Activité/prof.', max_length=100, blank=True)
    remarque = models.TextField("Remarque", blank=True)
    est_actif = models.BooleanField('actif', default=True)

    class Meta:
        verbose_name = 'Contact'
        ordering = ('nom', 'prenom')
        constraints = [
            models.UniqueConstraint(
                fields=['nom', 'prenom', 'service'], condition=Q(est_actif=True), name='unique_contact'
            )
        ]

    def __str__(self):
        sigle = self.service.sigle if self.service else ''
        return '{} ({})'.format(self.nom_prenom, sigle) if self.service else self.nom_prenom

    @property
    def nom_prenom(self):
        return '{} {}'.format(self.nom, self.prenom)

    @property
    def nom_initiale_prenom(self):
        return f"{self.prenom[0]}. {self.nom}"

    @property
    def adresse(self):
        return format_adresse(self.rue, self.npa, self.localite)

    @property
    def contact(self):
        return format_contact(self.telephone, self.email)

    @classmethod
    def membres_ope(cls):
        return cls.objects.filter(service__sigle__startswith='OPE', est_actif=True)

    def dossiers(self, actif=True):
        filtre = {'date_sortie__isnull': True} if actif else {}
        batoude = [f"{benef.nom} {benef.prenom} (Batoude)" for benef in
                   self.beneficiaire_set.filter(**filtre)]
        batoude.extend([f"{benef.nom} {benef.prenom} (Batoude)" for benef in
                        self.suivisbatoude.filter(**filtre)])
        batoude.extend([f"{benef.nom} {benef.prenom} (Batoude)" for benef in
                        self.suivisbatoude2.filter(**filtre)])
        filtre = {'famille__suiviaemo__id__isnull': False}
        if actif:
            filtre['famille__suiviaemo__date_fin_suivi__isnull'] = True
        aemo = [f"Fam. {pers.famille.nom} (Aemo)" for pers in self.personne_set.filter(**filtre)]
        filtre = {'date_fin_suivi__isnull': True} if actif else {}
        aemo.extend([f"Fam. {suivi.famille.nom} (Aemo)" for suivi in
                     self.suivisaemo.filter(**filtre)])
        filtre = {'famille__suiviasaef__id__isnull': False}
        if actif:
            filtre['famille__suiviasaef__date_fin_suivi__isnull'] = True
        asaef = [f"Fam. {pers.famille.nom} (Asaef)" for pers in self.personne_set.filter(**filtre)]
        filtre = {'date_fin_suivi__isnull': True} if actif else {}
        asaef.extend([f"Fam. {suivi.famille.nom} (Asaef)" for suivi in
                      self.suivisasaef.filter(**filtre)])
        filtre = {'statut': 'actif'} if actif else {}
        asap = [f"{pers.nom} {pers.prenom} (Asap)" for pers in self.beneficiaires.filter(**filtre)]
        return aemo + asaef + batoude + asap

    def fusionner(self, *doublons):
        """
        Reporte les liens des doublons sur le contact courant
        puis supprime les doublons.
        """
        with transaction.atomic():
            for doublon in doublons:
                # Batoude
                for benef in doublon.beneficiaire_set.all():
                    benef.reseau.add(self)
                    benef.reseau.remove(doublon)
                # Aemo - Asaef
                for person in doublon.personne_set.all():
                    person.reseaux.add(self)
                    person.reseaux.remove(doublon)
                # Ser
                for benef in doublon.beneficiaires.all():
                    benef.contacts.add(self)
                    benef.contacts.remove(doublon)
                doublon.suivisaemo.update(ope_referent=self)
                doublon.suivisasaef.update(ope_referent=self)
                doublon.suivisasaef2.update(ope_referent2=self)
                doublon.suivisbatoude.update(referent_ope=self)
                doublon.suivisbatoude2.update(referent_ope2=self)

                doublon.delete()


class UtilisateurManager(UserManager):
    def actifs(self):
        return self.filter(date_desactivation__isnull=True, is_superuser=False)


class Utilisateur(Contact, AbstractUser):
    sigle = models.CharField(max_length=5, blank=True)
    date_desactivation = models.DateField('Date désactivation', null=True, blank=True)
    taux_activite = models.PositiveSmallIntegerField(
        "Taux d’activité (en %)", blank=True, default=0, validators=[MaxValueValidator(100)]
    )

    objects = UtilisateurManager()

    @property
    def groupes(self):
        return [item.name for item in self.groups.all()]

    def prestations(self, unite):
        """Renvoie toutes les prestations (liées ou générales) de l’utilisateur."""
        return getattr(self, f'prestations_{unite}').all()

    def prestations_gen(self, unite):
        """Renvoie les prestations générales de l’utilisateur."""
        if unite == 'batoude':
            return self.prestations_batoude.filter(beneficiaire__isnull=True)
        else:
            return self.prestations(unite).filter(famille__isnull=True)

    def temps_total_prestations(self, unite):
        return self.prestations(unite).model.temps_total(self.prestations(unite), tous_utilisateurs=False)

    def total_mensuel(self, unite, month, year):
        prestations = self.prestations(unite).filter(
            date_prestation__year=year, date_prestation__month=month
        )
        return self.prestations(unite).model.temps_total(prestations, tous_utilisateurs=False)

    def totaux_mensuels(self, unite, year):
        return [self.total_mensuel(unite, m, year) for m in range(1, 13)]

    def total_annuel(self, unite, year):
        prestations = self.prestations(unite).filter(date_prestation__year=year)
        return self.prestations(unite).model.temps_total(prestations, tous_utilisateurs=False)

    def utilisateurs_meme_equipe(self):
        equipes = [g.name for g in self.groups.all()]
        return Utilisateur.objects.filter(
            is_active=True, groups__name__in=equipes
        ).exclude(groups__name='direction').distinct()

    @classmethod
    def _intervenants(cls, groupes=(), annee=None):
        if annee is None:
            utils = Utilisateur.objects.actifs()
        else:
            utils = Utilisateur.objects.filter(
                Q(is_superuser=False) & (
                    Q(date_desactivation__isnull=True) | Q(date_desactivation__year=annee)
                )
            )
        return utils.filter(
            groups__name__in=groupes,
        ).exclude(
            groups__name__in=['direction']
        )

    @classmethod
    def intervenants_aemo(cls, annee=None):
        return cls._intervenants(groupes=['aemo_littoral', 'aemo_montagnes'], annee=annee).distinct()

    @classmethod
    def intervenants_asaef(cls, annee=None):
        return cls._intervenants(groupes=['asaef'], annee=annee)

    @classmethod
    def intervenants_batoude(cls, annee=None):
        return cls._intervenants(groupes=['batoude'], annee=annee)

    @classmethod
    def intervenants_asap(cls, annee=None):
        return cls._intervenants(groupes=['asap'], annee=annee)

    def heures_act_mens(self):
        return round(settings.HEURES_MENSUELLES * self.taux_activite / 100)

    def is_responsable_equipe(self):
        return (
            self.has_perm('aemo.manage_littoral')
            or self.has_perm('aemo.manage_montagnes')
            or self.has_perm('asaef.manage_asaef')
            or self.has_perm('batoude.manage_batoude')
            or self.has_perm('ser.manage_ser')
        )

    def is_responsable_unite(self, unite):
        if unite == 'aemo':
            return any([self.has_perm('aemo.manage_littoral'), self.has_perm('aemo.manage_montagnes')])
        else:
            return self.has_perm(f"{unite}.manage_{unite}")


class FamilleManager(models.Manager):
    def with_niveau_interv(self):
        """
        Annote les familles avec le niveau d’intervention le plus récent.
        """
        return self.annotate(
            niveau_interv=Subquery(
                NiveauInterv.objects.filter(
                    famille=OuterRef('pk')
                ).order_by('-date_debut').values('niveau')[:1]
            )
        )


class Famille(models.Model):

    nom = models.CharField('Nom de famille', max_length=40)
    rue = models.CharField('Rue', max_length=30, blank=True)
    npa = models.CharField('NPA', max_length=4, blank=True)
    localite = models.CharField('Localité', max_length=30, blank=True)
    region = models.CharField('Région', max_length=12, choices=choices.REGIONS_CHOICES, blank=True)
    telephone = models.CharField('Tél.', max_length=35, blank=True)
    autorite_parentale = models.CharField("Autorité parentale", max_length=20,
                                          choices=choices.AUTORITE_PARENTALE_CHOICES, blank=True)
    monoparentale = models.BooleanField('Famille monoparent.', null=True, blank=True)
    statut_marital = models.CharField("Statut marital", max_length=20, choices=choices.STATUT_MARITAL_CHOICES,
                                      blank=True)
    connue = models.BooleanField(default=False, verbose_name="famille déjà suivie")
    accueil = models.BooleanField(default=False, verbose_name="famille d'accueil")
    garde = models.CharField('type de garde',
                             max_length=20,
                             choices=(('partage', 'garde partagée'), ('droit', 'droit de garde')), blank=True)
    genogramme = models.FileField(upload_to='genogrammes/', blank=True,
                                  validators=[FileExtensionValidator(['pdf'])])
    sociogramme = models.FileField(upload_to='sociogrammes/', blank=True,
                                   validators=[FileExtensionValidator(['pdf'])])
    archived_at = models.DateTimeField('Archivée le', blank=True, null=True)

    typ = None
    objects = FamilleManager()

    class Meta:
        ordering = ('nom', 'npa')
        permissions = (
            ('export_aemo', 'Exporter familles AEMO'),
        )

    def __str__(self):
        return '{} - {}'.format(self.nom, self.adresse)

    @classmethod
    def from_db(cls, db, field_names, values):
        """Implement polymorphism for Famille."""
        from aemo.models import FamilleAemo
        from asaef.models import FamilleAsaef
        new = super().from_db(db, field_names, values)
        if isinstance(new, (FamilleAemo, FamilleAsaef)):
            return new
        try:
            new.suiviaemo
            new.__class__ = FamilleAemo
        except ObjectDoesNotExist:
            try:
                new.suiviasaef
                new.__class__ = FamilleAsaef
            except ObjectDoesNotExist:
                pass
        return new

    @property
    def adresse(self):
        return format_adresse(self.rue, self.npa, self.localite)

    @property
    def suivi(self):
        raise NotImplementedError

    @property
    def suivi_url(self):
        return reverse('%s-famille-suivi' % self.typ, args=[self.pk])

    @property
    def edit_url(self):
        return reverse('%s-famille-edit' % self.typ, args=[self.pk])

    @property
    def print_coords_url(self):
        return reverse('%s-print-coord-famille' % self.typ, args=[self.pk])

    def membres_suivis(self):
        return self.membres.filter(role__nom="Enfant suivi").order_by('-date_naissance')

    def enfants_non_suivis(self):
        return self.membres.filter(role__nom='Enfant non-suivi').order_by('-date_naissance')

    def pere(self):
        try:
            return self.membres.get(role__nom='Père')
        except Personne.DoesNotExist:
            return None

    def mere(self):
        try:
            return self.membres.get(role__nom='Mère')
        except Personne.DoesNotExist:
            return None

    def parents_contractant(self):
        return [parent for parent in [self.mere(), self.pere()] if parent and parent.contractant]

    def autres_parents(self):
        return self.membres.exclude(role__nom__in=('Père', 'Mère', 'Enfant suivi', 'Enfant non-suivi'))

    def can_edit(self, user):
        return False

    @property
    def prestations(self):
        raise NotImplementedError

    def temps_total_prestations(self, interv=None):
        """
        Temps total des prestations liées à la famille, quel que soit le nombre
        de membres suivis.
        Filtré facultativement par intervenant.
        """
        prest = self.prestations if interv is None else self.prestations.filter(intervenant=interv)
        return prest.aggregate(total=Sum('duree'))['total'] or timedelta()

    def temps_total_prestations_reparti(self):
        """
        Temps total des prestations liées à la famille, divisé par le nombre d'enfants suivis.
        """
        duree = self.temps_total_prestations()
        duree //= (self.membres_suivis().count() or 1)
        return duree

    def total_mensuel_prest_gen(self, date_debut_mois):
        """
        Temps total de prestations générales sur un mois donné
        """
        return self.prestations.model.objects.filter(
            famille=None,
            date_prestation__month=date_debut_mois.month,
            date_prestation__year=date_debut_mois.year
        ).aggregate(
            total=Sum(F('duree') / NullIf(F('familles_actives'), 0), output_field=models.DurationField())
        )['total'] or timedelta()

    def total_mensuel(self, date_debut_mois):
        """
        Temps total de prestations sur un mois donné
        """
        return self.prestations.filter(
            date_prestation__month=date_debut_mois.month,
            date_prestation__year=date_debut_mois.year
        ).aggregate(total=Sum('duree'))['total'] or timedelta()

    def prestations_historiques(self):
        return PrestationBase.prestations_historiques(self.prestations.all())

    def prestations_du_mois(self):
        """
        Retourne le détail des prestations pour cette famille
        à partir du mois courant
        """
        date_ref = date(date.today().year, date.today().month, 1)
        return self.prestations.filter(date_prestation__gte=date_ref)

    @classmethod
    def suivis_en_cours(cls, debut, fin):
        return cls.objects.filter(
            **{f'suivi{cls.typ}__date_demande__lte': fin}
        ).filter(
            Q(**{f'suivi{cls.typ}__date_fin_suivi__isnull': True}) |
            Q(**{f'suivi{cls.typ}__date_fin_suivi__gte': debut})
        ).annotate(
            date_debut=F(f'suivi{cls.typ}__date_demande'),
            date_fin=Coalesce(f'suivi{cls.typ}__date_fin_suivi', fin),
        ).exclude(**{f'suivi{cls.typ}__motif_fin_suivi': 'erreur'})

    @classmethod
    def suivis_nouveaux(cls, debut, fin):
        return cls.objects.filter(**{
            f'suivi{cls.typ}__date_demande__lte': fin,
            f'suivi{cls.typ}__date_demande__gte': debut,
        }).annotate(
            date_debut=F(f'suivi{cls.typ}__date_demande'),
            date_fin=F(f'suivi{cls.typ}__date_demande'),
        ).exclude(**{f'suivi{cls.typ}__motif_fin_suivi': 'erreur'})

    @classmethod
    def suivis_termines(cls, debut, fin):
        return cls.objects.filter(**{
            f'suivi{cls.typ}__date_fin_suivi__lte': fin,
            f'suivi{cls.typ}__date_fin_suivi__gte': debut,
        }).annotate(
            date_debut=F(f'suivi{cls.typ}__date_fin_suivi'),
            date_fin=F(f'suivi{cls.typ}__date_fin_suivi'),
        ).exclude(**{f'suivi{cls.typ}__motif_fin_suivi': 'erreur'})

    @classmethod
    def suivis_en_attente(cls, debut, fin):
        return cls.objects.filter(**{
            f'suivi{cls.typ}__date_demande__lte': fin,
            f'suivi{cls.typ}__date_debut_suivi__isnull': True,
            f'suivi{cls.typ}__date_fin_suivi__isnull': True,
        }).annotate(
            date_debut=F(f'suivi{cls.typ}__date_demande'),
            date_fin=Value('--'),
        ).exclude(**{f'suivi{cls.typ}__motif_fin_suivi': 'erreur',})

    def anonymiser(self):
        new_data = {
            'nom': random_string_generator(),
            'rue': '',
            'npa': self.npa,
            'localite': self.localite,
            'region': self.region,
            'telephone': '',
            'autorite_parentale': self.autorite_parentale,
            'monoparentale': self.monoparentale,
            'statut_marital': self.statut_marital,
            'connue': self.connue,
            'accueil': self.accueil,
            'garde': self.garde,
            'genogramme': None,
            'sociogramme': None,
            'archived_at': timezone.now()
        }
        [setattr(self, field, value) for field, value in new_data.items()]
        self.save()
        for membre in self.membres.all().select_related('role'):
            if membre.role.nom == 'Enfant suivi':
                membre.anonymiser()
            else:
                membre.delete()
        for doc in self.documents.all():
            doc.delete()


class PersonneManager(models.Manager):

    def create_personne(self, **kwargs):
        kwargs.pop('_state', None)
        if 'role' in kwargs:
            roles_contractants = Role.objects.filter(
                famille=True).exclude(
                nom__in=['Enfant suivi', 'Enfant non-suivi'])
            kwargs['contractant'] = kwargs['role'] in roles_contractants
        pers = self.create(**kwargs)
        return self.add_formation(pers)

    def add_formation(self, pers):
        if pers.role.nom == 'Enfant suivi' and not hasattr(pers, 'formation'):
            moins_de_4ans = bool(pers.age and pers.age < 4)
            Formation.objects.create(
                personne=pers, statut='pre_scol' if moins_de_4ans else ''
            )
            if moins_de_4ans and pers.famille.typ == 'aemo':
                pers.famille.suiviaemo.demande_prioritaire = True
                pers.famille.suiviaemo.save()
        return pers


class Personne(models.Model):
    """ Classe de base des personnes """
    PROV_DEST_CHOICES = (
        ('famille', 'famille'),
        ('IES_NE', 'autre IES-NE'),
        ('IES-HC', 'autre IES-HC'),
        ('SAEMO', 'SAEMO'),
        ('autre', 'autre')
    )

    prenom = models.CharField('Prénom', max_length=30)
    nom = models.CharField('Nom', max_length=30)
    date_naissance = models.DateField('Date de naissance', null=True, blank=True)
    genre = models.CharField(
        'Genre', max_length=1, choices=(('M', 'M'), ('F', 'F'), ('X', 'Non binaire')), blank=True
    )
    rue = models.CharField('Rue', max_length=30, blank=True)
    npa = models.CharField('NPA', max_length=4, blank=True)
    localite = models.CharField('Localité', max_length=30, blank=True)
    telephone = models.CharField('Tél.', max_length=35, blank=True)
    email = models.EmailField('Courriel', blank=True)
    remarque = models.TextField(blank=True)
    remarque_privee = models.TextField('Remarque privée', blank=True)
    famille = models.ForeignKey(Famille, on_delete=models.CASCADE, related_name='membres')
    role = models.ForeignKey('Role', on_delete=models.PROTECT)
    profession = models.CharField('Profession', max_length=50, blank=True)
    filiation = models.CharField('Filiation', max_length=80, blank=True)
    reseaux = models.ManyToManyField(Contact)
    # Champs spécifiques ASAEF
    assurance = models.CharField('Assurance-maladie', max_length=30, blank=True)
    employeur = models.CharField('Adresse empl.', max_length=50, blank=True)
    permis = models.CharField('Permis/séjour', max_length=30, blank=True)
    contractant = models.BooleanField('Partie au contrat', default=False)

    objects = PersonneManager()

    class Meta:
        verbose_name = 'Personne'
        ordering = ('nom', 'prenom')

    def __str__(self):
        return '{} {}'.format(self.nom, self.prenom)

    @property
    def nom_prenom(self):
        return str(self)

    @property
    def adresse(self):
        return format_adresse(self.rue, self.npa, self.localite)

    @property
    def age(self):
        if not self.date_naissance:
            # Pourrait disparaître si date_naissance devient non null
            return None
        age = (date.today() - self.date_naissance).days / 365.25
        return int(age * 10) / 10  # 1 décimale arrondi vers le bas

    @property
    def localite_display(self):
        return '{} {}'.format(self.npa, self.localite)

    @property
    def edit_url(self):
        return reverse('personne-edit', args=[self.famille.typ, self.famille_id, self.pk])

    def can_edit(self, user):
        return self.famille.can_edit(user)

    def can_be_deleted(self, user):
        return self.famille.can_be_deleted(user)

    def anonymiser(self):
        new_data = {
            'prenom': random_string_generator(),
            'nom': random_string_generator(),
            'date_naissance': self.date_naissance,
            'genre': self.genre,
            'rue': '',
            'npa': '',
            'localite': '',
            'telephone': '',
            'email': '',
            'remarque': '',
            'remarque_privee': '',
            'famille': self.famille,
            'role':  self.role,
            'profession': '',
            'filiation': '',
            # reseaux
            'assurance': '',
            'employeur': '',
            'permis': '',
            'contractant': False
        }
        [setattr(self, field, value) for field, value in new_data.items()]
        self.save()
        self.reseaux.clear()

        if hasattr(self, 'formation'):
            self.formation.anonymiser()
        return self


class Formation(models.Model):

    FORMATION_CHOICES = (
        ('pre_scol', 'Pré-scolaire'),
        ('cycle1', 'Cycle 1'),
        ('cycle2', 'Cycle 2'),
        ('cycle3', 'Cycle 3'),
        ('apprenti', 'Apprentissage'),
        ('etudiant', 'Etudiant'),
        ('en_emploi', 'En emploi'),
        ('sans_emploi', 'Sans emploi'),
        ('sans_occupation', 'Sans occupation'),
    )

    personne = models.OneToOneField(Personne, on_delete=models.CASCADE)

    statut = models.CharField('Scolarité', max_length=20, choices=FORMATION_CHOICES, blank=True)
    cercle_scolaire = models.ForeignKey(
        CercleScolaire, blank=True, null=True, on_delete=models.SET_NULL,
        related_name='+', verbose_name='Cercle scolaire'
    )
    college = models.CharField('Collège', max_length=50, blank=True)
    classe = models.CharField('Classe', max_length=50, blank=True)
    enseignant = models.CharField('Enseignant', max_length=50, blank=True)

    creche = models.CharField('Crèche', max_length=50, blank=True)
    creche_resp = models.CharField('Resp.crèche', max_length=50, blank=True)

    entreprise = models.CharField('Entreprise', max_length=50, blank=True)
    maitre_apprentissage = models.CharField("Maître d'appr.", max_length=50, blank=True)

    remarque = models.TextField(blank=True)

    class Meta:
        verbose_name = 'Scolarité'

    def __str__(self):
        return 'Scolarité de {}'.format(self.personne)

    def can_edit(self, user):
        return self.personne.famille.can_edit(user)

    def delete(self, **kwargs):
        if self.personne:
            raise IntegrityError
        else:
            super().delete(**kwargs)

    @property
    def sse(self):
        return "Champ à définir"

    def pdf_data(self):
        if self.personne.formation.statut == 'pre_scol':
            data = [
                ['Crèche: {}'.format(self.creche), 'responsable: {}'.format(self.creche_resp)],
            ]
        elif self.personne.formation.statut in ['cycle1', 'cycle2', 'cycle3', 'etudiant']:
            data = [
                ['Cercle: {}'.format(self.cercle_scolaire), 'collège: {}'.format(self.college)],
                ['Classe: {}'.format(self.classe), 'Enseignant-e: {}'.format(self.enseignant)]
            ]

        elif self.personne.formation.statut == 'apprenti':
            data = [
                ['Employeur: {}'.format(self.entreprise), 'resp. apprenti-e: {}'.format(self.maitre_apprentissage)],
            ]
        else:
            data = [self.personne.formation.statut]
        return data

    def anonymiser(self):
        new_data = {
            'id': self.id,
            'personne': self.personne,
            'statut': self.statut,
            'cercle_scolaire': None,
            'college': '',
            'classe': '',
            'enseignant': '',
            'creche': '',
            'creche_resp': '',
            'entreprise': '',
            'maitre_apprentissage': '',
            'remarque': ''
        }
        [setattr(self, field, value) for field, value in new_data.items()]
        self.save()
        return self


class NiveauInterv(models.Model):
    INTERV_CHOICES = [(1, '1'), (2, '2'), (3, '3')]
    famille = models.ForeignKey(
        Famille, related_name='niveaux_interv', on_delete=models.CASCADE, verbose_name="Famille"
    )
    niveau = models.PositiveSmallIntegerField(
        "Niveau d’intervention", choices=INTERV_CHOICES, default=1
    )
    date_debut = models.DateField('Date début', blank=True, null=True)
    date_fin = models.DateField('Date fin', blank=True, null=True)

    def __str__(self):
        _str = f"{self.famille.nom} - {self.niveau_interv} - du {format_d_m_Y(self.date_debut)}"
        if self.date_fin:
            _str += f" au {format_d_m_Y(self.date_fin)}"
        return _str


class Document(models.Model):
    famille = models.ForeignKey(Famille, related_name='documents', on_delete=models.CASCADE)
    date_creation = models.DateTimeField(null=True, blank=True)
    auteur = models.ForeignKey(Utilisateur, null=True, blank=True, on_delete=models.SET_NULL)
    fichier = models.FileField("Nouveau fichier", upload_to='doc')
    titre = models.CharField(max_length=100)

    class Meta:
        unique_together = ('famille', 'titre')

    def __str__(self):
        return self.titre

    def delete(self):
        try:
            os.remove(self.fichier.path)
        except OSError:
            pass
        return super().delete()

    def can_edit(self, user):
        return self.famille.can_edit(user)

    def can_be_deleted(self, user):
        return self.famille.can_be_deleted(user)


class JournalBase(models.Model):
    auteur = models.ForeignKey(Utilisateur, on_delete=models.PROTECT, verbose_name='auteur')
    theme = models.CharField('thème', max_length=100)
    date = models.DateTimeField('date/heure')
    texte = models.TextField('contenu')

    class Meta:
        abstract = True


class LibellePrestation(models.Model):
    code = models.CharField('Code', max_length=5, unique=True)
    nom = models.CharField('Nom', max_length=30)
    unite = models.CharField('Unité', max_length=10, choices=choices.UNITE_CARREFOUR_CHOICES)

    class Meta:
        ordering = ('code',)

    def __str__(self):
        return self.nom


class PrestationBase(models.Model):
    date_prestation = models.DateField("date de la prestation")
    duree = models.DurationField("durée")
    lib_prestation = models.ForeignKey(
        LibellePrestation, on_delete=models.SET_NULL, null=True, default=None,
        related_name='prestations_%(app_label)s'
    )

    # jour du mois maximum où il est encore possible de saisir des prestations du mois précédent
    max_jour = 5
    max_jour_re = 8

    class Meta:
        ordering = ('-date_prestation',)
        abstract = True

    def __str__(self):
        if self.famille:
            return f'Prestation pour la famille {self.famille} le {self.date_prestation} : {self.duree}'
        return f'Prestation générale le {self.date_prestation} : {self.duree}'

    @classmethod
    def check_date_editable(cls, dt, user):
        """Contrôle si la prestation est modifiable en fonction de la date 'dt'."""
        max_delai = cls.max_jour_re if user.is_responsable_equipe() else cls.max_jour
        today = date.today()
        today_minus_delay = today - timedelta(days=max_delai)
        return (
            (dt.year == today.year and dt.month == today.month) or
            (dt.year == today_minus_delay.year and dt.month == today_minus_delay.month)
        )

    @classmethod
    def prestations_historiques(cls, prestations):
        """
        Renvoie un queryset avec toutes les prestations regroupées par annee/mois
        et le total correspondant.
        """
        return prestations.annotate(
            annee=ExtractYear('date_prestation'),
            mois=ExtractMonth('date_prestation')
        ).values('annee', 'mois').order_by('-annee', '-mois').annotate(total=Sum('duree'))

    @classmethod
    def temps_total(cls, prestations, tous_utilisateurs=False):
        """
        Renvoie le temps total des `prestations` (QuerySet) sous forme de chaîne '12:30'.
        Si tous_utilisateurs = True, multiplie le temps de chaque prestation par son
        nombre d'intervenants.
        """
        if prestations.count() > 0:
            if tous_utilisateurs:
                duree = prestations.annotate(num_util=Count('intervenants')).aggregate(
                    total=Sum(F('duree') * F('num_util'), output_field=models.DurationField())
                )['total']
            else:
                duree = prestations.aggregate(total=Sum('duree'))['total']
        else:
            duree = timedelta()
        return format_duree(duree)

    @classmethod
    def temps_total_general(cls, annee, familles=True):
        """
        Renvoie le temps total des prestations (familiales ou générales selon
        familles) pour l'année civile `annee`.
        """
        prest_tot = cls.objects.filter(
            famille__isnull=not familles,
            date_prestation__year=annee
        ).aggregate(total=Sum('duree'))['total'] or timedelta()
        return format_duree(prest_tot)

    @classmethod
    def temps_total_general_fam_gen(cls, annee):
        """
        Renvoie le temps total des prestations familiales ET générales pour l'année civile `annee`.
        """
        prest_tot = cls.objects.filter(
            date_prestation__year=annee
        ).aggregate(total=Sum('duree'))['total'] or timedelta()
        return format_duree(prest_tot)

    @classmethod
    def temps_totaux_mensuels(cls, annee):
        """
        Renvoie une liste du total mensuel de toutes les prestations familiales
        (sans prestations générales) pour l'année en cours (de janv. à déc.).
        """
        data = []
        for month in range(1, 13):
            tot = cls.objects.filter(
                famille__isnull=False,
                date_prestation__year=annee, date_prestation__month=month,
            ).aggregate(total=Sum('duree'))['total'] or timedelta()
            data.append(format_duree(tot))
        return data

    @classmethod
    def temps_totaux_mensuels_fam_gen(cls, annee):
        """
        Renvoie une liste du total mensuel de toutes les prestations familiales
        et générales pour l'année en cours (de janv. à déc.).
        """
        data = []
        for month in range(1, 13):
            tot = cls.objects.filter(
                date_prestation__year=annee, date_prestation__month=month,
            ).aggregate(total=Sum('duree'))['total'] or timedelta()
            data.append(format_duree(tot))
        return data


class ActeAPrester(models.Model):
    libelle_prestation = models.ForeignKey(LibellePrestation, on_delete=models.CASCADE, related_name='actes_aprester')
    nom = models.CharField('Nom', max_length=80)

    class Meta:
        verbose_name = 'Acte à prester'
        verbose_name_plural = 'Actes à prester'

    def __str__(self):
        return self.nom


class ProlongationBase(models.Model):

    date_debut = models.DateField("Date de début")
    date_fin = models.DateField("Date de fin", blank=True, null=True)
    intervenants = models.CharField("Intervenant-e-s", max_length=250, blank=True)
    membres_famille = models.CharField("Membres de la famille", max_length=250, blank=True)
    autres = models.CharField("Autres pers. présentes", max_length=250, blank=True)
    bilan = models.TextField("Bilan", blank=True)
    decision = models.TextField("Décision", blank=True)

    class Meta:
        abstract = True


class DerogationSaisieTardive(models.Model):
    """  Dérogation pour saisie tardive de prestations """

    educ = models.ForeignKey(to=Utilisateur, on_delete=models.CASCADE)
    duree = DateRangeField('Durée')

    def __str__(self):
        return f'{str(self.educ)}: du {format_d_m_Y(self.duree.lower)} au {format_d_m_Y(self.duree.upper)}'
