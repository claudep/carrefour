from datetime import date
from dateutil.relativedelta import relativedelta

from django import template
from django.template.defaultfilters import linebreaksbr
from django.templatetags.static import static
from django.urls import reverse
from django.utils.dates import MONTHS
from django.utils.html import escape, format_html_join, format_html
from django.utils.safestring import mark_safe
from django.utils.text import Truncator

from asaef.models import ProlongationAsaef
from carrefour.models import Role, Service
from carrefour.utils import format_d_m_Y, format_duree as _format_duree, format_adresse

register = template.Library()

CONTRACT_TOOLTIP = """
<button formaction="{}" class="btn btn-xs {}" data-toggle="tooltip" data-placement="top"
title="La personne sera mentionnée (en vert) ou non (en rouge) dans le contrat.
Cliquez pour modifier.">Contrat</button>
"""


def get_param_replace(context, **kwargs):
    d = context['request'].GET.copy()
    for k, v in kwargs.items():
        d[k] = v
    for k in [k for k, v in d.items() if not v]:
        del d[k]
    return d.urlencode()


@register.filter
def index(indexable, i):
    return indexable[i]


@register.simple_tag
def relative_url(value, field_name, urlencode=None):
    url = '?{}={}'.format(field_name, value)
    if urlencode:
        querystring = urlencode.split('&')
        filtered_querystring = filter(lambda p: p.split('=')[0] != field_name, querystring)
        encoded_querystring = '&'.join(filtered_querystring)
        url = '{}&{}'.format(url, encoded_querystring)
    return url


@register.simple_tag
def get_verbose_field_name(instance, field_name):
    """
    Returns verbose_name for a field.
    """
    return instance._meta.get_field(field_name).verbose_name.capitalize()


@register.simple_tag
def get_field_value(instance, field_name):
    return getattr(instance, field_name)


@register.filter
def can_edit(obj, user):
    return obj.can_edit(user)


@register.filter
def sigles_referents(suivi):
    return format_html_join(
        '/', '<span title="{}">{}</span>', ((ref.nom_prenom, ref.sigle or ref.nom) for ref in suivi.suivi_referents)
    )


@register.filter
def referents_pk_data(suivi):
    return ':'.join([str(ref.pk) for ref in suivi.suivi_referents])


@register.filter
def in_parens(value):
    """Enclose value in parentheses only if it's not empty."""
    return '' if value in (None, '', []) else '({})'.format(value)


@register.filter
def etape_cellule(suivi, code_etape):
    """Produit le contenu d'une cellule du tableau de suivi des étapes."""
    etape = suivi.WORKFLOW[code_etape]
    etape_suiv = suivi.etape_suivante()
    date_etape = etape.date(suivi)
    date_formatted = ''
    css_class = ''
    if date_etape:
        date_formatted = format_d_m_Y(date_etape)
        css_class = 'filled'
    elif etape_suiv and code_etape == etape_suiv.code:
        date_suiv = suivi.date_suivante()
        date_formatted = format_d_m_Y(date_suiv)
        delta = date_suiv - date.today()
        css_class = 'next'
        if 19 > delta.days > 0:
            css_class = 'urgent'
        if delta.days < 0:
            css_class = 'depasse'
        code_etape = etape_suiv.abrev
    return format_html(
        '<div title="{}:{}" class="{}" >{}</div>', code_etape, date_formatted, css_class, etape.abrev
    )


@register.filter
def etape_cellule_batoude(benef, code_etape):
    """Produit le contenu d'une cellule du tableau de suivi des étapes."""
    etape = benef.WORKFLOW[code_etape]
    etape_suiv = benef.etape_suivante()
    title = ''
    css_class = ''
    if not etape.actif:
        today = date.today()
        mois = ((today.year - benef.date_admission.year) * 12 + (today.month - benef.date_admission.month)) or 1
        mm = int(code_etape[:-4])
        delta = mm - mois
        if delta < 6:
            css_class = 'next'
        if delta == 0:
            css_class = 'urgent'
        if mois > mm:
            css_class = 'filled'
        date_formatted = format_d_m_Y(benef.date_admission + relativedelta(months=mm))
        title = f"délai:{date_formatted}"
    else:
        date_etape = etape.date(benef)
        if date_etape:
            date_formatted = format_d_m_Y(date_etape)
            css_class = 'filled'
            title = f'{code_etape}:{date_formatted}'
        elif etape_suiv and code_etape == etape_suiv.code:
            date_suiv = benef.date_suivante()
            date_formatted = format_d_m_Y(date_suiv)
            delta = date_suiv - date.today()
            css_class = 'next'
            if etape.delai * 0.8 > delta.days > 0:
                css_class = 'urgent'
            if delta.days < 0:
                css_class = 'depasse'
            title = f"délai:{date_formatted}"
    return format_html('<div title="{}" class="{}">{}</div>', title, css_class, etape.abrev)


@register.filter
def default_if_zero(duree):
    return '' if duree == '00:00' else duree


@register.filter
def strip_seconds(duree):
    if str(duree).count(':') > 1:
        return ':'.join(format_duree(duree).split(':')[:2])


@register.filter
def format_duree(duree):
    return _format_duree(duree)


@register.filter
def month_name(month_number):
    return MONTHS[month_number]


@register.filter(name='has_group')
def has_group(user, group_name):
    return user.groups.filter(name=group_name).exists()


@register.filter
def info_ope(ope):
    if ope:
        return format_html(
            '<span title="{}">{}</span>', ope.telephone, ope.nom_prenom,
        )
    else:
        return ''


@register.filter
def parents(famille, nom_role):
    """
    Renvoie une ligne de tableau avec les infos de la personne ayant `nom_role`,
    ou un lien vers l'ajout d'une nouvelle personne.
    """
    prompt = {
        'pere': 'Père',
        'mere': 'Mère',
    }

    parent = famille.pere() if nom_role == 'pere' else famille.mere()
    if parent:
        contrat = format_html(
            CONTRACT_TOOLTIP,
            reverse('personne-toggle-contract', args=[parent.pk]),
            'btn-success' if parent.contractant else 'btn-danger')
        return format_html(
            '<td width="100px">{}:</td><td>{} <a href="{}">{}</a> - {} / {} </td>',
            parent.role, contrat, parent.edit_url, parent.nom_prenom, parent.adresse, parent.telephone,

        )
    else:
        role = Role.objects.get(nom=prompt[nom_role]) if nom_role in ['pere', 'mere'] else None
        if role:
            qs = '?role={}'.format(role.pk)
        return format_html(
            '<td colspan="2"><a class="btn btn-outline-primary btn-sm ml-3" '
            'role="button" href="{}{}">Ajouter {}</a></td><td></td>',
            reverse('personne-add', args=[famille.typ, famille.pk]),
            qs,
            prompt[nom_role]
        )


@register.filter
def deletable(famille, user):
    return famille.can_be_deleted(user)


@register.filter
def nom_prenom_abreg(person):
    return '{} {}.'.format(person.nom, person.prenom[0].upper() if person.prenom else '')


@register.filter
def path_add_letter(request, letter):
    qs = request.GET.copy()
    qs['l'] = letter
    return '{}?{}'.format(request.path, qs.urlencode())


@register.simple_tag
def mes_totaux_mensuels(user, src, annee):
    return user.totaux_mensuels(src, annee)


@register.simple_tag
def mon_total_annuel(user, src, annee):
    return user.total_annuel(src, annee)


@register.filter
def sigles_intervenants(prestation):
    return format_html_join(
        '/', '{}', ((i.sigle,) for i in prestation.intervenants.all())
    )


@register.simple_tag(takes_context=True)
def param_replace(context, **kwargs):
    return get_param_replace(context, **kwargs)


@register.filter
def colorier_delai(value):
    if value.days <= 0:
        return "bg-danger-3"
    return ''


@register.filter
def benef_nom_adresse(obj):
    if obj:
        return '{} - {}'.format(
            obj.nom_prenom(),
            format_adresse(obj.rue_actuelle, obj.npa_actuelle, obj.localite_actuelle)
        )
    return 'Nouveau bénéficiaire'


@register.filter
def role_profession(contact):
    data = []
    if contact.role:
        data.append(contact.role.nom)
    if contact.profession:
        data.append(contact.profession)
    return ' / '.join(data)


@register.filter
def truncate_with_more(text, length):
    template = '''
        <div class="long" style="display:none">{txt_long}</div>
        <div class="short">{txt_short}</div>
        {read_more}
    '''
    read_more = '<a class="read_more" href=".">Afficher la suite</a>'
    html = '</' in text
    text_br = text if html else linebreaksbr(escape(text))
    text_br_trunc = Truncator(text_br).words(int(length), html=html)
    return format_html(
        template,
        txt_long=mark_safe(text_br),
        txt_short=mark_safe(text_br_trunc),
        read_more=mark_safe(read_more) if text_br != text_br_trunc else '',
    )


@register.filter
def truncate_with_more_and_default(text, length):
    if text == '':
        return format_html(
            '<div class="no-text">{}<div>',
            'Pas de texte saisi'
        )
    else:
        return truncate_with_more(text, length)


@register.filter
def get_item(obj, key):
    return obj[key]


@register.filter
def contractant(person):
    return format_html(CONTRACT_TOOLTIP,
                       reverse('personne-toggle-contract', args=[person.pk]),
                       'btn-success' if person.contractant else 'btn-danger')


@register.filter
def prolongation_asaef(famille):
    if famille.suiviasaef.date_debut_suivi is not None:
        try:
            prolong = ProlongationAsaef.objects.get(famille=famille)
            return format_html(
                '{date_prolong}<a href="{edit_url}"><img class="icon" src="{edit_icon}"></a>\
                 <a href="{print_url}"><img class="icon" src="{print_icon}"></a>',
                date_prolong=format_d_m_Y(prolong.date_debut),
                edit_url=reverse('asaef-prolongation-edit', args=[famille.pk, prolong.pk]),
                edit_icon=static('admin/img/icon-changelink.svg'),
                print_url=reverse('asaef-prolongation-print', args=[famille.pk, prolong.pk]),
                print_icon=static('img/printer.png')
            )
        except ProlongationAsaef.DoesNotExist:
            return format_html(
                '<a class="btn btn-secondary mb-5" href="{add_url}"><img src="{add_icon}"> {text}</a>',
                add_url=reverse('asaef-prolongation-add', args=[famille.pk]),
                add_icon=static('admin/img/icon-addlink.svg'),
                text="Créer une prolongation"
            )
    return ""


@register.inclusion_tag('form_item_inline.html')
def form_item_inline(field, wlabel, wfield, offset=None):
    return {'field': field, 'w1': wlabel, 'w2': wfield, 'offset': offset}


@register.inclusion_tag('form_item.html')
def form_item(field, width, offset=None):
    return {'field': field, 'w1': width, 'offset': offset}


@register.inclusion_tag('statistiques/ser_partial_header.html')
def partial_stat_ser(titre, stat, months):
    return {'titre': titre, 'stat': stat, 'months': months}


@register.inclusion_tag('statistiques/stats_table_header.html', takes_context=True)
def stats_table_header(context, col1=None, col2=None):
    url = get_param_replace(context)
    return {'titre': context['titre'],
            'months': context['months'],
            'additional_col': 'heure_intervenant' in context['request'].path,
            'param': url,
            'col1': col1, 'col2': col2,
            'can_export': context['can_export']}


@register.filter
def get_field_for_merge(obj, fieldname):
    if fieldname not in ['nom', 'prenom', 'rue', 'npa', 'localite', 'telephone', 'email',
                         'profession', 'remarque', 'role', 'service']:
        return ''
    if fieldname == 'role':
        return Role.objects.get(pk=obj['role_id']).nom if obj['role_id'] else ''
    elif fieldname == 'service':
        return Service.objects.get(pk=obj['service_id']).sigle if obj['service_id'] else ''
    return obj[fieldname]


@register.filter
def doc_deletable(doc, user):
    return doc.can_be_deleted(user)


@register.simple_tag(takes_context=True)
def stat_export_sortie_ser(context):
    return format_html(
        '<a href=".?{param}&export=1" class="btn btn-sm btn-outline-primary">Export *.xlsx</a>',
        param=get_param_replace(context)
    )


@register.simple_tag
def check_majorite(person):
    if person.naissance is None:
        return "Date de naissance inconnue"
    today = date.today()
    majorite_en_mois = 18 * 12
    mois_depuis_naissance = (today.year - person.naissance.year) * 12 + (today.month - person.naissance.month)
    delta = mois_depuis_naissance - majorite_en_mois
    css_class = 'text-success' if delta > 0 else (
        'text-warning bg-dark p-1' if abs(delta) < 4 else 'text-danger fw-bold'
    )
    foo = 'depuis' if delta > 0 else 'dans'
    return format_html('<div class="{}">Majeur-e {} {} mois</div>', css_class, foo, abs(delta))


@register.simple_tag
def duree_suivi(person):
    if person.date_admission is None:
        return "Date d'admission inconnue"
    today = date.today()
    mois = (today.year - person.date_admission.year) * 12 + (today.month - person.date_admission.month)
    css_class = 'text-warning bg-dark p-1' if mois > 23 else ''
    return format_html('<div class="{}">Durée du suivi: {} mois</div>', css_class, mois)


@register.inclusion_tag('batoude/partial_bouton_bilan.html')
def bouton_bilan(person, typ):
    return {'person': person, 'typ': typ}
