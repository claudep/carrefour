from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm
from django.contrib.auth.models import Permission

from .models import (
    Personne, Contact, Document, Service, CercleScolaire, Formation, Utilisateur, Role, Famille,
    LibellePrestation, ActeAPrester, DerogationSaisieTardive
)


class TypePrestationFilter(admin.SimpleListFilter):
    title = 'Prest. famil./générales'
    parameter_name = 'prest'
    default_value = None

    def lookups(self, request, model_admin):
        return (
            ('fam', 'Familiales'),
            ('gen', 'Générales'),
        )

    def queryset(self, request, queryset):
        value = self.value()
        if value == 'fam':
            return queryset.filter(famille__isnull=False).order_by('famille__nom')
        elif value == 'gen':
            return queryset.filter(famille__isnull=True).order_by('-date_prestation')
        return queryset


class DocumentInline(admin.TabularInline):
    model = Document
    extra = 1


class PersonneInLine(admin.TabularInline):
    model = Personne
    exclude = ('reseaux', 'remarque')
    extra = 1


@admin.register(Personne)
class PersonneAdmin(admin.ModelAdmin):
    list_display = ('nom_prenom', 'adresse')
    search_fields = ('nom', 'prenom')


@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    list_display = ('nom', 'prenom', 'service', 'est_actif')
    list_filter = ('service', 'est_actif')
    readonly_fields = ['dossiers_actifs']

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(utilisateur__isnull=True).prefetch_related(
            'personne_set', 'beneficiaire_set', 'beneficiaires'
        )

    def dossiers_actifs(self, obj):
        return '\n\r'.join(obj.dossiers(actif=True))


@admin.register(Famille)
class FamilleAdmin(admin.ModelAdmin):
    inlines = [PersonneInLine, DocumentInline]
    search_fields = ('nom', 'localite')


@admin.register(Formation)
class FormationAdmin(admin.ModelAdmin):
    list_display = ('personne', 'statut')
    search_fields = ('personne__nom',)
    ordering = ('personne__nom',)


class UtilisateurChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = Utilisateur


@admin.register(Utilisateur)
class UtilisateurAdmin(UserAdmin):
    form = UtilisateurChangeForm
    list_display = ('nom', 'prenom', 'telephone', 'email', 'is_active')
    fieldsets = UserAdmin.fieldsets + (
        (None, {'fields': (
            'sigle', 'prenom', 'nom', 'rue', 'npa', 'localite',
            'telephone', 'service', 'taux_activite', 'date_desactivation'
        )}),
    )


class ActeAPresterInline(admin.TabularInline):
    model = ActeAPrester
    extra = 0


@admin.register(LibellePrestation)
class LibellePrestationAdmin(admin.ModelAdmin):
    list_display = ('code', 'nom', 'unite')
    inlines = [ActeAPresterInline]
    fk_name = 'libelle_prestation'


@admin.register(ActeAPrester)
class ActeAPresterAdmin(admin.ModelAdmin):
    list_display = ('libelle_prestation', 'nom')
    ordering = ('libelle_prestation',)


@admin.register(Permission)
class PermissionAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'codename']
    search_fields = ['name', 'codename']


@admin.register(Document)
class DocumentAdmin(admin.ModelAdmin):
    list_display = ('famille', 'titre')
    search_fields = ('famille__nom',)
    ordering = ('famille__nom',)


@admin.register(DerogationSaisieTardive)
class DerogationSaisieTardiveAdmin(admin.ModelAdmin):
    list_display = ('educ', 'duree')


admin.site.register(CercleScolaire)
admin.site.register(Role)
admin.site.register(Service)
