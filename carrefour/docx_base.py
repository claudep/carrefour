from docx import Document
from docx.shared import Cm, Pt
from docx.enum.style import WD_STYLE_TYPE
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT

from django.contrib.staticfiles.finders import find
from django.utils.dateformat import format as django_format

from settings.carrefour import CARREFOUR_NOM, CARREFOUR_CONTACT

docx_content_type = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'


class DocxBase:
    footer = f"{CARREFOUR_NOM}\n{CARREFOUR_CONTACT}"

    def __init__(self, path):
        self.path = path
        self.document = Document()
        self.style_normal = self.document.styles['Normal']
        font = self.style_normal.font
        font.name = "Arial"
        font.size = Pt(11)
        paragraph_format = self.style_normal.paragraph_format
        paragraph_format.space_before = Pt(2)
        paragraph_format.space_after = Pt(2)

        picture_file = find('img/logo.png')
        self.document.add_picture(picture_file, width=Cm(7), height=Cm(2.4))

        self.style_titre1 = self.document.styles.add_style('Titre1', WD_STYLE_TYPE.PARAGRAPH)
        self.style_titre1.font.name = 'Arial'
        self.style_titre1.font.size = Pt(13)
        self.style_titre1.font.bold = True
        paragraph_format = self.document.styles['Titre1'].paragraph_format
        paragraph_format.space_before = Pt(12)
        paragraph_format.space_after = Pt(24)

        self.style_soustitre = self.document.styles.add_style('SousTitre', WD_STYLE_TYPE.PARAGRAPH)
        self.style_soustitre.font.name = 'Arial-Bold'
        self.style_soustitre.font.size = Pt(11)
        self.style_soustitre.font.bold = True
        paragraph_format = self.document.styles['SousTitre'].paragraph_format
        paragraph_format.space_before = Pt(6)
        paragraph_format.space_after = Pt(6)

        self.style_footer = self.document.styles.add_style('my_footer', WD_STYLE_TYPE.PARAGRAPH)
        self.style_footer.font.name = 'Arial'
        self.style_footer.font.size = Pt(7)
        paragraph_format = self.document.styles['my_footer'].paragraph_format
        paragraph_format.space_before = Pt(2)

        self.document.sections[0].top_margin = Cm(1)
        self.document.sections[0].left_margin = Cm(2)
        self.document.sections[0].righ_margin = Cm(2)
        self.document.sections[0].bottom_margin = Cm(1)

        footer = self.document.sections[0].footer
        p = footer.add_paragraph('_'*115, self.style_footer)
        p.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
        p = footer.add_paragraph(self.footer, self.style_footer)
        p.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER

    def set_column_width(self, column, width):
        column.width = Cm(width)
        for cell in column.cells:
            cell.width = Cm(width)

    def date_formated(self, date):
        return django_format(date, 'j.m.Y') if date else ''
