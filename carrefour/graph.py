import base64
import numpy as np

from io import BytesIO
from matplotlib.figure import Figure


class SondageRadarGraph:

    def __init__(self, nom, sondage, reponses):
        themes = sondage.themes.all()
        themes = [*themes, themes[0]]  # to close the radar, duplicate the first column

        n_points = len(themes)
        label_loc = np.linspace(start=0, stop=2 * np.pi, num=n_points)

        group_1 = reponses
        group_1 = [*group_1, group_1[0]]  # same here duplicate first value to close the radar

        fig = Figure()
        ax = fig.add_subplot(polar=True)
        ax.plot(label_loc, group_1, 'o--', color='green')
        ax.fill(label_loc, group_1, alpha=0.15, color='green')

        # Fix axis to go in the right order and start at 12 o'clock.
        ax.set_theta_offset(np.pi / 2)
        ax.set_theta_direction(-1)
        ax.set_thetagrids(np.degrees(label_loc), themes)
        ax.set_ylim(0, sondage.nbre_choix)
        ax.grid(color='#AAAAAA')
        ax.set_title(nom)

        buffer = BytesIO()
        fig.savefig(buffer, format='png')
        buffer.seek(0)
        image_png = buffer.getvalue()
        buffer.close()

        graphic = base64.b64encode(image_png)
        self.graphic = graphic.decode('utf-8')
