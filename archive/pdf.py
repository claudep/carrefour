import subprocess
import tempfile
from io import BytesIO
from pathlib import Path

from django.utils.text import slugify

from pypdf import PdfWriter, PdfReader

from aemo import pdf as aemo_pdf
from asaef import pdf as asaef_pdf
from batoude import pdf as batoude_pdf
from carrefour.pdf import BaseCarrefourPDF, MessagePdf


class ArchiveBase(BaseCarrefourPDF):
    def __init__(self, tampon, with_page_num=True, **kwargs):
        super().__init__(tampon, with_page_num, **kwargs)
        self.tampon = tampon
        self.merger = PdfWriter()

    def get_filename(self, fam_or_benef):
        return f"{slugify(fam_or_benef.nom)}-{fam_or_benef.pk}.pdf"

    def append_pdf(self, PDFClass, obj):
        temp = BytesIO()
        pdf = PDFClass(temp)
        pdf.produce(obj)
        self.merger.append(temp)

    def append_other_docs(self, documents):
        msg = []
        for doc in documents:
            doc_path = Path(doc.fichier.path)
            if not doc_path.exists():
                msg.append(f"Le fichier «{doc.titre}» n'existe pas!")
                continue
            if doc_path.suffix.lower() == '.pdf':
                self.merger.append(PdfReader(str(doc_path)))
            elif doc_path.suffix.lower() in ['.doc', '.docx']:
                with tempfile.TemporaryDirectory() as tmpdir:
                    cmd = ['libreoffice', '--headless', '--convert-to', 'pdf', '--outdir', tmpdir, doc_path]
                    subprocess.run(cmd, capture_output=True)
                    converted_path = Path(tmpdir) / f'{doc_path.stem}.pdf'
                    if converted_path.exists():
                        self.merger.append(PdfReader(str(converted_path)))
                    else:
                        msg.append(f"La conversion du fichier «{doc.titre}» a échoué")
            elif doc_path.suffix.lower() == '.msg':
                self.append_pdf(MessagePdf, doc)
            else:
                msg.append(f"Le format du fichier «{doc.titre}» ne peut pas être intégré.")
        return msg


class ArchiveAemo(ArchiveBase):
    title = "Archive AEMO"

    def produce(self, famille, **kwargs):
        self.append_pdf(aemo_pdf.CoordonneesPagePdf, famille)
        self.merger.write(self.tampon)
        return []


class ArchiveAsaef(ArchiveBase):
    title = "Archive ASAEF"

    def produce(self, famille, **kwargs):
        self.append_pdf(asaef_pdf.CoordonneesPdf, famille)
        self.append_pdf(asaef_pdf.ContratCollaborationPdf, famille)
        for bilan in famille.bilans_analyses.all():
            self.append_pdf(asaef_pdf.BilanAnalysePdf, bilan)
        if famille.genogramme:
            self.merger.append(PdfReader(famille.genogramme.path))
        if famille.sociogramme:
            self.merger.append(PdfReader(famille.sociogramme.path))
        for bilan in famille.bilans.all():
            self.append_pdf(asaef_pdf.BilanPdf, bilan)
        for prolongation in famille.prolongations_asaef.all():
            self.append_pdf(asaef_pdf.ProlongationAsaefPdf, prolongation)
        msg = self.append_other_docs(famille.documents.all())
        self.merger.write(self.tampon)
        return msg


class ArchiveBatoude(ArchiveBase):
    title = "Archive Batoude"

    def produce(self, benef, **kwargs):
        self.append_pdf(batoude_pdf.BeneficiaireCoordinatesPdf, benef)
        msg = self.append_other_docs(benef.documents.all())
        self.merger.write(self.tampon)
        return msg
