from django.contrib import admin

from archive.models import Archive


@admin.register(Archive)
class ArchiveAdmin(admin.ModelAdmin):
    list_display = ['nom', 'unite', 'date_debut', 'date_fin']
    list_filter = ['unite']
    ordering = ['nom']
    search_fields = ['nom']
