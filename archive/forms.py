from django import forms


class ArchiveFilterForm(forms.Form):

    search_famille = forms.CharField(
        label='Recherche par nom de famille',
        max_length=30,
        required=False
    )

    search_ref_aemo = forms.CharField(
        label='Recherche par réf. AEMO',
        max_length=30,
        required=False
    )

    def filter(self, archives):
        if not self.cleaned_data['search_famille'] and not self.cleaned_data['search_ref_aemo']:
            return archives.none()
        if self.cleaned_data['search_famille']:
            archives = archives.filter(nom__icontains=self.cleaned_data['search_famille'])
        if self.cleaned_data['search_ref_aemo']:
            archives = archives.filter(referent_carrefour__icontains=self.cleaned_data['search_ref_aemo'])
        return archives
