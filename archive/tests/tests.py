import datetime
import os.path
import subprocess
import tempfile
from datetime import date
from pathlib import Path

from django.conf import settings
from django.contrib.auth.models import Group, Permission
from django.core.files import File
from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils import timezone
from django.utils.text import slugify

from aemo.models import FamilleAemo, JournalAemo, PrestationAemo
from asaef.models import (
    BilanAnalyseAsaef, BilanAsaef, FamilleAsaef, JournalAsaef, PrestationAsaef,
    ProlongationAsaef
)
from batoude.models import Beneficiaire
from carrefour.models import Contact, Document, LibellePrestation, Personne, Role, Utilisateur
from carrefour.tests import InitialDataMixin
from common.test_utils import TempMediaRootMixin

from ..models import Archive

public_key_path = os.path.join(settings.BASE_DIR, 'archive/tests/carrefour_id_rsa.pub')


@override_settings(CARREFOUR_RSA_PUBLIC_KEY=public_key_path)
class ArchiveTests(InitialDataMixin, TempMediaRootMixin, TestCase):

    def setUp(self) -> None:
        self.user = Utilisateur.objects.create_user('user', 'user@example.org', sigle='XX')

    def _create_archive(self):
        fam = FamilleAsaef.objects.get(nom='Tournesol')
        fam.suivi.date_fin_suivi = date(2019, 1, 1)
        fam.suivi.motif_fin_suivi = 'autre'
        fam.suivi.save()
        Personne.objects.create_personne(
            nom='Tournesol', prenom='Paul', genre='M', famille=fam, role=Role.objects.get(nom='Enfant suivi')
        )
        for idx, doc_name in enumerate(['sample.docx', 'sample.doc', 'sample.pdf', 'sample.msg']):
            doc = Document(famille=fam, titre=f"Test {idx}")
            with (Path(__file__).parent / doc_name).open(mode='rb') as fh:
                doc.fichier = File(fh, name=doc_name)
                doc.save()

        self.user.user_permissions.add(Permission.objects.get(codename='export_aemo'))
        self.client.force_login(self.user)
        self.client.post(reverse('archive-add', args=['asaef', fam.pk]))
        return Archive.objects.get(nom='Tournesol', unite='asaef')

    def test_model_creation(self):
        arch = Archive.objects.create(**{
            'nom': 'John Doe',
            'unite': 'aemo',
            'date_debut': date(2021, 1, 1),
            'date_fin': date(2021, 12, 31),
            'key': '',
            'pdf': 'encrypted_data'
        })
        self.assertEqual(arch.nom, 'John Doe')
        self.assertEqual(arch.unite, 'aemo')
        self.assertEqual(arch.date_debut, date(2021, 1, 1))
        self.assertEqual(arch.date_fin, date(2021, 12, 31))
        self.assertEqual(arch.pdf, 'encrypted_data')

    def test_archivage_aemo(self):
        fam = FamilleAemo.objects.get(nom='Haddock')
        fam.suivi.date_fin_suivi = date(2009, 1, 1)
        fam.suivi.motif_fin_suivi = 'autre'
        fam.suivi.save()
        docx = Document(famille=fam, titre="Test")
        with (Path(__file__).parent / 'sample.docx').open(mode='rb') as fh:
            docx.fichier = File(fh, name='sample.docx')
            docx.save()
        JournalAemo.objects.create(
            famille=fam, auteur=self.user, theme="Un thème", date=timezone.now(), texte="Un texte")
        prest = PrestationAemo.objects.create(
            famille=fam, date_prestation=date.today(), duree=datetime.timedelta(hours=2),
            lib_prestation=LibellePrestation.objects.filter(unite='aemo').first())
        prest.intervenants.add(self.user)

        self.user.groups.add(Group.objects.get(name='aemo_montagnes'))
        self.user.user_permissions.add(
            *list(Permission.objects.filter(codename__in=['view_montagnes', 'manage_montagnes']))
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('aemo-suivis-termines'))
        self.assertEqual(response.status_code, 200)
        self.assertFalse(fam.can_be_deleted(self.user))

        self.user.user_permissions.add(Permission.objects.get(codename='export_aemo'))
        self.client.force_login(self.user)
        response = self.client.post(reverse('archive-add', args=['aemo', fam.pk]))
        self.assertRedirects(response, reverse('aemo-suivis-termines'))
        self.assertFalse(FamilleAemo.objects.filter(nom='Haddock').exists())
        arch = Archive.objects.get(nom='Haddock', unite='aemo')
        self.assertTrue(os.path.exists(arch.pdf.path))
        self.assertIn(f"aemo/{slugify(fam.nom)}-{fam.pk}", arch.pdf.name)
        self.assertFalse(fam.journaux.exists())
        self.assertTrue(fam.prestations_aemo.exists())

        # Cannot be archived a second time:
        response = self.client.post(reverse('archive-add', args=['aemo', fam.pk]))
        self.assertEqual(response.status_code, 404)

    def test_archivage_asaef(self):
        fam = FamilleAsaef.objects.get(nom='Tournesol')
        fam.suivi.date_fin_suivi = date(2009, 1, 1)
        fam.suivi.motif_fin_suivi = 'autre'
        fam.suivi.save()

        self.user.groups.add(Group.objects.get(name='asaef'))
        self.user.user_permissions.add(Permission.objects.get(codename='export_aemo'))
        self.client.force_login(self.user)
        self.assertTrue(fam.can_be_deleted(self.user))

        self.client.post(reverse('archive-add', args=['asaef', fam.pk]))
        self.assertFalse(FamilleAsaef.objects.filter(nom='Tournesol').exists())
        arch = Archive.objects.get(nom='Tournesol', unite='asaef')
        self.assertTrue(os.path.exists(arch.pdf.path))
        self.assertIn(f"asaef/{slugify(fam.nom)}-{fam.pk}", arch.pdf.name)
        # Cannot be archived a second time:
        response = self.client.post(reverse('archive-add', args=['asaef', fam.pk]))
        self.assertEqual(response.status_code, 404)

    def test_archivage_asaef_avec_prolongation(self):
        fam = FamilleAsaef.objects.get(nom='Tournesol')
        fam.suivi.date_fin_suivi = date(2009, 1, 1)
        fam.suivi.motif_fin_suivi = 'autre'
        fam.suivi.date_prolongation = date(2022, 1, 1)
        fam.suivi.save()

        ProlongationAsaef.objects.create(date_debut=date(2022, 1, 1), famille=fam)

        self.user.groups.add(Group.objects.get(name='asaef'))
        self.user.user_permissions.add(Permission.objects.get(codename='export_aemo'))
        self.client.force_login(self.user)
        self.client.post(reverse('archive-add', args=['asaef', fam.pk]))
        fam.refresh_from_db()
        self.assertNotEqual(fam.nom, 'Tournesol')
        self.assertFalse(fam.prolongations_asaef.exists())

    def test_archivage_asaef_complet(self):
        fam = FamilleAsaef.objects.get(nom='Tournesol')
        fam.suivi.date_fin_suivi = date(2009, 1, 1)
        fam.suivi.motif_fin_suivi = 'autre'
        fam.suivi.date_prolongation = date(2022, 1, 1)
        fam.suivi.save()
        bilan_analyse = BilanAnalyseAsaef.objects.create(famille=fam, date=date.today(), membres_famille='John Doe')
        bilan_analyse.destinataires.add(fam.pere())

        ProlongationAsaef.objects.create(date_debut=date(2022, 1, 1), famille=fam)
        BilanAsaef.objects.create(famille=fam, date=date.today(), typ='1mois')
        BilanAsaef.objects.create(famille=fam, date=date.today(), typ='3mois')
        JournalAsaef.objects.create(famille=fam, auteur=self.user, date=timezone.now(),
                                    theme="Un test", texte="Un text")
        PrestationAsaef.objects.create(
            famille=fam,
            lib_prestation=LibellePrestation.objects.filter(unite='asaef').first(),
            date_prestation=date.today(),
            duree=datetime.timedelta(hours=8),
            theme='Un test',
            texte='Un essai',
            intervenant=self.user
        )
        self.user.groups.add(Group.objects.get(name='asaef'))
        self.user.user_permissions.add(Permission.objects.get(codename='export_aemo'))
        self.client.force_login(self.user)
        self.client.post(reverse('archive-add', args=['asaef', fam.pk]))
        fam.refresh_from_db()
        self.assertNotEqual(fam.nom, 'Tournesol')
        self.assertFalse(fam.bilans.exists())
        self.assertFalse(fam.bilans_analyses.exists())
        self.assertFalse(fam.journaux.exists())
        self.assertFalse(fam.prolongations_asaef.exists())
        self.assertEqual(fam.prestations_asaef.first().theme, '')
        self.assertEqual(fam.prestations_asaef.first().texte, '')

    def test_archivage_batoude(self):
        benef = Beneficiaire.objects.create(nom='Batoudon', prenom='Marc-André', genre='M',
                                            date_admission=date(2008, 1, 1), date_sortie=date(2009, 12, 31))
        self.user.user_permissions.add(Permission.objects.get(codename='export_aemo'))
        self.client.force_login(self.user)
        self.assertTrue(benef.can_be_deleted(self.user))

        self.client.post(reverse('archive-add', args=['batoude', benef.pk]))
        self.assertFalse(Beneficiaire.objects.filter(nom='Batoudon').exists())
        arch = Archive.objects.get(nom='Batoudon', unite='batoude')
        self.assertTrue(os.path.exists(arch.pdf.path))
        self.assertIn(f"batoude/{slugify(benef.nom)}-{benef.pk}", arch.pdf.name)
        # Cannot be archived a second time:
        response = self.client.post(reverse('archive-add', args=['batoude', benef.pk]))
        self.assertEqual(response.status_code, 404)

    def test_archivage_liste_permission(self):
        self.user.groups.add(*list(Group.objects.filter(name__in=['aemo', 'asaef', 'batoude', 'ser'])))

        # Erreur pour Educs
        self.user.user_permissions.add(
            *list(Permission.objects.filter(codename__in=[
                'view_littoral', 'view_montagnes', 'view_asaef', 'view_batoude', 'view_ser'
            ]))
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('archive-list', args=['asaef']))
        self.assertEqual(response.status_code, 403)

        # OK pour responsable
        self.user.user_permissions.add(
            *list(Permission.objects.filter(codename__in=[
                'manage_littoral', 'manage_montagnes', 'manage_asaef', 'manage_batoude', 'manage_ser'
            ]))
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('archive-list', args=['asaef']))
        self.assertEqual(response.status_code, 200)
        # OK pour direction
        self.user.user_permissions.add(Permission.objects.get(codename='view_archive'))
        response = self.client.get(reverse('archive-list', args=['asaef']))
        self.assertEqual(response.status_code, 200)

    def test_archivage_export_permission(self):
        arch = self._create_archive()
        self.user.groups.add(*list(Group.objects.filter(name__in=['aemo', 'asaef', 'batoude', 'ser'])))
        self.user.user_permissions.add(
            *list(Permission.objects.filter(codename__in=[
                'manage_littoral', 'manage_montagnes', 'manage_asaef', 'manage_batoude', 'manage_ser'
            ]))
        )
        # Error
        self.client.force_login(self.user)
        response = self.client.get(reverse('archive-export', args=[arch.pk]))
        self.assertEqual(response.status_code, 403)
        # OK
        self.user.user_permissions.add(Permission.objects.get(codename='view_archive'))
        response = self.client.get(reverse('archive-export', args=[arch.pk]))
        self.assertEqual(response.status_code, 200)

    def test_archivage_decryptage(self):
        arch = self._create_archive()
        private_key = os.path.join(settings.BASE_DIR, 'archive/tests/carrefour_id_rsa')

        # decryptage
        self.user.groups.add(Group.objects.get(name='asaef'))
        self.user.user_permissions.add(
            *list(Permission.objects.filter(codename__in=['view_asaef', 'manage_asaef', 'view_archive']))
        )
        self.client.force_login(self.user)
        with open(private_key, 'rb') as f:
            response = self.client.post(reverse('archive-export', args=[arch.pk]), data={'file': f})
            self.assertEqual(
                response.get('Content-Disposition'),
                "inline; filename=tournesol.pdf"
            )

        with tempfile.NamedTemporaryFile(delete=True, mode='wb') as fh:
            fh.write(response.content)
            subprocess.run(['pdftotext', fh.name], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            with open(f'{fh.name}.txt', 'r') as f:
                content = f.read()

            self.assertIn('Famille Tournesol', content)
            self.assertIn('Coordonnées', content)
            self.assertIn('Contrat de collaboration Asaef', content)
            self.assertIn("Fichier docx d’exemple", content)
            self.assertIn("Exemple de fichier doc", content)
            self.assertIn("Fichier pdf d’exemple", content)
            self.assertIn("Kind regards", content)

    def test_affichage_liste_aemo_via_ajax(self):
        fam = FamilleAemo.objects.get(nom='Haddock')
        fam.suivi.ope_referent = Contact.objects.get(nom='Rathfeld')
        fam.suivi.date_fin_suivi = '2020-12-31'
        fam.suivi.motif_fin_suivi = 'abandon_famille'
        fam.suivi.save()
        fam.add_referent(self.user_aemo)

        # Archivage
        self.user.user_permissions.add(Permission.objects.get(codename='export_aemo'))
        self.client.force_login(self.user)
        response = self.client.post(reverse('archive-add', args=['aemo', fam.pk]))
        self.assertEqual(response.status_code, 302)

        # Affichage
        initiale = fam.nom[0]
        response = self.client.get(
            reverse('archive-list', args=['aemo']) + f'?search_famille={initiale}',
            content_type='text/html',
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertContains(response, '<td>Aemo P.</td>', html=True)
        self.assertContains(response, '<td>Refus de la famille</td>', html=True)
        self.assertContains(response, '<td>Rathfeld Christophe</td>', html=True)

    def test_affichage_aemo_vide_via_get(self):
        Archive.objects.create(**{
            'nom': 'John Doe',
            'unite': 'aemo',
            'date_debut': date(2021, 1, 1),
            'date_fin': date(2021, 12, 31),
            'key': '',
            'pdf': 'encrypted_data'
        })
        self.user.user_permissions.add(Permission.objects.get(codename='export_aemo'))
        self.client.force_login(self.user)
        response = self.client.get(reverse('archive-list', args=['aemo']))
        self.assertContains(response, '<div id="archive_table"></div>', html=True)

    def test_interdiction_decryptage_asaef(self):
        Archive.objects.create(**{
            'nom': 'John Doe',
            'unite': 'asaef',
            'date_debut': date(2021, 1, 1),
            'date_fin': date(2021, 12, 31),
            'key': '',
            'pdf': 'encrypted_data'
        })

        url = reverse('archive-list', args=['asaef'])
        self.user.groups.add(Group.objects.get(name='asaef'))
        self.user.user_permissions.add(Permission.objects.get(codename='export_aemo'))
        self.client.force_login(self.user)
        response = self.client.get(url)

        # Affichage sans bouton « Télécharger »
        self.assertContains(response, 'John Doe')
        self.assertNotContains(response, 'Télécharger', html=False)

    def test_autorisation_decryptage_asaef_pour_groupe_direction(self):
        archive = Archive.objects.create(**{
            'nom': 'John Doe',
            'unite': 'asaef',
            'date_debut': date(2021, 1, 1),
            'date_fin': date(2021, 12, 31),
            'key': '',
            'pdf': 'encrypted_data'
        })

        url = reverse('archive-list', args=['asaef'])
        self.user.user_permissions.add(
            *list(Permission.objects.filter(codename__in=['export_aemo', 'view_archive'])))
        self.client.force_login(self.user)
        response = self.client.get(url)
        self.assertContains(response, 'John Doe')
        url_decrypt = reverse('archive-export', args=[archive.pk])
        self.assertContains(
            response,
            f'<a class="btn btn-sm bg-success" href="{url_decrypt}">Télécharger</a>',
            html=True
        )

    def test_interdiction_decryptage_aemo(self):
        archive = Archive.objects.create(**{
            'nom': 'John Doe',
            'unite': 'aemo',
            'date_debut': date(2021, 1, 1),
            'date_fin': date(2021, 12, 31),
            'key': '',
            'pdf': 'encrypted_data'
        })

        self.user.groups.add(Group.objects.get(name='aemo_montagnes'))
        self.user.user_permissions.add(
            *list(Permission.objects.filter(codename__in=['view_montagnes', 'manage_montagnes']))
        )
        self.client.force_login(self.user)
        initiale = archive.nom[0]
        response = self.client.get(
            reverse('archive-list', args=['aemo']) + f'?search_famille={initiale}',
            content_type='text/html',
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        # Affichage sans bouton « Télécharger »
        self.assertContains(response, 'John Doe')
        self.assertNotContains(response, 'Télécharger', html=False)

    def test_autorisation_decryptage_aemo_pour_groupe_direction(self):
        archive = Archive.objects.create(**{
            'nom': 'John Doe',
            'unite': 'aemo',
            'date_debut': date(2021, 1, 1),
            'date_fin': date(2021, 12, 31),
            'key': '',
            'pdf': 'encrypted_data'
        })
        self.user.user_permissions.add(
            *list(Permission.objects.filter(codename__in=['export_aemo', 'view_archive'])))
        self.client.force_login(self.user)
        initiale = archive.nom[0]
        response = self.client.get(
            reverse('archive-list', args=['aemo']) + f'?search_famille={initiale}',
            content_type='text/html',
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertContains(response, 'John Doe')
        url_decrypt = reverse('archive-export', args=[archive.pk])
        self.assertContains(
            response,
            f'<a class=\\"btn btn-sm bg-success\\" href=\\"{url_decrypt}\\">T\\u00e9l\\u00e9charger</a>',
            html=True
        )
