from datetime import date

from django.contrib.auth.models import Permission
from django.urls import reverse

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from aemo.models import FamilleAemo
from common.test_utils import LiveTestBase
from carrefour.models import Utilisateur
from common.test_utils import TempMediaRootMixin


class LiveTests(TempMediaRootMixin, LiveTestBase):
    def setUp(self):
        self.user = Utilisateur.objects.create_user(
            'valjean', 'me@example.org', 'mepassword', first_name='Jean', last_name='Valjean',
        )
        self.user.user_permissions.add(
            *list(Permission.objects.filter(codename__in=['view_montagnes', 'manage_montagnes', 'export_aemo']))
        )

    def test_bouton_archivage_masse_aemo(self):
        fam = FamilleAemo.objects.create_famille(
            nom='Haddock', rue='Château1', npa=2000, localite='Moulinsart',
            equipe='montagnes', autorite_parentale='conjointe', statut_marital='divorce', monoparentale=False
        )
        fam.suivi.date_fin_suivi = date(2009, 1, 1)
        fam.suivi.motif_fin_suivi = 'autre'
        fam.suivi.save()

        self.login()

        self.selenium.get('%s%s' % (self.live_server_url, reverse('aemo-suivis-termines')))
        self.selenium.find_element(By.XPATH, value='//*[@data-bs-target="#archiveModal"]').click()
        WebDriverWait(self.selenium, 2).until(
            EC.visibility_of_element_located((By.ID, "archiveModal"))
        )

        self.selenium.find_element(By.ID, 'btn-js-archivage').click()
        WebDriverWait(self.selenium, 2).until(
            EC.presence_of_element_located((By.XPATH, "//*[@class='alert alert-success']"))
        )
        self.assertIn('1 dossiers ont été archivés avec succès.', self.selenium.page_source)
