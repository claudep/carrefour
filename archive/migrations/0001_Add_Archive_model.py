from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name='Archive',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=40)),
                ('unite', models.CharField(max_length=10)),
                ('date_debut', models.DateField(null=True)),
                ('date_fin', models.DateField()),
                ('key', models.TextField()),
                ('pdf', models.FileField(null=True, upload_to='archives/')),
            ],
            options={},
        ),
    ]
