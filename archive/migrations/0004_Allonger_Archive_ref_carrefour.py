# Generated by Django 3.2.7 on 2021-12-15 08:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('archive', '0003_Nouveaux_champs_Archive'),
    ]

    operations = [
        migrations.AlterField(
            model_name='archive',
            name='referent_carrefour',
            field=models.CharField(blank=True, max_length=50),
        ),
    ]
