from django.db import models


class Archive(models.Model):
    nom = models.CharField(max_length=40)
    unite = models.CharField(max_length=10)
    referent_carrefour = models.CharField(max_length=250, blank=True)
    referent_ope = models.CharField(max_length=50, blank=True)
    motif_fin = models.CharField(max_length=30, blank=True)
    date_debut = models.DateField(null=True)
    date_fin = models.DateField()
    key = models.TextField()
    pdf = models.FileField(upload_to='archives/', null=True)

    def __str__(self):
        return f"{self.unite}: {self.nom}"
