from django.urls import path

from archive import views

urlpatterns = [
    path('<str:unite>/<int:pk>/add/', views.ArchiveAddView.as_view(), name='archive-add'),
    path('<str:unite>/list/', views.ArchiveListView.as_view(), name='archive-list'),
    path('<int:pk>/export/', views.ArchiveExportView.as_view(), name='archive-export'),
]
