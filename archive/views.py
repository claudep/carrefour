import base64
from io import BytesIO

from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives import serialization
from cryptography.fernet import Fernet, InvalidToken

from django import forms
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from django.core.files.base import ContentFile
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, reverse
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.text import slugify
from django.views.generic import FormView, TemplateView, View

from aemo.models import FamilleAemo
from asaef.models import FamilleAsaef, Intervention
from batoude.models import Beneficiaire
from carrefour.forms import BootstrapMixin
from carrefour.utils import is_ajax
from carrefour.views import MSG_NO_ACCESS
from common.choices import MotifsFinSuivi

from .forms import ArchiveFilterForm
from .models import Archive
from .pdf import ArchiveAemo, ArchiveAsaef, ArchiveBatoude


class ArchiveExportCheckPermMixin:
    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm('archive.view_archive'):
            raise PermissionDenied(MSG_NO_ACCESS)
        return super().dispatch(request, *args, **kwargs)


class ArchiveListCheckPermMixin:
    def dispatch(self, request, *args, **kwargs):
        if (request.user.has_perm('aemo._manage_littoral') or
                request.user.has_perm('aemo.manage_montagnes') or
                request.user.has_perm('asaef.manage_asaef') or
                request.user.has_perm('batoude.manage_batoude') or
                request.user.has_perm('carrefour.export_aemo')):
            return super().dispatch(request, *args, **kwargs)
        raise PermissionDenied(MSG_NO_ACCESS)


class ArchiveKeyUploadForm(BootstrapMixin, forms.Form):
    file = forms.FileField(label='Clé de décryptage des archives', required=True)


class ArchiveAddView(PermissionRequiredMixin, View):
    """
    Create an archive (encrypted PDF + entry in Archive table) then anonymize the
    the family and mark it as 'archived'.
    """

    permission_required = 'carrefour.export_aemo'

    def post(self, request, *args, **kwargs):
        temp = BytesIO()
        unite = kwargs.get('unite', None)
        referent_carrefour = ''
        referent_ope = ''
        motif_fin = ''
        dict_motif_abandon = dict(MotifsFinSuivi.choices_abandon(unite))
        if unite == 'aemo':
            obj = get_object_or_404(FamilleAemo, pk=kwargs['pk'], archived_at__isnull=True)
            referent_carrefour = '/'.join([f"{ref.nom} {ref.prenom[0].upper()}." for ref in obj.suivi.suivi_referents])
            referent_ope = obj.suivi.ope_referent.nom_prenom if obj.suivi.ope_referent else ''
            motif_fin = obj.suivi.get_motif_fin_suivi_display() \
                if obj.suivi.motif_fin_suivi in dict_motif_abandon else ''
            pdf = ArchiveAemo(temp)
        elif unite == 'asaef':
            obj = get_object_or_404(FamilleAsaef, pk=kwargs['pk'], archived_at__isnull=True)
            referent_carrefour = ' / '.join(
                [
                    f'{inter.educ.sigle} {inter.date_debut}-{inter.date_fin}'
                    for inter in Intervention.objects.filter(suivi=obj.suivi)
                 ]
            )
            referent_ope = obj.suivi.get_ope_referents()
            pdf = ArchiveAsaef(temp)
        elif unite == 'batoude':
            obj = get_object_or_404(Beneficiaire, pk=kwargs['pk'], archived_at__isnull=True)
            referent_ope = obj.referent_ope.nom_prenom if obj.referent_ope else ''
            pdf = ArchiveBatoude(temp)
        else:
            raise NotImplementedError

        if not obj.can_be_deleted(self.request.user):
            raise PermissionDenied(MSG_NO_ACCESS)

        obj.archived_at = timezone.now()
        pdf.produce(obj)
        filename = f"{unite}/{pdf.get_filename(obj)}"
        temp.seek(0)
        pdf = temp.read()

        # Create a symmetric Fernet key to encrypt the PDF, and encrypt that key with asymmetric RSA key.
        with open(settings.CARREFOUR_RSA_PUBLIC_KEY, "rb") as key_file:
            public_key = serialization.load_ssh_public_key(key_file.read())
        padd = padding.OAEP(mgf=padding.MGF1(algorithm=hashes.SHA1()), algorithm=hashes.SHA1(), label=None)

        key = Fernet.generate_key()
        fernet = Fernet(key)
        pdf_crypted = fernet.encrypt(pdf)
        fernet_crypted = public_key.encrypt(key, padding=padd)

        arch = Archive.objects.create(
            nom=obj.nom,
            unite=unite,
            referent_carrefour=referent_carrefour,
            referent_ope=referent_ope,
            motif_fin=motif_fin,
            date_debut=obj.date_admission if unite == 'batoude' else obj.suivi.date_debut_suivi,
            date_fin=obj.date_sortie if unite == 'batoude' else obj.suivi.date_fin_suivi,
            key=base64.b64encode(fernet_crypted).decode(),
            pdf=None
        )
        arch.pdf.save(filename, ContentFile(pdf_crypted))

        obj.anonymiser()

        if is_ajax(request):
            return JsonResponse({'id': obj.pk}, safe=True)

        msg = f"Le dossier {obj} a été archivé."
        messages.success(request, msg)
        return HttpResponseRedirect(reverse(f"{unite}-suivis-termines"))


class ArchiveListView(ArchiveListCheckPermMixin, TemplateView):
    template_name = 'archive/list.html'
    model = Archive
    context_object_name = 'archives'

    def dispatch(self, request, *args, **kwargs):
        self.unite = self.kwargs['unite']
        self.filter_form = ArchiveFilterForm(data=request.GET or None)
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        if is_ajax(request) and self.unite == 'aemo':
            if self.filter_form.is_bound and self.filter_form.is_valid():
                archives = self.filter_form.filter(Archive.objects.filter(unite=self.unite).order_by('nom'))
            else:
                archives = Archive.objects.none()
            response = render_to_string(
                template_name='archive/list_partial.html',
                context={
                    'archives': archives,
                    'can_download': request.user.has_perm('archive.view_archive'),
                }
            )
            return JsonResponse(response, safe=False)
        return super().get(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        return {
            **super().get_context_data(*args, **kwargs),
            'unite': self.unite,
            'form': self.filter_form,
            'archives': Archive.objects.filter(unite=self.unite).order_by('nom'),
            'can_download': self.request.user.has_perm('archive.view_archive'),
        }


class ArchiveExportView(ArchiveExportCheckPermMixin, FormView):
    form_class = ArchiveKeyUploadForm
    template_name = 'archive/key_upload.html'

    def form_valid(self, form):
        arch = get_object_or_404(Archive, pk=self.kwargs['pk'])
        try:
            with open(arch.pdf.path, "rb") as fh:
                pdf_crypted = fh.read()
        except OSError as err:
            messages.error(self.request, f"Erreur lors de la lecture du document ({str(err)})")
            return HttpResponseRedirect(reverse('archive-list', args=[arch.unite]))

        try:
            private_key_file = self.request.FILES['file'].read()
            private_key = serialization.load_pem_private_key(private_key_file, password=None)
            padd = padding.OAEP(mgf=padding.MGF1(algorithm=hashes.SHA1()), algorithm=hashes.SHA1(), label=None)
            sim_key = private_key.decrypt(base64.b64decode(arch.key), padding=padd)

            fernet = Fernet(sim_key)
            pdf_content = fernet.decrypt(pdf_crypted)
        except ValueError as err:
            messages.error(self.request, f"Erreur lors de la lecture de la clé ({str(err)})")
            return HttpResponseRedirect(reverse('archive-list', args=[arch.unite]))
        except InvalidToken:
            messages.error(self.request, "Erreur lors du déchiffrement")
            return HttpResponseRedirect(reverse('archive-list', args=[arch.unite]))

        filename = f"{slugify(arch.nom)}.pdf"
        response = HttpResponse(pdf_content, content_type='application/pdf')
        response['Content-Disposition'] = "inline; filename=%s" % filename
        return response
