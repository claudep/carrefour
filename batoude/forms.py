from datetime import date
from tinymce.widgets import TinyMCE

from django import forms
from django.utils import timezone

from batoude.models import Beneficiaire, Bilan, Document, Medication, MembreFamille, PrestationBatoude
from carrefour.forms import (
    BootstrapMixin, BSCheckboxSelectMultiple, BSRadioSelect, PickDateWidget, PrestationFormBase
)
from carrefour.forms import CleanNpaLocaliteMixin
from carrefour.models import Contact, LibellePrestation, Utilisateur
from common.choices import MotifsFinSuivi


class BeneficiaireForm(BootstrapMixin, CleanNpaLocaliteMixin, forms.ModelForm):

    referent_ope = forms.ModelChoiceField(label='', queryset=Contact.membres_ope())
    referent_ope2 = forms.ModelChoiceField(label='', queryset=Contact.membres_ope(), required=False)

    class Meta:
        model = Beneficiaire
        fields = ('nom', 'prenom', 'naissance', 'genre', 'telephone', 'email',
                  'rue', 'npa', 'localite', 'rue_actuelle', 'npa_actuelle', 'localite_actuelle',
                  'referent_ope', 'referent_ope2', 'autorite_parentale', 'mandat_ope',
                  'region', 'date_demande')

        widgets = {'date_demande': PickDateWidget}


class MembreFamilleForm(BootstrapMixin, CleanNpaLocaliteMixin, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['role'].disabled = True

    class Meta:
        model = MembreFamille
        fields = ('role', 'nom', 'prenom', 'naissance', 'genre',
                  'rue', 'npa', 'localite', 'telephone', 'email',
                  'emploi', 'permis', 'validite', 'remarque')
        widgets = {
            'naissance': PickDateWidget,
        }


class AssuranceForm(BootstrapMixin, forms.ModelForm):

    class Meta:
        model = Beneficiaire
        fields = ('permis', 'validite', 'caisse_mal_acc', 'no_assure_lamal', 'assurance_accident',
                  'ass_maladie_compl', 'no_assure_compl', 'assurance_rc', 'no_avs')


class PrestationForm(PrestationFormBase):

    def __init__(self, *args, prest_code=None, user=None, **kwargs):
        super().__init__(*args, user=user, **kwargs)
        self.fields['lib_prestation'] = forms.ModelChoiceField(
            label='Prestations',
            queryset=LibellePrestation.objects.filter(unite='batoude', code__in=prest_code),
            widget=BSRadioSelect,
            required=True,
            empty_label=None,
        )
        self.fields['intervenants'] = forms.ModelMultipleChoiceField(
            label='Intervenants',
            queryset=Utilisateur.intervenants_batoude(),
            widget=BSCheckboxSelectMultiple,
            required=True
        )

    class Meta(PrestationFormBase.Meta):
        model = PrestationBatoude
        exclude = ('benef_actifs',)


class AgendaForm(BootstrapMixin, forms.ModelForm):

    class Meta:
        model = Beneficiaire
        fields = (
            'date_demande', 'date_entretien', 'date_validation', 'date_admission', 'date_appartement',
            'date_prolongation', 'date_sortie', 'motif_fin_suivi', 'destination', 'autoevaluation'
        )
        widgets = {
            **{field: PickDateWidget for field in fields if field.startswith('date_')},
        }

    def __init__(self, data=None, **kwargs):
        super().__init__(data=data, **kwargs)
        empty = [('', '--------')]
        has_suivi = self.instance.date_admission is not None and self.instance.date_sortie is None
        self.fields['motif_fin_suivi'].choices = empty + (
            MotifsFinSuivi.choices_fin() if has_suivi else MotifsFinSuivi.choices_abandon('batoude')
        )

    def clean(self):
        cleaned_data = self.cleaned_data
        motif = cleaned_data.get('motif_fin_suivi')
        date_sortie = cleaned_data.get('date_sortie', None)

        if motif and (motif in ['erreur', 'non_aboutie'] or motif.startswith('abandon_')) and date_sortie:
            raise forms.ValidationError("Vous ne pouvez plus modifier le dossier.")
        if motif and (motif in ['erreur', 'non_aboutie'] or motif.startswith('abandon_')) and not date_sortie:
            cleaned_data['date_sortie'] = date.today()
            return cleaned_data

        # Check date chronology
        date_preced = None
        for field_name in ['date_demande', 'date_entretien', 'date_validation', 'date_admission', 'date_sortie']:
            dt = cleaned_data.get(field_name)
            if not dt:
                continue
            if date_preced and dt < date_preced:
                raise forms.ValidationError(
                    "La date «{}» ne respecte pas l’ordre chronologique!".format(self.fields[field_name].label)
                )
            date_preced = dt

        # Check fin du dossier
        if cleaned_data['date_admission']:
            oblig = [date_sortie, motif, cleaned_data['destination']]
            if any(oblig) and not all(oblig):
                raise forms.ValidationError(
                    "Pour fermer un dossier, il faut une date de sortie, un motif de fin et une destination."
                )

        # Dates obligatoires
        date_admission = cleaned_data['date_admission']
        if not date_admission and not cleaned_data['date_demande']:
            raise forms.ValidationError(
                "La date de demande est manquante."
            )


class DocumentUploadForm(BootstrapMixin, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super().__init__(*args, **kwargs)

    class Meta:
        model = Document
        exclude = ('date_creation', 'auteur')
        widgets = {'person': forms.HiddenInput}
        labels = {'fichier': ''}

    def save(self, *args, **kwargs):
        if self.instance.pk is None:
            self.instance.auteur = self.user
            self.instance.date_creation = timezone.now()
        return super().save(*args, **kwargs)


class BilanForm(BootstrapMixin, forms.ModelForm):

    class Meta:
        model = Bilan
        fields = ('typ', 'categorie', 'date_bilan', 'present_batoude', 'present_ope',
                  'present_famille', 'present_autres', 'texte')
        widgets = {
            'texte': TinyMCE,
            'date_bilan': PickDateWidget,
            'typ': forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['categorie'].required = True


class JournalSearchForm(BootstrapMixin, forms.Form):

    search = forms.CharField(
        label="", max_length=20, required=False,
        widget=forms.TextInput(attrs={'placeholder': 'Recherche...', 'class': 'form-control-sm'})
    )

    def filter(self, journaux):
        if self.cleaned_data['search']:
            return journaux.filter(texte__unaccent__icontains=self.cleaned_data['search'])
        return journaux


class MedicFormsetForm(BootstrapMixin, forms.ModelForm):

    class Meta:
        model = Medication
        fields = ['medic', 'date_debut', 'date_fin']
        widgets = {
            'medic': forms.TextInput(attrs={'rows': '2'}),
            'date_debut': PickDateWidget,
            'date_fin': PickDateWidget
        }


class MedicationForm(BootstrapMixin, forms.ModelForm):

    class Meta:
        model = Beneficiaire
        fields = ('id',)

    def __init__(self, data=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        MedicFormset = forms.inlineformset_factory(
            Beneficiaire, Medication, form=MedicFormsetForm, formset=forms.BaseInlineFormSet
        )
        self.formset = MedicFormset(
            instance=self.instance, data=data, queryset=Medication.objects.filter(beneficiaire=self.instance),
            prefix='medic'
        )
        self.formset.extra = 0

    def is_valid(self):
        return all([super().is_valid(), self.formset.is_valid()])

    def has_changed(self):
        return any([super().has_changed(), self.formset.has_changed()])

    def save(self, commit=True):
        self.formset.save()
        return self.instance
