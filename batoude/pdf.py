from functools import partial

from django.utils.text import slugify

from reportlab.lib import colors
from reportlab.lib.enums import TA_LEFT
from reportlab.lib.units import cm
from reportlab.platypus import (
    KeepTogether, Paragraph, Spacer, Table, TableStyle,
)

from carrefour.pdf import BaseCarrefourPDF
from carrefour.utils import format_d_m_Y


class BeneficiaireCoordinatesPdf(BaseCarrefourPDF):
    title = "Coordonnées"

    def get_filename(self, person):
        return '{}_Coordonnees.pdf'.format(slugify(person.nom))

    def produce(self, person, **kwargs):
        self.leftMargin = 1.5 * cm
        self.story.append(Spacer(0, 1 * cm))
        self.story.append(Paragraph('Coordonnées', self.style_title))
        self.story.append(Spacer(0, 0.5 * cm))

        # Cadre supérieur
        data = [
            ["Nom", person.nom],
            ["Prénom", person.prenom],
            ["Date de naissance", format_d_m_Y(person.naissance)],
            ["Téléphone", person.telephone],
            ["Courriel", person.email],
            ["Lieu de vie officiel", '{}, {} {}'.format(person.rue, person.npa, person.localite)],
            ["Lieu de vie actuel", '{}, {} {}'.format(
                person.rue_actuelle, person.npa_actuelle, person.localite_actuelle
            )],
            ['Autorité parentale', person.get_autorite_parentale_display()],
        ]
        t = Table(
            data=data, colWidths=[5 * cm, 13 * cm], hAlign=TA_LEFT, rowHeights=0.6 * cm,
            spaceBefore=0 * cm, spaceAfter=0.5 * cm
        )
        t.setStyle(tblstyle=TableStyle([
            ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
            ('FONTSIZE', (0, 0), (-1, -1), self.base_font_size),
            ('FONTNAME', (0, 0), (-1, 1), f'{self.base_font_name}-Bold'),
            ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
        ]))
        self.story.append(t)

        # Parents
        mere = person.membres_famille.filter(role='mere').first()
        pere = person.membres_famille.filter(role='pere').first()
        data_family = (
            ("Nom", "nom"), ("Prénom", "prenom"), ("Adresse", "rue"),
            ("Localité", 'localite'), ("Emploi actuel", 'emploi'), ("Tél.", 'telephone'),
            ("Courriel", 'email'), ('Remarque', 'remarque'),
        )
        data = [[label, getattr(mere, field) if mere else ''] for label, field in data_family]
        self.story.append(Paragraph('Mère', self.style_sub_title))
        self.story.append(self.get_table(data, columns=[5, 13], before=0, after=0.4))

        data = [[label, getattr(pere, field) if pere else ''] for label, field in data_family]
        self.story.append(Paragraph('Père', self.style_sub_title))
        self.story.append(self.get_table(data, columns=[5, 13], before=0, after=0.4))

        # Situation matrimoniale
        data = [
            ['Situation matrimoniale: {}'.format(person.get_statut_marital_display()),
             'Autorité parentale: {}'.format(person.get_autorite_parentale_display())],
        ]
        self.story.append(self.get_table(data, columns=[5, 13], before=0.5, after=0.5))

        # Assurances
        self.story.append(Paragraph('Assurances', self.style_sub_title))
        data = [
            ["Assur.-maladie LAMal", person.caisse_mal_acc],
            ["Avec accident", {True: 'OUI', False: 'NON', None: '?'}[person.assurance_accident]],
            ["No assuré LAMAL", person.no_assure_lamal],
            ["Assur.-maladie compl.", person.ass_maladie_compl],
            ["No assur. compl.", person.no_assure_compl],
            ["Assur. RC", person.assurance_rc],
            ['No AVS', person.no_avs],
        ]
        self.story.append(self.get_table(data, columns=[5, 13], before=0, after=0.5))

        # Réseau
        data = [
            ["AS OPE et mandat",
             Paragraph(
                 '{} - {}'.format(
                     person.get_referents_ope(),
                     person.get_mandat_ope_display()
                 ),
                 self.style_normal
             )],
        ]
        for contact in person.reseau.all():
            role = []
            if contact.role:
                role.append(contact.role.nom)
            if contact.profession:
                role.append(contact.profession)
            data.append([
                Paragraph('/'.join(role), self.style_normal),
                Paragraph(contact.nom_prenom, self.style_normal)
            ])
        self.story.append(KeepTogether([
            Paragraph('Réseau', self.style_sub_title),
            self.get_table(data, columns=[5, 13], before=0, after=0.5, inter=0.8)
        ]))
        super().produce()


class JournalPdf(BaseCarrefourPDF):
    title = "Journal de bord"

    def get_filename(self, beneficiaire):
        return '{}_journal.pdf'.format(slugify(beneficiaire.nom))

    def produce(self, beneficiaire, **kwargs):
        self.set_title(
            '{} {} - {}'.format(beneficiaire.prenom, beneficiaire.nom, self.title)
        )
        self.style_italic.spaceAfter = 0.1 * cm

        for page in beneficiaire.prestations_batoude.all():
            self.story.append(Paragraph(page.participants, self.style_sub_title))
            self.story.append(Paragraph(page.texte.replace('\n', '<br />\n'), self.style_normal))
            self.story.append(
                Paragraph('{} - {}'.format(page.auteur.sigle, format_d_m_Y(page.date_prestation)), self.style_italic)
            )
        return super().produce()


class BilanPdf(BaseCarrefourPDF):
    title = ''

    def produce(self, bilan, **kwargs):
        P = partial(Paragraph, style=self.style_normal)

        beneficiaire = bilan.beneficiaire
        title = bilan.get_typ_display()
        self.set_title(f"Bénéficiaire: {beneficiaire.nom_prenom()} - {title}")
        data = [
            ['Date du bilan:', format_d_m_Y(bilan.date_bilan)],
            ['Personnes présentes au bilan: ', ''],
            ['Interv. Batoude:', P(bilan.present_batoude)],
            ["Interv. OPE:", beneficiaire.get_referents_ope()],
            ['Membres de la famille:', P(bilan.present_famille)],
            ['Autres:', P(bilan.present_autres)],
            ["Date d'admission:", format_d_m_Y(beneficiaire.date_admission)],
        ]
        self.story.append(self.get_table(data, columns=[6, 12], before=0.0, after=0.5, bordered=True))
        self.story.append(Paragraph("Bilan:"))
        self.story.append(P(bilan.texte))
        return super().produce()

    def get_filename(self, bilan):
        return f"{slugify(bilan.beneficiaire.nom_prenom())}_bilan_{bilan.date_bilan}.pdf"
