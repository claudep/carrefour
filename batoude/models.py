import calendar
import os
from datetime import date, timedelta
from django.db import models
from django.db.models import Q, Sum, F, Count
from django.db.models.functions import Coalesce
from django.utils import timezone

from aemo.models import Etape as AemoEtape
from carrefour.models import Contact, PrestationBase, Utilisateur
from carrefour.utils import delay_for_destruction, format_duree, format_d_m_Y, random_string_generator
from common import choices
from common.fields import ChoiceArrayField


class Etape(AemoEtape):
    """
        1. Numéro d'ordre pour tri dans la vue
        2. Code
        3. Affichage dans tableau
        4. Nom visible de l'étape
        5. Code de l'étape suivante
        6. Délai jusqu'à l'échéance (en jours)
        7. Code de l'étape précédente
        8. Date obligatoire précédente (code ou '')
        9. Bool.
    """
    def __init__(self, num, code, abrev, nom, suivante, delai, precedente, preced_oblig, oblig, actif=True):
        super().__init__(num, code, abrev, nom, suivante, delai, precedente, preced_oblig, oblig)
        self.actif = actif


class PersonneBase(models.Model):
    nom = models.CharField('Nom', max_length=40)
    prenom = models.CharField('Prénom', max_length=40)
    naissance = models.DateField('Date de naissance', null=True, blank=True, default=None)
    telephone = models.CharField('Téléphone', max_length=40, blank=True)
    email = models.EmailField('Courriel', blank=True)
    genre = models.CharField(
        'Genre', max_length=1, choices=(('M', 'M'), ('F', 'F'), ('X', 'Non binaire')), blank=True
    )
    rue = models.CharField('Rue', max_length=30, blank=True)
    npa = models.CharField('NPA', max_length=4, blank=True)
    localite = models.CharField('Localité', max_length=30, blank=True)
    permis = models.CharField('Nationalité, Permis/séjour', max_length=30, blank=True)
    validite = models.DateField('Date validité', blank=True, null=True)
    emploi = models.CharField('Emploi actuel', max_length=40, blank=True)
    remarque = models.CharField('Remarque', max_length=200, blank=True)

    class Meta:
        abstract = True

    def nom_prenom(self):
        return f'{self.nom} {self.prenom}'

    def adresse_officielle(self):
        sep = ', ' if self.rue else ''
        return f'{self.rue}{sep}{self.npa} {self.localite}'.strip()

    def __str__(self):
        return self.nom_prenom()


class Beneficiaire(PersonneBase):
    WORKFLOW = {
        'demande': Etape(
            1, 'demande', 'dem', 'Demande', 'entretien', 30, 'demande', 'demande', True
        ),
        'entretien': Etape(
            2, 'entretien', 'ent', 'Entretien', 'admission', 30, 'demande', 'demande', True
        ),
        'admission': Etape(
            3, 'admission', 'adm', 'Admission', 'appartement', 90, 'entretien', 'entretien', True
        ),
        'appartement': Etape(
            4, 'appartement', 'app', 'Appartement', '6mois', 00, 'admission', 'admission', True
        ),
        '6mois': Etape(
            5, '6mois', '6m', '6 mois', '12mois', 180, 'appartement', 'appartement', True, False
        ),
        '12mois': Etape(
            6, '12mois', '12m', '12 mois', '18mois', 180, '6mois', '6mois', True, False
        ),
        '18mois': Etape(
            7, '18mois', '18m', '18 mois', '24mois', 180, '12mois', '12mois', True, False
        ),
        '24mois': Etape(
            8, '24mois', '24m', '24 mois', '30mois', 180, '12mois', '12mois', True, False
        ),
        '30mois': Etape(
            9, '30mois', '30m', '30 mois', 'prolongation', 180, '24mois', '24mois', True, False
        ),
        '36mois': Etape(
            10, '36mois', 'pro', 'Prolongation', 'sortie', 180,  '30mois', '30mois', True, False
        ),
        'sortie': Etape(
            11, 'sortie', 'sor', 'Sortie', 'archivage', 30, 'prolongation', 'admission', True
        ),
        'archivage': Etape(
            12, 'archivage', 'arc', 'Archivage', None, 300, 'sortie', 'sortie', True, False
        ),
    }

    rue_actuelle = models.CharField('Rue act.', max_length=30, blank=True)
    npa_actuelle = models.CharField('NPA act.', max_length=4, blank=True)
    localite_actuelle = models.CharField('Localité act.', max_length=30, blank=True)
    region = models.CharField('Région', max_length=12, choices=choices.REGIONS_CHOICES, blank=True)

    caisse_mal_acc = models.CharField('Caisse maladie-acc.', max_length=50, blank=True)
    no_assure_lamal = models.CharField("No d'assuré", max_length=30, blank=True)
    assurance_rc = models.CharField('Assurance RC', max_length=50, blank=True)
    no_avs = models.CharField("N° AVS", max_length=16, blank=True)
    assurance_accident = models.BooleanField('Assurance avec accident', blank=True, null=True, default=None)
    no_assure_compl = models.CharField("No d'assuré compl.", max_length=30, blank=True)
    ass_maladie_compl = models.CharField('Ass.-maladie compl.', max_length=50, blank=True)
    loisirs = models.TextField('Loisirs/passions', blank=True)
    scolarite = models.TextField('Situation scolaire', blank=True)
    autorite_parentale = models.CharField(
        "Autorité parentale", max_length=20, choices=choices.AUTORITE_PARENTALE_CHOICES, blank=True
    )
    statut_marital = models.CharField("Statut marital", max_length=20, choices=choices.STATUT_MARITAL_CHOICES,
                                      blank=True)
    statut_financier = models.CharField(
        'Statut financier', max_length=20, choices=choices.STATUT_FINANCIER_CHOICES, blank=True
    )
    monoparentale = models.BooleanField('Famille monoparent.', null=True, blank=True)
    reseau = models.ManyToManyField(Contact, blank=True)
    referent_ope = models.ForeignKey(
        Contact, blank=True, null=True, on_delete=models.PROTECT, related_name="suivisbatoude",
    )
    referent_ope2 = models.ForeignKey(
        Contact, blank=True, null=True, on_delete=models.PROTECT, related_name="suivisbatoude2",
    )
    mandat_ope = ChoiceArrayField(
        models.CharField(max_length=65, choices=choices.MANDATS_OPE_CHOICES, blank=True),
        verbose_name="Mandat OPE", blank=True, null=True,
    )

    provenance = models.CharField(
        'Provenance av. admission', max_length=30, choices=choices.PROVENANCE_DESTINATION_CHOICES, blank=True
    )
    destination = models.CharField(
        'Destination après suivi', max_length=30, choices=choices.DESTINATION_BATOUDE_CHOICES, blank=True
    )
    service_orienteur = models.CharField(
        "Orienté vers la Batoude par", max_length=15, choices=choices.SERVICE_ORIENTEUR_ASAEF_CHOICES, blank=True
    )
    autoevaluation = models.SmallIntegerField("Auto-évaluation", blank=True, null=True, choices=choices.SCORE_CHOICES)
    motif_fin_suivi = models.CharField(
        'Motif de fin de suivi', max_length=20, choices=choices.MotifsFinSuivi.choices, blank=True
    )
    date_demande = models.DateField("Date de la demande", null=True)
    date_entretien = models.DateField("Date entretien d'info.", null=True, blank=True, default=None)
    date_validation = models.DateField("Date demande validée", null=True, blank=True, default=None)
    date_admission = models.DateField("Date d'admission", null=True, blank=True, default=None)
    date_appartement = models.DateField("Date entrée appartement", null=True, blank=True, default=None)
    date_prolongation = models.DateField("Date prolongation", null=True, blank=True, default=None)
    date_sortie = models.DateField("Date de sortie", null=True, blank=True, default=None)
    date_facturation = models.DateField('Début de la facturation', null=True, blank=True)
    archived_at = models.DateTimeField('Archivée le', blank=True, null=True)

    class Meta:
        verbose_name = 'Bénéficiaire BATOUDE'
        verbose_name_plural = 'Bénéficiaires BATOUDE'
        ordering = ('nom', 'prenom')
        permissions = (
            ('view_batoude', 'Consulter les dossiers BATOUDE'),
            ('manage_batoude', 'RE BATOUDE'),
            ('change_batoude', 'Gérer les dossiers Batoude'),
        )

    def get_mandat_ope_display(self):
        return ", ".join(
            [dict(choices.MANDATS_OPE_CHOICES)[v] for v in self.mandat_ope]
        ) if self.mandat_ope else ''

    def mere(self):
        return self.membres_famille.filter(role='mere').first()

    def pere(self):
        return self.membres_famille.filter(role='pere').first()

    def can_edit(self, user):
        return user.has_perm('batoude.change_batoude') and self.date_sortie is None

    def prestations_historiques(self):
        return PrestationBase.prestations_historiques(self.prestations_batoude.all())

    def jours_factures_par_mois(self, mydate):
        nbre_jours_absence = self.absences.filter(
            date_debut__year=mydate.year, date_debut__month=mydate.month
        ).aggregate(total=Sum('nbre_jour'))['total'] or 0
        return calendar.monthrange(mydate.year, mydate.month)[1] - nbre_jours_absence

    def prestations_du_mois(self):
        """
        Retourne le détail des prestations pour ce bénéficiaire pour le mois courant
        """
        date_ref = date(date.today().year, date.today().month, 1)
        return self.prestations_batoude.filter(date_prestation__gte=date_ref)

    def temps_total_prestations(self, interv=None):
        """
        Temps total des prestations liées à ce bénéficiare, quel que soit le nombre
        de membres suivis.
        Filtré facultativement par intervenant.
        """
        prest = self.prestations_batoude if interv is None else self.prestations_batoude.filter(intervenant=interv)
        return prest.annotate(
            nbre_inter=Count('intervenants')
        ).aggregate(
            total=Sum(F('duree') * F('nbre_inter'), output_field=models.DurationField())
        )['total'] or timedelta()

    @classmethod
    def benef_actifs(cls, mydate):
        annee = mydate.year
        mois = mydate.month
        debut_mois = date(annee, mois, 1)
        fin_mois = date(annee, mois, calendar.monthrange(annee, mois)[1])
        """
        Les bénéficiaires actifs à la date mydate sont ceux dont la date d'admission est antérieure et ceux admis
        durant tout le mois concerné.
        Cf répartition des heures de prestations générales entre les bénéficiaires actifs 
        """
        return Beneficiaire.objects.filter(
            date_admission__lte=fin_mois).filter(
            Q(date_sortie__isnull=True) | Q(date_sortie__gte=debut_mois)
        )

    @classmethod
    def nouvelles_demandes(cls, debut, fin):
        return cls.objects.filter(
            date_demande__lte=fin, date_demande__gte=debut,
        ).filter(
            Q(date_sortie__isnull=True) | Q(date_sortie__gte=debut)
        ).annotate(
            date_debut=F('date_demande'),
            date_fin=F('date_demande'),
        ).exclude(
            motif_fin_suivi='erreur'
        )

    @classmethod
    def suivis_en_cours(cls, debut, fin):
        return cls.objects.filter(
            date_admission__isnull=False,
            date_admission__lte=fin).filter(
            Q(date_sortie__isnull=True) | Q(date_sortie__gte=debut)
        ).annotate(
            date_debut=F('date_admission'),
            date_fin=Coalesce('date_sortie', fin),
        ).exclude(
            motif_fin_suivi='erreur'
        )

    @classmethod
    def suivis_nouveaux(cls, debut, fin):
        return cls.objects.filter(
            date_admission__lte=fin, date_admission__gte=debut,
        ).filter(
            Q(date_sortie__isnull=True) | Q(date_sortie__gte=debut)
        ).annotate(
            date_debut=F('date_admission'),
            date_fin=F('date_admission'),
        ).exclude(
            motif_fin_suivi='erreur'
        )

    @classmethod
    def suivis_termines(cls, debut, fin):
        return cls.objects.filter(
            date_sortie__lte=fin, date_sortie__gte=debut
        ).annotate(
            date_debut=F('date_sortie'),
            date_fin=F('date_sortie'),
        ).exclude(
            motif_fin_suivi='erreur'
        )

    @classmethod
    def check_perm_view(cls, user):
        return user.has_perm('batoude.view_batoude') or user.has_perm('batoude.manage_batoude')

    def can_be_deleted(self, user):
        if not user.has_perm('carrefour.export_aemo'):
            return False
        return delay_for_destruction(self.date_sortie)

    def get_referents_ope(self, tel=False):
        if not tel:
            return ' / '.join([str(ope) for ope in [self.referent_ope, self.referent_ope2] if ope])
        return ' / '.join(
            [f"{str(ope)} ({ope.telephone})" for ope in [self.referent_ope, self.referent_ope2] if ope])

    def anonymiser(self):
        new_data = {
            'nom': random_string_generator(),
            'prenom': '',
            'naissance': self.naissance,
            'telephone': '',
            'email': '',
            'genre': self.genre,
            'rue': '',
            'npa': self.npa,
            'localite': self.localite,
            'permis': '',
            'validite': None,
            'emploi': '',
            'remarque': '',
            'rue_actuelle': '',
            'npa_actuelle': '',
            'localite_actuelle': '',
            'caisse_mal_acc': '',
            'no_assure_lamal': '',
            'assurance_rc': '',
            'no_avs': '',
            'assurance_accident': None,
            'no_assure_compl': '',
            'ass_maladie_compl': '',
            'loisirs': '',
            'scolarite': '',
            'autorite_parentale': self.autorite_parentale,
            'statut_marital': self.statut_marital,
            'statut_financier': self.statut_financier,
            'monoparentale': self.monoparentale,
            # 'reseau': pass,
            'referent_ope': None,
            'referent_ope2': None,
            'mandat_ope': self. mandat_ope,
            'provenance': self.provenance,
            'destination': self.destination,
            'date_admission': self.date_admission,
            'date_sortie': self.date_sortie,
            'date_facturation': self.date_facturation,
            'archived_at': timezone.now()
        }
        [setattr(self, field, value) for field, value in new_data.items()]
        self.save()
        self.reseau.clear()
        self.membres_famille.all().delete()
        for doc in self.documents.all():
            doc.delete()
        self.absences.all().delete()
        self.taches.all().delete()

    @property
    def etape(self):
        for key, etape in reversed(self.WORKFLOW.items()):
            if etape.actif:
                if etape.date(self):
                    return etape

    def etape_suivante(self):
        etape = self.etape
        return self.WORKFLOW[etape.suivante] if etape else None

    def date_suivante(self):
        return self.etape.date_suivante(self)


class MembreFamille(PersonneBase):
    ROLE_CHOICES = (
        ('pere', 'père'),
        ('mere', 'mère'),
        ('fratrie', 'fratrie'),
        ('autre', 'autre'),
    )
    beneficiaire = models.ForeignKey(Beneficiaire, on_delete=models.PROTECT, related_name='membres_famille')
    role = models.CharField('Degré parenté', max_length=40, choices=ROLE_CHOICES)
    activite = models.TextField('Activité', blank=True)

    class Meta(PersonneBase.Meta):
        verbose_name = 'Membre famille BATOUDE'
        verbose_name_plural = 'Membres familles BATOUDE'


class PrestationBatoude(PrestationBase):
    beneficiaire = models.ForeignKey(Beneficiaire, related_name='prestations_batoude', null=True, blank=True,
                                     on_delete=models.SET_NULL, verbose_name="Bénéficiaire")
    auteur = models.ForeignKey(Utilisateur, null=True, on_delete=models.PROTECT)
    benef_actifs = models.PositiveSmallIntegerField(default=0)
    participants = models.CharField('Participants', max_length=100)
    texte = models.TextField('Contenu', blank=True)
    intervenants = models.ManyToManyField(Utilisateur, related_name='prestations_batoude')

    class Meta(PrestationBase.Meta):
        verbose_name = 'Prestation BATOUDE'
        verbose_name_plural = 'Prestations BATOUDE'

    def __str__(self):
        if self.beneficiaire:
            return f'Prestation pour {self.beneficiaire} le {self.date_prestation} : {self.duree}'
        return f'Prestation générale le {self.date_prestation} : {self.duree}'

    def save(self, *args, **kwargs):
        self.benef_actifs = Beneficiaire.benef_actifs(self.date_prestation).count()
        super().save(*args, **kwargs)

    def can_edit(self, user):
        return user == self.auteur and self.check_date_editable(self.date_prestation, user)

    @classmethod
    def temps_total_mensuel_prest_export(cls, mydate, typ=''):
        if typ not in ['gen', 'benef']:
            raise ValueError('Paramètre incorrect')
        is_gen = typ == 'gen'
        return PrestationBatoude.objects.filter(
            beneficiaire__isnull=is_gen, date_prestation__year=mydate.year, date_prestation__month=mydate.month
        ).annotate(
            num_intervenants=Count('intervenants'),
        ).aggregate(
            total=Sum(F('duree') * F('num_intervenants') / F('benef_actifs'), output_field=models.DurationField())
        )['total'] or timedelta()

    @classmethod
    def temps_total_prest_gen(cls):
        """
        Renvoie le temps total des prestations GÉNÉRALES pour l'année civile en cours.
        """
        year = date.today().year
        tot = cls.objects.filter(
            beneficiaire__isnull=True,
            date_prestation__year=year
        ).annotate(
            nbre_inter=Count('intervenants')
        ).aggregate(
            total=Sum(F('duree') * F('nbre_inter'), output_field=models.DurationField())
        )['total'] or timedelta()
        return format_duree(tot)


class Absence(models.Model):
    beneficiaire = models.ForeignKey(Beneficiaire, on_delete=models.CASCADE, related_name='absences')
    date_debut = models.DateField('Date début', max_length=20)
    nbre_jour = models.PositiveSmallIntegerField("Jour(s) d'absence", default=0)
    motif = models.TextField(verbose_name='Motif')

    class Meta:
        verbose_name = "Absence"
        verbose_name_plural = "Absences"
        ordering = ('-date_debut',)

    def __str__(self):
        return 'Absence de {} du {} au {}'.format(
            self.beneficiaire.nom_prenom(),
            format_d_m_Y(self.date_debut),
            format_d_m_Y(self.date_debut + timedelta(days=self.nbre_jour))
        )


class Tache(models.Model):
    beneficiaire = models.ForeignKey(Beneficiaire, on_delete=models.CASCADE, related_name='taches')
    date = models.DateTimeField('Date/heures', default=None)
    texte = models.CharField('Activité', max_length=250)
    resp = models.ForeignKey(Utilisateur, related_name='taches_resp', on_delete=models.SET_NULL, null=True, blank=True)
    delai = models.DateField('Délai', null=True, blank=True)
    auteur = models.ForeignKey(Utilisateur, related_name='taches_auteur', on_delete=models.PROTECT)
    realise = models.DateField('Réalisé le', null=True, blank=True)

    class Meta:
        verbose_name = 'Tâche'

    def __str__(self):
        return '{} - {}'.format(format_d_m_Y(self.date), self.texte)


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/doc/batoude/user_<id>/<filename>
    return 'doc/batoude/user_{0}/{1}'.format(instance.person.id, filename)


class Document(models.Model):
    CATEGORIE_CHOICES = (
        ('communication', 'Communication OPE-Batoude'),
        ('formation', 'Scolarité - Formation prof.'),
        ('appartement', 'Appartement'),
        ('assurance', 'Assurances'),
        ('doc_perso', 'Documents personnels'),
        ('projet', 'Projet de vie'),
        ('finance', 'Finances'),
        ('sante', 'Santé'),
    )
    person = models.ForeignKey(Beneficiaire, related_name='documents', on_delete=models.CASCADE)
    date_creation = models.DateTimeField()
    auteur = models.ForeignKey(Utilisateur, related_name='documents_batoude', on_delete=models.PROTECT)
    fichier = models.FileField("Fichier", upload_to=user_directory_path)
    titre = models.CharField(max_length=100)
    categorie = models.CharField("Dossier", max_length=25, choices=CATEGORIE_CHOICES)

    class Meta:
        models.UniqueConstraint(fields=['person', 'categorie', 'titre'], name='unique_person_titre')

    def __str__(self):
        return self.titre

    def delete(self):
        try:
            os.remove(self.fichier.path)
        except OSError:
            pass
        return super().delete()

    def can_edit(self, user):
        return self.person.can_edit(user)

    def can_be_deleted(self, user):
        return user == self.auteur


class Bilan(models.Model):
    BILAN_CHOICES = (
        ('demande', "Bilan de la demande"),
        ('admission', "Bilan d’admission"),
        ('autonomisation', "Bilan de l'autonomisation"),
        ('prolongation', "Bilan de la prolongation"),
        ('final', "Bilan final")
    )
    typ = models.CharField("Type de bilan", max_length=20, choices=BILAN_CHOICES)
    beneficiaire = models.ForeignKey(
        to=Beneficiaire, related_name='bilans', on_delete=models.CASCADE, verbose_name="Bénéficiaire")
    date_bilan = models.DateField("Date du bilan")
    present_batoude = models.CharField("Interv. Batoude", max_length=250, blank=True)
    present_ope = models.CharField("Interv. OPE", max_length=250, blank=True)
    present_famille = models.CharField("Membres de la famille", max_length=250, blank=True)
    present_autres = models.CharField("Autres pers. présentes", max_length=250, blank=True)

    texte = models.TextField("Bilan", blank=True)
    categorie = models.CharField(
        'Catégorie',
        max_length=15,
        choices=(
            ('education', 'Éducation'), ('famille', 'Famille'), ('atelier', 'Atelier'), ('sante', 'Santé'),
            ('autre', 'Autre')
        ),
        blank=True
    )

    def __str__(self):
        return f"Bilan pour {self.beneficiaire.nom_prenom()} du {self.date_bilan}"


class Medication(models.Model):
    beneficiaire = models.ForeignKey(
        to=Beneficiaire, related_name='medications', on_delete=models.CASCADE, verbose_name="Bénéficiaire")
    medic = models.TextField('Médication')
    date_debut = models.DateField("Début", blank=True, null=True)
    date_fin = models.DateField("Fin", blank=True, null=True)

    def __str__(self):
        return self.medic[:30]

