import os
from dateutil.relativedelta import relativedelta
from datetime import date, timedelta
from pathlib import Path
from unittest.mock import patch

from django.conf import settings
from django.contrib.auth.models import Permission, Group
from django.core.files import File
from django.test import TestCase, tag
from django.utils import timezone
from django.urls import reverse

from freezegun import freeze_time

from batoude.forms import AgendaForm, BeneficiaireForm, BilanForm, PrestationForm
from batoude.models import Absence, Beneficiaire, Bilan, Document, Medication, MembreFamille, PrestationBatoude, Tache
from batoude.views import FAM_CODE
from carrefour.export import openxml_contenttype
from carrefour.models import Contact, Role, Service, Utilisateur, LibellePrestation
from carrefour.tests import InitialDataMixin, TestExporter
from carrefour.utils import format_duree, format_d_m_Y
from common.test_utils import TempMediaRootMixin


class BatoudeDataMixin(InitialDataMixin):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.group_batoude = Group.objects.get(name='batoude')
        cls.user_batoude = Utilisateur.objects.create_user('batman', 'batman@example.org', sigle='usb')
        cls.user_batoude.user_permissions.add(Permission.objects.get(codename='view_batoude'))
        cls.user_batoude.user_permissions.add(Permission.objects.get(codename='change_batoude'))
        cls.user_batoude.groups.add(cls.group_batoude)

        cls.manager_batoude = Utilisateur.objects.create_user('Manager', 'manager@example.org')
        cls.manager_batoude.user_permissions.add(Permission.objects.get(codename='manage_batoude'))
        cls.manager_batoude.groups.add(cls.group_batoude)

        cls.anonymous = Utilisateur.objects.create_user('Anonymous', 'anon@ymous.com')
        cls.person = Beneficiaire.objects.create(
            nom='Robespierre',
            prenom='Archibald',
            genre='M',
            mandat_ope=['curatelle2'],
            referent_ope=Contact.objects.get(nom='Rathfeld'),
            date_admission="2019-10-01"
        )


class BeneficiaireTest(BatoudeDataMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.client.force_login(self.user_batoude)

    def test_adresse_officielle(self):
        self.assertEqual(
            Beneficiaire(rue='Av. du Bonheur 4', npa='7777', localite='Paradis').adresse_officielle(),
            'Av. du Bonheur 4, 7777 Paradis'
        )
        self.assertEqual(
            Beneficiaire(npa='7777', localite='Paradis').adresse_officielle(),
            '7777 Paradis'
        )
        self.assertEqual(
            Beneficiaire().adresse_officielle(),
            ''
        )

    def test_create_beneficiaire(self):
        response = self.client.post(reverse('batoude-person-add'), data={
            'nom': 'Batoudon', 'prenom': 'Marc-André', 'genre': 'X',
            'referent_ope': self.person.referent_ope.pk, 'date_demande': date.today(),
            'region': 'mont_locle'
        })
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, reverse('batoude-person-list'))
        self.assertTrue(Beneficiaire.objects.filter(nom='Batoudon').exists())
        response = self.client.get(reverse('batoude-person-list'))
        self.assertContains(response, 'Batoudon Marc-André')
        # Référent OPE
        self.assertContains(response, 'Rathfeld Christophe')
        person = Beneficiaire.objects.get(nom='Batoudon')
        self.assertEqual(person.service_orienteur, 'OPEN')
        self.assertEqual(person.get_genre_display(), 'Non binaire')

    def test_create_beneficiaire_avec_ope_obligatoire(self):
        form = BeneficiaireForm(
            data={'nom': 'Doe', 'prenom': 'John', 'genre': 'M', 'date_demande': date.today(), 'region': 'mont_locle'}
        )
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['referent_ope'], ["Ce champ est obligatoire."])

    def test_champs_obligatoires(self):
        form = BeneficiaireForm()
        obligs = ['nom', 'prenom', 'date_demande', 'referent_ope']
        for field in obligs:
            self.assertTrue(form.fields[field].required)

    def test_edit_beneficiaire(self):
        response = self.client.post(
            reverse('batoude-person-edit', args=[self.person.pk]),
            data={
                'nom': 'Batoudon', 'prenom': 'Marc-André', 'genre': 'M', 'rue': 'Rue du Parc 12',
                'date_demande': date.today(), 'referent_ope': self.person.referent_ope.pk, 'region': 'mont_locle'
            }
        )
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, reverse('batoude-person-list'))
        self.person.refresh_from_db()
        self.assertEqual(self.person.rue, 'Rue du Parc 12')

    def test_non_acces_dossiers_fermes(self):
        self.person.date_sortie = "2023-12-31"
        self.person.save()

        but_save = '<button class="btn btn-sm btn-success" name="save" type="submit">Enregistrer</button>'
        urls = ['batoude-person-edit', 'batoude-assurance-edit', 'batoude-medication-edit',
                'batoude-person-agenda']
        for url in urls:
            response = self.client.get(reverse(url, args=[self.person.pk]))
            self.assertNotContains(response, but_save)

    def test_assurance_form(self):
        assurance_data = {
            'beneficiaire': self.person.pk, 'caisse_mal_acc': 'Assura', 'no_assure_lamal': '111222333',
            'assurance_rc': 'la Bâloise', 'no_avs': '444555666777',
        }
        response = self.client.post(
            reverse('batoude-assurance-edit', args=[self.person.pk]),
            data=assurance_data
        )
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, reverse('batoude-person-edit', args=[self.person.pk]))
        self.person.refresh_from_db()
        self.assertEqual(self.person.no_assure_lamal, '111222333')

    def test_affichage_des_contacts_dans_reseau(self):
        new_contact = Contact.objects.create(
            nom="Dupont",
            prenom="Paul",
            role=Role.objects.get(nom='Médecin')
        )
        self.person.reseau.add(new_contact)
        url = reverse('batoude-contact-list', args=(self.person.pk,))
        response = self.client.get(url)
        self.assertEqual(response.context['person'].reseau.count(), 1)
        self.assertEqual(response.context['person'].reseau.first().nom, 'Dupont')

    def test_ajouter_contact_dans_reseau(self):
        new_contact = Contact.objects.create(
            nom="Dupont",
            prenom="Paul",
            role=Role.objects.get(nom='Médecin')
        )
        response = self.client.post(
            reverse('batoude-contact-add', args=[self.person.pk]),
            data={'reseaux': [new_contact.pk]}
        )
        self.assertRedirects(response, reverse('batoude-contact-list', args=[self.person.pk]))
        self.person.refresh_from_db()
        self.assertEqual(self.person.reseau.count(), 1)

    def test_retirer_contact_du_reseau(self):
        new_contact = Contact.objects.create(
            nom="Dupont",
            prenom="Paul",
            role=Role.objects.get(nom='Médecin')
        )
        self.person.reseau.add(new_contact)
        self.assertEqual(self.person.reseau.count(), 1)
        response = self.client.post(
            reverse('batoude-contact-remove', args=[self.person.pk, new_contact.pk]),
            data={})
        self.assertEqual(response.status_code, 302)
        self.person.refresh_from_db()
        self.assertEqual(self.person.reseau.count(), 0)

    def test_ajouter_membre_famille_autre(self):
        response = self.client.get('{}?r=autre'.format(reverse('batoude-famille-add', args=[self.person.pk])))
        self.assertContains(
            response,
            '<option value="autre" selected>autre</option>',
            html=True
        )

    def test_archiver_beneficiaire(self):
        person = Beneficiaire.objects.get(nom='Robespierre', prenom='Archibald')
        person.date_sortie = '2020-01-01'
        person.save()

        response = self.client.get(reverse('batoude-person-list'))
        self.assertContains(
            response,
            '<tr><td colspan="5" class="bg_warning">La liste est actuellement vide</td></tr>',
            html=True
        )
        response = self.client.get(reverse('batoude-suivis-termines'))
        edit_url = reverse('batoude-person-edit', args=[person.pk])
        self.assertContains(
            response,
            f'{person.nom_prenom()}',
        )

        self.user_batoude.user_permissions.add(Permission.objects.get(codename='manage_batoude'))
        response = self.client.get(reverse('batoude-suivis-termines'))
        edit_url = reverse('batoude-person-edit', args=[person.pk])
        self.assertContains(
            response,
            f'<a href="{edit_url}">{person.nom_prenom()}</a>',
            html=True
        )

    def test_dossier_archivable(self):
        person = Beneficiaire.objects.get(nom='Robespierre', prenom='Archibald')
        self.user_batoude.user_permissions.add(Permission.objects.get(codename='export_aemo'))
        self.assertFalse(person.can_be_deleted(self.user_batoude))

        person.date_sortie = date(2020, 1, 1)
        person.save()
        self.assertTrue(person.can_be_deleted(self.user_batoude))

    def test_beneficiaire_form_localite_error(self):
        form = BeneficiaireForm(instance=None, data={'nom': 'Doe', 'localite': '2000 Neuchâtel'})
        self.assertFalse(form.is_valid())
        self.assertIn("Le nom de la localité est incorrect.", form.errors['localite'])

    def test_famille_form_localite_sans_npa_error(self):
        form = BeneficiaireForm(instance=None, data={'nom': 'Doe', 'localite': 'Neuchâtel'})
        self.assertFalse(form.is_valid())
        self.assertIn("Vous devez saisir un NPA pour la localité.", form.errors['__all__'])

    def test_suivi_deux_referents_ope(self):
        person = Beneficiaire.objects.get(nom='Robespierre')
        self.client.force_login(self.user_batoude)
        suivi_url = reverse('batoude-person-edit', args=[person.pk])
        response = self.client.post(
            suivi_url,
            data={
                'nom': person.nom,
                'prenom': person.prenom,
                'genre': person.genre,
                'referent_ope': Contact.objects.get(nom='Rathfeld').pk,
                'referent_ope2': Contact.objects.create(
                    nom='Doe',
                    prenom='John',
                    service=Service.objects.get(sigle='OPEN')).pk,
                'date_demande': date.today(),
                'region': 'mont_locle'
            }
        )
        self.assertRedirects(response, reverse('batoude-person-list'))
        person.refresh_from_db()
        self.assertEqual(
            person.get_referents_ope(),
            'Rathfeld Christophe (OPEN) / Doe John (OPEN)'
        )

    def test_beneficiaire_anonymiser(self):
        benef = Beneficiaire.objects.create(
            nom='Doe', prenom='John', rue='Rue du Lac', npa='2000', localite='Marseille', telephone='+41 79 111 22 33',
        )
        MembreFamille.objects.create(nom='Doe', genre='M', role=Role.objects.get(nom='Père'), beneficiaire=benef)
        benef.referent_ope = None
        benef.referent_ope2 = None
        benef.reseau.add(Contact.objects.get(nom='Rathfeld'))
        benef.date_admission = date(2020, 1, 1)
        benef.date_sortie = date.today()
        benef.save()
        PrestationBatoude.objects.create(
            date_prestation=date.today(),
            duree='2:33',
            lib_prestation=LibellePrestation.objects.filter(code__in=['p05', 'p06'], unite='batoude').first(),
            participants='Pierre, Paul, Jacques',
            texte='Un text',
            auteur=self.user_batoude,
            beneficiaire=benef
        )

        Tache.objects.create(**{
            'date': timezone.now(),
            'texte': 'Arroser les plantes',
            'delai': (date.today() + timedelta(days=3)).strftime('%Y-%m-%d'),
            'resp': self.user_batoude,
            'auteur': self.user_batoude,
            'realise': None,
            'beneficiaire': benef
        })

        with open(__file__, mode='rb') as fh:
            Document.objects.create(person=benef, fichier=File(fh, name='essai.txt'), titre='Essai',
                                    date_creation=timezone.now(), auteur=self.user_batoude)

        Absence.objects.create(**{
            'date_debut': '2019-12-12',
            'nbre_jour': 2,
            'motif': 'maladie',
            'beneficiaire': benef
        })

        self.assertEqual(benef.absences.count(), 1)
        self.assertEqual(benef.documents.count(), 1)
        self.assertEqual(benef.membres_famille.count(), 1)
        self.assertEqual(benef.prestations_batoude.count(), 1)
        self.assertEqual(benef.taches.count(), 1)
        self.assertEqual(benef.reseau.count(), 1)

        benef.anonymiser()
        benef.refresh_from_db()

        self.assertEqual(benef.absences.count(), 0)
        self.assertEqual(benef.documents.count(), 0)
        self.assertEqual(benef.membres_famille.count(), 0)
        self.assertEqual(benef.taches.count(), 0)
        self.assertEqual(benef.reseau.count(), 0)

        self.assertEqual(benef.prestations_batoude.count(), 1)

    def test_date_demande_obligatoire(self):
        form = BeneficiaireForm(data={'nom': 'Doe', 'prenom': 'John'})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['date_demande'], ["Ce champ est obligatoire."])

    @freeze_time(date(2023, 11, 30))
    def test_liste_attente(self):
        aujourdhui = date.today()
        hier = aujourdhui - timedelta(days=1)
        demain = aujourdhui + timedelta(days=1)
        Beneficiaire.objects.bulk_create([
            Beneficiaire(nom='Benef1', prenom='Paul', date_demande=hier),
            Beneficiaire(nom='Benef2', prenom='Paul', date_demande=hier, date_entretien=aujourdhui,
                         date_validation=aujourdhui),
            Beneficiaire(nom='Benef3', prenom='Paul', date_demande=hier, date_entretien=aujourdhui,
                         date_validation=aujourdhui, date_admission=demain),
            Beneficiaire(nom='Benef4', prenom='Paul', date_demande=hier, date_entretien=aujourdhui,
                         date_sortie=demain)
        ])
        benef1 = Beneficiaire.objects.get(nom='Benef1')
        benef1.referent_ope = Contact.objects.get(nom='Rathfeld')
        self.client.force_login(self.user_batoude)
        list_url = reverse('batoude-person-list')
        response = self.client.get(list_url)
        if aujourdhui.month == demain.month:
            self.assertIn('Benef3', response.context['object_list'][0].nom)
        else:
            self.assertNotIn('Benef3', response.context['object_list'])
            self.assertNotIn('Benef3', response.context['liste_attente'])
            # Benef3 sera actif le mois suivant
        self.assertIn('Benef1', response.context['liste_demande'][0].nom)
        self.assertContains(response, '<td>Rathfeld Christophe (OPEN)</td>', html=True)
        self.assertIn('Benef2', response.context['liste_attente'][0].nom)

        benef4 = Beneficiaire.objects.get(nom='Benef4')
        self.assertNotIn(benef4, response.context['liste_attente'])
        self.assertNotIn(benef4, response.context['liste_demande'])

    def test_affiche_seulement_contact_ope(self):
        Contact.objects.create(nom='Doe', service=Service.objects.get(sigle='OPEN'))
        Contact.objects.create(nom='Smith', service=Service.objects.get(sigle='CARREFOUR'))
        self.client.force_login(self.user_batoude)
        response = self.client.get(reverse('batoude-person-add'))
        self.assertContains(response, 'Doe', html=False)
        self.assertNotContains(response, 'Smith', html=False)

    def test_list_beneficiaire(self):
        aujourdhui = date.today()
        hier = aujourdhui - timedelta(days=1)
        demain = aujourdhui + timedelta(days=1)
        dans_2_mois = demain + timedelta(days=60)
        Beneficiaire.objects.create(
            nom='Benef', prenom='Paul', date_demande=hier, date_entretien=aujourdhui,
            date_validation=aujourdhui, date_admission=dans_2_mois
        )
        self.client.force_login(self.user_batoude)
        response = self.client.get(reverse('batoude-person-list'))
        self.assertEqual(len(response.context['object_list']), 2)

    def test_ne_plus_afficher_beneficiaire_des_le_jour_du_depart(self):
        aujourdhui = date.today()
        Beneficiaire.objects.create(
            nom='Benef', prenom='Paul', date_demande=aujourdhui, date_entretien=aujourdhui,
            date_validation=aujourdhui, date_admission=aujourdhui, date_sortie=aujourdhui
        )
        self.client.force_login(self.user_batoude)
        response = self.client.get(reverse('batoude-person-list'))
        self.assertNotIn('Benef', response)


@tag('pdf')
class PdfTests(BatoudeDataMixin, TestCase):

    def test_print_coordinates(self):
        medecin = Contact.objects.get(nom='DrSpontz')
        self.person.reseau.set([medecin])
        self.person.referent_ope = Contact.objects.get(nom='Rathfeld')
        self.person.save()
        self.client.force_login(self.user_batoude)
        response = self.client.get(reverse('batoude-print-coordinates', args=(self.person.pk,)))
        self.assertEqual(
            response['content-disposition'],
            'attachment; filename="robespierre_Coordonnees.pdf"'
        )
        self.assertEqual(response['content-type'], 'application/pdf')
        self.assertGreater(len(response.getvalue()), 200)

    def test_print_journal(self):
        self.client.force_login(self.user_batoude)
        response = self.client.get(reverse('batoude-print-journal', args=(self.person.pk,)))
        self.assertEqual(
            response['content-disposition'],
            'attachment; filename="robespierre_journal.pdf"'
        )
        self.assertEqual(response['content-type'], 'application/pdf')
        self.assertGreater(len(response.getvalue()), 200)


@tag('journal')
class JournalTest(BatoudeDataMixin, TestCase):

    def _creer_dossier_ferme(self):
        self.person.date_sortie = date.today() - timedelta(days=1)
        self.person.save()
        prest = PrestationBatoude.objects.create(
            date_prestation=date.today(),
            duree='2:33',
            lib_prestation=LibellePrestation.objects.filter(code__in=['p05', 'p06'], unite='batoude').first(),
            participants='Pierre, Paul, Jacques',
            texte='Un text',
            auteur=self.user_batoude,
            beneficiaire=self.person
        )

        return prest

    def test_affichage_journal(self):
        prest = PrestationBatoude.objects.create(
            date_prestation=date.today(),
            duree='2:33',
            lib_prestation=LibellePrestation.objects.filter(code__in=['p05', 'p06'], unite='batoude').first(),
            participants='Pierre, Paul, Jacques',
            texte='Un text',
            auteur=self.user_batoude,
            beneficiaire=self.person
        )
        prest.intervenants.add(self.user_batoude.pk)
        self.client.force_login(self.user_batoude)
        response = self.client.get(reverse('batoude-journal-list', args=[self.person.pk]))
        self.assertEqual(
            response.context['person'].prestations_batoude.all()[0].participants, 'Pierre, Paul, Jacques'
        )

    def test_afficher_lien_vers_journal(self):
        self._creer_dossier_ferme()
        self.client.force_login(self.user_batoude)
        response = self.client.get(reverse('batoude-suivis-termines'))
        url = reverse('batoude-journal-list', args=[self.person.pk])
        self.assertContains(response, f'<a href="{ url }">', html=False)

    def test_autoriser_lecture_journal_par_educ(self):
        self._creer_dossier_ferme()
        self.client.force_login(self.user_batoude)
        response = self.client.get(reverse('batoude-journal-list', args=[self.person.pk]))
        self.assertEqual(response.status_code, 200)

    def test_recherche(self):
        PrestationBatoude.objects.create(
            date_prestation=date.today(),
            duree='2:33',
            lib_prestation=LibellePrestation.objects.filter(code__in=['p05', 'p06'], unite='batoude').first(),
            participants='Pierre, Paul, Jacques',
            texte='Un texte',
            auteur=self.user_batoude,
            beneficiaire=self.person
        )
        PrestationBatoude.objects.create(
            date_prestation=date.today(),
            duree='2:33',
            lib_prestation=LibellePrestation.objects.filter(code__in=['p05', 'p06'], unite='batoude').first(),
            participants='Pierre, Paul, Jacques',
            texte='Lorèm ipsum dôlor',
            auteur=self.user_batoude,
            beneficiaire=self.person
        )
        self.client.force_login(self.user_batoude)
        response = self.client.get(reverse('batoude-journal-list', args=[self.person.pk]) + "?search=dolor")
        self.assertEqual(len(response.context['object_list']), 1)
        self.assertContains(response, 'Lorèm ipsum dôlor', html=False)
        self.assertNotContains(response, 'Un texte', html=False)


class PrestationBatoudeTest(BatoudeDataMixin, TestCase):

    def setUp(self):
        super().setUp()
        self.educ1 = Utilisateur.objects.create_user('educ1', 'educ1@example.org', sigle='ed1')
        self.educ2 = Utilisateur.objects.create_user('educ2', 'educ2@example.org', sigle='ed2')
        self.educ1.groups.add(self.group_batoude)
        self.educ2.groups.add(self.group_batoude)
        self.educ1.user_permissions.add(Permission.objects.get(codename='view_batoude'))
        self.educ2.user_permissions.add(Permission.objects.get(codename='view_batoude'))
        self.educ1.user_permissions.add(Permission.objects.get(codename='change_batoude'))
        self.educ2.user_permissions.add(Permission.objects.get(codename='change_batoude'))

        self.prestation_privee = {
            'date_prestation': date.today(),
            'duree': '2:30',
            'lib_prestation': LibellePrestation.objects.get(code='p05', unite='batoude').pk,
            'participants': 'Pierre, Paul, Jacques',
            'texte': 'Un text',
            'intervenants': [self.educ1.pk, self.educ2.pk, self.user_batoude.pk],
            'auteur': self.user_batoude.pk,
            'beneficiaire': self.person.pk
        }
        self.client.force_login(self.user_batoude)

    def test_affichage_menu_prestation(self):
        response = self.client.get(reverse('batoude-prestation-menu'))
        self.assertEqual(response.status_code, 200)
        person = response.context['beneficiaires'][0]
        self.assertEqual(person.nom_prenom(), 'Robespierre Archibald')
        self.assertEqual(format_duree(person.user_prest), '00:00')
        self.assertEqual(format_duree(person.temps_total_prestations()), '00:00')
        self.assertEqual(response.context['temps_gen_user'], '00:00')
        self.assertEqual(response.context['temps_gen_batoude'], '00:00')

    def test_ajout_prestations(self):
        # Prestation privée
        self.assertEqual(PrestationBatoude.objects.all().count(), 0)
        response = self.client.post(
            reverse('batoude-prestation-add', args=[self.person.pk]),
            data=self.prestation_privee
        )
        self.assertRedirects(response, reverse('batoude-prestation-list', args=[self.person.pk]))
        self.assertEqual(PrestationBatoude.objects.all().count(), 1)
        prestation = PrestationBatoude.objects.get(date_prestation=date.today(), beneficiaire=self.person)

        response = self.client.get(reverse('batoude-prestation-list', args=[self.person.pk]))
        self.assertContains(
            response,
            '<a href="{}">'.format(reverse('batoude-prestation-edit', args=[self.person.pk, prestation.pk])),
            html=False
        )

        response = self.client.get(reverse('batoude-prestation-menu'))
        person = response.context['beneficiaires'][0]
        self.assertEqual(person.nom_prenom(), 'Robespierre Archibald')
        self.assertEqual(format_duree(person.user_prest), '02:30')
        self.assertEqual(format_duree(person.temps_total_prestations()), '07:30')
        self.assertEqual(response.context['temps_gen_user'], '00:00')
        self.assertEqual(response.context['temps_gen_batoude'], '00:00')

        # Prestation générale
        prest_gen = self.prestation_privee
        prest_gen['lib_prestation'] = LibellePrestation.objects.get(
            code='p06',
            unite='batoude'
        ).pk,
        self.assertEqual(PrestationBatoude.objects.all().count(), 1)
        response = self.client.post(
            reverse('batoude-prestation-gen-add'),
            data=prest_gen,
            follow=True
        )
        person = response.context['beneficiaires'][0]
        self.assertEqual(person.nom_prenom(), 'Robespierre Archibald')
        self.assertEqual(format_duree(person.user_prest), '02:30')
        self.assertEqual(format_duree(person.temps_total_prestations()), '07:30')
        self.assertEqual(response.context['temps_gen_user'], '02:30')
        self.assertEqual(response.context['temps_gen_batoude'], '07:30')

        response = self.client.get(reverse('batoude-prestation-list', args=[self.person.pk]))
        prestation = response.context['prestations'][0]
        self.assertEqual(format_duree(prestation[0]['total']), '02:30')
        self.assertEqual(prestation[1][0].auteur, self.user_batoude)

        response = self.client.get(reverse('batoude-prestation-gen-list'))
        prestation = response.context['prestations'][0]
        self.assertEqual(format_duree(prestation[0]['total']), '02:30')
        self.assertEqual(prestation[1][0].auteur, self.user_batoude)

    def test_affichage_temps_total_mensuel(self):
        prestation_privee = {
            'date_prestation': date.today(),
            'duree': '43:30',
            'lib_prestation': LibellePrestation.objects.get(code='p05', unite='batoude').pk,
            'participants': 'Pierre, Paul, Jacques',
            'texte': 'Un text',
            'intervenants': [self.educ1.pk, self.educ2.pk, self.user_batoude.pk],
            'auteur': self.user_batoude.pk,
            'beneficiaire': self.person.pk
        }
        response = self.client.post(
            reverse('batoude-prestation-add', args=[self.person.pk]),
            data=prestation_privee
        )
        response = self.client.get(reverse('batoude-prestation-list', args=[self.person.pk]))
        self.assertContains(response, "43:30", html=False)

    def test_saisie_avec_date_erronnee(self):
        today = date.today()
        self.prestation_privee['date_prestation'] = date(2015, today.month, 10)
        form = PrestationForm(prest_code=FAM_CODE, user=self.user_batoude, data=self.prestation_privee)
        self.assertFalse(form.is_valid())

    def test_prestation_personnelle(self):
        response = self.client.post(
            reverse('batoude-prestation-add', args=[self.person.pk]), data=self.prestation_privee
        )

        # Prestation générale
        create_kwargs = {
            **self.prestation_privee,
            'lib_prestation': LibellePrestation.objects.get(code='p06').pk,
            'beneficiaire': '',
        }
        response = self.client.post(reverse('batoude-prestation-gen-add'), data=create_kwargs)

        # Prestation générale passée
        create_kwargs = {
            **self.prestation_privee,
            'lib_prestation': LibellePrestation.objects.get(code='p07'),
            'beneficiaire': '',
            'date_prestation': date.today() - timedelta(days=33),
        }
        self.client.post(reverse('batoude-prestation-gen-add'), data=create_kwargs)

        response = self.client.get(reverse('batoude-prestation-recap-perso'))
        self.assertEqual(format_duree(response.context['totaux'][0][1]), '02:30')
        self.assertEqual(format_duree(response.context['totaux'][1][1]), '02:30')
        self.assertEqual(format_duree(response.context['totaux'][2][1]), '00:00')
        self.assertEqual(format_duree(response.context['totaux'][3][1]), '00:00')
        self.assertEqual(format_duree(response.context['total_final']), '05:00')

    def test_delai_saisie_prestation_pour_educs(self):
        data = {**self.prestation_privee, 'date_prestation': "2023-01-31"}
        mock_date = date(2023, 2, PrestationBatoude.max_jour)
        with freeze_time(mock_date):
            form = PrestationForm(prest_code=['p05'], user=self.user_batoude, data=data)
            self.assertTrue(form.is_valid())

        mock_date = date(2023, 2, PrestationBatoude.max_jour + 1)
        with freeze_time(mock_date):
            form = PrestationForm(prest_code=['p05'], user=self.user_batoude, data=data)
            self.assertFalse(form.is_valid())

    def test_delai_saisie_prestation_pour_re(self):
        user_batoude_re = Utilisateur.objects.create_user(
            'user_batoude_re', 'user_batoude_re@example.org', nom='BatoudeRe', prenom='PrénomRe', sigle='AP'
        )
        user_batoude_re.user_permissions.add(Permission.objects.get(codename='manage_batoude'))
        data = {**self.prestation_privee, 'date_prestation': "2023-01-31"}
        mock_date = date(2023, 2, PrestationBatoude.max_jour_re)
        with freeze_time(mock_date):
            form = PrestationForm(prest_code=['p05'], user=user_batoude_re, data=data)
            self.assertTrue(form.is_valid())

        mock_date = date(2023, 2, PrestationBatoude.max_jour_re + 1)
        with freeze_time(mock_date):
            form = PrestationForm(prest_code=['p05'], user=user_batoude_re, data=data)
            self.assertFalse(form.is_valid())

    def test_affichage_formulaire_prestation(self):
        response = self.client.post(reverse('batoude-prestation-add', args=[self.person.pk]))
        self.assertContains(response, 'Prestation 5 (avec le jeune)')
        response = self.client.post(reverse('batoude-prestation-gen-add'))
        self.assertContains(response, 'Prestation 6 (sans le jeune)')


class ExportTests(BatoudeDataMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.cfg_date = date(2019, 10, 3)
        self.cfg_sheet = 'En_cours_{}.{}'.format(self.cfg_date.month, self.cfg_date.year)
        self.cfg_export_month = {'mois': self.cfg_date.month, 'annee': self.cfg_date.year}
        Beneficiaire.objects.all().delete()

    def test_repartition_prest_pers_entre_beneficiaires(self):
        """ La somme totale des prestations mensuelles personnelles est répartie entre
            les bénéficiaires actifs
        """
        mydate = date(2019, 10, 3)
        b1 = Beneficiaire.objects.create(nom='Haddock', date_demande=mydate, date_admission=mydate)
        b2 = Beneficiaire.objects.create(nom='Tournesol', date_demande=mydate, date_admission=mydate)

        p1 = PrestationBatoude.objects.create(beneficiaire=b1, date_prestation=mydate, duree='5:20')
        p1.intervenants.add(self.user_batoude)
        self.assertEqual(p1.benef_actifs, 2)

        p2 = PrestationBatoude.objects.create(beneficiaire=b2, date_prestation=mydate, duree='1:20')
        p2.intervenants.add(self.user_batoude)
        self.assertEqual(p2.benef_actifs, 2)

        exp = TestExporter()
        self.client.force_login(self.user_admin)
        with patch('carrefour.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '10', 'annee': '2019'})
        sheet_name = 'En_cours_10.2019'
        self.assertEqual(len(exp.sheets[sheet_name]), 3)
        self.assertEqual([line[2] for line in exp.sheets[sheet_name]], ['Nom', 'Haddock', 'Tournesol'])

    def test_repartition_prest_gen_entre_beneficiaire(self):
        """ La somme totale des prestations mensuelles générales est répartie entre
            les bénéficiaires actifs
        """
        Beneficiaire.objects.create(nom='Haddock', date_demande=self.cfg_date, date_admission=self.cfg_date)
        Beneficiaire.objects.create(nom='Tournesol', date_demande=self.cfg_date, date_admission=self.cfg_date)

        p1 = PrestationBatoude.objects.create(beneficiaire=None, date_prestation=self.cfg_date, duree='5:20')
        p1.intervenants.add(self.user_batoude)
        p2 = PrestationBatoude.objects.create(beneficiaire=None, date_prestation=self.cfg_date, duree='1:20')
        p2.intervenants.add(self.user_batoude)

        exp = TestExporter()
        self.client.force_login(self.user_admin)
        with patch('carrefour.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), self.cfg_export_month)
        self.assertEqual(len(exp.sheets[self.cfg_sheet]), 3)
        self.assertEqual(exp.sheets[self.cfg_sheet][1][21], timedelta(hours=3, minutes=20))
        self.assertEqual(exp.sheets[self.cfg_sheet][2][21], timedelta(hours=3, minutes=20))

    def test_temps_prest_multiplie_par_nombre_intervenants(self):
        b1 = Beneficiaire.objects.create(
            nom='Haddock', prenom='Toto', naissance=date(2010, 2, 16), npa='2000', rue='Château1',
            localite='Moulinsart', date_demande=self.cfg_date, date_admission=self.cfg_date
        )
        p1 = PrestationBatoude.objects.create(beneficiaire=b1, date_prestation=self.cfg_date, duree='5:20')
        p1.intervenants.add(self.user_batoude)
        p1.intervenants.add(self.user_externe)

        exp = TestExporter()
        self.client.force_login(self.user_admin)
        with patch('carrefour.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), self.cfg_export_month)
        self.assertEqual(len(exp.sheets[self.cfg_sheet]), 2)
        self.assertEqual(exp.sheets[self.cfg_sheet][1][20], timedelta(hours=10, minutes=40))

    def test_export_liste_beneficiaire_acces_RE(self):
        self.client.force_login(self.user_batoude)
        self.assertFalse(self.user_batoude.has_perm('batoude.manage_batoude'))
        response = self.client.get(reverse('batoude-person-list') + '?export=1')
        self.assertEqual(response.status_code, 403)

        self.client.force_login(self.manager_batoude)
        response = self.client.get(reverse('batoude-person-list') + '?export=1')
        self.assertEqual(response.status_code, 200)

    def test_export_liste(self):
        Beneficiaire.objects.create(nom='Haddock', date_demande=date(2023, 1, 1))
        Beneficiaire.objects.create(nom='Tournesol', date_demande=date(2023, 1, 1), date_admission=date(2023, 2, 1))
        self.client.force_login(self.manager_batoude)
        response = self.client.get(reverse('batoude-person-list') + "?export=1")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['content-type'],openxml_contenttype)


class DocumentTests(BatoudeDataMixin, TempMediaRootMixin, TestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user_asaef = Utilisateur.objects.create_user('user_asaef', 'user_asaef@example.org', sigle='UA')
        cls.user_asaef.user_permissions.add(Permission.objects.get(codename='view_asaef'))
        cls.user_asaef.user_permissions.add(Permission.objects.get(codename='change_asaef'))

        cls.user_asap = Utilisateur.objects.create_user('user_asap', 'user_asap@example.org', sigle='US')
        cls.user_asap.user_permissions.add(Permission.objects.get(codename='view_ser'))
        cls.user_asap.user_permissions.add(Permission.objects.get(codename='change_ser'))

        cls.group_batoude = Group.objects.get(name='batoude')
        cls.user_batoude_2 = Utilisateur.objects.create_user('Doe', 'doe@example.org', sigle='dj')
        cls.user_batoude_2.user_permissions.add(Permission.objects.get(codename='view_batoude'))
        cls.user_batoude_2.user_permissions.add(Permission.objects.get(codename='change_batoude'))

    def setUp(self):
        self.doc_create_kwargs = {
            'person': self.person.pk,
            'date_creation': timezone.now(),
            'auteur': self.user_batoude.pk,
            'titre': "Fichier test",
            'categorie': 'communication'
        }

    def _create_document(self):
        path = os.path.join(settings.BASE_DIR, 'archive/tests')
        with (Path(path) / 'sample.docx').open(mode='rb') as fh:
            return Document.objects.create(
                person=self.person,
                date_creation=timezone.now(),
                auteur=self.user_batoude,
                fichier=File(fh, name='sample.docx'),
                titre="Fichier test",
                categorie='communication'
            )

    def test_document_listview_access_ok(self):
        self.client.force_login(self.user_batoude)
        response = self.client.get(reverse('batoude-doc-list', args=[self.person.pk]))
        self.assertEqual(response.status_code, 200)

    def test_document_listview_access_erreur(self):
        for user in [self.user_aemo, self.user_asaef, self.user_asap, self.user_externe]:
            self.client.force_login(user)
            response = self.client.get(reverse('batoude-doc-list', args=[self.person.pk]))
            self.assertEqual(response.status_code, 403)

    def test_affichage_titres_dossiers(self):
        self.client.force_login(self.user_batoude)
        response = self.client.get(reverse('batoude-doc-list', args=[self.person.pk]))
        for value in Document.CATEGORIE_CHOICES:
            self.assertContains(response, value[1])

    def test_affichage_lien_vers_document(self):
        doc = self._create_document()
        self.client.force_login(self.user_batoude)
        response = self.client.get(reverse('batoude-doc-list', args=[self.person.pk]))
        self.assertContains(response, f'<a href="{ doc.fichier.url }">Fichier test</a>', html=True)

    def test_bouton_supression_document_affiche_pour_auteur(self):
        doc = self._create_document()
        self.client.force_login(self.user_batoude)
        response = self.client.get(reverse('batoude-doc-list', args=[self.person.pk]))
        delete_url = reverse('batoude-doc-delete', args=[self.person.pk, doc.pk])
        self.assertContains(
            response,
            f'<form class="inline" method="post" action="{ delete_url }">',
            html=False
        )

    def test_bouton_suppression_document_non_affiche(self):
        doc = self._create_document()
        self.client.force_login(self.user_batoude_2)
        response = self.client.get(reverse('batoude-doc-list', args=[self.person.pk]))
        delete_url = reverse('batoude-doc-delete', args=[self.person.pk, doc.pk])
        self.assertNotContains(
            response,
            f'<form class="inline" method="post" action="{ delete_url }">',
            html=False
        )

    def test_suppression_document_impossible(self):
        doc = self._create_document()
        self.client.force_login(self.user_batoude_2)
        delete_url = reverse('batoude-doc-delete', args=[self.person.pk, doc.pk])
        response = self.client.post(delete_url)
        self.assertEqual(response.status_code, 403)

    def test_upload_view(self):
        self.client.force_login(self.user_batoude)
        path = Path(settings.BASE_DIR, 'archive/tests/sample.docx')
        with path.open(mode='rb') as fh:
            test_file = File(fh, name='sample.docx')
            response = self.client.post(
                reverse('batoude-doc-upload', args=[self.person.pk]),
                data={**self.doc_create_kwargs, 'fichier': test_file},
            )
        self.assertRedirects(response, reverse('batoude-doc-list', args=[self.person.pk]))

    def test_effacement_dossier_person_si_vide(self):
        doc = self._create_document()
        user_path = Path(doc.fichier.path).parent
        # Clear all other user docs
        [os.remove(f) for f in user_path.iterdir() if f.name not in doc.fichier.name]
        self.assertTrue(user_path.exists())
        self.client.force_login(self.user_batoude)
        delete_url = reverse('batoude-doc-delete', args=[self.person.pk, doc.pk])
        response = self.client.post(delete_url)
        self.assertEqual(response.status_code, 302)
        self.assertFalse(user_path.exists())


class AgendaTests(BatoudeDataMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        Beneficiaire.objects.create(nom='Doe', genre='M')

    def test_affichage_agenda(self):
        benef = Beneficiaire.objects.get(nom="Doe")
        self.client.force_login(self.user_batoude)
        suivi_url = reverse('batoude-person-agenda', args=[benef.pk])
        response = self.client.get(suivi_url)
        titres = ['DEMANDE', 'ADMISSION', 'AUTONOMISATION', 'PROLONGATION', 'FIN']
        for titre in titres:
            self.assertContains(response, titre, html=False)

    def test_chronologie(self):
        auj = date.today()
        demain = auj + timedelta(days=1)
        dates = {dd: auj for dd in ['date_demande', 'date_entretien', 'date_admission', 'date_sortie']}
        benef = Beneficiaire.objects.get(nom='Doe')

        form = AgendaForm(instance=benef, data={**dates, 'date_demande': demain})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['__all__'], ["La date «Date entretien d'info.» ne respecte pas l’ordre chronologique!"]
        )

        form = AgendaForm(instance=benef, data={**dates, 'date_entretien': demain})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['__all__'], ["La date «Date d'admission» ne respecte pas l’ordre chronologique!"]
        )

        form = AgendaForm(instance=benef, data={**dates, 'date_admission': demain})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['__all__'], ["La date «Date de sortie» ne respecte pas l’ordre chronologique!"]
        )

    def test_cloture_dossier_sans_date_sortie(self):
        benef = Beneficiaire.objects.get(nom='Doe')
        benef.date_admission = '2019-01-01'  # mode = suivi
        benef.save()
        data = {'date_demande': '2019-01-01', 'date_entretien': '2019-01-01', 'date_admission': '2019-01-01',
                'motif_fin_suivi': 'relai', 'autoevaluation': 6, 'destination': 'autre'}
        form = AgendaForm(instance=benef, data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['__all__'],
            ["Pour fermer un dossier, il faut une date de sortie, un motif de fin et une destination."]
        )

    def test_fin_suivi_avec_date_invalide(self):
        benef = Beneficiaire.objects.get(nom='Doe')
        data = {'date_demande': '2019-01-01', 'date_entretien': '2019-01-01', 'date_admission': '2019-01-01',
                'motif_fin_suivi': 'abandon_famille', 'autoevaluation': 6, 'date_sortie': '2019-01-32'}
        form = AgendaForm(instance=benef, data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['date_sortie'], ['Saisissez une date valide.']
        )

    def test_cloture_dossier_sans_motif(self):
        benef = Beneficiaire.objects.get(nom='Doe')
        data = {'date_demande': '2019-01-01', 'date_entretien': '2019-01-01', 'date_admission': '2019-01-01',
                'autoevaluation': 6, 'date_sortie': '2019-01-01', 'destination': 'autre'}
        form = AgendaForm(instance=benef, data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['__all__'],
            ["Pour fermer un dossier, il faut une date de sortie, un motif de fin et une destination."]
        )

    def test_cloture_dossier_sans_destination(self):
        benef = Beneficiaire.objects.get(nom='Doe')
        data = {'date_demande': '2019-01-01', 'date_entretien': '2019-01-01', 'date_admission': '2019-01-01',
                'autoevaluation': 6, 'date_sortie': '2019-01-01', 'motif_fin_suivi': 'relai'}
        form = AgendaForm(instance=benef, data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['__all__'],
            ["Pour fermer un dossier, il faut une date de sortie, un motif de fin et une destination."]
        )

    def test_cloture_dossier_sans_evaluation(self):
        benef = Beneficiaire.objects.get(nom='Doe')
        benef.date_admission = '2019-01-01'  # mode = suivi
        benef.save()
        data = {'date_demande': '2019-01-01', 'date_entretien': '2019-01-01', 'date_admission': '2019-01-01',
                'motif_fin_suivi': 'relai', 'date_sortie': '2019-01-01', 'destination': 'autre'}
        form = AgendaForm(instance=benef, data=data)
        self.assertTrue(form.is_valid())

    def test_saisie_par_erreur_OK(self):
        benef = Beneficiaire.objects.get(nom="Doe")
        form = AgendaForm(instance=benef, data={'date_demande': '2019-09-01', 'motif_fin_suivi': 'erreur'})
        self.assertTrue(form.is_valid())

        self.assertEqual(form.cleaned_data['date_sortie'], date.today())  # valeur par defaut
        self.assertEqual(benef.motif_fin_suivi, 'erreur')

    def test_abandon(self):
        benef = Beneficiaire.objects.get(nom="Doe")
        form = AgendaForm(instance=benef, data={'date_demande': date.today(), 'motif_fin_suivi': 'abandon_batoude'})
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['date_sortie'], date.today())
        self.assertEqual(benef.motif_fin_suivi, 'abandon_batoude')

    def test_affichage_menu_abandon(self):
        benef = Beneficiaire.objects.get(nom="Doe")
        benef.date_demande = date.today()
        benef.save()
        self.client.force_login(self.user_batoude)
        suivi_url = reverse('batoude-person-agenda', args=[benef.pk])
        response = self.client.get(suivi_url)
        self.assertEqual(response.context['mode'], 'preparation')
        self.assertContains(response, '<div><em>Si abandon du dossier:</em></div>', html=True)
        self.assertContains(response, '<option value="erreur">Erreur de saisie</option>', html=True)
        self.assertContains(response, '<option value="non_aboutie">Demande non aboutie</option>', html=True)
        self.assertContains(response, '<option value="abandon_famille">Refus de la famille</option>', html=True)
        self.assertContains(response, '<option value="abandon_batoude">Refus de la Batoude</option>', html=True)
        self.assertContains(response, '<option value="abandon_mase">MASE refusé</option>', html=True)
        self.assertContains(response, '<option value="abandon_relais">Réorientation</option>', html=True)

    def test_masquer_menu_abandon(self):
        benef = Beneficiaire.objects.get(nom="Doe")
        benef.date_admission = date.today()
        benef.save()
        self.client.force_login(self.user_batoude)
        suivi_url = reverse('batoude-person-agenda', args=[benef.pk])
        response = self.client.get(suivi_url)
        self.assertEqual(response.context['mode'], 'suivi')
        self.assertNotContains(
            response, '<div id="abandon" class="border p-3" style="background-color:#FFDDAA;">', html=False
        )

    def test_nouvel_affichage_apres_modif(self):
        today = date.today()
        benef = Beneficiaire.objects.get(nom="Doe")
        self.client.force_login(self.user_batoude)
        edit_url = reverse('batoude-person-agenda', args=[benef.pk])
        response = self.client.post(
            edit_url,
            data={'date_demande': today, 'date_entretien': today}
        )
        self.assertRedirects(response, reverse('batoude-person-agenda', args=[benef.pk]))

    def test_demande_non_aboutie(self):
        benef = Beneficiaire.objects.get(nom="Doe")
        form = AgendaForm(instance=benef, data={'date_demande': date.today(), 'motif_fin_suivi': 'non_aboutie'})
        self.assertTrue(form.is_valid())
        self.assertEqual(benef.date_sortie,  date.today())
        self.assertEqual(benef.motif_fin_suivi, 'non_aboutie')

    @freeze_time("2023-10-01")
    def test_statut_appartement_next(self):
        self.client.force_login(self.user_batoude)
        benef = Beneficiaire.objects.get(nom="Doe")
        benef.date_demande = date(2023, 1, 1)
        benef.date_entretien = date(2023, 1, 1)
        benef.date_admission = date(2023, 1, 1)
        benef.date_appartement = None
        benef.save()
        delai_6mois = format_d_m_Y(benef.date_admission + relativedelta(months=6))
        delai_12mois = format_d_m_Y(benef.date_admission + relativedelta(months=12))
        response = self.client.get(reverse('batoude-person-list'))
        self.assertContains(response, '<div title="délai:01.04.2023" class="depasse">app</div>', html=True)
        self.assertContains(response, f'<div title="délai:{delai_6mois}" class="filled">6m</div>', html=True)
        self.assertContains(response, f'<div title="délai:{delai_12mois}" class="next">12m</div>', html=True)

    @freeze_time("2023-01-01")
    def test_statut(self):
        self.client.force_login(self.user_batoude)
        benef = Beneficiaire.objects.get(nom="Doe")
        benef.date_demande = date(2023, 1, 1)
        benef.date_entretien = date(2023, 1, 1)
        benef.date_admission = date(2023, 1, 1)
        benef.date_appartement = date(2023, 1, 1)
        benef.save()
        delai_6mois = format_d_m_Y(benef.date_admission + relativedelta(months=6))
        response = self.client.get(reverse('batoude-person-list'))
        self.assertContains(response, '<div title="appartement:01.01.2023" class="filled">app</div>', html=True)
        self.assertContains(response, f'<div title="délai:{delai_6mois}" class="next">6m</div>', html=True)

    @freeze_time("2023-01-02")
    def test_statut_1(self):
        self.client.force_login(self.user_batoude)
        benef = Beneficiaire.objects.get(nom="Doe")
        benef.date_demande = date(2023, 1, 1)
        benef.date_entretien = date(2023, 1, 1)
        benef.date_admission = date(2023, 1, 1)
        benef.date_appartement = date(2023, 1, 1)
        benef.save()
        delai_6mois = format_d_m_Y(benef.date_admission + relativedelta(months=6))
        response = self.client.get(reverse('batoude-person-list'))
        self.assertContains(response, '<div title="appartement:01.01.2023" class="filled">app</div>', html=True)
        self.assertContains(response, f'<div title="délai:{delai_6mois}" class="next">6m</div>', html=True)

    @freeze_time("2023-07-01")
    def test_statut_2(self):
        self.client.force_login(self.user_batoude)
        benef = Beneficiaire.objects.get(nom="Doe")
        benef.date_demande = date(2023, 1, 1)
        benef.date_entretien = date(2023, 1, 1)
        benef.date_admission = date(2023, 1, 1)
        benef.date_appartement = date(2023, 1, 1)
        benef.save()
        delai_6mois = format_d_m_Y(benef.date_admission + relativedelta(months=6))
        delai_12mois = format_d_m_Y(benef.date_admission + relativedelta(months=12))
        response = self.client.get(reverse('batoude-person-list'))
        self.assertContains(response, '<div title="appartement:01.01.2023" class="filled">app</div>', html=True)
        self.assertContains(response, f'<div title="délai:{delai_6mois}" class="urgent">6m</div>', html=True)
        self.assertContains(response, f'<div title="délai:{delai_12mois}" class="">12m</div>', html=True)

    @freeze_time("2024-03-01")
    def test_statut_3(self):
        self.client.force_login(self.user_batoude)
        benef = Beneficiaire.objects.get(nom="Doe")
        benef.date_demande = date(2023, 1, 1)
        benef.date_entretien = date(2023, 1, 1)
        benef.date_admission = date(2023, 1, 1)
        benef.date_appartement = date(2023, 1, 1)
        benef.save()
        response = self.client.get(reverse('batoude-person-list'))
        delai_6mois = format_d_m_Y(benef.date_admission + relativedelta(months=6))
        delai_12mois = format_d_m_Y(benef.date_admission + relativedelta(months=12))
        delai_18mois = format_d_m_Y(benef.date_admission + relativedelta(months=18))
        self.assertContains(response, '<div title="appartement:01.01.2023" class="filled">app</div>', html=True)
        self.assertContains(response, f'<div title="délai:{delai_6mois}" class="filled">6m</div>', html=True)
        self.assertContains(response, f'<div title="délai:{delai_12mois}" class="filled">12m</div>', html=True)
        self.assertContains(response, f'<div title="délai:{delai_18mois}" class="next">18m</div>', html=True)


class BilanTests(BatoudeDataMixin, TestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        Beneficiaire.objects.create(nom='Doe', genre='M', region='mont_locle')

    def test_bilan_createview(self):
        benef = Beneficiaire.objects.get(nom="Doe")
        self.client.force_login(self.user_batoude)
        create_url = reverse('batoude-bilan-create', args=[benef.pk, 'demande'])
        response = self.client.post(
            create_url,
            data={'date_bilan': date.today(), 'typ': 'demande', 'texte': 'Un essai',
                  'referent_ope': Contact.objects.get(nom='Rathfeld'), 'beneficiaire': benef.pk,
                  'categorie': 'famille'},
        )
        self.assertTrue(Bilan.objects.filter(categorie='famille').exists())
        self.assertRedirects(response, reverse('batoude-person-agenda', args=[benef.pk]))

    def test_bouton_sauvegarde(self):
        benef = Beneficiaire.objects.get(nom="Doe")
        self.client.force_login(self.user_batoude)
        create_url = reverse('batoude-bilan-create', args=[benef.pk, 'demande'])

        response = self.client.get(
            create_url,
            data={'date_bilan': date.today(), 'typ': 'demande', 'texte': 'Un essai',
                  'referent_ope': Contact.objects.get(nom='Rathfeld'), 'beneficiaire': benef.pk,
                  'categorie': 'famille'},
        )

        self.assertContains(
            response,
            '<button class="btn btn-sm btn-success" name="save" type="submit">Enregistrer</button>',
            html=True
        )

    def test_bilan_updateview(self):
        today = date.today()
        benef = Beneficiaire.objects.get(nom="Doe")
        self.client.force_login(self.user_batoude)
        bilan = Bilan.objects.create(beneficiaire=benef, date_bilan=today, typ='admission')
        edit_url = reverse('batoude-bilan-edit', args=[benef.pk, bilan.pk])
        self.client.post(
            edit_url,
            data={'date_bilan': today, 'typ': 'admission', 'beneficiaire': benef.pk, 'texte': "Un texte",
                  'categorie': 'atelier'}
        )
        bilan.refresh_from_db()
        self.assertEqual(bilan.texte, 'Un texte')
        self.assertEqual(bilan.categorie, 'atelier')

    def test_bilan_createform(self):
        benef = Beneficiaire.objects.get(nom="Doe")
        form = BilanForm(data={
            'date_bilan': date.today(), 'typ': 'admission', 'beneficiaire': benef.pk, 'texte': "Un texte",
            'categorie': 'autre'
        })
        self.assertTrue(form.is_valid())

    def test_bilan_form_update(self):
        today = date.today()
        benef = Beneficiaire.objects.get(nom="Doe")
        bilan = Bilan.objects.create(beneficiaire=benef, date_bilan=today, typ='admission')
        form = BilanForm(
            instance=bilan,
            data={'date_bilan': today, 'typ': 'admission', 'beneficiaire': benef.pk, 'texte': "Un long texte",
                  'categorie': 'education'}
        )
        self.assertTrue(form.is_valid())
        form.save()
        bilan.refresh_from_db()
        self.assertEqual(bilan.texte, 'Un long texte')

    def test_bilan_create_access_anonymous(self):
        benef = Beneficiaire.objects.get(nom="Doe")
        self.client.force_login(self.anonymous)
        create_url = reverse('batoude-bilan-create', args=[benef.pk, 'demande'])
        response = self.client.post(
            create_url,
            data={'date_bilan': date.today(), 'typ': 'demande', 'texte': 'Un essai',
                  'referent_ope': Contact.objects.get(nom='Rathfeld')},
        )
        self.assertEqual(response.status_code, 403)

    def test_bilan_update_access_anonymous(self):
        today = date.today()
        benef = Beneficiaire.objects.get(nom="Doe")
        self.client.force_login(self.anonymous)
        bilan = Bilan.objects.create(beneficiaire=benef, date_bilan=today, typ='admission')
        edit_url = reverse('batoude-bilan-edit', args=[benef.pk, bilan.pk])
        response = self.client.post(
            edit_url,
            data={'date_bilan': today, 'typ': 'admission', 'beneficiaire':benef.pk, 'texte': "Un texte"}
        )
        self.assertEqual(response.status_code, 403)

    def test_bilan_print_access_anonymous(self):
        benef = Beneficiaire.objects.get(nom="Doe")
        self.client.force_login(self.anonymous)
        bilan = Bilan.objects.create(beneficiaire=benef, date_bilan=date.today(), typ='admission')
        response = self.client.post(reverse('batoude-bilan-print', args=[benef.pk, bilan.pk]))
        self.assertEqual(response.status_code, 403)

    def test_bilan_print_success(self):
        benef = Beneficiaire.objects.get(nom="Doe")
        self.client.force_login(self.user_batoude)
        bilan = Bilan.objects.create(
            beneficiaire=benef, date_bilan=date.today(), typ='admission', texte="Un texte"
        )
        response = self.client.get(reverse('batoude-bilan-print', args=[benef.pk, bilan.pk]))
        self.assertEqual(response.status_code, 200)

        page = self.extract_page_pdf(response, 0)
        self.assertIn("Doe - Bilan d’admission", page)

    @freeze_time("2024-01-15")
    def test_alarme_majorite(self):
        benef = Beneficiaire.objects.get(nom="Doe")
        benef.naissance = date(2006, 4, 1)
        benef.save()
        self.client.force_login(self.user_batoude)
        response = self.client.get(reverse('batoude-person-agenda', args=[benef.pk]))
        self.assertContains(response, '<div class="text-warning bg-dark p-1">Majeur-e dans 3 mois</div>', html=True)

        benef.naissance = date(2006, 8, 1)
        benef.save()
        response = self.client.get(reverse('batoude-person-agenda', args=[benef.pk]))
        self.assertContains(response, '<div class="text-danger fw-bold">Majeur-e dans 7 mois</div>', html=True)

        benef.naissance = date(2005, 8, 1)
        benef.save()
        response = self.client.get(reverse('batoude-person-agenda', args=[benef.pk]))
        self.assertContains(response, '<div class="text-success">Majeur-e depuis 5 mois</div>', html=True)


class MedicationTests(BatoudeDataMixin, TestCase):

    def create_medication(self, medic='Aspirine', date_debut='2024-01-01', date_fin='2024-12-31'):
        return Medication.objects.create(
            medic=medic, date_debut=date_debut, date_fin=date_fin, beneficiaire=self.person
        )

    def test_model_medication(self):
        obj = self.create_medication()
        self.assertTrue(isinstance(obj, Medication))
        self.assertEqual(obj.medic, 'Aspirine')
        self.assertEqual(obj.date_debut, "2024-01-01")
        self.assertEqual(obj.date_fin, "2024-12-31")
        self.assertEqual(obj.beneficiaire.nom, 'Robespierre')

    def test_acces_autorise_pour_membre_batoude(self):
        url = reverse('batoude-medication-edit', args=[self.person.pk])

        self.client.force_login(self.user_batoude)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.client.force_login(self.manager_batoude)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.client.force_login(self.anonymous)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

    def test_medication_formset(self):
        self.client.force_login(self.user_batoude)
        response = self.client.post(
            reverse('batoude-medication-edit', args=[self.person.pk]),
            instance=self.person,
            data={
                "medic-TOTAL_FORMS": "2", "medic-INITIAL_FORMS": "0",
                "medic-0-medic": "Aspirine", 'medic-0-date_debut': '2024-01-01', 'medic-0-date_fin': '',
                "medic-1-medic": "Goutte", 'medic-1-date_debut': '', 'medic-1-date_fin': '2024-12-31',
            }
        )
        self.assertRedirects(response, reverse('batoude-person-edit', args=[self.person.pk]))
        self.assertEqual(self.person.medications.count(), 2)
        self.assertTrue(self.person.medications.filter(medic='Aspirine', date_debut="2024-1-1").exists())
        self.assertTrue(self.person.medications.filter(medic='Goutte', date_fin="2024-12-31").exists())

    def test_bilan_categorie_sante(self):
        form = BilanForm(data={
            'date_bilan': date.today(), 'typ': 'admission', 'beneficiaire': self.person.pk, 'texte': "Un texte",
            'categorie': 'sante'
        })
        self.assertTrue(form.is_valid())

    def test_lien_upload_document(self):
        self.client.force_login(self.user_batoude)
        url = reverse('batoude-medication-edit', args=[self.person.pk])
        response = self.client.get(url)
        upload_url = f"{reverse('batoude-doc-upload', args=[self.person.pk])}?cat=sante"
        self.assertContains(response, upload_url, html=False)

    def test_categorie_sante_par_defaut(self):
        self.client.force_login(self.user_batoude)
        upload_url = f"{reverse('batoude-doc-upload', args=[self.person.pk])}?cat=sante"
        response = self.client.get(upload_url)
        self.assertContains(response, '<option value="sante" selected>Santé</option>', html=True)
