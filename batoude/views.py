import os
from datetime import date, timedelta
from itertools import groupby
from pathlib import Path

from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.db.models import Q, Sum
from django.http import HttpResponseRedirect
from django.shortcuts import reverse, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import (
    UpdateView, CreateView, ListView, DeleteView, DetailView, FormView, TemplateView, View
)

from batoude.forms import (
    AgendaForm, AssuranceForm, BeneficiaireForm, BilanForm, DocumentUploadForm, JournalSearchForm, MedicationForm,
    MembreFamilleForm, PrestationForm,
)
from batoude.models import Beneficiaire, Bilan, Document, MembreFamille, PrestationBatoude
from batoude.pdf import BeneficiaireCoordinatesPdf, BilanPdf, JournalPdf
from carrefour.export import ExportStatistique
from carrefour.forms import ContactExterneAutocompleteForm
from carrefour.models import Contact, LibellePrestation, Utilisateur
from carrefour.utils import get_selected_date
from carrefour.views import (
    BaseAdminPrestationListView, BasePDFView, ContactUpdateView as CarrefourContactUpdateView,
    PaginateListView, MSG_NO_ACCESS, MSG_READ_ONLY
)


FAM_CODE = ['p05']
GEN_CODE = ['p06']


class BatoudeCheckPermMixin:
    def dispatch(self, request, *args, **kwargs):
        if not Beneficiaire.check_perm_view(request.user):
            raise PermissionDenied(MSG_NO_ACCESS)
        self.person = get_object_or_404(Beneficiaire, pk=kwargs['pk']) if 'pk' in self.kwargs else None
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.person:
            can_edit = self.person.can_edit(self.request.user)
            if not can_edit:
                if self.person.date_sortie is None:
                    messages.info(self.request, MSG_READ_ONLY)
                else:
                    messages.info(self.request, 'Ce dossier est fermé!')
            context['can_edit'] = can_edit
            context['famille'] = self.person
        else:
            context['can_edit'] = self.request.user.has_perm('batoude.change_batoude')
        return context


class BatoudeCheckOwnerMixin:
    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        owner = obj.auteur if isinstance(obj, PrestationBatoude) else None
        if owner == self.request.user:
            return super().dispatch(request, *args, **kwargs)
        raise PermissionDenied()


class BeneficiaireListView(BatoudeCheckPermMixin, ListView):
    template_name = 'batoude/beneficiaire_list.html'
    model = Beneficiaire

    def dispatch(self, request, *args, **kwargs):
        self.export_flag = request.GET.get('export', None)
        if self.export_flag and not request.user.has_perm('batoude.manage_batoude'):
            raise PermissionDenied(MSG_NO_ACCESS)
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return Beneficiaire.objects.filter(date_admission__isnull=False, date_sortie__isnull=True)

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'liste_attente': Beneficiaire.objects.filter(
                date_demande__isnull=False,
                date_validation__isnull=False,
                date_admission__isnull=True,
                date_sortie__isnull=True,
            ).order_by('date_demande'),
            'liste_demande': Beneficiaire.objects.filter(
                date_demande__isnull=False,
                date_validation__isnull=True,
                date_admission__isnull=True,
                date_sortie__isnull=True,
            )
        }

    def render_to_response(self, context, **response_kwargs):
        if self.export_flag:
            export = ExportStatistique()
            export.produce_beneficiaire_batoude("Bénéficiaires Batoude", context)
            return export.get_http_response("beneficiaire_batoude")
        else:
            return super().render_to_response(context, **response_kwargs)


class SuivisTerminesListView(BatoudeCheckPermMixin, ListView):
    template_name = 'batoude/suivis_termines.html'
    model = Beneficiaire

    def get_queryset(self):
        return super().get_queryset().filter(date_sortie__isnull=False, archived_at__isnull=True)


class BeneficiaireCreateView(BatoudeCheckPermMixin, CreateView):
    template_name = 'batoude/beneficiaire_edit.html'
    model = Beneficiaire
    form_class = BeneficiaireForm
    success_url = reverse_lazy('batoude-person-list')

    def dispatch(self, request, *args, **kwargs):
        if request.user.has_perm('batoude.change_batoude'):
            return super().dispatch(request, *args, **kwargs)
        raise PermissionDenied(MSG_NO_ACCESS)

    def form_valid(self, form):
        obj = form.save()
        if obj.referent_ope and obj.referent_ope.service:
            obj.service_orienteur = obj.referent_ope.service.sigle
            obj.save()
        return HttpResponseRedirect(self.success_url)


class BeneficiaireEditView(BatoudeCheckPermMixin, UpdateView):
    template_name = 'batoude/beneficiaire_edit.html'
    model = Beneficiaire
    form_class = BeneficiaireForm
    success_url = reverse_lazy('batoude-person-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'person': get_object_or_404(Beneficiaire, pk=self.kwargs['pk']),
            'can_edit': self.request.user.has_perm('batoude.change_batoude'),
        })
        return context


class JournalListView(BatoudeCheckPermMixin, PaginateListView):
    template_name = 'batoude/journal_list.html'
    model = PrestationBatoude
    paginate_by = 10

    def get(self, request, *args, **kwargs):
        self.filter_form = JournalSearchForm(data=self.request.GET or None)
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        journaux = self.person.prestations_batoude.all()
        if self.filter_form.is_bound and self.filter_form.is_valid():
            journaux = self.filter_form.filter(journaux)
        return journaux.order_by('-date_prestation')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context.update({
            'person': self.person,
            'search_form': self.filter_form
        })
        return context


class MembreFamilleCreateView(BatoudeCheckPermMixin, CreateView):
    template_name = 'batoude/famille_edit.html'
    model = MembreFamille
    form_class = MembreFamilleForm

    def get_initial(self):
        return {
            **super().get_initial(),
            'role': self.request.GET.get('r', None)
        }

    def form_valid(self, form):
        form.instance.beneficiaire = get_object_or_404(Beneficiaire, pk=self.kwargs['pk'])
        form.save()
        return HttpResponseRedirect(reverse('batoude-famille-list', args=[self.kwargs['pk']]))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        benef = get_object_or_404(Beneficiaire, pk=self.kwargs['pk'])
        context.update({
            'person': benef,
            'can_edit': benef.can_edit(self.request.user)
        })
        return context


class MembreFamilleUpdateView(BatoudeCheckPermMixin, UpdateView):
    template_name = 'batoude/famille_edit.html'
    model = MembreFamille
    form_class = MembreFamilleForm
    pk_url_kwarg = 'obj_pk'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'person': self.object,
            'can_edit': self.object.beneficiaire.can_edit(self.request.user)
        })
        return context

    def get_success_url(self):
        return reverse('batoude-famille-list', args=[self.kwargs['pk']])


class MembreFamilleListView(BatoudeCheckPermMixin, DetailView):
    template_name = 'batoude/famille_list.html'
    model = Beneficiaire
    context_object_name = 'person'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        roles_exist = self.object.membres_famille.values_list('role', flat=True)
        roles_dispo = {
            r[0]: r[1] for r in MembreFamille.ROLE_CHOICES
            if r[0] == 'fratrie' or r[0] not in roles_exist
        }
        roles_dispo['autre'] = 'autre'

        context.update({
            'membres': self.object.membres_famille.all().order_by('-role'),
            'roles': roles_dispo,
            'can_edit': self.request.user.has_perm('batoude.change_batoude')
        })
        return context


class AssuranceUpdateView(BatoudeCheckPermMixin, UpdateView):
    template_name = 'batoude/assurance_edit.html'
    model = Beneficiaire
    form_class = AssuranceForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'person': get_object_or_404(Beneficiaire, pk=self.kwargs['pk']),
        })
        return context

    def get_success_url(self):
        return reverse('batoude-person-edit', args=[self.kwargs['pk']])


class PrestationListView(BatoudeCheckPermMixin, TemplateView):
    template_name = 'batoude/prestation_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'pk' in self.kwargs:
            person = get_object_or_404(Beneficiaire, pk=self.kwargs['pk'])
            prestations = person.prestations_batoude.all().order_by('-date_prestation')
            historique = person.prestations_historiques()
            context['person'] = person
        else:
            person = None
            prestations = PrestationBatoude.objects.filter(beneficiaire=None)
            historique = PrestationBatoude.prestations_historiques(prestations)
        grouped_by_month = {
            k: list(prests) for (k, prests) in groupby(
                prestations, lambda p: f'{p.date_prestation.year}_{p.date_prestation.month}'
            )
        }
        context['prestations'] = [
            (hist, grouped_by_month.get(f"{hist['annee']}_{hist['mois']}"))
            for hist in historique
        ]
        return context


class PrestationRecapPersoListView(ListView):
    template_name = 'batoude/prestation_personnelle.html'
    model = PrestationBatoude
    start_date = date(2020, 1, 1)

    def dispatch(self, request, *args, **kwargs):
        str_curdate = request.GET.get('date', None)
        self.dfrom, self.dto = get_selected_date(str_curdate)
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return self.request.user.prestations_batoude.filter(
            date_prestation__gte=self.dfrom, date_prestation__lte=self.dto
        )

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        prev_month = self.dfrom - timedelta(days=2)
        next_month = self.dfrom + timedelta(days=33)
        context.update({
            'totaux': [(pp.code, self.get_queryset().filter(
                    lib_prestation__code=f"{pp.code}"
                ).aggregate(
                    tot=Sum('duree')
                )['tot']) for pp in LibellePrestation.objects.filter(unite='batoude')],
            'total_final': self.get_queryset().aggregate(tot=Sum('duree'))['tot'],
            'current_date': self.dfrom,
            'prev_month': prev_month if prev_month >= self.start_date else None,
            'next_month': next_month if next_month <= date.today() else None,
        })
        return context


class PrestationCreateView(BatoudeCheckPermMixin, CreateView):
    template_name = 'batoude/prestation_edit.html'
    model = PrestationBatoude
    form_class = PrestationForm

    def dispatch(self, *args, **kwargs):
        self.person = get_object_or_404(Beneficiaire, pk=self.kwargs['pk']) if 'pk' in self.kwargs else None
        return super().dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'prest_code': FAM_CODE if self.person else GEN_CODE,
            'user': self.request.user,
        })
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        prest_code = FAM_CODE if self.person else GEN_CODE
        context.update({
            'lib_prestations': LibellePrestation.objects.filter(unite='batoude', code__in=prest_code),
            'person': self.person,
        })
        return context

    def form_valid(self, form):
        form.instance.beneficiaire = self.person
        return super().form_valid(form)

    def get_success_url(self):
        if self.person:
            return reverse('batoude-prestation-list', args=[self.person.pk])
        return reverse('batoude-prestation-menu')


class PrestationUpdateView(BatoudeCheckOwnerMixin, UpdateView):
    template_name = 'batoude/prestation_edit.html'
    model = PrestationBatoude
    form_class = PrestationForm
    pk_url_kwarg = 'obj_pk'

    def dispatch(self, *args, **kwargs):
        self.person_id = self.kwargs['pk']
        self.person = get_object_or_404(Beneficiaire, pk=self.person_id) if self.person_id > 0 else None
        return super().dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'prest_code': FAM_CODE if self.object.lib_prestation.code in FAM_CODE else GEN_CODE,
            'user': self.request.user
        })
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'person': self.person,
        })
        return context

    def form_valid(self, form):
        form.instance.beneficiaire = self.person
        return super().form_valid(form)

    def delete_url(self):
        return reverse('batoude-prestation-delete', args=[self.person_id, self.object.pk])

    def get_success_url(self):
        if self.person:
            return reverse('batoude-prestation-list', args=[self.person.pk])
        return reverse('batoude-prestation-menu')


class PrestationDeleteView(BatoudeCheckPermMixin, DeleteView):
    template_name = 'carrefour/object_confirm_delete.html'
    model = PrestationBatoude
    pk_url_kwarg = 'obj_pk'

    def dispatch(self, *args, **kwargs):
        self.person_id = self.kwargs['pk']
        self.person = get_object_or_404(Beneficiaire, pk=self.person_id) if self.person_id > 0 else None
        return super().dispatch(*args, **kwargs)

    def get_success_url(self):
        if self.person:
            return reverse('batoude-prestation-list', args=[self.person.pk])
        return reverse('batoude-prestation-menu')


class PrestationMenuView(BatoudeCheckPermMixin, TemplateView):
    template_name = 'batoude/prestation_menu.html'

    def get_context_data(self, **kwargs):
        user = self.request.user
        context = super().get_context_data(**kwargs)
        context.update({
            'beneficiaires': Beneficiaire.benef_actifs(date.today()).annotate(
                user_prest=Sum('prestations_batoude__duree',
                               filter=Q(prestations_batoude__in=user.prestations_batoude.all())
                               ) or 0
            ).order_by('nom', 'prenom'),
            'temps_gen_user': PrestationBatoude.temps_total(user.prestations_gen('batoude'), tous_utilisateurs=False),
            'temps_gen_batoude': PrestationBatoude.temps_total_prest_gen(),
        })
        return context


class CoordPDFView(BasePDFView):
    obj_class = Beneficiaire
    pdf_class = BeneficiaireCoordinatesPdf


class JournalPDFView(BasePDFView):
    obj_class = Beneficiaire
    pdf_class = JournalPdf


class ContactListView(BatoudeCheckPermMixin, DetailView):
    template_name = 'batoude/contact_list.html'
    model = Beneficiaire

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'person': self.object,
            'form': ContactExterneAutocompleteForm(),
        })
        return context


class ContactRemoveView(BatoudeCheckPermMixin, View):
    def post(self, request, *args, **kwargs):
        person = get_object_or_404(Beneficiaire, pk=self.kwargs['pk'])
        contact = get_object_or_404(Contact, pk=self.kwargs['obj_pk'])
        person.reseau.remove(contact)
        return HttpResponseRedirect(reverse('batoude-contact-list', args=[person.pk]))


class ContactAddView(BatoudeCheckPermMixin, FormView):
    template_name = 'batoude/contact_add_form.html'
    form_class = ContactExterneAutocompleteForm
    titre_page = "Contacts"
    titre_formulaire = "Contacts enregistrés"

    def get_success_url(self):
        return reverse('batoude-contact-list', args=[self.person.pk])

    def get_context_data(self, **kwargs):
        return {**super().get_context_data(**kwargs), 'benef': self.person}

    def form_valid(self, form):
        benef = get_object_or_404(Beneficiaire, pk=self.person.pk)
        benef.reseau.add(*form.cleaned_data['reseaux'])
        return HttpResponseRedirect(self.get_success_url())


class ContactUpdateView(BatoudeCheckPermMixin, CarrefourContactUpdateView):
    pk_url_kwarg = 'obj_pk'

    def get_success_url(self):
        return reverse('batoude-contact-list', args=[self.kwargs['pk']])


class AgendaUpdateView(BatoudeCheckPermMixin, UpdateView):
    template_name = 'batoude/agenda_edit.html'
    model = Beneficiaire
    form_class = AgendaForm
    context_object_name = 'person'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'mode': 'preparation' if (self.object.date_admission is None
                                      and self.object.date_sortie is None) else 'suivi',
        })
        return context

    def get_success_url(self):
        return reverse('batoude-person-agenda', args=[self.object.pk])


class AdminPrestationListView(BatoudeCheckPermMixin, BaseAdminPrestationListView):
    prest_model = PrestationBatoude
    titre = 'Interv. Batoude'

    def get_queryset(self):
        return Utilisateur.intervenants_batoude(annee=self.annee)


class DocumentListView(BatoudeCheckPermMixin, ListView):
    template_name = 'batoude/doc_list.html'
    model = Document

    def dispatch(self, request, *args, **kwargs):
        self.person = get_object_or_404(Beneficiaire, pk=kwargs['pk'])
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return self.person.documents.all()

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'person': self.person,
            'categories': dict(Document.CATEGORIE_CHOICES),
        }


class DocumentUploadView(BatoudeCheckPermMixin, CreateView):
    template_name = 'carrefour/document_upload.html'
    form_class = DocumentUploadForm
    titre_page = 'Documents de {person}'
    titre_formulaire = 'Nouveau document'

    def dispatch(self, request, *args, **kwargs):
        self.person = get_object_or_404(Beneficiaire, pk=kwargs['pk'])
        self.titre_page = self.titre_page.format(person=self.person.nom_prenom())

        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('batoude-doc-list', args=[self.person.pk])

    def get_initial(self):
        return {
            **super().get_initial(),
            'person': self.person,
            'categorie': self.request.GET.get('cat', '')
        }

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'user': self.request.user}


class DocumentDeleteView(DeleteView):
    model = Document
    pk_url_kwarg = 'doc_pk'

    def dispatch(self, request, *args, **kwargs):
        if request.user != self.get_object().auteur:
            raise PermissionDenied(MSG_NO_ACCESS)
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        user_dir = Path(self.get_object().fichier.path).parent
        response = super().delete(self.request)
        messages.success(self.request, "Le document a été supprimé avec succès")
        if not any(user_dir.iterdir()):
            os.rmdir(user_dir)
        return response

    def get_success_url(self):
        return reverse('batoude-doc-list', args=[self.object.person.pk])


class BilanBaseView(BatoudeCheckPermMixin):
    template_name = 'batoude/bilan_change.html'
    model = Bilan
    form_class = BilanForm

    def dispatch(self, request, *args, **kwargs):
        self.person = Beneficiaire.objects.get(pk=kwargs['pk'])
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        return {**super().get_context_data(**kwargs), 'person': self.person}


class BilanCreateView(BilanBaseView, CreateView):

    def get_initial(self):
        return {
            **super().get_initial(),
            'date_bilan': date.today(),
            'present_batoude': ', '.join([u.nom_prenom for u in Utilisateur.intervenants_batoude()]),
            'present_ope': self.person.get_referents_ope(),
            'typ': self.kwargs['typ'],
        }

    def form_valid(self, form):
        form.instance.beneficiaire = self.person
        form.save()
        return HttpResponseRedirect(reverse('batoude-person-agenda', args=[self.person.pk]))


class BilanEditView(BilanBaseView, UpdateView):
    pk_url_kwarg = 'obj_pk'

    def get_success_url(self):
        return reverse('batoude-person-agenda', args=[self.person.pk])


class BilanPrintView(BatoudeCheckPermMixin, BasePDFView):
    obj_class = Bilan
    pdf_class = BilanPdf

    def get_object(self, queryset=None):
        return get_object_or_404(Bilan, pk=self.kwargs['obj_pk'])


class MedicationUpdateView(BatoudeCheckPermMixin, UpdateView):
    template_name = 'batoude/medication_edit.html'
    model = Beneficiaire
    form_class = MedicationForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'person': get_object_or_404(Beneficiaire, pk=self.kwargs['pk']),
        })
        return context

    def get_success_url(self):
        return reverse('batoude-person-edit', args=[self.kwargs['pk']])
