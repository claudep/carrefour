from django.contrib import admin

from .models import Beneficiaire, Bilan, MembreFamille, PrestationBatoude


class TypePrestationFilter(admin.SimpleListFilter):
    title = 'Prest. perso./générales'
    parameter_name = 'prest'
    default_value = None

    def lookups(self, request, model_admin):
        return (
            ('perso', 'Personnelles'),
            ('gen', 'Générales'),
        )

    def queryset(self, request, queryset):
        value = self.value()
        if value == 'perso':
            return queryset.filter(beneficiaire__isnull=False)
        elif value == 'gen':
            return queryset.filter(beneficiaire__isnull=True)
        return queryset


@admin.register(PrestationBatoude)
class PrestationBatoudeAdmin(admin.ModelAdmin):
    list_filter = (TypePrestationFilter,)
    search_fields = ('beneficiaire__nom',)


admin.site.register(Beneficiaire)
admin.site.register(MembreFamille)
admin.site.register(Bilan)
