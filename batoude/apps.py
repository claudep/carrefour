from django.apps import AppConfig


class BatoudeConfig(AppConfig):
    name = 'batoude'

    @property
    def benef_model(self):
        from .models import Beneficiaire
        return Beneficiaire
