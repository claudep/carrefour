from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('carrefour', '0035_contact_unique_constraint'),
        ('batoude', '0016_Rpl_fk_contact_protect'),
    ]

    operations = [
        migrations.AlterField(
            model_name='beneficiaire',
            name='referent_ope',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='suivisbatoude', to='carrefour.contact'),
        ),
    ]
