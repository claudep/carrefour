from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('batoude', '0023_Nouveaux_champs_batoude'),
    ]

    operations = [
        migrations.CreateModel(
            name='Bilan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('typ', models.CharField(choices=[('demande', 'Bilan de la demande'), ('admission', 'Bilan d’admission'), ('autonomisation', "Bilan de l'autonomisation"), ('prolongation', 'Bilan de la prolongation'), ('final', 'Bilan final')], max_length=20, verbose_name='Type de bilan')),
                ('date_bilan', models.DateField(verbose_name='Date du bilan')),
                ('present_batoude', models.CharField(blank=True, max_length=250, verbose_name='Interv. Batoude')),
                ('present_ope', models.CharField(blank=True, max_length=250, verbose_name='Interv. OPE')),
                ('present_famille', models.CharField(blank=True, max_length=250, verbose_name='Membres de la famille')),
                ('present_autres', models.CharField(blank=True, max_length=250, verbose_name='Autres pers. présentes')),
                ('texte', models.TextField(blank=True, verbose_name='Bilan')),
                ('beneficiaire', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='bilans', to='batoude.beneficiaire', verbose_name='Bénéficiaire')),
            ],
        ),
    ]
