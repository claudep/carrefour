# Generated by Django 2.2.1 on 2019-07-25 09:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('batoude', '0002_Ajout_table_prestation'),
    ]

    operations = [
        migrations.AddField(
            model_name='beneficiaire',
            name='archive',
            field=models.BooleanField(default=False),
        ),
    ]
