from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('batoude', '0021_alter_prestationbatoude_lib_prestation'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='beneficiaire',
            name='archived',
        ),
        migrations.AddField(
            model_name='beneficiaire',
            name='archived_at',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Archivée le'),
        ),
    ]
