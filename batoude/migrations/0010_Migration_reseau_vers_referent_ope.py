from django.db import migrations


def apply_migration(apps, schema_editor):
    """
    Déplace le contact OPE du champ Reseau vers le champ referent _ope
    """
    Beneficiaire = apps.get_model('batoude', 'Beneficiaire')

    for person in Beneficiaire.objects.all():
        for contact in person.reseau.filter(role__nom='OPE'):
            person.reseau.remove(contact)
            person.referent_ope = contact
            person.save()
            break


class Migration(migrations.Migration):

    dependencies = [
        ('batoude', '0009_Add_batoude_referent_ope'),
    ]

    operations = [
        migrations.RunPython(apply_migration)
    ]
