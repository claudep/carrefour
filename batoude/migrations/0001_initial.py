from django.conf import settings
import common.fields
from common.choices import MANDATS_OPE_CHOICES
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('carrefour', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Beneficiaire',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=40, verbose_name='Nom')),
                ('prenom', models.CharField(max_length=40, verbose_name='Prénom')),
                ('naissance', models.DateField(blank=True, default=None, null=True, verbose_name='Date de naissance')),
                ('telephone', models.CharField(blank=True, max_length=40, verbose_name='Téléphone')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='Courriel')),
                ('genre', models.CharField(choices=[('M', 'M'), ('F', 'F')], default='M', max_length=1, verbose_name='Genre')),
                ('rue', models.CharField(blank=True, max_length=30, verbose_name='Rue')),
                ('npa', models.CharField(blank=True, max_length=4, verbose_name='NPA')),
                ('localite', models.CharField(blank=True, max_length=30, verbose_name='Localité')),
                ('permis', models.CharField(blank=True, max_length=30, verbose_name='Nationalité, Permis/séjour')),
                ('validite', models.DateField(blank=True, null=True, verbose_name='Date validité')),
                ('emploi', models.CharField(blank=True, max_length=40, verbose_name='Emploi actuel')),
                ('remarque', models.CharField(blank=True, max_length=200, verbose_name='Remarque')),
                ('rue_actuelle', models.CharField(blank=True, max_length=30, verbose_name='Rue act.')),
                ('npa_actuelle', models.CharField(blank=True, max_length=4, verbose_name='NPA act.')),
                ('localite_actuelle', models.CharField(blank=True, max_length=30, verbose_name='Localité act.')),
                ('caisse_mal_acc', models.CharField(blank=True, max_length=50, verbose_name='Caisse maladie-acc.')),
                ('no_assure_lamal', models.CharField(blank=True, max_length=30, verbose_name="No d'assuré")),
                ('assurance_rc', models.CharField(blank=True, max_length=50, verbose_name='Assurance RC')),
                ('no_avs', models.CharField(blank=True, max_length=16, verbose_name='N° AVS')),
                ('loisirs', models.TextField(blank=True, verbose_name='Loisirs/passions')),
                ('scolarite', models.TextField(blank=True, verbose_name='Situation scolaire')),
                ('autorite_parentale', models.CharField(blank=True, choices=[('conjointe', 'Conjointe'), ('pere', 'Père'), ('mere', 'Mère'), ('tutelle', 'Tutelle')], max_length=20, verbose_name='Autorité parentale')),
                ('statut_marital', models.CharField(blank=True, choices=[('celibat', 'Célibataire'), ('mariage', 'Marié'), ('pacs', 'PACS'), ('concubin', 'Concubin'), ('veuf', 'Veuf'), ('separe', 'Separé'), ('divorce', 'Divorcé')], max_length=20, verbose_name='Statut marital')),
                ('statut_financier', models.CharField(blank=True, choices=[('ai', 'AI PC'), ('gsr', 'GSR'), ('revenu', 'Revenu')], max_length=20, verbose_name='Statut financier')),
                ('monoparentale', models.BooleanField(blank=True, null=True, verbose_name='Famille monoparent.')),
                ('mandat_ope', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=MANDATS_OPE_CHOICES, max_length=65), blank=True, null=True, size=None, verbose_name='Mandat OPE')),
                ('provenance', models.CharField(blank=True, choices=[('famille', 'Famille'), ('ies-ne', 'IES-NE'), ('ies-hc', 'IES-HC'), ('aemo', 'AEMO'), ('fah', "Famille d'accueil"), ('autre', 'Autre')], max_length=30, verbose_name='Provenance av. admission')),
                ('destination', models.CharField(blank=True, choices=[('famille', 'Famille'), ('ies-ne', 'IES-NE'), ('ies-hc', 'IES-HC'), ('aemo', 'AEMO'), ('fah', "Famille d'accueil"), ('autre', 'Autre')], max_length=30, verbose_name='Destination après suivi')),
                ('date_admission', models.DateField(blank=True, default=None, null=True, verbose_name="Date d'admission")),
                ('date_sortie', models.DateField(blank=True, default=None, null=True, verbose_name='Date de sortie')),
                ('reseau', models.ManyToManyField(to='carrefour.Contact')),
            ],
            options={
                'abstract': False,
                'ordering': ('nom', 'prenom'),
                'permissions': (('view_batoude', 'Gérer les dossiers BATOUDE'), ('manage_batoude', 'RE BATOUDE')),
                'verbose_name': 'Bénéficiaire BATOUDE',
                'verbose_name_plural': 'Bénéficiaires BATOUDE',
            },
        ),
        migrations.CreateModel(
            name='MembreFamille',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=40, verbose_name='Nom')),
                ('prenom', models.CharField(max_length=40, verbose_name='Prénom')),
                ('naissance', models.DateField(blank=True, default=None, null=True, verbose_name='Date de naissance')),
                ('telephone', models.CharField(blank=True, max_length=40, verbose_name='Téléphone')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='Courriel')),
                ('genre', models.CharField(choices=[('M', 'M'), ('F', 'F')], default='M', max_length=1, verbose_name='Genre')),
                ('rue', models.CharField(blank=True, max_length=30, verbose_name='Rue')),
                ('npa', models.CharField(blank=True, max_length=4, verbose_name='NPA')),
                ('localite', models.CharField(blank=True, max_length=30, verbose_name='Localité')),
                ('permis', models.CharField(blank=True, max_length=30, verbose_name='Nationalité, Permis/séjour')),
                ('validite', models.DateField(blank=True, null=True, verbose_name='Date validité')),
                ('emploi', models.CharField(blank=True, max_length=40, verbose_name='Emploi actuel')),
                ('remarque', models.CharField(blank=True, max_length=200, verbose_name='Remarque')),
                ('role', models.CharField(choices=[('pere', 'père'), ('mere', 'mère'), ('fratrie', 'fratrie'), ('autre', 'autre')], max_length=40, verbose_name='Degré parenté')),
                ('activite', models.TextField(blank=True, verbose_name='Activité')),
                ('beneficiaire', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT,
                                                   related_name='membres_famille', to='batoude.Beneficiaire')),
            ],
            options={
                'abstract': False,
                'verbose_name': 'Membre famille BATOUDE',
                'verbose_name_plural': 'Membres familles BATOUDE',
            },
        ),
        migrations.CreateModel(
            name='JournalBatoude',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('theme', models.CharField(max_length=100, verbose_name='thème')),
                ('date', models.DateTimeField(verbose_name='date/heure')),
                ('texte', models.TextField(verbose_name='contenu')),
                ('auteur', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL,
                                             verbose_name='auteur')),
                ('beneficiaire', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='journaux',
                                                   to='batoude.Beneficiaire', verbose_name='Bénéficiaire')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
