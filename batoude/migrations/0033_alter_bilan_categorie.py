# Generated by Django 5.0.6 on 2024-07-26 14:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('batoude', '0032_alter_document_categorie'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bilan',
            name='categorie',
            field=models.CharField(blank=True, choices=[('education', 'Éducation'), ('famille', 'Famille'), ('atelier', 'Atelier'), ('sante', 'Santé'), ('autre', 'Autre')], max_length=15, verbose_name='Catégorie'),
        ),
    ]
