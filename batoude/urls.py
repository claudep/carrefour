from django.urls import path

from batoude import views

urlpatterns = [
    path('person/list/', views.BeneficiaireListView.as_view(), name='batoude-person-list'),
    path('person/add/', views.BeneficiaireCreateView.as_view(), name='batoude-person-add'),
    path('person/<int:pk>/edit/', views.BeneficiaireEditView.as_view(), name='batoude-person-edit'),
    path('person/<int:pk>/journal/', views.JournalListView.as_view(), name='batoude-journal-list'),
    path('person/<int:pk>/famille/list/', views.MembreFamilleListView.as_view(), name='batoude-famille-list'),
    path('person/<int:pk>/famille/add/', views.MembreFamilleCreateView.as_view(), name='batoude-famille-add'),
    path('person/<int:pk>/famille/<int:obj_pk>/edit/', views.MembreFamilleUpdateView.as_view(),
         name='batoude-famille-edit'),
    path('person/<int:pk>/assurance/edit/', views.AssuranceUpdateView.as_view(),
         name='batoude-assurance-edit'),
    path('person/<int:pk>/medic/edit/', views.MedicationUpdateView.as_view(),name='batoude-medication-edit'),
    path('suivis_termines/', views.SuivisTerminesListView.as_view(), name='batoude-suivis-termines'),
    # Prestations
    path('prestation/menu/', views.PrestationMenuView.as_view(), name='batoude-prestation-menu'),
    path('person/<int:pk>/prestation/list/', views.PrestationListView.as_view(), name='batoude-prestation-list'),
    path('person/<int:pk>/prestation/add/', views.PrestationCreateView.as_view(), name='batoude-prestation-add'),
    path('person/<int:pk>/prestation/<int:obj_pk>/edit/', views.PrestationUpdateView.as_view(),
         name='batoude-prestation-edit'),
    path('person/<int:pk>/prestation/<int:obj_pk>/delete/', views.PrestationDeleteView.as_view(),
         name='batoude-prestation-delete'),
    path('prestation_gen/list/', views.PrestationListView.as_view(), name='batoude-prestation-gen-list'),
    path('prestation_gen/add/', views.PrestationCreateView.as_view(), name='batoude-prestation-gen-add'),
    path('prestation/recap-perso/', views.PrestationRecapPersoListView.as_view(),
         name='batoude-prestation-recap-perso'),

    path('prestations/list/', views.AdminPrestationListView.as_view(),
         name='batoude-prestation-adminlist'),
    path('person/<int:pk>/contact/', views.ContactListView.as_view(), name='batoude-contact-list'),
    path('person/<int:pk>/contact/<int:obj_pk>/remove/', views.ContactRemoveView.as_view(),
         name='batoude-contact-remove'),
    path('person/<int:pk>/contact/add/', views.ContactAddView.as_view(), name='batoude-contact-add'),
    path('person/<int:pk>/contact/<int:obj_pk>/edit/', views.ContactUpdateView.as_view(), name='batoude-contact-edit'),

    path('person/<int:pk>/print-coord/', views.CoordPDFView.as_view(), name='batoude-print-coordinates'),
    path('person/<int:pk>/print-journal/', views.JournalPDFView.as_view(), name='batoude-print-journal'),

    path('person/<int:pk>/agenda/', views.AgendaUpdateView.as_view(), name='batoude-person-agenda'),

    path('person/<int:pk>/doc/list/', views.DocumentListView.as_view(), name='batoude-doc-list'),
    path('person/<int:pk>/doc/upload/', views.DocumentUploadView.as_view(), name='batoude-doc-upload'),
    path('person/<int:pk>/doc/<int:doc_pk>/delete/', views.DocumentDeleteView.as_view(), name='batoude-doc-delete'),

    path('person/<int:pk>/bilan/<str:typ>/add/', views.BilanCreateView.as_view(), name='batoude-bilan-create'),
    path('person/<int:pk>/bilan/<int:obj_pk>/edit/', views.BilanEditView.as_view(), name='batoude-bilan-edit'),
    path('person/<int:pk>/bilan/<int:obj_pk>/print/', views.BilanPrintView.as_view(), name='batoude-bilan-print'),
]
