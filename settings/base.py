import os
from ipaddress import IPv4Network
from django.contrib.messages import constants as messages

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DEBUG = False

ALLOWED_HOSTS = ['localhost']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'carrefour',
    }
}

# Application definition

INSTALLED_APPS = [
    'dal',  # dal: django-auto-complete
    'dal_select2',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.postgres',

    'django_otp',
    'django_otp.plugins.otp_totp',
    'django_otp.plugins.otp_static',
    'two_factor',

    'bootstrap_datepicker_plus',
    'tinymce',
    # Our apps:
    'carrefour',
    'aemo',
    'asaef',
    'batoude',
    'ser',
    'archive',
    'sondage',

    'django.forms',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.http.ConditionalGetMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django_otp.middleware.OTPMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'common.middleware.LoginRequiredMiddleware',
]

ROOT_URLCONF = 'common.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
                'carrefour.context_processors.current_app',
            ],
            'libraries': {
                'my_tags': 'carrefour.templatetags.my_tags',
            },
            'builtins': [
                'carrefour.templatetags.my_tags', 'django.templatetags.static',
            ],
        },

    },
]

FORM_RENDERER = 'django.forms.renderers.TemplatesSetting'

WSGI_APPLICATION = 'common.wsgi.application'

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTH_USER_MODEL = 'carrefour.Utilisateur'

LOGIN_URL = 'two_factor:login'
LOGIN_REDIRECT_URL = '/'
EXEMPT_2FA_NETWORKS = [IPv4Network('127.0.0.0/30')]

# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = 'fr'

TIME_ZONE = 'Europe/Zurich'

USE_I18N = True

USE_TZ = True

FORMAT_MODULE_PATH = ['common.formats']

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

SESSION_EXPIRE_AT_BROWSER_CLOSE = True

TINYMCE_DEFAULT_CONFIG = {
    'height': 360,
    'width': '100%',
    'language': 'fr_FR',
    'cleanup_on_startup': True,
    'entity_encoding': 'raw',
    'custom_undo_redo_levels': 20,
    'browser_spellcheck': True,
    'theme': 'silver',
    'plugins': 'lists fullscreen searchreplace fullscreen autolink paste',
    'toolbar1': 'preview bold italic underline paste',
    'contextmenu': 'formats | link image',
    'menubar': False,
    'statusbar': True,
    # Keep the deprecated <u> tag for reportlab compatibility
    # https://gitlab.com/claudep/carrefour/-/issues/325
    'formats': {
        'underline': {'inline': 'u', 'exact': True},
    }
}

MESSAGE_TAGS = {
    messages.DEBUG: 'alert-info',
    messages.INFO: 'alert-info',
    messages.SUCCESS: 'alert-success',
    messages.WARNING: 'alert-warning',
    messages.ERROR: 'alert-danger',
}

# Path to public key to encrypt archives
CARREFOUR_RSA_PUBLIC_KEY = ''
