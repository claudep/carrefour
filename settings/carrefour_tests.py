from settings.carrefour import *  # noqa

SECRET_KEY = 'ForAutomatedTestsC9g'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'ci_test',
        'USER': 'runner',
        'PASSWORD': '',
        'HOST': 'postgres',
        'PORT': '5432',
    }
}
