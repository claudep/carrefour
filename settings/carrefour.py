from settings.base import *  # NOQA

DIRECTOR = ''
PERMANENCE_TEL_ASAEF = ''
HEURES_MENSUELLES = 120  # soit 1440 heures annuelles (80% de 1800)
CARREFOUR_NOM = "Fondation Carrefour"
CARREFOUR_CONTACT = "Rue de Neuchâtel 34 - 2034 Peseux - tél. 032 886 88 98 - fondation.carrefour@ne.ch"
