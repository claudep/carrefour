from django.db.models import TextChoices


AUTORITE_PARENTALE_CHOICES = (
    ('conjointe', 'Conjointe'),
    ('pere', 'Père'),
    ('mere', 'Mère'),
    ('tutelle', 'Tutelle')
)


DEMARCHE_CHOICES = (
    ('volontaire', 'Volontaire'),
    ('contrainte', 'Contrainte'),
    ('post_placement', 'Post placement'),
    ('non_placement', 'Eviter placement')
)


MANDATS_OPE_CHOICES = (
    ('volontaire', 'Mandat volontaire'),
    ('curatelle', 'Curatelle 308.1'),
    ('curatelle2', 'Curatelle 308.2'),
    ('referent', 'Référent'),
    ('enquete', 'Enquête'),
    ('tutelle', 'Tutelle'),
    ('justice', 'Mandat judiciaire'),
)


MOTIF_DEMANDE_CHOICES = (
    ('parentalite', 'Soutien à la parentalité'),
    ('education', "Aide éducative à l'enfant"),
    ('developpement', "Répondre aux besoins de développement de l'enfant"),
    ('integration', "Soutien à l'intégration sociale")
)


class MotifsFinSuivi(TextChoices):
    DESENGAGEMENT = 'desengagement', 'Désengagement'
    EVOL_POSITIVE = 'evol_positive', 'Evolution positive'
    RELAI = 'relai', 'Relai vers autre service'
    PLACEMENT = 'placement', 'Placement'
    ABANDON_NON_ABOUTIE = 'non_aboutie', 'Demande non aboutie'
    ERREUR = 'erreur', 'Erreur de saisie'
    AUTRES = 'autres', 'Autres'
    ABANDON_FAMILLE = 'abandon_famille', 'Refus de la famille'
    ABANDON_AEMO = 'abandon_aemo', 'Refus de l’AEMO'
    ABANDON_ASAEF = 'abandon_asaef', 'Refus de l’ASAEF'
    ABANDON_BATOUDE = 'abandon_batoude', 'Refus de la Batoude'
    ABANDON_MASE = 'abandon_mase', "MASE refusé"
    ABANDON_RELAIS = 'abandon_relais', 'Réorientation'
    ABANDON_INDISPONIBILITE = 'abandon_indisponible', 'Manque de disponibilité'

    @classmethod
    def choices_abandon(cls, unite):
        suppr = [f'abandon_{u}' for u in ['aemo', 'asaef', 'batoude'] if u != unite]
        if unite in ['aemo', 'asaef']:
            suppr.append('abandon_mase')
        choices = [(item.value, item.label) for item in cls if item.value not in suppr]
        return [
            (member[0], member[1]) for member in choices
            if member[0] in ['erreur', 'non_aboutie'] or member[0].startswith("abandon_")
        ]

    @classmethod
    def choices_fin(cls):
        return [
            (member.value, member.label) for member in cls
            if member.value not in ['erreur', 'non_aboutie'] and not member.value.startswith("abandon_")
        ]

    @classmethod
    def choices_stats(cls, unite):
        suppr = [f'abandon_{u}' for u in ['aemo', 'asaef', 'batoude'] if u != unite]
        choices = [(item.value, item.label) for item in cls if item.value not in suppr]
        return [
            (member[0], member[1]) for member in choices if member[0] not in ['erreur']
        ]


PROVENANCE_DESTINATION_CHOICES = (
    ('famille', 'Famille'),
    ('ies-ne', 'IES-NE'),
    ('ies-hc', 'IES-HC'),
    ('aemo', 'AEMO'),
    ('fah', "Famille d'accueil"),
    ('autre', 'Autre'),
)

DESTINATION_BATOUDE_CHOICES = (
    ('famille', 'Famille'),
    ('fah', "Famille d'accueil"),
    ('ies-ne', 'IES-NE'),
    ('ies-hc', 'IES-HC'),
    ('independant', 'Indépendant'),
    ('aide_sociale', "Aide sociale"),
    ('autre', 'Autre'),
)


SERVICE_ORIENTEUR_CHOICES = (
    ('famille', 'Famille'),
    ('ope', 'OPE'),
    ('spe', 'SPE'),
    ('cnpea', 'CNPea'),
    ('ecole', 'École'),
    ('res_prim', 'Réseau primaire'),
    ('res_sec', 'Réseau secondaire'),
    ('autre', 'Autre'),
)

SERVICE_ORIENTEUR_ASAEF_CHOICES = (
    ('ope', 'OPE'),
    ('opec', 'OPEC'),
    ('open', 'OPEN'),
    ('opelovdt', 'OPELOVDT'),
)

STATUT_FINANCIER_CHOICES = (
    ('ai', 'AI PC'),
    ('gsr', 'GSR'),
    ('revenu', 'Revenu')
)


STATUT_MARITAL_CHOICES = (
    ('celibat', 'Célibataire'),
    ('mariage', 'Marié'),
    ('pacs', 'PACS'),
    ('concubin', 'Concubin'),
    ('veuf', 'Veuf'),
    ('separe', 'Séparé'),
    ('divorce', 'Divorcé')
)

UNITE_CARREFOUR_CHOICES = (
    ('aemo', 'AEMO'),
    ('asaef', 'ASAEF'),
    ('batoude', 'La Batoude'),
    ('ser', 'ASAP')
)

REGIONS_CHOICES = (
    ('mont_locle', 'Le Locle'),
    ('mont_pms', 'Ponts-de-Martel/Sagne'),
    ('mont_cdf', 'La Chaux-de-Fonds'),
    ('litt_est', 'Littoral Est'),
    ('litt_ouest', 'Littoral Ouest'),
    ('litt_ntel', 'Neuchâtel Centre'),
    ('litt_vdr', 'Val-de-Ruz'),
    ('litt_vdt', 'Val-de-Travers'),
)

TYPE_INTERVENTION_CHOICES = (
    ('tshm', 'TSHM'),
    ('tshm_bip', 'TSHM BIP'),
    ('tshm_libre', 'THSM Libre'),
    ('permanence', 'Permanence'),
    ('ecole', 'École'),
    ('formation', 'Dans le cadre de formation'),
    ('manifestation', 'Manifestation'),
)

SCORE_CHOICES = (
    (6, '6 Très bien'),
    (5, '5 Bien'),
    (4, '4 Suffisant'),
    (3, '3 Insuffisant'),
    (2, '2 Très insuffisant'),
)

ASAP_MOTIF_DEMANDE = (
    ('emploi', 'Recherche emploi/formation'),
    ('psy', "Soutien psycho-social"),
    ('admin', "Aide administrative"),
    ('migration', 'Migration'),
    ('projet', 'Projet'),
    ('autre', 'Autres'),
)

ASAP_SERVICE_ORIENTEUR = (
    ('ope', 'OPE'),
    ('opa', 'OPA'),
    ('justice', 'Justice'),
    ('ecole', 'Ecole obligatoire'),
    ('post_obl', 'Ecole post-obligatoire'),
    ('soc', 'Aide sociale'),
    ('resident', 'Domaine résidentiel'),
    ('anim', "Centre d'anim./Centre de jeunesse"),
    ('famille', 'Réseau familial/amical'),
    ('tshm', 'TSHM'),
    ('autre', 'Autre'),
)
