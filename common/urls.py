from django.apps import apps
from django.conf import settings
from django.contrib import admin
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path, include
from django.views.decorators.cache import cache_page
from django.views.i18n import JavaScriptCatalog
from django.views.static import serve

from two_factor.urls import urlpatterns as tf_urls

from carrefour import views

urlpatterns = [
    path('', views.HomeView.as_view(), name='home'),
    path('account/password_change/', views.PasswordChangeView.as_view(), name='password_change'),
    # Overriden 2FA path:
    path('account/two_factor/setup/', views.SetupView.as_view(), name='setup'),
    path('', include(tf_urls)),
    # Standard login is still permitted depending on IP origin
    path('login/', LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', LogoutView.as_view(next_page='/account/login'), name='logout'),
    path('jsi18n/', cache_page(86400, key_prefix='jsi18n-1')(JavaScriptCatalog.as_view()),
         name='javascript-catalog'),

    path('', include('carrefour.urls')),
    path('aemo/', include('aemo.urls')),
    path('asaef/', include('asaef.urls')),
    path('batoude/', include('batoude.urls')),
    path('archive/', include('archive.urls')),
    path('asap/', include('ser.urls')),
    path('stats/', include('stats.urls')),
    path('sondage/', include('sondage.urls')),
    path('admin/', admin.site.urls),
    path('tinymce/', include('tinymce.urls')),
    path('media/<path:path>', serve,
         {'document_root': settings.MEDIA_ROOT, 'show_indexes': False}),
]

if apps.is_installed('debug_toolbar'):
    urlpatterns.append(path('__debug__/', include('debug_toolbar.urls')))
