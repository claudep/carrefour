import shutil
import tempfile
from io import BytesIO

from pypdf import PdfReader

from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.webdriver import WebDriver as ChromeDriver
from selenium.webdriver.firefox.webdriver import WebDriver

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import tag
from django.urls import reverse


class TempMediaRootMixin:
    @classmethod
    def setUpClass(cls):
        cls._temp_media = tempfile.mkdtemp()
        cls._overridden_settings = cls._overridden_settings or {}
        cls._overridden_settings['MEDIA_ROOT'] = cls._temp_media
        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(cls._temp_media)
        super().tearDownClass()


class TestUtilsMixin:
    def extract_page_pdf(self, response, num_page):
        pdf_reader = PdfReader(BytesIO(response.getvalue()))
        page = pdf_reader.pages[num_page]
        return page.extract_text()


@tag("selenium")
class LiveTestBase(StaticLiveServerTestCase):
    driver = 'firefox'
    # driver = 'chromium'

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        if cls.driver == 'firefox':
            import geckodriver_autoinstaller
            geckodriver_autoinstaller.install()
            cls.selenium = WebDriver()
        elif cls.driver == 'chromium':
            import chromedriver_autoinstaller
            chromedriver_autoinstaller.install()
            cls.selenium = ChromeDriver()
        cls.selenium.implicitly_wait(5)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def login(self):
        self.selenium.get('%s%s' % (self.live_server_url, reverse('login')))
        username_input = self.selenium.find_element(By.NAME, value="username")
        username_input.send_keys(self.user.username)
        password_input = self.selenium.find_element(By.NAME, value="password")
        password_input.send_keys('mepassword')
        self.selenium.find_element(By.XPATH, value='//button[@type="submit"]').click()
