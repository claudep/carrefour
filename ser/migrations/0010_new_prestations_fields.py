import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ser', '0009_Add_change_permission'),
    ]

    operations = [
        migrations.AddField(
            model_name='prestationser',
            name='duree_admin',
            field=models.DurationField(default=datetime.timedelta(0), verbose_name='Travaux. admin.'),
        ),
        migrations.AddField(
            model_name='prestationser',
            name='duree_projet',
            field=models.DurationField(default=datetime.timedelta(0), verbose_name='Projet et intervention'),
        ),
        migrations.AddField(
            model_name='prestationser',
            name='duree_reseau',
            field=models.DurationField(default=datetime.timedelta(0), verbose_name='Rencontre réseau'),
        ),
        migrations.AddField(
            model_name='prestationser',
            name='duree_suivi',
            field=models.DurationField(default=datetime.timedelta(0), verbose_name='Suivi et jeunes'),
        ),
    ]
