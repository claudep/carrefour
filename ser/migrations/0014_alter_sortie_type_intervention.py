import common.fields
from django.db import migrations, models


def renommer_permanance_en_permanence(apps, schema_editor):
    """
    du latin «permanEre» et non pas «permanare» !
    """
    Sortie = apps.get_model('ser', 'Sortie')

    for sortie in Sortie.objects.filter(type_intervention__contains=['permanance']):
        type_inter = [item.replace('permanance', 'permanence') for item in sortie.type_intervention]
        sortie.type_intervention = type_inter
        sortie.save()


class Migration(migrations.Migration):

    dependencies = [
        ('ser', '0013_Add_document_model'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sortie',
            name='type_intervention',
            field=common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=[
                ('tshm', 'TSHM'), ('tshm_bip', 'TSHM BIP'), ('tshm_libre', 'THSM Libre'),
                ('permanence', 'Permanence'), ('ecole', 'École'),
                ('formation', 'Dans le cadre de formation'), ('manifestation', 'Manifestation')
            ], max_length=65), blank=True, null=True, size=None, verbose_name='Type d’intervention'),
        ),
        migrations.RunPython(renommer_permanance_en_permanence),
    ]
