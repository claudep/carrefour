# Generated by Django 3.1 on 2021-02-11 08:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ser', '0008_add_JournalSer_date'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='person',
            options={'ordering': ('prenom',), 'permissions': (('view_ser', 'Consulter les dossiers SER'), ('manage_ser', 'RE SER'), ('change_ser', 'Gérer les dossiers du SER')), 'verbose_name': 'Bénéf. SER', 'verbose_name_plural': 'Bénéf. SER'},
        ),
    ]
