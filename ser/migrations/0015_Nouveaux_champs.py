import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ser', '0014_alter_sortie_type_intervention'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='prestationser',
            options={'verbose_name': 'Prestation ASAP', 'verbose_name_plural': 'Prestations ASAP'},
        ),
        migrations.AlterModelOptions(
            name='sortie',
            options={'verbose_name': 'Sortie du BIP', 'verbose_name_plural': 'Sorties du BIP'},
        ),
        migrations.AlterUniqueTogether(
            name='prestationser',
            unique_together=set(),
        ),
        migrations.AddField(
            model_name='journalser',
            name='duree',
            field=models.DurationField(default=datetime.timedelta(0), verbose_name='Durée de la prest.'),
        ),
        migrations.AddField(
            model_name='prestationser',
            name='duree_prestation',
            field=models.DurationField(blank=True, default=datetime.timedelta(0), verbose_name='Durée'),
        ),
        migrations.AddField(
            model_name='prestationser',
            name='motif',
            field=models.CharField(blank=True, choices=[('bulle', 'Bulle Horaire'), ('pe', 'Point Equipe'), ('autre', 'Autre')], verbose_name='Motif'),
        ),
        migrations.AddField(
            model_name='sortie',
            name='nbre_non_binaires',
            field=models.IntegerField(blank=True, default=0, verbose_name='personnes non binaires'),
        ),
        migrations.AlterField(
            model_name='sortie',
            name='nbre_filles',
            field=models.IntegerField(blank=True, default=0, verbose_name='jeunes femmes'),
        ),
        migrations.AlterField(
            model_name='sortie',
            name='nbre_garcons',
            field=models.IntegerField(blank=True, default=0, verbose_name='jeunes hommes'),
        ),
    ]
