from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('ser', '0010_new_prestations_fields'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='journalser',
            options={'verbose_name': 'Journal ASAP', 'verbose_name_plural': 'Journaux ASAP'},
        ),
        migrations.AlterModelOptions(
            name='person',
            options={'ordering': ('prenom',), 'permissions': (('view_ser', 'Consulter les dossiers ASAP'), ('manage_ser', 'RE ASAP'), ('change_ser', 'Gérer les dossiers de l’ASAP')), 'verbose_name': 'Bénéf. ASAP', 'verbose_name_plural': 'Bénéf. ASAP'},
        ),
        migrations.AlterModelOptions(
            name='prestationser',
            options={'ordering': ('-date',), 'verbose_name': 'Prestation ASAP', 'verbose_name_plural': 'Prestations ASAP'},
        ),
        migrations.AlterField(
            model_name='person',
            name='educs',
            field=models.ManyToManyField(blank=True, related_name='beneficiaires_suivis', to=settings.AUTH_USER_MODEL, verbose_name='Éduc. ASAP'),
        ),
    ]
