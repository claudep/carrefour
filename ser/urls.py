from django.urls import path

from . import views

urlpatterns = [
    path('person/list/', views.PersonListView.as_view(), name='asap-person-list'),
    path('person/enveille/list/', views.PersonEnVeilleListView.as_view(), name='asap-person-veille'),
    path('person/add/', views.PersonCreateView.as_view(), name='asap-person-add'),
    path('person/<int:pk>/change/', views.PersonUpdateView.as_view(), name='asap-person-change'),
    path('person/<int:pk>/delete/', views.PersonDeleteView.as_view(), name='asap-person-delete'),

    path('person/<int:pk>/journal/', views.JournalListView.as_view(), name='asap-journal-list'),
    path('person/<int:pk>/journal/add/', views.JournalCreateView.as_view(), name='asap-journal-add'),
    path('person/<int:pk>/journal/<int:obj_pk>/edit/', views.JournalUpdateView.as_view(), name='asap-journal-change'),

    path('person/<int:pk>/upload/', views.DocumentUploadView.as_view(), name='person-doc-upload'),
    path('person/<int:pk>/doc/<int:doc_pk>/delete/', views.DocumentDeleteView.as_view(), name='person-doc-delete'),

    path('place/list/', views.PlaceListView.as_view(), name='asap-place-list'),
    path('place/add/', views.PlaceCreateView.as_view(), name='asap-place-add'),
    path('place/<int:pk>/change/', views.PlaceUpdateView.as_view(), name='asap-place-change'),
    path('place/<int:pk>/delete/', views.PlaceDeleteView.as_view(), name='asap-place-delete'),

    path('utilisateur/dal/', views.UtilisateurAsapAutocompleteView.as_view(), name='asap-utilisateur-autocomplete'),


    path('prestation/list/', views.PrestationListView.as_view(), name='asap-prestation-list'),
    path('prestation/add/', views.PrestationCreateView.as_view(), name='asap-prestation-add'),
    path('prestation/<int:pk>/change/', views.PrestationUpdateView.as_view(), name='asap-prestation-change'),
    path('prestation/extra/<str:action>/add/', views.PrestationExtraAddView.as_view(),
         name='asap-prestation-extra-add'),
    path('prestation/<int:pk>/delete/', views.PrestationDeleteView.as_view(), name='asap-prestation-delete'),
    path('tshm/create/', views.TshmCreateView.as_view(), name="asap-tshm-add"),
    path('tshm/<int:pk>/change/', views.TshmUpdateView.as_view(), name="asap-tshm-change"),
    path('tshm/list/', views.TshmListView.as_view(), name='asap-tshm-list'),

]
