import os
from datetime import date, timedelta
from pathlib import Path

from dal import autocomplete

from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from django.db.models import Sum, DurationField
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.views.generic import (
    CreateView, DeleteView, DetailView, ListView, TemplateView, UpdateView, View
)

from carrefour.models import Utilisateur
from carrefour.utils import get_selected_date
from carrefour.views import MSG_NO_ACCESS, MSG_READ_ONLY
from .forms import (
    DocumentUploadForm, JournalChangeForm, PersonChangeForm, PersonFilterForm, PlaceChangeForm, PrestationChangeForm,
    TshmChangeForm
)
from .models import Document, Journal, Person, Place, Prestation, Tshm


class AsapCheckPermMixin:
    def dispatch(self, request, *args, **kwargs):
        if not Person.check_perm_view(request.user):
            raise PermissionDenied(MSG_NO_ACCESS)
        if 'person' in request.path:
            self.person = get_object_or_404(Person, pk=kwargs['pk']) if 'pk' in self.kwargs else None
        else:
            self.person = None
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.person:
            can_edit = self.person.can_edit(self.request.user)
            if not can_edit:
                messages.info(self.request, MSG_READ_ONLY)
            context['can_edit'] = can_edit
            context['famille'] = self.person
        else:
            can_edit = self.request.user.has_perm('ser.change_ser')
            if not can_edit:
                messages.info(self.request, MSG_READ_ONLY)
            context['can_edit'] = can_edit
        return context


class AsapCheckOwnerMixin:
    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        owner = obj.auteur if isinstance(obj, Prestation) else None
        if owner == self.request.user:
            return super().dispatch(request, *args, **kwargs)
        raise PermissionDenied()


class PersonCreateView(AsapCheckPermMixin, CreateView):
    model = Person
    template_name = 'ser/person_change.html'
    form_class = PersonChangeForm

    def dispatch(self, request, *args, **kwargs):
        if request.user.has_perm('ser.change_ser'):
            return super().dispatch(request, *args, **kwargs)
        raise PermissionDenied(MSG_NO_ACCESS)

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(reverse('asap-person-list'))


class PersonDeleteView(AsapCheckPermMixin, DeleteView):
    model = Person


class PersonListView(AsapCheckPermMixin, ListView):
    model = Person
    template_name = 'ser/person_list.html'
    context_object_name = 'persons'
    titre = 'actifs'

    def get(self, request, *args, **kwargs):
        self.filter_form = PersonFilterForm(data=self.request.GET or None)
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        beneficiaires = Person.actives()
        if not self.filter_form.is_bound:
            beneficiaires = beneficiaires.filter(educs=self.request.user)
        if self.filter_form.is_bound and self.filter_form.is_valid():
            beneficiaires = self.filter_form.filter(beneficiaires)
        return beneficiaires

    def get_context_data(self, **kwargs):
        return {**super().get_context_data(**kwargs), 'filter_form': self.filter_form}


class PersonEnVeilleListView(AsapCheckPermMixin, ListView):
    model = Person
    template_name = 'ser/person_list.html'
    context_object_name = 'persons'
    titre = 'en veille'

    def get_queryset(self):
        return Person.objects.filter(statut='veille')


class PersonUpdateView(AsapCheckPermMixin, UpdateView):
    model = Person
    template_name = 'ser/person_change.html'
    form_class = PersonChangeForm
    context_object_name = 'person'
    success_url = reverse_lazy('asap-person-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'documents': self.object.documents.order_by('-date_creation')
        })
        return context


class PlaceBaseView(PermissionRequiredMixin):
    permission_required = 'ser.manage_ser'
    model = Place
    success_url = reverse_lazy('asap-place-list')


class PlaceCreateView(PlaceBaseView, CreateView):
    template_name = 'ser/place_change.html'
    form_class = PlaceChangeForm


class PlaceListView(PlaceBaseView, ListView):
    template_name = 'ser/place_list.html'

    def get_queryset(self):
        return super().get_queryset().filter(version=2)


class PlaceUpdateView(PlaceBaseView, UpdateView):
    template_name = 'ser/place_change.html'
    form_class = PlaceChangeForm

    def delete_url(self):
        return reverse('asap-place-delete', args=[self.object.pk])


class PlaceDeleteView(PlaceBaseView, DeleteView):

    def form_valid(self, form):
        if self.object.sorties.exists():
            messages.error(
                self.request,
                "Ce lieu est utilisé dans une sortie. Vous devez d'abord supprimer la sortie concernée."
            )
            return HttpResponseRedirect(self.success_url)
        return super().form_valid(form)


class UtilisateurAsapAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Utilisateur.objects.filter(groups__name='asap')
        if self.q:
            qs = qs.filter(nom__istartswith=self.q)
        return qs


class JournalListView(AsapCheckPermMixin, DetailView):
    template_name = 'ser/journal_list.html'
    model = Person
    context_object_name = 'person'

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'notes': self.object.journaux.order_by('date', 'id')
        }


class JournalCreateView(AsapCheckPermMixin, CreateView):
    template_name = 'ser/journal_change.html'
    model = Journal
    form_class = JournalChangeForm

    def get_initial(self):
        return {'date': timezone.now()}

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'person': self.person
        }

    def form_valid(self, form):
        form.instance.person = self.person
        form.instance.auteur = self.request.user
        form.save()
        return HttpResponseRedirect(reverse('asap-journal-list', args=[self.person.pk]))


class JournalUpdateView(AsapCheckPermMixin, UpdateView):
    template_name = 'ser/journal_change.html'
    form_class = JournalChangeForm
    pk_url_kwarg = 'obj_pk'
    model = Journal
    context_object_name = 'journal'

    def dispatch(self, request, *args, **kwargs):
        journal = get_object_or_404(Journal, pk=kwargs['obj_pk'])
        if journal.auteur != request.user:
            raise PermissionDenied(MSG_NO_ACCESS)
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('asap-journal-list', args=[self.object.person.pk])

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'person': self.object.person,
        }


class PrestationListView(AsapCheckPermMixin, TemplateView):
    model = Prestation
    template_name = 'ser/prestation_list.html'
    start_date = date(2024, 1, 1)  # Month of the first record

    def dispatch(self, request, *args, **kwargs):
        str_curdate = request.GET.get('date', None)
        self.dfrom, self.dto = get_selected_date(str_curdate)
        return super().dispatch(request, *args, **kwargs)

    def get_prest_gen(self):
        prest_gen = Prestation.objects.filter(
            auteur=self.request.user, date__year=self.dfrom.year, date__month=self.dfrom.month, motif='autre'
        )
        self.tot_prest_gen = (
            prest_gen.aggregate(tot=Sum('duree_prestation', output_field=DurationField()))['tot'] or timedelta()
        )
        return prest_gen

    def get_prest_personne(self):
        journaux = Journal.objects.filter(
            auteur=self.request.user, date__year=self.dfrom.year, date__month=self.dfrom.month,
        ).values('person').annotate(total=Sum('duree', output_field=DurationField()))
        self.tot_personnes = timedelta()
        for journal in journaux:
            journal['person'] = Person.objects.get(pk=journal['person'])
            self.tot_personnes += journal['total']
        return journaux

    def get_prest_sorties(self):
        qs = Prestation.objects.filter(
            motif='tshm', auteur=self.request.user, date__year=self.dfrom.year, date__month=self.dfrom.month
        )
        self.tot_sorties = qs.aggregate(tot=Sum('duree_prestation'))['tot'] or timedelta()
        return qs

    def get_prest_equipe(self):
        qs = Prestation.objects.filter(
            auteur=self.request.user, date__year=self.dfrom.year, date__month=self.dfrom.month,
            motif__in=['pe', 'bulle']
        )
        self.tot_equipe = (
            qs.aggregate(tot=Sum('duree_prestation', output_field=DurationField()))['tot'] or timedelta()
        )
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        prev_month = self.dfrom - timedelta(days=2)
        next_month = self.dfrom + timedelta(days=33)
        context.update({
            'current_date': self.dfrom,
            'prev_month': prev_month if prev_month >= self.start_date else None,
            'next_month': next_month if next_month <= date.today() else None,
            'generales': self.get_prest_gen(),
            'tot_prest_gen': self.tot_prest_gen,
            'personnes': self.get_prest_personne(),
            'sorties': self.get_prest_sorties(),
            'equipe': self.get_prest_equipe(),
            'tot_prest_ind': self.tot_personnes + self.tot_sorties + self.tot_equipe
        })
        return context


class PrestationDeleteView(AsapCheckOwnerMixin, DeleteView):
    model = Prestation
    success_url = reverse_lazy('asap-prestation-list')


class PrestationExtraAddView(View):
    def get_current_week(self):
        today = date.today()
        monday = today - timedelta(days=today.weekday())
        return [monday + timedelta(days=d) for d in [0, 7]]

    def post(self, request, *args, **kwargs):
        action = kwargs['action']
        assert action in ['pe', 'bulle']
        duree = 3 if action == 'bulle' else 1
        cur_week = self.get_current_week()
        if Prestation.objects.filter(
            auteur=self.request.user, date__range=cur_week, motif=action
        ).exists():
            err_msg = {
                'pe': "Le «Point Equipe» a déjà été enregistré pour la semaine courante.",
                'bulle': "La «Bulle-horaire» a déjà été enregistrée pour la semaine courante.",
            }.get(action)
            messages.error(request, err_msg)
        else:
            Prestation.objects.create(
                auteur=self.request.user,
                date=date.today(),
                motif=action,
                duree_prestation=timedelta(hours=duree)
            )
        return HttpResponseRedirect(reverse('asap-prestation-list'))


class PrestationCreateView(AsapCheckPermMixin, CreateView):
    model = Prestation
    template_name = 'ser/prestation_change.html'
    form_class = PrestationChangeForm
    success_url = reverse_lazy('asap-prestation-list')

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm('ser.change_ser'):
            raise PermissionDenied(MSG_NO_ACCESS)
        return super().dispatch(request, *args, **kwargs)

    def get_initial(self):
        return {**super().get_initial(), 'date': date.today()}

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'user': self.request.user}

    def form_valid(self, form):
        prest = form.save(commit=False)
        prest.auteur = self.request.user
        prest.motif = 'autre'
        prest.save()
        return HttpResponseRedirect(self.success_url)


class PrestationUpdateView(AsapCheckOwnerMixin, UpdateView):
    model = Prestation
    template_name = 'ser/prestation_change.html'
    form_class = PrestationChangeForm
    success_url = reverse_lazy('asap-prestation-list')

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm('ser.change_ser'):
            raise PermissionDenied(MSG_NO_ACCESS)
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'user': self.request.user}

    def delete_url(self):
        return reverse('asap-prestation-delete', args=[self.object.pk])


class DocumentUploadView(AsapCheckPermMixin, CreateView):
    template_name = 'carrefour/document_upload.html'
    model = Document
    form_class = DocumentUploadForm
    titre_formulaire = 'Nouveau document'

    def dispatch(self, request, *args, **kwargs):
        self.person = get_object_or_404(Person, pk=kwargs['pk'])
        self.titre_page = f"Documents pour {self.person.nom_prenom}"
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('asap-person-change', args=[self.person.pk])

    def get_initial(self):
        initial = super().get_initial()
        initial['person'] = self.person
        return initial

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'user': self.request.user}


class DocumentDeleteView(AsapCheckPermMixin, DeleteView):
    model = Document
    pk_url_kwarg = 'doc_pk'

    def dispatch(self, request, *args, **kwargs):
        if request.user != self.get_object().auteur:
            raise PermissionDenied(MSG_NO_ACCESS)
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        user_dir = Path(self.get_object().fichier.path).parent
        response = super().delete(self.request)
        messages.success(self.request, "Le document a été supprimé avec succès")
        if not any(user_dir.iterdir()):
            os.rmdir(user_dir)
        return response

    def get_success_url(self):
        return reverse('asap-person-change', args=[self.object.person.pk])


class TshmListView(PermissionRequiredMixin, ListView):
    permission_required = 'ser.manage_ser'
    model = Tshm
    template_name = 'ser/tshm_list.html'
    start_date = date(2024, 1, 1)  # Month of first record

    def dispatch(self, request, *args, **kwargs):
        str_curdate = request.GET.get('date', None)
        self.dfrom, self.dto = get_selected_date(str_curdate)
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        qs = super().get_queryset().filter(
            prestation__date__year=self.dfrom.year, prestation__date__month=self.dfrom.month
        ).select_related(
            'prestation', 'prestation__auteur'
        ).order_by('prestation__date', 'typ', 'prestation__auteur')
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        prev_month = self.dfrom - timedelta(days=2)
        next_month = self.dfrom + timedelta(days=33)
        context.update({
            'current_date': self.dfrom,
            'prev_month': prev_month if prev_month >= self.start_date else None,
            'next_month': next_month if next_month <= date.today() else None,
        })
        return context


class TshmBaseView(AsapCheckPermMixin):
    model = Prestation
    form_class = TshmChangeForm
    template_name = 'ser/tshm_change.html'
    success_url = reverse_lazy('asap-prestation-list')

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'data': self.request.POST or None, 'user': self.request.user}

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(reverse("asap-prestation-list"))

    def form_invalid(self, form):
        return render(self.request, self.template_name, self.get_context_data(form=form))


class TshmCreateView(TshmBaseView, CreateView):

    def get_initial(self):
        return {**super().get_initial(), 'date': date.today()}


class TshmUpdateView(TshmBaseView, UpdateView):

    def dispatch(self, request, *args, **kwargs):
        self.object = Prestation.objects.get(pk=kwargs['pk'])
        if self.object.auteur != self.request.user:
            raise PermissionDenied(MSG_NO_ACCESS)
        return super().dispatch(request, *args, **kwargs)

    def delete_url(self):
        return reverse('asap-prestation-delete', args=[self.object.pk])
