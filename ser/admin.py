from django.contrib import admin

from .models import Journal, Person, Place, Prestation, Tshm


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    list_display = ('prenom', 'nom', 'telephone')
    search_fields = ('prenom', 'nom')


@admin.register(Place)
class PlaceAdmin(admin.ModelAdmin):
    list_display = ('localite', 'nom')


@admin.register(Journal)
class JournalAdmin(admin.ModelAdmin):
    pass


@admin.register(Prestation)
class PrestationAdmin(admin.ModelAdmin):
    list_display = ('date', 'auteur', 'motif', 'duree_prestation')


@admin.register(Tshm)
class TshmAdmin(admin.ModelAdmin):
    list_display = ('prestation', 'typ', 'duree', 'lieu')
