from datetime import date, timedelta

from dal import autocomplete

from django import forms
from django.db.models import Q, Sum
from django.utils import timezone

from carrefour.forms import BootstrapMixin, HMDurationField, PickDateWidget, PickTimeWidget
from carrefour.models import Utilisateur
from common import choices
from .models import Document, Journal, Person, Place, Prestation, Tshm


class PersonChangeForm(BootstrapMixin, forms.ModelForm):

    class Meta:
        model = Person
        exclude = ['contacts', 'age_approximatif']
        widgets = {
            'educs': autocomplete.ModelSelect2Multiple(url='asap-utilisateur-autocomplete'),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['reseau_prof'].widget.attrs['rows'] = 4
        self.fields['reseau_perso'].widget.attrs['rows'] = 4
        self.fields['naissance_jour'].label = 'Naiss. jour:'


class TshmFormsetForm(BootstrapMixin, forms.ModelForm):

    lieu = forms.ModelChoiceField(label='Lieu', queryset=Place.objects.filter(version=2), required=False)

    class Meta:
        model = Tshm
        fields = ('duree', 'typ', 'lieu', 'homme', 'femme', 'non_binaire')
        widgets = {
            'duree': PickTimeWidget,
        }
        field_classes = {
            'duree': HMDurationField
        }

    def clean_homme(self):
        return self.cleaned_data['homme'] or 0

    def clean_femme(self):
        return self.cleaned_data['femme'] or 0

    def clean_non_binaire(self):
        return self.cleaned_data['non_binaire'] or 0


class PlaceChangeForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = Place
        fields = ('nom', 'localite')

    def save(self, commit=True):
        self.instance.version = 2
        return super().save(commit)


class JournalChangeForm(BootstrapMixin, forms.ModelForm):

    class Meta:
        model = Journal
        fields = ('date', 'duree', 'texte')
        widgets = {
            'date': PickDateWidget,
            'duree': PickTimeWidget,
        }
        field_classes = {
            'duree': HMDurationField
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['texte'].required = False


class PrestationChangeForm(BootstrapMixin, forms.ModelForm):

    class Meta:
        model = Prestation
        fields = ('date', 'duree_prestation',)
        widgets = {
            'date': PickDateWidget,
            'duree_prestation': PickTimeWidget,
        }
        field_classes = {
            'duree_prestation': HMDurationField,
        }

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super().__init__(*args, **kwargs)

    def clean_date(self):
        date_prestation = self.cleaned_data['date']
        if date_prestation > date.today():
            raise forms.ValidationError("La saisie anticipée est impossible.")
        if not self.instance.check_date_editable(date_prestation, self.user):
            raise forms.ValidationError(
                "La saisie des prestations des mois précédents est close !"
            )
        return date_prestation


class TshmChangeForm(PrestationChangeForm):

    class Meta(PrestationChangeForm.Meta):
        fields = ('date', 'duree_prestation', )

    def __init__(self, data=None, *args, **kwargs):
        super().__init__(*args, data=data, **kwargs)

        TshmInlineFormset = forms.inlineformset_factory(
            Prestation, Tshm, form=TshmFormsetForm, extra=5, can_delete=True
        )
        self.formset = TshmInlineFormset(
            instance=self.instance, data=data,
            queryset=Tshm.objects.filter(prestation=self.instance) if self.instance.pk else Tshm.objects.none()
        )

        self.formset.extra = 5 - self.formset.queryset.count()

    def is_valid(self):
        return all([super().is_valid(), self.formset.is_valid()])

    def has_changed(self):
        return any([super().has_changed()] + self.formset.has_changed())

    def save(self, **kwargs):
        self.instance.motif = 'tshm'
        self.instance.auteur = self.user
        parent = super().save(**kwargs)
        self.formset.save()
        parent.duree_prestation = parent.tshms.aggregate(tot=Sum('duree'))['tot'] or timedelta(0)
        parent.save()
        return parent


class DocumentUploadForm(BootstrapMixin, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super().__init__(*args, **kwargs)

    class Meta:
        model = Document
        exclude = ('date_creation', 'auteur')
        widgets = {'person': forms.HiddenInput}
        labels = {'fichier': ''}

    def save(self, *args, **kwargs):
        if self.instance.pk is None:
            self.instance.auteur = self.user
            self.instance.date_creation = timezone.now()
        return super().save(*args, **kwargs)


class PersonFilterForm(forms.Form):
    region = forms.ChoiceField(
        label="Région", choices=(('', 'Toutes'),) + choices.REGIONS_CHOICES, required=False
    )
    interv = forms.ModelChoiceField(
        label="Interv.", queryset=Utilisateur.intervenants_asap(), empty_label="Tous", required=False
    )
    letter = forms.CharField(
        label='Bénéf.', widget=forms.TextInput(attrs={'placeholder': 'Recherche…', 'autocomplete': 'off'}),
        required=False
    )

    def filter(self, beneficiaires):
        region = self.cleaned_data['region']
        if region:
            beneficiaires = beneficiaires.filter(region=region)

        interv = self.cleaned_data['interv']
        if interv:
            beneficiaires = beneficiaires.filter(educs=interv)

        letter = self.cleaned_data['letter']
        if letter:
            beneficiaires = beneficiaires.filter(
                Q(prenom__istartswith=letter) | Q(nom__istartswith=letter)
            )
        return beneficiaires
