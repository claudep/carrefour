import os
from datetime import date, datetime, timedelta

from django.db import models
from django.db.models import F, Sum
from django.db.models.functions import TruncMonth

from carrefour.models import Contact, Utilisateur, JournalBase
from carrefour.utils import format_d_m_Y, format_duree
from common import choices
from common.fields import ChoiceArrayField


class Person(models.Model):
    STATUS_CHOICES = (
        ('actif', 'Actif'),
        ('veille', 'En veille'),
        ('supprime', 'Supprimé')
    )
    nom = models.CharField('Nom', max_length=40, blank=True)
    prenom = models.CharField('Prénom', max_length=40, unique=True)
    surnom = models.CharField('Surnom', max_length=40, blank=True)
    age_approximatif = models.IntegerField('Âge approximatif', null=True, blank=True)
    date_naissance = models.DateField('Date de naissance', null=True, blank=True)
    telephone = models.CharField('Téléphone', max_length=40, blank=True)
    email = models.EmailField('Courriel', blank=True)
    genre = models.CharField('Genre', max_length=1, choices=(('M', 'M'), ('F', 'F')), default='M')
    rue = models.CharField('Rue', max_length=30, blank=True)
    npa = models.CharField('NPA', max_length=4, blank=True)
    localite = models.CharField('Localité', max_length=30, blank=True)
    remarque = models.TextField('Remarque', blank=True)
    educs = models.ManyToManyField(
        to=Utilisateur, related_name='beneficiaires_suivis', blank=True, verbose_name='Référent ASAP'
    )
    contacts = models.ManyToManyField(
        to=Contact, related_name='beneficiaires', blank=True, verbose_name='Membres du réseau'
    )
    statut = models.CharField("Statut", max_length=20, choices=STATUS_CHOICES, default='actif')
    naissance_jour = models.CharField(
        'Jour', max_length=2, choices=((str(j), str(j)) for j in range(1, 32)), blank=True, default=''
    )
    naissance_mois = models.CharField(
        'Mois', max_length=2, choices=((str(m), str(m)) for m in range(1, 13)), blank=True, default=''
    )
    naissance_annee = models.CharField('Année', max_length=4, blank=True)
    region = models.CharField('Région', max_length=12, choices=choices.REGIONS_CHOICES, blank=True)
    motif_demande = ChoiceArrayField(
        models.CharField(max_length=60, choices=choices.ASAP_MOTIF_DEMANDE),
        verbose_name="Motif de la demande", blank=True, null=True, default=list
    )
    reseau_prof = models.TextField("Réseau professionnel", blank=True)
    reseau_perso = models.TextField("Réseau personnel", blank=True)
    service_orienteur = models.CharField(
        "Orientation par", max_length=20, choices=choices.ASAP_SERVICE_ORIENTEUR, blank=True
    )

    class Meta:
        verbose_name = 'Bénéf. ASAP'
        verbose_name_plural = 'Bénéf. ASAP'
        ordering = ('prenom',)
        permissions = (
            ('view_ser', 'Consulter les dossiers ASAP'),
            ('manage_ser', 'RE ASAP'),
            ('change_ser', 'Gérer les dossiers de l’ASAP')
        )

    def __str__(self):
        return f"{self.prenom} {self.nom}" if self.nom else self.prenom

    @classmethod
    def actives(cls):
        return Person.objects.filter(statut='actif')

    @property
    def nom_prenom(self):
        return f"{self.nom} {self.prenom}"

    def can_edit(self, user):
        return user.has_perm('ser.change_ser')

    @classmethod
    def check_perm_view(cls, user):
        return user.has_perm('ser.view_ser')

    def get_educs(self):
        return ', '.join([educ.nom_prenom for educ in self.educs.all()])


class Place(models.Model):
    LOCALITE_CHOICES = (
        ('', ''),
        ('cdf', 'CDF'),
        ('ll', 'LL')
    )
    nom = models.CharField('Lieu de stationnement', max_length=40)
    localite = models.CharField('Localité', max_length=30, choices=LOCALITE_CHOICES, blank=True)
    version = models.SmallIntegerField('Version')

    class Meta:
        verbose_name = 'Stationnement du BIP'
        verbose_name_plural = 'Stationnements du BIP'
        ordering = ('nom',)
        unique_together = ('localite', 'nom')

    def __str__(self):
        return f"{self.nom}"

    def can_edit(self, user):
        return user.has_perm('ser.change_ser')


class Sortie(models.Model):
    date = models.DateField('Date')
    heure_debut = models.TimeField('Heure de début', null=True, blank=True)
    heure_fin = models.TimeField('Heure de fin', null=True, blank=True)
    lieu = models.ForeignKey(to=Place, on_delete=models.CASCADE, related_name='sorties')
    type_intervention = ChoiceArrayField(
        models.CharField(max_length=65, choices=choices.TYPE_INTERVENTION_CHOICES, blank=True),
        verbose_name="Type d’intervention", blank=True, null=True,
    )
    autre_intervention = models.CharField("Autre interv.", max_length=50, blank=True)

    nbre_garcons = models.IntegerField(verbose_name='jeunes hommes', blank=True, default=0)
    nbre_filles = models.IntegerField(verbose_name='jeunes femmes', blank=True, default=0)
    nbre_non_binaires = models.IntegerField(verbose_name='personnes non binaires', blank=True, default=0)
    suivi_possible = models.IntegerField("Nbre de suivis possibles", blank=True, default=0)
    remarque = models.TextField('Remarque', blank=True)
    educs = models.ManyToManyField(to=Utilisateur, related_name='sorties')

    class Meta:
        verbose_name = "Sortie du BIP"
        verbose_name_plural = "Sorties du BIP"

    def __str__(self):
        return f"{format_d_m_Y(self.date)} - {self.lieu.localite}"

    def can_edit(self, user):
        return user.has_perm('ser.change_ser')

    def get_duree(self):
        if self.heure_debut is None or self.heure_fin is None:
            return timedelta()
        diff = datetime.combine(date.min, self.heure_fin) - datetime.combine(date.min, self.heure_debut)
        return diff if diff >= timedelta() else diff + timedelta(days=1)

    @property
    def get_lieu(self):
        return self.lieu.nom if self.lieu else ''

    @property
    def get_type_intervention(self):
        dct = dict((x, y) for x, y in choices.TYPE_INTERVENTION_CHOICES)
        return ", ".join([dct[item] for item in self.type_intervention]) if self.type_intervention else ''

    @property
    def get_educs(self):
        return ', '.join([educ.sigle for educ in self.educs.all()]) if self.educs else ''


class Journal(JournalBase):
    person = models.ForeignKey(
        Person, related_name='journaux', on_delete=models.CASCADE, verbose_name="Bénéficiaire"
    )
    date = models.DateField('Date')
    duree = models.DurationField('Durée de la prest.', default=timedelta(0))

    class Meta(JournalBase.Meta):
        db_table = 'ser_journalser'
        verbose_name = "Journal ASAP"
        verbose_name_plural = "Journaux ASAP"

    def __str__(self):
        return f"Journal de {self.person} - {self.date}"


class Prestation(models.Model):
    MOTIFS_CHOICES = [('tshm', 'Tshm'), ('bulle', 'Bulle Horaire'), ('pe', 'Point Equipe'), ('autre', 'Autre')]
    auteur = models.ForeignKey(Utilisateur, null=True, on_delete=models.PROTECT, related_name='prestations_asap')
    date = models.DateField('Date')

    nbre_suivi_relationnel = models.IntegerField('Nbre de suivis relationnels', default=0)

    nbre_acc_educatif = models.IntegerField("Nbre d’accompagnements éducatifs", default=0)

    # Anciens champs avant 2022, à supprimer plus tard.
    colloque = models.DurationField('Colloque hebdo.', default=timedelta(0))
    analyse = models.DurationField('Analyse de pratique', default=timedelta(0))
    journee_recul = models.DurationField('Journée Recul', default=timedelta(0))
    bulle = models.DurationField('Bulle horaire hebdomadaire', default=timedelta(0))
    soutien_parental = models.DurationField('Soutien parental', default=timedelta(0))
    accompagnement = models.DurationField('Accompagnement jeunes et familles', default=timedelta(0))
    tshm = models.DurationField('TSHM', default=timedelta(0))
    permanence = models.DurationField('Permanence', default=timedelta(0))
    intervention = models.DurationField('Intervention externe', default=timedelta(0))
    manifestation = models.DurationField('Manifestation', default=timedelta(0))
    reseau = models.DurationField('Rencontre réseau', default=timedelta(0))
    administratif = models.DurationField('Trav. admin.', default=timedelta(0))

    duree_admin = models.DurationField('Travaux. admin.', default=timedelta(0))
    duree_projet = models.DurationField('Projet et intervention', default=timedelta(0))
    duree_reseau = models.DurationField('Rencontre réseau', default=timedelta(0))
    duree_suivi = models.DurationField('Suivi et jeunes', default=timedelta(0))

    motif = models.CharField('Motif', choices=MOTIFS_CHOICES)
    duree_prestation = models.DurationField('Durée', blank=True, default=timedelta(0))
    homme = models.SmallIntegerField('Jeunes hommes', default=0)
    femme = models.SmallIntegerField('Jeunes femmes', default=0)
    non_binaire = models.SmallIntegerField('Personnes non binaires', default=0)

    # jour du mois maximum où il est encore possible de saisir des prestations du mois précédent
    max_jour = 5
    max_jour_re = 8

    class Meta:
        db_table = 'ser_prestationser'
        verbose_name = 'Prestation ASAP'
        verbose_name_plural = 'Prestations ASAP'

    def __str__(self):
        return f"{format_d_m_Y(self.date)} -  {self.auteur}: {format_duree(self.temps_total_prest())}"

    def temps_total_prest(self):
        return self.duree_suivi + self.duree_projet + self.duree_reseau + self.duree_admin + self.duree_prestation

    @classmethod
    def check_date_editable(cls, dt, user):
        """Contrôle si la prestation est modifiable en fonction de la date 'dt'."""
        max_delai = cls.max_jour_re if user.is_responsable_equipe else cls.max_jour
        today = date.today()
        prev_month = today.replace(day=1) - timedelta(days=1)
        return (
                ((dt.year == today.year) and (dt.month == today.month)) or
                ((dt.year == prev_month.year) and (dt.month == (today - timedelta(days=max_delai)).month))
        )

    @staticmethod
    def somme_durees():
        return Sum(
            F('duree_suivi') +
            F('duree_projet') +
            F('duree_reseau') +
            F('duree_admin')
        )

    @classmethod
    def totaux_mensuels(cls, base_qs, annee):
        """ Return ['20:00', '12:30', '', etc] * 12 (each month) + annual total """
        query = base_qs.filter(
            date__year=annee).annotate(
            month=TruncMonth('date')).values(
            'month').annotate(
            temps_total=cls.somme_durees()).order_by('month')
        by_month = {res['month'].month: res['temps_total'] for res in query}

        tot_mens_gen = []
        tot_annuel = timedelta()
        for mois in range(1, 13):
            duree = by_month.get(mois, timedelta())
            tot_annuel += duree
            tot_mens_gen.append(format_duree(duree) if duree else '')
        return tot_mens_gen + [format_duree(tot_annuel)]


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/doc/ser/user_<id>/<filename>
    return 'doc/ser/user_{0}/{1}'.format(instance.person.id, filename)


class Document(models.Model):
    person = models.ForeignKey(Person, related_name='documents', on_delete=models.CASCADE)
    date_creation = models.DateTimeField()
    auteur = models.ForeignKey(Utilisateur, related_name='documents_ser', on_delete=models.PROTECT)
    fichier = models.FileField("Fichier", upload_to=user_directory_path)
    titre = models.CharField(max_length=100)

    class Meta:
        models.UniqueConstraint(fields=['person', 'titre'], name='unique_person_titre')

    def __str__(self):
        return self.titre

    def delete(self):
        try:
            os.remove(self.fichier.path)
        except OSError:
            pass
        return super().delete()

    def can_edit(self, user):
        return self.person.can_edit(user)

    def can_be_deleted(self, user):
        return user == self.auteur


class Tshm(models.Model):
    TYP_CHOICES = (
        ('tshm', 'TSHM'), ('permanence', 'Permanence'), ('ecole', 'École'), ('manifestation', 'Manifestation'),
        ('autre', 'Autre')
    )
    prestation = models.ForeignKey(to=Prestation, on_delete=models.CASCADE, related_name='tshms')
    typ = models.CharField('Type', max_length=15, choices=TYP_CHOICES)
    lieu = models.ForeignKey(
        to=Place, null=True, on_delete=models.SET_NULL, related_name='tshm'
    )
    duree = models.DurationField('Durée', blank=True, default=timedelta(0))
    homme = models.SmallIntegerField('Jeunes hommes', default=0, blank=True)
    femme = models.SmallIntegerField('Jeunes femmes', default=0, blank=True)
    non_binaire = models.SmallIntegerField('Personnes non binaires', default=0, blank=True)

    def __str__(self):
        return f"Sortie du {format_d_m_Y(self.prestation.date)}: {self.get_typ_display()}"
