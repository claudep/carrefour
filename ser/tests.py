from datetime import date, timedelta
from pathlib import Path

from freezegun import freeze_time

from django.conf import settings
from django.contrib.auth.models import Group, Permission
from django.core.files import File
from django.template import Context, Template
from django.test import TestCase
from django.utils import timezone
from django.urls import reverse

from carrefour.models import Utilisateur
from carrefour.utils import format_m_Y, format_j_F_Y
from common.test_utils import TempMediaRootMixin
from .forms import PersonChangeForm, PrestationChangeForm
from .models import Document, Journal, Person, Place, Prestation, Sortie, Tshm


class AsapInitialDataMixin:

    @classmethod
    def setUpTestData(cls):
        cls.user_asap = Utilisateur.objects.create_user(
            'user_asap', 'user_asap@example.org', nom='Valjean', prenom='Jean'
        )
        group_asap = Group.objects.create(name='asap')
        group_asap.permissions.add(Permission.objects.get(codename='view_ser'))
        group_asap.permissions.add(Permission.objects.get(codename='change_ser'))
        cls.user_asap.groups.add(group_asap)

        cls.manager_asap = Utilisateur.objects.create_user('manager_asap', 'manager_asap@example.org')
        cls.manager_asap.user_permissions.add(Permission.objects.get(codename='add_contact'))
        cls.manager_asap.user_permissions.add(Permission.objects.get(codename='manage_ser'))
        cls.manager_asap.groups.add(group_asap)

        cls.anonyme = Utilisateur.objects.create_user(username='Doe', password='password')

        Place.objects.create(nom="Esplanade", localite='', version=2)


class PersonTests(AsapInitialDataMixin, TestCase):

    custom_data = {
        'nom': 'Doe',
        'prenom': "John",
        'surnom': 'The Boss',
        'age_approximatif': '18',
        'date_naissance': '2002-01-01',
        'telephone': '099 123 45 67',
        'email': 'jd@example.org',
        'genre': "M",
        'rue': 'Rue de la Fontaine',
        'npa': 9990,
        'localite': 'Moulinsart',
        'remarque': 'Une remarque',
        'statut': "actif",
        'region': 'mont_locle'
    }

    def test_user_list_permission(self):
        self.client.force_login(self.anonyme)
        response = self.client.get(reverse('asap-person-list'))
        self.assertEqual(response.status_code, 403)

        self.client.force_login(self.user_asap)
        response = self.client.get(reverse('asap-person-list'))
        self.assertEqual(response.status_code, 200)

    def test_person_list(self):
        p1 = Person.objects.create(prenom='John', genre='M', region='mont_locle', statut='actif')
        p1.educs.add(self.user_asap)
        p2 = Person.objects.create(prenom='Paul', genre='M', region='mont_locle', statut='actif')
        p2.educs.add(self.manager_asap)
        Person.objects.create(prenom='Sylvie', genre='F', statut='veille')

        self.client.force_login(self.user_asap)
        response = self.client.get(reverse('asap-person-list'))
        self.assertEqual(len(response.context['object_list']), 1)

        self.client.force_login(self.manager_asap)
        response = self.client.get(reverse('asap-person-list'))
        self.assertEqual(len(response.context['object_list']), 1)
        self.assertContains(
            response,
            f'<td><a href="/asap/person/{p2.pk}/change/">Paul</a></td><td>Le Locle</td><td> </td>', html=True
        )
        response = self.client.get(reverse('asap-person-veille'))
        self.assertEqual(len(response.context['object_list']), 1)
        # Menu ASAP
        self.assertContains(response, 'Mes Prestations')

    def test_add_champs_obligatoires(self):
        ul_txt = '<ul class="errorlist"><li>Ce champ est obligatoire.</li></ul>'
        self.client.force_login(self.user_asap)
        response = self.client.post(reverse('asap-person-add'))
        self.assertContains(
            response,
            ul_txt,
            html=True,
            count=3)
        self. assertEqual(
            response.context['form'].errors,
            {'prenom': ['Ce champ est obligatoire.'],
             'genre': ['Ce champ est obligatoire.'],
             'statut': ['Ce champ est obligatoire.'],
             }
        )

    def test_add_person(self):
        self.client.force_login(self.user_asap)
        response = self.client.post(reverse('asap-person-add'), data=self.custom_data)
        self.assertRedirects(response, reverse('asap-person-list'))
        person = Person.objects.get(nom='Doe')
        person.educs.add(self.user_asap)
        response = self.client.get(reverse('asap-person-list'))
        self.assertEqual(len(response.context['object_list']), 1)
        self.assertEqual(response.context['object_list'][0].nom, self.custom_data['nom'])

    def test_edit_person(self):
        self.client.force_login(self.user_asap)
        person = Person.objects.create(**self.custom_data)
        self.client.post(
            reverse('asap-person-change', args=[person.pk]),
            data={**self.custom_data, 'prenom': 'Paul'}
        )
        person.refresh_from_db()
        self.assertEqual(person.prenom, 'Paul')

    def test_supprimer_affichage_age_approx_et_contacts(self):
        self.client.force_login(self.user_asap)
        person = Person.objects.create(**self.custom_data)
        response = self.client.get(reverse('asap-person-change', args=[person.pk]))
        self.assertNotContains(response, 'Créer contact', html=False)
        self.assertNotContains(response, 'age_approximatif', html=False)

        form = PersonChangeForm()
        self.assertNotIn('age_approximatif', form.fields)
        self.assertNotIn('contacts', form.fields)

    def test_nom_prenom_abreg(self):
        out = Template(
            "{% load my_tags %}"
            "{{ educ|nom_prenom_abreg }}"
        ).render(Context({'educ': self.user_asap}))
        self.assertEqual(out, 'Valjean J.')

    def test_filtre_beneficiaire(self):
        p1 = Person.objects.create(prenom='John', genre='M', region='mont_locle', statut='actif')
        p2 = Person.objects.create(prenom='John2', genre='M', region='litt_ntel', statut='actif')
        p3 = Person.objects.create(prenom='John3', genre='M', region='mont_locle', statut='actif')
        p1.educs.add(self.manager_asap)
        p2.educs.add(self.manager_asap)
        p3.educs.add(self.user_asap)

        self.client.force_login(self.user_asap)
        response = self.client.get(reverse('asap-person-list'))
        self.assertEqual(len(response.context['object_list']), 1)

        response = self.client.get(reverse('asap-person-list') + "?region=mont_locle")
        self.assertEqual(len(response.context['object_list']), 2)

        response = self.client.get(reverse('asap-person-list') + f"?interv={self.manager_asap.pk}")
        self.assertEqual(len(response.context['object_list']), 2)

        response = self.client.get(reverse('asap-person-list') + "?letter=x")
        self.assertEqual(len(response.context['object_list']), 0)

        response = self.client.get(
            reverse('asap-person-list') + f"?letter=j&region=mont_locle&interv={self.user_asap.pk}"
        )
        self.assertEqual(len(response.context['object_list']), 1)


class PrestationTests(AsapInitialDataMixin, TestCase):

    def setUp(self):
        self.custom_data = {
            'date': date(2020, 8, 1),
            'auteur': self.user_asap,
            'duree_prestation': timedelta(),
            'motif': 'autre',
            'homme': 1,
            'femme': 2,
            'non_binaire': 3
        }
        self.form_data = {
            'date': '1.8.2020',
            'auteur': self.user_asap.pk,
            'duree_prestation': '00:00',
            'motif': 'autre',
            'homme': 1,
            'femme': 2,
            'non_binaire': 3
        }

    def test_check_date_editable(self):
        with freeze_time("2021-01-04"):
            self.assertFalse(Prestation.check_date_editable(date(2020, 11, 30), self.user_asap))
            self.assertTrue(Prestation.check_date_editable(date(2020, 12, 2), self.user_asap))
            self.assertTrue(Prestation.check_date_editable(date(2021, 1, 2), self.user_asap))

    def test_add_prestation_avec_valeurs_par_defaut(self):
        self.client.force_login(self.user_asap)
        response = self.client.get(reverse('asap-prestation-add'))
        self.assertContains(
            response,
            f'name="date" value="{date.today().strftime("%Y-%m-%d")}"',
        )
        self.assertEqual(response.context['user'], self.user_asap)

    def test_add_prestation(self):
        self.client.force_login(self.user_asap)
        form_kwargs = {**self.form_data, 'date': date.today() - timedelta(days=5), 'duree_prestation': '01:30'}
        response = self.client.post(reverse('asap-prestation-add'), data=form_kwargs)
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertEqual(Prestation.objects.all().count(), 1)

    def test_list_prestation(self):
        gen = Prestation.objects.create(
            date=date.today(), auteur=self.user_asap, motif='autre', duree_prestation=timedelta(hours=1)
        )

        person = Person.objects.create(prenom='Paul')
        journal = Journal.objects.create(
            person=person, auteur=self.user_asap, date=date.today(), duree=timedelta(minutes=90)
        )

        pe = Prestation.objects.create(
            date=date.today(), auteur=self.user_asap, motif='pe', duree_prestation=timedelta(hours=1)
        )
        bh = Prestation.objects.create(
            date=date.today(), auteur=self.user_asap, motif='bulle', duree_prestation=timedelta(hours=3)
        )

        sortie = Prestation.objects.create(
            date=date.today(), auteur=self.user_asap, motif='tshm', duree_prestation=timedelta(hours=1)
        )

        self.client.force_login(self.user_asap)
        response = self.client.get(reverse('asap-prestation-list'))
        self.assertContains(
            response,
            f"""<td width="80%"><a href="/asap/prestation/{gen.pk}/change/">{format_j_F_Y(gen.date)}</a></td>
                <td class="text-end">01:00</td>""",
            html=True
        )
        self.assertContains(
            response,
            f"""<td width="80%"><a href="/asap/person/{journal.person.pk}/journal/"> Paul</a></td>
                <td class="text-end">01:30</td>""",
            html=True
        )
        self.assertContains(
            response,
            f"""<td><a href="/asap/tshm/{sortie.pk}/change/">Sortie du {format_j_F_Y(sortie.date)}</a></td>
            <td class="text-end">01:00</td>""",
            html=True
        )
        self.assertContains(
            response,
            f"""<td><a href="/asap/prestation/{pe.pk}/change/">Point Equipe - {format_j_F_Y(pe.date)}</a></td>
                    <td class="text-end">01:00</td>""",
            html=True
        )
        self.assertContains(
            response,
            f"""<td><a href="/asap/prestation/{bh.pk}/change/">Bulle Horaire - {format_j_F_Y(bh.date)}</a></td>
                <td class="text-end">03:00</td>""",
            html=True
        )

    def test_edit_prestation_generale(self):
        self.client.force_login(self.user_asap)
        gen = Prestation.objects.create(
            date=date.today(), auteur=self.user_asap, motif='autre', duree_prestation=timedelta(hours=1),
            homme=1, femme=2, non_binaire=3
        )
        self.client.post(
            reverse('asap-prestation-change', args=[gen.pk]),
            data={'date': date.today(), 'auteur': self.user_asap, 'duree_prestation': '04:00',
                  'motif': 'tshm', 'homme': 1, 'femme': 2, 'non_binaire': 3}
        )
        gen.refresh_from_db()
        self.assertEqual(gen.duree_prestation, timedelta(hours=4))

    def test_date_saisie_anticipee_erreur(self):
        self.client.force_login(self.user_asap)
        tomorrow = date.today() + timedelta(days=1)
        form_kwargs = {**self.form_data, 'date': tomorrow}
        form = PrestationChangeForm(data=form_kwargs, user=self.user_asap)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {'date': ["La saisie anticipée est impossible."]})

    def test_saisie_tardive(self):
        self.client.force_login(self.user_asap)
        mock_date = date.today() - timedelta(days=31 + Prestation.max_jour)
        with freeze_time(mock_date):
            form_kwargs = {**self.form_data}
            response = self.client.post(reverse('asap-prestation-add'), data=form_kwargs)
            self.assertContains(
                response,
                '<li>La saisie des prestations des mois précédents est close !</li>',
                html=True
            )

    def test_access_prestation_list_error(self):
        self.client.force_login(self.anonyme)
        response = self.client.get(reverse('asap-prestation-list'))
        self.assertEqual(response.status_code, 403)

    def test_access_prestation_create_error(self):
        self.client.force_login(self.anonyme)
        response = self.client.get(reverse('asap-prestation-add'))
        self.assertEqual(response.status_code, 403)

    def test_access_only_owner(self):
        today = date.today()
        prest = Prestation.objects.create(date=today, auteur=self.user_asap, motif='pe', duree_prestation='01:00')

        self.client.force_login(self.user_asap)
        response = self.client.get(reverse('asap-prestation-list'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['equipe']), 1)

        response = self.client.get(reverse('asap-prestation-change', args=[prest.pk]))
        self.assertContains(response, "Supprimer", html=False)

        self.client.post(
            reverse('asap-prestation-change', args=[prest.pk]),
            instance=prest,
            data={
                'auteur': self.user_asap, 'date': today, 'motif': 'bulle', 'duree_prestation': '03:00',
                'homme': 1, 'femme': 2, 'non_binaire': 3
            }
        )
        prest.refresh_from_db()
        self.assertEqual(prest.duree_prestation, timedelta(hours=3))

        response = self.client.post(reverse('asap-prestation-delete', args=[prest.pk]))
        self.assertEqual(self.user_asap.prestations_asap.count(), 0)
        self.assertRedirects(response, reverse('asap-prestation-list'))

    def test_access_not_owner(self):
        prest = Prestation.objects.create(
            date=date.today(), auteur=self.user_asap, motif='pe', duree_prestation='01:00'
        )
        self.client.force_login(self.manager_asap)
        response = self.client.get(reverse('asap-prestation-list'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['equipe']), 0)

        response = self.client.get(reverse('asap-prestation-change', args=[prest.pk]))
        self.assertEqual(response.status_code, 403)

        response = self.client.post(reverse('asap-prestation-delete', args=[prest.pk]))
        self.assertEqual(response.status_code, 403)

    def test_access_prestation_change_error(self):
        self.client.force_login(self.anonyme)
        prest = Prestation.objects.create(**self.custom_data)
        response = self.client.get(reverse('asap-prestation-change', args=[prest.pk]))
        self.assertEqual(response.status_code, 403)

    def test_list_par_defaut_sur_mois_courant(self):
        self.client.force_login(self.user_asap)
        response = self.client.get((reverse('asap-prestation-list')))
        self.assertContains(
            response,
            format_m_Y(date.today())
        )

    @freeze_time("2024-01-01")
    def test_bouton_mois_precedent_pas_affiche(self):
        self.client.force_login(self.user_asap)
        today = date.today()
        response = self.client.get((reverse('asap-prestation-list')))
        prev_date = today.replace(day=1) - timedelta(days=1)
        prev_month = prev_date.strftime("%m%Y")
        self.assertNotContains(
            response,
            f'<a class ="btn btn-sm" href="?date={prev_month}">&lt;</a>',
            html=True,
        )
        # never next button with today date

    @freeze_time("2024-02-02")
    def test_bouton_suivant_precedent(self):
        self.client.force_login(self.user_asap)
        response = self.client.get(f"{reverse('asap-prestation-list')}?date=022024")
        self.assertContains(
            response,
            '<a class ="btn btn-sm" href="?date=012024">&lt;</a>',
            html=True
        )

    def test_delai_saisie_prestation_OK_pour_educs(self):
        mock_date = date(2023, 2, Prestation.max_jour)
        with freeze_time(mock_date):
            self.client.force_login(self.user_asap)
            data = {**self.form_data, 'duree_prestation': '02:20', 'date': "2023-01-30"}
            response = self.client.post(reverse('asap-prestation-add'), data=data)
            self.assertEqual(response.status_code, 302)
            self.assertEqual(Prestation.objects.all().count(), 1)

    def test_delai_saisie_prestation_tardive_pour_educs(self):
        mock_date = date(2023, 2, Prestation.max_jour + 1)
        with freeze_time(mock_date):
            self.client.force_login(self.user_asap)
            data = {**self.form_data, 'date': "2022-01-31"}
            response = self.client.post(reverse('asap-prestation-add'), data=data)
            self.assertContains(response, "La saisie des prestations des mois précédents est close !", html=False)

    def test_delai_saisie_prestation_pour_re(self):
        moke_date = date(2023, 2, Prestation.max_jour_re)
        with freeze_time(moke_date):
            data = {**self.form_data, 'date': "2023-01-28", 'auteur': self.manager_asap.pk}
            form = PrestationChangeForm(user=self.manager_asap, data=data)
            self.assertTrue(form.is_valid())

        moke_date = date(2023, 2, Prestation.max_jour_re + 1)
        with freeze_time(moke_date):
            data = {**self.form_data, 'date': "2023-01-29", 'auteur': self.manager_asap.pk}
            form = PrestationChangeForm(user=self.manager_asap, data=data)
            self.assertFalse(form.is_valid())

    def test_prestation_point_equipe(self):
        self.client.force_login(self.user_asap)
        response = self.client.get(reverse('asap-prestation-list'))
        self.assertEqual(len(response.context['equipe']), 0)
        response = self.client.post(
            reverse('asap-prestation-extra-add', args=['pe']),
            follow=True
        )
        self.assertEqual(len(response.context['equipe']), 1)

    @freeze_time("2024-01-16")
    def test_doublon_prestation_point_equipe(self):
        Prestation.objects.create(
            date=date.today(), auteur=self.user_asap, duree_prestation='03:00', motif='pe'
        )
        self.client.force_login(self.user_asap)
        response = self.client.post(
            reverse('asap-prestation-extra-add', args=['pe']),
            follow=True
        )
        self.assertContains(response, "Le «Point Equipe» a déjà été enregistré pour la semaine courante.")

    def test_prestation_bulle_horaire(self):
        self.client.force_login(self.user_asap)
        response = self.client.get(reverse('asap-prestation-list'))
        self.assertEqual(len(response.context['equipe']), 0)
        response = self.client.post(
            reverse('asap-prestation-extra-add', args=['bulle']),
            follow=True
        )
        self.assertEqual(len(response.context['equipe']), 1)

    @freeze_time("2024-01-19")
    def test_doublon_prestation_bulle_horaire(self):
        Prestation.objects.create(
            date=date(2024, 1, 17), auteur=self.user_asap, duree_prestation='03:00', motif='bulle'
        )
        self.client.force_login(self.user_asap)
        response = self.client.post(
            reverse('asap-prestation-extra-add', args=['bulle']),
            follow=True
        )
        self.assertContains(response, "La «Bulle-horaire» a déjà été enregistrée pour la semaine courante.")

    def test_delete_prestation_generale(self):
        prest = Prestation.objects.create(
            date=date(2024, 1, 17), auteur=self.user_asap, duree_prestation='03:00', motif='autre'
        )
        self.client.force_login(self.user_asap)
        response = self.client.post(reverse('asap-prestation-delete', args=[prest.pk]))
        self.assertRedirects(response, reverse('asap-prestation-list'))
        self.assertFalse(Prestation.objects.filter(date="2024-01-17").exists())


class ContactTests(AsapInitialDataMixin, TestCase):

    def setUp(self):
        self.person_data = {
            'nom': 'Doe',
            'prenom': "John",
            'surnom': 'The Boss',
            'age_approximatif': '18',
            'date_naissance': '2002-01-01',
            'telephone': '099 123 45 67',
            'email': 'jd@example.org',
            'genre': "M",
            'rue': 'Rue de la Fontaine',
            'npa': 9990,
            'localite': 'Moulinsart',
            'remarque': 'Une remarque',
            'statut': "actif"
        }

    def test_btn_invisible_lors_creation_person(self):
        person = Person.objects.create(**self.person_data)
        self.client.force_login(self.manager_asap)
        response = self.client.get(reverse('asap-person-add'), data=self.person_data)
        url = f"/contact/add/?forpers=person-{person.pk}"
        btn_contact = f"""
                <a class="btn btn-sm btn-outline-primary mt-2" href="{url}">Créer contact</a>
                """
        self.assertNotContains(response, btn_contact, html=True)


class JournalTests(AsapInitialDataMixin, TestCase):

    def test_access_journal_list_error(self):
        self.client.force_login(self.anonyme)
        person = Person.objects.create(prenom='John', genre='M', statut='actif')
        response = self.client.get(reverse('asap-journal-list', args=[person.pk]))
        self.assertEqual(response.status_code, 403)

    def test_access_journal_creation_error(self):
        self.client.force_login(self.anonyme)
        person = Person.objects.create(prenom='John', genre='M', statut='actif')
        response = self.client.get(reverse('asap-journal-add', args=[person.pk]))
        self.assertEqual(response.status_code, 403)

    def test_access_journal_change_error(self):
        person = Person.objects.create(prenom='John', genre='M', statut='actif')
        journal = Journal.objects.create(
            person=person, auteur=self.user_asap, theme='Thème', date=date.today(), texte='Un texte'
        )
        self.client.force_login(self.anonyme)
        response = self.client.get(reverse('asap-journal-change', args=[person.pk, journal.pk]))
        self.assertEqual(response.status_code, 403)

    def test_modif_journal_par_auteur_seulement(self):
        person = Person.objects.create(prenom='John', genre='M', statut='actif')
        journal = Journal.objects.create(
            person=person, auteur=self.user_asap, date=date.today(), texte='Un texte'
        )
        self.client.force_login(self.manager_asap)
        response = self.client.get(reverse('asap-journal-change', args=[person.pk, journal.pk]))
        self.assertEqual(response.status_code, 403)

    def test_journal_list(self):
        today = date.today()
        person = Person.objects.create(prenom='John', genre='M', statut='actif')
        Journal.objects.bulk_create([
            Journal(date=today, texte="Un premier texte", auteur=self.user_asap, person=person),
            Journal(date=today, texte="Un deuxième texte", auteur=self.user_asap, person=person),
            Journal(date=today, texte="Un troisième texte", auteur=self.user_asap, person=person)
        ])
        journaux_pk = Journal.objects.all().values_list('pk', flat=True)

        self.client.force_login(self.user_asap)
        response = self.client.get(reverse('asap-journal-list', args=[person.pk]))
        self.assertEqual(len(response.context['person'].journaux.all()), 3)
        self.assertContains(response, 'Un premier texte', html=False)
        self.assertContains(response, 'Un deuxième texte', html=False)
        self.assertContains(response, 'Un troisième texte', html=False)

        # liens pour édition
        for i, j in enumerate(journaux_pk):
            url = reverse('asap-journal-change', args=[person.pk, j])
            self.assertContains(response, f'<a href="{url}"># {i+1}</a>', html=True)

    def test_journal_creation(self):
        self.client.force_login(self.user_asap)
        person = Person.objects.create(prenom='John', genre='M', statut='actif')
        create_kwargs = {'date': date.today(), 'duree': '1:00', 'texte': 'Un texte'}
        response = self.client.post(reverse('asap-journal-add', args=[person.pk]), data=create_kwargs)
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, reverse('asap-journal-list', args=[person.pk]))

    def test_journal_change(self):
        person = Person.objects.create(prenom='John', genre='M', statut='actif')
        create_kwargs = {
            'person': person,
            'date': date.today(),
            'auteur': self.user_asap,
            'theme': 'Un thème',
            'texte': 'Un texte',
        }
        journal = Journal.objects.create(**create_kwargs)
        self.client.force_login(self.user_asap)
        self.client.post(
            reverse('asap-journal-change', args=[person.pk, journal.pk]),
            data={'date': date.today(), 'duree': '00:45', 'texte': 'Un autre texte'},
            follow=True
        )
        journal.refresh_from_db()
        self.assertEqual(journal.texte, 'Un autre texte')


class DocumentTests(AsapInitialDataMixin, TempMediaRootMixin, TestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.person = Person.objects.create(nom='Doe', prenom='John', genre='M', region='mont_locle', statut='actif')
        cls.person.educs.add(cls.user_asap)

    def test_model_creation(self):
        doc = Document.objects.create(
            person=self.person, date_creation=timezone.now(), auteur=self.user_asap, fichier='', titre="Un test"
        )
        self.assertIsInstance(doc, Document)

    def test_anonymous_upload(self):
        self.client.force_login(self.anonyme)
        response = self.client.get(reverse('person-doc-upload', args=[self.person.pk]))
        self.assertEqual(response.status_code, 403)

    def test_logged_user_upload(self):
        self.client.force_login(self.user_asap)
        response = self.client.get(reverse('person-doc-upload', args=[self.person.pk]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'carrefour/document_upload.html')

    def test_view_fail_blank(self):
        self.client.force_login(self.user_asap)
        response = self.client.post(reverse('person-doc-upload', args=[self.person.pk]), {})
        form = response.context['form']
        self.assertEqual(len(form.errors), 3)
        self.assertFormError(form, 'fichier', 'Ce champ est obligatoire.')
        self.assertFormError(form, 'person', 'Ce champ est obligatoire.')
        self.assertFormError(form, 'titre', 'Ce champ est obligatoire.')

    def test_view_invalid_data(self):
        self.client.force_login(self.user_asap)
        path = Path(settings.BASE_DIR, 'archive/tests/sample.docx')
        with path.open(mode='rb') as fh:
            file = File(fh, name='sample.docx'),
            response = self.client.post(
                reverse('person-doc-upload', args=[self.person.pk]),
                {'fichier': '', 'person': self.person.pk, 'titre': 'Un titre'},
                format="multipart"
            )
            self.assertFormError(response.context['form'], 'fichier', 'Ce champ est obligatoire.')

            response = self.client.post(
                reverse('person-doc-upload', args=[self.person.pk]),
                {'fichier': file, 'person': self.person.pk, 'titre': ''},
                format="multipart"
            )
            self.assertFormError(response.context['form'], 'titre', 'Ce champ est obligatoire.')

    def test_view_success(self):
        self.client.force_login(self.user_asap)
        path = Path(settings.BASE_DIR, 'archive/tests/sample.docx')
        with path.open(mode='rb') as fh:
            file = File(fh, name='sample.docx'),
            response = self.client.post(
                reverse('person-doc-upload', args=[self.person.pk]),
                {'fichier': file, 'person': self.person.pk, 'titre': 'Un titre'},
                format="multipart"
            )
            self.assertRedirects(response, reverse('asap-person-change', args=[self.person.pk]))
            self.assertTrue(Path(path).exists())
            self.assertEqual(self.person.documents.count(), 1)

    def test_delete_doc_no_access(self):
        path = Path(settings.BASE_DIR, 'archive/tests/sample.docx')
        with path.open(mode='rb') as fh:
            doc = Document.objects.create(
                person=self.person,
                date_creation=timezone.now(),
                auteur=self.user_asap,
                fichier=File(fh, name='sample.docx'),
                titre="Fichier test",
            )
        self.assertEqual(self.person.documents.count(), 1)
        self.client.force_login(self.anonyme)
        response = self.client.post(reverse('person-doc-delete', args=[self.person.pk, doc.pk]))
        self.assertEqual(response.status_code, 403)

    def test_delete_document_succes(self):
        path = Path(settings.BASE_DIR, 'archive/tests/sample.docx')
        with path.open(mode='rb') as fh:
            doc = Document.objects.create(
                person=self.person,
                date_creation=timezone.now(),
                auteur=self.user_asap,
                fichier=File(fh, name='sample.docx'),
                titre="Fichier test",
            )
        self.assertEqual(self.person.documents.count(), 1)
        self.client.force_login(self.user_asap)
        response = self.client.post(reverse('person-doc-delete', args=[self.person.pk, doc.pk]))
        self.assertEqual(self.person.documents.count(), 0)
        self.assertRedirects(response, reverse('asap-person-change', args=[self.person.pk]))


class PlaceTest(AsapInitialDataMixin, TestCase):

    def test_create(self):
        self.client.force_login(self.manager_asap)
        response = self.client.post(reverse('asap-place-add'), data={'nom': 'La gare', 'localite': 'cdf'})
        self.assertRedirects(response, reverse('asap-place-list'))
        place = Place.objects.get(nom='La gare')
        self.assertEqual(place.localite, 'cdf')

    def test_update(self):
        self.client.force_login(self.manager_asap)
        place = Place.objects.create(nom='La gare', localite='cdf', version=2)
        self.client.post(
            reverse('asap-place-change', args=[place.pk]),
            data={'nom': 'La gare', 'localite': 'll'}
        )
        place.refresh_from_db()
        self.assertEqual(place.localite, 'll')

    def test_delete_permission_re(self):
        place = Place.objects.get(nom='Esplanade')
        self.client.force_login(self.user_asap)
        response = self.client.post(reverse('asap-place-delete', args=[place.pk]))
        self.assertEqual(response.status_code, 403)
        self.client.force_login(self.manager_asap)
        self.client.post(reverse('asap-place-delete', args=[place.pk]))
        self.assertEqual(Place.objects.all().count(), 0)

    def test_no_delete_with_related_sortie(self):
        place = Place.objects.get(nom='Esplanade')
        Sortie.objects.create(date=date.today(), lieu=place)
        self.client.force_login(self.manager_asap)
        response = self.client.post(reverse('asap-place-delete', args=[place.pk]), follow=True)
        self.assertEqual(Place.objects.all().count(), 1)
        self.assertContains(
            response,
            """<li class="alert alert-danger">
            Ce lieu est utilisé dans une sortie. Vous devez d'abord supprimer la sortie concernée.
            </li>""",
            html=True
        )

    def test_list(self):
        Place.objects.create(nom='La gare', localite='cdf', version=2)
        self.client.force_login(self.user_asap)
        response = self.client.get(reverse('asap-place-list'))
        self.assertEqual(response.status_code, 403)
        self.client.force_login(self.manager_asap)
        response = self.client.get(reverse('asap-place-list'))
        self.assertEqual(len(response.context['object_list']), 2)


class TshmTests(AsapInitialDataMixin, TestCase):

    def test_create(self):
        self.client.force_login(self.user_asap)
        response = self.client.get(reverse('asap-tshm-add'))
        self.assertContains(
            response,
            '<input type="hidden" name="tshms-TOTAL_FORMS" value="5" id="id_tshms-TOTAL_FORMS">',
            html=True
        )

        self.client.post(
            reverse('asap-tshm-add'),
            data={
                'date': date.today(), 'auteur': self.user_asap.pk,
                "tshms-TOTAL_FORMS": "2", "tshms-INITIAL_FORMS": "0",
                "tshms-0-typ": 'ecole', 'tshms-0-duree': '03:00', "tshms-0-homme": 1, "tshms-0-femme": 2,
                "tshms-0-non_binaire": 3,
                "tshms-1-typ": 'autre', 'tshms-1-duree': '02:00', "tshms-1-homme": 3, "tshms-1-femme": 2,
                "tshms-1-non_binaire": '',
            }
        )
        prest = Prestation.objects.get(date=date.today())
        self.assertEqual(prest.duree_prestation, timedelta(hours=5))
        self.assertEqual(Tshm.objects.last().non_binaire, 0)

    def test_update_formset(self):
        today = date.today()
        place = Place.objects.get(nom='Esplanade')
        prest = Prestation.objects.create(
            date=today, auteur=self.user_asap, duree_prestation='05:00', motif='tshm'
        )
        tshm1 = Tshm.objects.create(
            prestation=prest, typ='permanence', duree='03:00', lieu=place, homme=1, femme=2, non_binaire=3
        )
        tshm2 = Tshm.objects.create(
            prestation=prest, typ='autre', duree='02:00', lieu=place, homme=1, femme=2, non_binaire=3
        )

        self.client.force_login(self.user_asap)
        self.client.post(
            reverse('asap-tshm-change', args=[prest.pk]),
            instance=prest,
            data={
                'date': today, 'auteur': self.user_asap.pk,
                "tshms-TOTAL_FORMS": "2", "tshms-INITIAL_FORMS": "2",
                "tshms-0-typ": 'ecole', 'tshms-0-duree': '03:00', 'tshms-0-id': tshm1.pk,
                "tshms-0-homme": 1, "tshms-0-femme": 2, "tshms-0-non_binaire": 3,
                "tshms-1-typ": 'autre', 'tshms-1-duree': '06:00', 'tshms-1-id': tshm2.pk,
                "tshms-1-homme": '', "tshms-1-femme": 2, "tshms-1-non_binaire": 1,
                'initial': {'duree': timedelta()}
            },
        )
        prest = Prestation.objects.get(date=date.today())
        self.assertEqual(prest.duree_prestation, timedelta(hours=9))
        tshm2.refresh_from_db()
        self.assertEqual(tshm2.homme, 0)

    def test_add_and_update_formset(self):
        today = date.today()
        place = Place.objects.get(nom='Esplanade')
        prest = Prestation.objects.create(
            date=today, auteur=self.user_asap, duree_prestation='03:00', motif='tshm',
        )
        tshm = Tshm.objects.create(
            prestation=prest, typ='permanence', duree='03:00', lieu=place, homme=1, femme=2, non_binaire=3)

        self.client.force_login(self.user_asap)
        self.client.post(
            reverse('asap-tshm-change', args=[prest.pk]),
            data={
                'date': today, 'auteur': self.user_asap.pk,
                "tshms-TOTAL_FORMS": "2", "tshms-INITIAL_FORMS": "1",
                "tshms-0-typ": 'ecole',
                'tshms-0-duree': '13:00',
                'tshms-0-id': tshm.pk,
                "tshms-0-homme": 1, "tshms-0-femme": 2, "tshms-0-non_binaire": 3,

                "tshms-1-typ": 'manifestation',
                'tshms-1-duree': '12:00',
                "tshms-1-homme": 3, "tshms-1-femme": 2, "tshms-1-non_binaire": 1,
            }
        )
        prest.refresh_from_db()
        self.assertEqual(prest.duree_prestation, timedelta(hours=25))

    def test_delete_formset(self):
        today = date.today()
        prest = Prestation.objects.create(
            date=today, auteur=self.user_asap, duree_prestation='05:00', motif='tshm',
        )
        tshm1 = Tshm.objects.create(prestation=prest, typ='permanence', duree='03:00')
        tshm2 = Tshm.objects.create(prestation=prest, typ='autre', duree='02:00')

        self.client.force_login(self.user_asap)
        self.client.post(
            reverse('asap-tshm-change', args=[prest.pk]),
            data={
                'date': today, 'auteur': self.user_asap.pk,
                "tshms-TOTAL_FORMS": "2", "tshms-INITIAL_FORMS": "2",
                'tshms-0-id': tshm1.pk,
                'tshms-0-DELETE': 'True',

                "tshms-1-typ": 'autre',
                'tshms-1-duree': '06:00',
                'tshms-1-id': tshm2.pk,
                "tshms-1-homme": 3, "tshms-1-femme": 2, "tshms-1-non_binaire": 1,
            }
        )
        prest.refresh_from_db()
        self.assertEqual(prest.duree_prestation, timedelta(hours=6))

    def test_empty_formset(self):
        today = date.today()
        prest = Prestation.objects.create(
            date=today, auteur=self.user_asap, duree_prestation='05:00', motif='tshm'
        )
        self.client.force_login(self.user_asap)
        response = self.client.post(
            reverse('asap-tshm-change', args=[prest.pk]),
            data={
                'date': today, 'auteur': self.user_asap.pk,
                "tshms-TOTAL_FORMS": "5", "tshms-INITIAL_FORMS": "0",
                'tshms-0-DELETE': 'True',
                'tshms-1-DELETE': 'True',
                'tshms-2-DELETE': 'True',
                'tshms-3-DELETE': 'True',
                'tshms-4-DELETE': 'True',
            }
        )
        prest.refresh_from_db()
        self.assertEqual(prest.duree_prestation, timedelta())
        self.assertRedirects(response, reverse("asap-prestation-list"))

    def test_tshm_list_access_re_only(self):
        self.client.force_login(self.user_asap)
        response = self.client.get(reverse('asap-tshm-list'))
        self.assertEqual(response.status_code, 403)
        self.client.force_login(self.manager_asap)
        response = self.client.get(reverse('asap-tshm-list'))
        self.assertEqual(response.status_code, 200)

    @freeze_time("2024-02-01")
    def test_tshm_list(self):
        today = date.today()
        hier = today - timedelta(days=1)
        demain = today + timedelta(days=1)
        place = Place.objects.get(nom='Esplanade')
        p1 = Prestation.objects.create(date=today, auteur=self.user_asap, motif='tshm')
        p2 = Prestation.objects.create(date=hier, auteur=self.user_asap, motif='tshm')
        p3 = Prestation.objects.create(date=demain, auteur=self.manager_asap, motif='tshm')
        Tshm.objects.bulk_create([
            Tshm(prestation=p1, typ='manifestation', duree=timedelta(hours=2), homme=1, femme=2, non_binaire=3),
            Tshm(prestation=p1, typ='ecole', duree=timedelta(hours=2), lieu=place, homme=1, femme=2, non_binaire=3),
            Tshm(prestation=p2, typ='autre', duree=timedelta(hours=2), lieu=place, homme=1, femme=2, non_binaire=3),
            Tshm(prestation=p3, typ='tshm', duree=timedelta(hours=2), lieu=place, homme=1, femme=2, non_binaire=3),
        ])
        self.client.force_login(self.manager_asap)
        response = self.client.get(reverse('asap-tshm-list'))

        self.assertEqual(len(response.context['object_list']), 3)
        self.assertContains(
            response,
            """<tr><td>1 février 2024</td><td>École</td><td>Valjean Jean</td><td>Esplanade</td><td>02:00</td></tr>""",
            html=True
        )
        self.assertContains(response, 'février', html=False)
        self.assertContains(
            response,
            """<a class="btn btn-sm" href="?date=012024">&lt;</a>""",
            html=True
        )

    def test_delete_tshm(self):
        prest = Prestation.objects.create(
            date=date(2024, 1, 17), auteur=self.user_asap, duree_prestation='02:00', motif='tshm'
        )
        Tshm.objects.create(prestation=prest, typ='manifestation', duree=timedelta(hours=2))

        self.client.force_login(self.user_asap)
        response = self.client.post(reverse('asap-prestation-delete', args=[prest.pk]))
        self.assertRedirects(response, reverse('asap-prestation-list'))
        self.assertFalse(Prestation.objects.filter(date="2024-01-17").exists())
        self.assertFalse(Tshm.objects.filter(prestation=prest).exists())

    def test_access_only_for_owner(self):
        prest = Prestation.objects.create(
            date=date(2024, 1, 17), auteur=self.user_asap, duree_prestation='02:00', motif='tshm'
        )
        Tshm.objects.create(prestation=prest, typ='manifestation', duree=timedelta(hours=2))
        self.client.force_login(self.manager_asap)
        response = self.client.get(reverse('asap-tshm-change', args=[prest.pk]))
        self.assertEqual(response.status_code, 403)

    def test_typ_mandatory_field(self):
        self.client.force_login(self.user_asap)
        response = self.client.post(
            reverse('asap-tshm-add'),
            data={
                'date': date.today(), 'auteur': self.user_asap.pk,
                "tshms-TOTAL_FORMS": "1", "tshms-INITIAL_FORMS": "0",
                "tshms-0-typ": '', 'tshms-0-duree': '03:00', "tshms-0-homme": 1, "tshms-0-femme": 2,
                "tshms-0-non_binaire": 3,
            }
        )
        self.assertContains(response, '<ul class="errorlist"><li>Ce champ est obligatoire.</li></ul>', html=True)
