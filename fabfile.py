import getpass
from fabric import task
from invoke import Context, Exit

APP_DIR = '/var/www/gestion'
VIRTUALENV_DIR = '/var/venv/gestion/bin/activate'
default_db_owner = getpass.getuser()

MAIN_HOST = 'gestion.fondation-carrefour.net'


@task(hosts=[MAIN_HOST])
def deploy(conn):
    """
    Deploy project with latest GitLab code
    """
    with conn.cd(APP_DIR):
        conn.run("git pull")
        with conn.prefix('source %s' % VIRTUALENV_DIR):
            conn.run("python manage.py migrate")
            conn.run("python manage.py collectstatic --noinput")
        conn.run("touch common/wsgi.py")


@task(hosts=[MAIN_HOST])
def clone_remote_db(conn, dbname='carrefour'):
    """ Dump a remote database and load it locally

        !!!! Call the function with: fab clone-remote-db !!!!

     """

    local = Context()

    def exist_local_db(db):
        res = local.run('psql --list', hide='stdout')
        return db in res.stdout.split()

    def exist_username(user):
        res = local.run('psql -d postgres -c "select usename from pg_user;"', hide='stdout')
        return user in res.stdout.split()

    conn.config['sudo']['password'] = getpass.getpass("Enter the sudo password (on the server):")
    tmp_path = '/tmp/{db}.dump'.format(db=dbname)
    conn.sudo('pg_dump --no-owner --no-privileges -Fc {db} > {tmp}'.format(db=dbname, tmp=tmp_path), user='postgres')
    conn.get(tmp_path, None)
    conn.run('rm {}'.format(tmp_path))

    if exist_local_db(dbname):
        rep = input('A local database named "{}" already exists. Overwrite? (y/n)'.format(dbname))
        if rep == 'y':
            local.run('psql -d postgres -c "DROP DATABASE {};"'.format(dbname))
        else:
            raise Exit("Database not copied")

    if exist_username(dbname):
        owner = dbname
    else:
        owner = default_db_owner
    local.run('psql -d postgres -c "CREATE DATABASE {db} OWNER={owner};"'.format(db=dbname, owner=owner))
    local.run('pg_restore --no-owner --no-privileges -d {db} {db}.dump'.format(db=dbname))
