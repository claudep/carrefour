import datetime
from functools import partial
from io import BytesIO

from django.utils.text import slugify

from pypdf import PdfWriter, PdfReader
from reportlab.lib.units import cm
from reportlab.lib.enums import TA_LEFT, TA_JUSTIFY
from reportlab.lib import colors
from reportlab.pdfgen import canvas
from reportlab.platypus import (
    KeepTogether, PageBreak, Paragraph, Spacer, Table, TableStyle,
)
from carrefour.pdf import (
    BaseCarrefourPDF, CleanParagraph, JournalPdf, PageNumCanvas,
)
from carrefour.utils import format_d_m_Y, str_or_empty

"""
Organisation des documents PDF pour Asaef:

1. AnalyseDemandePdf
  (DemandeAccompagnementPagePdf + génogramme/sociogramme si fichier présent)
2. CoordonneesPdf
3. ContratCollaborationPdf
"""


class AnalyseDemandePdf:
    def __init__(self, tampon):
        self.tampon = tampon
        self.merger = PdfWriter()

    def append_pdf(self, PDFClass):
        tampon = BytesIO()
        pdf = PDFClass(tampon, with_page_num=True)
        pdf.produce(self.famille)
        self.merger.append(tampon)

    def produce(self, famille, **kwargs):
        self.famille = famille

        self.append_pdf(DemandeAccompagnementPagePdf)
        if famille.genogramme:
            self.merger.append(PdfReader(famille.genogramme.path))
        if famille.sociogramme:
            self.merger.append(PdfReader(famille.sociogramme.path))
        self.merger.write(self.tampon)

    def get_filename(self, famille):
        return '{}_analyse_demande.pdf'.format(slugify(famille.nom))


class BaseAsaefPdf(BaseCarrefourPDF):
    unite = 'asaef'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style_bold.spaceBefore = 0.3 * cm
        self.style_normal.alignment = TA_JUSTIFY
        self.P = partial(CleanParagraph, style=self.style_normal)
        self.Pbold = partial(Paragraph, style=self.style_bold)

    def _print_items(self, title, fields, bilan):
        if title != "":
            self.story.append(Paragraph(title, self.style_sub_title))
        for field in fields:
            label = bilan._meta.get_field(field).verbose_name
            self.story.append(self.Pbold(label))
            self.story.append(self.P(getattr(bilan, field).replace('\n', '<br />\n')))

    def print_encadre(self):
        suivi = self.famille.suiviasaef
        data = [
            ["Date de l’entretien: {}".format(format_d_m_Y(suivi.date_analyse)),
             "Inter. ASAEF présents:"],
            [Paragraph("IPE: {}".format(suivi.get_ope_referents()), self.style_normal),
             Paragraph(suivi.intervenants_presents, self.style_normal)],
            ["Membres famille présents: {}".format(suivi.pers_famille_presentes)],
            ["Autres pers. présentes: {}".format(suivi.autres_pers_presentes)],
        ]
        t = Table(
            data=data, colWidths=[7 * cm, 11 * cm], hAlign=TA_LEFT,
            spaceBefore=0 * cm, spaceAfter=0.5 * cm
        )
        t.hAlign = 0
        t.setStyle(tblstyle=TableStyle([
            ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
            ('VALIGN', (0, 0), (-1, -1), 'TOP'),
            ('FONTSIZE', (0, 0), (-1, -1), self.base_font_size),
            ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
            ('LINEABOVE', (0, 2), (-1, -1), 0.25, colors.black),
        ]))
        self.story.append(t)

    def print_enfants(self):
        P = partial(Paragraph, style=self.style_normal)
        enfants = self.famille.membres_suivis()
        titres = [
            'Nom', 'Prénom', 'Filiation', 'Naissance', 'Tél.', 'Formation',
            'Centre scol.', 'École/Coll.', 'Enseignant', 'Struct. E-F.',
            'Assurance', 'Permis/séjour',
        ]
        data = [
            [P(enfant.nom) for enfant in enfants],
            [P(enfant.prenom) for enfant in enfants],
            [P(enfant.filiation) for enfant in enfants],
            [P(format_d_m_Y(enfant.date_naissance)) for enfant in enfants],
            [P(enfant.telephone) for enfant in enfants],
            [P(enfant.formation.get_statut_display()) for enfant in enfants],
            [P(str_or_empty(enfant.formation.cercle_scolaire)) for enfant in enfants],
            [P(str_or_empty(enfant.formation.college)) for enfant in enfants],
            [P(str_or_empty(enfant.formation.enseignant)) for enfant in enfants],
            [P(str_or_empty(enfant.formation.creche)) for enfant in enfants],
            [P(enfant.assurance) for enfant in enfants],
            [P(enfant.permis) for enfant in enfants],
        ]

        for i, value in enumerate(titres):
            data[i].insert(0, value)
        larg_col = [16 // len(enfants) for enfant in enfants]
        larg_col.insert(0, 2)
        self.story.append(KeepTogether([
            Paragraph("Enfants", self.style_sub_title),
            self.get_table(data, columns=larg_col, before=0, after=0)
        ]))

    def print_reseau(self):
        suivi = self.famille.suiviasaef
        data = [
            ['Famille', '{} - (Mandat: {})'.format(
                suivi.get_ope_referents(),
                suivi.get_mandat_ope_display()
            )]
        ]
        for enfant in self.famille.membres_suivis():
            for contact in enfant.reseaux.all():
                data.append([
                    enfant.prenom,
                    '{} ({})'.format(contact, contact.contact)
                ])
        self.story.append(KeepTogether([
            Paragraph("Réseau", self.style_sub_title),
            self.get_table(data, columns=[2, 16], before=0, after=0)
        ]))

    def draw_asaef_footer(self, canvas, doc):
        canvas.saveState()
        canvas.setLineWidth(0.5)
        canvas.line(doc.leftMargin, 2 * cm, doc.width + doc.leftMargin, 2 * cm)
        canvas.setFont(self.base_font_name, 10)
        txt = f"Famille {self.famille.nom} - {self.title}"
        canvas.drawString(doc.leftMargin, 1.2 * cm, txt)
        canvas.restoreState()


class CoordonneesPdf(BaseAsaefPdf):
    title = "Coordonnées"

    def get_filename(self, famille):
        return '{}_coordonnees.pdf'.format(slugify(famille.nom))

    def produce(self, famille, **kwargs):
        self.famille = famille
        self.set_title("Famille {} {}".format(famille.nom, self.title))

        self.print_encadre()
        self.print_parents(self.story, famille)
        self.print_personnes_significatives(self.story, famille)
        self.print_enfants()
        self.print_reseau()

        self.print_chronologie(famille)

        self.doc.build(
            self.story,
            onFirstPage=self.draw_header_footer, onLaterPages=self.draw_asaef_footer,
            canvasmaker=PageNumCanvas if self.with_page_num else canvas.Canvas
        )


class DemandeAccompagnementPagePdf(BaseAsaefPdf):
    title = "Analyse de la demande ASAEF"

    def produce(self, famille, unite='aemo', **kwargs):
        self.famille = famille
        suivi = famille.suiviasaef
        self.set_title("Famille {} - {}".format(famille.nom, self.title))

        self.print_encadre()
        self.print_parents(self.story, famille)
        self.print_personnes_significatives(self.story, famille)
        self.print_enfants()

        self.story.append(PageBreak())

        self.set_title(f"Famille {famille.nom} - Demande d'accompagnement ASAEF")

        self.story.append(Paragraph("<b>Motif(s) de la demande:</b> {}".format(
            suivi.get_motif_demande_display()), self.style_normal
        ))

        self.story.append(Paragraph('_' * 90, self.style_normal))
        self.story.append(Spacer(0, 0.5 * cm))
        self.story.append(Paragraph('Difficultés', self.style_sub_title))
        self.story.append(Paragraph(
            "Quelles sont les difficultés que vous rencontrez et depuis combien de temps ?",
            self.style_normal
        ))
        self.story.append(Spacer(0, 0.5 * cm))
        self.story.append(CleanParagraph(suivi.difficultes, self.style_indent))

        self.story.append(KeepTogether([
            Paragraph('Aides souhaitées', self.style_sub_title),
            CleanParagraph(suivi.aides, self.style_indent)
        ]))
        self.story.append(KeepTogether([
            Paragraph('Autres services', self.style_sub_title),
            Paragraph(
                "Avez-vous fait appel à d'autres services ? Si oui, avec quels vécus ?",
                self.style_italic
            ),
            Paragraph(suivi.autres_contacts, self.style_indent),
        ]))
        self.story.append(KeepTogether([
            Paragraph('Disponibilités', self.style_sub_title),
            CleanParagraph(suivi.disponibilites, self.style_indent)
        ]))
        self.story.append(KeepTogether([
            Paragraph('Remarques', self.style_sub_title),
            Paragraph(suivi.remarque, self.style_indent)
        ]))

        self.doc.build(
            self.story,
            onFirstPage=self.draw_header_footer, onLaterPages=self.draw_asaef_footer,
            canvasmaker=PageNumCanvas if self.with_page_num else canvas.Canvas
        )


class ContratCollaborationPdf(BaseAsaefPdf):
    title = "Contrat de collaboration Asaef"

    def get_filename(self, famille):
        return '{}_contrat_collaboration.pdf'.format(slugify(famille.nom))

    def produce(self, famille, **kwargs):
        self.famille = famille
        self.set_title(f"Famille {famille.nom} - {self.title}")
        suivi = famille.suiviasaef

        P = partial(Paragraph, style=self.style_normal)
        Pbold = partial(Paragraph, style=self.style_bold)

        # Enfants/Jeunes
        self.story.append(Paragraph("Enfants/Jeunes", self.style_sub_title))
        for enf in famille.membres_suivis():
            self.story.append(P('{} (*{} - {})'.format(
                enf.nom_prenom, format_d_m_Y(enf.date_naissance), enf.adresse)))

        # Parents
        self.story.append(Paragraph('Parents', self.style_sub_title))
        data = self.formate_persons(famille.parents_contractant())
        if data:
            self.story.append(self.get_table(data, columns=[2, 7, 2, 7], before=0, after=0.5))

        # Personnes significatives
        autres_parents = list(famille.autres_parents().filter(contractant=True))
        if autres_parents:
            self.story.append(Paragraph('Personne-s significative-s', self.style_sub_title))
            data = self.formate_persons(autres_parents)
            self.story.append(self.get_table(data, columns=[2, 7, 2, 7], before=0, after=1.0))

        intervenants = ' - '.join(f"{interv.nom_prenom} ({interv.telephone})"
                                  for interv in suivi.intervenants_asaef.all())
        ope = suivi.get_ope_referents(tel=True)
        data = [
            [Pbold("Office de protection de l'enfant (OPE):"), "Intervenant-e en protection de l'enfant (IPE)"],
            ["", P(ope)],
            [Pbold("Membres de l'équipe ASAEF"), P(intervenants)],
        ]
        self.story.append(self.get_table(data, columns=[6, 10], before=0, after=0.5))

        self.story.append(Paragraph("1. Les modalités", self.style_sub_title))
        data = [
            ["Durée de l'intervention:", "8 mois, avec une possibilité de prolongation de 3 mois."],
            [P("Office de protection de l'enfant (OPE):"), P(
                    "Un-e intervenant-e en protection de l'enfant (IPE) a un mandat sur l'enfant. "
                    "Une copie du contrat lui est envoyée.")],
            ["Coût:", P("Aucune participation financière n'est demandée à la famille, <strong> hormis les coûts "
                        "inhérents au suivi par la psychologue. Ceux-ci sont facturés selon le principe de"
                        " l'assurance Lamal. </strong>")],
            ["Annulation des rendez-vous:", "Par appel téléphonique, 24 heures à l'avance."],
            ["Engagement:", P("Un investissement personnel est indispensable à cette démarche.")],
            ["Collaboration avec le réseau:", P("Transmission d'informations et échanges avec accord "
                                                "préalable de la famille.")],
            ["Collaboration avec l'OPE:", P("Garantie d'un espace de confidentialité entre la famille, "
                                            "l'ASAEF et l'OPE pour autant que l'intégrité physique et psychique "
                                            "des personnes impliquées ne soit pas mise en danger. "
                                            "La confidentialité lors des séances individuelles avec le ou la "
                                            "psychologue répond aux principes du secret médical.")],
            ["Bilans:", P("Des bilans sont réalisés à la fin de chaque phase de l'intervention. "
                          "Ils se déroulent à l'OPE en présence des parents, des enfants, de l'IPE "
                          "et des membres de l'ASAEF.")],
            ["Restitution écrite du bilan:", P(
                "Une copie de chaque bilan est remise aux parents et à l'IPE.")],
            ["Arrêt de l'intervention:", "Un bilan de fin de l'intervention doit avoir lieu."],
        ]
        self.story.append(self.get_table(data, columns=[6, 10], before=0.0, after=0.5))

        self.story.append(Paragraph("2. L'organisation", self.style_sub_title))
        self.story.append(Pbold("Interventions des éducateurs-trices de l'ASAEF"))
        data = [
            ["Fréquence:", P(
                    "Durant la phase d'action et de soutien, les interventions varient entre 3 "
                    "heures (minimum) et 12 heures (maximum) par semaine. L'augmentation et la "
                    "diminution de l'intensité du soutien est définie durant les bilans.")],
            ["Lieux des rencontres:", "Domicile, extérieur et/ou bureau de la Fondation Carrefour."],
        ]
        self.story.append(self.get_table(data, columns=[6, 10], before=0.0, after=0.5))
        self.story.append(Pbold("Interventions du ou de la psychologue ASAEF:")),
        data = [
            ["Fréquence:", P("Au minimum 1 fois par mois.")],
            ["Lieux des rencontres:", "Bureau, domicile."],
        ]
        self.story.append(self.get_table(data, columns=[6, 10], before=0.0, after=0.5))

        self.story.append(Pbold("Interventions de l'assistant-e social-e ASAEF:")),
        data = [
            ["Fréquence:", P("Au minimum 1 fois durant la prestation.")],
            ["Lieux des rencontres:", "Bureau, domicile."],
        ]
        self.story.append(self.get_table(data, columns=[6, 10], before=0.0, after=1.5))

        self.story.append(P("Lieu et date:"))
        self.story.append(Spacer(0, 0.5 * cm))
        self.story.append(P("Par leurs signatures, les personnes soussignées s'engagent à "
                            "respecter les points ci-dessus."))
        data = [
            ["les Parents:", "Enfant-s:", "pour l'OPE:", "pour l'ASAEF:"]
        ]
        self.story.append(self.get_table(data, columns=[4, 4, 4, 4], before=1.0, after=1.0))
        self.doc.build(
            self.story,
            onFirstPage=self.draw_header_footer, onLaterPages=self.draw_asaef_footer,
            canvasmaker=PageNumCanvas if self.with_page_num else canvas.Canvas
        )


class JournalAsaefPdf(JournalPdf):
    def get_queryset(self, famille, selector):
        today = datetime.date.today()
        query = famille.prestations.exclude(texte='').order_by('-date_prestation')
        if selector:
            if selector == 'month':
                start_date = datetime.date(today.year, today.month, 1)
                return query.filter(date_prestation__gte=start_date)
            elif selector == 'two_months':
                previous = today - datetime.timedelta(days=31)
                start_date = datetime.date(previous.year, previous.month, 1)
                return query.filter(date_prestation__gte=start_date)
            elif selector == 'last_10':
                return query[:10]
        return query

    def produce(self, famille, **kwargs):
        self.set_title(self.get_title(famille))
        selector = kwargs.get('selector', None)
        for page in self.get_queryset(famille, selector):
            txt = f"{format_d_m_Y(page.date_prestation)}: {page.theme} ({page.intervenant.sigle})"
            self.story.append(Paragraph(txt, self.style_sub_title))
            self.story.append(Paragraph(page.texte.replace('\n', '<br />\n'), self.style_normal))
        # By-pass JournalPdf.produce
        return super(JournalPdf, self).produce(famille, **kwargs)


class BilanPdf(BaseAsaefPdf):
    title = ''

    def produce(self, bilan, **kwargs):
        self.famille = bilan.famille
        self.title = bilan.get_typ_display()
        self.set_title(f"Famille {self.famille.nom} - {self.title}")
        data = [
            [self.P('Date du bilan:'), self.P(format_d_m_Y(bilan.date))],
            [self.P('Destinataire(s):'), self.P(', '.join([pers.nom_prenom for pers in bilan.destinataires.all()]))],
            [self.P('Intervenant-e-s ASAEF:'), self.P(bilan.interv_asaef)],
            [self.P('Membres de la famille présents:'), self.P(bilan.membres_famille)],
            [self.P("Interv. en protection de l'enfance (IPE):"), self.P(bilan.famille.suivi.get_ope_referents())],
            [self.P('Autres personnes présentes:'), self.P(bilan.autres)],
            [self.P('Date de début du soutien:'), self.P(format_d_m_Y(bilan.famille.suivi.date_debut_suivi))],
            [self.P('Fréquence des interventions:'), self.P(bilan.freq_interv)]
        ]
        self.story.append(self.get_table(data, columns=[6, 12], before=0.0, after=0.5, bordered=True))

        if bilan.typ == '1mois':
            fields = ['objectif', 'moyens', 'evol_obj', 'evolution_fam',
                      'ajustement', 'acc_as', 'acc_psy', 'evol_intensite']
            self._print_items("État actuel", fields, bilan)

        if bilan.typ == '3mois':
            fields = ['situation_fam', 'objectif', 'moyens', 'evol_obj']
            self._print_items("Etat actuel", fields, bilan)

            self.story.append(
                Paragraph("Évolution de la situation familiale", self.style_sub_title))
            data = {"Selon la famille": 'evol_famille',
                    "Selon l'ASAEF": 'evol_asaef',
                    "Selon l'assistant-e social-e": 'acc_as',
                    "Selon le ou la psychologue": 'acc_psy'}
            for key, value in data.items():
                self.story.append(self.Pbold(key))
                self.story.append(self.P(getattr(bilan, value).replace('\n', '<br />\n')))

            fields = ['ajustement', 'evol_intensite']
            self._print_items("", fields, bilan)

            fields = ['amelioration', 'avenir', 'relais']
            self._print_items("Projection de la fin du soutien", fields, bilan)

        if bilan.typ == 'final':
            fields = ['situation_fam', 'objectif', 'moyens']
            self._print_items("État actuel", fields, bilan)

            self.story.append(Paragraph("Évolution de la situation familiale", self.style_sub_title))
            data = {"Selon la famille": 'evol_famille',
                    "Selon l'ASAEF": 'evol_asaef',
                    "Selon l'assistant-e social-e": 'acc_as',
                    "Selon le ou la psychologue": 'acc_psy'}
            for key, value in data.items():
                self.story.append(self.Pbold(key))
                self.story.append(self.P(getattr(bilan, value).replace('\n', '<br />\n')))

            fields = ['ressources', 'evol_intensite']
            self._print_items("", fields, bilan)

            fields = ['amelioration', 'avenir', 'prolongation', 'relais']
            self._print_items("Fin du soutien ASAEF et perspectives d'avenir", fields, bilan)

        self.doc.build(
            self.story,
            onFirstPage=self.draw_header_footer, onLaterPages=self.draw_asaef_footer,
            canvasmaker=PageNumCanvas if self.with_page_num else canvas.Canvas
        )

    def get_filename(self, bilan):
        filename = {
            '1mois': 'bilan_soutien_int.pdf', '3mois': 'bilan_autonom.pdf', 'final': 'bilan_final.pdf',
            'suppl': 'bilan_suppl.pdf'
        }
        return f"{slugify(bilan.famille.nom)}_{filename.get(bilan.typ)}"


class BilanAnalysePdf(BaseAsaefPdf):
    title = ''

    def produce(self, bilan, **kwargs):
        self.famille = bilan.famille
        self.title = bilan.titre
        self.set_title(f"Famille {self.famille.nom} - {self.title}")

        data = [
            ['Date du bilan:', format_d_m_Y(bilan.date)],
            ['Destinataire(s):', ', '.join([pers.nom_prenom for pers in bilan.destinataires.all()])],
            ['Intervenant-e-s ASAEF:', self.P(bilan.interv_asaef)],
            ['Membres de la famille:', self.P(bilan.membres_famille)],
            ['IPE:', bilan.famille.suivi.get_ope_referents()],
            ['Autres personnes présentes:', self.P(bilan.autres)],
            ["Date de début de l'analyse:", format_d_m_Y(bilan.famille.suivi.date_analyse)],
            ['Nbre de rencontres:', self.P(bilan.rencontre)]
        ]
        self.story.append(self.get_table(data, columns=[6, 10], before=0.0, after=0.5, bordered=True))

        fields = ['rdv_manque', 'acceptation', 'rappel_demande', 'contexte', 'observation',
                  'outil', 'problematique', 'hypothese', 'obj_general', 'obj_specifique',
                  'moyen', 'planification']
        self._print_items("", fields, bilan)

        self.doc.build(
            self.story,
            onFirstPage=self.draw_header_footer, onLaterPages=self.draw_asaef_footer,
            canvasmaker=PageNumCanvas if self.with_page_num else canvas.Canvas
        )

    def get_filename(self, bilan):
        return '{}_bilan_analyse.pdf'.format(slugify(bilan.famille.nom))


class ProlongationAsaefPdf(BaseAsaefPdf):
    title = "Prolongation du soutien"

    def produce(self, prolong, **kwargs):
        self.famille = prolong.famille
        self.set_title(f"Famille {self.famille.nom} - {self.title}")

        data = [
            ['Début de la prolongation:', format_d_m_Y(prolong.date_debut)],
            ['Fin de la prolongation:', format_d_m_Y(prolong.date_fin)],
            ['Intervenant-e-s ASAEF:', self.P(prolong.intervenants)],
            ['Membres de la famille:', self.P(prolong.membres_famille)],
            ['IPE:', prolong.famille.suivi.get_ope_referents()],
            ['Autres personnes présentes:', self.P(prolong.autres)],
        ]
        self.story.append(self.get_table(data, columns=[7, 11], before=0.0, after=0.5, bordered=True))

        fields = ['bilan', 'decision']
        self._print_items("", fields, prolong)

        self.doc.build(
            self.story,
            onFirstPage=self.draw_header_footer, onLaterPages=self.draw_asaef_footer,
            canvasmaker=PageNumCanvas if self.with_page_num else canvas.Canvas
        )

    def get_filename(self, prolong):
        return '{}_prolongation.pdf'.format(slugify(prolong.famille.nom))
