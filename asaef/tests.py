import os
from datetime import date, timedelta
from io import BytesIO
from pypdf import PdfReader

from django.contrib.auth.models import Group, Permission
from django.core.files import File
from django.templatetags.static import static
from django.test import TestCase, tag
from django.urls import reverse
from django.utils import timezone

from freezegun import freeze_time

from asaef.forms import (
    AgendaAsaefForm, BilanAnalyseAsaefForm, PrestationAsaefForm, ProlongationAsaefForm, SuiviAsaefForm
)
from asaef.models import (
    BilanAsaef, BilanAnalyseAsaef, FamilleAsaef, Intervention, JournalAsaef, PrestationAsaef, ProlongationAsaef,
    SuiviAsaef
)
from carrefour.models import (
    Contact, Document, DerogationSaisieTardive, Formation, LibellePrestation, Personne, Role, Service, Utilisateur
)
from carrefour.utils import format_duree, format_d_m_Y


class ASAEFTestBase(TestCase):

    @classmethod
    def setUpTestData(cls):
        Service.objects.bulk_create([
            Service(sigle='OPEN'),
            Service(sigle='SSE'),
            Service(sigle='CARREFOUR'),
        ], ignore_conflicts=True)

        Role.objects.bulk_create([
            Role(nom='Père', famille=True),
            Role(nom='Mère', famille=True),
            Role(nom='Beau-père', famille=True),
            Role(nom='Enfant suivi', famille=True),
            Role(nom='Enfant non-suivi', famille=True),
            Role(nom='Médecin', famille=False),
        ])
        LibellePrestation.objects.bulk_create([
            LibellePrestation(unite='asaef', code='p09', nom='Prestation 9 Étude'),
            LibellePrestation(unite='asaef', code='p10', nom='Prestation 10 Soutien'),
            LibellePrestation(unite='asaef', code='p11', nom='Prestation 11'),
            LibellePrestation(unite='asaef', code='p12', nom='Prestation 12'),
        ], ignore_conflicts=True)

        FamilleAsaef.objects.create_famille(
            nom='Tournesol', rue='Château1', npa=2000, localite='Moulinsart',
        )
        Utilisateur.objects.create_user(
            'user_asaef', 'user_asaef@example.org', sigle='UA', nom='Lampion', prenom='Séraphin'
        )
        Utilisateur.objects.create_user(
            'user_externe', 'externe@example.org', 'mepassword', first_name='Bruce', last_name='Batmann',
        )
        Utilisateur.objects.create(nom='Champmard', prenom='Ben', service=Service.objects.get(sigle='CARREFOUR'))
        Utilisateur.objects.create_user('user_aemo', 'user_aemo@example.org')

        Contact.objects.bulk_create([
            Contact(nom='Sybarnez', prenom='Tina', service=Service.objects.get(sigle='CARREFOUR')),
            Contact(nom='Rathfeld', prenom='Christophe', service=Service.objects.get(sigle='OPEN')),
            Contact(nom='DrSpontz', prenom='Igor', role=Role.objects.get(nom='Médecin')),
        ])

        Personne.objects.create_personne(
            famille=FamilleAsaef.objects.get(nom='Tournesol'), role=Role.objects.get(nom='Père'),
            nom='Hergé', prenom='Rémy', genre='M', date_naissance=date(1965, 4, 26),
            rue='Château1', npa=2000, localite='Moulinsart',
        )

    def setUp(self):
        self.user_asaef = Utilisateur.objects.get(username='user_asaef')
        self.user_asaef.user_permissions.add(Permission.objects.get(codename='view_asaef'))
        self.user_asaef.user_permissions.add(Permission.objects.get(codename='change_asaef'))
        self.user_externe = Utilisateur.objects.get(username='user_externe')


class SuiviAsaefTests(ASAEFTestBase):

    def test_famille_creation(self):
        famille_data = {
            'nom': 'Martin',
            'rue': 'Rue des Fleurs 4',
            'npa': '2000',
            'localite': 'Neuchâtel',
            'autorite_parentale': 'conjointe',
        }
        self.client.force_login(self.user_externe)
        with self.assertLogs('django.request', level='WARNING'):
            response = self.client.post(reverse('asaef-famille-add'), data=famille_data)
        self.assertEqual(response.status_code, 403)

        self.client.force_login(self.user_asaef)
        response = self.client.get(reverse('asaef-famille-add'))
        self.assertContains(response, "Enregistrer")
        self.assertNotContains(response, 'id="id_archived"')
        response = self.client.post(reverse('asaef-famille-add'), data=famille_data)

        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, reverse('asaef-famille-list'))
        famille = FamilleAsaef.objects.get(nom='Martin')
        self.assertEqual(famille.suiviasaef.service_orienteur, '')
        self.assertEqual(famille.suiviasaef.date_demande, date.today())

    def test_famille_update(self):
        famille_data = {
            'nom': 'Martin',
            'rue': 'Rue des Fleurs 4',
            'npa': '2000',
            'localite': 'Neuchâtel',
            'autorite_parentale': 'conjointe',
        }
        famille = FamilleAsaef.objects.create_famille(**famille_data)
        famille_data['rue'] = 'Rue du Vignoble'
        edit_url = reverse('asaef-famille-edit', args=[famille.pk])

        self.client.force_login(self.user_externe)
        with self.assertLogs('django.request', level='WARNING'):
            response = self.client.post(edit_url, data=famille_data)
        self.assertEqual(response.status_code, 403)

        self.client.force_login(self.user_asaef)
        response = self.client.get(edit_url)
        self.assertContains(response, "Enregistrer")
        self.assertNotContains(response, 'id="id_archived"')

        role_suivi = Role.objects.get(nom='Enfant suivi')
        role_nonsuivi = Role.objects.get(nom='Enfant non-suivi')
        pers_add_url = reverse('personne-add', args=[famille.typ, famille.pk])
        self.assertContains(response, '{}?role={}'.format(pers_add_url, role_suivi.pk))
        self.assertContains(response, '{}?role={}'.format(pers_add_url, role_nonsuivi.pk))

        response = self.client.post(edit_url, data=famille_data)
        self.assertRedirects(response, reverse('asaef-famille-edit', args=[famille.pk]))
        famille.refresh_from_db()
        self.assertEqual(famille.rue, 'Rue du Vignoble')

    def test_famille_demande(self):
        famille = FamilleAsaef.objects.get(nom='Tournesol')
        self.client.force_login(self.user_asaef)
        response = self.client.get(reverse('asaef-demande', args=[famille.pk]))
        self.assertContains(response, "Enregistrer")
        # TODO: tester le POST de la demande

    def test_personne_creation(self):
        self.client.force_login(self.user_asaef)
        response = self.client.post(reverse('asaef-famille-add'), data={
            'nom': 'Martin',
            'rue': 'Rue des Fleurs 4',
            'npa': '2000',
            'localite': 'Neuchâtel',
            'autorite_parentale': 'conjointe',
        })
        famille = FamilleAsaef.objects.get(nom='Martin')
        role_pere = Role.objects.get(nom='Père').pk
        url = '{}?role={}'.format(reverse('personne-add', args=[famille.typ, famille.pk]), role_pere)
        response = self.client.get(url)
        self.assertContains(response, "Enregistrer")
        response = self.client.post(url, data={
            'role': role_pere,
            'nom': 'Martinet',
            'prenom': 'Paul',
            'genre': 'M',
            'contractant': 'False',
        })
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, reverse('asaef-famille-edit', args=[famille.pk]))
        personne = Personne.objects.get(nom='Martinet')
        self.assertEqual(personne.prenom, 'Paul')
        self.assertEqual(personne.famille.nom, 'Martin')

    def test_personne_delete(self):
        famille = FamilleAsaef.objects.get(nom='Tournesol')
        pere = Personne.objects.create(
            famille=famille, nom='Martinet', prenom='Paul', role=Role.objects.get(nom='Père')
        )
        enf_suivi = Personne.objects.create(
            famille=famille, nom='Tournesol', prenom='Georgette', role=Role.objects.get(nom='Enfant suivi')
        )

        self.client.force_login(self.user_externe)
        delete_url = reverse('personne-delete', args=[famille.typ, famille.pk, pere.pk])
        with self.assertLogs('django.request', level='WARNING'):
            response = self.client.post(delete_url)
        self.assertEqual(response.status_code, 403)

        self.client.force_login(self.user_asaef)
        response = self.client.post(delete_url)
        self.assertFalse(Personne.objects.filter(pk=pere.pk).exists())
        # Un enfant suivi ne doit pas pouvoir être supprimé
        with self.assertLogs('django.request', level='WARNING'):
            response = self.client.post(
                reverse('personne-delete', args=[famille.typ, famille.pk, enf_suivi.pk])
            )
        self.assertEqual(response.status_code, 403)

    def test_suivi_dossier(self):
        famille = FamilleAsaef.objects.get(nom='Tournesol')
        self.client.force_login(self.user_asaef)
        suivi_url = reverse('asaef-famille-suivi', args=[famille.pk])
        response = self.client.post(suivi_url, data={
            'ope_referent': Contact.objects.get(nom='Rathfeld').pk,
            'date_demande': '2018-10-24',
            'etape_suivi': 'demande',
        })
        self.assertRedirects(response, suivi_url)

    def test_prestation_date_prestation(self):
        auj = date.today()
        lib_prest = LibellePrestation.objects.get(unite='asaef', code='p11')

        # saisie dans le mois courant
        data = {'duree': '3:40', 'date_prestation': auj, 'lib_prestation': lib_prest.pk,
                'theme': 'Test', 'texte': 'Un essai'}
        form = PrestationAsaefForm(user=self.user_asaef, data=data)
        self.assertTrue(form.is_valid(), msg=form.errors)

        # saisie par anticipation
        data['date_prestation'] = auj + timedelta(days=1)
        form = PrestationAsaefForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {'date_prestation': ['La saisie anticipée est impossible !']})

        # saisie trop tardive
        mois_prec = auj - timedelta(days=31 + PrestationAsaef.max_jour)
        data['date_prestation'] = mois_prec
        form = PrestationAsaefForm(user=self.user_asaef, data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'date_prestation': ['La saisie des prestations des mois précédents est close !']}
        )

    def test_ajout_prestation(self):
        famille = FamilleAsaef.objects.get(nom='Tournesol')
        Personne.objects.create(
            famille=famille, nom='Tournesol', prenom='Georgette', role=Role.objects.get(nom='Enfant suivi')
        )
        user_asaef = Utilisateur.objects.get(nom='Champmard')
        famille.suiviasaef.intervenants_asaef.add(user_asaef)
        famille.suiviasaef.save()
        self.client.force_login(self.user_asaef)
        add_url = reverse('asaef-prestation-famille-add', args=[famille.pk])
        self.client.get(add_url)
        response = self.client.post(add_url, data={
            'lib_prestation': LibellePrestation.objects.filter(unite='asaef').first().pk,
            'date_prestation': date.today(),
            'duree': '1:00',
            'theme': 'Test',
            'texte': 'Un essai'
        })
        self.assertRedirects(response, reverse('asaef-prestation-famille-list', args=[famille.pk]))
        prest = famille.prestations.first()
        self.assertEqual(prest.duree, timedelta(hours=1))
        self.assertEqual(prest.intervenant, self.user_asaef)
        self.assertEqual(prest.lib_prestation.code, 'p09')
        response = self.client.get(reverse('asaef-prestation-edit', args=[famille.pk, prest.pk]))
        self.assertContains(
            response,
            '<input type="text" name="duree" value="01:00" class="form-control"'
            ' required id="id_duree" data-dbdp-config',
            html=False
        )
        response = self.client.get(reverse('asaef-prestation-adminlist'))
        # Just check total
        self.assertContains(response, '<th class="text-center">01:00</th>')

    def test_dossier_archivable(self):
        famille = FamilleAsaef.objects.get(nom='Tournesol')
        self.user_asaef.user_permissions.add(Permission.objects.get(codename='export_aemo'))
        self.assertFalse(famille.can_be_deleted(self.user_asaef))
        famille.suiviasaef.date_fin_suivi = date.today() - timedelta(days=370)
        if date.today().month == 1:
            # Impossible de supprimer des familles au mois de janvier
            # (temps des statistiques de l'année précédente)
            self.assertFalse(famille.can_be_deleted(self.user_asaef))
        else:
            self.assertTrue(famille.can_be_deleted(self.user_asaef))

    def test_suivi_deux_ope_referents(self):
        famille = FamilleAsaef.objects.get(nom='Tournesol')
        self.client.force_login(self.user_asaef)
        suivi_url = reverse('asaef-famille-suivi', args=[famille.pk])
        response = self.client.post(
            suivi_url,
            data={
                'ope_referent': Contact.objects.get(nom='Rathfeld').pk,
                'ope_referent2': Contact.objects.create(
                    nom='Doe',
                    prenom='John',
                    service=Service.objects.get(sigle='OPEN')).pk,
                'date_demande': '2018-10-24',
                'etape_suivi': 'demande',
                'service_orienteur': 'ope',
            }
        )
        self.assertRedirects(response, suivi_url)
        self.assertEqual(
            famille.suivi.get_ope_referents(),
            'Rathfeld Christophe (OPEN) / Doe John (OPEN)'
        )

    def test_service_anonceur(self):
        famille = FamilleAsaef.objects.get(nom='Tournesol')
        referent = Contact.objects.get(nom='Rathfeld')
        form = SuiviAsaefForm(instance=famille.suivi, data={'ope_referent': referent.pk})
        self.assertTrue(form.is_valid())
        form.save()
        famille.suiviasaef.refresh_from_db()
        self.assertEqual(famille.suivi.service_orienteur, 'open')

        form = SuiviAsaefForm(instance=famille.suivi, data={'ope_referent2': referent.pk})
        self.assertTrue(form.is_valid())
        form.save()
        famille.suiviasaef.refresh_from_db()
        self.assertEqual(famille.suivi.service_orienteur, 'open')

    def test_intervenants_asaef(self):
        today = date.today()
        famille = FamilleAsaef.objects.get(nom='Tournesol')
        group_asaef = Group.objects.create(name='asaef')
        for i in range(4):
            user = Utilisateur.objects.create_user(
                username=f'Utilisateur{i}', nom=f'Utilisateur{i}', password='secret'
            )
            user.user_permissions.add(Permission.objects.get(codename='change_asaef'))
            user.user_permissions.add(Permission.objects.get(codename='view_asaef'))
            user.groups.add(group_asaef)
            Intervention.objects.create(suivi=famille.suivi, educ=user, role='coord', date_debut=today)
            famille.suivi.intervenants_asaef.add(user)
        self.client.force_login(self.user_asaef)
        response = self.client.get(reverse('asaef-famille-suivi', args=[famille.pk]))
        for i in range(4):
            self.assertContains(response, f'Utilisateur{i} ', html=False)
        self.assertContains(
            response,
            f'<a class="btn btn-sm btn-outline-primary" '
            f'href="/asaef/famille/{famille.suivi.pk}/intervenants/">Modifier</a>',
            html=True
        )

        Intervention.objects.filter(educ__nom='Utilisateur0').update(date_fin=today)
        self.client.force_login(self.user_asaef)
        response = self.client.get(reverse('asaef-famille-suivi', args=[famille.pk]))
        self.assertNotContains(response, 'Utilisateur0', html=False)


class AgendaAsaefTests(TestCase):
    def setUp(self):
        self.famille = FamilleAsaef.objects.create_famille(
            nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart',
        )
        self.user_asaef = Utilisateur.objects.create_user('user_asaef', 'user_asaef@example.org')
        self.user_asaef.user_permissions.add(Permission.objects.get(codename='view_asaef'))
        self.client.force_login(self.user_asaef)

    def test_affichage_agenda(self):
        suivi_url = reverse('asaef-famille-agenda', args=[self.famille.pk])
        response = self.client.get(suivi_url)
        titres = ['ÉTUDE', 'SOUTIEN INTENSIF', 'AUTONOMISATION', 'CONCLUSION', 'PROLONGATION', 'FIN']
        for titre in titres:
            self.assertContains(response, titre, html=False)

    def test_saisie_par_erreur_view(self):
        agenda_url = reverse('asaef-famille-agenda', args=[self.famille.pk])
        response = self.client.post(agenda_url, data={'motif_fin_suivi': 'erreur'})
        self.assertRedirects(response, agenda_url)
        self.famille.refresh_from_db()
        self.assertEqual(self.famille.suiviasaef.date_fin_suivi, date.today())
        self.assertEqual(self.famille.suiviasaef.motif_fin_suivi, 'erreur')

    def test_saisie_par_erreur_form(self):
        data = {'motif_fin_suivi': 'erreur'}
        form = AgendaAsaefForm(data)
        self.assertTrue(form.is_valid())

    def test_dates_obligatoires(self):
        dates_fields = [
            'date_demande', 'date_analyse', 'date_bilan_analyse', 'date_debut_suivi', 'date_prolongation',
            'date_fin_suivi'
        ]
        form_data = {field_name: '2019-01-15'for field_name in dates_fields}
        for field, etape in SuiviAsaef.WORKFLOW.items():
            if etape.oblig is True:
                etape_preced_oblig = SuiviAsaef.WORKFLOW[etape.preced_oblig]
                form = AgendaAsaefForm(data={**form_data, etape_preced_oblig.date_nom(): ''})
                self.assertFalse(form.is_valid())
                self.assertEqual(
                    form.errors['__all__'],
                    ['La date «{}» est obligatoire'.format(etape_preced_oblig)]
                )
            else:
                form = AgendaAsaefForm(data={**form_data, 'motif_fin_suivi': 'placement', 'autoevaluation': 6})
                self.assertTrue(form.is_valid())

    def test_dates_non_chronologiques(self):
        data = {field_name: '2019-01-15' for field_name in AgendaAsaefForm._meta.fields if
                field_name not in ['motif_fin_suivi', 'autoevaluation']}
        date_preced = None
        for field_name in AgendaAsaefForm._meta.fields:
            if field_name in ['date_demande', 'motif_fin_suivi', 'autoevaluation']:
                date_preced = field_name
                continue
            data2 = dict(data)
            data2[date_preced] = '2019-01-16'
            form = AgendaAsaefForm(data2)
            self.assertFalse(form.is_valid())
            self.assertEqual(
                form.errors,
                {'__all__': ["La date «{}» ne respecte pas l’ordre chronologique!".format(
                    form.__dict__["fields"][field_name].label)]}
            )
            date_preced = field_name

    def test_abandon(self):
        data = {'motif_fin_suivi': 'abandon_asaef'}
        form = AgendaAsaefForm(instance=self.famille.suiviasaef, data=data)
        self.assertTrue(form.is_valid())
        form.save()
        self.assertEqual(form.cleaned_data['date_fin_suivi'], date.today())
        self.famille.suiviasaef.refresh_from_db()
        self.assertEqual(self.famille.suiviasaef.date_fin_suivi, date.today())

    def test_abandon_indisponible(self):
        data = {'motif_fin_suivi': 'abandon_indisponible'}
        form = AgendaAsaefForm(instance=self.famille.suiviasaef, data=data)
        self.assertTrue(form.is_valid())
        form.save()
        self.assertEqual(form.cleaned_data['date_fin_suivi'], date.today())
        self.famille.suiviasaef.refresh_from_db()
        self.assertEqual(self.famille.suiviasaef.date_fin_suivi, date.today())

    def test_affichage_abandon_indisponibilite(self):
        agenda_url = reverse('asaef-famille-agenda', args=[self.famille.pk])
        response = self.client.get(agenda_url)
        self.assertContains(
            response, '<option value="abandon_indisponible">Manque de disponibilité</option>', html=True)

    def test_date_bilan_analyse_affichage_cache(self):
        self.famille.suiviasaef.date_bilan_analyse = date.today()
        self.famille.suiviasaef.save()
        agenda_url = reverse('asaef-famille-agenda', args=[self.famille.pk])
        response = self.client.get(agenda_url)
        ddate = date.today().strftime("%Y-%m-%d")
        html = (
            f'<input type="hidden" name="date_bilan_analyse" value="{ddate}" '
            'class="form-control" id="id_date_bilan_analyse">'
        )
        self.assertContains(response, html, html=True)

    def test_date_bilan_analyse_obligatoire_pour_affichage_bouton_contrat(self):
        agenda_url = reverse('asaef-famille-agenda', args=[self.famille.pk])
        response = self.client.get(agenda_url)
        url = reverse('asaef-print-contrat-collaboration', args=[self.famille.pk])
        self.assertContains(
            response,
            f'<a class="btn btn-outline-primary btn-sm disabled " href="{url}">Contrat collab.</a>',
            html=True
        )
        # with date_bilan_analyse
        self.famille.suiviasaef.date_bilan_analyse = date.today()
        self.famille.suiviasaef.save()
        response = self.client.get(agenda_url)
        self.assertContains(
            response,
            f'<a class="btn btn-outline-primary btn-sm " href="{url}">Contrat collab.</a>',
            html=True
        )

    def test_date_bilan_analyse_obligatoire_pour_saisir_date_debut_suivi(self):
        form = AgendaAsaefForm(
            data={'famille': self.famille, 'date_demande': '27.09.2022', 'date_analyse': '27.09.2022',
                  'date_bilan_analyse': '', 'date_debut_suivi': '27.09.2022'}
        )
        self.assertFalse(form.is_valid())

        form = AgendaAsaefForm(
            data={'famille': self.famille, 'date_demande': '27.09.2022', 'date_analyse': '27.09.2022',
                  'date_bilan_analyse': '27.09.2022', 'date_debut_suivi': '27.09.2022'}
        )
        self.assertTrue(form.is_valid())

    def test_autoevaluation_obligatoire_a_la_cloture(self):
        form = AgendaAsaefForm(
            data={'famille': self.famille, 'date_demande': '27.09.2022', 'date_analyse': '27.09.2022',
                  'date_bilan_analyse': '27.09.2022', 'date_debut_suivi': '27.09.2022', 'date_fin_suivi': '30.09.2023',
                  'motif_fin_suivi': 'autres', 'autoevaluation': None
                  }
        )
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['__all__'], ['Auto-évaluation: La saisie est obligatoire.'])

    def test_autoevaluation_interdite_si_non_cloture(self):
        form = AgendaAsaefForm(
            data={'famille': self.famille, 'date_demande': '27.09.2022', 'date_analyse': '27.09.2022',
                  'date_bilan_analyse': '27.09.2022', 'date_debut_suivi': '27.09.2022', 'date_fin_suivi': '',
                  'motif_fin_suivi': 'autres', 'autoevaluation': 6
                  }
        )
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['__all__'], ['Auto-évaluation: Une date de fin de suivi est obligatoire.'])


@tag('pdf')
class PdfTests(ASAEFTestBase):

    def extract_page_pdf(self, response, num_page):
        pdf_reader = PdfReader(BytesIO(response.getvalue()))
        page = pdf_reader.pages[num_page]
        return page.extract_text()

    def _test_print_pdf(self, url, filename):
        self.client.force_login(self.user_asaef)
        response = self.client.get(url)
        self.assertEqual(
            response['content-disposition'],
            'attachment; filename="{}"'.format(filename)
        )
        self.assertEqual(response['content-type'], 'application/pdf')
        self.assertGreater(len(response.getvalue()), 200)

    def test_print_analyse_demande(self):
        fam = FamilleAsaef.objects.get(nom='Tournesol')
        self._test_print_pdf(
            reverse('asaef-print-analyse-demande', args=(fam.pk,)),
            'tournesol_analyse_demande.pdf'
        )

    def test_print_contrat_collaboration(self):
        fam = FamilleAsaef.objects.get(nom='Tournesol')
        self._test_print_pdf(
            reverse('asaef-print-contrat-collaboration', args=(fam.pk,)),
            'tournesol_contrat_collaboration.pdf'
        )

    def test_print_coord_famille(self):
        fam = FamilleAsaef.objects.get(nom='Tournesol')
        self._test_print_pdf(
            reverse('asaef-print-coord-famille', args=(fam.pk,)),
            'tournesol_coordonnees.pdf'
        )

    def test_print_journal(self):
        fam = FamilleAsaef.objects.get(nom='Tournesol')
        today = date.today()
        date_from_prev_month = today - timedelta(days=today.day - 1)
        date_from_prev_year = today - timedelta(days=365)
        # Ajoute qques prestations familiales
        for dt, descr in [(today, "Prestation d'aujourd'hui"),
                          (date_from_prev_month, "Prestation il y a un mois"),
                          (date_from_prev_year, "Prestation de l'année passée")]:
            PrestationAsaef.objects.create(
                famille=fam,
                intervenant=self.user_asaef,
                lib_prestation=LibellePrestation.objects.filter(unite='asaef').first(),
                date_prestation=dt,
                duree='1:10',
                texte=descr)
        self.client.force_login(self.user_asaef)
        response = self.client.get(reverse('asaef-print-journal', args=[fam.pk]))
        pdf = self.extract_page_pdf(response, 0)
        self.assertIn("Prestation d'aujourd'hui", pdf)
        self.assertIn("Prestation de l'année passée", pdf)

        response = self.client.get(reverse('asaef-print-journal', args=[fam.pk]) + '?selector=two_months')
        pdf = self.extract_page_pdf(response, 0)
        self.assertNotIn("Prestation de l'année passée", pdf)

    def test_print_bilan_1mois(self):
        fam = FamilleAsaef.objects.get(nom='Tournesol')
        bilan = BilanAsaef.objects.create(
            famille=fam, date=date.today(), typ='1mois',
            moyens='<p>Les <span style="font-family: sans-serif;">meilleurs</span></p>',
        )
        url = reverse('asaef-bilan-print', args=(fam.pk, bilan.pk))
        self._test_print_pdf(url, 'tournesol_bilan_soutien_int.pdf')

    def test_print_bilan_3mois(self):
        fam = FamilleAsaef.objects.get(nom='Tournesol')
        bilan = BilanAsaef.objects.create(famille=fam, date=date.today(), typ='3mois')
        url = reverse('asaef-bilan-print', args=(fam.pk, bilan.pk))
        self._test_print_pdf(url, 'tournesol_bilan_autonom.pdf')

    def test_print_bilan_final(self):
        fam = FamilleAsaef.objects.get(nom='Tournesol')
        bilan = BilanAsaef.objects.create(famille=fam, date=date.today(), typ='final')
        url = reverse('asaef-bilan-print', args=(fam.pk, bilan.pk))
        self._test_print_pdf(url, 'tournesol_bilan_final.pdf')

    def test_print_prolongation(self):
        fam = FamilleAsaef.objects.get(nom='Tournesol')
        prolon = ProlongationAsaef.objects.create(famille=fam, date_debut=date.today())
        url = reverse('asaef-prolongation-print', args=(fam.pk, prolon.pk,))
        self._test_print_pdf(url, 'tournesol_prolongation.pdf')


class PrestationAsaefTests(ASAEFTestBase):

    def test_nouvelle_prestation_fam(self):
        famille = FamilleAsaef.objects.get(nom='Tournesol')
        famille.suiviasaef.date_analyse = date.today()
        famille.suiviasaef.save()
        Personne.objects.create(
            famille=famille, nom='Tournesol', prenom='Georgette', role=Role.objects.get(nom='Enfant suivi')
        )
        self.client.force_login(self.user_asaef)
        add_url = reverse('asaef-prestation-famille-add', args=[famille.pk])
        response = self.client.post(add_url, data={
            'lib_prestation': LibellePrestation.objects.filter(unite='asaef').first().pk,
            'date_prestation': date.today(),
            'duree': '1:10',
            'theme': 'Un test',
            'texte': 'Un essai',
        })
        self.assertRedirects(response, reverse('asaef-prestation-famille-list', args=[famille.pk]))
        self.assertEqual(famille.temps_total_prestations_reparti(), timedelta(hours=1, minutes=10))
        self.assertEqual(PrestationAsaef.objects.count(), 1)

    def test_nouvelle_prestation_gen(self):
        famille = FamilleAsaef.objects.get(nom='Tournesol')
        famille.suiviasaef.date_analyse = date.today()
        famille.suiviasaef.save()
        Personne.objects.create(
            famille=famille, nom='Tournesol', prenom='Georgette', role=Role.objects.get(nom='Enfant suivi')
        )
        self.client.force_login(self.user_asaef)
        add_url = reverse('asaef-prestation-gen-add')
        response = self.client.post(add_url, famille=None, data={
            'lib_prestation': LibellePrestation.objects.get(code='p11').pk,
            'date_prestation': date.today(),
            'duree': '3:00',
            'theme': 'Un test',
            'texte': 'Un essai',
        })
        self.assertRedirects(response, reverse('asaef-prestation-gen-list'))
        self.assertEqual(format_duree(famille.total_mensuel_prest_gen(date.today())), '03:00')
        self.assertEqual(PrestationAsaef.objects.count(), 1)

    def test_prestations_melangees(self):
        famille = FamilleAsaef.objects.get(nom='Tournesol')
        Personne.objects.create(
            famille=famille, nom='Tournesol', prenom='Georgette', role=Role.objects.get(nom='Enfant suivi')
        )
        famille.suiviasaef.date_analyse = date.today()
        famille.suiviasaef.save()

        PrestationAsaef.objects.create(
            famille=famille,
            lib_prestation=LibellePrestation.objects.filter(unite='asaef').first(),
            date_prestation= date.today(),
            duree='1:10',
            theme='Un test',
            texte='Un essai'
        )
        PrestationAsaef.objects.create(
            famille=None,
            lib_prestation=LibellePrestation.objects.get(code='p11'),
            date_prestation=date.today(),
            duree='3:00',
            theme='Un test',
            texte='Un essai'
        )
        self.assertEqual(famille.temps_total_prestations_reparti(), timedelta(hours=1, minutes=10))
        self.assertEqual(PrestationAsaef.objects.count(), 2)
        self.assertEqual(format_duree(famille.total_mensuel_prest_gen(date.today())), '03:00')

        # Ajoute une deuxième famille Asaef active
        famille2 = FamilleAsaef.objects.create_famille(
            nom='Montesquieu', rue='Rue des Plantes 4', npa=2000, localite='Neuchâtel',
            autorite_parentale='conjointe',
        )
        famille2.suiviasaef.date_analyse = date.today()
        famille2.suiviasaef.save()

        # Ajoute une prestation générale
        PrestationAsaef.objects.create(
            famille=None,
            lib_prestation=LibellePrestation.objects.get(code='p11'),
            date_prestation=date.today(),
            duree='5:00',
            theme='Un test',
            texte='Un essai'
        )
        self.assertEqual(format_duree(famille.total_mensuel_prest_gen(date.today())), '05:30')
        self.assertEqual(format_duree(famille.temps_total_prestations_reparti()), "01:10")

        self.assertEqual(format_duree(famille2.total_mensuel_prest_gen(date.today())), '05:30')
        self.assertEqual(format_duree(famille2.temps_total_prestations()), "00:00")

    def test_prestation_menu(self):

        famille = FamilleAsaef.objects.get(nom='Tournesol')
        famille.suiviasaef.date_analyse = date.today()
        famille.suiviasaef.save()
        Personne.objects.create_personne(
            famille=famille, role=Role.objects.get(nom='Enfant suivi'),
            nom='Tournesol', prenom='Junior', genre='M', date_naissance=date(2001, 4, 26),
        )
        # Ajoute une prestation familiale
        PrestationAsaef.objects.create(
            famille=famille,
            intervenant=self.user_asaef,
            lib_prestation=LibellePrestation.objects.filter(unite='asaef').first(),
            date_prestation=date.today(),
            duree='1:10'
        )
        # Ajoute une prestation générale
        PrestationAsaef.objects.create(
            intervenant=self.user_asaef,
            lib_prestation=LibellePrestation.objects.get(code='p11'),
            date_prestation=date.today(),
            duree='3:00',
        )
        self.assertEqual(PrestationAsaef.objects.all().count(), 2)

        self.client.force_login(self.user_asaef)
        response = self.client.get(reverse('asaef-prestation-menu'))
        self.assertEqual(response.status_code, 200)
        # Prestations familiales
        self.assertContains(
            response,
            '<tr><th>Familles</th><th>Mes prestations</th><th>Prestations ASAEF</th>'
            '<th width="5%" align="center">Actions</th></tr>',
            html=True
        )
        journal_url = reverse('asaef-journal-list', args=[famille.pk])
        self.assertContains(
            response,
            f'<td><a href="{journal_url}" title="vers le Journal">Tournesol</a> - Château1, 2000 Moulinsart</td>',
            html=True
        )

        # Heures par intervenant
        self.assertEqual(format_duree(response.context['familles'][0].user_prest), '01:10')

        # Total ASAEF
        self.assertEqual(format_duree(response.context['familles'][0].temps_total_prestations()), '01:10')

    def test_affichage_prestations_generales(self):
        PrestationAsaef.objects.create(
            famille=None,
            intervenant=self.user_asaef,
            lib_prestation=LibellePrestation.objects.get(code='p11'),
            date_prestation='2020-09-15',
            duree='3:00',
        )
        PrestationAsaef.objects.create(
            famille=None,
            intervenant=self.user_asaef,
            lib_prestation=LibellePrestation.objects.get(code='p11'),
            date_prestation='2020-09-16',
            duree='1:30',
        )

        self.client.force_login(self.user_asaef)
        response = self.client.get(reverse('asaef-prestation-gen-list'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['prestations']), 1)
        self.assertEqual(len(response.context['prestations'][0][1]), 2)

        self.assertContains(
            response,
            '<tr><th width="15%">Date</th><th width="10%">Durée</th><th>Référent-e ASAEF</th>'
            '<th>Type de prestation</th><th width="5%" align="center">Actions</th></tr>',
            html=True
        )
        self.assertContains(response, '<td>septembre 2020</td>', html=True)
        self.assertContains(response, '<td>04:30</td>', html=True)
        self.assertContains(response, '<td>15.09.2020</td>', html=True)
        self.assertContains(response, '<td>01:30</td>', html=True)

    def test_recap_perso(self):
        famille = FamilleAsaef.objects.get(nom='Tournesol')
        Personne.objects.create_personne(
            famille=famille, role=Role.objects.get(nom='Enfant suivi'),
            nom='Tournesol', prenom='Junior', genre='M', date_naissance=date(2001, 4, 26),
        )
        # Ajoute une prestation familiale
        pr_fam = PrestationAsaef.objects.create(
            famille=famille,
            intervenant=self.user_asaef,
            lib_prestation=LibellePrestation.objects.filter(unite='asaef').first(),
            date_prestation=date.today(),
            duree='1:10'
        )
        # Ajoute une prestation générale
        pr_gen = PrestationAsaef.objects.create(
            intervenant=self.user_asaef,
            lib_prestation=LibellePrestation.objects.get(code='p11'),
            date_prestation=date.today(),
            duree='3:00',
        )
        self.client.force_login(self.user_asaef)
        response = self.client.get(reverse('asaef-prestation-recap-perso'))
        str_test = f"""
            <tr>
            <td>{format_d_m_Y(date.today())}</td><td>01:10</td><td>Tournesol</td>
            <td>Prestation 9 Étude</td><td>UA</td>
            <td align="center"><a href="{pr_fam.get_edit_url()}">
            <img src="/static/admin/img/icon-changelink.svg"></a></td>
            </tr>
        """
        self.assertContains(response, str_test, html=True)
        str_test = f"""
            <tr>
            <td>{format_d_m_Y(date.today())}</td><td>03:00</td><td>--</td>
            <td>Prestation 11</td><td>UA</td>
            <td align="center"><a href="{pr_gen.get_edit_url()}">
            <img src="/static/admin/img/icon-changelink.svg"></a></td>
            </tr>
        """
        self.assertContains(response, str_test, html=True)

        # Récapitulatif
        self.assertContains(
            response,
            '<tr><th>Total prestations p09</th><td align="right">01:10</td></tr>',
            html=True
        )
        self.assertContains(
            response,
            '<tr><th>Total prestations p10</th><td align="right">00:00</td></tr>',
            html=True
        )
        self.assertContains(
            response,
            '<tr><th>Total prestations p11</th><td align="right">03:00</td></tr>',
            html=True
        )
        self.assertContains(
            response,
            '<tr><th>Total prestations p12</th><td align="right">00:00</td></tr>',
            html=True
        )

    def test_supprime_prestation_generale(self):
        famille = FamilleAsaef.objects.get(nom='Tournesol')
        Personne.objects.create_personne(
            famille=famille, role=Role.objects.get(nom='Enfant suivi'),
            nom='Tournesol', prenom='Junior', genre='M', date_naissance=date(2001, 4, 26),
        )
        # Ajoute une prestation générale
        prest = PrestationAsaef.objects.create(
            intervenant=self.user_asaef,
            lib_prestation=LibellePrestation.objects.get(code='p11'),
            date_prestation=date.today(),
            duree='3:00',
        )
        delete_url = reverse('asaef-prestation-delete', args=[0, prest.pk])
        self.client.force_login(self.user_asaef)
        response = self.client.post(delete_url)
        self.assertRedirects(response, reverse('asaef-prestation-menu'))

    def test_supprime_prestation_familiale(self):
        famille = FamilleAsaef.objects.get(nom='Tournesol')
        Personne.objects.create_personne(
            famille=famille, role=Role.objects.get(nom='Enfant suivi'),
            nom='Tournesol', prenom='Junior', genre='M', date_naissance=date(2001, 4, 26),
        )
        # Ajoute une prestation familiale
        prest = PrestationAsaef.objects.create(
            famille=famille,
            intervenant=self.user_asaef,
            lib_prestation=LibellePrestation.objects.filter(unite='asaef').first(),
            date_prestation=date.today(),
            duree='1:10'
        )
        delete_url = reverse('asaef-prestation-delete', args=[famille.pk, prest.pk])
        self.client.force_login(self.user_asaef)
        response = self.client.post(delete_url)
        self.assertRedirects(response, reverse('asaef-prestation-famille-list', args=[famille.pk]))

    def test_affichage_journal(self):
        fam1 = FamilleAsaef.objects.get(nom='Tournesol')
        Personne.objects.create_personne(
            famille=fam1, role=Role.objects.get(nom='Enfant suivi'),
            nom='Tournesol', prenom='Junior', genre='M', date_naissance=date(2001, 4, 26),
        )
        PrestationAsaef.objects.create(
            famille=fam1,
            intervenant=self.user_asaef,
            lib_prestation=LibellePrestation.objects.filter(unite='asaef').first(),
            date_prestation=date.today(),
            texte="Un essai",
            duree='1:10'
        )
        fam2 = FamilleAsaef.objects.create_famille(**{
            'nom': 'Martin',
            'rue': 'Rue des Fleurs 4',
            'npa': '2000',
            'localite': 'Neuchâtel',
            'autorite_parentale': 'conjointe',
        })
        Personne.objects.create_personne(
            famille=fam2, role=Role.objects.get(nom='Enfant suivi'),
            nom='Martinet', prenom='Junior', genre='M', date_naissance=date(2002, 2, 2)
        )
        PrestationAsaef.objects.create(
            famille=fam2,
            intervenant=self.user_asaef,
            lib_prestation=LibellePrestation.objects.filter(unite='asaef').first(),
            date_prestation=date.today(),
            texte="Une tentative",
            duree='1:10'
        )
        PrestationAsaef.objects.create(
            famille=fam2,
            intervenant=self.user_asaef,
            lib_prestation=LibellePrestation.objects.filter(unite='asaef').first(),
            date_prestation=date.today(),
            texte="<p>Une deuxième tentative</p>",
            duree='1:10'
        )
        self.client.force_login(self.user_asaef)
        response = self.client.get(reverse('asaef-journal-list', args=[fam1.pk]))
        self.assertEqual(len(response.context['object_list']), 1)

        response = self.client.get(reverse('asaef-journal-list', args=[fam2.pk]))
        self.assertEqual(len(response.context['object_list']), 2)

        self.assertContains(response, '<p>Une deuxième tentative</p>', html=True)

    def test_droit_modification_journal(self):
        fam = FamilleAsaef.objects.get(nom='Tournesol')
        Personne.objects.create_personne(
            famille=fam, role=Role.objects.get(nom='Enfant suivi'),
            nom='Tournesol', prenom='Junior', genre='M', date_naissance=date(2001, 4, 26),
        )
        prest = PrestationAsaef.objects.create(
            famille=fam,
            intervenant=self.user_asaef,
            lib_prestation=LibellePrestation.objects.filter(unite='asaef').first(),
            date_prestation=date.today(),
            texte="Un essai",
            duree='1:10'
        )
        self.client.force_login(self.user_externe)
        response = self.client.get(reverse('asaef-prestation-edit', args=[fam.pk, prest.pk]))
        self.assertEqual(response.status_code, 403)

        user_asaef2 = Utilisateur.objects.create_user(username='asaef2', email='asaef2@example.org')
        user_asaef2.user_permissions.add(Permission.objects.get(codename='view_asaef'))
        self.client.force_login(user_asaef2)
        response = self.client.get(reverse('asaef-prestation-edit', args=[fam.pk, prest.pk]))
        self.assertEqual(response.status_code, 403)

        self.client.force_login(self.user_asaef)
        response = self.client.get(reverse('asaef-prestation-edit', args=[fam.pk, prest.pk]))
        self.assertEqual(response.status_code, 200)

    def test_affichage_lien_modification(self):
        fam = FamilleAsaef.objects.get(nom='Tournesol')
        Personne.objects.create_personne(
            famille=fam, role=Role.objects.get(nom='Enfant suivi'),
            nom='Tournesol', prenom='Junior', genre='M', date_naissance=date(2001, 4, 26),
        )
        prest1 = PrestationAsaef.objects.create(
            famille=fam,
            intervenant=self.user_asaef,
            lib_prestation=LibellePrestation.objects.filter(unite='asaef').first(),
            date_prestation=date.today(),
            texte="Un essai",
            duree='1:10'
        )

        prest2 = PrestationAsaef.objects.create(
            famille=fam,
            intervenant=self.user_asaef,
            lib_prestation=LibellePrestation.objects.filter(unite='asaef').first(),
            date_prestation='2020-01-01',
            texte="Un essai passé",
            duree='1:10'
        )
        prest2.refresh_from_db()
        self.assertFalse(prest2.check_date_editable(prest2.date_prestation, self.user_asaef))

        self.client.force_login(self.user_externe)
        response = self.client.get(reverse('asaef-journal-list', args=[fam.pk]))
        self.assertEqual(response.status_code, 403)

        user_asaef2 = Utilisateur.objects.create_user(username='asaef2', email='asaef2@example.org')
        user_asaef2.user_permissions.add(Permission.objects.get(codename='view_asaef'))
        self.client.force_login(user_asaef2)
        response = self.client.get(reverse('asaef-journal-list', args=[fam.pk]))
        self.assertEqual(response.status_code, 200)
        prest1_code = f'<a role="button" href="{prest1.get_edit_url()}">' \
                      f'<img src="/static/admin/img/icon-changelink.svg"></a>'
        prest2_code = f'<a role="button" href="{prest2.get_edit_url()}">' \
                      f'<img src="/static/admin/img/icon-changelink.svg"></a>'
        self.assertNotContains(response, prest1_code, html=True)
        self.assertNotContains(response, prest2_code, html=True)

        self.client.force_login(self.user_asaef)
        response = self.client.get(reverse('asaef-journal-list', args=[fam.pk]))
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, prest1_code, html=True)
        self.assertNotContains(response, prest2_code, html=True)

    def test_affichage_par_defaut_form_saisie(self):
        fam = FamilleAsaef.objects.get(nom='Tournesol')
        Personne.objects.create_personne(
            famille=fam, role=Role.objects.get(nom='Enfant suivi'),
            nom='Tournesol', prenom='Junior', genre='M', date_naissance=date(2001, 4, 26),
        )
        PrestationAsaef.objects.create(
            famille=fam,
            intervenant=self.user_asaef,
            lib_prestation=LibellePrestation.objects.filter(unite='asaef').first(),
            date_prestation=date.today(),
            texte="",
            duree='1:10')
        self.client.force_login(self.user_asaef)
        response = self.client.get(reverse('asaef-journal-list', args=[fam.pk]))
        self.assertContains(
            response,
            '<div class="no-text">Pas de texte saisi<div>',
            html=True
        )

    def test_delai_saisie_prestation_pour_educs(self):
        fam = FamilleAsaef.objects.get(nom='Tournesol')
        prestation = LibellePrestation.objects.filter(unite='asaef').first()
        Personne.objects.create_personne(
            famille=fam, role=Role.objects.get(nom='Enfant suivi'),
            nom='Tournesol', prenom='Junior', genre='M', date_naissance=date(2001, 4, 26),
        )
        mock_date = date(2023, 2, PrestationAsaef.max_jour)
        with freeze_time(mock_date):
            data = dict(duree='3:40', date_prestation="2023-01-31", lib_prestation=prestation.pk)
            form = PrestationAsaefForm(famille=fam, user=self.user_asaef, data=data)
            self.assertTrue(form.is_valid())

        mock_date = date(2023, 2, PrestationAsaef.max_jour + 1)
        with freeze_time(mock_date):
            data = dict(duree='3:40', date_prestation="2023-01-31", lib_prestation=prestation.pk)
            form = PrestationAsaefForm(famille=fam, user=self.user_asaef, data=data)
            self.assertFalse(form.is_valid())

    def test_delai_saisie_prestation_pour_re(self):
        user_asaef_re = Utilisateur.objects.create_user(
            'user_asaef_re', 'user_asaef_re@example.org', nom='AsaefRe', prenom='PrénomRe', sigle='AP'
        )
        user_asaef_re.user_permissions.add(Permission.objects.get(codename='manage_asaef'))
        fam = FamilleAsaef.objects.get(nom='Tournesol')
        prestation = LibellePrestation.objects.filter(unite='asaef').first()
        Personne.objects.create_personne(
            famille=fam, role=Role.objects.get(nom='Enfant suivi'),
            nom='Tournesol', prenom='Junior', genre='M', date_naissance=date(2001, 4, 26),
        )
        mock_date = date(2023, 2, PrestationAsaef.max_jour_re)
        with freeze_time(mock_date):
            data = dict(duree='3:40', date_prestation="2023-01-31", lib_prestation=prestation.pk)
            form = PrestationAsaefForm(famille=fam, user=user_asaef_re, data=data)
            self.assertTrue(form.is_valid())

        mock_date = date(2023, 2, PrestationAsaef.max_jour_re + 1)
        with freeze_time(mock_date):
            data = dict(duree='3:40', date_prestation="2023-01-31", lib_prestation=prestation.pk)
            form = PrestationAsaefForm(famille=fam, user=user_asaef_re, data=data)
            self.assertFalse(form.is_valid())

    def test_derogation_saisie_tardive(self):
        today = date.today()
        demain = today + timedelta(days=1)
        fam = FamilleAsaef.objects.get(nom='Tournesol')
        prestation = LibellePrestation.objects.filter(unite='asaef').first()
        Personne.objects.create_personne(
            famille=fam, role=Role.objects.get(nom='Enfant suivi'),
            nom='Tournesol', prenom='Junior', genre='M', date_naissance=date(2001, 4, 26),
        )
        data = dict(duree='3:40', date_prestation="2023-01-31", lib_prestation=prestation.pk)
        with freeze_time(demain):
            form = PrestationAsaefForm(famille=fam, user=self.user_asaef, data=data)
            self.assertFalse(form.is_valid())
            DerogationSaisieTardive.objects.create(
                educ=self.user_asaef, duree=[demain, demain + timedelta(days=1)]
            )
            form = PrestationAsaefForm(famille=fam, user=self.user_asaef, data=data)
            self.assertTrue(form.is_valid())

    def test_widget_formulaire_prestation(self):
        form = PrestationAsaefForm()
        self.assertEqual(form.fields['texte'].widget.__class__.__name__, 'TinyMCE')

    def test_valeur_par_defaut(self):
        today = date.today()
        famille = FamilleAsaef.objects.get(nom="Tournesol")
        Personne.objects.create(
            famille=famille, nom='Tournesol', prenom='Georgette', role=Role.objects.get(nom='Enfant suivi')
        )
        self.client.force_login(self.user_asaef)
        response = self.client.get(reverse('asaef-prestation-famille-add', args=[famille.pk]))
        self.assertContains(
            response,
            f'<input type="text" name="date_prestation" value="{today.strftime("%Y-%m-%d")}"',
            html=False
        )
        self.assertContains(
            response,
            '<input type="text" name="duree" value="00:00"',
            html=False
        )


class BilanAsaefTests(ASAEFTestBase):

    def test_creation(self):
        famille = FamilleAsaef.objects.get(nom="Tournesol")
        pere = famille.membres.get(role__nom='Père')
        create_kwargs = {'famille': famille, 'typ': '1mois', 'date': date.today(),
                         'autres': "Pierre Paul Jacques",
                         'destinataires': [pere.pk], 'moyens': "Les meilleurs"}
        self.client.force_login(self.user_asaef)
        url = reverse('asaef-bilan-add', args=[famille.pk, '1mois'])
        response = self.client.post(url, data=create_kwargs)
        self.assertEqual(response.status_code, 302)
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        bilan = BilanAsaef.objects.get(famille=famille, typ='1mois')
        self.assertEqual(bilan.date, date.today())
        self.assertEqual(bilan.famille, famille)
        self.assertEqual(bilan.autres, "Pierre Paul Jacques")
        self.assertQuerySetEqual(bilan.destinataires.all(), [pere])
        self.assertEqual(bilan.moyens, "Les meilleurs")

    def test_edition(self):
        famille = FamilleAsaef.objects.get(nom="Tournesol")
        pere = famille.membres.get(role__nom='Père')
        create_kwargs = {'famille': famille, 'typ': '1mois', 'date': date.today(),
                         'autres': "Pierre Paul Jacques"}
        bilan = BilanAsaef.objects.create(**create_kwargs)
        self.assertQuerySetEqual(bilan.destinataires.all(), [])

        create_kwargs['autres'] = 'Jean Valjean'
        create_kwargs['destinataires'] = [pere.pk]
        self.client.force_login(self.user_asaef)
        self.client.post(reverse('asaef-bilan-edit', args=[famille.pk, bilan.pk]),
                         data=create_kwargs)
        bilan.refresh_from_db()
        self.assertEqual(bilan.autres, "Jean Valjean")
        self.assertQuerySetEqual(bilan.destinataires.all(), [pere])

    def test_nom_complet_des_intervenants_affiche_par_defaut_bilan(self):
        famille = FamilleAsaef.objects.get(nom="Tournesol")
        famille.suiviasaef.intervenants_asaef.add(Utilisateur.objects.get(nom='Champmard'))
        self.client.force_login(self.user_asaef)
        for type_bilan in ('1mois', '3mois', 'final'):
            url = reverse('asaef-bilan-add', args=[famille.pk, type_bilan])
            response = self.client.get(url, data={
                'famille': famille, 'typ': type_bilan, 'date': date.today()
            })
            self.assertContains(response, 'Champmard Ben')

    def test_nom_complet_des_intervenants_affiche_par_defaut_bilan_analyse(self):
        famille = FamilleAsaef.objects.get(nom="Tournesol")
        famille.suiviasaef.intervenants_asaef.add(Utilisateur.objects.get(nom='Champmard'))
        self.client.force_login(self.user_asaef)
        response = self.client.get(reverse('asaef-bilananalyse-add', args=[famille.pk]))
        self.assertContains(response, 'Champmard Ben')

    def test_nom_complet_des_intervenants_affiche_par_defaut_prolongation(self):
        famille = FamilleAsaef.objects.get(nom="Tournesol")
        famille.suiviasaef.intervenants_asaef.add(Utilisateur.objects.get(nom='Champmard'))
        create_kwargs = {'famille': famille, 'date': date.today()}
        self.client.force_login(self.user_asaef)
        url = reverse('asaef-prolongation-add', args=[famille.pk])
        response = self.client.get(url, data=create_kwargs)
        self.assertContains(response, 'Champmard Ben')


class BilanAnalyseAsaefTests(ASAEFTestBase):

    def test_creation_model(self):
        famille = FamilleAsaef.objects.create_famille(nom="Doe")
        destinataire = Personne.objects.get(nom='Hergé', prenom='Rémy')
        bilan = BilanAnalyseAsaef.objects.create(famille=famille, date=date.today())
        bilan.destinataires.add(destinataire)
        self.assertEqual(famille.bilans_analyses.count(), 1)

    def test_creation_vue(self):
        self.client.force_login(self.user_asaef)
        famille = FamilleAsaef.objects.get(nom='Tournesol')
        url = reverse('asaef-bilananalyse-add', args=[famille.pk])
        response = self.client.post(
            url,
            data={'famille': famille, 'date': date.today(), 'destinataires': [famille.pere().pk]}
        )
        self.assertRedirects(response, reverse('asaef-famille-agenda', args=[famille.pk]))

    def test_update_vue(self):
        self.client.force_login(self.user_asaef)
        famille = FamilleAsaef.objects.get(nom='Tournesol')
        bilan = BilanAnalyseAsaef.objects.create(famille=famille, date=date.today())

        url = reverse('asaef-bilananalyse-edit', args=[famille.pk, bilan.pk])
        response = self.client.post(
            url,
            data={'famille': famille, 'date': date.today(), 'destinataires': [famille.pere().pk]}
        )
        self.assertRedirects(response, reverse('asaef-famille-agenda', args=[famille.pk]))

    def test_creation_form(self):
        famille = FamilleAsaef.objects.get(nom='Tournesol')
        form = BilanAnalyseAsaefForm(
            famille=famille,
            data={'famille': famille, 'date': date.today(), 'destinataires': [famille.pere().pk]}
        )
        self.assertTrue(form.is_valid())
        bilan = form.save()
        self.assertEqual(famille.suiviasaef.date_bilan_analyse, bilan.date)

    def test_update_form(self):
        famille = FamilleAsaef.objects.get(nom='Tournesol')
        destinataire = Personne.objects.get(nom='Hergé', prenom='Rémy')
        bilan = BilanAnalyseAsaef.objects.create(famille=famille, date=date.today())
        bilan.destinataires.add(destinataire)
        form = BilanAnalyseAsaefForm(
            instance=bilan,
            data={'famille': famille, 'date': date.today(), 'destinataires': [famille.pere().pk]}
        )
        self.assertTrue(form.is_valid())

    def test_acces_bilan_analyse_OK(self):
        famille = FamilleAsaef.objects.get(nom="Tournesol")
        bilan = BilanAnalyseAsaef.objects.create(famille=famille, date=date.today())
        self.client.force_login(self.user_asaef)
        response = self.client.get(reverse('asaef-bilananalyse-edit', args=[famille.pk, bilan.pk]))
        self.assertEqual(response.status_code, 200)

    def test_acces_bilan_analyse_erreur(self):
        famille = FamilleAsaef.objects.get(nom="Tournesol")
        bilan = BilanAnalyseAsaef.objects.create(famille=famille, date=date.today())
        user_aemo = Utilisateur.objects.get(username='user_aemo')
        user_aemo.user_permissions.add(Permission.objects.get(codename='view_montagnes'))
        user_aemo.user_permissions.add(Permission.objects.get(codename='change_montagnes'))
        self.client.force_login(user_aemo)
        response = self.client.get(reverse('asaef-bilananalyse-edit', args=[famille.pk, bilan.pk]))
        self.assertEqual(response.status_code, 403)

    def test_edition_bilan_analyse(self):
        famille = FamilleAsaef.objects.get(nom="Tournesol")
        famille.suivi.ope_referent = Contact.objects.get(nom="Rathfeld")
        famille.suivi.ope_referent2 = Contact.objects.create(
            nom="Doe", prenom='John', service=Service.objects.get(sigle='OPEN'))
        famille.suivi.save()

        bilan = BilanAnalyseAsaef.objects.create(famille=famille, date=date.today())
        bilan.destinataires.add(famille.pere())

        edit_url = reverse('asaef-bilananalyse-edit', args=[famille.pk, bilan.pk])
        self.client.force_login(self.user_asaef)
        response = self.client.get(edit_url)
        self.assertContains(response, "Rathfeld Christophe (OPEN) / Doe John (OPEN)")
        response = self.client.post(
            edit_url,
            data={'famille': famille, 'date': date.today(), 'membres_famille': 'Pierre Paul Jacques',
                  'destinataires': [famille.pere().pk]}
        )
        self.assertRedirects(response, reverse('asaef-famille-agenda', args=[famille.pk]))
        famille.refresh_from_db()
        self.assertEqual(famille.bilans_analyses.first().membres_famille, 'Pierre Paul Jacques')


class ListeFamilleAsaefTests(ASAEFTestBase):

    def test_list_asaef_en_cours(self):
        famille = FamilleAsaef.objects.get(nom="Tournesol")
        famille.suiviasaef.date_demande = date.today()
        famille.suiviasaef.date_analyse = date.today()
        famille.suiviasaef.date_debut_suivi = date.today()
        famille.suiviasaef.save()

        self.client.force_login(self.user_asaef)
        response = self.client.get(reverse('asaef-famille-list'))
        suivi_url = reverse("asaef-famille-suivi", args=[famille.pk])
        self.assertContains(
            response,
            f'<a href="{suivi_url}" title="Suivi">Tournesol</a>',
            html=True
        )

    def test_list_asaef_en_attente(self):
        famille = FamilleAsaef.objects.get(nom="Tournesol")
        famille.suiviasaef.date_demande = date.today()
        famille.suiviasaef.save()

        self.client.force_login(self.user_asaef)
        response = self.client.get(reverse('asaef-famille-list'))
        suivi_url = reverse("asaef-famille-suivi", args=[famille.pk])
        self.assertContains(
            response,
            f'<a href="{suivi_url}" title="En attente">Tournesol</a>',
            html=True
        )

    def test_critere_date_analyse_pour_sortir_liste_attente(self):
        famille = FamilleAsaef.objects.get(nom="Tournesol")
        famille.suiviasaef.date_demande = date.today()
        famille.suiviasaef.date_analyse = None
        famille.suiviasaef.save()

        self.client.force_login(self.user_asaef)
        response = self.client.get(reverse('asaef-famille-list'))
        suivi_url = reverse("asaef-famille-suivi", args=[famille.pk])
        self.assertContains(
            response,
            f'<a href="{suivi_url}" title="En attente">Tournesol</a>',
            html=True
        )

        famille.suiviasaef.date_analyse = date.today()
        famille.suiviasaef.save()
        response = self.client.get(reverse('asaef-famille-list'))
        suivi_url = reverse("asaef-famille-suivi", args=[famille.pk])
        self.assertNotContains(
            response,
            f'<a href="{suivi_url}" title="En attente">Tournesol</a>',
            html=True
        )
        self.assertContains(
            response,
            f'<a href="{suivi_url}" title="Suivi">Tournesol</a>',
            html=True
        )


class ProlongationAsaefTests(ASAEFTestBase):

    def test_creation(self):
        famille = FamilleAsaef.objects.get(nom="Tournesol")
        famille.suiviasaef.date_demande = date.today()
        famille.suiviasaef.date_debut_suivi = date.today()
        famille.suiviasaef.save()

        self.client.force_login(self.user_asaef)
        form = ProlongationAsaefForm(data={'famille': famille, 'date_debut': date.today()})
        self.assertTrue(form.is_valid())
        form.save()
        famille.suiviasaef.refresh_from_db()
        self.assertEqual(famille.suiviasaef.date_prolongation, date.today())

    def test_edition(self):
        famille = FamilleAsaef.objects.get(nom="Tournesol")
        famille.suiviasaef.date_demande = date.today()
        famille.suiviasaef.date_debut_suivi = date.today()
        famille.suiviasaef.save()
        prolong = ProlongationAsaef.objects.create(famille=famille, date_debut=date.today())

        self.client.force_login(self.user_asaef)
        edit_url = reverse('asaef-prolongation-edit', args=[famille.pk, prolong.pk])
        response = self.client.post(
            edit_url,
            data={'famille': famille.pk, 'date_debut': date.today(), 'date_fin': date.today()}
        )
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertEqual(response.status_code, 302)
        prolong.refresh_from_db()
        self.assertEqual(prolong.date_fin, date.today())

    def test_affichage_avant_creation(self):
        famille = FamilleAsaef.objects.get(nom="Tournesol")
        famille.suiviasaef.date_debut_suivi = date.today()
        famille.suiviasaef.save()

        self.client.force_login(self.user_asaef)
        response = self.client.get(reverse('asaef-famille-agenda', args=[famille.pk]))
        prolong_url = reverse('asaef-prolongation-add', args=[famille.pk])
        icon = static('admin/img/icon-addlink.svg')
        self.assertContains(
            response,
            f'<a class="btn btn-secondary mb-5" href="{prolong_url}"><img src="{icon}"> Créer une prolongation</a>',
            html=True
        )

    def test_affichage_apres_creation(self):
        famille = FamilleAsaef.objects.get(nom="Tournesol")
        famille.suiviasaef.date_debut_suivi = date.today()
        prolong = ProlongationAsaef.objects.create(famille=famille, date_debut=date.today())
        edit_url = reverse('asaef-prolongation-edit', args=[famille.pk, prolong.pk])
        famille.suiviasaef.date_prolongation = prolong.date_debut
        famille.suiviasaef.save()

        self.client.force_login(self.user_asaef)
        response = self.client.get(reverse('asaef-famille-agenda', args=[famille.pk]))
        icon = static('admin/img/icon-changelink.svg')
        self.assertContains(response,
                            f'<a href="{edit_url}"><img class="icon" src="{icon}"></a>',
                            html=True)

    def test_widget_formulaire(self):
        form = ProlongationAsaefForm()
        self.assertEqual(form.fields['bilan'].widget.__class__.__name__, 'TinyMCE')
        self.assertEqual(form.fields['decision'].widget.__class__.__name__, 'TinyMCE')


class FamilleTests(ASAEFTestBase):

    def test_famille_anonymiser(self):
        famille = FamilleAsaef.objects.create_famille(
            nom='Doe', rue='Rue du Lac', npa='2000', localite='Marseille', telephone='+41 79 111 22 33',
        )
        Personne.objects.create_personne(
            nom='Doe', genre='M', role=Role.objects.get(nom='Père'), famille=famille
        )
        enf_suivi = Personne.objects.create_personne(
            nom='Doe', genre='M', role=Role.objects.get(nom='Enfant suivi'), famille=famille,
            date_naissance=date(2018, 1, 1)
        )

        famille.suivi.intervenants_asaef.add(Utilisateur.objects.get(nom='Champmard'))
        famille.suivi.ope_referent = Contact.objects.get(nom='Rathfeld')
        famille.suivi.ope_referent2 = Contact.objects.get(nom='Rathfeld')
        famille.suivi.date_demande = date.today() - timedelta(days=10)
        famille.suivi.date_fin_suivi = date.today()
        famille.suivi.save()
        PrestationAsaef.objects.create(
            famille=famille, intervenant=self.user_asaef, date_prestation=date.today(),  duree='1:10',
            lib_prestation=LibellePrestation.objects.filter(unite='asaef').first()
        )
        JournalAsaef.objects.create(
            date=timezone.now(), theme='Un thème', texte='Un texte', famille=famille, auteur=self.user_asaef,
        )
        with open(__file__, mode='rb') as fh:
            doc = Document.objects.create(famille=famille, fichier=File(fh, name='essai.txt'), titre='Essai')
        doc_path = doc.fichier.path
        BilanAsaef.objects.create(famille=famille, typ='1mois', date=date.today(), autres="Pierre Paul Jacques")
        BilanAnalyseAsaef.objects.create(famille=famille, date=date.today())
        ProlongationAsaef.objects.create(famille=famille, date_debut=date.today())

        self.assertTrue(FamilleAsaef.objects.filter(pk=famille.pk).exists())
        self.assertTrue(Formation.objects.filter(personne=enf_suivi).exists())
        self.assertEqual(famille.documents.count(), 1)
        self.assertEqual(famille.membres.count(), 2)
        self.assertEqual(famille.prestations_asaef.count(), 1)
        self.assertEqual(famille.journaux.count(), 1)
        self.assertTrue(famille.bilans_analyses.count(), 1)
        self.assertEqual(famille.bilans.count(), 1)

        famille.anonymiser()

        famille.refresh_from_db()

        self.assertNotIn('Doe', famille.nom)
        self.assertEqual('', famille.rue)
        self.assertEqual('', famille.telephone)
        self.assertIn('2000', famille.npa)
        self.assertIn('Marseille', famille.localite)
        self.assertEqual(famille.suivi.intervenants_asaef.count(), 1)
        self.assertIsNone(famille.suivi.ope_referent)
        self.assertIsNone(famille.suivi.ope_referent2)
        self.assertIsNotNone(famille.suivi.date_demande)
        self.assertIsNotNone(famille.suivi.date_demande)
        self.assertEqual(famille.membres.count(), 1)
        self.assertEqual(famille.documents.count(), 0)
        self.assertFalse(os.path.exists(doc_path))
        self.assertEqual(famille.journaux.count(), 0)

        self.assertTrue(Formation.objects.filter(personne=enf_suivi).exists())
        self.assertEqual(famille.prestations_asaef.count(), 1)

    def test_famille_updateview(self):
        famille_data = {'nom': 'Doe', 'rue': "Route de la gare", 'npa': '2000', 'localite': 'Marseille',
                        'telephone': '+41 79 111 22 33'}
        famille = FamilleAsaef.objects.create_famille(**famille_data)
        update_url = reverse('asaef-famille-edit', args=[famille.pk])
        new_data = {**famille_data, 'localite': 'Paris'}
        self.client.force_login(self.user_asaef)
        response = self.client.post(update_url, data=new_data, follow=True)
        self.assertContains(
            response,
            '<li class="alert alert-info">Les modifications ont été enregistrées avec succès.</li>',
            html=False
        )
        famille.refresh_from_db()
        self.assertEqual(famille.localite, 'Paris')


class InterventionTests(ASAEFTestBase):

    def test_create(self):
        famille = FamilleAsaef.objects.get(nom='Tournesol')
        group_asaef = Group.objects.create(name='asaef')
        self.user_asaef.groups.add(group_asaef)
        self.client.force_login(self.user_asaef)
        response = self.client.get(reverse('asaef-intervention-edit', args=[famille.suivi.pk]))
        self.assertContains(
            response,
            '<input type="hidden" name="inter-TOTAL_FORMS" value="5" id="id_inter-TOTAL_FORMS">',
            html=True
        )

        self.client.post(
            reverse('asaef-intervention-edit', args=[famille.suivi.pk]),
            instance=famille.suivi,
            data={
                "inter-TOTAL_FORMS": "5", "inter-INITIAL_FORMS": "0",
                "inter-0-educ": self.user_asaef.pk, 'inter-0-role': 'psy', 'inter-0-date_debut': '2024-01-01'
            }
        )
        interv = Intervention.objects.get(date_debut='2024-01-01')
        self.assertEqual(interv.suivi, famille.suivi)
        self.assertEqual(interv.educ, self.user_asaef)
        self.assertEqual(interv.role, 'psy')
        self.assertEqual(famille.suivi.interventions_asaef.first().educ, self.user_asaef)
