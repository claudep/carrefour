from django.urls import path

from asaef import views

urlpatterns = [
    # Famille
    path('famille/list/', views.FamilleListView.as_view(), name='asaef-famille-list'),
    path('famille/add/', views.FamilleCreateView.as_view(), name='asaef-famille-add'),
    path('famille/<int:pk>/edit/', views.FamilleUpdateView.as_view(), name='asaef-famille-edit'),

    # Fonctions
    path('famille/<int:pk>/demande/', views.DemandeView.as_view(), name='asaef-demande'),
    path('famille/<int:pk>/suivi/', views.AsaefSuiviView.as_view(), name='asaef-famille-suivi'),
    path('famille/<int:pk>/projet/', views.ProjetUpdateView.as_view(), name='asaef-projet-edit'),
    path('famille/<int:pk>/agenda/', views.AgendaUpdateView.as_view(), name='asaef-famille-agenda'),
    path('suivis_termines/', views.SuivisTerminesListView.as_view(), name='asaef-suivis-termines'),
    path('asaef/dal/', views.AsaefAutocompleteView.as_view(), name='asaef-autocomplete'),
    path('famille/archivable/list/', views.FamilleArchivableListe.as_view(), name='asaef-famille-archivable'),
    path('famille/<int:pk>/intervenants/', views.InterventionUpdateView.as_view(), name='asaef-intervention-edit'),

    # Prestations
    path('prestation/menu/', views.PrestationMenu.as_view(), name='asaef-prestation-menu'),
    path('famille/<int:pk>/prestation/list/', views.PrestationListView.as_view(),
         name='asaef-prestation-famille-list'),
    path('famille/<int:pk>/prestation/add/', views.PrestationCreateView.as_view(),
         name='asaef-prestation-famille-add'),
    path('famille/<int:pk>/prestation/<int:obj_pk>/edit/', views.PrestationUpdateView.as_view(),
         name='asaef-prestation-edit'),
    path('famille/<int:pk>/prestation/<int:obj_pk>/delete/', views.PrestationDeleteView.as_view(),
         name='asaef-prestation-delete'),

    path('prestation/recap_perso/', views.PrestationRecapPersoView.as_view(), name='asaef-prestation-recap-perso'),
    path('prestation_gen/list/', views.PrestationListView.as_view(), name='asaef-prestation-gen-list'),
    path('prestation_gen/add/', views.PrestationCreateView.as_view(), name='asaef-prestation-gen-add'),
    path('prestations/list/', views.AdminPrestationListView.as_view(),
         name='asaef-prestation-adminlist'),

    # Journal
    path('famille/<int:pk>/journal/list/', views.JournalListView.as_view(), name='asaef-journal-list'),

    # Impression
    path('famille/<int:pk>/print-analyse_demande/', views.AnalyseDemandePDFView.as_view(),
         name='asaef-print-analyse-demande'),
    path('famille/<int:pk>/print-contrat-collaboration/', views.ContratCollaborationPDFView.as_view(),
         name='asaef-print-contrat-collaboration'),
    path('famille/<int:pk>/print-coordonnees/', views.CoordonneesFamillePDFView.as_view(),
         name='asaef-print-coord-famille'),
    path('famille/<int:pk>/print-journal', views.JournalPDFView.as_view(),
         name='asaef-print-journal'),
    path('famille/<int:pk>/bilan_analyse/', views.print_docx, {'docname': 'bilan_analyse'},
         name='asaef-print-bilan-analyse'),
    path('famille/<int:pk>/projet_commun/', views.print_docx, {'docname': 'projet_commun'},
         name='asaef-print-projet-commun'),

    # Nouveaux bilans
    path('famille/<int:pk>/bilan/<str:typ>/add/', views.BilanAsaefCreateView.as_view(),
         name='asaef-bilan-add'),
    path('famille/<int:pk>/bilan/<int:obj_pk>/edit/', views.BilanAsaefUpdateView.as_view(),
         name='asaef-bilan-edit'),
    path('famille/<int:pk>/bilan/<int:obj_pk>/print/', views.BilanAsaefPdfView.as_view(),
         name='asaef-bilan-print'),

    path('famille/<int:pk>/bilananalyse/add/', views.BilanAnalyseCreateView.as_view(),
         name="asaef-bilananalyse-add"),
    path('famille/<int:pk>/bilananalyse/<obj_pk>/edit/', views.BilanAnalyseUpdateView.as_view(),
         name="asaef-bilananalyse-edit"),
    path('famille/<int:pk>/bilananalyse/<int:obj_pk>/print/', views.BilanAnalysePrintView.as_view(),
         name="asaef-bilananalyse-print"),

    # Prolongation
    path('famille/<int:pk>/prolongation/add/', views.ProlongationAsaefCreateView.as_view(),
         name='asaef-prolongation-add'),
    path('famille/<int:pk>/prolongation/<int:obj_pk>/edit/', views.ProlongationAsaefUpdateView.as_view(),
         name='asaef-prolongation-edit'),
    path('famille/<int:pk>/prolongation/<int:obj_pk>/print/', views.ProlongationAsaefPrintView.as_view(),
         name='asaef-prolongation-print'),


]
