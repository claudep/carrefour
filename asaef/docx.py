from docx.enum.text import WD_PARAGRAPH_ALIGNMENT


from carrefour.docx_base import DocxBase


class DocxBilanAsaef(DocxBase):

    def produce(self, famille, title, doc_date):
        suivi = famille.suiviasaef
        p = self.document.add_paragraph('ASAEF - Bilan {}'.format(title), self.style_titre1)
        p.alignment = WD_PARAGRAPH_ALIGNMENT.RIGHT
        self.document.add_paragraph('Famille: {} - {}'.format(famille.nom, famille.adresse), self.style_soustitre)

        table = self.document.add_table(rows=7, cols=2)
        self.set_column_width(table.columns[0], 6)
        self.set_column_width(table.columns[1], 11)
        table.cell(0, 0).text = 'Date du bilan:'
        table.cell(0, 1).text = self.date_formated(getattr(suivi, doc_date))
        table.cell(1, 0).text = 'Intervenants ASAEF: '
        table.cell(1, 1).text = ' - '.join([ref.nom_initiale_prenom for ref in suivi.suivi_referents])
        table.cell(2, 0).text = 'Membres de la famille: '
        table.cell(3, 0).text = "AS de l'OPE:"
        table.cell(3, 1).text = suivi.get_ope_referents()
        table.cell(4, 0).text = "Autres personnes présentes: "
        if title == "de l'analyse":
            table.cell(5, 0).text = "Date de début de l'analyse:"
            table.cell(5, 1).text = self.date_formated(suivi.date_analyse)
            table.cell(6, 0).text = "Nombre de rencontres:"
        else:
            table.cell(5, 0).text = "Date de début du soutien:"
            table.cell(5, 1).text = self.date_formated(suivi.date_debut_suivi)
            table.cell(6, 0).text = "Fréquence des interventions:"

        self.document.add_paragraph('')
        self.document.add_paragraph('')

        table = self.document.add_table(rows=0, cols=2)
        self.set_column_width(table.columns[0], 5)
        self.set_column_width(table.columns[1], 12)

        table.style = 'Table Grid'
        if title == "1er mois d'action et de soutien":
            titres = [
                ["Rappel de la demande:\n\n",
                 "Rappel du projet commun (objectifs):\n",
                 "Moyens/outils mis en place:\n",
                 "Critères d'évaluation:\n\n",
                 "Evolution:\n\n",
                 "Réajustements nécessaires:\n",
                 "Accompagnement de l'AS ASAEF:\n",
                 "Contexte de diminution de l'intensité:\n",
                 ]
            ]

        elif title == "3 mois":
            titres = [
                ["Rappel de la situation et des décisions prises au bilan du premier mois:",
                 "Rappel du projet commun (objectifs):\n",
                 "Moyens/outils mis en place:\n",
                 "Critères d'évaluation:\n\n",
                 "Evolution:\n\n",
                 "Réajustements nécessaires:\n",
                 "Accompagnement de l'AS ASAEF:\n",
                 "Contexte de diminution de l'intensité:\n"
                 ],
                "Projection de la fin de l'ASAEF dans 3 mois\n",
                ["Perspectives d'avenir:\n",
                 "Relais envisageables, pour quelle raison? Selon qui?\n",
                 "Améliorations possibles à entreprendre:\n",
                 "Décisions prises:\n\n"
                 ],
                "Evolution du projet commun\n",
                ["Selon la famille:\n\n",
                 "Selon l'enfant:\n\n",
                 "Selon l'OPE:\n\n"
                 ]
            ]

        elif title == "Final":
            titres = [
                ["Rappel de la situation et des décisions prises au bilan des 3 mois:",
                 "Rappel du projet commun (objectifs):\n",
                 "Moyens/outils mis en place:\n",
                 "Critères d'évaluation:\n\n",
                 "Evolution de la problématique:\n",
                 "Ressources mobilisées:\n\n",
                 "Accompagnement de l'AS ASAEF:\n",
                 "Contexte de diminution de l'intensité:\n",
                 ],
                "Fin de l'ASAEF et perspectives d'avenir\n",
                ["Perspectives d'avenir:\n\n",
                 "Mobilisation des ressources/compétences:\n",
                 "Ressources/compétences développées:\n",
                 "Ressources/compétences à développer:\n",
                 "Vécu de la famille:\n\n",
                 "Prolongation:\n\n",
                 "Relai vers d'autres services:\n",
                 ],
            ]
        elif title == "de l'analyse":
            titres = [
                ["Nbre de RDV annulés et pourquoi:\n",
                 "Niveau d'acceptation de la démarche et vécu de la famille:",
                 ],
                "Rappel de la demande:",
                "#",
                "Résumé des observations:",
                ["Contexte d'observation:\n",
                 "Observations:\n",
                 "Outils d'analyse:\n"
                 ],
                "Objectifs à travailler durant la période d'action et de soutien",
                ["1. Identification de la problématique\n",
                 "2. Hypothèse des causes de la problématique\n",
                 "3. Objectif général\n",
                 "4. Objectif spécifique\n",
                 "5. Moyens\n",
                 "6. Planificiation de l'intervention (sur les causes de la problématique)"],
                "Evaluation",
                "#Une évaluation de l'intervention et de l'évolution de la problématique "
                "est planifiée après 1 mois d'action et de soutien intensif."
            ]
        else:
            titres = []

        row_count = 0
        for titre in titres:
            if isinstance(titre, str):
                table.add_row()
                table.cell(row_count, 0).merge(table.cell(row_count, 1))
                if titre[0] == '#':
                    table.cell(row_count, 0).add_paragraph(titre[1:], self.style_normal)
                else:
                    table.cell(row_count, 0).add_paragraph(titre, self.style_soustitre)
                row_count += 1
            else:
                for item in titre:
                    table.add_row()
                    table.cell(row_count, 0).text = item
                    row_count += 1
        self.document.save(self.path)
