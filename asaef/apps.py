from django.apps import AppConfig


class AsaefConfig(AppConfig):
    name = 'asaef'

    @property
    def benef_model(self):
        from .models import FamilleAsaef
        return FamilleAsaef
