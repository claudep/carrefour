from collections import OrderedDict
from datetime import date, timedelta

from django.db import models
from django.urls import reverse

from carrefour.models import (
    Contact, Famille, FamilleManager, JournalBase, Personne, PrestationBase,
    ProlongationBase, Utilisateur
)
from carrefour.utils import delay_for_destruction, format_d_m_Y

from common import choices
from common.fields import ChoiceArrayField


class Etape:
    """
        1. Numéro d'ordre pour tri dans la vue
        2. Code
        3. Affichage dans tableau
        4. Nom visible de l'étape
        5. Code de l'étape suivante
        6. Délai jusqu'à l'échéance (en jours)
        7. Code de l'étape précédente
        8. Date obligatoire précédente
        9. Saisie obligatoire ou non (True / False)
    """
    def __init__(self, num, code, abrev, nom, suivante, delai, precedente, preced_oblig, oblig):
        self.num = num
        self.code = code
        self.abrev = abrev
        self.nom = nom
        self.suivante = suivante
        self.delai = delai
        self.precedente = precedente
        self.preced_oblig = preced_oblig
        self.oblig = oblig

    def __str__(self):
        return self.nom

    def __repr__(self):
        return '<Etape - {}/{}>'.format(self.num, self.code)

    def date(self, suivi):
        return getattr(suivi, 'date_{}'.format(self.code))

    def date_suivante(self, suivi):
        """Date d'échéance de cette étape selon délai."""
        date_etape = self.date(suivi)
        return (date_etape + timedelta(self.delai)) if date_etape else None

    def date_nom(self):
        return 'date_{}'.format(self.code)

    def etape_precedente(self):
        return SuiviAsaef.WORKFLOW[self.precedente]


class FamilleAsaefManager(FamilleManager):
    def get_queryset(self):
        return super().get_queryset().filter(suiviasaef__isnull=False)

    def create_famille(self, **kwargs):
        kwargs.pop('_state', None)
        famille = self.create(**kwargs)
        SuiviAsaef.objects.create(
            famille=famille,
        )
        famille.suiviasaef.date_demande = date.today()
        famille.suiviasaef.save()
        return famille


class FamilleAsaef(Famille):
    class Meta:
        proxy = True
        verbose_name = "Famille ASAEF"
        verbose_name_plural = "Familles ASAEF"

    typ = 'asaef'
    objects = FamilleAsaefManager()

    @classmethod
    def actives(cls, date=None):
        """
        Les critères d'activité sont: la date d'analyse et la date de fin de suivi
        (cf ListeFamilleAsaefTests.test_critere_date_analyse_pour_sortir_liste_attente)

        """
        qs = FamilleAsaef.objects.filter(
            suiviasaef__isnull=False,
            suiviasaef__date_analyse__isnull=False,
        )
        if date is not None:
            return qs.filter(
                suiviasaef__date_analyse__lte=date,
            ).filter(
                models.Q(suiviasaef__date_fin_suivi__isnull=True) |
                models.Q(suiviasaef__date_fin_suivi__gte=date)
            )
        else:
            return qs.filter(suiviasaef__date_fin_suivi__isnull=True)

    @property
    def suivi(self):
        return self.suiviasaef

    def can_edit(self, user):
        return user.has_perm('asaef.change_asaef')

    @classmethod
    def check_perm_view(cls, user):
        return user.has_perm('asaef.view_asaef')

    @property
    def prestations(self):
        return self.prestations_asaef

    @property
    def prolongation(self):
        if self.prolongations_asaef.count() > 0:
            return self.prolongations_asaef.first()
        else:
            return None

    def can_be_deleted(self, user):
        if user.has_perm('carrefour.export_aemo') or user.has_perm('asaef.manage_asaef'):
            if self.suiviasaef.motif_fin_suivi == choices.MotifsFinSuivi.ERREUR:
                return True
            if user.has_perm('carrefour.export_aemo') and delay_for_destruction(self.suiviasaef.date_fin_suivi):
                return True
        return False

    def anonymiser(self):
        super().anonymiser()
        self.suivi.anonymiser()
        self.prestations_asaef.update(theme='', texte='')
        for field in ['bilans', 'bilans_analyses', 'prolongations_asaef', 'journaux']:
            getattr(self, field).all().delete()


class Intervention(models.Model):

    suivi = models.ForeignKey(to='SuiviAsaef', related_name='interventions_asaef', on_delete=models.CASCADE)
    educ = models.ForeignKey(to=Utilisateur, related_name='interventions_asaef', on_delete=models.CASCADE)
    date_debut = models.DateField('Date début', blank=True, null=True)
    date_fin = models.DateField('Date fin', blank=True, null=True)
    role = models.CharField(
        'Rôle',
        max_length=15,
        choices=(('re', 'Resp. Equipe'), ('coord', 'Educ. de coord.'), ('educ', 'Educ.'), ('as', 'Assist. soc.'),
                 ('psy', 'Psychologue')),
    )


class SuiviAsaef(models.Model):
    """
    Dossier de suivi
    """

    WORKFLOW = OrderedDict([
        ('demande',
         Etape(1, 'demande', 'dem', 'Demande OPE', 'analyse', 180, 'demande', 'demande', True)),
        ('analyse',
         Etape(2, 'analyse', 'ana', 'Analyse de la demande', 'bilan_analyse', 40, 'demande', 'demande', True)),
        ('bilan_analyse',
         Etape(3, 'bilan_analyse', "bilan", "Bilan de l'analyse", 'debut_suivi', 20, 'analyse', 'analyse', True)),
        ('debut_suivi',
         Etape(4, 'debut_suivi', "deb", 'Soutien intens.', 'bilan_1mois', 60, 'bilan_analyse', 'bilan_analyse', True)),
        ('bilan_1mois',
         Etape(5, 'bilan_1mois', "b1m", 'Bilan 1 mois', 'bilan_3mois', 60, 'debut_suivi', 'debut_suivi', False)),
        ('bilan_3mois',
         Etape(6, 'bilan_3mois', 'b3m', 'Bilan 3 mois', 'bilan_final', 60, 'bilan_1mois', 'debut_suivi', False)),
        ('bilan_final',
         Etape(7, 'bilan_final', "bfin", 'Bilan final', 'fin_suivi', 30, 'bilan_3mois', 'debut_suivi', False)),
        ('prolongation',
         Etape(8, 'prolongation', "bprol", 'Bilan prolongation', 'fin_suivi', 30,
               'bilan_final', 'debut_suivi', False)),
        ('fin_suivi',
         Etape(9, 'fin_suivi', 'fin', 'Fin du suivi', None, 0, 'prolongation', 'debut_suivi', True)),
        ('archivage',
         Etape(10, 'arch_dossier', 'arch', 'Archivage du dossier', None, 0, 'fin_suivi', 'fin_suivi', False)),
    ])

    famille = models.OneToOneField(FamilleAsaef, on_delete=models.CASCADE)
    difficultes = models.TextField("Difficultés", blank=True)
    aides = models.TextField("Aides souhaitées", blank=True)
    autres_contacts = models.TextField("Autres services contactés", blank=True)
    disponibilites = models.TextField('Disponibilités', blank=True)
    remarque = models.TextField(blank=True)
    service_orienteur = models.CharField(
        "Orienté vers l'ASAEF par", max_length=15, choices=choices.SERVICE_ORIENTEUR_ASAEF_CHOICES, blank=True
    )
    motif_demande = ChoiceArrayField(
        models.CharField(max_length=60, choices=choices.MOTIF_DEMANDE_CHOICES),
        verbose_name="Motif de la demande", blank=True, null=True)
    ope_referent = models.ForeignKey(
        Contact, blank=True, null=True, related_name='suivisasaef', on_delete=models.PROTECT, verbose_name='as. OPE',
    )
    ope_referent2 = models.ForeignKey(
        Contact, blank=True, null=True, related_name='suivisasaef2', on_delete=models.PROTECT,
        verbose_name='as. OPE 2',
    )
    mandat_ope = ChoiceArrayField(
        models.CharField(max_length=65, choices=choices.MANDATS_OPE_CHOICES, blank=True),
        verbose_name="Mandat OPE", blank=True, null=True,
    )

    date_demande = models.DateField("Dépôt demande MAS", blank=True, null=True, default=None)
    date_analyse = models.DateField("Date début analyse", blank=True, null=True, default=None)
    date_bilan_analyse = models.DateField("Date bilan analyse", blank=True, null=True, default=None)
    date_debut_suivi = models.DateField("Début accomp.", blank=True, null=True, default=None)
    date_bilan_1mois = models.DateField("Bilan du soutien intensif", blank=True, null=True, default=None)
    date_bilan_3mois = models.DateField("Bilan d’autonomisation", blank=True, null=True, default=None)
    date_bilan_final = models.DateField("Bilan final", blank=True, null=True, default=None)
    date_prolongation = models.DateField("Prolongation", blank=True, null=True, default=None)
    date_fin_suivi = models.DateField("Fin accomp.", blank=True, null=True, default=None)

    demarche = ChoiceArrayField(
        models.CharField(max_length=60, choices=choices.DEMARCHE_CHOICES, blank=True),
        verbose_name="Démarche", blank=True, null=True,
    )

    pers_famille_presentes = models.CharField('Membres famille présents', max_length=200, blank=True)
    intervenants_presents = models.CharField('Intervenants Asaef présents', max_length=250, blank=True)
    autres_pers_presentes = models.CharField('Autres pers. présentes', max_length=100, blank=True)
    motif_fin_suivi = models.CharField(
        'Motif de fin de suivi', max_length=20, choices=choices.MotifsFinSuivi.choices, blank=True
    )
    projet = models.TextField('Projet commun', blank=True)
    autoevaluation = models.SmallIntegerField("Auto-évaluation", blank=True, null=True, choices=choices.SCORE_CHOICES)
    intervenants_asaef = models.ManyToManyField(
        to=Utilisateur, blank=True, through=Intervention, related_name='suivis'
    )

    class Meta:
        verbose_name = "Suivi ASAEF"
        verbose_name_plural = "Suivis ASAEF"
        permissions = (
            ('view_asaef', 'Consulter les dossiers ASAEF'),
            ('manage_asaef', 'RE ASAEF'),
            ('change_asaef', 'Gérer les dossiers ASAEF')
        )

    def __str__(self):
        return 'Suivi ASAEF pour la famille {} '.format(self.famille)

    @property
    def date_fin_theorique(self):
        if self.date_fin_suivi:
            return self.date_fin_suivi
        if self.date_debut_suivi is None:
            return None
        return self.date_debut_suivi + timedelta(days=180)  # env. 6 mois

    def get_ope_referents(self, tel=False):
        if not tel:
            return ' / '.join([str(ope) for ope in [self.ope_referent, self.ope_referent2] if ope])
        return ' / '.join(
            [f"{str(ope)} ({ope.telephone})" for ope in [self.ope_referent, self.ope_referent2] if ope])

    @property
    def suivi_referents(self):
        return self.intervenants_asaef.all()

    @property
    def etape(self):
        for key, etape in reversed(self.WORKFLOW.items()):
            if key != 'archivage':
                if etape.date(self):
                    return etape

    def etape_suivante(self):
        etape = self.etape
        return self.WORKFLOW[etape.suivante] if etape else None

    def date_suivante(self):
        return self.etape.date_suivante(self)

    def get_mandat_ope_display(self):
        dic = dict(choices.MANDATS_OPE_CHOICES)
        return '; '.join([dic[value] for value in self.mandat_ope]) if self.mandat_ope else '-'

    def get_motif_demande_display(self):
        dic = dict(choices.MOTIF_DEMANDE_CHOICES)
        return '; '.join([dic[value] for value in self.motif_demande]) if self.motif_demande else '-'

    def anonymiser(self):
        new_data = {
            'famille': self.famille,
            'difficultes': '',
            'aides': '',
            'autres_contacts': '',
            'disponibilites': '',
            'remarque': '',
            'service_orienteur': self.service_orienteur,
            'motif_demande': self.motif_demande,
            # 'asaef_intervenants': pass,
            # 'asaef_equipe': pass,
            'ope_referent': None,
            'ope_referent2': None,
            'mandat_ope': self.mandat_ope,
            'date_demande': self.date_demande,
            'date_analyse': self.date_analyse,
            'date_bilan_analyse': self.date_bilan_analyse,
            'date_debut_suivi': self.date_debut_suivi,
            'date_bilan_1mois': self.date_bilan_1mois,
            'date_bilan_3mois': self.date_bilan_3mois,
            'date_bilan_final': self.date_bilan_final,
            'date_prolongation': self.date_prolongation,
            'date_fin_suivi': self.date_fin_suivi,
            'demarche': self.demarche,
            'pers_famille_presentes': '',
            'intervenants_presents': '',
            'autres_pers_presentes': '',
            'motif_fin_suivi': self.motif_fin_suivi,
            'projet': ''
        }
        [setattr(self, field, value) for field, value in new_data.items()]
        self.save()


class PrestationAsaef(PrestationBase):
    famille = models.ForeignKey(FamilleAsaef, related_name='prestations_asaef', null=True, blank=True,
                                on_delete=models.SET_NULL, verbose_name="Famille")
    intervenant = models.ForeignKey(Utilisateur, on_delete=models.PROTECT, related_name='prestations_asaef',
                                    null=True, default=None)
    theme = models.CharField('thème', max_length=100, blank=True)
    texte = models.TextField('contenu', blank=True)
    # Nombre de familles actives au moment de la prestation, utilisé pour ventiler les heures
    # dans les statistiques.
    familles_actives = models.PositiveSmallIntegerField(blank=True, default=0)

    class Meta(PrestationBase.Meta):
        verbose_name = "Prestation ASAEF"
        verbose_name_plural = "Prestations ASAEF"

    def save(self, *args, **kwargs):
        if self.famille is None:
            self.familles_actives = FamilleAsaef.actives(self.date_prestation).count()
        super().save(*args, **kwargs)

    def can_edit(self, user):
        return user == self.intervenant and self.check_date_editable(self.date_prestation, user) or user.is_superuser

    def get_edit_url(self):
        famille_id = 0 if self.famille is None else self.famille.pk
        return reverse('asaef-prestation-edit', args=[famille_id, self.pk])

    def get_delete_url(self):
        famille_id = 0 if self.famille is None else self.famille.pk
        return reverse('asaef-prestation-delete', args=[famille_id, self.pk])


class JournalAsaef(JournalBase):
    famille = models.ForeignKey(
        FamilleAsaef, related_name='journaux', on_delete=models.CASCADE, verbose_name="Famille"
    )

    class Meta(JournalBase.Meta):
        verbose_name = "Journal ASAEF"
        verbose_name_plural = "Journaux ASAEF"

    def __str__(self):
        return f"Journal de la famille {self.famille} - {self.date}"


class BilanAsaef(models.Model):
    BILAN_CHOICES = (
        ('1mois', "Bilan du soutien intensif"),
        ('3mois', "Bilan d’autonomisation"),
        ('final', "Bilan final"),
        ('suppl', "Bilan suppl.")
    )
    typ = models.CharField("Type de bilan", max_length=10, choices=BILAN_CHOICES)
    famille = models.ForeignKey(to=FamilleAsaef, related_name='bilans', on_delete=models.CASCADE)
    date = models.DateField("Date du bilan")
    interv_asaef = models.CharField("Intervenant-e-s ASAEF", max_length=250, blank=True)
    membres_famille = models.CharField("Membres de la famille", max_length=250, blank=True)
    autres = models.CharField("Autres pers. présentes", max_length=250, blank=True)
    freq_interv = models.CharField("Fréquence des interventions", max_length=250, blank=True)
    #
    objectif = models.TextField("Rappel des objectifs", blank=True)
    moyens = models.TextField("Moyens de réalisation", blank=True)
    evol_obj = models.TextField("Évolution des objectifs", blank=True)
    situation_fam = models.TextField("État actuel de la situation familiale", blank=True)
    evolution_fam = models.TextField("Évolution de la situation familiale", blank=True)
    ajustement = models.TextField("Réajustements nécessaires", blank=True)
    acc_as = models.TextField("Accomp. de l'AS ASAEF", blank=True)
    acc_psy = models.TextField("Accomp. du ou de la psy. ASAEF", blank=True)
    evol_famille = models.TextField("Évolution selon la famille", blank=True)
    evol_asaef = models.TextField("Évolution selon l'ASAEF", blank=True)
    ressources = models.TextField("Ressources mobilisées et compétences développées", blank=True)
    evol_intensite = models.TextField("Contexte d'évolution de l'intensité", blank=True)

    avenir = models.TextField("Perspectives d'avenir", blank=True)
    relais = models.TextField("Relais envisageables", blank=True)
    amelioration = models.TextField("Améliorations possibles", blank=True)
    prolongation = models.TextField("Prolongation", blank=True)
    destinataires = models.ManyToManyField(to=Personne, related_name='bilans')

    def __str__(self):
        return f"Famille {self.famille.nom}: {self.typ} du {self.date}"


class BilanAnalyseAsaef(models.Model):
    famille = models.ForeignKey(to=FamilleAsaef, related_name='bilans_analyses', on_delete=models.CASCADE)
    date = models.DateField("Date du bilan", blank=True, null=True)
    interv_asaef = models.CharField("Intervenant-e-s ASAEF", max_length=250, blank=True)
    membres_famille = models.CharField("Membres de la famille", max_length=250, blank=True)
    autres = models.CharField("Autres pers. présentes", max_length=250, blank=True)
    rencontre = models.CharField("Nbre de rencontres", max_length=50, blank=True)

    rdv_manque = models.TextField("RDV annulés (nbre, motifs)", blank=True)
    acceptation = models.TextField("Degré d'acceptation de la démarche et vécu de la famille",
                                   blank=True)
    rappel_demande = models.TextField("Rappel de la demande", blank=True)
    contexte = models.TextField("Contexte d'observation", blank=True)
    observation = models.TextField("Observations", blank=True)
    outil = models.TextField("Outils d'analyse", blank=True)
    problematique = models.TextField("Identification de la problématique", blank=True)
    hypothese = models.TextField("Hypothèses sur les causes de la problématique", blank=True)
    obj_general = models.TextField("Objectif général", blank=True)
    obj_specifique = models.TextField("Objectifs spécifiques", blank=True)
    moyen = models.TextField("Moyens", blank=True)
    planification = models.TextField("Planification de l'intervention", blank=True)
    destinataires = models.ManyToManyField(to=Personne, related_name='bilans_analyses')

    titre = "Bilan de l'analyse ASAEF"

    class Meta:
        verbose_name = "Bilan de l'analyse ASAEF"

    def __str__(self):
        return f"Bilan de l'analyse ASAEF pour la famille {self.famille}"


class ProlongationAsaef(ProlongationBase):
    famille = models.ForeignKey(to=FamilleAsaef, related_name='prolongations_asaef',
                                on_delete=models.CASCADE)

    class Meta(PrestationBase.Meta):
        verbose_name = "Prolongation ASAEF"
        verbose_name_plural = "Prolongations ASAEF"
        ordering = ('date_debut',)

    def __str__(self):
        return f"Famille {self.famille.nom}: prolongation du {format_d_m_Y(self.date_debut)}"
