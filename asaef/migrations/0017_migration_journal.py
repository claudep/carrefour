from datetime import timedelta

from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.db import migrations


def apply_migration(apps, schema_editor):
    """ Transfert des journaux ASAEF vers Prestations ASAEF  """

    JournalAsaef = apps.get_model('asaef', 'JournalAsaef')
    PrestationAsaef = apps.get_model('asaef', 'PrestationAsaef')
    for journal in JournalAsaef.objects.all():
        try:
            obj = PrestationAsaef.objects.get(
                date_prestation=journal.date.date(),
                famille=journal.famille,
                intervenant=journal.auteur,
                theme='',
            )
            obj.theme = journal.theme
            obj.texte = journal.texte
            obj.save()
        except MultipleObjectsReturned:
            obj = PrestationAsaef.objects.filter(
                date_prestation=journal.date.date(),
                famille=journal.famille,
                intervenant=journal.auteur
            ).first()
            obj.theme = journal.theme
            obj.texte = journal.texte
            obj.save()
        except ObjectDoesNotExist:
            obj = PrestationAsaef.objects.create(
                date_prestation=journal.date.date(),
                famille=journal.famille,
                intervenant=journal.auteur,
                lib_prestation=None,
                duree=timedelta(0),
                theme=journal.theme,
                texte=journal.texte,
            )


class Migration(migrations.Migration):

    dependencies = [
        ('asaef', '0016_xfer_journal_prestation'),
    ]

    operations = [
        migrations.RunPython(apply_migration)
    ]
