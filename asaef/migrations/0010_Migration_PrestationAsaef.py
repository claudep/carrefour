from django.db import migrations


def apply_migration(apps, schema_editor):
    """
    Complète le champ PrestationAsaef.intervenant avec la première
    valeur corresppondant de la table PrestationAsaef_IntervenantsAsaef
    """

    PrestationAsaef = apps.get_model('asaef', 'PrestationAsaef')

    for prest in PrestationAsaef.objects.all():
        prest.intervenant = prest.intervenants.first()
        prest.save()


class Migration(migrations.Migration):

    dependencies = [
        ('asaef', '0009_PrestationAsaef_famille_a_null'),
    ]

    operations = [

        migrations.RunPython(apply_migration)
    ]
