# Generated by Django 2.2.1 on 2019-06-19 14:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('asaef', '0011_Suppression_PrestationAsaef_M2M'),
    ]

    operations = [
        migrations.AddField(
            model_name='prestationasaef',
            name='familles_actives',
            field=models.PositiveSmallIntegerField(blank=True, default=0),
        ),
    ]
