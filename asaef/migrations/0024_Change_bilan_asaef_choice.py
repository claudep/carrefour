# Generated by Django 3.1 on 2021-03-28 06:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('asaef', '0023_Modif_bilan_asaef'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bilanasaef',
            name='typ',
            field=models.CharField(choices=[('1mois', 'Bilan 1er mois'), ('3mois', 'Bilan 3 mois'), ('final', 'Bilan final'), ('suppl', 'Bilan suppl.')], max_length=10, verbose_name='Type de bilan'),
        ),
    ]
