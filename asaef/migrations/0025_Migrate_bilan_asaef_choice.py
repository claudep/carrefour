# Generated by Django 3.1 on 2021-03-28 06:56

from django.db import migrations


def apply_migration(apps, schema_editor):
    BilanAsaef = apps.get_model('asaef', 'BilanAsaef')
    BilanAsaef.objects.filter(typ="b1m").update(typ='1mois')
    BilanAsaef.objects.filter(typ="b3m").update(typ='3mois')


class Migration(migrations.Migration):

    dependencies = [
        ('asaef', '0024_Change_bilan_asaef_choice'),
    ]

    operations = [
        migrations.RunPython(apply_migration)
    ]
