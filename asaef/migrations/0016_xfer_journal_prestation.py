from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('asaef', '0015_Rename_date_saisie'),
    ]

    operations = [
        migrations.AddField(
            model_name='prestationasaef',
            name='texte',
            field=models.TextField(blank=True, verbose_name='contenu'),
        ),
        migrations.AddField(
            model_name='prestationasaef',
            name='theme',
            field=models.CharField(blank=True, max_length=100, verbose_name='thème'),
        ),
    ]
