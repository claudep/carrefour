from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('asaef', '0038_Add_asaef_autoevaluation'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bilanasaef',
            name='typ',
            field=models.CharField(choices=[('1mois', 'Bilan du soutien intensif'), ('3mois', "Bilan d’autonomisation"), ('final', 'Bilan final'), ('suppl', 'Bilan suppl.')], max_length=10, verbose_name='Type de bilan'),
        ),
        migrations.AlterField(
            model_name='suiviasaef',
            name='date_bilan_1mois',
            field=models.DateField(blank=True, default=None, null=True, verbose_name='Bilan du soutien intensif'),
        ),
        migrations.AlterField(
            model_name='suiviasaef',
            name='date_bilan_3mois',
            field=models.DateField(blank=True, default=None, null=True, verbose_name="Bilan d’autonomisation"),
        ),
    ]
