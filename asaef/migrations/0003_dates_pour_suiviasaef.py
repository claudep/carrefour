# Generated by Django 2.2 on 2019-04-24 15:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('asaef', '0002_M2M_asaef_intervenants'),
    ]

    operations = [
        migrations.AddField(
            model_name='suiviasaef',
            name='date_autonomie',
            field=models.DateField(blank=True, default=None, null=True, verbose_name='Date début autonomisation'),
        ),
        migrations.AddField(
            model_name='suiviasaef',
            name='date_conclusion',
            field=models.DateField(blank=True, default=None, null=True, verbose_name='Date début conclusion'),
        ),
        migrations.AddField(
            model_name='suiviasaef',
            name='date_intensif',
            field=models.DateField(blank=True, default=None, null=True, verbose_name='Date début soutien intensif'),
        ),
        migrations.RemoveField(
            model_name='suiviasaef',
            name='demande_prioritaire',
        ),
        migrations.RemoveField(
            model_name='suiviasaef',
            name='ref_aemo_presents',
        ),
        migrations.AddField(
            model_name='suiviasaef',
            name='intervenants_presents',
            field=models.CharField(blank=True, max_length=40, verbose_name='Intervenants Asaef présents'),
        ),
    ]
