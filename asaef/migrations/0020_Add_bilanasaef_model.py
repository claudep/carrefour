# Generated by Django 3.1 on 2021-02-20 07:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('carrefour', '0031_Ajout_taux_activite'),
        ('asaef', '0019_Add_change_permission'),
    ]

    operations = [
        migrations.CreateModel(
            name='BilanAsaef',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('typ', models.CharField(choices=[('b1m', 'Bilan 1er mois'), ('b3m', 'Bilan 3 mois'), ('final', 'Bilan final'), ('suppl', 'Bilan suppl.')], max_length=10, verbose_name='Type de bilan')),
                ('date', models.DateField(verbose_name='Date du bilan')),
                ('interv_asaef', models.CharField(blank=True, max_length=250, verbose_name='Intervenant-e-s ASAEF')),
                ('membres_famille', models.CharField(blank=True, max_length=250, verbose_name='Membres de la famille')),
                ('autres', models.CharField(blank=True, max_length=250, verbose_name='Autres pers. présentes')),
                ('debut_soutien', models.DateField(blank=True, null=True, verbose_name='Date du début du soutien')),
                ('freq_interv', models.CharField(blank=True, max_length=250, verbose_name='Fréquence des interventions')),
                ('objectif', models.TextField(blank=True, verbose_name='Rappel des objectifs')),
                ('moyens', models.TextField(blank=True, verbose_name='Moyens / outils mis en place')),
                ('evol_obj', models.TextField(blank=True, verbose_name='Évolution des objectifs')),
                ('situation_fam', models.TextField(blank=True, verbose_name='Situation familiale')),
                ('ajustement', models.TextField(blank=True, verbose_name='Réajustements nécessaires')),
                ('acc_as', models.TextField(blank=True, verbose_name="Accomp. de l'AS ASAEF")),
                ('acc_psy', models.TextField(blank=True, verbose_name='Accomp. du ou de la psy. ASAEF')),
                ('evol_intensite', models.TextField(blank=True, verbose_name="Contexte d'évolution de l'intensité")),
                ('avenir', models.TextField(blank=True, verbose_name="Perspectives d'avenir")),
                ('relais', models.TextField(blank=True, verbose_name='Relais envisagés')),
                ('amelioration', models.TextField(blank=True, verbose_name='Améliorations possibles')),
                ('decision', models.TextField(blank=True, verbose_name='Décisions prises')),
                ('evol_famille', models.TextField(blank=True, verbose_name='Selon la famille')),
                ('evol_enfant', models.TextField(blank=True, verbose_name='Selon le ou les enfants')),
                ('evol_ipe', models.TextField(blank=True, verbose_name="Selon l'IPE")),
                ('divers', models.TextField(blank=True, verbose_name='Autres remarques')),
                ('famille', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='bilans', to='asaef.familleasaef')),
                ('ipe', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='carrefour.contact', verbose_name='IPE')),
            ],
            options={
                'unique_together': {('famille', 'typ')},
            },
        ),
    ]
