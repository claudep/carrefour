import common.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    replaces = [('asaef', '0001_initial'), ('asaef', '0002_M2M_asaef_intervenants'), ('asaef', '0003_dates_pour_suiviasaef'), ('asaef', '0004_ajouter_journal'), ('asaef', '0005_remove_journal_date_auto'), ('asaef', '0006_modif_suivi_asaef'), ('asaef', '0007_FK_PrestationAsaef'), ('asaef', '0008_Ajouter_SuiviAsaef_projet'), ('asaef', '0009_PrestationAsaef_famille_a_null'), ('asaef', '0010_Migration_PrestationAsaef'), ('asaef', '0011_Suppression_PrestationAsaef_M2M'), ('asaef', '0012_Ajout_PrestationAsaef_familles_actives'), ('asaef', '0013_famille_asaef_proxy'), ('asaef', '0014_Add_asaef_equipe'), ('asaef', '0015_Rename_date_saisie'), ('asaef', '0016_xfer_journal_prestation'), ('asaef', '0017_migration_journal'), ('asaef', '0018_Modif_long_Interv_present'), ('asaef', '0019_Add_change_permission'), ('asaef', '0020_Add_bilanasaef_model'), ('asaef', '0021_Add_BilanAnalyseAsaef'), ('asaef', '0022_Migration_BilanAnalyseAsaef'), ('asaef', '0023_Modif_bilan_asaef'), ('asaef', '0024_Change_bilan_asaef_choice'), ('asaef', '0025_Migrate_bilan_asaef_choice'), ('asaef', '0026_Suppr_doublons_bilan_analyse'), ('asaef', '0027_Add_prolongation_asaef'), ('asaef', '0028_Rpl_fk_contact_protect'), ('asaef', '0029_alter_suiviasaef_ope_referent'), ('asaef', '0030_Add_asaef_ope_referent2'), ('asaef', '0031_Add_related_set_for_asaef_equipe'), ('asaef', '0032_alter_prestationasaef_lib_prestation'), ('asaef', '0033_add_destinataire_fk_to_bilanasaef'), ('asaef', '0034_Migrate_pers_signif_to_bilans'), ('asaef', '0035_add_bilananalyse_destinataires'), ('asaef', '0036_migrate_bilananalyse_destinataires'), ('asaef', '0037_migrate_service_orienteur'), ('asaef', '0038_Add_asaef_autoevaluation'), ('asaef', '0039_Modif_noms_bilans_asaef'), ('asaef', '0040_intervention_suiviasaef_intervenants_asaef'), ('asaef', '0041_remove_suiviasaef_asaef_equipe_and_more')]

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('carrefour', '0037_Add_famille_archived_at'),
    ]

    operations = [
        migrations.CreateModel(
            name='FamilleAsaef',
            fields=[
            ],
            options={
                'proxy': True,
                'indexes': [],
                'constraints': [],
                'verbose_name': 'Famille ASAEF',
                'verbose_name_plural': 'Familles ASAEF',
            },
            bases=('carrefour.famille',),
        ),
        migrations.CreateModel(
            name='JournalAsaef',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(verbose_name='date/heure')),
                ('theme', models.CharField(max_length=100, verbose_name='thème')),
                ('texte', models.TextField(verbose_name='contenu')),
                ('auteur', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='auteur')),
                ('famille', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='journaux', to='asaef.familleasaef', verbose_name='Famille')),
            ],
            options={
                'abstract': False,
                'verbose_name': 'Journal ASAEF',
                'verbose_name_plural': 'Journaux ASAEF',
            },
        ),
        migrations.CreateModel(
            name='SuiviAsaef',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('difficultes', models.TextField(blank=True, verbose_name='Difficultés')),
                ('aides', models.TextField(blank=True, verbose_name='Aides souhaitées')),
                ('autres_contacts', models.TextField(blank=True, verbose_name='Autres services contactés')),
                ('disponibilites', models.TextField(blank=True, verbose_name='Disponibilités')),
                ('remarque', models.TextField(blank=True)),
                ('service_orienteur', models.CharField(blank=True, choices=[('ope', 'OPE'), ('opec', 'OPEC'), ('open', 'OPEN'), ('opelovdt', 'OPELOVDT')], max_length=15, verbose_name="Orienté vers l'ASAEF par")),
                ('motif_demande', common.fields.ChoiceArrayField(base_field=models.CharField(choices=[('parentalite', 'Soutien à la parentalité'), ('education', "Aide éducative à l'enfant"), ('developpement', "Répondre aux besoins de développement de l'enfant"), ('integration', "Soutien à l'intégration sociale")], max_length=60), blank=True, null=True, size=None, verbose_name='Motif de la demande')),
                ('mandat_ope', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=[('volontaire', 'Mandat volontaire'), ('curatelle', 'Curatelle 308.1'), ('curatelle2', 'Curatelle 308.2'), ('referent', 'Référent'), ('enquete', 'Enquête'), ('tutelle', 'Tutelle'), ('justice', 'Mandat judiciaire')], max_length=65), blank=True, null=True, size=None, verbose_name='Mandat OPE')),
                ('date_demande', models.DateField(blank=True, default=None, null=True, verbose_name='Dépôt demande MAS')),
                ('date_analyse', models.DateField(blank=True, default=None, null=True, verbose_name='Date début analyse')),
                ('date_debut_suivi', models.DateField(blank=True, default=None, null=True, verbose_name='Début accomp.')),
                ('date_fin_suivi', models.DateField(blank=True, default=None, null=True, verbose_name='Fin accomp.')),
                ('date_bilan_1mois', models.DateField(blank=True, default=None, null=True, verbose_name='Bilan du soutien intensif')),
                ('date_bilan_analyse', models.DateField(blank=True, default=None, null=True, verbose_name='Date bilan analyse')),
                ('date_bilan_3mois', models.DateField(blank=True, default=None, null=True, verbose_name='Bilan d’autonomisation')),
                ('date_bilan_final', models.DateField(blank=True, default=None, null=True, verbose_name='Bilan final')),
                ('demarche', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=[('volontaire', 'Volontaire'), ('contrainte', 'Contrainte'), ('post_placement', 'Post placement'), ('non_placement', 'Eviter placement')], max_length=60), blank=True, null=True, size=None, verbose_name='Démarche')),
                ('pers_famille_presentes', models.CharField(blank=True, max_length=200, verbose_name='Membres famille présents')),
                ('autres_pers_presentes', models.CharField(blank=True, max_length=100, verbose_name='Autres pers. présentes')),
                ('motif_fin_suivi', models.CharField(blank=True, choices=[('desengagement', 'Désengagement'), ('evol_positive', 'Evolution positive'), ('relai', 'Relai vers autre service'), ('placement', 'Placement'), ('non_aboutie', 'Demande non aboutie'), ('erreur', 'Erreur de saisie'), ('autres', 'Autres'), ('abandon_famille', 'Refus de la famille'), ('abandon_aemo', 'Refus de l’AEMO'), ('abandon_asaef', 'Refus de l’ASAEF'), ('abandon_batoude', 'Refus de la Batoude'), ('abandon_mase', 'MASE refusé'), ('abandon_relais', 'Réorientation'), ('abandon_indisponible', 'Manque de disponibilité')], max_length=20, verbose_name='Motif de fin de suivi')),
                ('famille', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='asaef.familleasaef')),
                ('ope_referent', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='suivisasaef', to='carrefour.contact', verbose_name='as. OPE')),
                ('ope_referent2', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='suivisasaef2', to='carrefour.contact', verbose_name='as. OPE 2')),
                ('intervenants_presents', models.CharField(blank=True, max_length=250, verbose_name='Intervenants Asaef présents')),
                ('date_prolongation', models.DateField(blank=True, default=None, null=True, verbose_name='Prolongation')),
                ('projet', models.TextField(blank=True, verbose_name='Projet commun')),
                ('autoevaluation', models.SmallIntegerField(blank=True, choices=[(6, '6 Très bien'), (5, '5 Bien'), (4, '4 Suffisant'), (3, '3 Insuffisant'), (2, '2 Très insuffisant')], null=True, verbose_name='Auto-évaluation')),
            ],
            options={
                'verbose_name': 'Suivi ASAEF',
                'verbose_name_plural': 'Suivis ASAEF',
                'permissions': (
                    ('view_asaef', 'Consulter les dossiers ASAEF'), ('manage_asaef', 'RE ASAEF'), ('change_asaef', 'Gérer les dossiers ASAEF')
                ),
            },
        ),
        migrations.CreateModel(
            name='PrestationAsaef',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_prestation', models.DateField(verbose_name='date de la prestation')),
                ('duree', models.DurationField(verbose_name='durée')),
                ('famille', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='prestations_asaef', to='asaef.familleasaef', verbose_name='Famille')),
                ('lib_prestation', models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='prestations_%(app_label)s', to='carrefour.libelleprestation')),
                ('texte', models.TextField(blank=True, verbose_name='contenu')),
                ('theme', models.CharField(blank=True, max_length=100, verbose_name='thème')),
                ('intervenant', models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='prestations_asaef', to=settings.AUTH_USER_MODEL)),
                ('familles_actives', models.PositiveSmallIntegerField(blank=True, default=0)),
            ],
            options={
                'ordering': ('-date_prestation',),
                'verbose_name': 'Prestation ASAEF',
                'verbose_name_plural': 'Prestations ASAEF'
            },
        ),
        migrations.CreateModel(
            name='BilanAsaef',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('typ', models.CharField(choices=[('1mois', 'Bilan du soutien intensif'), ('3mois', 'Bilan d’autonomisation'), ('final', 'Bilan final'), ('suppl', 'Bilan suppl.')], max_length=10, verbose_name='Type de bilan')),
                ('date', models.DateField(verbose_name='Date du bilan')),
                ('interv_asaef', models.CharField(blank=True, max_length=250, verbose_name='Intervenant-e-s ASAEF')),
                ('membres_famille', models.CharField(blank=True, max_length=250, verbose_name='Membres de la famille')),
                ('autres', models.CharField(blank=True, max_length=250, verbose_name='Autres pers. présentes')),
                ('freq_interv', models.CharField(blank=True, max_length=250, verbose_name='Fréquence des interventions')),
                ('objectif', models.TextField(blank=True, verbose_name='Rappel des objectifs')),
                ('moyens', models.TextField(blank=True, verbose_name='Moyens de réalisation')),
                ('evol_obj', models.TextField(blank=True, verbose_name='Évolution des objectifs')),
                ('situation_fam', models.TextField(blank=True, verbose_name='État actuel de la situation familiale')),
                ('ajustement', models.TextField(blank=True, verbose_name='Réajustements nécessaires')),
                ('acc_as', models.TextField(blank=True, verbose_name="Accomp. de l'AS ASAEF")),
                ('acc_psy', models.TextField(blank=True, verbose_name='Accomp. du ou de la psy. ASAEF')),
                ('evol_intensite', models.TextField(blank=True, verbose_name="Contexte d'évolution de l'intensité")),
                ('avenir', models.TextField(blank=True, verbose_name="Perspectives d'avenir")),
                ('relais', models.TextField(blank=True, verbose_name='Relais envisageables')),
                ('amelioration', models.TextField(blank=True, verbose_name='Améliorations possibles')),
                ('evol_famille', models.TextField(blank=True, verbose_name='Évolution selon la famille')),
                ('evol_asaef', models.TextField(blank=True, verbose_name="Évolution selon l'ASAEF")),
                ('evolution_fam', models.TextField(blank=True, verbose_name='Évolution de la situation familiale')),
                ('prolongation', models.TextField(blank=True, verbose_name='Prolongation')),
                ('ressources', models.TextField(blank=True, verbose_name='Ressources mobilisées et compétences développées')),
                ('famille', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='bilans', to='asaef.familleasaef')),
                ('destinataires', models.ManyToManyField(related_name='bilans', to='carrefour.personne')),
            ],
            options={},
        ),
        migrations.CreateModel(
            name='BilanAnalyseAsaef',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(blank=True, null=True, verbose_name='Date du bilan')),
                ('interv_asaef', models.CharField(blank=True, max_length=250, verbose_name='Intervenant-e-s ASAEF')),
                ('membres_famille', models.CharField(blank=True, max_length=250, verbose_name='Membres de la famille')),
                ('autres', models.CharField(blank=True, max_length=250, verbose_name='Autres pers. présentes')),
                ('rencontre', models.CharField(blank=True, max_length=50, verbose_name='Nbre de rencontres')),
                ('rdv_manque', models.TextField(blank=True, verbose_name='RDV annulés (nbre, motifs)')),
                ('acceptation', models.TextField(blank=True, verbose_name="Degré d'acceptation de la démarche et vécu de la famille")),
                ('rappel_demande', models.TextField(blank=True, verbose_name='Rappel de la demande')),
                ('contexte', models.TextField(blank=True, verbose_name="Contexte d'observation")),
                ('observation', models.TextField(blank=True, verbose_name='Observations')),
                ('outil', models.TextField(blank=True, verbose_name="Outils d'analyse")),
                ('problematique', models.TextField(blank=True, verbose_name='Identification de la problématique')),
                ('hypothese', models.TextField(blank=True, verbose_name='Hypothèses sur les causes de la problématique')),
                ('obj_general', models.TextField(blank=True, verbose_name='Objectif général')),
                ('obj_specifique', models.TextField(blank=True, verbose_name='Objectifs spécifiques')),
                ('moyen', models.TextField(blank=True, verbose_name='Moyens')),
                ('planification', models.TextField(blank=True, verbose_name="Planification de l'intervention")),
                ('famille', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='bilans_analyses', to='asaef.familleasaef')),
                ('destinataires', models.ManyToManyField(related_name='bilans_analyses', to='carrefour.personne')),
            ],
            options={
                'verbose_name': "Bilan de l'analyse ASAEF",
            },
        ),
        migrations.CreateModel(
            name='ProlongationAsaef',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_debut', models.DateField(verbose_name='Date de début')),
                ('date_fin', models.DateField(blank=True, null=True, verbose_name='Date de fin')),
                ('autres', models.CharField(blank=True, max_length=250, verbose_name='Autres pers. présentes')),
                ('intervenants', models.CharField(blank=True, max_length=250, verbose_name='Intervenant-e-s')),
                ('membres_famille', models.CharField(blank=True, max_length=250, verbose_name='Membres de la famille')),
                ('bilan', models.TextField(blank=True, verbose_name='Bilan')),
                ('decision', models.TextField(blank=True, verbose_name='Décision')),
                ('famille', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='prolongations_asaef', to='asaef.familleasaef')),
            ],
            options={
                'verbose_name': 'Prolongation ASAEF',
                'verbose_name_plural': 'Prolongations ASAEF',
                'ordering': ('date_debut',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Intervention',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_debut', models.DateField(blank=True, null=True, verbose_name='Date début')),
                ('date_fin', models.DateField(blank=True, null=True, verbose_name='Date fin')),
                ('role', models.CharField(choices=[('re', 'Resp. Equipe'), ('coord', 'Educ. de coord.'), ('educ', 'Educ.'), ('as', 'Assist. soc.'), ('psy', 'Psychologue')], max_length=15, verbose_name='Rôle')),
                ('educ', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='interventions_asaef', to=settings.AUTH_USER_MODEL)),
                ('suivi', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='interventions_asaef', to='asaef.suiviasaef')),
            ],
        ),
        migrations.AddField(
            model_name='suiviasaef',
            name='intervenants_asaef',
            field=models.ManyToManyField(blank=True, related_name='suivis', through='asaef.Intervention', to=settings.AUTH_USER_MODEL),
        ),

    ]
