# Generated by Django 4.2.6 on 2023-10-31 05:45

from django.db import migrations

"""
Remplace le service orienteur par le service auquel est rattaché l'assistant social OPE
"""


def migrate_service_orienteur(apps, schema_editor):
    SuiviAsaef = apps.get_model('asaef', 'SuiviAsaef')

    suivis = SuiviAsaef.objects.filter(famille__archived_at__isnull=True, ope_referent__isnull=False)
    for suivi in suivis:
        suivi.service_orienteur = suivi.ope_referent.service.sigle.lower()
        suivi.save()


class Migration(migrations.Migration):

    dependencies = [
        ('asaef', '0036_migrate_bilananalyse_destinataires'),
        ('carrefour', '0037_Add_famille_archived_at'),
    ]

    operations = [
        migrations.RunPython(migrate_service_orienteur)
    ]
