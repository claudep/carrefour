# Generated by Django 3.1 on 2021-03-04 06:19

from django.db import migrations


def apply_migration(apps, schema_editor):
    """
    Ajoute un BilanAnalyseAsaef à toutes les familels ASAEF
    """

    FamilleAsaef = apps.get_model('asaef', 'FamilleAsaef')
    BilanAnalyseAsaef = apps.get_model('asaef', 'BilanAnalyseAsaef')

    for famille in FamilleAsaef.objects.all():
        BilanAnalyseAsaef.objects.create(famille=famille)


class Migration(migrations.Migration):

    dependencies = [
        ('asaef', '0021_Add_BilanAnalyseAsaef'),
    ]

    operations = [
        migrations.RunPython(apply_migration)
    ]
