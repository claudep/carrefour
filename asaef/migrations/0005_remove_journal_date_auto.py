from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('asaef', '0004_ajouter_journal'),
    ]

    operations = [
        migrations.AlterField(
            model_name='journalasaef',
            name='date',
            field=models.DateTimeField(verbose_name='date/heure'),
        ),
    ]
