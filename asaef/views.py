from datetime import date, timedelta
from io import BytesIO
from itertools import groupby

from dal import autocomplete

from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.core.exceptions import PermissionDenied
from django.http import Http404, HttpResponse, HttpResponseRedirect, JsonResponse
from django.db.models import Q, Sum
from django.shortcuts import get_object_or_404, reverse
from django.utils.text import slugify
from django.views.generic import (
    UpdateView, CreateView, ListView, DeleteView, TemplateView, View
)

from asaef.docx import DocxBilanAsaef
from asaef.forms import (
    AgendaAsaefForm, BilanAsaefForm, BilanAnalyseAsaefForm,  DemandeAsaefForm, FamilleAsaefForm,
    InterventionForm, PrestationAsaefForm, ProjetAsaefForm, ProlongationAsaefForm, SuiviAsaefForm
)
from asaef.models import (
    BilanAnalyseAsaef, BilanAsaef, FamilleAsaef, PrestationAsaef, ProlongationAsaef,
    SuiviAsaef
)
from asaef.pdf import (
    AnalyseDemandePdf, BilanAnalysePdf, BilanPdf, ContratCollaborationPdf, CoordonneesPdf,
    JournalAsaefPdf, ProlongationAsaefPdf
)
from carrefour.forms import JournalPopupForm
from carrefour.models import LibellePrestation, Utilisateur, Role
from carrefour.docx_base import docx_content_type
from carrefour.utils import get_selected_date
from carrefour.views import (
    BaseAdminPrestationListView, BaseDemandeView, BasePDFView, PaginateListView, MSG_NO_ACCESS, MSG_READ_ONLY
)


class AsaefCheckPermMixin:
    def dispatch(self, request, *args, **kwargs):
        if not FamilleAsaef.check_perm_view(request.user):
            raise PermissionDenied(MSG_NO_ACCESS)
        self.famille = get_object_or_404(FamilleAsaef,
                                         pk=kwargs['pk']) if 'pk' in self.kwargs else None
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.famille:
            can_edit = self.famille.can_edit(self.request.user)
            if not can_edit:
                messages.info(self.request, MSG_READ_ONLY)
            context['can_edit'] = can_edit
            context['famille'] = self.famille
        else:
            context['can_edit'] = self.request.user.has_perm('asaef.change_asaef')
        return context


class AsaefCheckOwnerMixin:
    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        if obj.intervenant == request.user or request.user.is_superuser:
            return super().dispatch(request, *args, **kwargs)
        raise PermissionDenied(MSG_NO_ACCESS)


class AsaefAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Utilisateur.intervenants_asaef()
        if self.q:
            qs = qs.filter(nom__istartswith=self.q)
        return qs


class FamilleCreateView(AsaefCheckPermMixin, CreateView):
    template_name = 'aemo/famille_edit.html'
    model = FamilleAsaef
    form_class = FamilleAsaefForm
    action = 'Nouvelle famille'

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(reverse('asaef-famille-list'))


class FamilleUpdateView(AsaefCheckPermMixin, UpdateView):
    template_name = 'aemo/famille_edit.html'
    context_object_name = 'famille'
    model = FamilleAsaef
    form_class = FamilleAsaefForm
    title = 'Modification'

    def form_valid(self, form):
        form.save()
        if form.has_changed:
            messages.info(self.request, "Les modifications ont été enregistrées avec succès.")
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('asaef-famille-edit', args=[self.get_object().pk])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'enfant_suivi': Role.objects.get(nom='Enfant suivi'),
            'enfant_non_suivi': Role.objects.get(nom="Enfant non-suivi"),
        })
        return context


class FamilleListView(AsaefCheckPermMixin, PaginateListView):
    template_name = 'asaef/famille_list.html'
    model = FamilleAsaef

    def get_queryset(self):
        return FamilleAsaef.actives().select_related('suiviasaef')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'options_etapes_suivi': [(key, etape.nom) for key, etape in SuiviAsaef.WORKFLOW.items()],
            'liste_attente': FamilleAsaef.objects.filter(
                suiviasaef__date_demande__isnull=False,
                suiviasaef__date_analyse__isnull=True,
                suiviasaef__date_fin_suivi__isnull=True
            ).order_by('suiviasaef__date_demande')
        })
        return context


class FamilleArchivableListe(View):
    """Return all family ids which are deletable by the current user."""

    def get(self, request, *args, **kwargs):
        data = [famille.pk for famille in FamilleAsaef.objects.filter(archived_at__isnull=True)
                if famille.can_be_deleted(request.user)]
        return JsonResponse(data, safe=False)


class AsaefSuiviView(AsaefCheckPermMixin, SuccessMessageMixin, UpdateView):
    template_name = 'asaef/suivi_edit.html'
    model = SuiviAsaef
    form_class = SuiviAsaefForm
    success_message = 'Les modifications ont été enregistrées avec succès'

    def get_object(self, queryset=None):
        famille = get_object_or_404(FamilleAsaef, pk=self.kwargs['pk'])
        return famille.suiviasaef

    def get_success_url(self):
        return reverse('asaef-famille-suivi', args=[self.object.famille.pk])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'famille': self.object.famille,
            'documents': self.object.famille.documents.order_by('-date_creation'),
            'interventions': self.object.interventions_asaef.filter(date_fin__isnull=True)
        })
        return context


class DemandeView(AsaefCheckPermMixin, BaseDemandeView):
    model = SuiviAsaef
    form_class = DemandeAsaefForm

    def get_object(self, queryset=None):
        famille = get_object_or_404(FamilleAsaef, pk=self.kwargs['pk'])
        suivi = self.prepare_form(famille.suiviasaef)
        if suivi.intervenants_presents == '':
            suivi.intervenants_presents = ' - '.join([ref.nom_prenom for ref in suivi.suivi_referents])
        return suivi


class PrestationMenu(AsaefCheckPermMixin, TemplateView):
    template_name = 'asaef/prestation_menu.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        annee = date.today().year

        context.update({
            'familles': FamilleAsaef.actives().annotate(
                user_prest=Sum('prestations_asaef__duree',
                               filter=Q(prestations_asaef__intervenant=user))
            ).order_by('nom', 'npa'),
            'annee': annee,
        })
        return context


class PrestationBaseView:
    template_name = 'asaef/prestation_edit.html'
    model = PrestationAsaef
    form_class = PrestationAsaefForm

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm('asaef.view_asaef'):
            raise PermissionDenied(MSG_NO_ACCESS)
        self.famille = get_object_or_404(FamilleAsaef, pk=self.kwargs['pk']) \
            if 'pk' in self.kwargs and self.kwargs['pk'] > 0 else None
        if self.famille:
            if self.famille.can_edit(request.user) and not self.famille.membres_suivis().exists():
                raise PermissionDenied("Vous devez d'abord saisir un Enfant avant une prestation.")
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'famille': self.famille,
        })
        return context

    def get_success_url(self):
        if self.famille:
            return reverse('asaef-prestation-famille-list', args=[self.famille.pk])
        return reverse('asaef-prestation-gen-list')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['famille'] = self.famille
        kwargs['user'] = self.request.user
        return kwargs


class PrestationListView(AsaefCheckPermMixin, TemplateView):
    template_name = 'asaef/prestation_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'pk' in self.kwargs:
            famille = get_object_or_404(FamilleAsaef, pk=self.kwargs['pk'])
            prestations = famille.prestations.all().order_by('-date_prestation')
            historique = famille.prestations_historiques()
            context['famille'] = famille
        else:
            famille = None
            prestations = self.request.user.prestations_asaef.filter(famille=None)
            historique = PrestationAsaef.prestations_historiques(prestations)
        grouped_by_month = {
            k: list(prests) for (k, prests) in groupby(
                prestations, lambda p: f'{p.date_prestation.year}_{p.date_prestation.month}'
            )
        }
        context['prestations'] = [
            (hist, grouped_by_month.get(f"{hist['annee']}_{hist['mois']}"))
            for hist in historique
        ]
        return context


class PrestationCreateView(AsaefCheckPermMixin, PrestationBaseView, CreateView):
    action = 'Création'

    def dispatch(self, request, *args, **kwargs):
        if request.user.has_perm('asaef.change_asaef'):
            return super().dispatch(request, *args, **kwargs)
        raise PermissionDenied(MSG_NO_ACCESS)

    def get_initial(self):
        initial = super().get_initial()
        initial['date_prestation'] = date.today()
        initial['duree'] = timedelta(0)
        return initial

    def form_valid(self, form):
        if self.famille:
            form.instance.famille = self.famille
        form.instance.intervenant = self.request.user
        return super().form_valid(form)


class PrestationUpdateView(AsaefCheckOwnerMixin, PrestationBaseView, UpdateView):
    pk_url_kwarg = 'obj_pk'
    action = 'Modification'

    def delete_url(self):
        return reverse(
            'asaef-prestation-delete',
            args=[self.object.famille.pk if self.object.famille else 0, self.object.pk]
        )


class PrestationDeleteView(AsaefCheckOwnerMixin, DeleteView):
    template_name = 'carrefour/object_confirm_delete.html'
    model = PrestationAsaef
    pk_url_kwarg = 'obj_pk'

    def get_success_url(self):
        if self.kwargs['pk'] > 0:
            return reverse('asaef-prestation-famille-list', args=[self.kwargs['pk']])
        return reverse('asaef-prestation-menu')


class PrestationRecapPersoView(PrestationBaseView, ListView):
    template_name = 'asaef/prestation_recap_perso.html'
    start_date = date(2020, 9, 1)  # Month of the first record

    def dispatch(self, request, *args, **kwargs):
        str_curdate = request.GET.get('date', None)
        self.dfrom, self.dto = get_selected_date(str_curdate)
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return self.request.user.prestations_asaef.filter(
            date_prestation__gte=self.dfrom, date_prestation__lte=self.dto
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        prev_month = self.dfrom - timedelta(days=2)
        next_month = self.dfrom + timedelta(days=33)

        context.update({
            'current_date': self.dfrom,
            'prev_month': prev_month if prev_month >= self.start_date else None,
            'next_month': next_month if next_month <= date.today() else None,
            'totaux': [(pp.code, self.get_queryset().filter(
                lib_prestation__code=f"{pp.code}"
            ).aggregate(
                tot=Sum('duree')
            )['tot']) for pp in LibellePrestation.objects.filter(unite='asaef')],
            'total_final': self.get_queryset().filter(
                lib_prestation__unite='asaef'
            ).aggregate(tot=Sum('duree'))['tot'],
        })
        return context


class AdminPrestationListView(AsaefCheckPermMixin, BaseAdminPrestationListView):
    prest_model = PrestationAsaef
    titre = 'Interv. Asaef'

    def get_queryset(self):
        return Utilisateur.intervenants_asaef(annee=self.annee)


class AnalyseDemandePDFView(BasePDFView):
    obj_class = FamilleAsaef
    pdf_class = AnalyseDemandePdf


class ContratCollaborationPDFView(AsaefCheckPermMixin, BasePDFView):
    obj_class = FamilleAsaef
    pdf_class = ContratCollaborationPdf


class CoordonneesFamillePDFView(BasePDFView):
    obj_class = FamilleAsaef
    pdf_class = CoordonneesPdf


class JournalPDFView(BasePDFView):
    obj_class = FamilleAsaef
    pdf_class = JournalAsaefPdf


class JournalListView(AsaefCheckPermMixin, PaginateListView):
    template_name = 'asaef/journal_list.html'
    model = PrestationAsaef
    paginate_by = 40

    def post(self, request, *args, **kwargs):
        selector = request.POST.get('selector', None)
        return HttpResponseRedirect(
            reverse('asaef-print-journal', args=[self.famille.pk]) + (
                f'?selector={selector}' if selector else ''
            )
        )

    def get_queryset(self):
        return self.famille.prestations.all().order_by('-date_prestation')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context.update({
            'famille': self.famille,
            'form': JournalPopupForm(initial={'selector': 'month'}),
        })
        return context


def print_docx(request, pk, docname):
    documents = {
        'bilan_1mois': {
            'file_template': '{}_bilan_1mois.docx',
            'titre': "1er mois d'action et de soutien",
            'date': 'date_bilan_1mois',
            'doc_class': DocxBilanAsaef
        },
        'bilan_3mois': {
            'file_template': '{}_bilan_3mois.docx',
            'titre': '3 mois',
            'date': 'date_bilan_3mois',
            'doc_class': DocxBilanAsaef
        },
        'bilan_final': {
            'file_template': '{}_bilan_final.docx',
            'titre': 'Final',
            'date': 'date_bilan_final',
            'doc_class': DocxBilanAsaef
        },
        'bilan_analyse': {
            'file_template': '{}_bilan_analyse.docx',
            'titre': "de l'analyse",
            'date': 'date_bilan_analyse',
            'doc_class': DocxBilanAsaef
        }
    }
    famille = get_object_or_404(FamilleAsaef, pk=pk)
    if docname not in documents:
        raise Http404
    doc = documents[docname]
    tampon = BytesIO()
    docx = doc['doc_class'](tampon)
    docx.produce(famille, doc['titre'], doc['date'])

    filename = doc['file_template'].format(slugify(famille.nom))
    response = HttpResponse(tampon.getvalue(), content_type=docx_content_type)
    response['Content-Disposition'] = 'attachment; filename="{}"'.format(filename)
    return response


class ProjetUpdateView(AsaefCheckPermMixin, UpdateView):
    template_name = 'asaef/projet_edit.html'
    model = SuiviAsaef
    form_class = ProjetAsaefForm

    def get_object(self, queryset=None):
        famille = get_object_or_404(FamilleAsaef, pk=self.kwargs['pk'])
        return famille.suiviasaef

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'famille': self.object.famille,
        })
        return context

    def get_success_url(self):
        return reverse('asaef-famille-suivi', args=[self.object.famille.pk])


class AgendaUpdateView(AsaefCheckPermMixin, SuccessMessageMixin, UpdateView):
    template_name = 'asaef/agenda_edit.html'
    model = SuiviAsaef
    form_class = AgendaAsaefForm
    success_message = 'Les modifications ont été enregistrées avec succès'

    def get_object(self, queryset=None):
        famille = get_object_or_404(FamilleAsaef, pk=self.kwargs['pk'])
        return famille.suiviasaef

    def get_success_url(self):
        return reverse('asaef-famille-agenda', args=[self.object.famille.pk])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        bilan_list = self.object.famille.bilans.all()
        context.update({
            'famille': self.object.famille,
            'mode': 'preparation' if (self.object.date_debut_suivi is None
                                      and self.object.date_fin_suivi is None) else 'suivi',
            'bilans_1mois': [b for b in bilan_list if b.typ == '1mois'],
            'bilans_3mois': [b for b in bilan_list if b.typ == '3mois'],
            'bilans_finaux': [b for b in bilan_list if b.typ == 'final'],
        })
        return context

    def form_valid(self, form):
        suivi = form.save()
        if suivi.date_fin_suivi and suivi.motif_fin_suivi and suivi.autoevaluation:
            suivi.interventions_asaef.filter(date_fin__isnull=True).update(date_fin=suivi.date_fin_suivi)
        return HttpResponseRedirect(self.get_success_url())


class SuivisTerminesListView(AsaefCheckPermMixin, PaginateListView):
    template_name = 'asaef/suivis_termines_list.html'
    model = FamilleAsaef

    def get_queryset(self):
        return super().get_queryset().filter(
            suiviasaef__date_fin_suivi__isnull=False,
        ).exclude(archived_at__isnull=False).select_related('suiviasaef')


class BilanAsaefBase(AsaefCheckPermMixin):
    template_name = 'asaef/bilan.html'
    model = BilanAsaef
    form_class = BilanAsaefForm

    def dispatch(self, request, *args, **kwargs):
        self.famille = get_object_or_404(FamilleAsaef, pk=kwargs.get('pk'))
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('asaef-famille-agenda', args=[self.famille.pk])


class BilanAsaefCreateView(BilanAsaefBase, CreateView):

    def dispatch(self, request, *args, **kwargs):
        self.typ = self.kwargs.get('typ')
        return super().dispatch(request, *args, **kwargs)

    def get_initial(self):
        initial = super().get_initial()
        initial['date'] = getattr(self.famille.suivi, f"date_bilan_{self.typ}")
        initial['interv_asaef'] = ' - '.join([ref.nom_prenom for ref in self.famille.suivi.intervenants_asaef.all()])
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['typ'] = self.typ
        kwargs['famille'] = self.famille
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'typ': self.typ,
            'famille': self.famille,
            'titre': dict(BilanAsaef.BILAN_CHOICES)[self.typ],
        })
        return context


class BilanAsaefUpdateView(BilanAsaefBase, UpdateView):
    pk_url_kwarg = 'obj_pk'


class BilanAsaefPdfView(BasePDFView):
    obj_class = BilanAsaef
    pdf_class = BilanPdf

    def get_object(self, queryset=None):
        return get_object_or_404(BilanAsaef, pk=self.kwargs['obj_pk'])


class BilanAnalyseCreateView(AsaefCheckPermMixin, CreateView):
    template_name = 'asaef/bilan_analyse.html'
    model = BilanAnalyseAsaef
    form_class = BilanAnalyseAsaefForm
    pk_url_kwarg = 'obj_pk'

    def get_initial(self):
        initial = super().get_initial()
        initial['date'] = date.today()
        initial['interv_asaef'] = ' - '.join([ref.nom_prenom for ref in self.famille.suivi.intervenants_asaef.all()])
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['famille'] = self.famille
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'famille': self.famille,
            'titre': "Bilan d'analyse",
        })
        return context

    def get_success_url(self):
        return reverse('asaef-famille-agenda', args=[self.famille.pk])


class BilanAnalyseUpdateView(AsaefCheckPermMixin, SuccessMessageMixin, UpdateView):
    template_name = 'asaef/bilan_analyse.html'
    model = BilanAnalyseAsaef
    form_class = BilanAnalyseAsaefForm
    success_message = 'Les modifications ont été enregistrées avec succès'
    pk_url_kwarg = 'obj_pk'

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(reverse('asaef-famille-agenda', args=[self.famille.pk]))


class BilanAnalysePrintView(BasePDFView):
    obj_class = BilanAnalyseAsaef
    pdf_class = BilanAnalysePdf

    def get_object(self, queryset=None):
        return get_object_or_404(BilanAnalyseAsaef, pk=self.kwargs['obj_pk'])


class ProlongationAsaefCreateView(AsaefCheckPermMixin, CreateView):
    template_name = 'asaef/prolongation_edit.html'
    model = ProlongationAsaef
    form_class = ProlongationAsaefForm

    def get_initial(self):
        initial = super().get_initial()
        initial['famille'] = self.famille
        initial['intervenants'] = ' - '.join(
            [ref.nom_prenom for ref in self.famille.suivi.intervenants_asaef.all()])
        return initial

    def get_success_url(self):
        return reverse('asaef-famille-agenda', args=[self.famille.pk])


class ProlongationAsaefUpdateView(AsaefCheckPermMixin, UpdateView):
    template_name = 'asaef/prolongation_edit.html'
    model = ProlongationAsaef
    pk_url_kwarg = 'obj_pk'
    form_class = ProlongationAsaefForm

    def get_object(self, queryset=None):
        return get_object_or_404(ProlongationAsaef, pk=self.kwargs['obj_pk'])

    def get_success_url(self):
        return reverse('asaef-famille-agenda', args=[self.famille.pk])


class ProlongationAsaefPrintView(BasePDFView):
    obj_class = ProlongationAsaef
    pdf_class = ProlongationAsaefPdf

    def get_object(self):
        return get_object_or_404(ProlongationAsaef, pk=self.kwargs['obj_pk'])


class InterventionUpdateView(UpdateView):
    template_name = 'asaef/intervention_edit.html'
    model = SuiviAsaef
    form_class = InterventionForm

    def form_valid(self, form):
        suivi = form.save()
        return HttpResponseRedirect(reverse('asaef-famille-suivi', args=[suivi.famille.pk]))
