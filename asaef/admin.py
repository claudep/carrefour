from django.contrib import admin

from carrefour.admin import TypePrestationFilter
from .models import (
    BilanAsaef, BilanAnalyseAsaef, FamilleAsaef, JournalAsaef, PrestationAsaef,
    SuiviAsaef
)


@admin.register(FamilleAsaef)
class FamilleAsaefAdmin(admin.ModelAdmin):
    list_display = ('nom', 'npa', 'localite', 'region')
    list_filter = ('region',)
    search_fields = ['nom', 'npa', 'localite']
    ordering = ('nom',)


@admin.register(SuiviAsaef)
class SuiviAsaefAdmin(admin.ModelAdmin):
    list_display = ('famille',)
    ordering = ('famille__nom',)
    search_fields = ('famille__nom',)


@admin.register(PrestationAsaef)
class PrestationAsaefAdmin(admin.ModelAdmin):
    list_filter = (TypePrestationFilter,)
    search_fields = ('famille__nom',)


@admin.register(BilanAsaef)
class BilanAsaefAdmin(admin.ModelAdmin):
    list_display = ['typ', 'famille', 'date']


@admin.register(BilanAnalyseAsaef)
class BilanAnalyseAsaefAdmin(admin.ModelAdmin):
    list_display = ['famille', 'date', 'interv_asaef']


admin.site.register(JournalAsaef)
