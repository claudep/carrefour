from tinymce.widgets import TinyMCE

from django import forms

from asaef.models import (
    BilanAnalyseAsaef, BilanAsaef, FamilleAsaef, Intervention, SuiviAsaef, PrestationAsaef, ProlongationAsaef
)
from carrefour.forms import (
    AgendaFormBase, CleanNpaLocaliteMixin, HMDurationField, PickDateWidget, PickTimeWidget, PrestationFormBase,
)
from carrefour.forms import BootstrapMixin, BSRadioSelect, BSCheckboxSelectMultiple
from carrefour.models import Contact, LibellePrestation, Utilisateur
from common.choices import MotifsFinSuivi


class DemandeAsaefForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = SuiviAsaef
        fields = (
            'pers_famille_presentes', 'intervenants_presents', 'autres_pers_presentes',
            'difficultes', 'aides', 'disponibilites', 'autres_contacts', 'remarque'
        )
        widgets = {
            'difficultes': TinyMCE,
            'aides': TinyMCE,
            'disponibilites': TinyMCE,
        }


class SuiviAsaefForm(BootstrapMixin, forms.ModelForm):
    ope_referent = forms.ModelChoiceField(
        queryset=Contact.membres_ope(), required=False,
    )
    ope_referent2 = forms.ModelChoiceField(
        queryset=Contact.membres_ope(), required=False,
    )

    class Meta:
        model = SuiviAsaef
        fields = (
            'ope_referent', 'ope_referent2', 'mandat_ope', 'motif_demande', 'demarche', 'remarque',
        )

    def save(self, commit=True):
        obj = super().save()
        if 'ope_referent' in self.changed_data or 'ope_referent2' in self.changed_data:
            first_referent = obj.ope_referent or obj.ope_referent2
            obj.service_orienteur = first_referent.service.sigle.lower() if first_referent else ''
            obj.save()
        return obj


class PrestationAsaefForm(PrestationFormBase):
    class Meta:
        model = PrestationAsaef
        fields = ('lib_prestation', 'date_prestation', 'duree', 'theme', 'texte')
        widgets = {
            'texte': TinyMCE,
            'date_prestation': PickDateWidget,
            'duree': PickTimeWidget,
        }
        field_classes = {
            'duree': HMDurationField,
        }

    def __init__(self, *args, user=None, **kwargs):
        self.famille = kwargs.pop('famille', None)
        super().__init__(*args, user=user, **kwargs)
        prest_codes = ['p09', 'p10'] if self.famille else ['p11', 'p12']
        self.fields['lib_prestation'] = forms.ModelChoiceField(
            label='Prestations',
            queryset=LibellePrestation.objects.filter(unite='asaef', code__in=prest_codes).order_by('code'),
            widget=BSRadioSelect,
            required=True,
            empty_label=None,
        )

    def clean(self):
        if self.famille and self.instance.pk is None and not self.famille.membres_suivis().exists():
            raise forms.ValidationError("Vous devez enregistrer un Enfant pour saisir une prestation!")
        return self.cleaned_data


class FamilleAsaefForm(BootstrapMixin, CleanNpaLocaliteMixin, forms.ModelForm):

    class Meta:
        model = FamilleAsaef
        exclude = ('genogramme', 'sociogramme', 'archived')

    def save(self, **kwargs):
        if self.instance.pk is None:
            famille = FamilleAsaef.objects.create_famille(
                **self.instance.__dict__
            )
        else:
            famille = super().save(**kwargs)
        return famille


class ProjetAsaefForm(forms.ModelForm):

    class Meta:
        model = SuiviAsaef
        fields = ('projet',)


class AgendaAsaefForm(AgendaFormBase):

    class Meta:
        model = SuiviAsaef
        fields = (
            'date_demande', 'date_analyse', 'date_bilan_analyse',
            'date_debut_suivi', 'date_prolongation', 'date_fin_suivi', 'motif_fin_suivi', 'autoevaluation'
        )
        widgets = {
            **{field: PickDateWidget for field in fields if field.startswith('date_')},
            'date_bilan_analyse': forms.HiddenInput,
        }

    def __init__(self, data=None, **kwargs):
        super().__init__(data=data, **kwargs)
        empty = [('', '--------')]
        has_suivi = self.instance.date_debut_suivi is not None or bool(data and data.get('date_debut_suivi'))
        self.fields['motif_fin_suivi'].choices = empty + (
            MotifsFinSuivi.choices_fin() if has_suivi else MotifsFinSuivi.choices_abandon('asaef')
        )

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['autoevaluation'] and 'date_fin_suivi' not in cleaned_data:
            raise forms.ValidationError("Auto-évaluation: Une date de fin de suivi est obligatoire.")
        if (cleaned_data['date_debut_suivi'] and cleaned_data['date_fin_suivi']
                and cleaned_data['autoevaluation'] is None):
            raise forms.ValidationError("Auto-évaluation: La saisie est obligatoire.")
        return cleaned_data


class BilanAsaefForm(BootstrapMixin, forms.ModelForm):

    fields_1mois = ['objectif', 'moyens', 'evol_obj', 'evolution_fam', 'ajustement',
                    'acc_as', 'acc_psy', 'evol_intensite']
    fields_3mois = ['situation_fam', 'objectif', 'moyens', 'evol_obj', 'evol_famille',
                    'evol_asaef', 'ajustement', 'acc_as', 'acc_psy', 'evol_intensite',
                    'amelioration', 'avenir', 'relais']
    fields_final = ['situation_fam', 'objectif', 'moyens', 'evol_famille', 'evol_asaef',
                    'acc_as', 'acc_psy', 'ressources', 'evol_intensite',
                    'amelioration', 'avenir', 'prolongation', 'relais']

    class Meta:
        model = BilanAsaef
        fields = ['date', 'destinataires', 'interv_asaef', 'membres_famille', 'autres', 'freq_interv']
        widgets = {'date': PickDateWidget}

    def __init__(self, *args, **kwargs):
        instance = kwargs.get('instance', None)
        self.typ = kwargs.pop('typ', None) if not instance else instance.typ
        self.famille = kwargs.pop('famille', None) if not instance else instance.famille
        super().__init__(*args, **kwargs)

        self.fields['destinataires'] = forms.ModelMultipleChoiceField(
            label='Destinataire(s) du bilan',
            queryset=self.famille.membres.exclude(role__nom__in=['Enfant suivi', 'Enfant non-suivi']),
            widget=BSCheckboxSelectMultiple()
        )

        for field in getattr(self, f"fields_{self.typ}"):
            self.fields[field] = forms.CharField(
                label=self._meta.model._meta.get_field(field).verbose_name,
                widget=TinyMCE(mce_attrs={'height': 250}),
                required=False,
                initial=getattr(self.instance, field)
            )

    def save(self, **kwargs):
        if self.instance.pk is None:
            self.instance.famille = self.famille
            self.instance.typ = self.typ
        for field in getattr(self, f"fields_{self.typ}"):
            setattr(self.instance, field, self.cleaned_data[field])

        instance = super().save(**kwargs)
        setattr(instance.famille.suivi, f"date_bilan_{instance.typ}", instance.date)
        instance.famille.suivi.save()
        return instance


class BilanAnalyseAsaefForm(forms.ModelForm):

    class Meta:
        model = BilanAnalyseAsaef
        fields = ['date', 'destinataires', 'interv_asaef', 'membres_famille', 'autres', 'rencontre', 'rdv_manque',
                  'acceptation', 'rappel_demande', 'contexte', 'observation', 'outil', 'problematique',
                  'hypothese', 'obj_general', 'obj_specifique', 'moyen', 'planification']
        widgets = {
            'date': PickDateWidget,
        }

    def __init__(self, *args, **kwargs):
        instance = kwargs.get('instance', None)
        self.famille = instance.famille if instance and instance.pk else kwargs.pop('famille', None)
        super().__init__(*args, **kwargs)

        self.fields['destinataires'] = forms.ModelMultipleChoiceField(
            label='Destinataire(s) du bilan',
            queryset=self.famille.membres.exclude(role__nom__in=['Enfant suivi', 'Enfant non-suivi']),
            widget=BSCheckboxSelectMultiple()
        )

        for field in ['acceptation', 'rappel_demande', 'contexte', 'observation', 'outil', 'problematique',
                      'hypothese', 'obj_general', 'obj_specifique', 'moyen', 'planification']:
            self.fields[field] = forms.CharField(
                label=self._meta.model._meta.get_field(field).verbose_name,
                widget=TinyMCE(mce_attrs={'height': 250}),
                required=False,
                initial=getattr(self.instance, field)
            )

    def save(self, **kwargs):
        if self.instance.pk is None:
            self.instance.famille = self.famille
        instance = super().save(**kwargs)
        self.famille.suivi.date_bilan_analyse = instance.date
        instance.famille.suivi.save()
        return instance


class ProlongationAsaefForm(BootstrapMixin, forms.ModelForm):

    class Meta:
        model = ProlongationAsaef
        fields = '__all__'
        widgets = {
            'date_debut': PickDateWidget,
            'date_fin': PickDateWidget,
            'bilan': TinyMCE(),
            'decision': TinyMCE(),
            'famille': forms.HiddenInput,
        }

    def save(self, commit=True):
        self.instance.save()
        self.instance.famille.suiviasaef.date_prolongation = self.instance.date_debut
        self.instance.famille.suiviasaef.save()
        return self.instance


class InterventionFormsetForm(BootstrapMixin, forms.ModelForm):

    class Meta:
        model = Intervention
        fields = ('educ', 'role', 'date_debut', 'date_fin')
        widgets = {
            'date_debut': PickDateWidget,
            'date_fin': PickDateWidget
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['educ'].queryset = Utilisateur.intervenants_asaef()

    def clean_date_debut(self):
        data = self.cleaned_data['date_debut']
        if data in ['', None]:
            raise forms.ValidationError('La date de début est obligatoire.')
        return data


class InterventionForm(BootstrapMixin, forms.ModelForm):

    class Meta:
        model = SuiviAsaef
        fields = ('id', )

    def __init__(self, data=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        InterventionFormset = forms.inlineformset_factory(SuiviAsaef, Intervention, form=InterventionFormsetForm)
        self.formset = InterventionFormset(
            instance=self.instance, data=data, queryset=Intervention.objects.filter(suivi=self.instance),
            prefix='inter'
        )
        self.formset.extra = 5 if self.formset.queryset.count() < 3 else 2

    def is_valid(self):
        return all([super().is_valid(), self.formset.is_valid()])

    def has_changed(self):
        return any([super().has_changed()] + self.formset.has_changed())

    def save(self, commit=True):
        self.formset.save()
        return self.instance

