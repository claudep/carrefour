from datetime import timedelta

from django.db.models import Q, Sum
from carrefour.export import ExportStatistique
from carrefour.models import Utilisateur
from ser.models import Prestation, Tshm

from .views import StatBaseView, StatHeureParIntervenantBaseView


class StatMensuelles(StatBaseView):
    template_name = "statistiques/ser_stats_mensuelles.html"
    unite = 'asap'

    def _init_counters(self, months):
        counters = {}
        motifs = [(key, val) for key, val in Prestation.MOTIFS_CHOICES] + [('tot_mois', 'Total mensuel')]
        for key, val in motifs:
            counters[key] = {month: timedelta() for month in months}
            counters[key]['titre'] = val
            counters[key]['tot_annee'] = timedelta()
        return counters

    def get_users(self, debut, fin):
        return Utilisateur.objects.filter(
            Q(date_desactivation__isnull=True) | Q(date_desactivation__gte=debut),
            groups__name__contains=self.unite
        )

    def produce(self, months):
        counters = {}
        debut, fin = self.get_debut_fin(months)
        for interv in self.get_users(debut, fin):
            key = f"{interv.nom} {interv.prenom}"
            if key not in counters:
                counters[key] = self._init_counters(months)
            prestations = interv.prestations_asap.filter(date__range=[debut, fin]).exclude(motif='')
            for prestation in prestations:
                month = (prestation.date.year, prestation.date.month)
                duree = prestation.duree_prestation
                counters[key][prestation.motif][month] += duree
                counters[key][prestation.motif]['tot_annee'] += duree
                counters[key]['tot_mois'][month] += duree
                counters[key]['tot_mois']['tot_annee'] += duree

        asap = self._init_counters(months)
        for educ, values in counters.items():
            for month in months:
                for motif in ['tshm', 'pe', 'bulle', 'autre']:
                    duree = counters[educ][motif][month]
                    asap[motif][month] += duree
                    asap[motif]['tot_annee'] += duree
                    asap['tot_mois'][month] += duree
                    asap['tot_mois']['tot_annee'] += duree
        counters['Total ASAP'] = asap
        return counters

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        data = self.produce(context['months'])
        context.update({
            'unite': self.unite,
            'data': data,
        })
        return context


class StatSortieView(StatBaseView):
    """ Les statistiques ne sont complètes que depuis février 2024 """
    template_name = "statistiques/ser_stats_sorties.html"
    unite = 'asap'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        debut, fin = self.get_debut_fin(context['months'])
        qs = Tshm.objects.filter(
            prestation__date__gte=debut, prestation__date__lte=fin
        ).order_by('prestation__date').select_related('prestation', 'prestation__auteur')

        context.update({
            'unite': self.unite,
            'data': qs,
            'duree_totale': qs.aggregate(tot=Sum('duree'))['tot'] or timedelta(),
            'titre': 'Sorties ASAP',
            'debut': self.date_start,
            'fin': self.date_end
        })
        return context

    def render_to_response(self, context, **response_kwargs):
        if self.export_flag:
            export = ExportStatistique()
            export.produce_sortie_asap('sorties_asap', context)
            return export.get_http_response('ASAP_sorties_asap')
        else:
            return super().render_to_response(context, **response_kwargs)


class StatSuiviView(StatBaseView):
    template_name = 'statistiques/ser_stats_suivi.html'
    unite = 'asap'

    def dispatch(self, request, *args, **kwargs):
        self.mode = self.kwargs.get('mode', None)
        if self.mode == 'relationnel':
            self.count_field = 'nbre_suivi_relationnel'
            self.sheet_title = "Suivis relationnels"
            self.export_filename = 'asap_suivis_relationnels'
        else:
            self.count_field = 'nbre_acc_educatif'
            self.sheet_title = "Accompagnements éducatifs"
            self.export_filename = 'asap_accomp_educatifs'
        return super().dispatch(request, *args, **kwargs)

    def produce(self, months):
        debut, fin = self.get_debut_fin(months)
        counters = {}
        prestations = Prestation.objects.filter(
            date__gte=debut, date__lte=fin
        )
        for prest in prestations:
            interv_key = f"{prest.auteur.nom} {prest.auteur.prenom}"
            if interv_key not in counters:
                counters[interv_key] = {}
                counters[interv_key].update({month: 0 for month in months})
                counters[interv_key]['total'] = 0
            month = (prest.date.year, prest.date.month)
            counters[interv_key][month] += getattr(prest, self.count_field)
            counters[interv_key]['total'] += getattr(prest, self.count_field)
        return counters

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'months' in context:
            context.update({
                'unite': self.unite,
                'titre': self.sheet_title,
                'suivis': self.produce(context['months']),
                'mode': self.mode,
            })
        return context

    def render_to_response(self, context, **response_kwargs):
        if self.export_flag:
            export = ExportStatistique()
            export.produce_suivi_ser(self.sheet_title, context)
            return export.get_http_response(self.export_filename)
        else:
            return super().render_to_response(context, **response_kwargs)


class StatHeureParIntervenantView(StatHeureParIntervenantBaseView):
    template_name = "statistiques/stats_heure_intervenant.html"
    unite = 'asap'


class StatRegion(StatBaseView):
    template_name = 'statistiques/ser_stats_region.html'
    unite = 'asap'

    def _init_counters(self, months):
        counters = {}
        motifs = [(key, val) for key, val in Tshm.TYP_CHOICES] + [('tot_mois', 'Total mensuel')]
        for key, val in motifs:
            counters[key] = {month: timedelta() for month in months}
            counters[key]['titre'] = val
            counters[key]['tot_annee'] = timedelta()
        return counters

    def produce(self, months):
        debut, fin = self.get_debut_fin(months)
        counters = {}
        sorties = Tshm.objects.filter(
            prestation__date__gte=debut, prestation__date__lte=fin
        ).filter(
            Q(lieu__version=2) | Q(lieu__isnull=True)
        ).order_by('prestation__date').select_related('prestation', 'lieu')
        # version 2: Simplification des lieux depuis mars 2024
        for sortie in sorties:
            place = sortie.lieu.nom if sortie.lieu else 'Lieu non saisi'
            duree = sortie.duree
            if place not in counters:
                counters[place] = self._init_counters(months)
            month = (sortie.prestation.date.year, sortie.prestation.date.month)
            counters[place][sortie.typ][month] += duree
            counters[place][sortie.typ]['tot_annee'] += duree
            counters[place]['tot_mois'][month] += duree
            counters[place]['tot_mois']['tot_annee'] += duree

        asap = self._init_counters(months)
        for regio, values in counters.items():
            for month in months:
                for motif in [key for key, value in Tshm.TYP_CHOICES]:
                    duree = counters[regio][motif][month]
                    asap[motif][month] += duree
                    asap[motif]['tot_annee'] += duree
                    asap['tot_mois'][month] += duree
                    asap['tot_mois']['tot_annee'] += duree
        counters['Total ASAP'] = asap
        return counters

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'unite': self.unite,
            'regions': self.produce(context['months']),
            'titre': 'Heures par région,'
        })
        return context


class StatFrequentation(StatBaseView):
    template_name = 'statistiques/ser_stats_frequentation.html'
    unite = 'asap'

    def _init_counters(self, months):
        counters = {}
        motifs = [(key, val) for key, val in Tshm.TYP_CHOICES] + [('tot_mois', 'Total mensuel')]
        for key, val in motifs:
            counters[key] = {month: 0 for month in months}
            counters[key]['titre'] = val
            counters[key]['tot_annee'] = 0
        return counters

    def produce(self, months):
        debut, fin = self.get_debut_fin(months)
        counters = {}
        sorties = Tshm.objects.filter(
            prestation__date__gte=debut, prestation__date__lte=fin
        ).filter(
            Q(lieu__version=2)|Q(lieu__isnull=True)
        ).order_by('prestation__date').select_related('prestation', 'lieu')
        # version 2: Simplification des lieux depuis mars 2024

        for sortie in sorties:
            place = sortie.lieu.nom if sortie.lieu else 'Lieu non saisi'
            freq = sortie.homme + sortie.femme + sortie.non_binaire
            if place not in counters:
                counters[place] = self._init_counters(months)
            month = (sortie.prestation.date.year, sortie.prestation.date.month)
            counters[place][sortie.typ][month] += freq
            counters[place][sortie.typ]['tot_annee'] += freq
            counters[place]['tot_mois'][month] += freq
            counters[place]['tot_mois']['tot_annee'] += freq

        asap = self._init_counters(months)
        for place, values in counters.items():
            for month in months:
                for motif in [key for key, value in Tshm.TYP_CHOICES]:
                    freq = counters[place][motif][month]
                    asap[motif][month] += freq
                    asap[motif]['tot_annee'] += freq
                    asap['tot_mois'][month] += freq
                    asap['tot_mois']['tot_annee'] += freq
        counters['Total ASAP'] = asap
        return counters

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'unite': self.unite,
            'regions': self.produce(context['months']),
            'titre': "Fréquentation par région",
        })
        return context
