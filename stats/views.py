import calendar
from datetime import date, timedelta

from django.apps import apps
from django.core.exceptions import PermissionDenied
from django.db.models import Count, DurationField, ExpressionWrapper, F, Q
from django.http import Http404
from django.views.generic import TemplateView

from batoude.models import Beneficiaire
from carrefour.export import ExportStatistique
from carrefour.forms import DateLimitForm
from carrefour.models import Formation, LibellePrestation, Utilisateur
from carrefour.views import MSG_NO_ACCESS
from common.choices import MOTIF_DEMANDE_CHOICES, MotifsFinSuivi, REGIONS_CHOICES


class StatBaseView(TemplateView):
    def dispatch(self, request, *args, **kwargs):
        """ Access for direction and RE only """
        export = request.GET.get('export', None)
        if export == '1' and request.user.has_perm('carrefour.export_aemo'):
            return super().dispatch(request, *args, **kwargs)
        if not export and (request.user.has_perm('carrefour.export_aemo') or self._check_re_unit(self.unite)):
            return super().dispatch(request, *args, **kwargs)
        raise PermissionDenied(MSG_NO_ACCESS)

    def _check_re_unit(self, unit):
        if unit == 'aemo':
            return (self.request.user.has_perm('aemo.manage_littoral')
                    or self.request.user.has_perm('aemo.manage_montagnes'))
        if unit == 'asap':
            return self.request.user.has_perm('ser.manage_ser')
        return self.request.user.has_perm(f'{unit}.manage_{unit}')

    def get_months(self):
        """Return a list of tuples [(year, month), ...] from date_start to date_end."""
        months = [(self.date_start.year, self.date_start.month)]
        while True:
            if months[-1][1] == 12:
                next_m = (months[-1][0] + 1, 1)
            else:
                next_m = (months[-1][0], months[-1][1] + 1)
            if next_m > (self.date_end.year, self.date_end.month):
                break
            months.append(next_m)
        return months

    def month_limits(self, month):
        """From (2020, 4), return (date(2020, 4, 1), date(2020, 5, 1))."""
        return (
            date(month[0], month[1], 1),
            date(month[0] + 1, 1, 1) if month[1] == 12 else date(month[0], month[1] + 1, 1)
        )

    def init_counters(self, names, months):
        """
        Create stat counters:
            {<counter_name>: {(2020, 3): 0, (2020, 4): 0, …, 'total': 0}, …}
        """
        counters = {}
        for count_name in names:
            counters[count_name] = {}
            for month in months:
                counters[count_name][month] = 0
            counters[count_name]['total'] = 0
            counters[count_name]['pourcent'] = 0.0
        return counters

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        get_params = self.request.GET.copy()
        self.export_flag = get_params.pop('export', None)
        date_form = DateLimitForm(get_params)
        context['date_form'] = date_form
        if not date_form.is_valid():
            return context
        self.date_start = date_form.start
        self.date_end = date_form.end
        months = self.get_months()
        context.update({
            'date_form': date_form,
            'months': months,
            'can_export': self.request.user.has_perm('carrefour.export_aemo')
        })
        return context

    def get_debut_fin(self, months):
        """
        From {(2019, 3), (2019, 4), ..., (2020,5)} return (date(2019, 3, 1), date(2020, 5, 31))
        """
        debut = date(months[0][0], months[0][1], 1)
        fin = date(months[-1][0], months[-1][1],
                   calendar.monthrange(months[-1][0], months[-1][1])[1])
        return debut, fin

    def render_to_response(self, context, **response_kwargs):
        if self.export_flag:
            export = ExportStatistique()
            getattr(export, self.export_method)(self.sheet_title, context)
            return export.get_http_response(f"{context['unite']}_{self.filename}")
        else:
            return super().render_to_response(context, **response_kwargs)


class StatHeureParIntervenantBaseView(StatBaseView):
    export_method = 'produce_heure_intervenant'
    filename = 'heures_par_intervenant'
    sheet_title = 'Heures par intervenant-e'

    def dispatch(self, *args, **kwargs):
        if self.unite not in ('aemo', 'asaef', 'batoude', 'asap'):
            raise Http404
        return super().dispatch(*args, **kwargs)

    def get_users(self, debut, fin):
        return Utilisateur.objects.filter(
            Q(date_desactivation__isnull=True) | Q(date_desactivation__gte=debut),
            groups__name__contains=self.unite
        )

    def _init_counter(self, months):
        counter = dict()
        counter['h_prestees'] = {}
        counter['h_prestees'].update({month: timedelta() for month in months})
        counter['h_prestees']['total'] = timedelta()
        counter['h_prestees']['pourcent'] = 0
        counter['h_dues'] = {}
        counter['h_dues'].update({month: timedelta() for month in months})
        counter['h_dues']['total'] = timedelta()
        counter['ecart'] = {}
        counter['ecart'].update({month: timedelta() for month in months})
        counter['ecart']['total'] = timedelta()
        return counter

    def produce(self, months):
        debut, fin = self.get_debut_fin(months)
        counters = {}
        for interv in self.get_users(debut, fin):
            interv_key = f"{interv.nom} {interv.prenom}"
            if self.unite == 'asap':
                prestations = interv.prestations_asap.filter(
                    date__gte=debut, date__lte=fin
                    ).annotate(
                        date_prestation=F('date'),
                        duree=F('duree_prestation')
                    ).values('duree', 'date_prestation')
            else:
                prestations = interv.prestations(self.unite).filter(
                    date_prestation__gte=debut, date_prestation__lte=fin
                ).values(
                    'duree', 'date_prestation'
                )
            if interv_key not in counters:
                counters[interv_key] = self._init_counter(months)

            for prestation in prestations:
                month = (prestation['date_prestation'].year, prestation['date_prestation'].month)
                counters[interv_key]['h_prestees'][month] += prestation['duree']
                counters[interv_key]['h_prestees']['total'] += prestation['duree']

            for month in months:
                counters[interv_key]['h_dues'][month] = timedelta(
                    hours=interv.heures_act_mens())
                counters[interv_key]['h_dues']['total'] += counters[interv_key]['h_dues'][
                    month]
                counters[interv_key]['ecart'][month] = (
                        counters[interv_key]['h_prestees'][month]
                        - counters[interv_key]['h_dues'][month]
                )
                counters[interv_key]['ecart']['total'] += counters[interv_key]['ecart'][
                    month]

        for interv, stats in counters.items():
            if counters[interv]['h_dues']['total'] > timedelta():
                counters[interv]['h_prestees']['pourcent'] = (
                    round(counters[interv]['h_prestees']['total'] /
                          counters[interv]['h_dues']['total'] * 100, 1)
                )
        return counters

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['unite'] = self.unite
        context['titre'] = "Heures prestées et dues par intervenant-e"
        context['intervenants_data'] = [{
            'titre': self.unite.upper(),
            'intervenants': self.produce(context['months']),
        }]
        return context


class StatPrestationBaseView(StatBaseView):
    export_method = 'produce_prestation'
    filename = 'heures_par_prestation'
    sheet_title = 'Heures par prestation'

    def init_counters(self, names, months):
        """
        Create stat counters:
            {<counter_name>: {(2020, 3): timedelta(), (2020, 4): timedelta(), …, 'total': 0}, …}
        """
        counters = {}
        for count_name in names:
            counters[count_name] = {}
            for month in months:
                counters[count_name][month] = timedelta()
            counters[count_name]['total'] = timedelta()
            counters[count_name]['pourcent'] = timedelta()
        return counters

    def produce(self, months, unite):
        debut, fin = self.get_debut_fin(months)
        if self.unite not in ['aemo', 'asaef', 'batoude']:
            raise Http404
        counters = self.init_counters([k for k, v in self.prest_lib_choices], months)
        prest_lib_list = [c for c in counters]
        model = apps.get_model(self.unite, f"Prestation{self.unite.capitalize()}")
        prestations = model.objects.filter(
            lib_prestation__in=prest_lib_list,
            date_prestation__gte=debut,
            date_prestation__lte=fin
        )
        if self.unite in ['aemo', 'batoude']:
            prestations = prestations.annotate(
                num_util=Count('intervenants'),
                duree_tot=ExpressionWrapper(F('duree') * F('num_util'), output_field=DurationField())
            ).values(
                'date_prestation', 'duree_tot', 'lib_prestation_id'
            )
        else:
            prestations = prestations.annotate(
                duree_tot=F('duree')
            ).values(
                'date_prestation', 'duree_tot', 'lib_prestation_id'
            )
        tot_gen = timedelta()
        for prest in prestations:
            month = (prest['date_prestation'].year, prest['date_prestation'].month)
            counters[str(prest['lib_prestation_id'])][month] += prest['duree_tot']
            counters[str(prest['lib_prestation_id'])]['total'] += prest['duree_tot']
            tot_gen += prest['duree_tot']

        for k, v in self.prest_lib_choices:
            if tot_gen > timedelta():
                counters[k]['pourcent'] = round(counters[k]['total'] / tot_gen * 100, 1)
        return counters

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        prest_lib = LibellePrestation.objects.filter(unite=self.unite)
        self.prest_lib_choices = [(str(prest.pk), prest.nom) for prest in prest_lib]
        prest_lib_dict = dict(self.prest_lib_choices)
        if 'months' in context:
            stats = self.produce(context['months'], self.unite)
            context.update({
                'unite': self.unite,
                'titre': "Nbre d'heures par prestation",
                'motifs_data': {prest_lib_dict[key]: stats for key, stats in stats.items()},
            })
        return context


class StatSuiviBaseView(StatBaseView):
    template_name = 'statistiques/stats_suivi.html'
    export_method = 'produce_suivi_aemo_asaef'
    filename = 'suivis'
    sheet_title = 'Suivis'

    def _produce(self, familles, category, months, equipe=None):
        attente = timedelta()
        if equipe:
            familles = familles.filter(suiviaemo__equipe=equipe)
        for famille in familles:
            if famille.suivi.date_debut_suivi and famille.suivi.date_demande:
                attente += famille.suivi.date_debut_suivi - famille.suivi.date_demande
            for month in months:
                month_start, month_end = self.month_limits(month)
                if (famille.date_debut < month_end) and (famille.date_fin >= month_start):
                    self.counters[category][month] += 1
            self.counters[category]['total'] += 1
        if familles.count():
            self.attente = attente.days // familles.count()
        else:
            self.attente = attente

    def suivi_stats(self, model, months, equipe=None):
        debut, fin = self.get_debut_fin(months)
        self.counters = self.init_counters(
            ['nouvelles_demandes', 'suivis_en_cours', 'suivis_termines'], months
        )
        self.counters['nouvelles_demandes']['titre'] = 'Demande déposée'
        self.counters['suivis_en_cours']['titre'] = 'Suivis en cours'
        self.counters['suivis_termines']['titre'] = 'Suivis terminés'

        # Nouvelles demandes
        familles = model.suivis_nouveaux(debut, fin)
        self._produce(familles, 'nouvelles_demandes', months, equipe=equipe)

        # Suivis en cours
        familles = model.suivis_en_cours(debut, fin)
        self._produce(familles, 'suivis_en_cours', months, equipe=equipe)

        # Suivis terminés
        familles = model.suivis_termines(debut, fin)
        self._produce(familles, 'suivis_termines', months, equipe=equipe)

        return self.counters


class StatMotifSuiviBaseView(StatBaseView):
    template_name = 'statistiques/stats_motif_suivi.html'
    motifs_choices = MOTIF_DEMANDE_CHOICES
    export_method = 'produce_motif_suivi'
    filename = 'motifs_suivi'
    sheet_title = 'Motifs de suivi'

    def motifs_stats(self, model, months, equipe=None):
        debut, fin = self.get_debut_fin(months)
        counters = self.init_counters([k for k, v in self.motifs_choices], months)
        familles = model.suivis_nouveaux(debut, fin).values(
            'date_debut', f'suivi{self.unite}__motif_demande'
        )
        if equipe:
            familles = familles.filter(suiviaemo__equipe=equipe)
        tot_gen = 0
        tot_empty = 0

        for famille in familles:
            month = (famille['date_debut'].year, famille['date_debut'].month)
            if famille[f'suivi{self.unite}__motif_demande']:
                for motif in famille[f'suivi{self.unite}__motif_demande']:
                    counters[motif][month] += 1
                    counters[motif]['total'] += 1
                    tot_gen += 1
            else:
                tot_gen += 1
                tot_empty += 1

        for key, choices in self.motifs_choices:
            if tot_gen > 0:
                counters[key]['pourcent'] = counters[key]['total'] / tot_gen * 100

        return counters, round(tot_empty / tot_gen * 100) if tot_gen > 0 else 0


class StatMotifDepartBaseView(StatBaseView):
    template_name = 'statistiques/stats_motif_depart.html'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.motifs_dict = MotifsFinSuivi.choices_stats(self.unite)

    def motif_in_month(self, suivi, month):
        month_start, month_end = self.month_limits(month)
        return suivi.date_debut <= month_end and suivi.date_fin >= month_start

    def motifs_stats(self, model, months, equipe=None):
        debut, fin = self.get_debut_fin(months)
        counters = self.init_counters([k for k, v in self.motifs_dict], months)
        tot_gen = 0
        tot_empty = 0

        if self.unite == 'batoude':
            for benef in model.suivis_termines(debut, fin):
                month = (benef.date_sortie.year, benef.date_sortie.month)
                if benef.motif_fin_suivi:
                    counters[benef.motif_fin_suivi][month] += 1
                    counters[benef.motif_fin_suivi]['total'] += 1
                    tot_gen += 1
                else:
                    tot_empty += 1
        else:
            familles = model.suivis_termines(debut, fin).select_related(f'suivi{self.unite}')
            if equipe:
                familles = familles.filter(suiviaemo__equipe=equipe)
            for famille in familles:
                month = (famille.suivi.date_fin_suivi.year, famille.suivi.date_fin_suivi.month)
                if famille.suivi.motif_fin_suivi:
                    counters[famille.suivi.motif_fin_suivi][month] += 1
                    counters[famille.suivi.motif_fin_suivi]['total'] += 1
                    tot_gen += 1
                else:
                    tot_empty += 1

        for key, choices in self.motifs_dict:
            if tot_gen > 0:
                counters[key]['pourcent'] = counters[key]['total'] / tot_gen * 100 or 0

        return counters, round(tot_empty / (tot_gen + tot_empty) * 100) if tot_gen > 0 else 0

    def render_to_response(self, context, **response_kwargs):
        if self.export_flag:
            export = ExportStatistique()
            export.produce_depart("Motif_départ", context)
            return export.get_http_response(f"{context['unite']}_motifs_departs")
        else:
            return super().render_to_response(context, **response_kwargs)


class StatAnnonceurBaseView(StatBaseView):
    template_name = 'statistiques/stats_annonceur.html'
    export_method = 'produce_annonceur'
    filename = 'services_orient'
    sheet_title = 'Services'

    def annonce_stats(self, model, months, equipe=None):
        debut, fin = self.get_debut_fin(months)
        counters = self.init_counters([k for k, v in self.orienteurs], months)

        familles = model.suivis_nouveaux(debut, fin).values(
            'date_debut', 'date_fin', f'suivi{self.unite}__service_orienteur'
        )
        if equipe:
            familles = familles.filter(suiviaemo__equipe=equipe)
        tot_gen = 0
        tot_empty = 0
        for famille in familles:
            if famille[f'suivi{self.unite}__service_orienteur']:
                month = (famille['date_debut'].year, famille['date_debut'].month)
                counters[famille[f'suivi{self.unite}__service_orienteur']][month] += 1
                counters[famille[f'suivi{self.unite}__service_orienteur']]['total'] += 1
                tot_gen += 1
            else:
                tot_empty += 1

        for key, choices in self.orienteurs:
            if tot_gen > 0:
                counters[key]['pourcent'] = round(counters[key]['total'] / tot_gen * 100, 1)

        return counters, round(tot_empty / (tot_gen + tot_empty) * 100) if tot_empty > 0 else 0


class StatRegionBaseView(StatBaseView):
    template_name = 'statistiques/stats_region.html'
    regions_choices = REGIONS_CHOICES
    export_method = 'produce_region'
    filename = 'suivis_par_region'
    sheet_title = 'Régions'

    def motif_in_month(self, suivi, month):
        month_start, month_end = self.month_limits(month)
        return suivi.date_debut <= month_end and suivi.date_fin >= month_start

    def regions_stats(self, model, months, equipe=None):
        debut, fin = self.get_debut_fin(months)
        counters = self.init_counters([k for k, v in self.regions_choices], months)
        tot_gen = 0
        tot_empty = 0
        if self.unite == 'batoude':
            for benef in Beneficiaire.suivis_nouveaux(debut, fin):
                month = (benef.date_demande.year, benef.date_demande.month)
                if benef.region:
                    counters[benef.region][month] += 1
                    counters[benef.region]['total'] += 1
                    tot_gen += 1
                else:
                    tot_empty += 1
        else:
            familles = model.suivis_nouveaux(debut, fin).select_related(f'suivi{self.unite}').values(
                f'suivi{self.unite}__date_demande', 'region'
            )
            for famille in familles:
                if famille[f'suivi{self.unite}__date_demande']:
                    month = (famille[f'suivi{self.unite}__date_demande'].year,
                             famille[f'suivi{self.unite}__date_demande'].month)
                    if famille['region']:
                        counters[famille['region']][month] += 1
                        counters[famille['region']]['total'] += 1
                        tot_gen += 1
                    else:
                        tot_empty += 1

        for key, choices in self.regions_choices:
            counters[key]['pourcent'] = round(counters[key]['total'] / tot_gen * 100, 1) if tot_gen > 0 else 0

        return counters, round(tot_empty / (tot_gen + tot_empty) * 100) if tot_gen > 0 else 0


class StatAgeBaseView(StatBaseView):
    template_name = 'statistiques/stats_age.html'
    visible_ages = ['pre_scol', 'cycle1', 'cycle2', 'cycle3', 'autres']
    formation_dict = Formation.FORMATION_CHOICES + (('autres', 'Hors scol. oblig.'),)
    export_method = 'produce_age'
    filename = 'suivis_par_cycle'
    sheet_title = 'Suivis par cycle'

    def age_stats(self, model, months, equipe=None):
        debut, fin = self.get_debut_fin(months)
        counters = self.init_counters([key for key, choices in self.formation_dict], months)
        familles = model.suivis_nouveaux(debut, fin).prefetch_related('membres')
        if equipe:
            familles = familles.filter(suiviaemo__equipe=equipe)
        tot_gen = 0
        tot_empty = 0
        for famille in familles:
            for enfant in famille.membres_suivis():
                if enfant.formation.statut:
                    month = (famille.date_debut.year, famille.date_debut.month)
                    if enfant.formation.statut not in self.visible_ages:
                        enfant.formation.statut = 'autres'
                    counters[enfant.formation.statut][month] += 1
                    counters[enfant.formation.statut]['total'] += 1
                    tot_gen += 1
                else:
                    tot_empty += 1

        for key, stats in self.formation_dict:
            if tot_gen > 0:
                counters[key]['pourcent'] = round(counters[key]['total'] / tot_gen * 100, 1)
        return counters, round(tot_empty / (tot_gen + tot_empty) * 100) if tot_empty else 0
