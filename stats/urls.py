from django.urls import path
from stats import views_aemo, views_asaef, views_batoude, views_asap

urlpatterns = [
    # Statistiques AEMO
    path('aemo/suivi/', views_aemo.StatSuiviView.as_view(), name='stat-aemo-suivi'),
    path('aemo/motif/suivi/', views_aemo.StatMotifSuiviView.as_view(), name='stat-aemo-motif-suivi'),
    path('aemo/motif/depart/', views_aemo.StatMotifDepartView.as_view(), name='stat-aemo-motif-depart'),
    path('aemo/annonceurs/', views_aemo.StatAnnonceurView.as_view(), name='stat-aemo-annonceur'),
    path('aemo/localite/', views_aemo.StatLocaliteView.as_view(), name='stat-aemo-localite'),
    path('aemo/age/', views_aemo.StatAgeView.as_view(), name='stat-aemo-age'),
    path('aemo/suivi_intervenant/', views_aemo.StatSuiviParIntervenantView.as_view(),
         name='stat-aemo-suiviintervenant'),
    path('aemo/region/', views_aemo.StatRegionView.as_view(), name='stat-aemo-region'),
    path('aemo/heure_intervenant/', views_aemo.StatHeureParIntervenantView.as_view(),
         name='stat-aemo-heureintervenant'),
    path('aemo/prestation/', views_aemo.StatPrestationView.as_view(), name='stat-aemo-prestation'),

    # Statistiques ASAP
    path('asap/mensuelle/', views_asap.StatMensuelles.as_view(), name='stat-asap-mensuelle'),
    path('asap/sortie/', views_asap.StatSortieView.as_view(), name='stat-asap-sortie'),
    path('asap/suivi/<str:mode>/', views_asap.StatSuiviView.as_view(), name='stat-asap-suivi'),
    path('asap/heure_intervenant/', views_asap.StatHeureParIntervenantView.as_view(),
         name='stat-asap-heureintervenant'),
    path('asap/region/', views_asap.StatRegion.as_view(), name='stat-asap-region'),
    path('asap/frequentation/', views_asap.StatFrequentation.as_view(), name='stat-asap-frequentation'),

    # Statistiques BATOUDE
    path('batoude/prestation/', views_batoude.StatPrestationView.as_view(), name='stat-batoude-prestation'),
    path('batoude/heure_intervenant/', views_batoude.StatHeureParIntervenantView.as_view(),
         name='stat-batoude-heureintervenant'),
    path('batoude/suivis/', views_batoude.StatSuiviView.as_view(), name='stat-batoude-suivi'),
    path('batoude/motif/depart/', views_batoude.StatMotifDepartView.as_view(), name='stat-batoude-motif-depart'),
    path('batoude/age/', views_batoude.StatBatoudeAgeView.as_view(), name='stat-batoude-age'),

    # Statistiques Asaef
    path('asaef/prestation/', views_asaef.StatPrestationView.as_view(), name='stat-asaef-prestation'),
    path('asaef/heure_intervenant/', views_asaef.StatHeureParIntervenantView.as_view(),
         name='stat-asaef-heureintervenant'),
    path('asaef/suivis/', views_asaef.StatSuiviView.as_view(), name='stat-asaef-suivi'),
    path('asaef/motif/suivi/', views_asaef.StatMotifSuiviView.as_view(), name='stat-asaef-motif-suivi'),
    path('asaef/annonceurs/', views_asaef.StatAnnonceurView.as_view(), name='stat-asaef-annonceur'),
    path('asaef/region/', views_asaef.StatRegionView.as_view(), name='stat-asaef-region'),
    path('asaef/age/', views_asaef.StatAgeView.as_view(), name='stat-asaef-age'),
    path('asaef/motif/depart/', views_asaef.StatMotifDepartView.as_view(), name='stat-asaef-motif-depart'),
]
