import math

from django.db.models import F, Avg, Count, DurationField, ExpressionWrapper
from django.db.models.functions import TruncYear
from batoude.models import Beneficiaire

from .views import (
    StatBaseView, StatSuiviBaseView, StatPrestationBaseView, StatHeureParIntervenantBaseView, StatMotifDepartBaseView
)


class StatPrestationView(StatPrestationBaseView):
    template_name = "statistiques/stats_prestation.html"
    unite = 'batoude'


class StatHeureParIntervenantView(StatHeureParIntervenantBaseView):
    template_name = "statistiques/stats_heure_intervenant.html"
    unite = 'batoude'


class StatSuiviView(StatSuiviBaseView):
    unite = 'batoude'
    export_method = 'produce_suivi_aemo_asaef'

    def suivi_stats(self, model, months, equipe=None):
        debut, fin = self.get_debut_fin(months)
        self.counters = self.init_counters(
            ['nouvelles_demandes', 'suivis_nouveaux', 'suivis_en_cours', 'suivis_termines'], months
        )
        self.counters['nouvelles_demandes']['titre'] = 'Nouvelles demandes'
        self.counters['suivis_nouveaux']['titre'] = 'Nouveaux suivis'
        self.counters['suivis_en_cours']['titre'] = 'Suivis en cours'
        self.counters['suivis_termines']['titre'] = 'Suivis terminés'

        # Nouvelles demandes
        beneficiaires = model.nouvelles_demandes(debut, fin)
        self._produce(beneficiaires, 'nouvelles_demandes', months, equipe=equipe)

        # Nouveaux suivis
        beneficiaires = model.suivis_nouveaux(debut, fin)
        self._produce(beneficiaires, 'suivis_nouveaux', months, equipe=equipe)

        # Suivis en cours
        beneficiaires = model.suivis_en_cours(debut, fin)
        self._produce(beneficiaires, 'suivis_en_cours', months, equipe=equipe)

        # Suivis terminés
        beneficiaires = model.suivis_termines(debut, fin)
        self._produce(beneficiaires, 'suivis_termines', months, equipe=equipe)

        return self.counters

    def _produce(self, beneficiaires, category, months, equipe=None):
        delai = list()
        for beneficiaire in beneficiaires:
            if beneficiaire.date_demande and beneficiaire.date_admission:
                delai.append((beneficiaire.date_admission - beneficiaire.date_demande).days)
            for month in months:
                month_start, month_end = self.month_limits(month)
                if (beneficiaire.date_debut < month_end) and (beneficiaire.date_fin >= month_start):
                    self.counters[category][month] += 1
            self.counters[category]['total'] += 1
        if category == 'suivis_en_cours':
            self.attente = sum(delai) // len(delai) if delai else 0

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'months' in context:
            context['unite'] = 'batoude'
            context['titre'] = "Résumés des suivis BATOUDE"
            context['entites'] = [
                {
                    'titre': 'BATOUDE',
                    'data': self.suivi_stats(Beneficiaire, context['months']),
                    'attente': self.attente
                }
            ]
        return context


class StatMotifDepartView(StatMotifDepartBaseView):
    unite = 'batoude'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'months' in context:
            motif_fin_dict = dict(self.motifs_dict)
            stats, empty = self.motifs_stats(Beneficiaire, context['months'])
            context['unite'] = self.unite
            context['titre'] = "Motif des fins de suivis Batoude"
            context['motifs_data'] = [{
                'titre': 'Batoude',
                'depart': {motif_fin_dict[key]: stats for key, stats in stats.items()
                           if key != 'erreur'},
            }]
        return context


class StatBatoudeAgeView(StatBaseView):
    template_name = 'statistiques/batoude_stats_age.html'

    unite = 'batoude'

    def _format_duree(self, delta):
        annee = delta.days / 365.2425
        mois, an = math.modf(annee)
        return f'{int(annee)} ans {int(mois * 12)} mois'

    def produce(self):
        stat = {}
        beneficiaires = Beneficiaire.objects.filter(
            naissance__isnull=False, date_demande__isnull=False, date_admission__isnull=False
        ).annotate(
            diff_demande=ExpressionWrapper(F('date_demande') - F('naissance'), output_field=DurationField()),
            diff_admission=ExpressionWrapper(F('date_admission') - F('naissance'), output_field=DurationField()),
        ).values(
            year=TruncYear('date_demande')
        ).annotate(
            moy_demande=Avg("diff_demande"),
            moy_admission=Avg("diff_admission"),
            nbre=Count('id')
        ).order_by("-year")

        for benef in beneficiaires:
            stat[benef['year'].year] = {
                'nbre': benef['nbre'],
                'moy_dem': self._format_duree(benef['moy_demande']),
                'moy_adm': self._format_duree(benef['moy_admission'])
            }
        return stat

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'unite': self.unite,
            'stat': self.produce(),
            'titre': "Statistique des âges à l'admission",
        })
        return context
