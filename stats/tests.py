from datetime import date, timedelta
from itertools import chain
from freezegun import freeze_time
from django.contrib.auth.models import Group, Permission
from django.test import TestCase
from django.urls import reverse

from aemo.models import FamilleAemo, PrestationAemo
from asaef.models import FamilleAsaef
from batoude.models import Beneficiaire
from carrefour.export import openxml_contenttype
from carrefour.forms import DateLimitForm
from carrefour.models import Contact, Personne, Role, Service, Utilisateur, LibellePrestation
from common.choices import MotifsFinSuivi
from ser.models import Place, Prestation, Tshm


class StatTestBase(TestCase):
    aemo_urls = [
        reverse(name) for name in [
            'stat-aemo-suivi', 'stat-aemo-motif-suivi', 'stat-aemo-motif-depart', 'stat-aemo-annonceur',
            'stat-aemo-localite', 'stat-aemo-age', 'stat-aemo-suiviintervenant', 'stat-aemo-region',
        ]
    ]
    asaef_urls = [
        reverse(name) for name in [
            'stat-asaef-heureintervenant', 'stat-asaef-prestation', 'stat-asaef-suivi', 'stat-asaef-motif-suivi',
            'stat-asaef-motif-depart'
        ]
    ]
    batoude_urls = [
        reverse(name) for name in ['stat-batoude-heureintervenant', 'stat-batoude-prestation', 'stat-batoude-suivi']
    ]
    asap_urls = [
        reverse('stat-asap-heureintervenant'),
        reverse('stat-asap-suivi', args=['relationnel']), reverse('stat-asap-suivi', args=['educatif']),
    ]

    @classmethod
    def setUpTestData(cls):
        Utilisateur.objects.create_user(
            'user_aemo', 'user_aemo@example.org', nom='Aemo', prenom='Prénom', sigle='AE'
        )
        Utilisateur.objects.create_user(
            'user_asaef', 'user_asaef@example.org', nom='Asaef', prenom='Prénom', sigle='AS'
        )
        Utilisateur.objects.create_user(
            'user_batoude', 'user_batoude@example.org', nom='Batoude', prenom='Prénom', sigle='BP'
        )
        Utilisateur.objects.create_user(
            'user_asap', 'user_asap@example.org', nom='Asap', prenom='Prénom', sigle='SP', taux_activite=70
        )
        Utilisateur.objects.create_user(
            'manager_asap', 'asap@example.org', prenom='Jean', nom='Valjean', sigle='B1',
            taux_activite=80
        )

        cls.user_admin = Utilisateur.objects.create_user(
            'user_admin', 'admin@example.org', first_name='Jean', last_name='Admin',
        )
        cls.user_admin.user_permissions.add(Permission.objects.get(codename='export_aemo'))

        Group.objects.bulk_create([
            Group(name='aemo_littoral'),
            Group(name='aemo_montagnes'),
            Group(name='direction'),
            Group(name='asaef'),
            Group(name='batoude'),
            Group(name='asap'),
            Group(name='admin')
        ], ignore_conflicts=True)

        Role.objects.bulk_create([
            Role(nom='Père', famille=True),
            Role(nom='Mère', famille=True),
            Role(nom='Beau-père', famille=True),
            Role(nom='Enfant suivi', famille=True),
            Role(nom='Enfant non-suivi', famille=True),
            Role(nom='Médecin', famille=False),
        ])

        LibellePrestation.objects.bulk_create([
            LibellePrestation(unite='aemo', code='p01', nom='Prestation 1'),
            LibellePrestation(unite='aemo', code='p02', nom='Prestation 2'),
        ], ignore_conflicts=True)

        FamilleAemo.objects.create_famille(
            nom='Fam_Aemo', rue='Château1', npa=2000, localite='Moulinsart',
            equipe='montagnes', autorite_parentale='conjointe', statut_marital='divorce', monoparentale=False
        )

        FamilleAemo.objects.create_famille(
            nom='Fam_Aemo2', rue='Château1', npa=2000, localite='Moulinsart',
            equipe='montagnes', autorite_parentale='conjointe', statut_marital='divorce', monoparentale=False
        )

        FamilleAsaef.objects.create_famille(
            nom='Fam_Asaef', rue='Château1', npa=2000, localite='Moulinsart',
            autorite_parentale='conjointe', statut_marital='divorce', monoparentale=False
        )

        FamilleAsaef.objects.create_famille(
            nom='Fam_Asaef2', rue='Château1', npa=2000, localite='Moulinsart',
            autorite_parentale='conjointe', statut_marital='divorce', monoparentale=False
        )


class CarrefourTests(StatTestBase):

    def test_export_xls(self):

        def export_region_xls(url, filename):
            self.client.force_login(self.user_admin)
            param = "?start_month=1&start_year=2020&end_month=12&end_year=2020"
            export = "&export=1"
            response = self.client.get(url + param + export)
            self.assertEqual(
                response['content-disposition'],
                'attachment; filename="{}"'.format(filename)
            )
            self.assertEqual(response['content-type'], openxml_contenttype)
            self.assertGreater(len(response.getvalue()), 200)

        data = {
            reverse('stat-aemo-age'): 'aemo_suivis_par_cycle.xlsx',
            reverse('stat-aemo-annonceur'): 'aemo_services_orient.xlsx',
            reverse('stat-aemo-suivi'): 'aemo_suivis.xlsx',
            reverse('stat-aemo-motif-suivi'): 'aemo_motifs_suivi.xlsx',
            reverse('stat-aemo-motif-depart'): 'aemo_motifs_departs.xlsx',
            reverse('stat-aemo-region'): 'aemo_suivis_par_region.xlsx',
            reverse('stat-aemo-prestation'): 'aemo_heures_par_prestation.xlsx',

            reverse('stat-asaef-age'): 'asaef_suivis_par_cycle.xlsx',
            reverse('stat-asaef-annonceur'): 'asaef_services_orient.xlsx',
            reverse('stat-asaef-heureintervenant'): 'asaef_heures_par_intervenant.xlsx',
            reverse('stat-asaef-motif-depart'): 'asaef_motifs_departs.xlsx',
            reverse('stat-asaef-motif-suivi'): 'asaef_motifs_suivi.xlsx',
            reverse('stat-asaef-region'): 'asaef_suivis_par_region.xlsx',
            reverse('stat-asaef-suivi'): 'asaef_suivis.xlsx',
            reverse('stat-asaef-prestation'): 'asaef_heures_par_prestation.xlsx',

            reverse('stat-batoude-heureintervenant'): 'batoude_heures_par_intervenant.xlsx',
            reverse('stat-batoude-prestation'): 'batoude_heures_par_prestation.xlsx',

            reverse('stat-asap-heureintervenant'): 'asap_heures_par_intervenant.xlsx',
            reverse('stat-asap-sortie'): 'ASAP_sorties_asap.xlsx',
            reverse('stat-asap-suivi', args=['relationnel']): 'asap_suivis_relationnels.xlsx',
            reverse('stat-asap-suivi', args=['educatif']): 'asap_accomp_educatifs.xlsx',
        }
        for url, filename in data.items():
            export_region_xls(url, filename)

    def test_export_xlsx_deny_for_re(self):

        for unite in ['aemo', 'asaef', 'batoude', 'asap']:
            manager = Utilisateur.objects.get(username=f'user_{unite}')
            manager.user_permissions.add(Permission.objects.get(
                codename={
                    'aemo': 'manage_montagnes', 'asap': 'manage_ser'
                }.get(unite, f'manage_{unite}')
            ))
            self.client.force_login(manager)
            urls = getattr(self, f'{unite}_urls')
            for url in urls:
                param = "?start_month=1&start_year=2020&end_month=12&end_year=2020"
                export = "&export=1"
                response = self.client.get(url + param + export)
                self.assertEqual(response.status_code, 403)

    def test_date_par_defaut(self):
        form = DateLimitForm(data=None)
        self.assertInHTML('<option value="1" selected>janvier</option>', str(form))


class AemoTests(StatTestBase):

    def setUp(self):
        self.user_aemo = Utilisateur.objects.get(username='user_aemo')
        self.user_aemo.user_permissions.add(Permission.objects.get(codename='view_montagnes'))
        self.user_aemo.user_permissions.add(Permission.objects.get(codename='change_montagnes'))

    def test_acces_aemo(self):
        # Forbidden for aemo educ
        self.client.force_login(self.user_aemo)
        for url in self.aemo_urls:
            self.assertEqual(self.client.get(url).status_code, 403)

        # Permissions for RE
        self.user_aemo.user_permissions.add(Permission.objects.get(codename='manage_montagnes'))
        self.client.force_login(self.user_aemo)
        for url in self.aemo_urls:
            self.assertEqual(self.client.get(url).status_code, 200)
        for url in chain(self.asaef_urls, self.batoude_urls, self.asap_urls):
            self.assertEqual(self.client.get(url).status_code, 403)

    def test_age_aemo(self):
        famille = FamilleAemo.objects.get(nom='Fam_Aemo')
        famille.suiviaemo.date_demande = "2020-05-15"
        famille.suiviaemo.save()

        enf1 = Personne.objects.create_personne(
            famille=famille, prenom='Jean', nom='Haddock',
            role=Role.objects.get(nom='Enfant suivi')
        )
        enf1.formation.statut = 'cycle1'
        enf1.formation.save()

        enf2 = Personne.objects.create_personne(
            famille=famille, prenom='Paul', nom='Haddock',
            role=Role.objects.get(nom='Enfant suivi')
        )
        enf2.formation.statut = ''
        enf2.formation.save()

        self.client.force_login(self.user_admin)
        param = "?start_month=1&start_year=2020&end_month=12&end_year=2020"
        response = self.client.get(reverse('stat-aemo-age') + param)
        response_key = response.context['motifs_data']
        self.assertEqual(response_key[0]['titre'], 'AEMO - Global')
        self.assertEqual(response_key[0]['age']['Cycle 1'][(2020, 5)], 1)
        self.assertEqual(response_key[0]['empty'], 50)
        self.assertEqual(response_key[1]['titre'], 'AEMO - Montagnes')
        self.assertEqual(response_key[1]['age']['Cycle 1']['total'], 1)
        self.assertEqual(response_key[1]['empty'], 50)
        self.assertEqual(response_key[2]['titre'], 'AEMO - Littoral')
        self.assertEqual(response_key[2]['empty'], 0)

    def test_annonceur_aemo(self):
        famille = FamilleAemo.objects.get(nom='Fam_Aemo')
        famille.suiviaemo.date_demande = "2020-04-15"
        famille.suiviaemo.service_orienteur = "famille"
        famille.suiviaemo.save()

        famille2 = FamilleAemo.objects.create_famille(nom='Valjean', equipe='littoral')
        famille2.suiviaemo.date_demande = "2020-05-15"
        famille2.suiviaemo.service_orienteur = "autre"
        famille2.suiviaemo.save()

        self.client.force_login(self.user_admin)
        param = "?start_month=1&start_year=2020&end_month=12&end_year=2020"
        response = self.client.get(reverse('stat-aemo-annonceur') + param)
        response_key = response.context['motifs_data']
        self.assertEqual(response_key[0]['annonceur']['Famille'][(2020, 4)], 1)
        self.assertEqual(response_key[0]['annonceur']['Famille']['total'], 1)
        self.assertEqual(response_key[1]['annonceur']['Famille'][(2020, 4)], 1)
        self.assertEqual(response_key[2]['annonceur']['Autre'][(2020, 5)], 1)
        self.assertEqual(response_key[2]['annonceur']['Autre']['total'], 1)

    def test_motif_debut_suivi_aemo(self):
        famille = FamilleAemo.objects.get(nom='Fam_Aemo')
        famille.suiviaemo.date_demande = "2020-04-15"
        famille.suiviaemo.motif_demande = ['parentalite', 'developpement']
        famille.suiviaemo.save()

        famille2 = FamilleAemo.objects.create_famille(nom='Valjean', equipe='littoral')
        famille2.suiviaemo.date_demande = "2020-05-15"
        famille2.suiviaemo.motif_demande = ['parentalite', 'integration']
        famille2.suiviaemo.save()

        self.client.force_login(self.user_admin)
        param = "?start_month=1&start_year=2020&end_month=12&end_year=2020"
        response = self.client.get(reverse('stat-aemo-motif-suivi') + param)
        response_key = response.context['motifs_data'][0]['dem']
        self.assertEqual(response_key['Soutien à la parentalité'][(2020, 4)], 1)
        self.assertEqual(response_key['Soutien à la parentalité'][(2020, 5)], 1)
        self.assertEqual(response_key['Soutien à la parentalité']['total'], 2)
        self.assertEqual(response_key['Soutien à la parentalité']['pourcent'], 50.0)

    def test_motif_fin_suivi_aemo(self):
        famille = FamilleAemo.objects.get(nom='Fam_Aemo')
        famille.suiviaemo.date_demande = "2020-04-15"
        famille.suiviaemo.date_fin_suivi = "2020-05-12"
        famille.suiviaemo.motif_fin_suivi = 'desengagement'
        famille.suiviaemo.save()

        famille2 = FamilleAemo.objects.create_famille(nom='Valjean', equipe='littoral')
        famille2.suiviaemo.date_demande = "2020-05-15"
        famille2.suiviaemo.date_fin_suivi = "2020-06-15"
        famille2.suiviaemo.motif_fin_suivi = 'desengagement'
        famille2.suiviaemo.save()

        self.client.force_login(self.user_admin)
        param = "?start_month=1&start_year=2020&end_month=12&end_year=2020"
        response = self.client.get(reverse('stat-aemo-motif-depart') + param)
        response_key = response.context['motifs_data']
        self.assertEqual(response_key[0]['depart']['Désengagement'][(2020, 5)], 1)
        self.assertEqual(response_key[0]['depart']['Désengagement'][(2020, 6)], 1)
        self.assertEqual(response_key[0]['depart']['Désengagement']['total'], 2)
        self.assertEqual(response_key[1]['depart']['Désengagement'][(2020, 5)], 1)
        self.assertEqual(response_key[2]['depart']['Désengagement'][(2020, 6)], 1)

    def test_prestation_aemo(self):
        user_aemo2 = Utilisateur.objects.create_user(
            'user_aemo2', 'user_aemo2@example.org', nom='Aemo2', prenom='Prénom2', sigle='AP2'
        )
        user_aemo2.groups.add(Group.objects.get(name='aemo_montagnes'))
        famille = FamilleAemo.objects.get(nom='Fam_Aemo')
        famille.suiviaemo.date_demande = "2020-04-15"
        famille.suiviaemo.save()
        p1 = PrestationAemo.objects.create(
            famille=famille, date_prestation=date(2021, 1, 30), duree="4:25",
            lib_prestation=LibellePrestation.objects.get(code='p01')
        )
        p1.intervenants.add(self.user_aemo)
        p1.intervenants.add(user_aemo2)
        p2 = PrestationAemo.objects.create(
            famille=famille, date_prestation=date(2021, 2, 15), duree="1:12",
            lib_prestation=LibellePrestation.objects.get(code='p02'),
        )
        p2.intervenants.add(self.user_aemo)

        self.client.force_login(self.user_admin)
        param = "?start_month=1&start_year=2021&end_month=2&end_year=2021"
        response = self.client.get(reverse('stat-aemo-prestation') + param)
        response_key = response.context['motifs_data']
        self.assertEqual(response_key['Prestation 1']['total'], timedelta(hours=8, minutes=50))
        self.assertEqual(response_key['Prestation 2']['total'], timedelta(hours=1, minutes=12))

    def test_heure_par_intervenant_aemo(self):
        prest_fam = LibellePrestation.objects.get(code='p01')
        prest_gen = LibellePrestation.objects.get(code='p02')
        self.user_aemo.groups.add(Group.objects.get(name='aemo_montagnes'))
        self.user_aemo.taux_activite = 80
        self.user_aemo.save()
        famille = FamilleAemo.objects.get(nom='Fam_Aemo')
        famille.suiviaemo.date_demande = "2020-05-15"
        famille.suiviaemo.date_debut_suivi = "2020-06-01"
        famille.suiviaemo.save()
        data = {
            'date_prestation': date(2020, 7, 12), 'duree': timedelta(hours=8), 'famille': famille,
            'lib_prestation': prest_fam
        }
        prest = PrestationAemo.objects.create(**data)
        prest.intervenants.add(self.user_aemo)

        data = {
            'date_prestation': date(2020, 8, 20), 'duree': timedelta(hours=5), 'famille': None,
            'lib_prestation': prest_gen
        }
        prest = PrestationAemo.objects.create(**data)
        prest.intervenants.add(self.user_aemo)

        self.client.force_login(self.user_admin)
        param = "?start_month=1&start_year=2020&end_month=12&end_year=2020"
        response = self.client.get(reverse('stat-aemo-heureintervenant') + param)
        response_key = response.context['intervenants_data'][0]['intervenants']['Aemo Prénom']
        heures_act_ann = self.user_aemo.heures_act_mens() * 12
        self.assertEqual(response_key['h_prestees']['total'], timedelta(hours=13))
        self.assertEqual(response_key['h_prestees']['pourcent'], round(13/heures_act_ann*100, 1))
        self.assertEqual(response_key['h_dues']['total'], timedelta(hours=heures_act_ann))
        self.assertEqual(response_key['ecart']['total'], timedelta(hours=13-heures_act_ann))

    def test_suivi_par_intervenant_aemo(self):
        famille = FamilleAemo.objects.get(nom='Fam_Aemo')
        famille.suiviaemo.date_demande = "2020-05-15"
        famille.suiviaemo.date_debut_suivi = "2020-06-01"
        famille.suiviaemo.save()
        famille.add_referent(self.user_aemo, debut=famille.suiviaemo.date_demande)

        famille2 = FamilleAemo.objects.create_famille(nom='Valjean', equipe='montagnes')
        famille2.suiviaemo.date_demande = "2020-07-15"
        famille2.suiviaemo.save()
        famille2.add_referent(self.user_aemo, debut=famille2.suiviaemo.date_demande)

        self.client.force_login(self.user_admin)
        param = "?start_month=1&start_year=2020&end_month=12&end_year=2020"
        response = self.client.get(reverse('stat-aemo-suiviintervenant') + param)
        response_key = response.context['intervenants_data'][1]['intervenants']['Aemo Prénom']
        self.assertEqual(response_key[(2020, 5)], 1)
        for i in range(7, 13):
            self.assertEqual(response_key[(2020, i)], 2)
        self.assertEqual(response_key['moyenne'], round(14 / 12))

    def test_region_aemo(self):
        famille = FamilleAemo.objects.get(nom='Fam_Aemo')
        famille.region = 'mont_locle'
        famille.save()
        famille.suiviaemo.date_demande = "2020-04-15"
        famille.suiviaemo.save()

        famille2 = FamilleAemo.objects.create_famille(nom='Valjean', equipe='littoral', region='litt_ouest')
        famille2.suiviaemo.date_demande = "2020-05-15"
        famille2.suiviaemo.save()

        self.client.force_login(self.user_admin)
        param = "?start_month=1&start_year=2020&end_month=12&end_year=2020"
        response = self.client.get(reverse('stat-aemo-region') + param)
        response_key = response.context['regions']
        self.assertEqual(response_key['Le Locle'][(2020, 4)], 1)
        self.assertEqual(response_key['Littoral Ouest'][(2020, 5)], 1)

    def test_suivis_aemo(self):
        famille = FamilleAemo.objects.get(nom='Fam_Aemo')
        famille.suiviaemo.date_demande = "2020-04-15"
        famille.suiviaemo.date_debut_suivi = "2020-05-15"
        famille.suiviaemo.date_fin_suivi = "2020-07-15"
        famille.suiviaemo.save()

        famille2 = FamilleAemo.objects.create_famille(nom='Valjean', equipe='littoral')
        famille2.suiviaemo.date_demande = "2020-05-15"
        famille2.suiviaemo.date_debut_suivi = "2020-06-15"
        famille2.suiviaemo.date_fin_suivi = "2020-07-15"
        famille2.suiviaemo.save()

        self.client.force_login(self.user_admin)
        param = "?start_month=1&start_year=2020&end_month=12&end_year=2020"
        response = self.client.get(reverse('stat-aemo-suivi') + param)
        response_key = response.context['entites'][0]['data']
        self.assertEqual(response_key['nouvelles_demandes'][(2020, 4)], 1)
        self.assertEqual(response_key['nouvelles_demandes'][(2020, 5)], 1)
        self.assertEqual(response_key['nouvelles_demandes']['total'], 2)

        self.assertEqual(response_key['suivis_en_cours'][(2020, 5)], 2)
        self.assertEqual(response_key['suivis_en_cours'][(2020, 6)], 2)
        self.assertEqual(response_key['suivis_en_cours'][(2020, 7)], 2)
        self.assertEqual(response_key['suivis_en_cours']['total'], 2)

        self.assertEqual(response_key['suivis_termines'][(2020, 6)], 0)
        self.assertEqual(response_key['suivis_termines'][(2020, 7)], 2)
        self.assertEqual(response_key['suivis_termines']['total'], 2)

        self.assertEqual(response.context['entites'][0]['attente'], 30)

    def test_suivis_export_aemo(self):
        famille = FamilleAemo.objects.get(nom='Fam_Aemo')
        famille.suiviaemo.date_demande = "2020-04-15"
        famille.suiviaemo.date_debut_suivi = "2020-05-15"
        famille.suiviaemo.date_fin_suivi = "2020-07-15"
        famille.suiviaemo.save()

        famille2 = FamilleAemo.objects.create_famille(nom='Fam_Aemo2', equipe='littoral')
        famille2.suiviaemo.date_demande = "2020-05-15"
        famille2.suiviaemo.date_debut_suivi = "2020-06-15"
        famille2.suiviaemo.date_fin_suivi = "2020-07-15"
        famille2.suiviaemo.save()

        self.client.force_login(self.user_admin)
        response = self.client.get(reverse('stat-aemo-suivi') + "?export=1")
        self.assertEqual(response['Content-Type'], openxml_contenttype)

    def test_localite_aemo(self):
        famille = FamilleAemo.objects.get(nom='Fam_Aemo', )
        famille.suiviaemo.date_demande = "2020-04-15"
        famille.suiviaemo.save()

        famille2 = FamilleAemo.objects.create_famille(nom='Valjean', equipe='littoral', npa='', localite='Pékin')
        famille2.suiviaemo.date_demande = "2020-05-15"
        famille2.suiviaemo.save()

        self.client.force_login(self.user_admin)
        param = "?start_month=1&start_year=2020&end_month=12&end_year=2020"
        response = self.client.get(reverse('stat-aemo-localite') + param)
        response_key = response.context['localites']
        self.assertEqual(response_key[' Pékin'][(2020, 5)], 1)
        self.assertEqual(response_key['2000 Moulinsart'][(2020, 4)], 1)

    def test_intervenants_actifs_partiellement_aemo(self):
        user_aemo2 = Utilisateur.objects.create_user(
            'user_aemo2', 'user_aemo2@example.org', nom='Aemo2', prenom='Prénom2', sigle='AP2'
        )
        user_aemo2.groups.add(Group.objects.get(name='aemo_montagnes'))
        user_aemo2.user_permissions.add(Permission.objects.get(codename='view_montagnes'))
        user_aemo2.user_permissions.add(Permission.objects.get(codename='change_montagnes'))

        user_aemo2.is_active = False
        user_aemo2.date_desactivation = date(2020, 10, 10)

        self.client.force_login(self.user_admin)
        param = "?start_month=1&start_year=2020&end_month=12&end_year=2020"
        response = self.client.get(reverse('stat-aemo-heureintervenant') + param)
        self.assertContains(response, '<td>Aemo2 Prénom2</td>', html=True)

    def test_export_pourcent_egal_a_zero_heures_par_intervenant_aemo(self):
        self.client.force_login(self.user_admin)
        param = "?start_month=1&start_year=2021&end_month=4&end_year=2021&export=1"
        response = self.client.get(reverse('stat-aemo-heureintervenant') + param)
        self.assertEqual(response.status_code, 200)

    def test_bouton_export_invisible_pour_re_aemo(self):
        self.user_aemo.user_permissions.add(Permission.objects.get(codename='manage_littoral'))
        self.client.force_login(self.user_aemo)
        self.assertTrue(self.user_aemo.is_responsable_equipe())
        response = self.client.get(reverse('stat-aemo-suivi'))
        self.assertFalse(response.context['can_export'])


class AsapTests(StatTestBase):

    def setUp(self):
        self.user_asap = Utilisateur.objects.get(username='user_asap')
        self.user_asap.user_permissions.add(Permission.objects.get(codename='view_ser'))
        self.user_asap.user_permissions.add(Permission.objects.get(codename='change_ser'))
        self.user_asap.groups.add(Group.objects.get(name='asap'))

        self.manager_asap = Utilisateur.objects.get(username='manager_asap')
        self.manager_asap.groups.add(Group.objects.get(name='asap'))
        self.manager_asap.user_permissions.add(Permission.objects.get(codename='manage_ser'))

    def test_acces_asap(self):
        # Forbidden for asap educ
        self.client.force_login(self.user_asap)
        for url in self.asap_urls:
            self.assertEqual(self.client.get(url).status_code, 403)

        # Permissions for RE
        self.user_asap.user_permissions.add(Permission.objects.get(codename='manage_ser'))
        self.client.force_login(self.user_asap)
        for url in self.asap_urls:
            self.assertEqual(self.client.get(url).status_code, 200)
        for url in chain(self.aemo_urls, self.batoude_urls, self.asaef_urls):
            self.assertEqual(self.client.get(url).status_code, 403)

    def test_heure_par_intervenant_asap(self):
        Prestation.objects.bulk_create([
            Prestation(date=date(2021, 3, 1), auteur=self.user_asap, motif='tshm',
                          duree_prestation=timedelta(hours=2)),
            Prestation(date=date(2021, 4, 1), auteur=self.user_asap, motif='tshm',
                          duree_prestation=timedelta(hours=3)),
        ])

        self.client.force_login(self.manager_asap)
        param = "?start_month=1&start_year=2021&end_month=12&end_year=2021"
        response = self.client.get(reverse('stat-asap-heureintervenant') + param)
        response_key = response.context['intervenants_data'][0]['intervenants']['Asap Prénom']
        heures_act_ann = self.user_asap.heures_act_mens() * 12
        self.assertEqual(response_key['h_prestees']['total'], timedelta(hours=5))
        self.assertEqual(response_key['h_prestees']['pourcent'], round(5 / heures_act_ann * 100, 1))
        self.assertEqual(response_key['h_dues']['total'], timedelta(hours=heures_act_ann))
        self.assertEqual(response_key['ecart']['total'], timedelta(hours=5 - heures_act_ann))

    def test_bouton_export_invisible_pour_re_asap(self):
        self.user_asap.user_permissions.add(Permission.objects.get(codename='manage_ser'))
        self.client.force_login(self.user_asap)
        self.assertTrue(self.user_asap.is_responsable_equipe())
        param = "?start_month=1&start_year=2021&end_month=12&end_year=2021"
        response = self.client.get(reverse('stat-asap-suivi', args=['educatif']) + param)
        self.assertFalse(response.context['can_export'])

    @freeze_time("2024-02-15")
    def test_stat_prestations_mensuelles(self):
        janvier = date(2024, 1, 15)
        fevrier = date(2024, 2, 15)
        Prestation.objects.bulk_create([
            Prestation(date=janvier, auteur=self.user_asap, motif='tshm', duree_prestation=timedelta(hours=3)),
            Prestation(date=janvier, auteur=self.user_asap, motif='pe', duree_prestation=timedelta(hours=1)),
            Prestation(date=janvier, auteur=self.user_asap, motif='bulle', duree_prestation=timedelta(hours=3)),
            Prestation(date=janvier, auteur=self.user_asap, motif='autre', duree_prestation=timedelta(hours=3)),
            Prestation(date=fevrier, auteur=self.manager_asap, motif='tshm', duree_prestation=timedelta(hours=2)),
            Prestation(date=fevrier, auteur=self.manager_asap, motif='pe', duree_prestation=timedelta(hours=2)),
            Prestation(date=fevrier, auteur=self.manager_asap, motif='bulle', duree_prestation=timedelta(hours=2)),
            Prestation(date=fevrier, auteur=self.manager_asap, motif='autre', duree_prestation=timedelta(hours=2)),
        ])
        self.client.force_login(self.manager_asap)
        param = "?start_month=1&start_year=2024&end_month=3&end_year=2024"
        response = self.client.get(reverse('stat-asap-mensuelle') + param)
        key = response.context['data'][self.user_asap.nom_prenom]
        self.assertEqual(key['tshm']['tot_annee'], timedelta(hours=3))
        self.assertEqual(key['pe']['tot_annee'], timedelta(hours=1))
        self.assertEqual(key['bulle']['tot_annee'], timedelta(hours=3))
        self.assertEqual(key['autre']['tot_annee'], timedelta(hours=3))
        self.assertEqual(key['tot_mois'][(2024, 1)], timedelta(hours=10))
        self.assertEqual(key['tot_mois']['tot_annee'], timedelta(hours=10))

        key = response.context['data'][self.manager_asap.nom_prenom]
        self.assertEqual(key['tshm']['tot_annee'], timedelta(hours=2))
        self.assertEqual(key['pe']['tot_annee'], timedelta(hours=2))
        self.assertEqual(key['bulle']['tot_annee'], timedelta(hours=2))
        self.assertEqual(key['autre']['tot_annee'], timedelta(hours=2))
        self.assertEqual(key['tot_mois'][(2024, 2)], timedelta(hours=8))
        self.assertEqual(key['tot_mois']['tot_annee'], timedelta(hours=8))

        key = response.context['data']['Total ASAP']
        self.assertEqual(key['tshm']['tot_annee'], timedelta(hours=5))
        self.assertEqual(key['pe']['tot_annee'], timedelta(hours=3))
        self.assertEqual(key['bulle']['tot_annee'], timedelta(hours=5))
        self.assertEqual(key['autre']['tot_annee'], timedelta(hours=5))
        self.assertEqual(key['tot_mois'][(2024, 2)], timedelta(hours=8))
        self.assertEqual(key['tot_mois']['tot_annee'], timedelta(hours=18))

    def test_stat_region(self):
        dt = date(2024, 3, 10)
        Place.objects.bulk_create([
            Place(nom='Le Locle', localite='', version=2),
            Place(nom='Les Forges', localite='', version=2),
        ])
        self.assertEqual(Place.objects.count(), 2)
        prest = Prestation.objects.create(
            date=dt, auteur=self.user_asap, motif='tshm', duree_prestation=timedelta(hours=2)
        )
        Tshm.objects.bulk_create([
            Tshm(
                prestation=prest, typ='manifestation', duree=timedelta(hours=2),
                lieu=Place.objects.get(nom='Le Locle'), homme=1, femme=2, non_binaire=3
            ),
            Tshm(
                prestation=prest, typ='permanence', duree=timedelta(hours=1),
                lieu=Place.objects.get(nom='Les Forges'), homme=1, femme=2, non_binaire=3
            ),
            Tshm(
                prestation=prest, typ='permanence', duree=timedelta(hours=5),
                lieu=None, homme=5, femme=5, non_binaire=5
            )
        ])

        self.client.force_login(self.manager_asap)
        param = "?start_month=1&start_year=2024&end_month=12&end_year=2024"
        response = self.client.get(reverse('stat-asap-region') + param)

        key = response.context['regions']['Le Locle']
        self.assertEqual(key['manifestation']['tot_annee'], timedelta(hours=2))
        self.assertEqual(key['permanence']['tot_annee'], timedelta(hours=0))
        self.assertEqual(key['tot_mois'][(2024, 3)], timedelta(hours=2))
        self.assertEqual(key['tot_mois']['tot_annee'], timedelta(hours=2))

        key = response.context['regions']['Les Forges']
        self.assertEqual(key['manifestation']['tot_annee'], timedelta(hours=0))
        self.assertEqual(key['permanence']['tot_annee'], timedelta(hours=1))
        self.assertEqual(key['tot_mois'][(2024, 3)], timedelta(hours=1))
        self.assertEqual(key['tot_mois']['tot_annee'], timedelta(hours=1))

        key = response.context['regions']['Lieu non saisi']
        self.assertEqual(key['manifestation']['tot_annee'], timedelta(hours=0))
        self.assertEqual(key['permanence']['tot_annee'], timedelta(hours=5))
        self.assertEqual(key['tot_mois'][(2024, 3)], timedelta(hours=5))
        self.assertEqual(key['tot_mois']['tot_annee'], timedelta(hours=5))

    def test_stat_frequentation(self):
        dt = date(2024, 3, 10)
        Place.objects.bulk_create([
            Place(nom='Le Locle', localite='', version=2),
            Place(nom='Les Forges', localite='', version=2),
        ])
        self.assertEqual(Place.objects.count(), 2)
        prest = Prestation.objects.create(
            date=dt, auteur=self.user_asap, motif='tshm', duree_prestation=timedelta(hours=8)
        )
        Tshm.objects.bulk_create([
            Tshm(
                prestation=prest, typ='manifestation', duree=timedelta(hours=2),
                lieu=Place.objects.get(nom='Le Locle'), homme=1, femme=2, non_binaire=3
            ),
            Tshm(
                prestation=prest, typ='permanence', duree=timedelta(hours=1),
                lieu=Place.objects.get(nom='Les Forges'), homme=3, femme=3, non_binaire=3
            ),
            Tshm(
                prestation=prest, typ='permanence', duree=timedelta(hours=5),
                lieu=None, homme=5, femme=5, non_binaire=5
            )
        ])

        self.client.force_login(self.manager_asap)
        param = "?start_month=1&start_year=2024&end_month=12&end_year=2024"
        response = self.client.get(reverse('stat-asap-frequentation') + param)

        key = response.context['regions']['Le Locle']
        self.assertEqual(key['manifestation']['tot_annee'], 6)
        self.assertEqual(key['permanence']['tot_annee'], 0)
        self.assertEqual(key['tot_mois'][(2024, 3)], 6)
        self.assertEqual(key['tot_mois']['tot_annee'], 6)

        key = response.context['regions']['Les Forges']
        self.assertEqual(key['manifestation']['tot_annee'], 0)
        self.assertEqual(key['permanence']['tot_annee'], 9)
        self.assertEqual(key['tot_mois'][(2024, 3)], 9)
        self.assertEqual(key['tot_mois']['tot_annee'], 9)

        key = response.context['regions']['Lieu non saisi']
        self.assertEqual(key['manifestation']['tot_annee'], 0)
        self.assertEqual(key['permanence']['tot_annee'], 15)
        self.assertEqual(key['tot_mois'][(2024, 3)], 15)
        self.assertEqual(key['tot_mois']['tot_annee'], 15)

        key = response.context['regions']['Total ASAP']
        self.assertEqual(key['manifestation']['tot_annee'], 6)
        self.assertEqual(key['permanence']['tot_annee'], 24)
        self.assertEqual(key['tot_mois'][(2024, 3)], 30)
        self.assertEqual(key['tot_mois']['tot_annee'], 30)


class AsaefTests(StatTestBase):

    def setUp(self):
        self.user_asaef = Utilisateur.objects.get(username='user_asaef')
        self.user_asaef.user_permissions.add(Permission.objects.get(codename='view_asaef'))
        self.user_asaef.user_permissions.add(Permission.objects.get(codename='change_asaef'))

    def test_acces_asaef(self):
        # Forbidden for asaef educ
        self.client.force_login(self.user_asaef)
        for url in self.asaef_urls:
            self.assertEqual(self.client.get(url).status_code, 403)

        # Permissions for RE
        self.user_asaef.user_permissions.add(Permission.objects.get(codename='manage_asaef'))
        self.client.force_login(self.user_asaef)
        for url in self.asaef_urls:
            self.assertEqual(self.client.get(url).status_code, 200)
        for url in chain(self.aemo_urls, self.batoude_urls, self.asap_urls):
            self.assertEqual(self.client.get(url).status_code, 403)

    def test_suivis_export_asaef(self):
        famille = FamilleAsaef.objects.get(nom='Fam_Asaef')
        famille.suivi.date_demande = "2020-04-15"
        famille.suivi.date_debut_suivi = "2020-05-15"
        famille.suivi.date_fin_suivi = "2020-07-15"
        famille.suivi.save()

        famille2 = FamilleAsaef.objects.create_famille(nom='Fam_Asaef2')
        famille2.suivi.date_demande = "2020-05-15"
        famille2.suivi.date_debut_suivi = "2020-06-15"
        famille2.suivi.date_fin_suivi = "2020-07-15"
        famille2.suivi.save()

        self.client.force_login(self.user_admin)
        response = self.client.get(reverse('stat-asaef-suivi') + "?export=1")
        self.assertEqual(response['Content-Type'], openxml_contenttype)

    def test_motif_suivi_asaef(self):
        famille = FamilleAsaef.objects.get(nom='Fam_Asaef')
        famille.suivi.date_demande = "2020-04-15"
        famille.suivi.motif_demande = ['parentalite', 'developpement']
        famille.suivi.save()

        famille2 = FamilleAsaef.objects.create_famille(nom='Valjean')
        famille2.suivi.date_demande = "2020-05-15"
        famille2.suivi.motif_demande = ['parentalite', 'integration']
        famille2.suivi.save()

        self.client.force_login(self.user_admin)
        param = "?start_month=1&start_year=2020&end_month=12&end_year=2020"
        response = self.client.get(reverse('stat-asaef-motif-suivi') + param)
        response_key = response.context['motifs_data'][0]['dem']
        self.assertEqual(response_key['Soutien à la parentalité'][(2020, 4)], 1)
        self.assertEqual(response_key['Soutien à la parentalité'][(2020, 5)], 1)
        self.assertEqual(response_key['Soutien à la parentalité']['total'], 2)
        self.assertEqual(response_key['Soutien à la parentalité']['pourcent'], 50.0)

    def test_motif_depart_asaef(self):
        famille = FamilleAsaef.objects.get(nom='Fam_Asaef')
        famille.suivi.date_demande = "2020-04-15"
        famille.suivi.date_fin_suivi = "2020-05-12"
        famille.suivi.motif_fin_suivi = 'desengagement'
        famille.suivi.save()

        famille2 = FamilleAsaef.objects.create_famille(nom='Valjean')
        famille2.suivi.date_demande = "2020-05-15"
        famille2.suivi.date_fin_suivi = "2020-06-15"
        famille2.suivi.motif_fin_suivi = 'desengagement'
        famille2.suivi.save()

        self.client.force_login(self.user_admin)
        param = "?start_month=1&start_year=2020&end_month=12&end_year=2020"
        response = self.client.get(reverse('stat-asaef-motif-depart') + param)
        response_key = response.context['motifs_data']
        self.assertEqual(response_key[0]['depart']['Désengagement'][(2020, 5)], 1)
        self.assertEqual(response_key[0]['depart']['Désengagement'][(2020, 6)], 1)
        self.assertEqual(response_key[0]['depart']['Désengagement']['total'], 2)

    def test_suivis_asaef(self):
        famille = FamilleAsaef.objects.get(nom='Fam_Asaef')
        famille.suivi.date_demande = "2020-04-15"
        famille.suivi.date_debut_suivi = "2020-05-15"
        famille.suivi.date_fin_suivi = "2020-07-15"
        famille.suivi.save()

        famille2 = FamilleAsaef.objects.get(nom='Fam_Asaef2')
        famille2.suivi.date_demande = "2020-05-15"
        famille2.suivi.date_debut_suivi = "2020-06-15"
        famille2.suivi.date_fin_suivi = "2020-07-15"
        famille2.suivi.save()

        self.client.force_login(self.user_admin)
        param = "?start_month=1&start_year=2020&end_month=12&end_year=2020"
        response = self.client.get(reverse('stat-asaef-suivi') + param)
        response_key = response.context['entites'][0]['data']
        self.assertEqual(response_key['nouvelles_demandes'][(2020, 4)], 1)
        self.assertEqual(response_key['nouvelles_demandes'][(2020, 5)], 1)
        self.assertEqual(response_key['nouvelles_demandes']['total'], 2)

        self.assertEqual(response_key['suivis_en_cours'][(2020, 5)], 2)
        self.assertEqual(response_key['suivis_en_cours'][(2020, 6)], 2)
        self.assertEqual(response_key['suivis_en_cours'][(2020, 7)], 2)
        self.assertEqual(response_key['suivis_en_cours']['total'], 2)

        self.assertEqual(response_key['suivis_termines'][(2020, 6)], 0)
        self.assertEqual(response_key['suivis_termines'][(2020, 7)], 2)
        self.assertEqual(response_key['suivis_termines']['total'], 2)

        self.assertEqual(response.context['entites'][0]['attente'], 30)

    def test_annonceur_asaef(self):
        famille = FamilleAsaef.objects.get(nom='Fam_Asaef')
        famille.suivi.date_demande = "2020-04-15"
        famille.suivi.service_orienteur = "opec"
        famille.suivi.save()

        famille2 = FamilleAsaef.objects.create_famille(nom='Valjean')
        famille2.suivi.date_demande = "2020-05-15"
        famille2.suivi.service_orienteur = "open"
        famille2.suivi.save()

        self.client.force_login(self.user_admin)
        param = "?start_month=1&start_year=2020&end_month=12&end_year=2020"
        response = self.client.get(reverse('stat-asaef-annonceur') + param)
        response_key = response.context['motifs_data']
        self.assertEqual(response_key[0]['annonceur']['OPEC'][(2020, 4)], 1)
        self.assertEqual(response_key[0]['annonceur']['OPEC']['total'], 1)
        self.assertEqual(response_key[0]['annonceur']['OPEN'][(2020, 5)], 1)
        self.assertEqual(response_key[0]['annonceur']['OPEN']['total'], 1)

    def test_region_asaef(self):
        famille = FamilleAsaef.objects.get(nom='Fam_Asaef')
        famille.region = 'mont_locle'
        famille.save()
        famille.suivi.date_demande = "2020-04-15"
        famille.suivi.save()

        famille2 = FamilleAsaef.objects.create_famille(nom='Valjean', region='litt_ouest')
        famille2.suivi.date_demande = "2020-05-15"
        famille2.suivi.save()

        self.client.force_login(self.user_admin)
        param = "?start_month=1&start_year=2020&end_month=12&end_year=2020"
        response = self.client.get(reverse('stat-asaef-region') + param)
        response_key = response.context['regions']
        self.assertEqual(response_key['Le Locle'][(2020, 4)], 1)
        self.assertEqual(response_key['Littoral Ouest'][(2020, 5)], 1)

    def test_bouton_export_invisible_pour_re_asaef(self):
        self.user_asaef.user_permissions.add(Permission.objects.get(codename='manage_asaef'))
        self.client.force_login(self.user_asaef)
        self.assertTrue(self.user_asaef.is_responsable_equipe())
        response = self.client.get(reverse('stat-asaef-suivi'))
        self.assertFalse(response.context['can_export'])

    def test_age_asaef(self):
        famille = FamilleAsaef.objects.get(nom='Fam_Asaef')
        famille.suivi.date_demande = "2020-05-15"
        famille.suivi.save()

        enf1 = Personne.objects.create_personne(
            famille=famille, prenom='Jean', nom='Haddock',
            role=Role.objects.get(nom='Enfant suivi')
        )
        enf1.formation.statut = 'cycle1'
        enf1.formation.save()

        enf2 = Personne.objects.create_personne(
            famille=famille, prenom='Paul', nom='Haddock',
            role=Role.objects.get(nom='Enfant suivi')
        )
        enf2.formation.statut = ''
        enf2.formation.save()

        self.client.force_login(self.user_admin)
        param = "?start_month=1&start_year=2020&end_month=12&end_year=2020"
        response = self.client.get(reverse('stat-asaef-age') + param)
        response_key = response.context['motifs_data']
        self.assertEqual(response_key[0]['age']['Cycle 1'][(2020, 5)], 1)
        self.assertEqual(response_key[0]['empty'], 50)
        self.assertEqual(response_key[0]['age']['Cycle 1']['total'], 1)


class BatoudeTests(StatTestBase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        s1 = Service.objects.create(sigle='OPEN')
        Contact.objects.create(nom='Rathfeld', prenom='Christophe', service=s1)
        Utilisateur.objects.create_user('manager_batoude')

    def setUp(self):
        self.user_batoude = Utilisateur.objects.get(username='user_batoude')
        self.user_batoude.user_permissions.add(Permission.objects.get(codename='view_batoude'))
        self.user_batoude.user_permissions.add(Permission.objects.get(codename='change_batoude'))

        self.manager_batoude = Utilisateur.objects.get(username='manager_batoude')
        self.manager_batoude.user_permissions.add(Permission.objects.get(codename='manage_batoude'))

    def test_acces_batoudef(self):
        # Forbidden for batoude educ
        self.client.force_login(self.user_batoude)
        for url in self.batoude_urls:
            self.assertEqual(self.client.get(url).status_code, 403)

        # Permissions for RE
        self.client.force_login(self.manager_batoude)
        for url in self.batoude_urls:
            self.assertEqual(self.client.get(url).status_code, 200)
        for url in chain(self.aemo_urls, self.asaef_urls, self.asap_urls):
            self.assertEqual(self.client.get(url).status_code, 403)

    def test_liste_educ_batoude(self):
        user_batoude = Utilisateur.objects.create_user(
            'batoude', 'batoude@example.org', prenom='Jean', nom='Valjean', sigle='B1'
        )
        user_batoude2 = Utilisateur.objects.create_user(
            'batoude2', 'batoude2@example.org', prenom='John', nom='Doe', sigle='B2'
        )
        user_batoude.groups.add(Group.objects.get(name='batoude'))
        user_batoude2.groups.add(Group.objects.get(name='batoude'))
        user_batoude.user_permissions.add(Permission.objects.get(codename='view_batoude'))
        user_batoude.user_permissions.add(Permission.objects.get(codename='change_batoude'))
        user_batoude2.user_permissions.add(Permission.objects.get(codename='view_batoude'))
        user_batoude2.user_permissions.add(Permission.objects.get(codename='change_batoude'))

        self.client.force_login(self.user_admin)
        param = "?start_month=1&start_year=2021&end_month=4&end_year=2021"
        response = self.client.get(reverse('stat-batoude-heureintervenant') + param)
        self.assertContains(response, '<td>Valjean Jean</td>', html=True)
        self.assertContains(response, '<td>Doe John</td>', html=True)

        # Désactivation avant la période; n'apparaît pas
        user_batoude.date_desactivation = date(2020, 12, 31)
        user_batoude.is_active = False
        user_batoude.save()
        response = self.client.get(reverse('stat-batoude-heureintervenant') + param)
        self.assertNotContains(response, '<td>Valjean Jean</td>', html=True)
        self.assertContains(response, '<td>Doe John</td>', html=True)

        # Désactivation après le début de la période; apparaît
        user_batoude.date_desactivation = date(2021, 1, 31)
        user_batoude.is_active = False
        user_batoude.save()
        response = self.client.get(reverse('stat-batoude-heureintervenant') + param)
        self.assertContains(response, '<td>Valjean Jean</td>', html=True)
        self.assertContains(response, '<td>Doe John</td>', html=True)

    def test_bouton_export_invisible_pour_re_batoude(self):
        self.client.force_login(self.manager_batoude)
        self.assertTrue(self.manager_batoude.is_responsable_equipe())
        for url in self.batoude_urls:
            response = self.client.get(url)
            self.assertFalse(response.context['can_export'])

    def test_suivi_nouvelle_demande_out_of_date(self):
        ref_ope = Contact.objects.get(nom='Rathfeld')
        Beneficiaire.objects.bulk_create([
            Beneficiaire(nom='D1', prenom='P1', genre='M', region='mont_locle', referent_ope=ref_ope,
                         date_demande="2022-01-01"),
            Beneficiaire(nom='D1', prenom='P1', genre='M', region='mont_locle', referent_ope=ref_ope,
                         date_demande="2024-01-01"),
        ])

        self.client.force_login(self.manager_batoude)
        param = "?start_month=1&start_year=2023&end_month=12&end_year=2023"
        response = self.client.get(reverse('stat-batoude-suivi') + param)
        response_key = response.context['entites'][0]['data']
        self.assertEqual(response_key['nouvelles_demandes']['total'], 0)

    def test_suivi_en_cours_out_of_date(self):
        ref_ope = Contact.objects.get(nom='Rathfeld')
        Beneficiaire.objects.bulk_create([
            Beneficiaire(nom='D2', prenom='P2', genre='M', region='mont_locle', referent_ope=ref_ope,
                         date_demande="2022-01-01", date_admission="2022-03-01"),
            Beneficiaire(nom='D2', prenom='P2', genre='M', region='mont_locle', referent_ope=ref_ope,
                         date_demande="2022-01-01", date_admission="2022-03-01"),
        ])

        self.client.force_login(self.manager_batoude)
        param = "?start_month=1&start_year=2023&end_month=12&end_year=2023"
        response = self.client.get(reverse('stat-batoude-suivi') + param)
        response_key = response.context['entites'][0]['data']
        self.assertEqual(response_key['suivis_en_cours']['total'], 2)

    def test_suivis_termines_out_of_date(self):
        ref_ope = Contact.objects.get(nom='Rathfeld')
        Beneficiaire.objects.bulk_create([
            Beneficiaire(nom='D3', prenom='P3', genre='M', region='mont_locle', referent_ope=ref_ope,
                         date_demande="2022-02-01", date_admission="2022-04-15", date_sortie="2022-12-31"),
            Beneficiaire(nom='D3', prenom='P3', genre='M', region='mont_locle', referent_ope=ref_ope,
                         date_demande="2024-02-01", date_admission="2024-04-15", date_sortie="2024-12-31"),
        ])
        self.user_batoude.user_permissions.add(Permission.objects.get(codename='manage_batoude'))
        self.client.force_login(self.user_batoude)
        param = "?start_month=1&start_year=2023&end_month=12&end_year=2023"
        response = self.client.get(reverse('stat-batoude-suivi') + param)
        response_key = response.context['entites'][0]['data']
        self.assertEqual(response_key['suivis_termines']['total'], 0)

    def test_suivi_success(self):
        ref_ope = Contact.objects.get(nom='Rathfeld')
        Beneficiaire.objects.bulk_create([
            Beneficiaire(nom='D1', prenom='P1', genre='M', region='mont_locle', referent_ope=ref_ope,
                         date_demande="2023-01-01"),
            Beneficiaire(nom='D2', prenom='P2', genre='M', region='mont_locle', referent_ope=ref_ope,
                         date_demande="2023-01-01", date_admission="2023-03-01"),
            Beneficiaire(nom='D3', prenom='P3', genre='M', region='mont_locle', referent_ope=ref_ope,
                         date_demande="2023-02-01", date_admission="2023-04-15", date_sortie="2023-12-31"),
        ])
        self.client.force_login(self.manager_batoude)
        param = "?start_month=1&start_year=2023&end_month=12&end_year=2023"
        response = self.client.get(reverse('stat-batoude-suivi') + param)
        response_key = response.context['entites'][0]['data']
        self.assertEqual(response_key['nouvelles_demandes'][(2023, 1)], 2)
        self.assertEqual(response_key['nouvelles_demandes'][(2023, 2)], 1)
        self.assertEqual(response_key['nouvelles_demandes']['total'], 3)

        self.assertEqual(response_key['suivis_nouveaux'][(2023, 1)], 0)
        self.assertEqual(response_key['suivis_nouveaux'][(2023, 2)], 0)
        self.assertEqual(response_key['suivis_nouveaux'][(2023, 3)], 1)
        self.assertEqual(response_key['suivis_nouveaux'][(2023, 4)], 1)
        self.assertEqual(response_key['suivis_nouveaux']['total'], 2)

        self.assertEqual(response_key['suivis_en_cours'][(2023, 1)], 0)
        self.assertEqual(response_key['suivis_en_cours'][(2023, 2)], 0)
        self.assertEqual(response_key['suivis_en_cours'][(2023, 3)], 1)
        self.assertEqual(response_key['suivis_en_cours']['total'], 2)

        self.assertEqual(response_key['suivis_termines'][(2023, 12)], 1)
        self.assertEqual(response_key['suivis_termines']['total'], 1)

        self.assertEqual(response.context['entites'][0]['attente'], 66)

    def test_suivis_export(self):
        ref_ope = Contact.objects.get(nom='Rathfeld')
        Beneficiaire.objects.bulk_create([
            Beneficiaire(nom='D1', prenom='P1', genre='M', region='mont_locle', referent_ope=ref_ope,
                         date_demande="2023-01-01"),
            Beneficiaire(nom='D2', prenom='P2', genre='M', region='mont_locle', referent_ope=ref_ope,
                         date_demande="2023-01-01", date_admission="2023-03-01"),
            Beneficiaire(nom='D3', prenom='P3', genre='M', region='mont_locle', referent_ope=ref_ope,
                         date_demande="2023-02-01", date_admission="2023-04-15", date_sortie="2023-12-31"),
        ])

        self.client.force_login(self.user_admin)
        response = self.client.get(reverse('stat-batoude-suivi') + "?export=1")
        self.assertEqual(response['Content-Type'], openxml_contenttype)

    def test_motif_fin(self):
        ref_ope = Contact.objects.get(nom='Rathfeld')
        Beneficiaire.objects.bulk_create([
            Beneficiaire(nom='D1', prenom='P1', genre='M', region='mont_locle', referent_ope=ref_ope,
                         date_demande="2023-01-01", date_admission="2023-04-15", date_sortie="2023-05-31",
                         motif_fin_suivi=MotifsFinSuivi.ABANDON_BATOUDE),
            Beneficiaire(nom='D2', prenom='P2', genre='M', region='mont_locle', referent_ope=ref_ope,
                         date_demande="2023-01-01", date_admission="2023-03-01", date_sortie="2023-07-31",
                         motif_fin_suivi=MotifsFinSuivi.AUTRES),
            Beneficiaire(nom='D3', prenom='P3', genre='M', region='mont_locle', referent_ope=ref_ope,
                         date_demande="2023-02-01", date_admission="2023-04-15", date_sortie="2023-10-31",
                         motif_fin_suivi=MotifsFinSuivi.DESENGAGEMENT),
            Beneficiaire(nom='D4', prenom='P4', genre='M', region='mont_locle', referent_ope=ref_ope,
                         date_demande="2023-02-01", date_admission="2023-04-15", date_sortie="2023-12-31",
                         motif_fin_suivi=MotifsFinSuivi.AUTRES),
        ])
        self.client.force_login(self.manager_batoude)
        param = "?start_month=1&start_year=2023&end_month=12&end_year=2023"
        response = self.client.get(reverse('stat-batoude-motif-depart') + param)
        self.assertEqual(response.context['motifs_data'][0]['depart']['Refus de la Batoude'][(2023, 5)], 1)
        self.assertEqual(response.context['motifs_data'][0]['depart']['Autres'][(2023, 7)], 1)
        self.assertEqual(response.context['motifs_data'][0]['depart']['Désengagement'][(2023, 10)], 1)
        self.assertEqual(response.context['motifs_data'][0]['depart']['Autres']['total'], 2)
