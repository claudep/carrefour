from aemo.models import FamilleAemo, Intervention
from carrefour.export import ExportStatistique
from common.choices import SERVICE_ORIENTEUR_CHOICES
from .views import (
    StatAgeBaseView, StatAnnonceurBaseView, StatBaseView, StatHeureParIntervenantBaseView,
    StatMotifDepartBaseView, StatMotifSuiviBaseView, StatPrestationBaseView,
    StatRegionBaseView, StatSuiviBaseView
)


class StatSuiviView(StatSuiviBaseView):
    unite = 'aemo'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'months' in context:
            context['unite'] = 'aemo'
            context['titre'] = "Résumés des suivis AEMO"
            context['entites'] = [
                {
                    'titre': 'Aemo - Global',
                    'data': self.suivi_stats(FamilleAemo, context['months']),
                    'attente': self.attente
                }, {
                    'titre': 'AEMO - Montagnes',
                    'data': self.suivi_stats(FamilleAemo, context['months'], equipe='montagnes'),
                    'attente': self.attente
                }, {
                    'titre': 'AEMO - Littoral',
                    'data': self.suivi_stats(FamilleAemo, context['months'], equipe='littoral'),
                    'attente': self.attente
                }
            ]
        return context


class StatMotifSuiviView(StatMotifSuiviBaseView):

    unite = 'aemo'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'months' in context:
            motif_ann_dict = dict(self.motifs_choices)
            stats_aemo, aemo_empty = self.motifs_stats(FamilleAemo, context['months'])
            stats_montagnes, mont_empty = self.motifs_stats(
                FamilleAemo, context['months'], equipe='montagnes')
            stats_littoral, litt_empty = self.motifs_stats(
                FamilleAemo, context['months'], equipe='littoral')
            context['unite'] = self.unite
            context['titre'] = "Motif des demandes de suivis AEMO"
            context['motifs_data'] = [{
                'titre': 'AEMO - Global',
                'dem': {motif_ann_dict[key]: stats for key, stats in stats_aemo.items()},
                'empty': aemo_empty,
            }, {
                'titre': 'AEMO - Montagnes',
                'dem': {motif_ann_dict[key]: stats for key, stats in stats_montagnes.items()},
                'empty': mont_empty,
            }, {
                'titre': 'AEMO - Littoral',
                'dem': {motif_ann_dict[key]: stats for key, stats in stats_littoral.items()},
                'empty': litt_empty,
            }]
        return context


class StatMotifDepartView(StatMotifDepartBaseView):
    unite = 'aemo'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'months' in context:
            motif_fin_dict = dict(self.motifs_dict)
            stats_aemo, aemo_empty = self.motifs_stats(FamilleAemo, context['months'])
            stats_montagnes, mont_empty = self.motifs_stats(
                FamilleAemo, context['months'], equipe='montagnes')
            stats_littoral, litt_empty = self.motifs_stats(
                FamilleAemo, context['months'], equipe='littoral')
            context['unite'] = 'aemo'
            context['titre'] = "Motif des fins de suivis AEMO"
            context['motifs_data'] = [{
                'titre': 'AEMO - Global',
                'depart': {motif_fin_dict[key]: stats for key, stats in stats_aemo.items()
                           if key != 'erreur'},
            }, {
                'titre': 'AEMO - Montagnes',
                'depart': {motif_fin_dict[key]: stats for key, stats in stats_montagnes.items()
                           if key != 'erreur'},
            }, {
                'titre': 'AEMO - Littoral',
                'depart': {motif_fin_dict[key]: stats for key, stats in stats_littoral.items()
                           if key != 'erreur'},
            }]
        return context


class StatAnnonceurView(StatAnnonceurBaseView):
    unite = 'aemo'
    orienteurs = SERVICE_ORIENTEUR_CHOICES

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'months' in context:
            annonce_dict = dict(self.orienteurs)
            stats_aemo, aemo_empty = self.annonce_stats(FamilleAemo, context['months'])
            stats_montagnes, montagnes_empty = self.annonce_stats(
                FamilleAemo, context['months'], equipe='montagnes')
            stats_littoral, littoral_empty = self.annonce_stats(
                FamilleAemo, context['months'], equipe='littoral')
            context['unite'] = self.unite
            context['titre'] = "Services orienteurs"
            context['motifs_data'] = [{
                'titre': 'AEMO - Global',
                'annonceur': {annonce_dict[key]: stats for key, stats in stats_aemo.items()},
                'empty': aemo_empty,
            }, {
                'titre': 'AEMO - Montagnes',
                'annonceur': {annonce_dict[key]: stats for key, stats in stats_montagnes.items()},
                'empty': montagnes_empty,
            }, {
                'titre': 'AEMO - Littoral',
                'annonceur': {annonce_dict[key]: stats for key, stats in stats_littoral.items()},
                'empty': littoral_empty,
            }]
        return context


class StatLocaliteView(StatBaseView):
    template_name = 'statistiques/aemo_stats_localite.html'
    unite = 'aemo'

    def localite_stats(self, model, months):
        debut, fin = self.get_debut_fin(months)
        familles = FamilleAemo.suivis_nouveaux(debut, fin).values(
            'date_debut', 'date_fin', 'npa', 'localite',
        )
        counters = {}
        for famille in familles:
            loc_key = f"{famille['npa']} {famille['localite']}"
            if loc_key not in counters:
                counters[loc_key] = {'total': 0}
                counters[loc_key].update({month: 0 for month in months})
            month = (famille['date_debut'].year, famille['date_debut'].month)
            counters[loc_key][month] += 1
            counters[loc_key]['total'] += 1

        return {localite: counters[localite] for localite in sorted(counters.keys())}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'months' in context:
            context.update({
                'unite': 'aemo',
                'titre': "Répartition par localité de résidence",
                'localites': self.localite_stats(FamilleAemo, context['months']),
            })
        return context

    def render_to_response(self, context, **response_kwargs):
        if self.export_flag:
            export = ExportStatistique()
            export.produce_localite('Localités', context)
            return export.get_http_response(f"{context['unite']}_localités")
        else:
            return super().render_to_response(context, **response_kwargs)


class StatAgeView(StatAgeBaseView):
    unite = 'aemo'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        stats_aemo, empty_aemo = self.age_stats(FamilleAemo, context['months'])
        stats_montagnes, empty_montagnes = self.age_stats(FamilleAemo, context['months'], equipe='montagnes')
        stats_littoral, empty_littoral = self.age_stats(FamilleAemo, context['months'], equipe='littoral')
        age_dict = dict(self.formation_dict)

        if 'months' in context:
            context['unite'] = self.unite
            context['titre'] = "Répartition par cycle scolaire"
            context['motifs_data'] = [{
                'titre': 'AEMO - Global',
                'age': {age_dict[key]: stats for key, stats in stats_aemo.items()
                        if key in self.visible_ages},
                'empty': empty_aemo
            }, {
                'titre': 'AEMO - Montagnes',
                'age': {age_dict[key]: stats for key, stats in stats_montagnes.items()
                        if key in self.visible_ages},
                'empty': empty_montagnes
            }, {
                'titre': 'AEMO - Littoral',
                'age': {age_dict[key]: stats for key, stats in stats_littoral.items()
                        if key in self.visible_ages},
                'empty': empty_littoral
            }]
        return context


class StatSuiviParIntervenantView(StatBaseView):
    template_name = 'statistiques/aemo_stats_suiviintervenant.html'
    unite = 'aemo'

    def interv_stats(self, months, equipe=None):
        debut, fin = self.get_debut_fin(months)
        familles = FamilleAemo.suivis_en_cours(debut, fin).filter(
            suiviaemo__equipe=equipe
        )
        intervs = Intervention.objects.filter(suivi__famille__in=familles).select_related('intervenant', 'suivi')
        counters = {}
        for interv in intervs:
            key = interv.intervenant.nom_prenom
            if key not in counters:
                counters[key] = {'moyenne': 0}
                counters[key].update({month: 0 for month in months})
            for month in months:
                month_start, month_end = self.month_limits(month)
                fin_suivi = interv.date_fin or interv.suivi.date_fin_suivi or fin
                if interv.date_debut < month_end and fin_suivi >= month_start:
                    counters[key][month] += 1

        for interv, stats in counters.items():
            total = 0
            count = 0
            for month in months:
                count += 1
                total += counters[interv][month]
            counters[interv]['moyenne'] = round(total / count)

        return {intervenant: counters[intervenant] for intervenant in sorted(counters.keys())}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['unite'] = self.unite
        context['titre'] = "Nombre de suivis par intervenant-e AEMO"
        context['intervenants_data'] = [
            {
                'titre': 'AEMO - Littoral',
                'intervenants': self.interv_stats(context['months'], 'littoral'),
            }, {
                'titre': 'AEMO - Montagnes',
                'intervenants': self.interv_stats(context['months'], 'montagnes'),
            }]
        return context

    def render_to_response(self, context, **response_kwargs):
        if self.export_flag:
            export = ExportStatistique()
            export.produce_suivi_intervenant("Suivis par intervenant-e", context)
            return export.get_http_response(f"{self.unite}_suivis_par_intervenant")
        else:
            return super().render_to_response(context, **response_kwargs)


class StatRegionView(StatRegionBaseView):
    unite = 'aemo'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        regions_dict = dict(self.regions_choices)
        if 'months' in context:
            stats_regions, region_empty = self.regions_stats(FamilleAemo, context['months'])
            context.update({
                'unite': self.unite,
                'titre': "Nbre de suivis AEMO par régions",
                'regions': {regions_dict[key]: stats for key, stats in stats_regions.items()},
            })
        return context


class StatPrestationView(StatPrestationBaseView):
    template_name = "statistiques/stats_prestation.html"
    unite = 'aemo'


class StatHeureParIntervenantView(StatHeureParIntervenantBaseView):
    template_name = "statistiques/stats_heure_intervenant.html"
    unite = 'aemo'
