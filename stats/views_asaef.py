from asaef.models import FamilleAsaef
from common.choices import SERVICE_ORIENTEUR_ASAEF_CHOICES
from .views import (
    StatAgeBaseView, StatAnnonceurBaseView, StatHeureParIntervenantBaseView,
    StatMotifDepartBaseView, StatMotifSuiviBaseView, StatPrestationBaseView,
    StatRegionBaseView, StatSuiviBaseView
)


class StatPrestationView(StatPrestationBaseView):
    template_name = "statistiques/stats_prestation.html"
    unite = 'asaef'


class StatHeureParIntervenantView(StatHeureParIntervenantBaseView):
    template_name = "statistiques/stats_heure_intervenant.html"
    unite = 'asaef'


class StatSuiviView(StatSuiviBaseView):
    unite = 'asaef'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'months' in context:
            context['unite'] = 'asaef'
            context['titre'] = "Résumés des suivis ASAEF"
            context['entites'] = [
                {
                    'titre': 'ASAEF',
                    'data': self.suivi_stats(FamilleAsaef, context['months']),
                    'attente': self.attente
                }
            ]
        return context


class StatMotifSuiviView(StatMotifSuiviBaseView):
    unite = 'asaef'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'months' in context:
            motif_ann_dict = dict(self.motifs_choices)
            stats, empty = self.motifs_stats(FamilleAsaef, context['months'])
            context['unite'] = self.unite
            context['titre'] = "Motif des demandes de suivis ASAEF"
            context['motifs_data'] = [{
                'titre': 'ASAEF',
                'dem': {motif_ann_dict[key]: stats for key, stats in stats.items()},
                'empty': empty,
            }]
        return context


class StatMotifDepartView(StatMotifDepartBaseView):
    unite = 'asaef'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'months' in context:
            motif_fin_dict = dict(self.motifs_dict)
            stats, empty = self.motifs_stats(FamilleAsaef, context['months'])
            context['unite'] = self.unite
            context['titre'] = "Motif des fins de suivis ASAEF"
            context['motifs_data'] = [{
                'titre': 'ASAEF',
                'depart': {motif_fin_dict[key]: stats for key, stats in stats.items()
                           if key != 'erreur'},
            }]
        return context


class StatAnnonceurView(StatAnnonceurBaseView):
    unite = 'asaef'
    orienteurs = SERVICE_ORIENTEUR_ASAEF_CHOICES

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'months' in context:
            annonce_dict = dict(self.orienteurs)
            stats, empty = self.annonce_stats(FamilleAsaef, context['months'])
            context['unite'] = self.unite
            context['titre'] = "Services orienteurs"
            context['motifs_data'] = [{
                'titre': 'ASAEF',
                'annonceur': {annonce_dict[key]: stats for key, stats in stats.items()},
                'empty': empty,
            }]
        return context


class StatRegionView(StatRegionBaseView):
    unite = 'asaef'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        regions_dict = dict(self.regions_choices)
        if 'months' in context:
            stats_regions, region_empty = self.regions_stats(FamilleAsaef, context['months'])
            context.update({
                'unite': self.unite,
                'titre': "Nbre de suivis ASAEF par régions",
                'regions': {regions_dict[key]: stats for key, stats in stats_regions.items()},
            })
        return context


class StatAgeView(StatAgeBaseView):
    unite = 'asaef'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        stats, empty = self.age_stats(FamilleAsaef, context['months'])
        age_dict = dict(self.formation_dict)

        if 'months' in context:
            context['unite'] = self.unite
            context['titre'] = "Répartition par cycle scolaire"
            context['motifs_data'] = [{
                'titre': 'ASAEF',
                'age': {age_dict[key]: stats for key, stats in stats.items()
                        if key in self.visible_ages},
                'empty': empty
            }]
        return context
