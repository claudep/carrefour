from django.urls import path

from sondage import views

urlpatterns = [
    path('<str:unite>/list/', views.SondageListView.as_view(), name='sondage-list'),
    path('<str:unite>/create/', views.SondageCreateUpdateView.as_view(), name='sondage-create'),
    path('<int:pk>/update/', views.SondageCreateUpdateView.as_view(), name='sondage-update'),
    path('<int:pk>/theme/update/', views.ThemeUpdateView.as_view(), name='theme-update'),
    path('<int:pk>/reponse/', views.SondageReponseListView.as_view(), name='sondage-reponse-list'),
    path('<int:pk>/print/', views.SondagePrintPdfView.as_view(), name='sondage-print'),

    path('theme/<int:pk>/question/update/', views.QuestionUpdateView.as_view(), name='question-update'),

    path('reponse/<str:token>/print/', views.ReponsePrintPdfView.as_view(), name='reponse-print'),
    path('public/<str:token>/', views.ReponseSondageView.as_view(), name='reponse-sondage'),
    path('public/<str:token>/<int:qpk>/save/', views.ReponseQuestionSave.as_view(), name='question-save'),
    path('<int:pk>/graphique/', views.SondageGraphiqueView.as_view(), name='sondage-graphique'),
    path('questionnaire/<str:token>/graphique/', views.QuestionnaireGraphiqueView.as_view(),
         name='questionnaire-graphique'),

]
