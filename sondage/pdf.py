from reportlab_qrcode import QRCodeImage

from reportlab.lib.units import cm
from reportlab.platypus import KeepTogether, Paragraph, Spacer, Table

from carrefour.pdf import BaseCarrefourPDF

from .models import ReponseQuestion


class SondagePdf(BaseCarrefourPDF):
    title = "Questionnaire de satisfaction"

    def radioselect(self, questions, reponses, nb=4):
        assert nb < 6
        data = []
        ligne1 = ['' for n in range(nb + 1)]
        ligne1[1] = '1'
        ligne1[-1] = str(nb)
        for question in questions:
            ligne2 = [Paragraph(question.texte, self.style_normal)]
            reponse = reponses[question.id] if question.id in reponses else None
            for n in range(nb):
                car = 'X' if n == reponse else 'O'
                ligne2.append(car)
            data.extend([ligne1, ligne2])
            data.append([Spacer(0, 0.2 * cm)])

        t = Table(
            data=data, colWidths=[14 * cm, 0.4 * cm, 0.4 * cm, 0.4 * cm, 0.4 * cm, 0.4 * cm],
            hAlign='LEFT', rowHeights=0.4 * cm, spaceBefore=0 * cm, spaceAfter=0.5 * cm,
        )
        return t

    def produce(self, repsondage, request, **kwargs):
        sondage = repsondage.sondage
        self.title = ' '.join([self.title, sondage.unite.upper()])
        self.set_title()
        url = repsondage.url(request)
        self.story.append(
            Paragraph("Vous pouvez remplir ce questionnaire en ligne à l’adresse:", style=self.style_normal)
        )
        self.story.append(Spacer(0, 0.3 * cm))
        self.story.append(Paragraph(url, self.style_bold))
        self.story.append(Spacer(0, 0.3 * cm))
        self.story.append(
            Paragraph("Vous pouvez également scanner le code QR ci-dessous qui vous fournira la même adresse.",
                      style=self.style_normal)
        )
        # Ajout du QR_code
        qr = QRCodeImage(url, size=2 * cm)
        self.story.append(qr)
        self.story.append(Spacer(0, 1 * cm))

        self.story.append(Paragraph(sondage.consigne, self.style_italic))
        self.story.append(Spacer(0, 0.5 * cm))
        reponses = {item.question_id: item.reponse for item in ReponseQuestion.objects.filter(reps=repsondage)}
        for theme in sondage.themes.all().prefetch_related('questions'):
            if theme.questions.all():
                theme_nom = Paragraph(theme.nom, self.style_sub_title)
                table = self.radioselect(theme.questions.all(), reponses, sondage.nbre_choix)
                self.story.append(KeepTogether([theme_nom, table]))
        self.story.append(Paragraph("Nous vous remercions de votre collaboration.", style=self.style_normal))
        return super().produce()
