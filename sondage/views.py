
from io import BytesIO

from django.core.exceptions import PermissionDenied
from django.db.models import Avg, Count, F, IntegerField, ExpressionWrapper
from django.http import FileResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, reverse
from django.views.generic import (
    FormView, ListView, TemplateView, UpdateView, View
)

from carrefour.graph import SondageRadarGraph
from carrefour.views import MSG_NO_ACCESS

from .forms import QuestionForm, ReponseSondageForm, SondageForm, ThemeForm
from .pdf import SondagePdf
from .models import Question, ReponseSondage, ReponseQuestion, Sondage, Theme


class CreateUpdateView(UpdateView):
    """Mix generic Create and Update views."""
    is_create = False

    def get_object(self):
        return None if self.is_create else super().get_object()


class SondageListView(ListView):
    template_name = 'sondage/sondage_list.html'
    model = Sondage

    def dispatch(self, request, *args, **kwargs):
        self.unite = kwargs.get('unite', 'asaef')
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return super().get_queryset().filter(unite=self.unite)

    def get_context_data(self, *args, **kwargs):
        return {**super().get_context_data(*args, **kwargs), 'source': self.unite}


class SondageCreateUpdateView(CreateUpdateView):
    template_name = 'sondage/sondage_change.html'
    model = Sondage
    form_class = SondageForm

    def dispatch(self, request, *args, **kwargs):
        self.is_create = 'pk' not in kwargs
        self.unite = kwargs['unite'] if self.is_create else None
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'source': self.kwargs['unite'] if self.is_create else self.object.unite,
            'themes': [] if self.is_create else self.object.themes.order_by('order'),
            'sondage': None if self.is_create else self.object,
            'titre_page': 'Nouveau sondage' if self.is_create else 'Modification',
        }

    def form_valid(self, form):
        if self.is_create:
            form.instance.unite = self.kwargs['unite']
            obj = form.save()
            return HttpResponseRedirect(reverse("sondage-update", args=[obj.pk]))
        else:
            obj = form.save()
            return HttpResponseRedirect(reverse("sondage-list", args=[obj.unite]))


class ThemeUpdateView(UpdateView):
    template_name = 'sondage/theme_change.html'
    model = Sondage
    form_class = ThemeForm
    context_object_name = 'sondage'

    def get_success_url(self):
        return reverse('sondage-update', args=[self.object.pk])

    def get_context_data(self, *args, **kwargs):
        return {**super().get_context_data(**kwargs), 'source': self.object.unite}


class QuestionUpdateView(UpdateView):
    template_name = 'sondage/question_change.html'
    model = Theme
    form_class = QuestionForm
    context_object_name = 'theme'

    def get_success_url(self):
        return reverse('sondage-update', args=[self.object.sondage.pk])

    def get_context_data(self, *args, **kwargs):
        return {**super().get_context_data(**kwargs), 'source': self.object.sondage.unite}


class SondagePrintPdfView(View):
    def dispatch(self, request, *args, **kwargs):
        self.sondage = get_object_or_404(Sondage, pk=kwargs['pk'])
        if not request.user.is_responsable_unite(self.sondage.unite):
            raise PermissionDenied(MSG_NO_ACCESS)
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        reps = ReponseSondage.nouveau(self.sondage)
        temp = BytesIO()
        pdf = SondagePdf(temp)
        pdf.produce(reps, self.request)
        filename = f'sondage_{self.sondage.unite}.pdf'
        temp.seek(0)
        return FileResponse(temp, as_attachment=False, filename=filename)


class ReponseSondageView(FormView):
    template_name = 'sondage/reponse_sondage.html'
    form_class = ReponseSondageForm

    def dispatch(self, request, *args, **kwargs):
        self.reps = get_object_or_404(ReponseSondage, token=kwargs['token'])
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            'reps': self.reps,
        }

    def get_success_url(self):
        return '/'


class ReponseQuestionSave(View):
    """
    Sauvegarde immédiate après chaque modification
    Accès depuis formulaire public
    """

    def post(self, request, *args, **kwargs):
        reps = get_object_or_404(ReponseSondage, token=kwargs['token'])
        question = get_object_or_404(Question, pk=request.POST.get('q'))
        obj, _ = ReponseQuestion.objects.update_or_create(
            reps=reps,
            question=question,
            defaults={'reponse': request.POST.get('v')}
        )
        retour = 'ok' if obj else 'erreur'
        return JsonResponse(retour, safe=False)


class SondageReponseListView(ListView):
    template_name = 'sondage/reponse_list.html'
    model = ReponseSondage

    def dispatch(self, request, *args, **kwargs):
        self.sondage = get_object_or_404(Sondage, pk=kwargs['pk'])
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return self.sondage.reponses.annotate(
            remplissage=ExpressionWrapper(
                Count(F('reponses__reponse')) * 1.0 /
                Question.objects.filter(theme__sondage=self.sondage).count() * 100,
                output_field=IntegerField()
            )
        )

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'sondage': Sondage.objects.get(pk=self.kwargs['pk'])
        }


class ReponsePrintPdfView(View):
    def dispatch(self, request, *args, **kwargs):
        self.reponse = get_object_or_404(ReponseSondage, token=kwargs['token'])
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        temp = BytesIO()
        pdf = SondagePdf(temp)
        pdf.produce(self.reponse, self.request)
        filename = f'reponse_{self.reponse.sondage.unite}_{kwargs["token"]}.pdf'
        temp.seek(0)
        return FileResponse(temp, as_attachment=False, filename=filename)


class SondageGraphiqueView(TemplateView):
    """
    Graphique montrant les résultats de tous les questionnaires d'un sondage
    """
    template_name = 'sondage/graphique.html'

    def dispatch(self, request, *args, **kwargs):
        self.sondage = get_object_or_404(Sondage, pk=kwargs['pk'])
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        reponses = []
        for theme in self.sondage.themes.all():
            rep = ReponseQuestion.objects.filter(
                question__in=theme.questions.all()
            ).aggregate(Avg('reponse', default=0))
            reponses.append(rep['reponse__avg'])
        graph = SondageRadarGraph(nom=self.sondage.nom, sondage=self.sondage, reponses=reponses)
        return {
            **super().get_context_data(**kwargs),
            'sondage': self.sondage,
            'graphic': graph.graphic,
            'source': 'asaef'
        }


class QuestionnaireGraphiqueView(TemplateView):
    """
    Graphique montrant les résultats d'un seul questionnaire
    """
    template_name = 'sondage/graphique.html'

    def dispatch(self, request, *args, **kwargs):
        self.reps = get_object_or_404(ReponseSondage, token=kwargs['token'])
        self.sondage = self.reps.sondage
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        reponses = []
        for theme in self.sondage.themes.all():
            rep = ReponseQuestion.objects.filter(
                question__in=theme.questions.all(),
                reps=self.reps
            ).aggregate(Avg('reponse', default=0))
            reponses.append(rep['reponse__avg'])
        graph = SondageRadarGraph(f"Questionnaire {self.reps.token}", self.sondage, reponses)
        return {
            **super().get_context_data(**kwargs),
            'sondage': self.sondage,
            'graphic': graph.graphic,
            'source': 'asaef'
        }