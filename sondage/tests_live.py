from django.urls import reverse

from selenium.webdriver.common.by import By

from common.test_utils import LiveTestBase

from .models import Question, ReponseQuestion, ReponseSondage
from .tests import SondageDataMixin


class LiveTests(SondageDataMixin, LiveTestBase):

    def setUp(self):
        self.setUpTestData()

    def test_saisie_automatique_d_une_reponse(self):
        self.assertEqual(ReponseQuestion.objects.count(), 0)
        self.client.force_login(self.manager_asaef)
        self.client.get(reverse('sondage-print', args=[self.sondage.pk]))
        reps = ReponseSondage.objects.get(sondage=self.sondage)

        question_pk = Question.objects.first().pk
        self.selenium.get('%s%s' % (self.live_server_url, reverse('reponse-sondage', args=[reps.token])))
        self.selenium.find_element(By.ID, f'id_{question_pk}_3').click()
        self.assertTrue(ReponseQuestion.objects.filter(question_id=question_pk, reps=reps, reponse=4).exists())
