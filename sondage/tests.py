from django.contrib.auth.models import Permission
from django.shortcuts import reverse
from django.test import RequestFactory, TestCase

from carrefour.models import Utilisateur
from common.test_utils import TestUtilsMixin
from .models import Question, ReponseQuestion, ReponseSondage, Sondage, Theme


class SondageDataMixin(TestUtilsMixin):
    @classmethod
    def setUpTestData(cls):
        cls.sondage = Sondage.objects.create(unite='asaef', nom="Sondage Test")
        themes = Theme.objects.bulk_create([
            Theme(sondage=cls.sondage, nom="Thème No 1", order=10),
            Theme(sondage=cls.sondage, nom="Thème No 2", order=20),
            Theme(sondage=cls.sondage, nom="Thème No 3", order=30),
        ])

        Question.objects.bulk_create([
            Question(theme=themes[0], texte="Question No 1", order=10),
            Question(theme=themes[0], texte="Question No 2", order=20),
            Question(theme=themes[1], texte="Question No 3", order=10),
        ])
        cls.manager_asaef =Utilisateur.objects.create_user(
            'manager_asaef', 'manager_asaef@example.org', sigle='MA', nom='Lampion', prenom='Séraphin'
        )
        cls.manager_asaef.user_permissions.add(Permission.objects.get(codename='manage_asaef'))


class SondageTests(SondageDataMixin, TestCase):
    def test_edition_sondage(self):
        self.client.force_login(self.manager_asaef)
        response = self.client.post(
            reverse('sondage-create', args=['asaef']), data={'nom': 'Test', 'nbre_choix': 5}
        )
        sondage = Sondage.objects.get(nom='Test')
        self.assertEqual(sondage.unite, 'asaef')
        self.assertEqual(sondage.nbre_choix, 5)
        self.assertRedirects(response, reverse('sondage-update', args=[sondage.pk]))

        self.client.post(reverse('sondage-update', args=[sondage.pk]), data={'nom': 'Essai', 'nbre_choix': 4})
        sondage.refresh_from_db()
        self.assertEqual(sondage.nom, 'Essai')
        self.assertEqual(sondage.nbre_choix, 4)

    def test_edition_theme(self):
        sondage = Sondage.objects.create(nom="Sondage No 1", nbre_choix=4, unite='asaef')
        self.client.force_login(self.manager_asaef)
        response = self.client.post(
            reverse('theme-update', args=[sondage.pk]),
            instance=sondage,
            data={
                "theme-TOTAL_FORMS": "2", "theme-INITIAL_FORMS": "0",
                "theme-0-nom": "Généralités", 'theme-0-ORDER': 10,
                "theme-1-nom": "Conclusion", 'theme-1-ORDER': 20,
            }
        )
        self.assertRedirects(response, reverse('sondage-update', args=[sondage.pk]))
        self.assertEqual(sondage.themes.count(), 2)

    def test_question_edition(self):
        sondage = Sondage.objects.create(nom="Sondage No 1", nbre_choix=4, unite='asaef')
        theme = Theme.objects.create(nom='Theme No 1', sondage=sondage, order=10)
        self.client.force_login(self.manager_asaef)
        response = self.client.post(
            reverse('question-update', args=[theme.pk]),
            instance=theme,
            data={
                "question-TOTAL_FORMS": "2", "question-INITIAL_FORMS": "0",
                "question-0-texte": "Question No 1", 'question-0-ORDER': 10,
                "question-1-texte": "Question No 2", 'question-1-ORDER': 20,
            }
        )
        self.assertRedirects(response, reverse('sondage-update', args=[sondage.pk]))
        self.assertEqual(theme.questions.count(), 2)

    def test_impression(self):
        self.client.force_login(self.manager_asaef)
        response = self.client.get(reverse('sondage-print', args=[self.sondage.pk]))

        self.assertEqual(response.status_code, 200)
        pdf_text = self.extract_page_pdf(response, 0)
        self.assertIn("Questionnaire de satisfaction ASAEF", pdf_text)

    def test_lien_pour_reponse(self):
        self.client.force_login(self.manager_asaef)
        response = self.client.get(reverse('sondage-print', args=[self.sondage.pk]))
        reps = ReponseSondage.objects.get(sondage=self.sondage)
        pdf_text = self.extract_page_pdf(response, 0)
        self.assertIn(f'/sondage/public/{reps.token}/', pdf_text)


class ReponseSondageTests(SondageDataMixin, TestCase):
    def test_nouveau(self):
        reps = ReponseSondage.nouveau(self.sondage)
        self.assertEqual(len(reps.token), 6)

    def test_affichage_questionnaire_pour_famille(self):
        self.client.force_login(self.manager_asaef)
        self.client.get(reverse('sondage-print', args=[self.sondage.pk]))
        reps = ReponseSondage.objects.get(sondage=self.sondage)

        factory = RequestFactory()
        request = factory.get(reverse('reponse-sondage', args=[reps.token]))
        response = self.client.get(reps.url(request))
        self.assertContains(response, "Thème No 1")
        self.assertContains(response, "Thème No 2")
        self.assertNotContains(response, "Thème No 3")  # Pas affiché, parce que vide
        self.assertContains(response, "Question No 1")
        self.assertContains(response, "Question No 2")
        self.assertContains(response, "Question No 3")

    def test_enregistrement_reponse(self):
        self.client.force_login(self.manager_asaef)
        self.client.get(reverse('sondage-print', args=[self.sondage.pk]))
        reps = ReponseSondage.objects.get(sondage=self.sondage)
        question_pk = Question.objects.first().pk
        response = self.client.post(
            reverse('question-save', args=[reps.token, question_pk]),
            data={'q': question_pk, 'v': 4},
        )
        self.assertEqual(response.content, b'"ok"')
        self.assertTrue(
            ReponseQuestion.objects.filter(question_id=question_pk, reps=reps, reponse=4).exists()
        )

    def test_reponse_list(self):
        self.client.force_login(self.manager_asaef)
        for i in range(3):
            self.client.get(reverse('sondage-print', args=[self.sondage.pk]))
        self.assertEqual(ReponseSondage.objects.count(), 3)
        response = self.client.get(reverse('sondage-reponse-list', args=[self.sondage.pk]))
        for reps in ReponseSondage.objects.all():
            self.assertContains(response, reps.token)
            self.assertContains(response, f"{reverse('reponse-sondage', args=[reps.token])}", html=False)

    def test_questionnnaire_rempli(self):
        self.client.force_login(self.manager_asaef)
        self.client.get(reverse('sondage-print', args=[self.sondage.pk]))
        repsondage = ReponseSondage.objects.get(sondage=self.sondage)
        response = self.client.get(reverse('reponse-print', args=[repsondage.token]))
        self.assertEqual(response.status_code, 200)

    def test_suppression_menu(self):
        self.client.force_login(self.manager_asaef)
        self.client.get(reverse('sondage-print', args=[self.sondage.pk]))
        reps = ReponseSondage.objects.get(sondage=self.sondage)
        factory = RequestFactory()
        request = factory.get(reverse('reponse-sondage', args=[reps.token]))
        response = self.client.get(reps.url(request))

        menus = ['home', 'asaef-famille-list', 'asaef-prestation-menu', 'asaef-suivis-termines', 'contact-list',
                 'service-list', 'stat-asaef-heureintervenant']
        for menu in menus:
            self.assertNotContains(response, f'<a href="{reverse(menu)}"', html=True)
        self.assertNotContains(response, reverse('sondage-list', args=['asaef']))


class GraphiqueTests(SondageDataMixin, TestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.reps = ReponseSondage.objects.create(sondage=cls.sondage, token="PPPPPP")

    def test_lien_sondage_graphique(self):
        self.client.force_login(self.manager_asaef)
        response = self.client.get(reverse('sondage-list', args=['asaef']))
        graph_url = reverse('sondage-graphique', args=[self.sondage.pk])
        self.assertContains(response, f'href="{graph_url}"')

    def test_sondage_graphique_html(self):
        self.client.force_login(self.manager_asaef)
        graph_url = reverse('sondage-graphique', args=[self.sondage.pk])
        response = self.client.get(graph_url)
        self.assertEqual(response.status_code, 200)
        self.assertGreater(len(response.content.decode()), 6000)

    def test_lien_questionnaire_graphique(self):
        self.client.force_login(self.manager_asaef)
        response = self.client.get(reverse('sondage-reponse-list', args=[self.sondage.pk]))
        graph_url = reverse('questionnaire-graphique', args=['PPPPPP'])
        self.assertContains(response, f'href="{graph_url}"')

    def test_questionnaire_graphique_html(self):
        self.client.force_login(self.manager_asaef)
        graph_url = reverse('questionnaire-graphique', args=['PPPPPP'])
        response = self.client.get(graph_url)
        self.assertEqual(response.status_code, 200)
        self.assertGreater(len(response.content.decode()), 6000)
