from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sondage', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='sondage',
            name='nbre_choix',
            field=models.PositiveSmallIntegerField(default=5, verbose_name='Nbre de choix'),
        ),
        migrations.AddField(
            model_name='sondage',
            name='consigne',
            field=models.TextField(blank=True, verbose_name='Consigne'),
        ),
    ]
