from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sondage', '0002_sondage_consigne_nbre_choix'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Token',
        ),
    ]
