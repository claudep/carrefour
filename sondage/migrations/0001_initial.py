import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name='Sondage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('unite', models.CharField(choices=[('aemo', 'AEMO'), ('asaef', 'ASAEF')], max_length=10, verbose_name='Unité')),
                ('nom', models.CharField(max_length=200, verbose_name='Nom')),
            ],
        ),
        migrations.CreateModel(
            name='Theme',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=200, verbose_name='Nom')),
                ('order', models.PositiveSmallIntegerField(verbose_name='Ordre')),
                ('sondage', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='themes', to='sondage.sondage')),
            ],
            options={
                'ordering': ['sondage', 'order'],
            },
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('theme', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='questions', to='sondage.theme')),
                ('texte', models.TextField(verbose_name='Texte')),
                ('order', models.PositiveSmallIntegerField(verbose_name='Ordre')),
            ],
            options={
                'ordering': ['theme__order', 'order'],
            },
        ),
        migrations.CreateModel(
            name='ReponseSondage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('token', models.CharField(editable=False, max_length=20, verbose_name='Jeton')),
                ('sondage', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='reponses', to='sondage.sondage')),
                ('date_creation', models.DateField(auto_now_add=True)),
                ('termine_le', models.DateField(blank=True, null=True)),
                ('remarques', models.TextField(blank=True, verbose_name='Remarques')),
            ],
        ),
        migrations.CreateModel(
            name='Token',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('token', models.CharField(editable=False, max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='ReponseQuestion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('reponse', models.SmallIntegerField(blank=True, default=0)),
                ('question', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='sondage.question')),
                ('reps', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='reponses', to='sondage.reponsesondage')),
            ],
        ),
    ]
