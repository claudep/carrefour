import random
import string

from django.db import models


class Sondage(models.Model):
    unite = models.CharField('Unité', max_length=10, choices=(('aemo', 'AEMO'), ('asaef', 'ASAEF')))
    nom = models.CharField('Nom', max_length=200)
    nbre_choix = models.PositiveSmallIntegerField("Nbre de choix", default=5)
    consigne = models.TextField('Consigne', blank=True)

    def __str__(self):
        return f"{self.nom} ({self.unite})"


class Theme(models.Model):
    sondage = models.ForeignKey(to=Sondage, related_name='themes', on_delete=models.CASCADE)
    nom = models.CharField('Nom', max_length=200)
    order = models.PositiveSmallIntegerField('Ordre')

    class Meta:
        ordering = ['sondage', 'order']

    def __str__(self):
        return self.nom


class Question(models.Model):
    theme = models.ForeignKey(to=Theme, related_name='questions', on_delete=models.CASCADE)
    texte = models.TextField('Texte')
    order = models.PositiveSmallIntegerField('Ordre')

    class Meta:
        ordering = ['theme__order', 'order']

    def __str__(self):
        return f"{self.theme} : {self.texte[:20]}"


class ReponseSondage(models.Model):
    """
    Ensemble des réponses à un sondage par une entité familiale.
    """
    token = models.CharField("Jeton", max_length=20, editable=False)
    sondage = models.ForeignKey(to=Sondage, related_name='reponses', on_delete=models.CASCADE)
    date_creation = models.DateField(auto_now_add=True)
    termine_le = models.DateField(null=True, blank=True)
    remarques = models.TextField("Remarques", blank=True)

    class Meta:
        models.UniqueConstraint(fields=['token'], name='unique_token')

    @classmethod
    def nouveau(cls, sondage):
        while True:
            token = ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))
            if not cls.objects.filter(token=token).exists():
                break
        return cls.objects.create(sondage=sondage, token=token)

    def __str__(self):
        return f"Réponse sondage «{self.token}» du {self.date_creation}"

    def url(self, request):
        return request.build_absolute_uri(f'/sondage/public/{self.token}/')


class ReponseQuestion(models.Model):
    """
    Réponse à une question d’un sondage par une famille.
    """
    reps = models.ForeignKey(to=ReponseSondage, related_name='reponses', on_delete=models.CASCADE)
    question = models.ForeignKey(to=Question, on_delete=models.PROTECT)
    reponse = models.SmallIntegerField(blank=True, default=0)

    class Meta:
        models.UniqueConstraint(fields=['reps', 'question'], name='reponse_question_unique')

    def __str__(self):
        return f"{self.question} : {self.reponse}"
