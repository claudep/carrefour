from django import forms

from carrefour.forms import BootstrapMixin
from .models import Question, ReponseQuestion, Sondage, Theme


class ThemeFormsetForm(BootstrapMixin, forms.ModelForm):

    class Meta:
        model = Theme
        fields = ['nom']


class QuestionFormsetForm(BootstrapMixin, forms.ModelForm):

    class Meta:
        model = Question
        fields = ['texte']
        widgets = {
            'texte': forms.TextInput(attrs={'rows': 2})
        }


class SondageForm(BootstrapMixin, forms.ModelForm):

    class Meta:
        model = Sondage
        fields = ['nom', 'nbre_choix', 'consigne']
        widgets = {
            'nbre_choix': forms.RadioSelect(
                choices=((2, 2), (3, 3), (4, 4), (5, 5))
            ),
            'consigne': forms.Textarea(attrs={'rows': 3}),
        }


class CustomInlineFormSet(forms.BaseInlineFormSet):
    @classmethod
    def get_ordering_widget(cls):
        return cls.ordering_widget(attrs={'class': 'form-control'})

    @classmethod
    def get_deletion_widget(cls):
        return cls.deletion_widget(attrs={'class': 'form-check-input'})


class ThemeForm(BootstrapMixin, forms.ModelForm):

    class Meta:
        model = Sondage
        fields = ['id']

    def __init__(self, data=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance and self.instance.pk:
            ThemeFormset = forms.inlineformset_factory(
                Sondage, Theme, form=ThemeFormsetForm, formset=CustomInlineFormSet,
                can_delete=True, can_order=True
            )
            self.formset = ThemeFormset(
                instance=self.instance, data=data, queryset=Theme.objects.filter(sondage=self.instance),
                prefix='theme'
            )
        self.formset.extra = 0 if self.instance and self.instance.pk else 1

    def is_valid(self):
        return all([super().is_valid(), self.formset.is_valid()])

    def has_changed(self):
        return any([super().has_changed(), self.formset.has_changed()])

    def save(self, commit=True):
        for obj in self.formset.ordered_forms:
            obj.instance.order = obj.cleaned_data['ORDER']
            obj.instance.save()
        for obj in self.formset.deleted_forms:
            obj.instance.delete()
        return self.instance


class QuestionForm(BootstrapMixin, forms.ModelForm):

    class Meta:
        model = Theme
        fields = ['id']

    def __init__(self, data=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance and self.instance.pk:
            QuestionFormset = forms.inlineformset_factory(
                Theme, Question, form=QuestionFormsetForm, formset=CustomInlineFormSet, can_delete=True, can_order=True
            )
            self.formset = QuestionFormset(
                instance=self.instance, data=data, queryset=Question.objects.filter(theme=self.instance),
                prefix='question'
            )
            self.formset.extra = 0 if self.instance and self.instance.pk else 1

    def is_valid(self):
        return all([super().is_valid(), self.formset.is_valid()])

    def has_changed(self):
        return any([super().has_changed(), self.formset.has_changed()])

    def save(self, commit=True):
        for obj in self.formset.ordered_forms:
            obj.instance.order = obj.cleaned_data['ORDER']
            obj.instance.save()
        for obj in self.formset.deleted_forms:
            obj.instance.delete()
        return self.instance


class ReponseSondageForm(BootstrapMixin, forms.Form):

    def __init__(self, reps, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.reps = reps
        choices = [(n, n) for n in range(1, reps.sondage.nbre_choix + 1)]
        reponses = {rep.question.pk: rep.reponse for rep in ReponseQuestion.objects.filter(reps=reps)}
        # Création dynamique d'un champ à choix pour chaque question du sondage
        for theme in reps.sondage.themes.all():
            for question in theme.questions.all():
                self.fields[question.pk] = forms.ChoiceField(
                    label=question.texte,
                    choices=choices,
                    required=False,
                    widget=forms.RadioSelect(attrs={'class': 'form-check-input'}),
                    initial=reponses.get(question.pk),
                )
                self.fields[question.pk].question = question
