from django.contrib import admin

from .models import Question, ReponseQuestion, ReponseSondage, Sondage, Theme


@admin.register(Sondage)
class SondageAdmin(admin.ModelAdmin):
    list_display = ('unite', 'nom')


@admin.register(Theme)
class ThemeAdmin(admin.ModelAdmin):
    list_display = ('nom', 'sondage', 'order')
    ordering = ('sondage', 'order')


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ('texte', 'theme', 'order')
    ordering = ('theme', 'order')


@admin.register(ReponseSondage)
class ReponseSondageAdmin(admin.ModelAdmin):
    list_display = ('token', 'date_creation', 'termine_le')


@admin.register(ReponseQuestion)
class ReponseQuestionAdmin(admin.ModelAdmin):
    list_display = ('question', 'reponse')
